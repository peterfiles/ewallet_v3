﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class UsersTempRepository : IUsersTempRepository
    {
        readonly IRepositoryEF<UserTemp> repo;
        readonly CW_EntitiesAzure db;

        public UsersTempRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<UserTemp>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(mobile_number.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.UserTemps.Where(sWhere).Count() : db.UserTemps.Count();
        }

        public UsersTempEx Create()
        {
            return new UsersTempEx();
        }

        public UsersTempEx Get(int Id)
        {
            var n = db.UserTemps.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public UsersTempEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private UsersTempEx Transform(UserTemp n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new UsersTempEx();
            l.id = n.id;
            l.mobile_number = n.mobile_number;
            l.OTP = n.OTP;
            l.token_id = n.token_id;
            l.email = n.email;
            l.password = n.password;
            return l;
        }

        private UserTemp Transform(UsersTempEx n)
        {
            UserTemp l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.mobile_number = n.mobile_number;
            l.OTP = n.OTP;
            l.token_id = n.token_id;
            l.email = n.email;
            l.password = n.password;

            return l;
        }

        public List<UsersTempEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(mobile_number.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.UserTemps.Where(sWhere).OrderBy(orderby) : db.UserTemps.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
           else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(UsersTempEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(UsersTempEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();
            l.id = n.id;
            l.mobile_number = n.mobile_number;
            l.OTP = n.OTP;
            l.token_id = n.token_id;
            l.email = n.email;
            l.password = n.password;
        }


      

        private List<UsersTempEx> Transform(IQueryable<UserTemp> aUsersTemp)
        {
            List<UsersTempEx> l = new List<UsersTempEx>();
            foreach (UserTemp b in aUsersTemp)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
