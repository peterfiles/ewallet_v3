﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class PayInPaymentRatesRepository : IPayInPaymentRatesRepository
    {
        readonly IRepositoryEF<payinsPaymentRate> payInRepo;
        readonly CW_EntitiesAzure db;

        public PayInPaymentRatesRepository(CW_EntitiesAzure db)
        {
            this.db = db;
            this.payInRepo = new IRepositoryEF<payinsPaymentRate>(db);

        }

        public List<PaymentRatesForPayinsEx> GetByCountryID(int id)
        {
            var d = db.payinsPaymentRates.Where(x => x.id == id);
            return Transform(d);
        }

        public List<PaymentRatesForPayinsEx> GetByCountryName(string name)
        {
            var d = db.payinsPaymentRates.Where(x => x.country.Equals(name));
            return Transform(d);
        }

        public List<PaymentRatesForPayinsEx> GetByMethod(string method)
        {
            var d = db.payinsPaymentRates.Where(x => x.method.Equals(method));
            return Transform(d);
        }

        public List<PaymentRatesForPayinsEx> GetByLocalPaymentService(string localPaymentService)
        {
            var d = db.payinsPaymentRates.Where(x => x.localPaymentService.Equals(localPaymentService));
            return Transform(d);
        }

        //public int Count(int id, string TID, string where)
        //{
        //    List<string> aWhere = new List<string>();
        //    string sWhere = "";
        //    if (!string.IsNullOrEmpty(where))
        //    {
        //        aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
        //    }
        //    if (aWhere.Count > 0)
        //    {
        //        sWhere = string.Join(" && ", aWhere);
        //    }

        //    return "" != sWhere ? db.serviceFees.Where(sWhere).Where(c => c.id == id).Count() : db.serviceFees.Where(c => c.id == id).Count();
        //}

        public PaymentRatesForPayinsEx Create()
        {
            return new PaymentRatesForPayinsEx();
        }

        public PaymentRatesForPayinsEx GetByID(int Id)
        {
            var n = db.payinsPaymentRates.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }
        

        private PaymentRatesForPayinsEx Transform(payinsPaymentRate n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new PaymentRatesForPayinsEx();
            l.id = n.id;
            l.country = n.country;
            l.currency = n.currency;
            l.localPaymentService = n.localPaymentService;
            l.mdr = n.mdr;
            l.method = n.method;
            l.remarks = n.remarks;
            l.txFee = n.txFee;
            l.txFeeCurrency = n.txFeeCurrency;
            return l;
        }

        private payinsPaymentRate Transform(PaymentRatesForPayinsEx n)
        {
            payinsPaymentRate l;

            if (n.id > 0)
            {
                l = payInRepo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = payInRepo.Create();
            }
            l.id = n.id;
            l.country = n.country;
            l.currency = n.currency;
            l.localPaymentService = n.localPaymentService;
            l.mdr = n.mdr;
            l.method = n.method;
            l.remarks = n.remarks;
            l.txFee = n.txFee;
            l.txFeeCurrency = n.txFeeCurrency;

            return l;
        }

       
        public void Remove(PaymentRatesForPayinsEx item)
        {
            var g = Transform(item);
            payInRepo.Remove(g);
            payInRepo.SaveChanges();
        }

        public void Save(PaymentRatesForPayinsEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                payInRepo.Add(g);
            }
            else
            {
                payInRepo.Update(g);
            }
            payInRepo.SaveChanges();
            item.id = g.id;
            item.country = g.country;
            item.currency = g.currency;
            item.localPaymentService = g.localPaymentService;
            item.mdr = g.mdr;
            item.method = g.method;
            item.remarks = g.remarks;
            item.txFee = g.txFee;
            item.txFeeCurrency = g.txFeeCurrency;
        }
        

        private List<PaymentRatesForPayinsEx> Transform(IQueryable<payinsPaymentRate> aTransaction)
        {
            List<PaymentRatesForPayinsEx> l = new List<PaymentRatesForPayinsEx>();
            foreach (payinsPaymentRate b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
