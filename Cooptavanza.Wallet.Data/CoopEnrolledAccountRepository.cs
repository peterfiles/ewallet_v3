﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Data;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class CoopEnrolledAccountRepository : ICoopEnrolledAccountRepository
    {
        readonly IRepositoryEF<coopenrolledAccount> repo;
        readonly CW_EntitiesAzure db;

        public CoopEnrolledAccountRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<coopenrolledAccount>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.coopenrolledAccounts.Where(sWhere).Count() : db.coopenrolledAccounts.Count();
        }

        public CoopEnrolledAccountEx Create()
        {
            return new CoopEnrolledAccountEx();
        }

        public CoopEnrolledAccountEx Get(int Id)
        {
            var n = db.coopenrolledAccounts.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            else
            {
                return null;
            }
        }

    
        private CoopEnrolledAccountEx Transform(coopenrolledAccount n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CoopEnrolledAccountEx();
            l.id = n.id;
            l.currency = n.currency;
            l.currenycid = n.currenycid;
            l.accountDefault = n.accountDefault;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.accountnumber = n.accountnumber;
            l.roleID = n.roleID;
            return l;
        }

        private coopenrolledAccount Transform(CoopEnrolledAccountEx n)
        {
            coopenrolledAccount l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.currency = n.currency;
            l.currenycid = n.currenycid;
            l.accountDefault = n.accountDefault;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.accountnumber = n.accountnumber == null ? "0" : n.accountnumber;
            l.roleID = n.roleID;
            return l;
        }

        public List<CoopEnrolledAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.coopenrolledAccounts.Where(sWhere).OrderBy(orderby) : db.coopenrolledAccounts.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(CoopEnrolledAccountEx item)
        {
            var g = Transform(item);
            //repo.Remove(g);
            repo.SaveChanges();
        }

        public void Remove(int id)
        {
            CoopEnrolledAccountEx item = Get(id);
            var g = Transform(item);
            //repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CoopEnrolledAccountEx l)
        {
            var n= Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();
            //if (tempid == 0) {
            //    CoopEnrolledAccountEx x = Get(n.id);
            //    x.accountnumber = new SPConnection(db).SP_getAccountNumberEnrolledForCoopRed(x.userId, x.currenycid, Convert.ToInt32(x.roleID));
            //    Save(x);
            //}
            l.id = n.id;
            l.currency = n.currency;
            l.currenycid = n.currenycid;
            l.accountDefault = n.accountDefault;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.roleID = n.roleID;
            l.accountnumber = n.accountnumber;

        }
        
      

        private List<CoopEnrolledAccountEx> Transform(IQueryable<coopenrolledAccount> aEnrolledAccount)
        {
            List<CoopEnrolledAccountEx> l = new List<CoopEnrolledAccountEx>();
            foreach (coopenrolledAccount b in aEnrolledAccount)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public string getAccountNumberForCoopRed(int userID, int currID, int roleID, int coopEnrolledID)
        {
            return new SPConnection(db).SP_getAccountNumberEnrolledForCoopRed(userID,currID,roleID,coopEnrolledID);
        }

        public List<AccountListEx> getCoopAccountList(int userID, int roleID)
        {
            return new SPConnection(db).sp_getCoopAccountList(userID, roleID);
        }

        public CoopEnrolledAccountEx getIdByUserIDRoleIDCurrency(int userID, int roleID, string iso)
        {
            return new SPConnection(db).sp_getCoopEnrolledAccount(userID, roleID, iso);
        }

    }
}
