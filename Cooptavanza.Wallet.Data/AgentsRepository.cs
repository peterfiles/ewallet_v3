﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class AgentsRepository : IAgentRepository
    {
        readonly IRepositoryEF<agent> repo;
        readonly CW_EntitiesAzure db;

        public AgentsRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<agent>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.agents.Where(sWhere).Count() : db.agents.Count();
        }

        public AgentsEx Create()
        {
            return new AgentsEx();
        }

        public AgentsEx Get(int Id)
        {
            var n = db.agents.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private AgentsEx Transform(agent n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new AgentsEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.email = n.email;
          
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.url = n.url;
            l.stateid = n.stateid;
            l.state = n.stateid != null ? n.state1.name : "";
            l.zipcode = n.zipcode;
            l.countryid = n.countryid;
            l.country = n.countryid != null ? n.country1.name : "";

            l.username = n.username;
            l.city = n.city;
            l.tid = n.tid;

            l.isInOFACList = n.isInOFACList;
            l.isVerified = n.isVerified;

            l.partnerID = n.partnerID;
            l.name = n.name;
            return l;
        }

        private agent Transform(AgentsEx n)
        {
            agent l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.email = n.email;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.url = n.url;
            l.state = n.state;
            l.zipcode = n.zipcode;
            l.stateid = n.stateid;
            l.username = n.username;
            l.city = n.city;
            l.cityid = n.cityid;
            l.tid = n.tid;
            l.isInOFACList = n.isInOFACList;
            l.isVerified = n.isVerified;
            l.partnerID = n.partnerID;
            l.name = n.name;
            return l;
        }

        public List<AgentsEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.agents.Where(sWhere).OrderBy(orderby) : db.agents.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(AgentsEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(AgentsEx item)
        {
            var g = Transform(item);
            var h = new UserAccountRepository(db).CheckUsername(g.username);
            Boolean cancel = false;
            if (g.id == 0)
            {
               
                if (h == null)
                {
                    cancel = false;
                    repo.Add(g);
                }
                else { cancel = true; }
                
            }
            else
            {
                if (h != null)
                {
                    if (h.id == g.id) {
                        cancel = false;
                    }
                    else { cancel = true; }


                }
                else
                {
                    cancel = false;
                }
                if(cancel == false) { repo.Update(g); } else
                {
                    repo.Update(g);
                    repo.SaveChanges();
                }
                
            }
            if(cancel == false) {
                repo.SaveChanges();

                item.id = g.id;
                item.accountnumber = g.accountnumber;
                item.firstname = g.firstname;
                item.lastname = g.lastname;
                item.currency = g.currency;
                item.currencyid = g.currencyid;
                item.email = g.email;
                item.country = g.country;
                item.mobilenumber = g.mobilenumber;
                item.phonenumber = g.phonenumber;
                item.address = g.address;
                item.url = g.url;
                item.state = g.state;
                item.zipcode = g.zipcode;

                item.username = g.username;
                item.city = g.city;
                item.tid = g.tid;
                item.cityid = g.cityid;

                item.isInOFACList = g.isInOFACList;
                item.isVerified = g.isVerified;
                item.partnerID = g.partnerID;
                item.name = g.name;
            }
           
            

        }
        
        private List<AgentsEx> Transform(IQueryable<agent> aPerson)
        {
            List<AgentsEx> l = new List<AgentsEx>();
            foreach (agent b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<AgentsEx> getAgentByCountryIDAndStateID(int cID, int sID)
        {
            var r = db.agents.Where(x => x.countryid == cID && x.stateid == sID);
            return Transform(r);
        }

        public int CountAgents()
        {
            return db.agents.Count();
        }
    }
}
