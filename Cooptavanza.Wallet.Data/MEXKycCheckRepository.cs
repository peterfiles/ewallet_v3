﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXKycCheckRepository : IMEXKycCheckRepository
    {
        readonly IRepositoryEF<MEX_kycCheck> repo;
        readonly CW_EntitiesAzure db;

        public MEXKycCheckRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_kycCheck>(db);

        }

      
        public MEXKycCheckEx Get(int Id)
        {
            var n = db.MEX_kycCheck.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXKycCheckEx Transform(MEX_kycCheck n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXKycCheckEx();
            l.id = n.id;
            l.status = n.status;

            return l;
        }

        private MEX_kycCheck Transform(MEXKycCheckEx n)
        {
            MEX_kycCheck l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.status = n.status;
            return l;
        }

        public List<MEXKycCheckEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(status.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_kycCheck.Where(sWhere).OrderBy(orderby) : db.MEX_kycCheck.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

     
        
        private List<MEXKycCheckEx> Transform(IQueryable<MEX_kycCheck> o)
        {
            List<MEXKycCheckEx> l = new List<MEXKycCheckEx>();
            foreach (MEX_kycCheck b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }

      
    }
}
