﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class CreditCardLogsRepository : ICreditCardLogsRepository
    {
        readonly IRepositoryEF<CreditCardLog> repo;
        readonly CW_EntitiesAzure db;

        public CreditCardLogsRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<CreditCardLog>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.CreditCardLogs.Where(sWhere).Count() : db.CreditCardLogs.Count();
        }

        public CreditCardLogsEx Create()
        {
            return new CreditCardLogsEx();
        }

        public CreditCardLogsEx Get(int Id)
        {
            var n = db.CreditCardLogs.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private CreditCardLogsEx Transform(CreditCardLog n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CreditCardLogsEx();
            l.id = n.id;
            l.transactionNo = n.transactionNo;
            l.logs = n.logs;
            l.dateLogged = n.dateLogged;
            l.status = n.status;

            return l;
        }

        private CreditCardLog Transform(CreditCardLogsEx n)
        {
            CreditCardLog l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.transactionNo = n.transactionNo;
            l.logs = n.logs;
            l.dateLogged = n.dateLogged;
            l.status = n.status;

            return l;
        }

        public List<CreditCardLogsEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.CreditCardLogs.Where(sWhere).OrderBy(orderby) : db.CreditCardLogs.OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
           {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }
        public List<CreditCardLogsEx> GetByDefault(bool defaultValue)
        {
            string where = "";
            string orderby = "name Asc";
            if (defaultValue)
            {
                where = "1";
            }
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(default.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.CreditCardLogs.Where(sWhere).OrderBy(orderby) : db.CreditCardLogs.OrderBy(orderby);
          
                return Transform(r);
           
        }

        public void Remove(CreditCardLogsEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CreditCardLogsEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.transactionNo = g.transactionNo;
            item.logs = g.logs;
            item.dateLogged = g.dateLogged;
            item.status = g.status;
        }
        
        private List<CreditCardLogsEx> Transform(IQueryable<CreditCardLog> aPerson)
        {
            List<CreditCardLogsEx> l = new List<CreditCardLogsEx>();
            foreach (CreditCardLog b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }
       
    }
}
