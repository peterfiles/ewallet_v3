﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class ProductsRepository : IProductsRepository
    {
        readonly IRepositoryEF<product> repo;
        readonly CW_EntitiesAzure db;

        public ProductsRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<product>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.products.Where(sWhere).Count() : db.products.Count();
        }

        public ProductsEx Create()
        {
            return new ProductsEx();
        }

        public ProductsEx Get(int Id)
        {
            var n = db.products.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

      
        private ProductsEx Transform(product n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ProductsEx();
            l.id = n.id;
            l.name = n.name;
            l.code = n.code;
            l.tokenid = n.tokenid;
            l.Description = n.Description;
            serviceFee sfx = db.serviceFees.Where(x => x.productID == n.id).FirstOrDefault();
            if (sfx != null)
            {
                l.amount = sfx.serviceFeeAmount == null ? 0 : sfx.serviceFeeAmount;
                l.percentage = sfx.serviceFeePercentage == null ? 0 : sfx.serviceFeePercentage;
            }
            else
            {
                l.amount = 0;
                l.percentage = 0;
            }
            return l;
        }

        private product Transform(ProductsEx n)
        {
           product l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.name = n.name;
            l.code = n.code;
            l.tokenid = n.tokenid;
            l.Description = n.Description;
            
            return l;
        }

        public List<ProductsEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.products.Where(sWhere).OrderBy(orderby) : db.products.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ProductsEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ProductsEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.name = g.name;
            item.code = g.code;
            item.tokenid = g.tokenid;
            item.Description = item.Description;
        }


      

        private List<ProductsEx> Transform(IQueryable<product> aProducts)
        {
            List<ProductsEx> l = new List<ProductsEx>();
            foreach (product b in aProducts)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
