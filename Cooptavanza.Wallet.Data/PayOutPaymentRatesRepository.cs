﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class PayOutPaymentRatesRepository : IPayOutPaymentRatesRepository
    {
        readonly IRepositoryEF<payoutsPaymentRate> payOutRepo;
        readonly CW_EntitiesAzure db;

        public PayOutPaymentRatesRepository(CW_EntitiesAzure db)
        {
            this.db = db;
            this.payOutRepo = new IRepositoryEF<payoutsPaymentRate>(db);

        }

        public List<PaymentRatesForPayoutsEx> GetByCountryID(int id)
        {
            var d = db.payoutsPaymentRates.Where(x => x.id == id);
            return Transform(d);
        }

        public List<PaymentRatesForPayoutsEx> GetByCountryName(string name)
        {
            var d = db.payoutsPaymentRates.Where(x => x.country.Equals(name));
            return Transform(d);
        }

        //public int Count(int id, string TID, string where)
        //{
        //    List<string> aWhere = new List<string>();
        //    string sWhere = "";
        //    if (!string.IsNullOrEmpty(where))
        //    {
        //        aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
        //    }
        //    if (aWhere.Count > 0)
        //    {
        //        sWhere = string.Join(" && ", aWhere);
        //    }

        //    return "" != sWhere ? db.serviceFees.Where(sWhere).Where(c => c.id == id).Count() : db.serviceFees.Where(c => c.id == id).Count();
        //}

        public PaymentRatesForPayoutsEx Create()
        {
            return new PaymentRatesForPayoutsEx();
        }

        public PaymentRatesForPayoutsEx GetByID(int Id)
        {
            var n = db.payoutsPaymentRates.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }
        

        private PaymentRatesForPayoutsEx Transform(payoutsPaymentRate n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new PaymentRatesForPayoutsEx();
            l.id = n.id;
            l.country = n.country;
            l.currency = n.currency;
            l.localPaymentService = n.localPaymentService;
            l.mdr = n.mdr;
            l.method = n.method;
            l.remarks = n.remarks;
            l.txFee = n.txFee;
            return l;
        }

        private payoutsPaymentRate Transform(PaymentRatesForPayoutsEx n)
        {
            payoutsPaymentRate l;

            if (n.id > 0)
            {
                l = payOutRepo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = payOutRepo.Create();
            }
            l.id = n.id;
            l.country = n.country;
            l.currency = n.currency;
            l.localPaymentService = n.localPaymentService;
            l.mdr = n.mdr;
            l.method = n.method;
            l.remarks = n.remarks;
            l.txFee = n.txFee;

            return l;
        }

       
        public void Remove(PaymentRatesForPayoutsEx item)
        {
            var g = Transform(item);
            payOutRepo.Remove(g);
            payOutRepo.SaveChanges();
        }

        public void Save(PaymentRatesForPayoutsEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                payOutRepo.Add(g);
            }
            else
            {
                payOutRepo.Update(g);
            }
            payOutRepo.SaveChanges();
            item.id = g.id;
            item.country = g.country;
            item.currency = g.currency;
            item.localPaymentService = g.localPaymentService;
            item.mdr = g.mdr;
            item.method = g.method;
            item.remarks = g.remarks;
            item.txFee = g.txFee;
        }
        

        private List<PaymentRatesForPayoutsEx> Transform(IQueryable<payoutsPaymentRate> aTransaction)
        {
            List<PaymentRatesForPayoutsEx> l = new List<PaymentRatesForPayoutsEx>();
            foreach (payoutsPaymentRate b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
