﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXAmlStatusRepository : IMEXAmlStatusRepository
    {
        readonly IRepositoryEF<MEX_amlStatus> repo;
        readonly CW_EntitiesAzure db;

        public MEXAmlStatusRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_amlStatus>(db);

        }

      
        public MEXAmlStatusEx Get(int Id)
        {
            var n = db.MEX_amlStatus.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXAmlStatusEx Transform(MEX_amlStatus n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXAmlStatusEx();
            l.id = n.id;
            l.status = n.status;

            return l;
        }

        private MEX_amlStatus Transform(MEXAmlStatusEx n)
        {
            MEX_amlStatus l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.status = n.status;
            return l;
        }

        public List<MEXAmlStatusEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(status.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_amlStatus.Where(sWhere).OrderBy(orderby) : db.MEX_amlStatus.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

     
        
        private List<MEXAmlStatusEx> Transform(IQueryable<MEX_amlStatus> o)
        {
            List<MEXAmlStatusEx> l = new List<MEXAmlStatusEx>();
            foreach (MEX_amlStatus b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }

      
    }
}
