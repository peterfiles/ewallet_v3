﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class ReceiversCustomerRepository : IReceiversCustomerRepository
    {
        readonly IRepositoryEF<receiversCustomer> repo;
        readonly CW_EntitiesAzure db;

        public ReceiversCustomerRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<receiversCustomer>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.receiversCustomers.Where(sWhere).Count() : db.receiversCustomers.Count();
        }

        public ReceiversCustomerEx Create()
        {
            return new ReceiversCustomerEx();
        }

        public ReceiversCustomerEx GetLogin(string mobilenumber, string password) {
            return null;
           
      //      return new SPConnection(db).SP_authenticate(mobilenumber, password); 
        }

        public ReceiversCustomerEx Get(int Id)
        {
            var n = db.receiversCustomers.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private ReceiversCustomerEx Transform(receiversCustomer n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ReceiversCustomerEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.expiryDate = n.expiryDate;
            l.email = n.email;
            l.countryid = n.countryid;
     //       l.country = n.country.commonname;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.username = n.username;
            l.stateid = n.stateid;
        //    l.state = n.state.name;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.cityid = n.cityid;
            l.profileimageId = n.profileimageId;
            l.recentbillimageId = n.recentbillimageId;
            l.tid = n.tid;
            l.isVerified = n.isVerified;
            l.isInOFACList = n.isInOFACList;
            l.theme = n.theme;
          
            return l;
        }

        private receiversCustomer Transform(ReceiversCustomerEx n)
        {
            receiversCustomer l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
           l.expiryDate = n.expiryDate;
            l.email = n.email;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.username = n.username;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.cityid = n.cityid;
            l.profileimageId = n.profileimageId;
            l.recentbillimageId = n.recentbillimageId;
            l.tid = n.tid;
            l.isVerified = n.isVerified;
            l.isInOFACList = n.isInOFACList;
            l.theme = n.theme;
            return l;
        }

        public List<ReceiversCustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.receiversCustomers.Where(sWhere).OrderBy(orderby) : db.receiversCustomers.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ReceiversCustomerEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ReceiversCustomerEx item)
        {
            int tempid = item.id;
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            if(tempid == 0)
            {
                ReceiversCustomerEx x = Get(g.id);
                x.accountnumber = new SPConnection(db).SP_getAccountNumber(7, g.id);
                Save(x);
            }

            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.middlename = g.middlename;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.expiryDate = g.expiryDate;
            item.email = g.email;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.username = g.username;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.profileimageId = g.profileimageId;
            item.recentbillimageId = g.recentbillimageId;
            item.cityid = g.cityid;
            item.isVerified = g.isVerified;
            item.isInOFACList = g.isInOFACList;
            item.theme = g.theme;
        }

        public ReceiversCustomerEx SaveCustomer(ReceiversCustomerEx item)
        {
            int tempid = item.id;
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            //if (tempid == 0)
            //{
            //    ReceiversCustomerEx x = Get(g.id);
            //    x.accountnumber = new SPConnection(db).SP_getAccountNumber(7, g.id);
            //    Save(x);
            //}
            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.middlename = g.middlename;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.expiryDate = g.expiryDate;
            item.email = g.email;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.username = g.username;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.profileimageId = g.profileimageId;
            item.recentbillimageId = g.recentbillimageId;
            item.cityid = g.cityid;
            item.isVerified = g.isVerified;
            item.isInOFACList = g.isInOFACList;
            item.theme = g.theme;
            return item;
        }

        private List<ReceiversCustomerEx> Transform(IQueryable<receiversCustomer> aPerson)
        {
            List<ReceiversCustomerEx> l = new List<ReceiversCustomerEx>();
            foreach (receiversCustomer b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public ReceiversCustomerEx GetCheckEmail(string str)
        {
            var data = db.receiversCustomers.Where(x => x.email == str).FirstOrDefault();

            return Transform(data);
        }

        public ReceiversCustomerEx GetByMobileNumber(string mobileNumber)
        {
            var data = db.receiversCustomers.Where(x => x.mobilenumber == mobileNumber).FirstOrDefault();
            return Transform(data);
        }

        public ReceiversCustomerEx GetCheckMobileEmail(string str)
        {
            var data = db.receiversCustomers.Where(x => x.mobilenumber == str || x.email == str).FirstOrDefault();

            return Transform(data);
        }
    }
}
