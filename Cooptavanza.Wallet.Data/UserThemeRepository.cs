﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class UserThemeRepository : IUserThemeRepository
    {
        readonly IRepositoryEF<userTheme> repo;
        readonly CW_EntitiesAzure db;

        public UserThemeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<userTheme>(db);

        }

      

        public UserThemeEx Create()
        {
            return new UserThemeEx();
        }

        public UserThemeEx Get(int Id)
        {
            var n = db.userThemes.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }
        public UserThemeEx GetByUserId(int Id)
        {
            var n = db.userThemes.Where(c => c.userid == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private UserThemeEx Transform(userTheme n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new UserThemeEx();
            l.id = n.id;
            l.roles = n.roles;
            l.theme = n.theme;
            l.userid= n.userid;
            return l;
        }

        private userTheme Transform(UserThemeEx n)
        {
            userTheme l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.roles = n.roles;
            l.theme = n.theme;
            l.userid = n.userid;

            return l;
        }

      

      
        public void Remove(UserThemeEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(UserThemeEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
         
        }


      
        
    }
}
