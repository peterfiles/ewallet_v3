﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using Cooptavanza.Wallet.Repository;

namespace Cooptavanza.Wallet.Data
{
    public class OFACListRepository : IOFAClistRepository
    {
        readonly IRepositoryEF<OFAClist> repo;
        readonly CW_EntitiesAzure db;

        public OFACListRepository(CW_EntitiesAzure db)
        {
            this.db = db;
            this.repo = new IRepositoryEF<OFAClist>(db);

        }
        
        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(ofac_sdn_name.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.OFAClists.Where(sWhere).Count() : db.OFAClists.Count();
        }

        public OFAClistEx Create()
        {
            return new OFAClistEx();
        }

        public OFAClistEx Get(int sdnId)
        {
            var n = db.OFAClists.Where(c => c.ofac_sdn_ent_num ==sdnId).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private OFAClistEx Transform(OFAClist n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new OFAClistEx();
            l.id = n.id;
            l.ofac_sdn_ent_num = n.ofac_sdn_ent_num;
            l.ofac_sdn_name = n.ofac_sdn_name;
            l.ofac_sdn_type = n.ofac_sdn_type;
            l.ofac_sdn_program = n.ofac_sdn_program;
            l.ofac_sdn_title = n.ofac_sdn_title;
            l.ofac_sdn_call_sign = n.ofac_sdn_call_sign;
            l.ofac_sdn_vess_type = n.ofac_sdn_vess_type;
            l.ofac_sdn_tonnage = n.ofac_sdn_tonnage;
            l.ofac_sdn_grt = n.ofac_sdn_grt;
            l.ofac_sdn_vess_flag = n.ofac_sdn_vess_flag;
            l.ofac_sdn_vess_owner = n.ofac_sdn_vess_owner;
            l.ofac_sdn_remarks = n.ofac_sdn_remarks;
            l.userID = n.userID;
            l.userCode = n.userCode;
            l.userIsVerified = n.userIsVerified;
            l.isCompany = n.isCompany;
            return l;
        }

        private OFAClist Transform(OFAClistEx n)
        {
            OFAClist l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.ofac_sdn_ent_num = n.ofac_sdn_ent_num;
            l.ofac_sdn_name = n.ofac_sdn_name;
            l.ofac_sdn_type = n.ofac_sdn_type;
            l.ofac_sdn_program = n.ofac_sdn_program;
            l.ofac_sdn_title = n.ofac_sdn_title;
            l.ofac_sdn_call_sign = n.ofac_sdn_call_sign;
            l.ofac_sdn_vess_type = n.ofac_sdn_vess_type;
            l.ofac_sdn_tonnage = n.ofac_sdn_tonnage;
            l.ofac_sdn_grt = n.ofac_sdn_grt;
            l.ofac_sdn_vess_flag = n.ofac_sdn_vess_flag;
            l.ofac_sdn_vess_owner = n.ofac_sdn_vess_owner;
            l.ofac_sdn_remarks = n.ofac_sdn_remarks;
            l.userID = n.userID;
            l.userCode = n.userCode;
            l.userIsVerified = n.userIsVerified;
            l.isCompany = n.isCompany;
            return l;
        }

        public List<OFAClistEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.OFAClists.Where(sWhere).OrderBy(orderby) : db.OFAClists.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(OFAClistEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(OFAClistEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                if (!(db.OFAClists.Where(d => d.id == g.id).ToList().Count > 0))
                {
                    repo.Add(g);
                }
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.ofac_sdn_ent_num = g.ofac_sdn_ent_num;
            item.ofac_sdn_name = g.ofac_sdn_name;
            item.ofac_sdn_type = g.ofac_sdn_type;
            item.ofac_sdn_program = g.ofac_sdn_program;
            item.ofac_sdn_title = g.ofac_sdn_title;
            item.ofac_sdn_call_sign = g.ofac_sdn_call_sign;
            item.ofac_sdn_vess_type = g.ofac_sdn_vess_type;
            item.ofac_sdn_tonnage = g.ofac_sdn_tonnage;
            item.ofac_sdn_grt = g.ofac_sdn_grt;
            item.ofac_sdn_vess_flag = g.ofac_sdn_vess_flag;
            item.ofac_sdn_vess_owner = g.ofac_sdn_vess_owner;
            item.ofac_sdn_remarks = g.ofac_sdn_remarks;
            item.userID = g.userID;
            item.userCode = g.userCode;
            item.userIsVerified = g.userIsVerified;
            item.isCompany = g.isCompany;
        }

        private List<OFAClistEx> Transform(IQueryable<OFAClist> list)
        {
            List<OFAClistEx> l = new List<OFAClistEx>();
            foreach (OFAClist b in list)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
