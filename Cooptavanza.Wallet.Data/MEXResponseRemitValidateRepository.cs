﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXResponseRemitValidateRepository : IMEXResponseRemitValidateRepository
    {
        readonly IRepositoryEF<MEX_ResponseRemitValidate> repo;
        readonly CW_EntitiesAzure db;

        public MEXResponseRemitValidateRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_ResponseRemitValidate>(db);

        }

      
        public MEXResponseRemitValidateEx Get(int Id)
        {
            var n = db.MEX_ResponseRemitValidate.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXResponseRemitValidateEx Transform(MEX_ResponseRemitValidate n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXResponseRemitValidateEx();
            l.id = n.id;
            l.ActionCode = n.ActionCode;
            l.ConversionRate = n.ConversionRate;
            l.FeeCurrency = n.FeeCurrency;
            l.FloatAmount = n.FloatAmount;
            l.FloatCurrency = n.FloatCurrency;
            l.KioskFeeAmount = n.KioskFeeAmount;
            l.LastUpdateTime = n.LastUpdateTime;
            l.Note = n.Note;
            l.OriginatorUID = n.OriginatorUID;
            l.PaidFeeAmount = n.PaidFeeAmount;
            l.QuoteAmt = n.QuoteAmt;
            l.QuoteCurr = n.QuoteCurr;
            l.RcvAccntBankCode = n.RcvAccntBankCode;
            l.RcvAccntBankName = n.RcvAccntBankName;
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;
            l.RespCode = n.RespCode;
            l.RespMessage = n.RespMessage;
            l.SentAmount = n.SentAmount;
            l.SentCurrency = n.SentCurrency;
            l.Signature = n.Signature;
            l.SndKycUrl = n.SndKycUrl;
            l.SubmitTime = n.SubmitTime;
            l.TotalFeeAmount = n.TotalFeeAmount;
            l.TransactionId = n.TransactionId;
            l.MEXType = n.MEXType;

            return l;
        }

        private MEX_ResponseRemitValidate Transform(MEXResponseRemitValidateEx n)
        {
            MEX_ResponseRemitValidate l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.ActionCode = n.ActionCode;
            l.ConversionRate = n.ConversionRate;
            l.FeeCurrency = n.FeeCurrency;
            l.FloatAmount = n.FloatAmount;
            l.FloatCurrency = n.FloatCurrency;
            l.KioskFeeAmount = n.KioskFeeAmount;
            l.LastUpdateTime = n.LastUpdateTime;
            l.Note = n.Note;
            l.OriginatorUID = n.OriginatorUID;
            l.PaidFeeAmount = n.PaidFeeAmount;
            l.QuoteAmt = n.QuoteAmt;
            l.QuoteCurr = n.QuoteCurr;
            l.RcvAccntBankCode = n.RcvAccntBankCode;
            l.RcvAccntBankName = n.RcvAccntBankName;
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;
            l.RespCode = n.RespCode;
            l.RespMessage = n.RespMessage;
            l.SentAmount = n.SentAmount;
            l.SentCurrency = n.SentCurrency;
            l.Signature = n.Signature;
            l.SndKycUrl = n.SndKycUrl;
            l.SubmitTime = n.SubmitTime;
            l.TotalFeeAmount = n.TotalFeeAmount;
            l.TransactionId = n.TransactionId;
            l.MEXType = n.MEXType;
            return l;
        }

        public List<MEXResponseRemitValidateEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(RcvFirstName.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_ResponseRemitValidate.Where(sWhere).OrderBy(orderby) : db.MEX_ResponseRemitValidate.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(RcvFirstName.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.MEX_ResponseRemitValidate.Where(sWhere).Count() : db.MEX_ResponseRemitValidate.Count();
        }



        private List<MEXResponseRemitValidateEx> Transform(IQueryable<MEX_ResponseRemitValidate> o)
        {
            List<MEXResponseRemitValidateEx> l = new List<MEXResponseRemitValidateEx>();
            foreach (MEX_ResponseRemitValidate b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }


        public void Remove(MEXResponseRemitValidateEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(MEXResponseRemitValidateEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();
            l.id = n.id;
            l.ActionCode = n.ActionCode;
            l.ConversionRate = n.ConversionRate;
            l.FeeCurrency = n.FeeCurrency;
            l.FloatAmount = n.FloatAmount;
            l.FloatCurrency = n.FloatCurrency;
            l.KioskFeeAmount = n.KioskFeeAmount;
            l.LastUpdateTime = n.LastUpdateTime;
            l.Note = n.Note;
            l.OriginatorUID = n.OriginatorUID;
            l.PaidFeeAmount = n.PaidFeeAmount;
            l.QuoteAmt = n.QuoteAmt;
            l.QuoteCurr = n.QuoteCurr;
            l.RcvAccntBankCode = n.RcvAccntBankCode;
            l.RcvAccntBankName = n.RcvAccntBankCode;
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;
            l.RespCode = n.RespCode;
            l.RespMessage = n.RespMessage;
            l.SentAmount = n.SentAmount;
            l.SentCurrency = n.SentCurrency;
            l.Signature = n.Signature;
            l.SndKycUrl = n.SndKycUrl;
            l.SubmitTime = n.SubmitTime;
            l.TotalFeeAmount = n.TotalFeeAmount;
            l.TransactionId = n.TransactionId;
            l.MEXType = n.MEXType;

        }

        public MEXResponseRemitValidateEx Create()
        {
           
           return new MEXResponseRemitValidateEx();
           
        }
    }
}
