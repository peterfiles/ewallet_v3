﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Data;

namespace Cooptavanza.Wallet.Data
{
    public class EnrolledAccountRepository : IEnrolledAccountRepository
    {
        readonly IRepositoryEF<enrolledAccount> repo;
        readonly CW_EntitiesAzure db;

        public EnrolledAccountRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<enrolledAccount>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.enrolledAccounts.Where(sWhere).Count() : db.enrolledAccounts.Count();
        }

        public EnrolledAccountEx Create()
        {
            return new EnrolledAccountEx();
        }

        public EnrolledAccountEx Get(int Id)
        {
            var n = db.enrolledAccounts.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public List<EnrolledAccountEx> GetByUserId(int Id)
        {
            var n = db.enrolledAccounts.Where(c => c.userId == Id);
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private EnrolledAccountEx Transform(enrolledAccount n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new EnrolledAccountEx();
            l.id = n.id;
            l.currency = n.currency;
            l.currenycid = n.currenycid;
            l.accountDefault = n.accountDefault;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.accountnumber = n.accountnumber;
            return l;
        }

        private enrolledAccount Transform(EnrolledAccountEx n)
        {
            enrolledAccount l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.currency = n.currency;
            l.currenycid = n.currenycid;
            l.accountDefault = n.accountDefault;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.accountnumber = n.accountnumber == null ? "0" : n.accountnumber;

            return l;
        }

        public List<EnrolledAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.enrolledAccounts.Where(sWhere).OrderBy(orderby) : db.enrolledAccounts.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(EnrolledAccountEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Remove(int id)
        {
            EnrolledAccountEx item = Get(id);
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }
        public static int returnID;
        public void Save(EnrolledAccountEx l)
        {
            var tempid = l.id;
            var n= Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
                
            }
            else
            {
                repo.Update(n);
            }

          
            repo.SaveChanges();
            if (tempid == 0) {

                EnrolledAccountEx x = Get(n.id);
                x.accountnumber = new SPConnection(db).SP_getAccountNumberEnrolled(x.userId, x.currenycid);
               

            Save(x);
                
            }
            l.id = n.id;
            l.currency = n.currency;
            l.currenycid = n.currenycid;
            l.accountDefault = n.accountDefault;
            l.userId = n.userId;
            l.userTID = n.userTID;
            returnID = l.id;
        }

        public int retID()
        {
            return returnID;
        }

      

        private List<EnrolledAccountEx> Transform(IQueryable<enrolledAccount> aEnrolledAccount)
        {
            List<EnrolledAccountEx> l = new List<EnrolledAccountEx>();
            foreach (enrolledAccount b in aEnrolledAccount)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<EnrolledAccountEx> GetByIdAndISO(int userID, string iso)
        {
            return Transform(db.enrolledAccounts.Where(x => x.userId == userID && x.currency == iso));
        }

        public EnrolledAccountEx GetByUserIDAndCurrencyID(int userID, string iso)
        {
            return Transform(db.enrolledAccounts.Where(x => x.userId == userID && x.currency == iso)).FirstOrDefault();
        }

        public EnrolledAccountEx GetByUserIDUserTIDAndCurrencyISO (int userID, string tID, string iso)
        {
            return Transform(db.enrolledAccounts.Where(x => x.userId == userID && x.userTID == tID && x.currency == iso)).FirstOrDefault();
        }
    }
}
