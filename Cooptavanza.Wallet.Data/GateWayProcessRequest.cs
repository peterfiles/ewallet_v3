﻿using Cooptavanza.Wallet.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cooptavanza.Wallet.Data
{
    public class GateWayProcessRequest
    {
        public static string processRequest(
            GateWayProcessPaymentRequestEx n,
            int process
            )
        {
            string proc = string.Empty;
            string format = string.Empty;
            switch (process)
            {
                case 1:
                    proc = "processPayment";
                    format = formatProcessForPaymentPreAuthCreditFraudCheckRequest(proc, n);
                    break;
                case 2:
                    proc = "processPreAuth";
                    format = formatProcessForPaymentPreAuthCreditFraudCheckRequest(proc, n);
                    break;
                case 3:
                    proc = "processCredit";
                    format = formatProcessForPaymentPreAuthCreditFraudCheckRequest(proc, n);
                    break;
                case 4:
                    proc = "processFraudCheck";
                    format = formatProcessForPaymentPreAuthCreditFraudCheckRequest(proc, n);
                    break;
                case 5:
                    proc = "processSettlement";
                    format = formatProcessForSettlement(proc, n);
                    break;
                case 6:
                    proc = "processRequery";
                    format = formatProcessForRequery(proc, n);
                    break;
                case 7:
                    proc = "processRefund";
                    format = formatProcessForRefund(proc, n);
                    break;
                case 8:
                    proc = "getTransactionsByRID";
                    format = formatGetTransactionByRID(proc, n);
                    break;
                case 9:
                    proc = "getTransactionsBySID";
                    format = formatGetTransactionBySID(proc, n);
                    break;
                case 10:
                    proc = "processRebill";
                    format = formatProcessRebill(proc, n);
                    break;
                case 11:
                    proc = "rebillRegister";
                    format = formatProcessRebillRegister(proc, n);
                    break;
                case 12:
                    proc = "rebillCancel";
                    format = formatRebillCancel(proc, n);
                    break;
                case 13:
                    proc = "rebillScheduleChange";
                    format = formatRebillScheduleChange(proc, n);
                    break;
                case 14:
                    proc = "rebillCardChange";
                    format = formatRebillCardChange(proc, n);
                    break;
                case 15:
                    proc = "verification";
                    format = formatVerification(proc, n);
                    break;
                case 16:
                    proc = "fraudcheck";
                    format = formatFraudCheck(proc, n);
                    break;
                case 17:
                    proc = "getFutureRebills";
                    format = formatGetFutureRebills(proc, n);
                    break;
                case 18:
                    proc = "processPayment";
                    format = formatProcessForPaymentPreAuthCreditFraudCheckRequest_EFT(proc, n);
                    break;
                case 19:
                    proc = "processPayment";
                    format = formatProcessForPaymentPreAuthCreditFraudCheckRequest_ACH(proc, n);
                    break;
                default: proc = string.Empty; break;
            }
            return format;
        }

        private static string formatProcessForPaymentPreAuthCreditFraudCheckRequest(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <udetails xsi:type=""ns1:udetails"">
                <firstname xsi:type=""xsd:string"">" + n.firstname + @"</firstname>
                <lastname xsi:type=""xsd:string"">" + n.lastname + @"</lastname>
                <email xsi:type=""xsd:string"">" + n.email + @"</email>
                <phone xsi:type=""xsd:string"">" + n.phone + @"</phone>
                <address xsi:type=""xsd:string"">" + n.address + @"</address>
                <suburb_city xsi:type=""xsd:string"">" + n.suburb_city + @"</suburb_city>
                <state xsi:type=""xsd:string"">" + n.state + @"</state>
                <postcode xsi:type=""xsd:string"">" + n.postcode + @"</postcode>
                <country xsi:type=""xsd:string"">" + n.country + @"</country>
                <uip xsi:type=""xsd:string"">" + n.uip + @"</uip>
            </udetails>
            <paydetails xsi:type=""ns1:paydetails"">
                <payby xsi:type=""xsd:string"">" + n.payby + @"</payby>
                <card_name xsi:type=""xsd:string"">" + n.card_name + @"</card_name>
                <card_no xsi:type=""xsd:string"">" + n.card_no + @"</card_no>
                <card_ccv xsi:type=""xsd:string"">" + n.card_ccv + @"</card_ccv>
                <card_exp_month xsi:type=""xsd:string"">" + n.card_exp_month + @"</card_exp_month>
                <card_exp_year xsi:type=""xsd:int"">" + n.card_exp_year + @"</card_exp_year>
            </paydetails>
            <cart xsi:type=""ns1:cart"">
                <summary xsi:type=""ns1:cartsummary"">
                    <quantity xsi:type=""xsd:int"">" + n.quantity + @"</quantity>
                    <amount_purchase xsi:type=""xsd:decimal"">" + n.amount_purchase + @"</amount_purchase>
                    <amount_shipping xsi:type=""xsd:decimal"">" + n.amount_shipping + @"</amount_shipping>
                    <amount_tax xsi:type=""xsd:decimal"">" + n.amount_tax + @"</amount_tax>
                    <currency_code xsi:type=""xsd:string"">" + n.currency_code + @"</currency_code>
                </summary>
                <items SOAP-ENC:arrayType=""ns1:cartitem[1]"" xsi:type=""ns1:cartitems"">
                    <item xsi:type=""ns1:cartitem"">
                        <name xsi:type=""xsd:string"">" + n.itemName + @"</name>
                        <quantity xsi:type=""xsd:int"">" + n.itemQuantity + @"</quantity>
                        <amount_unit xsi:type=""xsd:decimal"">" + n.amount_unit + @"</amount_unit>
                        <item_no xsi:type=""xsd:string"">" + n.item_no + @"</item_no>
                        <item_desc xsi:type=""xsd:string"">" + n.item_desc + @"</item_desc>
                    </item>
                </items>
            </cart>
            <txparams xsi:type=""ns1:txparams""/>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessForPaymentPreAuthCreditFraudCheckRequest_EFT(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <udetails xsi:type=""ns1:udetails"">
                <firstname xsi:type=""xsd:string"">" + n.firstname + @"</firstname>
                <lastname xsi:type=""xsd:string"">" + n.lastname + @"</lastname>
                <email xsi:type=""xsd:string"">" + n.email + @"</email>
                <phone xsi:type=""xsd:string"">" + n.phone + @"</phone>
                <address xsi:type=""xsd:string"">" + n.address + @"</address>
                <suburb_city xsi:type=""xsd:string"">" + n.suburb_city + @"</suburb_city>
                <state xsi:type=""xsd:string"">" + n.state + @"</state>
                <postcode xsi:type=""xsd:string"">" + n.postcode + @"</postcode>
                <country xsi:type=""xsd:string"">" + n.country + @"</country>
                <uip xsi:type=""xsd:string"">" + n.uip + @"</uip>
            </udetails>
            <paydetails xsi:type=""ns1:paydetails"">
                <payby xsi:type=""xsd:string"">" + n.payby + @"</payby>
                <card_name xsi:type=""xsd:string"">" + n.card_name + @"</card_name>
                <card_no xsi:type=""xsd:string"">" + n.card_no + @"</card_no>
                <card_ccv xsi:type=""xsd:string"">" + n.card_ccv + @"</card_ccv>
                <card_exp_month xsi:type=""xsd:string"">" + n.card_exp_month + @"</card_exp_month>
                <card_exp_year xsi:type=""xsd:int"">" + n.card_exp_year + @"</card_exp_year>
            </paydetails>
            <cart xsi:type=""ns1:cart"">
                <summary xsi:type=""ns1:cartsummary"">
                    <quantity xsi:type=""xsd:int"">" + n.quantity + @"</quantity>
                    <amount_purchase xsi:type=""xsd:decimal"">" + n.amount_purchase + @"</amount_purchase>
                    <amount_shipping xsi:type=""xsd:decimal"">" + n.amount_shipping + @"</amount_shipping>
                    <amount_tax xsi:type=""xsd:decimal"">" + n.amount_tax + @"</amount_tax>
                    <currency_code xsi:type=""xsd:string"">" + n.currency_code + @"</currency_code>
                </summary>
                <items SOAP-ENC:arrayType=""ns1:cartitem[1]"" xsi:type=""ns1:cartitems"">
                    <item xsi:type=""ns1:cartitem"">
                        <name xsi:type=""xsd:string"">" + n.itemName + @"</name>
                        <quantity xsi:type=""xsd:int"">" + n.itemQuantity + @"</quantity>
                        <amount_unit xsi:type=""xsd:decimal"">" + n.amount_unit + @"</amount_unit>
                        <item_no xsi:type=""xsd:string"">" + n.item_no + @"</item_no>
                        <item_desc xsi:type=""xsd:string"">" + n.item_desc + @"</item_desc>
                    </item>
                </items>
            </cart>
            <txparams xsi:type=""ns1:txparams"">
                <wid xsi:type = ""xsd:string"">" + n.txParams_wid + @"</wid>
                <tid xsi:type = ""xsd:string"">" + n.txParams_tid + @"</tid>
                <ref1 xsi:type = ""xsd:string"">" + n.txParams_ref1 + @"</ref1>
                <ref2 xsi:type = ""xsd:string"">" + n.txParams_ref2 + @"</ref2>
                <ref3 xsi:type = ""xsd:string"">" + n.txParams_ref3 + @"</ref3>
                <ref4 xsi:type = ""xsd:string"">" + n.txParams_ref4 + @"</ref4>
                <addinfo xsi:type = ""xsd:string"">" + n.txParams_addinfo + @"</addinfo>
                <ipayinfo xsi:type = ""xsd:string"">" + n.txParams_ipayinfo + @"</ipayinfo>
                <cmd xsi:type = ""xsd:string"">" + n.txParams_cmd + @"</cmd>
                <postbackurl xsi:type = ""xsd:string"">" + n.txParams_postbackurl + @"</postbackurl>
                <merchant_rebilling xsi:type = ""xsd:string"">" + n.txParams_merchant_rebilling + @"</merchant_rebilling>
                <kyc_status xsi:type = ""xsd:string"">" + n.txParams_kyc_status + @"</kyc_status>
            </txparams>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessForPaymentPreAuthCreditFraudCheckRequest_ACH(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <udetails xsi:type=""ns1:udetails"">
                <firstname xsi:type=""xsd:string"">" + n.firstname + @"</firstname>
                <lastname xsi:type=""xsd:string"">" + n.lastname + @"</lastname>
                <email xsi:type=""xsd:string"">" + n.email + @"</email>
                <phone xsi:type=""xsd:string"">" + n.phone + @"</phone>
                <address xsi:type=""xsd:string"">" + n.address + @"</address>
                <suburb_city xsi:type=""xsd:string"">" + n.suburb_city + @"</suburb_city>
                <state xsi:type=""xsd:string"">" + n.state + @"</state>
                <postcode xsi:type=""xsd:string"">" + n.postcode + @"</postcode>
                <country xsi:type=""xsd:string"">" + n.country + @"</country>
                <uip xsi:type=""xsd:string"">" + n.uip + @"</uip>
            </udetails>
            <paydetails xsi:type=""ns1:paydetails"">
                <payby xsi:type=""xsd:string"">" + n.payby + @"</payby>
                <routing_no xsi:type=""xsd:string"">" + n.ach_routing_no + @"</routing_no>
                <account_no xsi:type=""xsd:string"">" + n.ach_account_no + @"</account_no>
                <ach_type xsi:type=""xsd:string"">" + n.ach_type + @"</ach_type>
                <regulation_e xsi:type=""xsd:string"">" + n.ach_regulation_e + @"</regulation_e>
                <class xsi:type=""xsd:string"">" + n.ach_class + @"</class>
                <receive_name xsi:type=""xsd:string"">" + n.ach_receive_name + @"</receive_name>
            </paydetails>
            <cart xsi:type=""ns1:cart"">
                <summary xsi:type=""ns1:cartsummary"">
                    <quantity xsi:type=""xsd:int"">" + n.quantity + @"</quantity>
                    <amount_purchase xsi:type=""xsd:decimal"">" + n.amount_purchase + @"</amount_purchase>
                    <amount_shipping xsi:type=""xsd:decimal"">" + n.amount_shipping + @"</amount_shipping>
                    <amount_tax xsi:type=""xsd:decimal"">" + n.amount_tax + @"</amount_tax>
                    <currency_code xsi:type=""xsd:string"">" + n.currency_code + @"</currency_code>
                </summary>
                <items SOAP-ENC:arrayType=""ns1:cartitem[1]"" xsi:type=""ns1:cartitems"">
                    <item xsi:type=""ns1:cartitem"">
                        <name xsi:type=""xsd:string"">" + n.itemName + @"</name>
                        <quantity xsi:type=""xsd:int"">" + n.itemQuantity + @"</quantity>
                        <amount_unit xsi:type=""xsd:decimal"">" + n.amount_unit + @"</amount_unit>
                        <item_no xsi:type=""xsd:string"">" + n.item_no + @"</item_no>
                        <item_desc xsi:type=""xsd:string"">" + n.item_desc + @"</item_desc>
                    </item>
                </items>
            </cart>
            <txparams xsi:type=""ns1:txparams"">
                <ref1 xsi:type = ""xsd:string"">" + n.txParams_ref2 + @"</ref1>
                <ref3 xsi:type = ""xsd:string"">" + n.txParams_ref3 + @"</ref3>
            </txparams>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessForSettlement(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <txid xsi:type=""xsd:string"">" + n.txid + @"</txid>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessForRequery(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <txid xsi:type=""xsd:string"">" + n.txid + @"</txid>
            <reference xsi:type=""xsd:string"">" + n.reference + @"</reference>
            <postbackurl xsi:type=""xsd:string"">" + n.postbackurl + @"</postbackurl>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessForRefund(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <txid xsi:type=""xsd:string"">" + n.txid + @"</txid>
            <reason xsi:type=""xsd:string"">" + n.reason + @"</reason>
            <amount xsi:type=""xsd:string"">" + n.amount_refund + @"</amount>
            <postbackurl xsi:type=""xsd:string"">" + n.postbackurl + @"</postbackurl>
            <sendNotification xsi:type=""xsd:string"">" + n.sendNotification + @"</sendNotification>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatGetTransactionByRID(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <rid xsi:type=""xsd:int"">" + n.rid + @"</rid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <from_timestamp xsi:type=""xsd:string"">" + n.from_timestamp + @"</from_timestamp>
            <to_timestamp xsi:type=""xsd:string"">" + n.to_timestamp + @"</to_timestamp>
            <tx_actions xsi:type=""xsd:string"">" + n.tx_actions + @"</tx_actions>
            <responses xsi:type=""xsd:string"">" + n.responses + @"</responses>
            <card_types xsi:type=""xsd:string"">" + n.card_types + @"</card_types>
            <filter_type xsi:type=""xsd:string"">" + n.filter_type + @"</filter_type>
            <filter_search xsi:type=""xsd:string"">" + n.filter_search + @"</filter_search>
            <limit xsi:type=""xsd:int"">" + n.limit + @"</limit>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatGetTransactionBySID(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <from_timestamp xsi:type=""xsd:string"">" + n.from_timestamp + @"</from_timestamp>
            <to_timestamp xsi:type=""xsd:string"">" + n.to_timestamp + @"</to_timestamp>
            <tx_actions xsi:type=""xsd:string"">" + n.tx_actions + @"</tx_actions>
            <responses xsi:type=""xsd:string"">" + n.responses + @"</responses>
            <card_types xsi:type=""xsd:string"">" + n.card_types + @"</card_types>
            <filter_type xsi:type=""xsd:string"">" + n.filter_type + @"</filter_type>
            <filter_search xsi:type=""xsd:string"">" + n.filter_search + @"</filter_search>
            <limit xsi:type=""xsd:int"">" + n.limit + @"</limit>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessRebill(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <parent_txid xsi:type=""xsd:string"">" + n.parent_txid + @"</parent_txid>
            <rebillkey xsi:type=""xsd:string"">" + n.rebillkey + @"</rebillkey>
            <cart xsi:type=""ns1:cart"">
                <summary xsi:type=""ns1:cartsummary"">
                    <quantity xsi:type=""xsd:int"">" + n.quantity + @"</quantity>
                    <amount_purchase xsi:type=""xsd:decimal"">" + n.amount_purchase + @"</amount_purchase>
                    <amount_shipping xsi:type=""xsd:decimal"">" + n.amount_shipping + @"</amount_shipping>
                    <amount_tax xsi:type=""xsd:decimal"">" + n.amount_tax + @"</amount_tax>
                    <currency_code xsi:type=""xsd:string"">" + n.currency_code + @"</currency_code>
                </summary>
                <items SOAP-ENC:arrayType=""ns1:cartitem[1]"" xsi:type=""ns1:cartitems"">
                    <item xsi:type=""ns1:cartitem"">
                        <name xsi:type=""xsd:string"">" + n.itemName + @"</name>
                        <quantity xsi:type=""xsd:int"">" + n.itemQuantity + @"</quantity>
                        <amount_unit xsi:type=""xsd:decimal"">" + n.amount_unit + @"</amount_unit>
                        <item_no xsi:type=""xsd:string"">" + n.item_no + @"</item_no>
                        <item_desc xsi:type=""xsd:string"">" + n.item_desc + @"</item_desc>
                    </item>
                </items>
            </cart>
            <amount xsi:type=""xsd:decimal"">" + n.amount_rebill + @"</amount>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatProcessRebillRegister(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <description xsi:type=""xsd:string"">" + n.description + @"</description>
            <currency xsi:type=""xsd:string"">" + n.currency + @"</currency>
            <udetails xsi:type=""ns1:udetails"">
                <firstname xsi:type=""xsd:string"">" + n.firstname + @"</firstname>
                <lastname xsi:type=""xsd:string"">" + n.lastname + @"</lastname>
                <email xsi:type=""xsd:string"">" + n.email + @"</email>
                <phone xsi:type=""xsd:string"">" + n.phone + @"</phone>
                <address xsi:type=""xsd:string"">" + n.address + @"</address>
                <suburb_city xsi:type=""xsd:string"">" + n.suburb_city + @"</suburb_city>
                <state xsi:type=""xsd:string"">" + n.state + @"</state>
                <postcode xsi:type=""xsd:string"">" + n.postcode + @"</postcode>
                <country xsi:type=""xsd:string"">" + n.country + @"</country>
                <uip xsi:type=""xsd:string"">" + n.uip + @"</uip>
            </udetails>
            <paydetails xsi:type=""ns1:paydetails"">
                <payby xsi:type=""xsd:string"">" + n.payby + @"</payby>
                <card_name xsi:type=""xsd:string"">" + n.card_name + @"</card_name>
                <card_no xsi:type=""xsd:string"">" + n.card_no + @"</card_no>
                <card_ccv xsi:type=""xsd:string"">" + n.card_ccv + @"</card_ccv>
                <card_exp_month xsi:type=""xsd:string"">" + n.card_exp_month + @"</card_exp_month>
                <card_exp_year xsi:type=""xsd:int"">" + n.card_exp_year + @"</card_exp_year>
            </paydetails>
            <txparams xsi:type=""ns1:txparams""/>
            <schedules SOAP-ENC:arrayType=""ns1:schedule[1]""  xsi:type=""ns1:schedules"">
                <item xsi:type=""ns1:schedule"">
                    <startdate xsi:type=""xsd:string"">" + n.startdate + @"</startdate>
                    <amount xsi:type=""xsd:decimal"">" + n.amount_schedule + @"</amount>
                    <frequency xsi:type=""xsd:int"">" + n.frequency + @"</frequency>
                    <frequencyunit xsi:type=""xsd:string"">" + n.frequencyunit + @"</frequencyunit>
                    <repeat xsi:type=""xsd:int"">" + n.repeat + @"</repeat>
                    <description xsi:type=""xsd:string"">" + n.description + @"</description>
                    <item_no xsi:type=""xsd:int"">" + n.item_no + @"</item_no>
                </item>
            </schedules>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatRebillCancel(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <parent_txid xsi:type=""xsd:string"">" + n.parent_txid + @"</parent_txid>
            <postbackurl xsi:type=""xsd:string"">" + n.postbackurl + @"</postbackurl>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatRebillScheduleChange(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <parent_txid xsi:type=""xsd:string"">" + n.parent_txid + @"</parent_txid>
            <schedules SOAP-ENC:arrayType=""ns1:schedule[1]""  xsi:type=""ns1:schedules"">
                <item xsi:type=""ns1:schedule"">
                    <startdate xsi:type=""xsd:string"">" + n.startdate + @"</startdate>
                    <amount xsi:type=""xsd:decimal"">" + n.amount_schedule + @"</amount>
                    <frequency xsi:type=""xsd:int"">" + n.frequency + @"</frequency>
                    <frequencyunit xsi:type=""xsd:string"">" + n.frequencyunit + @"</frequencyunit>
                    <repeat xsi:type=""xsd:int"">" + n.repeat + @"</repeat>
                    <description xsi:type=""xsd:string"">" + n.description + @"</description>
                    <item_no xsi:type=""xsd:int"">" + n.item_no + @"</item_no>
                </item>
            </schedules>
            <merchantrebilling xsi:type=""xsd:string"">" + n.merchantrebilling + @"</merchantrebilling>
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatRebillCardChange(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <parent_txid xsi:type=""xsd:string"">" + n.parent_txid + @"</parent_txid>
            <description xsi:type=""xsd:string"">" + n.description + @"</description>
            <udetails xsi:type=""ns1:udetails"">
                <firstname xsi:type=""xsd:string"">" + n.firstname + @"</firstname>
                <lastname xsi:type=""xsd:string"">" + n.lastname + @"</lastname>
                <email xsi:type=""xsd:string"">" + n.email + @"</email>
                <phone xsi:type=""xsd:string"">" + n.phone + @"</phone>
                <address xsi:type=""xsd:string"">" + n.address + @"</address>
                <suburb_city xsi:type=""xsd:string"">" + n.suburb_city + @"</suburb_city>
                <state xsi:type=""xsd:string"">" + n.state + @"</state>
                <postcode xsi:type=""xsd:string"">" + n.postcode + @"</postcode>
                <country xsi:type=""xsd:string"">" + n.country + @"</country>
                <uip xsi:type=""xsd:string"">" + n.uip + @"</uip>
            </udetails>
            <paydetails xsi:type=""ns1:paydetails"">
                <payby xsi:type=""xsd:string"">" + n.payby + @"</payby>
                <card_name xsi:type=""xsd:string"">" + n.card_name + @"</card_name>
                <card_no xsi:type=""xsd:string"">" + n.card_no + @"</card_no>
                <card_ccv xsi:type=""xsd:string"">" + n.card_ccv + @"</card_ccv>
                <card_exp_month xsi:type=""xsd:string"">" + n.card_exp_month + @"</card_exp_month>
                <card_exp_year xsi:type=""xsd:int"">" + n.card_exp_year + @"</card_exp_year>
            </paydetails>
            <txparams xsi:type=""ns1:txparams""/>            
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatVerification(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <txid xsi:type=""xsd:string"">" + n.txid + @"</txid>
            <verifyresponse xsi:type=""ns1:verifyresponse"">
                <items SOAP-ENC:arrayType=""ns1:verifyitem[1]"" xsi:type=""ns1:verifyitems"">
                   <item xsi:type=""ns1:verifyitem"">
                        <name xsi:type=""xsd:string"">" + n.verifyItemName + @"</name>
                        <value xsi:type=""xsd:string"">" + n.verifyItemValue + @"</value>
                   </item>
                </items>
            </verifyresponse>         
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatFraudCheck(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <udetails xsi:type=""ns1:udetails"">
                <firstname xsi:type=""xsd:string"">" + n.firstname + @"</firstname>
                <lastname xsi:type=""xsd:string"">" + n.lastname + @"</lastname>
                <email xsi:type=""xsd:string"">" + n.email + @"</email>
                <phone xsi:type=""xsd:string"">" + n.phone + @"</phone>
                <address xsi:type=""xsd:string"">" + n.address + @"</address>
                <suburb_city xsi:type=""xsd:string"">" + n.suburb_city + @"</suburb_city>
                <state xsi:type=""xsd:string"">" + n.state + @"</state>
                <postcode xsi:type=""xsd:string"">" + n.postcode + @"</postcode>
                <country xsi:type=""xsd:string"">" + n.country + @"</country>
                <uip xsi:type=""xsd:string"">" + n.uip + @"</uip>
            </udetails>
            <paydetails xsi:type=""ns1:paydetails"">
                <payby xsi:type=""xsd:string"">" + n.payby + @"</payby>
                <card_name xsi:type=""xsd:string"">" + n.card_name + @"</card_name>
                <card_no xsi:type=""xsd:string"">" + n.card_no + @"</card_no>
                <card_ccv xsi:type=""xsd:string"">" + n.card_ccv + @"</card_ccv>
                <card_exp_month xsi:type=""xsd:string"">" + n.card_exp_month + @"</card_exp_month>
                <card_exp_year xsi:type=""xsd:int"">" + n.card_exp_year + @"</card_exp_year>
            </paydetails>
            <purchase xsi:type=""ns1:purchase"">
                <amount_purchase xsi:type=""xsd:decimal"">" + n.fraud_amount_purchase + @"</amount_purchase>
            </purchase>
            <txparams xsi:type=""ns1:txparams""/>          
            </ns1:" + processName + @">";
            return format;
        }

        private static string formatGetFutureRebills(string processName, GateWayProcessPaymentRequestEx n)
        {
            string format = @"<ns1:" + processName + @">
            <sid xsi:type=""xsd:int"">" + n.sid + @"</sid>
            <rcode xsi:type=""xsd:string"">" + n.rcode + @"</rcode>
            <txid xsi:type=""xsd:string"">" + n.txid + @"</txid>
            <from_timestamp xsi:type=""xsd:string"">" + n.from_timestamp + @"</from_timestamp>
            <to_timestamp xsi:type=""xsd:string"">" + n.to_timestamp + @"</to_timestamp>
            <card_types xsi:type=""xsd:string"">" + n.card_types + @"</card_types>
            <filter_type xsi:type=""xsd:string"">" + n.filter_type + @"</filter_type>
            <filter_search xsi:type=""xsd:string"">" + n.filter_search + @"</filter_search>
            <limit xsi:type=""xsd:int"">" + n.limit + @"</limit>
            </ns1:" + processName + @">";
            return format;
        }

    }
}