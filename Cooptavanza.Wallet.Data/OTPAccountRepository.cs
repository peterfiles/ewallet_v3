﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class OTPAccountRepository : IOTPAccountRepository
    {
        readonly IRepositoryEF<OTPAccount> repo;
        //readonly cooptavanza_ewalletEntities db;
        readonly CW_EntitiesAzure db;
        public OTPAccountRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<OTPAccount>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.OTPAccounts.Where(sWhere).Count() : db.OTPAccounts.Count();
        }

        public OTPAccountEx Create()
        {
            return new OTPAccountEx();
        }

        public OTPAccountEx Get(int Id)
        {
            var n = db.OTPAccounts.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public OTPAccountEx GetLogin(string username, string OTP, DateTime dateTime ) {
            OTPAccountEx r = new OTPAccountEx();
            List<OTPAccountEx> l = new SPConnection(db).sp_GetOTPRecord(username);
            if (l != null) {
                foreach( var x in l)
                {
                    r = x;
                }
            }
            else
            {
                r = null;
            }
            return r;
        }

        private OTPAccountEx Transform(OTPAccount n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new OTPAccountEx();
            l.id = n.id;
            l.OTP = n.OTP;
            l.userId = n.userId;
            l.deviceId = n.deviceId;
            l.userRoles = n.userRoles;
            l.dateTime = n.dateTime;
            l.mobileUTC = n.mobileUTC;
            return l;
        }

        private OTPAccount Transform(OTPAccountEx n)
        {
            OTPAccount l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.OTP = n.OTP;
            l.userId = n.userId;
            l.deviceId = n.deviceId;
            l.userRoles = n.userRoles;
            l.dateTime = n.dateTime;
            l.mobileUTC = n.mobileUTC;

            return l;
        }

        public List<OTPAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.OTPAccounts.Where(sWhere).OrderBy(orderby) : db.OTPAccounts.OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
           }
           else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(OTPAccountEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(OTPAccountEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
        }


      

        private List<OTPAccountEx> Transform(IQueryable<OTPAccount> aOTPAccount)
        {
            List<OTPAccountEx> l = new List<OTPAccountEx>();
            foreach (OTPAccount b in aOTPAccount)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public OTPAccountEx getByDUR(string deviceId, int id, int role)
        {
            var n = db.OTPAccounts.Where(c => c.userId == id && c.deviceId == deviceId && c.userRoles == role).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public OTPAccountEx getByUR(int id, int role)
        {
            var n = db.OTPAccounts.Where(c => c.userId == id && c.userRoles == role).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }
    }
}
