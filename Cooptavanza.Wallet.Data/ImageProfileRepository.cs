﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class ImageProfileRepository : IImageProfileRepository
    {
        readonly IRepositoryEF<imageprofile> repo;
        readonly CW_EntitiesAzure db;

        public ImageProfileRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<imageprofile>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public ImageProfileEx Create()
        {
            return new ImageProfileEx();
        }

        public ImageProfileEx Get(int Id)
        {
            var n = db.imageprofiles.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private ImageProfileEx Transform(imageprofile n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ImageProfileEx();

            l.id = n.id;
            l.userId = n.userId;
            l.imageString = n.imageString;
            l.path = n.path;
            return l;
        }

        private imageprofile Transform(ImageProfileEx n)
        {
            imageprofile l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.userId = n.userId;
            l.imageString = n.imageString;
            l.path = n.path;

            return l;
        }

        public List<ImageProfileEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.imageprofiles.Where(sWhere).OrderBy(orderby) : db.imageprofiles.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ImageProfileEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ImageProfileEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.userId = g.userId;
            item.imageString = g.imageString;
            item.path = g.path;

        }




        private List<ImageProfileEx> Transform(IQueryable<imageprofile> aPerson)
        {
            List<ImageProfileEx> l = new List<ImageProfileEx>();
            foreach (imageprofile b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public ImageProfileEx GetByUserId(int Id)
        {
            var n = db.imageprofiles.Where(c => c.userId == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

      
    }
}
