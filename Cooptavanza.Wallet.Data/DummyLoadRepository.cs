﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class DummyLoadRepository : IDummyLoadRepository
    {
        readonly IRepositoryEF<dummyload> repo;
        readonly CW_EntitiesAzure db;

        public DummyLoadRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<dummyload>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.dummyloads.Where(sWhere).Count() : db.dummyloads.Count();
        }

        public DummyLoadEx Create()
        {
            return new DummyLoadEx();
        }

        public DummyLoadEx Get(int Id)
        {
            var n = db.dummyloads.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

       

        private DummyLoadEx Transform(dummyload n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new DummyLoadEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.bankid = n.bankid;
            l.userid = n.userid;
            l.userpin = n.userpin;
            l.credit = n.credit;
            l.status = n.status;
            l.currencycode = n.currencycode;
            return l;
        }

        private dummyload Transform(DummyLoadEx n)
        {
            dummyload l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.bankid = n.bankid;
            l.userid = n.userid;
            l.userpin = n.userpin;
            l.credit = n.credit;
            l.status = n.status;
            l.currencycode = n.currencycode;


            return l;
        }

        public List<DummyLoadEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.dummyloads.Where(sWhere).OrderBy(orderby) : db.dummyloads.OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
           }
           else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(DummyLoadEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(DummyLoadEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
        }


      

        private List<DummyLoadEx> Transform(IQueryable<dummyload> aDummyLoad)
        {
            List<DummyLoadEx> l = new List<DummyLoadEx>();
            foreach (dummyload b in aDummyLoad)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public DummyLoadEx getByCredentials(string accountnumber, string userpin)
        {
            var l = repo.Query().Where(v => v.accountnumber == accountnumber && v.userpin == userpin).FirstOrDefault();
            if (l != null) {
                return Transform(l);
            }
            return null;
        }
    }
}
