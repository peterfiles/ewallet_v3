﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;
using System.Data;

namespace Cooptavanza.Wallet.Data
{
    public class UserAccountRepository : IUserAccountRepository
    {
        readonly IRepositoryEF<userAccount> repo;
        
        readonly CW_EntitiesAzure db;

        public UserAccountRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<userAccount>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.userAccounts.Where(sWhere).Count() : db.userAccounts.Count();
        }

        public UserAccountEx Create()
        {
            return new UserAccountEx();
        }

        public UserAccountEx Get(int Id)
        {
            var n = db.userAccounts.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public UserAccountEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private UserAccountEx Transform(userAccount n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new UserAccountEx();
            l.id = n.id;
            l.user_id = n.user_id;
            l.roles_id = n.roles_id;
            l.password = n.password;
            l.tID = n.tID;
            l.username = n.username;
            return l;
        }

        private userAccount Transform(UserAccountEx n)
        {
            userAccount l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.id = n.id;
            l.user_id = n.user_id;
            l.roles_id = n.roles_id;
            l.password = n.password;
            l.tID = n.tID;
            l.username = n.username;

            return l;
        }

        public List<UserAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.userAccounts.Where(sWhere).OrderBy(orderby) : db.userAccounts.OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
           }
           else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(UserAccountEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(UserAccountEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
        }


      

        private List<UserAccountEx> Transform(IQueryable<userAccount> aUserAccount)
        {
            List<UserAccountEx> l = new List<UserAccountEx>();
            foreach (userAccount b in aUserAccount)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public UserAccountEx GetAccountByUserId(int r, int id, string tid)
        {
            var n = db.userAccounts.Where(c => c.roles_id == r && c.tID == tid && c.user_id == id).FirstOrDefault();
            if (n != null) {
                return Transform(n);
            }return null;
        }

        public UserAccountEx GetAuthentication(int v, string password, string tid, int id)
        {
            var n = db.userAccounts.Where(c => c.roles_id == v && c.password == password && c.tID == tid && c.user_id == id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public UserAccountEx GetAdminAuthenticate(string username, string password)
        {
            var n = db.userAccounts.Where(c => c.password == password && c.username == username).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public UserAccountEx GetAdminAuthenticate(string username, string password, string corporatename)
        {
            
            AuthGlobalEx AG = new SPConnection(db).SP_Authenticate_Global(username, password, corporatename);
            if(AG != null)
            {
                UserAccountEx UA = new UserAccountEx();
                UA.roles_id = AG.role;
                UA.tID = AG.tid;
                UA.user_id = AG.id;
                return UA;
            }
            return null;
        }

        public List<MemberEx> GetAllMember(string str, int psize, int pno)
        {
            return new SPConnection(db).SP_getAllMember(str, psize, pno);
        }

        public int countmember(string str)
        {
            return new SPConnection(db).SP_getAllMember_count(str);
        }


        public List<MemberEx> GetAllMemberByParentID(string str, int psize, int pno, int userId)
        {
            return new SPConnection(db).SP_getAllMemberByParentID(str, psize, pno, userId);
        }

        public int countmemberFilterByParentID(string str, int parentID)
        {
            return new SPConnection(db).SP_getMember_count_filterByParentID(str, parentID);
        }

        public List<MemberEx> GetAllMemberByParentIDAndRoleID(string str, int psize, int pno, int userId, int roleID)
        {
            return new SPConnection(db).SP_getAllMemberByParentIDAndRoleID(str, psize, pno, userId, roleID);
        }

        public int countmemberFilterByParentIDAndRoleID(string str, int parentID, int roleID)
        {
            return new SPConnection(db).SP_getMember_count_filterByParentIDAndRoleID(str, parentID, roleID);
        }

        public UserAccountEx CheckUsername(string username)
        {
            var n = db.userAccounts.Where(c => c.username == username).FirstOrDefault();
            if(n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public AuthGlobalEx GetAccountByUserPassword(AuthMobileEx n)
        {
          return new SPConnection(db).SP_Authenticate_Global(n.username, n.password);
        }

     
    }
}
