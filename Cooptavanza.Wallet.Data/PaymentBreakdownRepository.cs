﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class PaymentBreakdownRepository : IPaymentBreakdownRepository
    {
        readonly IRepositoryEF<paymentBreakdown> repo;
        readonly CW_EntitiesAzure db;

        public PaymentBreakdownRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<paymentBreakdown>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public PaymentBreakdownEx Create()
        {
            return new PaymentBreakdownEx();
        }

        public PaymentBreakdownEx Get(int Id)
        {
            var n = db.paymentBreakdowns.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private PaymentBreakdownEx Transform(paymentBreakdown n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new PaymentBreakdownEx();

            l.id = n.id;
            l.amount = n.amount;
            l.currencyID = n.currencyID;
            l.currencyISO = n.currencyISO;
            l.isMerchant = n.isMerchant;
            l.mdr = n.mdr;
            l.remarks = n.remarks;
            l.transactionTID = n.transactionTID;
            l.txFee = n.txFee;
            l.txFeeCurrency = n.txFeeCurrency;
            l.userID = n.userID;
            l.userRole = n.userRole;
            l.dateCreated = n.dateCreated;
            l.transactionType = n.transactionType;

            return l;
        }

        private paymentBreakdown Transform(PaymentBreakdownEx n)
        {
            paymentBreakdown l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;

            l.amount = n.amount;
            l.currencyID = n.currencyID;
            l.currencyISO = n.currencyISO;
            l.isMerchant = n.isMerchant;
            l.mdr = n.mdr;
            l.remarks = n.remarks;
            l.transactionTID = n.transactionTID;
            l.txFee = n.txFee;
            l.txFeeCurrency = n.txFeeCurrency;
            l.userID = n.userID;
            l.userRole = n.userRole;
            l.dateCreated = n.dateCreated;
            l.transactionType = n.transactionType;

            return l;
        }

        public List<PaymentBreakdownEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.paymentBreakdowns.Where(sWhere).OrderBy(orderby) : db.paymentBreakdowns.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(PaymentBreakdownEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(PaymentBreakdownEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();

            l.id = n.id;

            l.amount = n.amount;
            l.currencyID = n.currencyID;
            l.currencyISO = n.currencyISO;
            l.isMerchant = n.isMerchant;
            l.mdr = n.mdr;
            l.remarks = n.remarks;
            l.transactionTID = n.transactionTID;
            l.txFee = n.txFee;
            l.txFeeCurrency = n.txFeeCurrency;
            l.userID = n.userID;
            l.userRole = n.userRole;
            l.dateCreated = n.dateCreated;
            l.transactionType = n.transactionType;


        }

        private List<PaymentBreakdownEx> Transform(IQueryable<paymentBreakdown> aBiller)
        {
            List<PaymentBreakdownEx> l = new List<PaymentBreakdownEx>();
            foreach (paymentBreakdown b in aBiller)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<PaymentBreakdownEx> GetCommissionByPaymentBreakdown(int uID, int rID)
        {
            var d = db.paymentBreakdowns.Where(x => x.userID == uID && x.userRole == rID);
            return Transform(d);
        }

        public List<PaymentBreakdownEx> GetPaymentBreakdownByTransactionID(string transactionID, string orderby, int PageNo, int PageSize)
        {
            var r = db.paymentBreakdowns.Where(x => x.transactionTID.Equals(transactionID)).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

    }
}
