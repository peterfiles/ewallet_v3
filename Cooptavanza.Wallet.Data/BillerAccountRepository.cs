﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class BillerAccountRepository : IBillerAccountRepository
    {
        readonly IRepositoryEF<billerAccount> repo;
        readonly CW_EntitiesAzure db;

        public BillerAccountRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<billerAccount>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.billerAccounts.Where(sWhere).Count() : db.billerAccounts.Count();
        }

        public BillerAccountEx Create()
        {
            return new BillerAccountEx();
        }

        public BillerAccountEx Get(int Id)
        {
            var n = db.billerAccounts.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private BillerAccountEx Transform(billerAccount n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new BillerAccountEx();

            l.id = n.id;
            l.billerAccount1 = n.billerAccount1;
            l.billerBranch = n.billerBranch;
            l.billerID = n.billerID;
            l.billerTID = n.billerTID;
            l.billerType = n.billerType;
            l.countryID = n.countryID;
            l.currencyID = n.currencyID;
            l.currencyISO = n.currencyISO;
            l.stateID = n.stateID;
            return l;
        }

        private billerAccount Transform(BillerAccountEx n)
        {
            billerAccount l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.billerAccount1 = n.billerAccount1;
            l.billerBranch = n.billerBranch;
            l.billerID = n.billerID;
            l.billerTID = n.billerTID;
            l.billerType = n.billerType;
            l.countryID = n.countryID;
            l.currencyID = n.currencyID;
            l.currencyISO = n.currencyISO;
            l.stateID = n.stateID;
          

            return l;
        }

        public List<BillerAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.billerAccounts.Where(sWhere).OrderBy(orderby) : db.billerAccounts.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(BillerAccountEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(BillerAccountEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();

            l.id = n.id;
        }

        public BillerAccountEx GetBillerByAccountNumber(string bA)
        {
            var d = db.billerAccounts.Where(x => x.billerAccount1.Equals(bA)).FirstOrDefault();
            return Transform(d);
        }

        public BillerAccountEx GetBillerByCountryStateBranchISO(int cID, int sID, string branch, string iso)
        {
            var d = db.billerAccounts.Where(x => x.countryID == cID)
                                     .Where(y => y.stateID == sID)
                                     .Where(z => z.billerBranch.Equals(branch))
                                     .Where(w => w.currencyISO.Equals(iso)).FirstOrDefault();
            return Transform(d);
        }


        private List<BillerAccountEx> Transform(IQueryable<billerAccount> aBillerType)
        {
            List<BillerAccountEx> l = new List<BillerAccountEx>();
            foreach (billerAccount b in aBillerType)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public int CountBillerAccount()
        {
            return db.billerAccounts.Count();
        }
    }
}
