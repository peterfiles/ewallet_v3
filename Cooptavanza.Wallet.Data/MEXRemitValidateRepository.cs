﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXRemitValidateRepository : IMEXRemitValidateRepository
    {
        readonly IRepositoryEF<MEX_RemitValidate> repo;
        readonly CW_EntitiesAzure db;

        public MEXRemitValidateRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_RemitValidate>(db);

        }

      
        public MoneyExpressRemitEx Get(int Id)
        {
            var n = db.MEX_RemitValidate.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MoneyExpressRemitEx Transform(MEX_RemitValidate n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MoneyExpressRemitEx();
            l.id = n.id;
            l.AmlStatus = n.AmlStatus;
            l.FloatCurrency = n.FloatCurrency;
            l.IncludeFees = n.IncludeFees;
            l.KioskAddress = n.KioskAddress;
            l.KioskName = n.KioskName;
            l.KycCheck = n.KycCheck;
            l.Note = n.Note;
            l.OriginatorUID = n.OriginatorUID;
            //reciever part
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvAccntType = n.RcvAccntType;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvIdNumber = n.RcvIdNumber;
            l.RcvIdType = n.RcvIdType;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;

            //sender
            l.SndAddress = n.SndAddress;
            l.SndFirstName = n.SndFirstName;
            l.SndIdNo = n.SndIdNo;
            l.SndIdType = n.SndIdType;
            l.SndKycUrl = n.SndKycUrl;
            l.SndLastName = n.SndLastName;
            l.SndPhoneNo = n.SndPhoneNo;
            l.SndRemitPurpose = n.SndRemitPurpose;

            //teller
            l.TellerFirstName = n.TellerFirstName;
            l.TellerLastName = n.TellerLastName;
            l.TranAmt = n.TranAmt;
            l.TranCurr = n.TranCurr;
            l.ValidateReferenceId = n.ValidateReferenceId;
            l.Signature = n.Signature_;

            l.id = n.id;
            l.MEXType = n.MEXType;

            return l;
        }

        private MEX_RemitValidate Transform(MoneyExpressRemitEx n)
        {
            MEX_RemitValidate l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.AmlStatus = n.AmlStatus;
            l.FloatCurrency = n.FloatCurrency;
            l.IncludeFees = n.IncludeFees;
            l.KioskAddress = n.KioskAddress;
            l.KioskName = n.KioskName;
            l.KycCheck = n.KycCheck;
            l.Note = n.Note;
            l.OriginatorUID = n.OriginatorUID;
            //reciever part
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvAccntType = n.RcvAccntType;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvIdNumber = n.RcvIdNumber;
            l.RcvIdType = n.RcvIdType;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;

            //sender
            l.SndAddress = n.SndAddress;
            l.SndFirstName = n.SndFirstName;
            l.SndIdNo = n.SndIdNo;
            l.SndIdType = n.SndIdType;
            l.SndKycUrl = n.SndKycUrl;
            l.SndLastName = n.SndLastName;
            l.SndPhoneNo = n.SndPhoneNo;
            l.SndRemitPurpose = n.SndRemitPurpose;

            //teller
            l.TellerFirstName = n.TellerFirstName;
            l.TellerLastName = n.TellerLastName;
            l.TranAmt = n.TranAmt;
            l.TranCurr = n.TranCurr;
            l.ValidateReferenceId = n.ValidateReferenceId;
            l.Signature_ = n.Signature;

            l.id = n.id;
            l.MEXType = n.MEXType;
            return l;
        }

        public List<MoneyExpressRemitEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(RcvFirstName.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_RemitValidate.Where(sWhere).OrderBy(orderby) : db.MEX_RemitValidate.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(RcvFirstName.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.MEX_RemitValidate.Where(sWhere).Count() : db.MEX_RemitValidate.Count();
        }



        private List<MoneyExpressRemitEx> Transform(IQueryable<MEX_RemitValidate> o)
        {
            List<MoneyExpressRemitEx> l = new List<MoneyExpressRemitEx>();
            foreach (MEX_RemitValidate b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }


        public void Remove(MoneyExpressRemitEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(MoneyExpressRemitEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();
            l.id = n.id;
            l.AmlStatus = n.AmlStatus;
            l.FloatCurrency = n.FloatCurrency;
            l.IncludeFees = n.IncludeFees;
            l.KioskAddress = n.KioskAddress;
            l.KioskName = n.KioskName;
            l.KycCheck = n.KycCheck;
            l.Note = n.Note;
            l.OriginatorUID = n.OriginatorUID;
            //reciever part
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvAccntType = n.RcvAccntType;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvIdNumber = n.RcvIdNumber;
            l.RcvIdType = n.RcvIdType;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;

            //sender
            l.SndAddress = n.SndAddress;
            l.SndFirstName = n.SndFirstName;
            l.SndIdNo = n.SndIdNo;
            l.SndIdType = n.SndIdType;
            l.SndKycUrl = n.SndKycUrl;
            l.SndLastName = n.SndLastName;
            l.SndPhoneNo = n.SndPhoneNo;
            l.SndRemitPurpose = n.SndRemitPurpose;

            //teller
            l.TellerFirstName = n.TellerFirstName;
            l.TellerLastName = n.TellerLastName;
            l.TranAmt = n.TranAmt;
            l.TranCurr = n.TranCurr;
            l.ValidateReferenceId = n.ValidateReferenceId;
            l.Signature = n.Signature_;

            l.id = n.id;
            l.MEXType = n.MEXType;

        }

        public int GetLastId()
        {
           var r = db.MEX_RemitValidate.OrderBy("id Desc").FirstOrDefault();
           
            if (r != null) {
                var x = Transform(r);
                return x.id + db.MEX_ResponseRemitErrorLog.Count();
            }
            else { return db.MEX_ResponseRemitErrorLog.Count(); }
            
        }

        public MoneyExpressRemitEx Create()
        {
           
           return new MoneyExpressRemitEx();
           
        }
    }
}
