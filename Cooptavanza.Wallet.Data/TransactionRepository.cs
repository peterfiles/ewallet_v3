﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;
using System.Data.Entity.Core.Objects;

namespace Cooptavanza.Wallet.Data
{
    public class TransactionRepository : ITransactionRepository
    {
        readonly IRepositoryEF<transaction> repo;
        readonly CW_EntitiesAzure db;

        public TransactionRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<transaction>(db);

        }

        public int Count(int id, string TID, string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.transactions.Where(sWhere).Where(c => c.userId == id && c.userTID == TID).Count() : db.transactions.Where(c => c.userId == id && c.userTID == TID).Count();
        }

        public TransactionEx Create()
        {
            return new TransactionEx();
        }

        public TransactionEx Get(int Id)
        {
            var n = db.transactions.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public TransactionEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private TransactionEx Transform(transaction n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new TransactionEx();
            l.id = n.id;
            l.transactionType = n.transactionType;
            l.dataCreated = n.dataCreated;
            l.debit = n.debit;
            l.credit = n.credit;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.transactionTID = n.transactionTID;
            l.balance = n.balance;
            l.description = n.description;
            l.status = n.status;
            l.currencyfromiso = n.currencyfromiso;
            l.currencytoiso = n.currencytoiso;
            l.enrolledAccountsID = n.enrolledAccountsID;
            l.enrolledAccountsCurrencyID = n.enrolledAccountsCurrencyID;
            l.accountNumber = n.accountNumber;
            l.receiversID = n.receiversID;
            l.partnerId = n.partnerId;
            return l;
        }

        private transaction Transform(TransactionEx n)
        {
            transaction l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.transactionType = n.transactionType;
            l.dataCreated = n.dataCreated;
            l.debit = n.debit;
            l.credit = n.credit;
            l.userId = n.userId;
            l.userTID = n.userTID;
            l.transactionTID = n.transactionTID;
            l.balance = n.balance;
            l.description = n.description;
            l.status = n.status;
            l.currencyfromiso = n.currencyfromiso;
            l.currencytoiso = n.currencytoiso;
            l.enrolledAccountsID = n.enrolledAccountsID;
            l.enrolledAccountsCurrencyID = n.enrolledAccountsCurrencyID;
            l.accountNumber = n.accountNumber;
            l.receiversID = n.receiversID;
            l.partnerId = n.partnerId;
            return l;
        }

        public List<TransactionEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.transactions.Where(sWhere).Where(c => c.userId == id && c.userTID == TID).OrderBy(orderby) : db.transactions.Where(c => c.userId == id && c.userTID == TID).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
           else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<TransactionEx> GetByPartnerIDAndTransactionType(int userID, int userRole, int PageNo, int PageSize, string ttype)
        {
            return new SPConnection(db).GetByPartnerIDAndTransactionType(userID,userRole,PageNo,PageSize,ttype);
        }

        public void Remove(TransactionEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(TransactionEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.transactionType = g.transactionType;
            item.dataCreated = g.dataCreated;
            item.debit = g.debit;
            item.credit = g.credit;
            item.userId = g.userId;
            item.userTID = g.userTID;
            item.transactionTID = g.transactionTID;
            item.balance = g.balance;
            item.description = g.description;
            item.status = g.status;
            item.currencyfromiso = g.currencyfromiso;
            item.currencytoiso = g.currencytoiso;
            item.enrolledAccountsID = g.enrolledAccountsID;
            item.enrolledAccountsCurrencyID = g.enrolledAccountsCurrencyID;
            item.accountNumber = g.accountNumber;
            item.receiversID = g.receiversID;
            item.partnerId = g.partnerId;
        }


      

        private List<TransactionEx> Transform(IQueryable<transaction> aTransaction)
        {
            List<TransactionEx> l = new List<TransactionEx>();
            foreach (transaction b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<BalanceEx> GetBalance(int id)
        {
            return new SPConnection(db).SP_getBalance(id);
        }

        public List<BalanceEx> GetBalancePending(int id)
        {
            return new SPConnection(db).SP_getBalancePending(id);
        }

        public List<BalanceEx> GetBalanceOverall(int id)
        {
            return new SPConnection(db).SP_getBalanceOverall(id);
        }

        public int GetTransactionLastIDNumber()
        {
            return db.transactions.OrderByDescending(x => x.id).Take(1).FirstOrDefault().id;
        }

        public List<GetTypeTransactionEx> GetTransactionType()
        {
            return new SPConnection(db).GetAllTransactionType();
        }

        public List<GetTransactionListByTypeEx> GetTransactionFilterByTransactionType(string tType,string status)
        {
            return new SPConnection(db).GetTransactionsByTransactionType(tType,status);
        }

        public List<TransactionEx> GetFilterTransactionPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(transactionType.Contains(\"" + where + "\"))");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.transactions.Where(sWhere)
                                                 .Where(x => !x.transactionType.Equals("LOAD_IN"))
                                                 .OrderBy(orderby) : db.transactions
                                                 .Where(x => !x.transactionType.Equals("LOAD_IN"))
                                                 .OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
           else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<TransactionEx> GetTransactionByType(string where, string orderby, int PageNo, int PageSize)
        {
        
            var r = db.transactions.Where(x => x.transactionType.Equals(where))
                                                 .OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<TransactionEx> GetByPartner(string searchText, int partnerId, string dateStart, string dateEnd, string transactionType, string referenceCode, string orderBy, int pageNo, int pageSize)
        {
            orderBy = orderBy != null ? orderBy : "Id Asc";
            searchText = searchText != null ? searchText : "";
            transactionType = transactionType != null ? transactionType : "";
            referenceCode = referenceCode != null ? referenceCode : "";
            dateStart = dateStart != "0001-01-01" ? dateStart : "";
            dateEnd = dateEnd != "0001-01-01" ? dateEnd : "";
            string where = "";
            List<string> lwhere = new List<string>();
            if (searchText != "")
            {
                lwhere.Add(" (description.ToUpper()).Contains(\"" + searchText.ToUpper() + "\") ");// || senderLastName.Contains(\"" + searchText + "\")) || receiverFirstName.Contains(\"" + searchText + "\")) || receiverLastName.Contains(\"" + searchText + "\"))");
            }
            if (transactionType != "")
            {
                lwhere.Add(" transactionType = \"" + transactionType + "\" ");
            }
          //  db.transactions.Where(x => (x.description.ToUpper()).Contains(searchText.ToUpper()));
            
            if (partnerId > 0)
            {
                lwhere.Add(" partnerId = " + partnerId + " ");
            }
            if (lwhere.Count > 0)
            {
                where = string.Join("&&", lwhere);
            }


          var r = db.transactions.Where(where).OrderBy(orderBy);
            if (dateStart != "")
            {
                DateTime dateS = Convert.ToDateTime(dateStart).AddDays(Convert.ToDouble(-1));
                DateTime dateE = Convert.ToDateTime(dateEnd).AddDays(Convert.ToDouble(1));
                r = db.transactions.Where(where).Where(x => x.dataCreated >= dateS && x.dataCreated <= dateE).OrderBy(orderBy);
            }
         

            if (pageNo == 0 || pageSize == 0)
            {
                return Transform(r);

            }
            else
            {
                return Transform(r.Skip((pageNo - 1) * pageSize).Take(pageSize));
            }
        }

        public int CountByPartner(string searchText, int partnerId, string dateStart, string dateEnd, string transactionType, string referenceCode)
        {
           
            searchText = searchText != null ? searchText : "";
            transactionType = transactionType != null ? transactionType : "";
            referenceCode = referenceCode != null ? referenceCode : "";
            dateStart = dateStart != "0001-01-01" ? dateStart : "";
            dateEnd = dateEnd != "0001-01-01" ? dateEnd : "";
            string where = "";
            List<string> lwhere = new List<string>();
            if (searchText != "")
            {
                lwhere.Add("(description.Contains(\"" + searchText + "\")) ");//|| senderLastName.Contains(\"" + searchText + "\")) || receiverFirstName.Contains(\"" + searchText + "\")) || receiverLastName.Contains(\"" + searchText + "\"))");
            }
            if (transactionType != "")
            {
                lwhere.Add("transactionType = \"" + transactionType + "\"");
            }
            //if (dateStart != "")
            //{
            //    lwhere.Add("(dataCreated.Value.Date" + dateStart + " && dataCreated.Value.Date <= " + dateEnd + ")");
            //}
            if (partnerId > 0)
            {
                lwhere.Add("partnerId = " + partnerId);
            }
            if (lwhere.Count > 0)
            {
                where = string.Join("&&", lwhere);
            }
            var r = db.transactions.Where(where);
            int count = 0;
            if (dateStart != "")
            {
                DateTime dateS = Convert.ToDateTime(dateStart).AddDays(Convert.ToDouble(-1));
                DateTime dateE = Convert.ToDateTime(dateEnd).AddDays(Convert.ToDouble(1));
                count = r.Where(x => x.dataCreated >= dateS && x.dataCreated <= dateE).Count();
            }
            else
            {
               count = db.transactions.Where(where).Count();
            }

            return count;
        }

        public TransactionEx getStatusByPartner(int id, string referenceNo)
        {
            var n = db.transactions.Where(x => x.transactionTID == referenceNo && x.partnerId == id).FirstOrDefault();
            if(n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public bool CheckTransactionId(string referenceNo, int partnerId)
        {
            var n = db.transactions.Where(x => x.transactionTID == referenceNo && x.partnerId == partnerId).FirstOrDefault();
            if(n != null)
            {
                return true;
            }
            return false;
        }

        public List<ChartsEx> GetChartsList(DateTime d1, DateTime d2)
        {
            return new SPConnection(db).sp_GetChartsList(d1,d2);
        }

        public TransactionEx GetTransactionListByTID(string tid)
        {
            var d = db.transactions.Where(x => x.transactionTID.Equals(tid)).FirstOrDefault();
            return Transform(d);
        }

        public List<TransactionEx> GetTransactionByTID(string tid, string orderby, int PageNo, int PageSize)
        {

            var r = db.transactions.Where(x => x.transactionTID.Equals(tid) && x.debit == true).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }
    }
}
