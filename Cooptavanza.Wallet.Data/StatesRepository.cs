﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class StatesRepository : IStatesRepository
    {
        readonly IRepositoryEF<state> repo;
        readonly CW_EntitiesAzure db;

        public StatesRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<state>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.states.Where(sWhere).Where(x => x.id > 0).Count() : db.states.Where(x => x.id > 0).Count();
        }

        public StatesEx Create()
        {
            return new StatesEx();
        }

        public StatesEx Get(int Id)
        {
            var n = db.states.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

      
        private StatesEx Transform(state n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new StatesEx();
            l.id = n.id;
            l.name = n.name;
            l.code = n.code;
            l.countryid = n.countryid;
            return l;
        }

        private state Transform(StatesEx n)
        {
            state l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.name = n.name;
            l.code = n.code;
            l.countryid = n.countryid;

            return l;
        }

        public List<StatesEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.states.Where(sWhere).Where(x => x.id > 0).OrderBy(orderby) : db.states.Where(x => x.id > 0).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<StatesEx> GetStatesByCountryId(int id)
        {
            var r = db.states.Where(x => x.countryid == id).OrderBy("id Asc");
            return Transform(r);
           
        }


        public void Remove(StatesEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(StatesEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
        }


      

        private List<StatesEx> Transform(IQueryable<state> aStates)
        {
            List<StatesEx> l = new List<StatesEx>();
            foreach (state b in aStates)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public StatesEx GetByStateName(string sname)
        {
            var res = db.states.Where(x => x.name.Equals(sname)).FirstOrDefault();
            return Transform(res);
        }

        public object getAgentStatesByCountryID(int cID)
        {
            return new SPConnection(db).getAgentStatesByCountryID(cID);
        }
    }
}
