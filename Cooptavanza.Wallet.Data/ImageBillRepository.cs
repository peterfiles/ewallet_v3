﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class ImageBillRepository : IImageBillsRepository
    {
        readonly IRepositoryEF<imagebill> repo;
        readonly CW_EntitiesAzure db;

        public ImageBillRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<imagebill>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public ImageBillEx Create()
        {
            return new ImageBillEx();
        }

        public ImageBillEx Get(int Id)
        {
            var n = db.imagebills.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private ImageBillEx Transform(imagebill n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ImageBillEx();
            l.id = n.id;
            l.date_created = n.date_created;
            l.path = n.path;
            l.userId = n.userId;
            l.imageString = n.imageString;
            return l;
        }

        private imagebill Transform(ImageBillEx n)
        {
            imagebill l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.date_created = n.date_created;
            l.path = n.path;
            l.userId = n.userId;
            l.imageString = n.imageString;
            return l;
        }

        public List<ImageBillEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.imagebills.Where(sWhere).OrderBy(orderby) : db.imagebills.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ImageBillEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ImageBillEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.date_created = g.date_created;
            item.userId = g.userId;
            item.imageString = g.imageString;
            item.path = g.path;
        }


      

        private List<ImageBillEx> Transform(IQueryable<imagebill> aPerson)
        {
            List<ImageBillEx> l = new List<ImageBillEx>();
            foreach (imagebill b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public ImageBillEx GetByUserId(int Id)
        {
            var n = db.imagebills.Where(c => c.userId == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

    }
}
