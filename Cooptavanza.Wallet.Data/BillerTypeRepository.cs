﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class BillerTypeRepository : IBillerTypeRepository
    {
        readonly IRepositoryEF<billerType> repo;
        readonly CW_EntitiesAzure db;

        public BillerTypeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<billerType>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public BillerTypeEx Create()
        {
            return new BillerTypeEx();
        }

        public BillerTypeEx Get(int Id)
        {
            var n = db.billerTypes.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private BillerTypeEx Transform(billerType n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new BillerTypeEx();

            l.id = n.id;
            l.name = n.name;
            l.tid = n.tid;
           
            return l;
        }

        private billerType Transform(BillerTypeEx n)
        {
            billerType l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.name = n.name;
            l.tid = n.tid;
          

            return l;
        }

        public List<BillerTypeEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.billerTypes.Where(sWhere).OrderBy(orderby) : db.billerTypes.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(BillerTypeEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(BillerTypeEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();

            l.id = n.id;
            l.name = n.name;
            l.tid = n.tid;
          

        }




        private List<BillerTypeEx> Transform(IQueryable<billerType> aBillerType)
        {
            List<BillerTypeEx> l = new List<BillerTypeEx>();
            foreach (billerType b in aBillerType)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
