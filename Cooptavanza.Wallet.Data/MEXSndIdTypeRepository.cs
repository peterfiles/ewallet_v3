﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXSndIdTypeRepository : IMEXSndIdTypeRepository
    {
        readonly IRepositoryEF<MEX_sndIdType> repo;
        readonly CW_EntitiesAzure db;

        public MEXSndIdTypeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_sndIdType>(db);

        }

      
        public MEXSndIdTypeEx Get(int Id)
        {
            var n = db.MEX_sndIdType.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXSndIdTypeEx Transform(MEX_sndIdType n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXSndIdTypeEx();
            l.id = n.id;
            l.status = n.status;

            return l;
        }

        private MEX_sndIdType Transform(MEXSndIdTypeEx n)
        {
            MEX_sndIdType l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.status = n.status;
            return l;
        }

        public List<MEXSndIdTypeEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(status.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_sndIdType.Where(sWhere).OrderBy(orderby) : db.MEX_sndIdType.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

     
        
        private List<MEXSndIdTypeEx> Transform(IQueryable<MEX_sndIdType> o)
        {
            List<MEXSndIdTypeEx> l = new List<MEXSndIdTypeEx>();
            foreach (MEX_sndIdType b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }

      
    }
}
