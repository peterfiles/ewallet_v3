﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;
using System.Data;

namespace Cooptavanza.Wallet.Data
{
    public class UserServicesRepository : IUserServicesRepository
    {
        readonly IRepositoryEF<userService> repo;
        
        readonly CW_EntitiesAzure db;

        public UserServicesRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<userService>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.userServices.Where(sWhere).Count() : db.userServices.Count();
        }

        public UserServicesEx Create()
        {
            return new UserServicesEx();
        }

        public UserServicesEx Get(int Id)
        {
            var n = db.userServices.Where(c => c.ID == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public List<UserServicesEx> GetByUserID(int Id)
        {
      
            var n = db.userServices.Where(c => c.user_id == Id);
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public List<UserServicesEx> GetByUserIDAndRoleID(int Id, int roleID)
        {

            var n = db.userServices.Where(c => c.user_id == Id && c.roles_id == roleID);
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public UserServicesEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private UserServicesEx Transform(userService n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new UserServicesEx();
            l.ID = n.ID;
            l.user_id = n.user_id;
            l.roles_id = n.roles_id;
            l.product_id = n.product_id;
            return l;
        }

        private userService Transform(UserServicesEx n)
        {
            userService l;

            if (n.ID > 0)
            {
                l = repo.Query().Where(v => v.ID == n.ID).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.ID = n.ID;
            l.user_id = n.user_id;
            l.roles_id = n.roles_id;
            l.product_id = n.product_id;

            return l;
        }

        public List<UserServicesEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.userServices.Where(sWhere).OrderBy(orderby) : db.userServices.OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
           }
           else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(UserServicesEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(UserServicesEx item)
        {
            var g = Transform(item);
            if (g.ID == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.ID = g.ID;
        }


      

        private List<UserServicesEx> Transform(IQueryable<userService> aUserAccount)
        {
            List<UserServicesEx> l = new List<UserServicesEx>();
            foreach (userService b in aUserAccount)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public UserServicesEx GetAccountByUserId(int r, int id, string tid)
        {
            var n = db.userServices.Where(c => c.roles_id == r && c.user_id == id).FirstOrDefault();
            if (n != null) {
                return Transform(n);
            }return null;
        }

        public UserServicesEx GetAuthentication(int v, string password, string tid, int id)
        {
            var n = db.userServices.Where(c => c.roles_id == v && c.user_id == id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }


        public List<MemberEx> GetAllMember(string str, int psize, int pno)
        {
            return new SPConnection(db).SP_getAllMember(str, psize, pno);
        }

        public int countmember(string str)
        {
            return new SPConnection(db).SP_getAllMember_count(str);
        }

   


    }
}
