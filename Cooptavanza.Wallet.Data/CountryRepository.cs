﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class CountryRepository : ICountryRepository
    {
        readonly IRepositoryEF<country> repo;
        readonly CW_EntitiesAzure db;
        readonly SPConnection sp;

        public CountryRepository(CW_EntitiesAzure db, SPConnection sp) {
            this.db = db;
            this.repo = new IRepositoryEF<country>(db);
            this.sp = sp;

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.countries.Where(sWhere).Where(x => x.id > 0).Count() : db.countries.Where(x => x.id > 0).Count();
        }

        public CountryEx Create()
        {
            return new CountryEx();
        }

        public CountryEx Get(int Id)
        {
            var n = db.countries.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private CountryEx Transform(country n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CountryEx();
            l.id = n.id;
            l.id_code = n.id_code;
            l.iso = n.iso;
            l.name = n.name;
            l.commonname = n.commonname;
            l.iso3 = n.iso3;
            l.numcode = n.numcode;
            l.phonecode = n.phonecode;
            l.salestaxrate = n.salestaxrate;
            l.currencycode = n.currencycode;
            return l;
        }

        private country Transform(CountryEx n)
        {
            country l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.id_code = n.id_code;
            l.iso = n.iso;
            l.name = n.name;
            l.commonname = n.commonname;
            l.iso3 = n.iso3;
            l.numcode = n.numcode;
            l.phonecode = n.phonecode;
            l.salestaxrate = n.salestaxrate;
            l.currencycode = n.currencycode;
            return l;
        }

        public List<CountryEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.countries.Where(sWhere).Where(x => x.id > 0).OrderBy(orderby) : db.countries.Where(x => x.id > 0).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
           {
                return Transform(r);
            }
            else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<CountryEx> GetDefault()
        {
            return new SPConnection(db).sp_getDefaultCountry();
        }

        public List<CountryEx> SearchCountry(string where, string orderBy = "name")
        {
            string aWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere = "name.Contains(\"" + where + "\")";//||Code.Contains(\"" + where + "\")";
            }
            var r = "" != aWhere ? repo.Query().Where(aWhere).Where(x => x.id > 0).OrderBy(orderBy) : repo.Query().Where(x => x.id > 0).OrderBy(orderBy);
            return Transform(r);
        }

        public void Remove(CountryEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CountryEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.id_code = g.id_code;
            item.iso = g.iso;
            item.name = g.name;
            item.commonname = g.commonname;
            item.iso3 = g.iso3;
            item.numcode = g.numcode;
            item.phonecode = g.phonecode;
            item.salestaxrate = g.salestaxrate;
            item.currencycode = g.currencycode;
        }


        public CountryEx GetByCurrencyCode(string curr)
        {
            var d = db.countries.Where(x => x.currencycode.Equals(curr)).FirstOrDefault();
            return Transform(d);
        }
        

        private List<CountryEx> Transform(IQueryable<country> aPerson)
        {
            List<CountryEx> l = new List<CountryEx>();
            foreach (country b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<CountryEx> GetCountries()
        {
            return Transform(db.countries.Where(x => x.id != 0));
        }

        public object getCountryFilterByAgent()
        {
            object data = sp.getCountryFilterByAgent();
            return data;
        }

        public CountryEx GetByCountryName(string cname)
        {
            var res = db.countries.Where(x => x.commonname.Equals(cname)).FirstOrDefault();
            return Transform(res);
        }
    }
}
