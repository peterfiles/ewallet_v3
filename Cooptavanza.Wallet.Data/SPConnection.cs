﻿using Cooptavanza.Wallet;
using Cooptavanza.Wallet.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

public class SPConnection
{
    CW_EntitiesAzure db;
    public SPConnection(CW_EntitiesAzure db)
    {
        this.db = db;
    }

    public string SP_getAccountNumberEnrolled(int userId, int currencyid)
    {
        string accountnumber = "";
        SqlDataReader readValue;

        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_GetAccountNumberEnrolled", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@idcode", userId);
                SqlParameter par2 = new SqlParameter("@currencyid", currencyid);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                //cmd.ExecuteNonQuery();
                readValue = cmd.ExecuteReader();
                readValue.Read();
                var s = readValue[0];
                connection.Close();
                accountnumber = "" + s;
            }
        }
        return accountnumber;
    }

    public string SP_getAccountNumber(int roles, int idcode)
    {
        string accountnumber = "";
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_GetAccountNumber", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@roles", roles);
                SqlParameter par2 = new SqlParameter("@idcode", idcode);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                //cmd.ExecuteNonQuery();
                readValue = cmd.ExecuteReader();
                readValue.Read();
                var s = readValue[0];
                connection.Close();
                accountnumber = "" + s;
            }
        }
        return accountnumber;
    }

    public List<MemberEx> SP_getAllMember(string str, int psize, int pno)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getmember", connection))
            {
                List<MemberEx> s = new List<MemberEx>();

                connection.Open();
                SqlParameter par1 = new SqlParameter("@pagenumber", pno);
                SqlParameter par2 = new SqlParameter("@pagesize", psize);
                SqlParameter par3 = new SqlParameter("@searchstr", str);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                MemberEx me;

                while (readValue.Read())
                {
                    me = new MemberEx();
                    me.id = int.Parse(readValue["id"].ToString());
                    me.firstname = readValue["firstname"].ToString();
                    me.lastname = readValue["lastname"].ToString();
                    me.mobilenumber = readValue["mobilenumber"].ToString();
                    me.tid = readValue["tid"].ToString();
                    me.address = readValue["address"].ToString();
                    me.email = readValue["email"].ToString();
                    me.designation = readValue["designation"].ToString();
                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
       
    }

    public List<BalanceEx> SP_getBalance(int id)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            connection.Open();
            using (SqlCommand cmd = new SqlCommand("sp_getBalance", connection))
            {
                List<BalanceEx> s = new List<BalanceEx>();

                SqlParameter par1 = new SqlParameter("@userid", id);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);


                readValue = cmd.ExecuteReader();

                BalanceEx me;

                while (readValue.Read())
                {
                    me = new BalanceEx();
                    me.currency = readValue["currency"].ToString();
                    me.balance = decimal.Parse(readValue["balance"].ToString());

                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
    }

    public List<BalanceEx> SP_getBalancePending(int id)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBalance_Pending", connection))
            {
                List<BalanceEx> s = new List<BalanceEx>();

                connection.Open();
                SqlParameter par1 = new SqlParameter("@userid", id);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);


                readValue = cmd.ExecuteReader();

                BalanceEx me;

                while (readValue.Read())
                {
                    me = new BalanceEx();
                    me.currency = readValue["currency"].ToString();
                    me.balance = decimal.Parse(readValue["balance"].ToString());

                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
    }

    public List<BalanceEx> SP_getBalanceOverall(int id)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBalance_Overall", connection))
            {
                List<BalanceEx> s = new List<BalanceEx>();

                connection.Open();
                SqlParameter par1 = new SqlParameter("@userid", id);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);


                readValue = cmd.ExecuteReader();

                BalanceEx me;

                while (readValue.Read())
                {
                    me = new BalanceEx();
                    me.currency = readValue["currency"].ToString();
                    me.balance = decimal.Parse(readValue["balance"].ToString());

                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
    }

    public CustomerEx SP_authenticate(string user, string password)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_authenticate", connection))
            {


                connection.Open();
                SqlParameter par1 = new SqlParameter("@password", password);
                SqlParameter par2 = new SqlParameter("@user", user);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);


                readValue = cmd.ExecuteReader();

                CustomerEx me = new CustomerEx(); ;

                while (readValue.Read())
                {

                    me.id = int.Parse(readValue["id"].ToString());
                    me.tid = readValue["tid"].ToString();
                    me.username = readValue["username"].ToString();
                    me.firstname = readValue["firstname"].ToString();
                    me.lastname = readValue["lastname"].ToString();
                    me.email = readValue["email"].ToString();
                    me.mobilenumber = readValue["mobilenumber"].ToString();
                    me.theme = readValue["theme"].ToString();
                    me.themeid = int.Parse(readValue["themeid"].ToString());
                    if (me.id > 0)
                    {
                        me.ImageProfile = new ImageProfileRepository(db).GetByUserId(me.id);
                    }


                }


                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();


                connection.Close();
                return me;
                //    member = s;
            }
        }
    }

    public int SP_getAllMember_count(string str)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_GetMember_count", connection))
            {

                int r = 0;
                connection.Open();

                SqlParameter par3 = new SqlParameter("@searchstr", str);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                while (readValue.Read()) {
                    r = int.Parse(readValue["count"].ToString());
                }
                connection.Close();
                return r;
            }
        }
    }

    public object getCountryFilterByAgent()
    {
        SqlDataReader readValue;
        List<object> data = new List<object>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getCountryFilterByAgent", connection)) {
                connection.Open();
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {
                    data.Add(new {
                        id = readValue["countryID"].ToString(),
                        Name = readValue["countryName"].ToString()
                    });
                }
                connection.Close();
            }
        }
        return data;
    }

    public object getAgentStatesByCountryID(int countryID)
    {
        SqlDataReader readValue;
        List<object> data = new List<object>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getAgentStatesByCountryID", connection))
            {
                connection.Open();
                SqlParameter param = new SqlParameter("@countryID", countryID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(param);
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {
                    data.Add(new
                    {
                        id = readValue["stateID"].ToString(),
                        Name = readValue["stateName"].ToString()
                    });
                }
                connection.Close();
            }
        }
        return data;
    }

    public object GetTransactionForMoneyTransferByTrackingNumberAndPassword(string trackingNum, string pass)
    {
        SqlDataReader readValue;
        object retData = null;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetTransactionForMoneyTransferByTrackingNumberAndPassword", connection))
            {
                connection.Open();
                SqlParameter params1 = new SqlParameter("@trackNum", trackingNum);
                SqlParameter params2 = new SqlParameter("@pass", pass);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(params1);
                cmd.Parameters.Add(params2);
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {
                    retData = new
                    {
                        moneyTransferID = readValue["id"],
                        moneyTrackingNumber = readValue["trackingnumber"],
                        status = readValue["status"],
                        /*Sender*/
                        sfirsname = readValue["sfirstname"],
                        smiddlename = readValue["smiddlename"],
                        slastname = readValue["slastname"],
                        saddress = readValue["saddress"],
                        scity = readValue["scity"],
                        scountry = readValue["scountry"],
                        sstateid = readValue["sstate"],
                        szipcode = readValue["szipcode"],
                        smobilenumber = readValue["smobilenumber"],
                        semail = readValue["semail"],
                        /*Reciever*/
                        rfirstname = readValue["rfirstname"],
                        rmiddlename = readValue["rmiddlename"],
                        rlastname = readValue["rlastname"],
                        raddress = readValue["raddress"],
                        rcity = readValue["rcity"],
                        rcountryid = readValue["rcountry"],
                        rstateid = readValue["rstate"],
                        rzipcode = readValue["rzipcode"],
                        rmobilenumber = readValue["rmobilenumber"],
                        remail = readValue["remail"],
                        rcurrencyToReceive = readValue["rcurrencyToReceive"],
                        ramountToReceive = readValue["ramountToReceive"]
                    };
                }
                connection.Close();
                return retData;
            }
        }

    }

    public object GetTransactionForMoneyTransferByTrackingNumberAndPasswordWalkin(string trackingNum, string pass)
    {
        SqlDataReader readValue;
        object retData = null;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_GetTransactionForMoneyTransferByTrackingNumberAndPasswordWalkin", connection))
            {
                connection.Open();
                SqlParameter params1 = new SqlParameter("@trackNum", trackingNum);
                SqlParameter params2 = new SqlParameter("@pass", pass);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(params1);
                cmd.Parameters.Add(params2);
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {
                    retData = new
                    {
                        moneyTransferID = readValue["id"],
                        moneyTrackingNumber = readValue["trackingnumber"],
                        status = readValue["status"],
                        /*Sender*/
                        sfirsname = readValue["sfirstname"],
                        smiddlename = readValue["smiddlename"],
                        slastname = readValue["slastname"],
                        saddress = readValue["saddress"],
                        scity = readValue["scity"],
                        scountry = readValue["scountry"],
                        sstateid = readValue["sstate"],
                        szipcode = readValue["szipcode"],
                        smobilenumber = readValue["smobilenumber"],
                        semail = readValue["semail"],
                        /*Reciever*/
                        rfirstname = readValue["rfirstname"],
                        rmiddlename = readValue["rmiddlename"],
                        rlastname = readValue["rlastname"],
                        raddress = readValue["raddress"],
                        rcity = readValue["rcity"],
                        rcountryid = readValue["rcountry"],
                        rstateid = readValue["rstate"],
                        rzipcode = readValue["rzipcode"],
                        rmobilenumber = readValue["rmobilenumber"],
                        remail = readValue["remail"],
                        rcurrencyToReceive = readValue["rcurrencyToReceive"],
                        ramountToReceive = readValue["ramountToReceive"]
                    };
                }
                connection.Close();
                return retData;
            }
        }

    }



    public AuthGlobalEx SP_Authenticate_Global(string user, string password)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_authenticate_global", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@password", password);
                SqlParameter par2 = new SqlParameter("@username", user);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);

                readValue = cmd.ExecuteReader();

                AuthGlobalEx me = new AuthGlobalEx(); ;

                while (readValue.Read())
                {

                    me.id = int.Parse(readValue["id"].ToString());
                    me.tid = readValue["tid"].ToString();
                    me.role = int.Parse(readValue["roles"].ToString());
                }

                connection.Close();
                return me;
            }
        }

    }

    public AuthGlobalEx SP_Authenticate_Global(string user, string password, string corporatename)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_authenticate_global_with_corporation", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@password", password);
                SqlParameter par2 = new SqlParameter("@username", user);
                SqlParameter par3 = new SqlParameter("@corporation", corporatename);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                AuthGlobalEx me = new AuthGlobalEx(); ;

                while (readValue.Read())
                {

                    me.id = int.Parse(readValue["id"].ToString());
                    me.tid = readValue["tid"].ToString();
                    me.role = int.Parse(readValue["roles"].ToString());
                }

                connection.Close();
                return me;
            }
        }

    }

    public List<GetTypeTransactionEx> GetAllTransactionType()
    {
        SqlDataReader readValue;
        List<GetTypeTransactionEx> retData = new List<GetTypeTransactionEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getAllTransactionType", connection))
            {
                connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {
                    retData.Add(new GetTypeTransactionEx() {
                        tType = readValue["transactionType"].ToString() }
                    );
                }
                connection.Close();
                return retData;
            }
        }

    }

    public List<GetTransactionListByTypeEx> GetTransactionsByTransactionType(string transactionType, string status)
    {
        SqlDataReader readValue;
        List<GetTransactionListByTypeEx> retData = new List<GetTransactionListByTypeEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getTransactionsByTransactionType", connection))
            {
                connection.Open();
                SqlParameter params1 = new SqlParameter("@tType", transactionType);
                SqlParameter params2 = new SqlParameter("@status", status);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(params1);
                cmd.Parameters.Add(params2);
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {
                    retData.Add(new GetTransactionListByTypeEx()
                    {
                        transactionType = readValue["transactionType"].ToString(),
                        dataCreated = Convert.ToDateTime(readValue["dataCreated"]),
                        CustomerFullName = readValue["CustomerFullName"].ToString(),
                        balance = Convert.ToDecimal(readValue["balance"]),
                        description = readValue["description"].ToString(),
                        status = readValue["status"].ToString(),
                        currencyfromiso = readValue["currencyfromiso"].ToString(),
                        currencytoiso = readValue["currencytoiso"].ToString(),
                        ReceiverFullname = readValue["ReceiverFullname"].ToString(),
                    });
                }

                connection.Close();
                return retData;
            }
        }

    }

    public List<OTPAccountEx> sp_GetOTPRecord(string username)
    {
        SqlDataReader readValue;
        List<OTPAccountEx> retData = new List<OTPAccountEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getOTPRecord", connection))
            {
                connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter par1 = new SqlParameter("@username", username);
                cmd.Parameters.Add(par1);
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {

                    retData.Add(new OTPAccountEx()
                    {
                        deviceId = readValue["deviceId"].ToString(),
                        id = int.Parse(readValue["id"].ToString()),
                        userId = int.Parse(readValue["userId"].ToString()),
                        userRoles = int.Parse(readValue["userRoles"].ToString()),
                        dateTime = DateTime.Parse(readValue["dateTime"].ToString()),
                        mobileUTC = DateTime.Parse(readValue["mobileUTC"].ToString())

                    });

                }
                connection.Close();
                return retData;
            }
        }

    }
    public object sp_getTransactionsByTransactionIDcustom(int transactionID)
    {
        SqlDataReader readValue;
        object retData = new object();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getTransactionsByTransactionIDcustom", connection))
            {
                connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter par1 = new SqlParameter("@transactid", transactionID);
                cmd.Parameters.Add(par1);
                readValue = cmd.ExecuteReader();
                while (readValue.Read())
                {

                    //retData.Add(new OTPAccountEx()
                    //{
                    //    deviceId = readValue["deviceId"].ToString(),
                    //    id = int.Parse(readValue["id"].ToString()),
                    //    userId = int.Parse(readValue["userId"].ToString()),
                    //    userRoles = int.Parse(readValue["userRoles"].ToString()),
                    //    dateTime = DateTime.Parse(readValue["dateTime"].ToString()),
                    //    mobileUTC = DateTime.Parse(readValue["mobileUTC"].ToString())

                    //});

                }
                connection.Close();
                return retData;
            }
        }
    }

    public string SP_getAccountNumberEnrolledForCoopRed(int userId, int currencyid, int roleID, int coopEnrolledID)
    {
        string accountnumber = "";
        SqlDataReader readValue;

        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_GetAccountNumberEnrolledForCoopRed", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@idcode", userId);
                SqlParameter par2 = new SqlParameter("@currencyid", currencyid);
                SqlParameter par3 = new SqlParameter("@roleID", roleID);
                SqlParameter par4 = new SqlParameter("@coopEnrolledID", coopEnrolledID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);
                cmd.Parameters.Add(par4);
                //cmd.ExecuteNonQuery();
                readValue = cmd.ExecuteReader();
                readValue.Read();
                var s = readValue[0];
                connection.Close();
                accountnumber = "" + s;
            }
        }
        return accountnumber;
    }
    

    public List<AccountListEx> sp_getCoopAccountList(int userID, int roleID)
    {
        SqlDataReader readValue;
        List<AccountListEx> compiled = new List<AccountListEx>();
        object c = new object();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getCurrentAmount", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@userID", userID);
                SqlParameter par2 = new SqlParameter("@roleID", roleID);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                
                readValue = cmd.ExecuteReader();

                AccountListEx me;
                while (readValue.Read())
                {
                    me = new AccountListEx();
                    me.amount = Convert.ToDecimal(readValue["remainingBalance"].ToString());
                    me.currencyAccountTo = readValue["givercurrencyAccountTo"].ToString();
                    me.name = readValue["currencyName"].ToString();

                    compiled.Add(me);
                }
                // c = compiled;
            }
        }
        return compiled;
    }

    public List<TransactionEx> GetByPartnerIDAndTransactionType(int userID, int roleID, int PageNo, int PageSize, string ttype)
    {
        SqlDataReader readValue;
        List<TransactionEx> compiled = new List<TransactionEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getEtransferTransactionByType", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@userID", userID);
                SqlParameter par2 = new SqlParameter("@userRoles", roleID);
                SqlParameter par3 = new SqlParameter("@pagenumber", PageNo);
                SqlParameter par4 = new SqlParameter("@pagesize", PageSize);
                SqlParameter par5 = new SqlParameter("@ttype", ttype);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);
                cmd.Parameters.Add(par4);
                cmd.Parameters.Add(par5);

                readValue = cmd.ExecuteReader();

                TransactionEx me;
                while (readValue.Read())
                {
                    me = new TransactionEx();
                    me.id = Convert.ToInt32(readValue["id"].ToString());
                    me.transactionType = readValue["transactionType"].ToString();
                    me.dataCreated = Convert.ToDateTime(readValue["dataCreated"].ToString());
                    me.debit = Convert.ToBoolean(readValue["debit"].ToString());
                    me.credit = Convert.ToBoolean(readValue["credit"].ToString());
                    me.userId = Convert.ToInt32(readValue["userId"].ToString());
                    me.userTID = readValue["userTID"].ToString();
                    me.transactionTID = readValue["transactionTID"].ToString();
                    me.balance = Convert.ToDecimal(readValue["balance"].ToString());
                    me.description = readValue["description"].ToString();
                    me.status = readValue["status"].ToString();
                    me.currencyfromiso = readValue["currencyfromiso"].ToString();
                    me.currencytoiso = readValue["currencytoiso"].ToString();
                    me.enrolledAccountsID = Convert.ToInt32(readValue["enrolledAccountsID"].ToString());
                    me.enrolledAccountsCurrencyID = Convert.ToInt32(readValue["enrolledAccountsCurrencyID"].ToString());
                    me.accountNumber = readValue["accountNumber"].ToString();
                    me.receiversID = Convert.ToInt32(readValue["receiversID"].ToString());
                    me.partnerId = Convert.ToInt32(readValue["partnerId"].ToString());

                    compiled.Add(me);
                }
            }
        }
        return compiled;
    }

    public List<CoopTransactionListEx> sp_getCoopTransactionList(string str, int psize, int pno, int userID, int roleID)
    {
        SqlDataReader readValue;
        List<CoopTransactionListEx> compiled = new List<CoopTransactionListEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getTransactionList", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@userID", userID);
                SqlParameter par2 = new SqlParameter("@roleID", roleID);
                SqlParameter par3 = new SqlParameter("@pagenumber", pno);
                SqlParameter par4 = new SqlParameter("@pagesize", psize);
                SqlParameter par5 = new SqlParameter("@searchstr", str);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);
                cmd.Parameters.Add(par4);
                cmd.Parameters.Add(par5);

                readValue = cmd.ExecuteReader();

                CoopTransactionListEx me;
                while (readValue.Read())
                {
                    me = new CoopTransactionListEx();
                    me.id = Convert.ToInt32(readValue["id"].ToString());
                    me.memberTypeOrigin = readValue["memberTypeOrigin"].ToString();
                    me.memberTypeOriginName = readValue["memberTypeOriginName"].ToString();
                    me.transactionType = readValue["transactionType"].ToString();
                    me.memberTypeReceiver = readValue["memberTypeReceiver"].ToString();
                    me.memberTypeReceiverName = readValue["memberTypeReceiverName"].ToString();
                    me.currencyName = readValue["currencyName"].ToString();
                    me.currencyISO = readValue["currencyISO"].ToString();
                    me.amount = Convert.ToDecimal(readValue["amount"].ToString());
                    me.transactionTID = readValue["transactionTID"].ToString();

                    compiled.Add(me);
                }
            }
        }
        return compiled;
    }

    public List<MemberEx> SP_getAllMemberByParentID(string str, int psize, int pno, int userID)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getallmemberfilterByParentID", connection))
            {
                List<MemberEx> s = new List<MemberEx>();

                connection.Open();
                SqlParameter par1 = new SqlParameter("@pagenumber", pno);
                SqlParameter par2 = new SqlParameter("@pagesize", psize);
                SqlParameter par3 = new SqlParameter("@searchstr", str);
                SqlParameter par4 = new SqlParameter("@parentID", userID);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);
                cmd.Parameters.Add(par4);

                readValue = cmd.ExecuteReader();

                MemberEx me;

                while (readValue.Read())
                {
                    me = new MemberEx();
                    me.id = int.Parse(readValue["id"].ToString());
                    me.firstname = readValue["firstname"].ToString();
                    me.lastname = readValue["lastname"].ToString();
                    me.mobilenumber = readValue["mobilenumber"].ToString();
                    me.tid = readValue["tid"].ToString();
                    me.address = readValue["address"].ToString();
                    me.email = readValue["email"].ToString();
                    me.designation = readValue["designation"].ToString();
                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
    }

    public List<MemberEx> SP_getAllMemberByParentIDAndRoleID(string str, int psize, int pno, int userID, int roleID)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getallmemberfilterByParentIDAndRole", connection))
            {
                List<MemberEx> s = new List<MemberEx>();

                connection.Open();
                SqlParameter par1 = new SqlParameter("@pagenumber", pno);
                SqlParameter par2 = new SqlParameter("@pagesize", psize);
                SqlParameter par3 = new SqlParameter("@searchstr", str);
                SqlParameter par4 = new SqlParameter("@parentID", userID);
                SqlParameter par5 = new SqlParameter("@memberRole", roleID);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);
                cmd.Parameters.Add(par4);
                cmd.Parameters.Add(par5);

                readValue = cmd.ExecuteReader();

                MemberEx me;

                while (readValue.Read())
                {
                    me = new MemberEx();
                    me.id = int.Parse(readValue["id"].ToString());
                    me.firstname = readValue["firstname"].ToString();
                    me.lastname = readValue["lastname"].ToString();
                    me.mobilenumber = readValue["mobilenumber"].ToString();
                    me.tid = readValue["tid"].ToString();
                    me.address = readValue["address"].ToString();
                    me.email = readValue["email"].ToString();
                    me.designation = readValue["designation"].ToString();
                    me.accountnumber = readValue["accountnumber"].ToString();
                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
       
    }

    public int SP_getMember_count_filterByParentIDAndRoleID(string str, int parentId, int userId)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getcountmemberfilterByParentIDAndRole", connection))
            {

                int r = 0;
                connection.Open();

                SqlParameter par1 = new SqlParameter("@searchstr", str);
                SqlParameter par2 = new SqlParameter("@parentID", parentId);
                SqlParameter par3 = new SqlParameter("@memberRole", userId);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                while (readValue.Read())
                {
                    r = int.Parse(readValue["count"].ToString());
                }
                connection.Close();
                return r;
            }
        }
    }

    public int SP_getMember_count_filterByParentID(string str, int parentId)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getcountMemberfilterByParentID", connection))
            {

                int r = 0;
                connection.Open();

                SqlParameter par1 = new SqlParameter("@searchstr", str);
                SqlParameter par2 = new SqlParameter("@parentID", parentId);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);

                readValue = cmd.ExecuteReader();

                while (readValue.Read())
                {
                    r = int.Parse(readValue["count"].ToString());
                }
                connection.Close();
                return r;
            }
        }
    }

    public int SP_sp_getcountTransactionListUserIDAndRoleID(string str, int userID, int roleID)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getcountTransactionList", connection))
            {

                int r = 0;
                connection.Open();

                SqlParameter par1 = new SqlParameter("@searchstr", str);
                SqlParameter par2 = new SqlParameter("@userID", userID);
                SqlParameter par3 = new SqlParameter("@roleID", roleID);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                while (readValue.Read())
                {
                    r = int.Parse(readValue["count"].ToString());
                }
                connection.Close();
                return r;
            }
        }
    }

    public List<BankDepositEx> Sp_GetBankDepositorByCountryID(int countryID)
    {
        SqlDataReader readValue;
        List<BankDepositEx> bankList = new List<BankDepositEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBankDepositGetByCountryID", connection))
            {
                connection.Open();

                SqlParameter par1 = new SqlParameter("@countryID", countryID);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(par1);

                readValue = cmd.ExecuteReader();
                BankDepositEx r;
                while (readValue.Read())
                {
                    r = new BankDepositEx();
                    r.bankName = readValue["bankname"].ToString();
                    r.rtcode = readValue["rtcode"].ToString();
                    bankList.Add(r);
                }
                connection.Close();
                return bankList;
            }
        }
    }


    public List<BankDepositEx> Sp_GetBankDepositorByCountryIDAndBankName(int countryID, string bankName)
    {
        SqlDataReader readValue;
        List<BankDepositEx> bankList = new List<BankDepositEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBankDepositGetByCountryIDAndBankName", connection))
            {
                connection.Open();

                SqlParameter par1 = new SqlParameter("@countryID", countryID);
                SqlParameter par2 = new SqlParameter("@bankName", bankName);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);

                readValue = cmd.ExecuteReader();
                BankDepositEx r;
                while (readValue.Read())
                {
                    r = new BankDepositEx();
                    r.currenciesISO = readValue["currenciesISO"].ToString();
                    r.currenciesID = Convert.ToInt32(readValue["currenciesID"].ToString());
                    bankList.Add(r);
                }
                connection.Close();
                return bankList;
            }
        }
    }

    public List<BankDepositEx> Sp_getBankDepositCountry()
    {
        SqlDataReader readValue;
        List<BankDepositEx> bankList = new List<BankDepositEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBankDepositCountry", connection))
            {
                connection.Open();
                
                readValue = cmd.ExecuteReader();
                BankDepositEx r;
                while (readValue.Read())
                {
                    r = new BankDepositEx();
                    r.countryName = readValue["commonname"].ToString();
                    r.countryid = Convert.ToInt32(readValue["countryid"].ToString());
                    bankList.Add(r);
                }
                connection.Close();
                return bankList;
            }
        }
    }

    public List<BankDepositEx> SP_getAllBankDepositorListGetPaged(string str, int psize, int pno)
    {
        SqlDataReader readValue;

        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBankDepositorGetPagedList", connection))
            {

                List<BankDepositEx> s = new List<BankDepositEx>();

                connection.Open();
                SqlParameter par1 = new SqlParameter("@pagenumber", pno);
                SqlParameter par2 = new SqlParameter("@pagesize", psize);
                SqlParameter par3 = new SqlParameter("@searchstr", str);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                BankDepositEx me;

                while (readValue.Read())
                {
                    me = new BankDepositEx();
                    me.id = Convert.ToInt32(readValue["id"].ToString());
                    me.userRole = Convert.ToInt32(readValue["userRole"].ToString());
                    me.dateCreated = Convert.ToDateTime(readValue["dateCreated"].ToString());
                    me.countryid = Convert.ToInt32(readValue["countryid"].ToString());
                    me.bankName = readValue["bankName"].ToString();
                    me.currenciesISO = readValue["currenciesISO"].ToString();
                    me.currenciesID = Convert.ToInt32(readValue["currenciesID"].ToString());
                    me.memberTypeName = readValue["userMemberType"].ToString();
                    me.countryName = readValue["countryName"].ToString();
                    me.rtcode = readValue["rtcode"].ToString();
                    s.Add(me);
                }

                //gvGrid.DataSource = TestList;
                //gvGrid.DataBind();

                var r = s;
                connection.Close();
                return s;
                //    member = s;
            }
        }
    }

    public int SP_getAllBankDepositorListGetPagedCount(string str)
    {
        SqlDataReader readValue;
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {

            using (SqlCommand cmd = new SqlCommand("sp_getBankDepositorGetPagedListCount", connection))
            {
                int s = 0;

                connection.Open();
                SqlParameter par1 = new SqlParameter("@searchstr", str);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);

                readValue = cmd.ExecuteReader();
                
                while (readValue.Read())
                {
                    s = Convert.ToInt32(readValue["count"].ToString());
                }

                var r = s;
                connection.Close();
                return s;
            }
        }
    }

    public CoopEnrolledAccountEx sp_getCoopEnrolledAccount(int userID, int roleID, string iso)
    {
        SqlDataReader readValue;
        CoopEnrolledAccountEx compiled = new CoopEnrolledAccountEx();
        object c = new object();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getCoopEnrolledAccount", connection))
            {
                connection.Open();
                SqlParameter par1 = new SqlParameter("@userID", userID);
                SqlParameter par2 = new SqlParameter("@roleID", roleID);
                SqlParameter par3 = new SqlParameter("@iso", iso);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(par1);
                cmd.Parameters.Add(par2);
                cmd.Parameters.Add(par3);

                readValue = cmd.ExecuteReader();

                CoopEnrolledAccountEx me;
                while (readValue.Read())
                {
                    me = new CoopEnrolledAccountEx();
                    me.id = Convert.ToInt32(readValue["id"].ToString());
                    me.currency = readValue["currency"].ToString();
                    me.currenycid = Convert.ToInt32(readValue["currenycid"].ToString());
                    me.accountDefault = Convert.ToBoolean(readValue["accountDefault"].ToString());
                    me.userId = Convert.ToInt32(readValue["userId"].ToString());
                    me.userTID = readValue["userTID"].ToString();
                    me.accountnumber = readValue["accountnumber"].ToString();
                    me.roleID = readValue["roleID"].ToString();
                    compiled = me;
                    
                }

                connection.Close();
                // c = compiled;
            }
        }
        return compiled;
    }

    public List<CountryEx> sp_getDefaultCountry()
    {
        SqlDataReader readValue;
        List<CountryEx> compiled = new List<CountryEx>();
        object c = new object();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getDefaultCountry", connection))
            {
                connection.Open();
                
                readValue = cmd.ExecuteReader();

                CountryEx me;
                while (readValue.Read())
                {
                    me = new CountryEx();
                    me.id = Convert.ToInt32(readValue["id"].ToString());
                    me.id_code = Convert.ToInt32(readValue["id_code"].ToString());
                    me.iso = readValue["iso"].ToString();
                    me.name = readValue["name"].ToString();
                    me.commonname = readValue["commonname"].ToString();
                    me.iso3 = readValue["iso3"].ToString();
                    me.numcode = Convert.ToInt32(readValue["numcode"].ToString());
                    me.phonecode = Convert.ToInt32(readValue["phonecode"].ToString());
                    compiled.Add(me);

                }

                connection.Close();
            }
        }
        return compiled;
    }

    public List<ChartsEx> sp_GetChartsList(DateTime d1, DateTime d2)
    {
        SqlDataReader readValue;
        List<ChartsEx> compiled = new List<ChartsEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getChartTransactionByType", connection))
            {
                {
                    connection.Open();

                    SqlParameter p1 = new SqlParameter("@d1", d1);
                    SqlParameter p2 = new SqlParameter("@d2", d2);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(p1);
                    cmd.Parameters.Add(p2);

                    readValue = cmd.ExecuteReader();

                    ChartsEx cEx;
                    while (readValue.Read())
                    {
                        cEx = new ChartsEx();
                        cEx.productName = readValue["productName"].ToString();
                        cEx.transacted = Convert.ToInt32(readValue["transacted"].ToString());
                        compiled.Add(cEx);
                    }

                    connection.Close();
                }
            }
        }
        return compiled;
    }

    public List<TotalCommissionEx> sp_getTotalCommissionByUserIDRoleIDAndMethod(int userID, int roleID, string method)
    {
        SqlDataReader readValue;
        List<TotalCommissionEx> compiled = new List<TotalCommissionEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getTotalCommissionByUserIDAndRoleIDAndMethod", connection))
            {
                {
                    connection.Open();

                    SqlParameter p1 = new SqlParameter("@userID", userID);
                    SqlParameter p2 = new SqlParameter("@userRoles", roleID);
                    SqlParameter p3 = new SqlParameter("@type", method);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(p1);
                    cmd.Parameters.Add(p2);
                    cmd.Parameters.Add(p3);

                    readValue = cmd.ExecuteReader();

                    TotalCommissionEx tcEx;
                    while (readValue.Read())
                    {
                        tcEx = new TotalCommissionEx();
                        tcEx.commissionAccountNumber = readValue["commissionAccountNumber"].ToString();
                        tcEx.currency = readValue["currency"].ToString();
                        tcEx.totalAmount = Convert.ToDecimal(readValue["totalAmount"].ToString());
                        compiled.Add(tcEx);
                    }

                    connection.Close();
                }
            }
        }

        return compiled;
    }

    public List<TotalCommissionByCurrencyEx> GetTotalCommByUidAndRidGroupByCurr(int uID, int rID)
    {
        SqlDataReader readValue;
        List<TotalCommissionByCurrencyEx> compiled = new List<TotalCommissionByCurrencyEx>();
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_getTotalCommissionByUserAndRoleIDGroupByCurrency", connection))
            {
                connection.Open();

                SqlParameter p1 = new SqlParameter("@userID", uID);
                SqlParameter p2 = new SqlParameter("@userRoles", rID);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(p1);
                cmd.Parameters.Add(p2);

                readValue = cmd.ExecuteReader();

                TotalCommissionByCurrencyEx tccEx;
                while (readValue.Read())
                {
                    tccEx = new TotalCommissionByCurrencyEx();
                    tccEx.totalCommission = Convert.ToDecimal(readValue["totalCommission"].ToString());
                    tccEx.currencyISO = readValue["currencyISO"].ToString();
                    compiled.Add(tccEx);
                }
            }
        }
        return compiled;
    }

    public void RefreshConvertedCurrencyOnDB()
    {
        using (SqlConnection connection = new SqlConnection(db.Database.Connection.ConnectionString))
        {
            using (SqlCommand cmd = new SqlCommand("sp_refreshConvertedCurrency", connection))
            {
                connection.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                connection.Close();
            }
        }
    }
}
