﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MoneyTransferTransactionsRepository : IMoneyTransferTransactionsRepository
    {
        readonly IRepositoryEF<moneyTransferTransaction> repo;
        readonly CW_EntitiesAzure db;

        public MoneyTransferTransactionsRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<moneyTransferTransaction>(db);

        }

        public int Count(int id, string TID, string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.moneyTransferTransactions.Where(sWhere).Where(c => c.id == id).Count() : db.moneyTransferTransactions.Where(c => c.id == id).Count();
        }

        public MoneyTransferTransactionsEx Create()
        {
            return new MoneyTransferTransactionsEx();
        }

        public MoneyTransferTransactionsEx Get(int Id)
        {
            var n = db.moneyTransferTransactions.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public MoneyTransferTransactionsEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private MoneyTransferTransactionsEx Transform(moneyTransferTransaction n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MoneyTransferTransactionsEx();
            l.id = n.id;
            l.accountNumber = n.accountNumber;
            l.agentID = n.agentID;
            l.receiversID = n.receiversID;
            l.status = n.status;
            l.transactionID = n.transactionID;
            l.userID = n.userID;
            l.trackingnumber = n.trackingnumber;
            l.idType = n.idType;
            l.idNumber = n.idNumber;
            l.walkin = n.walkin;
            return l;
        }

        private moneyTransferTransaction Transform(MoneyTransferTransactionsEx n)
        {
            moneyTransferTransaction l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.accountNumber = n.accountNumber;
            l.agentID = n.agentID;
            l.receiversID = n.receiversID;
            l.status = n.status;
            l.transactionID = n.transactionID;
            l.userID = n.userID; ;
            l.trackingnumber = n.trackingnumber;
            l.idType = n.idType;
            l.idNumber = n.idNumber;
            l.walkin = n.walkin;
            return l;
        }

        public List<MoneyTransferTransactionsEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.moneyTransferTransactions.Where(sWhere).Where(c => c.id == id ).OrderBy(orderby) : db.moneyTransferTransactions.Where(c => c.id == id).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(MoneyTransferTransactionsEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(MoneyTransferTransactionsEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.accountNumber = g.accountNumber;
            item.agentID = g.agentID;
            item.receiversID = g.receiversID;
            item.status = g.status;
            item.transactionID = g.transactionID;
            item.userID = g.userID; ;
            item.trackingnumber = g.trackingnumber;
            item.idType = g.idType;
            item.idNumber = g.idNumber;
            item.walkin = g.walkin;
        }
        
        private List<MoneyTransferTransactionsEx> Transform(IQueryable<moneyTransferTransaction> aTransaction)
        {
            List<MoneyTransferTransactionsEx> l = new List<MoneyTransferTransactionsEx>();
            foreach (moneyTransferTransaction b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public object getMoneyTransferTransactionsByTrackingNumberAndPassword(string trackingNo, string password)
        {
            bool isWalkin = Convert.ToBoolean(db.moneyTransferTransactions.Where(x => x.trackingnumber.Equals(trackingNo)).FirstOrDefault().walkin);
            if (isWalkin)
            {
                return new SPConnection(db).GetTransactionForMoneyTransferByTrackingNumberAndPasswordWalkin(trackingNo, password);
            }
            else
            {
                return new SPConnection(db).GetTransactionForMoneyTransferByTrackingNumberAndPassword(trackingNo, password);
            }
        }
    }
}
