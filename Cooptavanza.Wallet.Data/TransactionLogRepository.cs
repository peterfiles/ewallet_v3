﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Text;

namespace Cooptavanza.Wallet.Data
{
    public class TransactionLogRepository : ITransactionLogsRepository
    {
        readonly IRepositoryEF<transactionLog> repo;
        readonly CW_EntitiesAzure db;

        public TransactionLogRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<transactionLog>(db);

        }

       
        public TransactionLogEx Create()
        {
            return new TransactionLogEx();
        }

        public TransactionLogEx GetModuleName(string modName) //modName is ROUTE path
        {
            var r = db.transactionLogs.Where(x => x.moduleName.Equals(modName)).FirstOrDefault();
            return Transform(r);
        }

        public void SaveLog(string log, string modName)
        {
            TransactionLogEx l = new TransactionLogEx();
            l.moduleName = modName;
            l.log = log;
            l.logDate = DateTime.Now;
         
            l.status = false;
            //ele.logDate = DateTime.Now;
            Save(l);
        }



        private TransactionLogEx Transform(transactionLog n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new TransactionLogEx();
            l.id = n.id;
            l.log = n.log;
            l.moduleName = n.moduleName;
            l.logDate = n.logDate;
            l.userId = n.userId;
            l.userRole = n.userRole;
            l.status = n.status;
            return l;
        }

        public void Remove(TransactionLogEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        private transactionLog Transform(TransactionLogEx n)
        {
            transactionLog l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.log = n.log;
            l.moduleName = n.moduleName;
            l.logDate = n.logDate;
            l.userId = n.userId;
            l.userRole = n.userRole;
            l.status = n.status;

            return l;
        }

        
        public void Save(TransactionLogEx l)
        {
            var tempid = l.id;
            var n= Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
                
            }
            repo.SaveChanges();
            l.id = n.id;
            l.log = n.log;
            l.moduleName = n.moduleName;
            l.userId = n.userId;
            l.userRole = n.userRole;
            l.status = n.status;
            //  l.logDate = n.logDate;
        }

        public List<TransactionLogEx> GetPaged(string where, string orderBy, int pageNo, int pageSize)
        {
            DateTime startdate = DateTime.Now.AddDays(-3);
            DateTime enddate = DateTime.Now;



            var r = db.transactionLogs.Where(c => c.logDate >= startdate && c.logDate <= enddate && c.status == false).OrderBy(orderBy);
            if (pageNo == 0 || pageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((pageNo - 1) * pageSize).Take(pageSize));
            }
        }

        private List<TransactionLogEx> Transform(IQueryable<transactionLog> aUAA)
        {
            List<TransactionLogEx> l = new List<TransactionLogEx>();
            foreach (transactionLog b in aUAA)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
