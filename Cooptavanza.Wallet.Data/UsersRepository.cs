﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class UsersRepository : IUsersRepository
    {
        readonly IRepositoryEF<user> repo;
        //readonly cooptavanza_ewalletEntities db;
        readonly CW_EntitiesAzure db;
        public UsersRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<user>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.users.Where(sWhere).Count() : db.users.Count();
        }

        public UsersEx Create()
        {
            return new UsersEx();
        }

        public UsersEx Get(int Id)
        {
            var n = db.users.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public UsersEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private UsersEx Transform(user n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new UsersEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.birthdate = n.birthdate;
            l.email = n.email;
            l.country = n.countryid != null ? n.country1.name : "";
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.url = n.url;
            l.state = n.stateid != null ? n.state1.name : "";
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.date_created = n.date_created;
            l.date_update = n.date_update;
            l.password = n.password;
            l.username = n.username;
            l.membertype = n.memberType;
            l.tid = n.tid;
            l.defaultPartner = n.defaultPartner;
            return l;
        }

        private user Transform(UsersEx n)
        {
            user l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.birthdate = n.birthdate;
            l.email = n.email;
            l.country = n.country;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.url = n.url;
            l.state = n.state;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.date_created = n.date_created;
            l.date_update = n.date_update;
            l.password = n.password;
            l.username = n.username;
            l.memberType = n.membertype;
            l.tid = n.tid;
            l.defaultPartner = n.defaultPartner;

            return l;
        }

        public List<UsersEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.users.Where(sWhere).OrderBy(orderby) : db.users.OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
           }
           else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(UsersEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(UsersEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
        }


      

        private List<UsersEx> Transform(IQueryable<user> aUsers)
        {
            List<UsersEx> l = new List<UsersEx>();
            foreach (user b in aUsers)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
