﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class ImageIDCardRepository : IImageIdCardRepository
    {
        readonly IRepositoryEF<imageIDCard> repo;
        readonly CW_EntitiesAzure db;

        public ImageIDCardRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<imageIDCard>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public ImageIdCardEx Create()
        {
            return new ImageIdCardEx();
        }

        public ImageIdCardEx Get(int Id)
        {
            var n = db.imageIDCards.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private ImageIdCardEx Transform(imageIDCard n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ImageIdCardEx();

            l.id = n.id;
            l.userId = n.userId;
            l.imageString = n.imageString;
            l.path = n.path;
            l.idCardNo = n.idCardNo;
            l.idCardType = n.idCardType;
            l.expiryDate = n.expiryDate;
            l.adminID = n.adminID;
            l.adminRoleID = n.adminRoleID;
            l.status = n.status;
            return l;
        }

        private imageIDCard Transform(ImageIdCardEx n)
        {
            imageIDCard l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.userId = n.userId;
            l.imageString = n.imageString;
            l.path = n.path;
            l.idCardNo = n.idCardNo;
            l.idCardType = n.idCardType;
            l.expiryDate = n.expiryDate;
            l.adminID = n.adminID;
            l.adminRoleID = n.adminRoleID;
            l.status = n.status;

            return l;
        }

        public List<ImageIdCardEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.imageIDCards.Where(sWhere).OrderBy(orderby) : db.imageIDCards.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ImageIdCardEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ImageIdCardEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.userId = g.userId;
            item.imageString = g.imageString;
            item.path = g.path;
            item.idCardNo = g.idCardNo;
            item.idCardType = g.idCardType;
            item.expiryDate = g.expiryDate;
            item.adminID = g.adminID;
            item.adminRoleID = g.adminRoleID;
            item.status = g.status;
        }




        private List<ImageIdCardEx> Transform(IQueryable<imageIDCard> aImageIdCard)
        {
            List<ImageIdCardEx> l = new List<ImageIdCardEx>();
            foreach (imageIDCard b in aImageIdCard)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public ImageIdCardEx GetByUserId(int Id)
        {
            try
            {
                var n = db.imageIDCards.Where(c => c.userId == Id).FirstOrDefault();
                if (n != null)
                {
                    return Transform(n);
                }
                else
                {
                    return null;
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

    }

}
