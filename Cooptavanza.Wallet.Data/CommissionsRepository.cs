﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class CommissionsRepository : ICommissionsRepository
    {
        readonly IRepositoryEF<commission> repo;
        readonly CW_EntitiesAzure db;

        public CommissionsRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<commission>(db);

        }

        public CommissionsEx Create()
        {
            return new CommissionsEx();
        }

        public CommissionsEx Get(int Id)
        {
            var n = db.commissions.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private CommissionsEx Transform(commission n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CommissionsEx();

            l.id = n.id;
            l.commissionAccountNumber = n.commissionAccountNumber;
            l.country = n.country;
            l.currency = n.currency;
            l.date = n.date;
            l.localpaymentservice = n.localpaymentservice;
            l.mdr = n.mdr;
            l.method = n.method;
            l.txRate = n.txRate;
            l.txRateFeeCurrency = n.txRateFeeCurrency;
            l.userID = n.userID;
            l.userRole = n.userRole;
            l.userTID = n.userTID;
            l.status = n.status;
            l.transactionTID = n.transactionTID;
            return l;
        }

        private commission Transform(CommissionsEx n)
        {
            commission l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.commissionAccountNumber = n.commissionAccountNumber;
            l.country = n.country;
            l.currency = n.currency;
            l.date = n.date;
            l.localpaymentservice = n.localpaymentservice;
            l.mdr = n.mdr;
            l.method = n.method;
            l.txRate = n.txRate;
            l.txRateFeeCurrency = n.txRateFeeCurrency;
            l.userID = n.userID;
            l.userRole = n.userRole;
            l.userTID = n.userTID;
            l.status = n.status;
            l.transactionTID = n.transactionTID;
            return l;
        }

        public List<CommissionsEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.commissions.Where(sWhere).OrderBy(orderby) : db.commissions.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(CommissionsEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CommissionsEx item)
        {
            var n = Transform(item);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();
            item.id = n.id;
            item.commissionAccountNumber = n.commissionAccountNumber;
            item.country = n.country;
            item.currency = n.currency;
            item.date = n.date;
            item.localpaymentservice = n.localpaymentservice;
            item.mdr = n.mdr;
            item.method = n.method;
            item.txRate = n.txRate;
            item.txRateFeeCurrency = n.txRateFeeCurrency;
            item.userID = n.userID;
            item.userRole = n.userRole;
            item.userTID = n.userTID;
            item.status = n.status;
            item.transactionTID = n.transactionTID;
        }
        
        private List<CommissionsEx> Transform(IQueryable<commission> comm)
        {
            List<CommissionsEx> l = new List<CommissionsEx>();
            foreach (commission b in comm)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<CommissionsEx> GetCommissionListWithFilter(int userID, int userRole, string orderby, int PageNo, int PageSize)
        {
            //List<string> aWhere = new List<string>();
            //string sWhere = "";
            //if (!string.IsNullOrEmpty(where))
            //{
            //    aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            //}
            //if (aWhere.Count > 0)
            //{
            //    sWhere = string.Join(" && ", aWhere);
            //}
            //switch(type)
            //var r = "" != sWhere ? db.commissions.Where(sWhere).OrderBy(orderby) : db.commissions.OrderBy(orderby);
            var r = db.commissions.Where(x => x.userID == userID && x.userRole == userRole).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<CommissionsEx> GetCommissionListWithFilterByUserIDRoleIDMethod(int userID, int userRole, string method, string orderby, int PageNo, int PageSize)
        {
            //List<string> aWhere = new List<string>();
            //string sWhere = "";
            //if (!string.IsNullOrEmpty(where))
            //{
            //    aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            //}
            //if (aWhere.Count > 0)
            //{
            //    sWhere = string.Join(" && ", aWhere);
            //}
            //switch(type)
            //var r = "" != sWhere ? db.commissions.Where(sWhere).OrderBy(orderby) : db.commissions.OrderBy(orderby);
            var r = db.commissions.Where(x => x.userID == userID && x.userRole == userRole && x.method.Equals(method)).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public List<TotalCommissionEx> getTotalCommissionByUserIDRoleIDAndMethod(int userID, int roleID, string method)
        {
            return new SPConnection(db).sp_getTotalCommissionByUserIDRoleIDAndMethod(userID,roleID,method);
        }

        public CommissionsEx GetCommissionAccountNumberByMethodAndCurrency(string currency, string method)
        {
            var d = db.commissions.Where(x => x.currency.Equals(currency) && x.method.Equals(method)).FirstOrDefault();
            return Transform(d);
        }

        public List<TotalCommissionByCurrencyEx> GetTotalCommByUidAndRidGroupByCurr(int uID, int rID)
        {
            return new SPConnection(db).GetTotalCommByUidAndRidGroupByCurr(uID, rID);
        }

    }
}
