﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXIncludeFeesRepository : IMEXIncludeFeesRepository
    {
        readonly IRepositoryEF<MEX_includeFees> repo;
        readonly CW_EntitiesAzure db;

        public MEXIncludeFeesRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_includeFees>(db);

        }

      
        public MEXIncludeFeesEx Get(int Id)
        {
            var n = db.MEX_includeFees.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXIncludeFeesEx Transform(MEX_includeFees n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXIncludeFeesEx();
            l.id = n.id;
            l.status = n.status;

            return l;
        }

        private MEX_includeFees Transform(MEXIncludeFeesEx n)
        {
            MEX_includeFees l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.status = n.status;
            return l;
        }

        public List<MEXIncludeFeesEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(status.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_includeFees.Where(sWhere).OrderBy(orderby) : db.MEX_includeFees.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

     
        
        private List<MEXIncludeFeesEx> Transform(IQueryable<MEX_includeFees> o)
        {
            List<MEXIncludeFeesEx> l = new List<MEXIncludeFeesEx>();
            foreach (MEX_includeFees b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }

      
    }
}
