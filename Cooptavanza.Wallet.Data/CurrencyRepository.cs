﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class CurrencyRepository : ICurrencyRepository
    {
        readonly IRepositoryEF<currency> repo;
        readonly CW_EntitiesAzure db;

        public CurrencyRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<currency>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.currencies.Where(sWhere).Where(x => x.id > 0).Count() : db.currencies.Where(x => x.id > 0).Count();
        }

        public CurrencyEx Create()
        {
            return new CurrencyEx();
        }

        public CurrencyEx Get(int Id)
        {
            var n = db.currencies.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private CurrencyEx Transform(currency n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CurrencyEx();
            l.id = n.id;
            l.iso = n.iso;
            l.name = n.name;
            l.numeric_code = n.numeric_code;
            l.minor_unit = n.minor_unit;
            l.isDefault = n.isDefault;

            return l;
        }

        private currency Transform(CurrencyEx n)
        {
            currency l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.iso = n.iso;
            l.name = n.name;
            l.numeric_code = n.numeric_code;
            l.minor_unit = n.minor_unit;
            l.isDefault = n.isDefault;

            return l;
        }

        public List<CurrencyEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.currencies.Where(sWhere).Where(x => x.id > 0).OrderBy(orderby) : db.currencies.Where(x => x.id > 0).OrderBy(orderby);
           if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
           {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }
        public List<CurrencyEx> GetByDefault(bool defaultValue)
        {
            string where = "";
            string orderby = "name Asc";
            if (defaultValue)
            {
                where = "1";
            }
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(default.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.currencies.Where(sWhere).Where(x => x.id > 0).OrderBy(orderby) : db.currencies.Where(x => x.id > 0).OrderBy(orderby);
          
                return Transform(r);
           
        }

        public void Remove(CurrencyEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CurrencyEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.iso = g.iso;
            item.name = g.name;
            item.numeric_code = g.numeric_code;
            item.minor_unit = g.minor_unit;
            item.isDefault = g.isDefault;
        }
        
        private List<CurrencyEx> Transform(IQueryable<currency> aPerson)
        {
            List<CurrencyEx> l = new List<CurrencyEx>();
            foreach (currency b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<CurrencyEx> GetDefaultValueList()
        {
            var l = db.currencies.Where(x => x.isDefault == true).ToList();
            return Transform(l.AsQueryable());
        }

        public CurrencyEx GetByISO(string iso)
        {
            var l = db.currencies.Where(x => x.iso == iso).FirstOrDefault();
            return Transform(l);
        }
    }
}
