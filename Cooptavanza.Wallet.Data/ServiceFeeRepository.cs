﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class ServiceFeeRepository : IServiceFeeRepository
    {
        readonly IRepositoryEF<serviceFee> repo;
        readonly CW_EntitiesAzure db;

        public ServiceFeeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<serviceFee>(db);

        }

        public int Count(int id, string TID, string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.serviceFees.Where(sWhere).Where(c => c.id == id).Count() : db.serviceFees.Where(c => c.id == id).Count();
        }

        public ServiceFeeEx Create()
        {
            return new ServiceFeeEx();
        }

        public ServiceFeeEx Get(int Id)
        {
            var n = db.serviceFees.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public ServiceFeeEx GetByProductID(int productID)
        {
            var n = db.serviceFees.Where(c => c.productID == productID).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public ServiceFeeEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private ServiceFeeEx Transform(serviceFee n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ServiceFeeEx();
            l.id = n.id;
            l.serviceFeeType = n.serviceFeeType;
            l.serviceFeeAmount = n.serviceFeeAmount;
            l.dateCreated = n.dateCreated;
            l.productID = n.productID;
            l.serviceFeePercentage = n.serviceFeePercentage;
            return l;
        }

        private serviceFee Transform(ServiceFeeEx n)
        {
            serviceFee l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.serviceFeeType = n.serviceFeeType;
            l.serviceFeeAmount = n.serviceFeeAmount;
            l.dateCreated = n.dateCreated;
            l.productID = n.productID;
            l.serviceFeePercentage = n.serviceFeePercentage;

            return l;
        }

        public List<ServiceFeeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.serviceFees.Where(sWhere).Where(c => c.id == id ).OrderBy(orderby) : db.serviceFees.Where(c => c.id == id).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ServiceFeeEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ServiceFeeEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.serviceFeeType = g.serviceFeeType;
            item.serviceFeeAmount = g.serviceFeeAmount;
            item.dateCreated = g.dateCreated;
            item.productID = g.productID;
            item.serviceFeePercentage = g.serviceFeePercentage;
        }

        public ServiceFeeEx GetByServiceFeeType(string name)
        {
            return Transform(db.serviceFees.Where(x => x.serviceFeeType.Equals(name)).FirstOrDefault());
        }
      

        private List<ServiceFeeEx> Transform(IQueryable<serviceFee> aTransaction)
        {
            List<ServiceFeeEx> l = new List<ServiceFeeEx>();
            foreach (serviceFee b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
