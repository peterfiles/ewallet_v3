﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXSndRemitPurposeRepository : IMEXSndRemitPurposeRepository
    {
        readonly IRepositoryEF<MEX_sndRemitPurpose> repo;
        readonly CW_EntitiesAzure db;

        public MEXSndRemitPurposeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_sndRemitPurpose>(db);

        }

      
        public MEXSndRemitPurposeEx Get(int Id)
        {
            var n = db.MEX_sndRemitPurpose.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXSndRemitPurposeEx Transform(MEX_sndRemitPurpose n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXSndRemitPurposeEx();
            l.id = n.id;
            l.code = n.code;
            l.definition = n.definition;

            return l;
        }

        private MEX_sndRemitPurpose Transform(MEXSndRemitPurposeEx n)
        {
            MEX_sndRemitPurpose l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.code = n.code;
            l.definition = n.definition;
            return l;
        }

        public List<MEXSndRemitPurposeEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(status.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_sndRemitPurpose.Where(sWhere).OrderBy(orderby) : db.MEX_sndRemitPurpose.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

     
        
        private List<MEXSndRemitPurposeEx> Transform(IQueryable<MEX_sndRemitPurpose> o)
        {
            List<MEXSndRemitPurposeEx> l = new List<MEXSndRemitPurposeEx>();
            foreach (MEX_sndRemitPurpose b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }

      
    }
}
