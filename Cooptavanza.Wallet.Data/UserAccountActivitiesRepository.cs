﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Text;

namespace Cooptavanza.Wallet.Data
{
    public class UserAccountActivitiesRepository : IUserAccountActivitiesRepository
    {
        readonly IRepositoryEF<UserAccountActivity> repo;
        readonly CW_EntitiesAzure db;

        public UserAccountActivitiesRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<UserAccountActivity>(db);

        }

       
        public UserAccountActivitiesEx Create()
        {
            return new UserAccountActivitiesEx();
        }
        

        public void SaveActivity(UserAccountActivitiesEx n)
        {
            UserAccountActivitiesEx l = new UserAccountActivitiesEx();
            
            l.status = false;
            //ele.logDate = DateTime.Now;
            Save(l);
        }



        private UserAccountActivitiesEx Transform(UserAccountActivity n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new UserAccountActivitiesEx();
            l.id = n.id;
            l.dateCreated = n.dateCreated;
            l.token_id = n.token_id;
            
            l.userId = n.userId;
            l.userRole = n.userRole;
            l.status = n.status;
            return l;
        }

        public void Remove(UserAccountActivitiesEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        private UserAccountActivity Transform(UserAccountActivitiesEx n)
        {
            UserAccountActivity l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.dateCreated = n.dateCreated;
            l.token_id = n.token_id;

            l.userId = n.userId;
            l.userRole = n.userRole;
            l.status = n.status;

            return l;
        }

        
        public void Save(UserAccountActivitiesEx l)
        {
            var tempid = l.id;
            var n= Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
                
            }
            repo.SaveChanges();
            l.id = n.id;
            l.dateCreated = n.dateCreated;
            l.token_id = n.token_id;

            l.userId = n.userId;
            l.userRole = n.userRole;
            l.status = n.status;
            //  l.logDate = n.logDate;
        }

       

        public List<UserAccountActivitiesEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            DateTime startdate = DateTime.Now.AddDays(-3);
            DateTime enddate = DateTime.Now;
          
            

            var r = db.UserAccountActivities.Where(c => c.dateCreated >= startdate && c.dateCreated <= enddate && c.status == false).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }
        private List<UserAccountActivitiesEx> Transform(IQueryable<UserAccountActivity> aUAA)
        {
            List<UserAccountActivitiesEx> l = new List<UserAccountActivitiesEx>();
            foreach (UserAccountActivity b in aUAA)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
