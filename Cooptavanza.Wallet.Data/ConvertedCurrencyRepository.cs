﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class ConvertedCurrencyRepository : IConvertedCurrencyRepository
    {
        readonly IRepositoryEF<convertedcurrency> repo;
        readonly CW_EntitiesAzure db;

        public ConvertedCurrencyRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<convertedcurrency>(db);

        }
        
        public ConvertedCurrencyEx Create()
        {
            return new ConvertedCurrencyEx();
        }

        public ConvertedCurrencyEx Get(string quote)
        {
            var n = db.convertedcurrencies.Where(c => c.qoutes.Equals(quote)).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private ConvertedCurrencyEx Transform(convertedcurrency n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ConvertedCurrencyEx();
            l.id = n.id;
            l.@base = n.@base;
            l.qoutes = n.qoutes;
            l.timestamp = n.timestamp;
            l.amount = n.amount;
            return l;
        }

        private convertedcurrency Transform(ConvertedCurrencyEx n)
        {
            convertedcurrency l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.@base = n.@base;
            l.qoutes = n.qoutes;
            l.timestamp = n.timestamp;
            l.amount = n.amount;
            return l;
        }

        public void Remove(ConvertedCurrencyEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public ConvertedCurrencyEx getDataByQoute(string qoute)
        {
            var d = db.convertedcurrencies.Where(x => x.qoutes.Equals(qoute)).FirstOrDefault();
            return Transform(d);
        }
        public object RefreshConvertedCurrency()
        {
            try
            {
                new SPConnection(db).RefreshConvertedCurrencyOnDB();
                return "Refreshed";
            }
            catch (Exception ex)
            {
                return ex;
            }
        }
        
        public int Count()
        {
            return db.convertedcurrencies.Count(); 
        }

        public void Save(ConvertedCurrencyEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();

            item.id = g.id;
            item.@base = g.@base;
            item.qoutes = g.qoutes;
            item.timestamp = g.timestamp;
            item.amount = g.amount;
        }
        
        private List<ConvertedCurrencyEx> Transform(IQueryable<convertedcurrency> aPerson)
        {
            List<ConvertedCurrencyEx> l = new List<ConvertedCurrencyEx>();
            foreach (convertedcurrency b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }
      
    }
}
