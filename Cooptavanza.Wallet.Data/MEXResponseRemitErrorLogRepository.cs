﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MEXResponseRemitErrorLogRepository : IMEXResponseRemitErrorLogRepository
    {
        readonly IRepositoryEF<MEX_ResponseRemitErrorLog> repo;
        readonly CW_EntitiesAzure db;

        public MEXResponseRemitErrorLogRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<MEX_ResponseRemitErrorLog>(db);

        }

      
        public MEXResponseRemitErrorLogEx Get(int Id)
        {
            var n = db.MEX_ResponseRemitErrorLog.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MEXResponseRemitErrorLogEx Transform(MEX_ResponseRemitErrorLog n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MEXResponseRemitErrorLogEx();

            l.id = n.id;
            l.ActionCode = n.ActionCode;
            l.MEXType = n.MEXType;
            l.QuoteAmt = n.QuoteAmt;
            l.QuoteCurr = n.QuoteCurr;
            l.RcvAccntBankCode = n.RcvAccntBankCode;
            l.RcvAccntBankName = n.RcvAccntBankName;
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;
            l.RespCode = n.RespCode;
            l.RespMessage = n.RespMessage;
            l.SubmitTime = n.SubmitTime;
            l.TransactionId = n.TransactionId;

            return l;
        }

        private MEX_ResponseRemitErrorLog Transform(MEXResponseRemitErrorLogEx n)
        {
            MEX_ResponseRemitErrorLog l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.ActionCode = n.ActionCode;
            l.MEXType = n.MEXType;
            l.QuoteAmt = n.QuoteAmt;
            l.QuoteCurr = n.QuoteCurr;
            l.RcvAccntBankCode = n.RcvAccntBankCode;
            l.RcvAccntBankName = n.RcvAccntBankName;
            l.RcvAccntNo = n.RcvAccntNo;
            l.RcvFirstName = n.RcvFirstName;
            l.RcvLastName = n.RcvLastName;
            l.ReferenceId = n.ReferenceId;
            l.RespCode = n.RespCode;
            l.RespMessage = n.RespMessage;
            l.SubmitTime = n.SubmitTime;
            l.TransactionId = n.TransactionId;
          
            return l;
        }

        public List<MEXResponseRemitErrorLogEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(RcvFirstName.Contains(\"" + where + "\")");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.MEX_ResponseRemitErrorLog.Where(sWhere).OrderBy(orderby) : db.MEX_ResponseRemitErrorLog.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(RcvFirstName.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.MEX_ResponseRemitErrorLog.Where(sWhere).Count() : db.MEX_RemitValidate.Count();
        }



        private List<MEXResponseRemitErrorLogEx> Transform(IQueryable<MEX_ResponseRemitErrorLog> o)
        {
            List<MEXResponseRemitErrorLogEx> l = new List<MEXResponseRemitErrorLogEx>();
            foreach (MEX_ResponseRemitErrorLog b in o)
            {
                l.Add(Transform(b));
            }

            return l;
        }


        public void Remove(MEXResponseRemitErrorLogEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(MEXResponseRemitErrorLogEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();
            l.id = n.id;
        }

        public int GetCount()
        {
           return db.MEX_ResponseRemitErrorLog.Count();
        }

        public MEXResponseRemitErrorLogEx Create()
        {
           return new MEXResponseRemitErrorLogEx();
        }
    }
}
