﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class CustomerRepository : ICustomerRepository
    {
        readonly IRepositoryEF<customer> repo;
        readonly CW_EntitiesAzure db;

        public CustomerRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<customer>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.customers.Where(sWhere).Count() : db.customers.Count();
        }

        public CustomerEx Create()
        {
            return new CustomerEx();
        }

        public CustomerEx GetLogin(string mobilenumber, string password) {

           
            return new SPConnection(db).SP_authenticate(mobilenumber, password); 
        }

        public CustomerEx Get(int Id)
        {
            var n = db.customers.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private CustomerEx Transform(customer n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CustomerEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.birthdate = n.birthdate;
            l.email = n.email;
            l.countryid = n.countryid;
            l.country = n.country.commonname;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.username = n.username;
            l.stateid = n.stateid;
            l.state = n.state.name;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.cityid = n.cityid;
            l.profileimageId = n.profileimageId;
            l.recentbillimageId = n.recentbillimageId;
            l.tid = n.tid;
            l.isVerified = n.isVerified;
            l.isInOFACList = n.isInOFACList;
            l.theme = n.theme == null ? "skin-6" : "skin-2";
            l.partnerID = n.partnerID;
            l.gender = n.gender;
            return l;
        }

        private customer Transform(CustomerEx n)
        {
            customer l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.birthdate = n.birthdate;
            l.email = n.email;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.username = n.username;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.cityid = n.cityid;
            l.profileimageId = n.profileimageId;
            l.recentbillimageId = n.recentbillimageId;
            l.tid = n.tid;
            l.isVerified = n.isVerified;
            l.isInOFACList = n.isInOFACList;
            l.theme = n.theme == null ? "skin-6" : "skin-2";
            l.partnerID = n.partnerID;
            l.gender = n.gender;
            return l;
        }

        public List<CustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.customers.Where(sWhere).OrderBy(orderby) : db.customers.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(CustomerEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CustomerEx item)
        {
            int tempid = item.id;
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            if(tempid == 0)
            {
                CustomerEx x = Get(g.id);
                x.accountnumber = new SPConnection(db).SP_getAccountNumber(7, g.id);
                Save(x);
            }

            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.middlename = g.middlename;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.birthdate = g.birthdate;
            item.email = g.email;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.username = g.username;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.profileimageId = g.profileimageId;
            item.recentbillimageId = g.recentbillimageId;
            item.cityid = g.cityid;
            item.isVerified = g.isVerified;
            item.isInOFACList = g.isInOFACList;
            item.theme = g.theme;
            item.partnerID = g.partnerID;
            item.gender = g.gender;
        }

        public CustomerEx SaveCustomer(CustomerEx item)
        {
            int tempid = item.id;
            var g = Transform(item);
            Boolean cancel = false;
            if (g.id == 0)
            {
                var h = GetCheckEmail(g.email);
                var i = GetByMobileNumber(g.mobilenumber);
                if (h != null)
                {
                    cancel = true;
                }
                if (i != null)
                {
                    cancel = true;
                }
                if(cancel == false)
                {
                    repo.Add(g);
                }
                else
                {
                    return null;
                }
            }
            else
            {
                var h = GetCheckEmail(g.email);
                var i = GetByMobileNumber(g.mobilenumber);
                if (h != null && h.id != g.id)
                {
                    cancel = true;
                }
                if (i != null && h != null && h.id != g.id)
                {
                    cancel = true;
                }
                if (cancel == false)
                {
                    repo.Update(g);
                }
               
            }
            if(cancel == false) { 
                repo.SaveChanges();
                if (tempid == 0)
                {
                    CustomerEx x = Get(g.id);
                    x.accountnumber = new SPConnection(db).SP_getAccountNumber(7, g.id);
                    Save(x);
                }
            }
            else { return null; }
            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.middlename = g.middlename;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.birthdate = g.birthdate;
            item.email = g.email;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.username = g.username;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.profileimageId = g.profileimageId;
            item.recentbillimageId = g.recentbillimageId;
            item.cityid = g.cityid;
            item.isVerified = g.isVerified;
            item.isInOFACList = g.isInOFACList;
            item.theme = g.theme;
            item.partnerID = g.partnerID;
            item.gender = g.gender;
            return item;
        }

        private List<CustomerEx> Transform(IQueryable<customer> aPerson)
        {
            List<CustomerEx> l = new List<CustomerEx>();
            foreach (customer b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public CustomerEx GetCheckEmail(string str)
        {
            var data = db.customers.Where(x => x.email == str).FirstOrDefault();

            return Transform(data);
        }

        public CustomerEx GetByMobileNumber(string mobileNumber)
        {
            var data = db.customers.Where(x => x.mobilenumber == mobileNumber).FirstOrDefault();
            return Transform(data);
        }

        public CustomerEx GetCheckMobileEmail(string str)
        {
            var data = db.customers.Where(x => x.mobilenumber == str || x.email == str).FirstOrDefault();

            return Transform(data);
        }

        public int CountCustomer()
        {
            return db.customers.Count();
        }
    }
}
