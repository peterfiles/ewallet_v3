﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class TransactionFeeRepository : ITransactionFeeRepository
    {
        readonly IRepositoryEF<transactionFee> repo;
        readonly CW_EntitiesAzure db;

        public TransactionFeeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<transactionFee>(db);

        }

        public int Count(int id, string TID, string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.transactionFees.Where(sWhere).Where(c => c.id == id).Count() : db.transactionFees.Where(c => c.id == id).Count();
        }

        public TransactionFeeEx Create()
        {
            return new TransactionFeeEx();
        }

        public TransactionFeeEx Get(int Id)
        {
            var n = db.transactionFees.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public TransactionFeeEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private TransactionFeeEx Transform(transactionFee n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new TransactionFeeEx();
            l.id = n.id;
            l.country = n.country;
            l.currencyid = n.currencyid;
            l.currencyiso = n.currencyiso;
            l.dateCreated = n.dateCreated;
            l.description = n.description;
            l.grandtotal = n.grandtotal;
            l.servicefee = n.servicefee;
            l.servicefeeID = n.servicefeeID;
            l.status = n.status;
            l.subtotal = n.subtotal;
            l.tax = n.tax;
            l.taxPercentage = n.taxPercentage;
            l.totalfee = n.totalfee;
            l.transactionid = n.transactionid;
            l.transactiontid = n.transactiontid;
            return l;
        }

        private transactionFee Transform(TransactionFeeEx n)
        {
            transactionFee l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.country = n.country;
            l.currencyid = n.currencyid;
            l.currencyiso = n.currencyiso;
            l.dateCreated = n.dateCreated;
            l.description = n.description;
            l.grandtotal = n.grandtotal;
            l.servicefee = n.servicefee;
            l.servicefeeID = n.servicefeeID;
            l.status = n.status;
            l.subtotal = n.subtotal;
            l.tax = n.tax;
            l.taxPercentage = n.taxPercentage;
            l.totalfee = n.totalfee;
            l.transactionid = n.transactionid;
            l.transactiontid = n.transactiontid;

            return l;
        }

        public List<TransactionFeeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.transactionFees.Where(sWhere).Where(c => c.id == id ).OrderBy(orderby) : db.transactionFees.Where(c => c.id == id).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(TransactionFeeEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(TransactionFeeEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.country = g.country;
            item.currencyid = g.currencyid;
            item.currencyiso = g.currencyiso;
            item.dateCreated = g.dateCreated;
            item.description = g.description;
            item.grandtotal = g.grandtotal;
            item.servicefee = g.servicefee;
            item.servicefeeID = g.servicefeeID;
            item.status = g.status;
            item.subtotal = g.subtotal;
            item.tax = g.tax;
            item.taxPercentage = g.taxPercentage;
            item.totalfee = g.totalfee;
            item.transactionid = g.transactionid;
            item.transactiontid = g.transactiontid;
        }


      

        private List<TransactionFeeEx> Transform(IQueryable<transactionFee> aTransaction)
        {
            List<TransactionFeeEx> l = new List<TransactionFeeEx>();
            foreach (transactionFee b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
