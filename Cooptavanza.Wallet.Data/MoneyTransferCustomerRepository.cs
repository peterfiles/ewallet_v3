﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class MoneyTransferCustomerRepository : IMoneyTransferCustomerRepository
    {
        readonly IRepositoryEF<moneyTransferCustomer> repo;
        readonly CW_EntitiesAzure db;

        public MoneyTransferCustomerRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<moneyTransferCustomer>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.moneyTransferCustomers.Where(sWhere).Count() : db.moneyTransferCustomers.Count();
        }

        public MoneyTransferCustomerEx Create()
        {
            return new MoneyTransferCustomerEx();
        }

      

        public MoneyTransferCustomerEx Get(int Id)
        {
            var n = db.moneyTransferCustomers.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MoneyTransferCustomerEx Transform(moneyTransferCustomer n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MoneyTransferCustomerEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.birthdate = n.birthdate;
            l.email = n.email;
            l.countryid = n.countryid;
            l.country = n.country.commonname;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.username = n.username;
            l.stateid = n.stateid;
            l.state = n.state.name;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.cityid = n.cityid;
            l.profileimageId = n.profileimageId;
            l.recentbillimageId = n.recentbillimageId;
            l.tid = n.tid;
            l.isVerified = n.isVerified;
            l.isInOFACList = n.isInOFACList;
            l.theme = n.theme == null ? "skin-6" : "skin-2";
            l.partnerID = n.partnerID;
            l.gender = n.gender;
            l.roleid = n.roleid;
            return l;
        }

        private moneyTransferCustomer Transform(MoneyTransferCustomerEx n)
        {
            moneyTransferCustomer l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.birthdate = n.birthdate;
            l.email = n.email;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.username = n.username;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.cityid = n.cityid;
            l.profileimageId = n.profileimageId;
            l.recentbillimageId = n.recentbillimageId;
            l.tid = n.tid;
            l.isVerified = n.isVerified;
            l.isInOFACList = n.isInOFACList;
            l.theme = n.theme == null ? "skin-6" : "skin-2";
            l.partnerID = n.partnerID;
            l.gender = n.gender;
            l.roleid = n.roleid;
            return l;
        }

        public List<MoneyTransferCustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.moneyTransferCustomers.Where(sWhere).OrderBy(orderby) : db.moneyTransferCustomers.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(MoneyTransferCustomerEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(MoneyTransferCustomerEx item)
        {
            int tempid = item.id;
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.middlename = g.middlename;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.birthdate = g.birthdate;
            item.email = g.email;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.username = g.username;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.profileimageId = g.profileimageId;
            item.recentbillimageId = g.recentbillimageId;
            item.cityid = g.cityid;
            item.isVerified = g.isVerified;
            item.isInOFACList = g.isInOFACList;
            item.theme = g.theme;
            item.partnerID = g.partnerID;
            item.gender = g.gender;
            item.roleid = g.roleid;
        }

        public MoneyTransferCustomerEx SaveMoneyTransferCustomer(MoneyTransferCustomerEx item)
        {
            int tempid = item.id;
            var g = Transform(item);
            Boolean cancel = false;
            if (g.id == 0)
            {
                var h = GetCheckEmail(g.email);
                var i = GetByMobileNumber(g.mobilenumber);
                if (h != null)
                {
                    cancel = true;
                }
                if (i != null)
                {
                    cancel = true;
                }
                if(cancel == false)
                {
                    repo.Add(g);
                }
            }
            else
            {
                var h = GetCheckEmail(g.email);
                var i = GetByMobileNumber(g.mobilenumber);
                if (h != null && h.id != g.id)
                {
                    cancel = true;
                }
                if (i != null && h.id != g.id)
                {
                    cancel = true;
                }
                if (cancel == false)
                {
                    repo.Update(g);
                }
               
            }
            if(cancel == false) { 
                repo.SaveChanges();
                if (tempid == 0)
                {
                    MoneyTransferCustomerEx x = Get(g.id);
                    x.accountnumber = new SPConnection(db).SP_getAccountNumber(7, g.id);
                    Save(x);
                }
            }
            else { return null; }
            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.middlename = g.middlename;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.birthdate = g.birthdate;
            item.email = g.email;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.username = g.username;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.profileimageId = g.profileimageId;
            item.recentbillimageId = g.recentbillimageId;
            item.cityid = g.cityid;
            item.isVerified = g.isVerified;
            item.isInOFACList = g.isInOFACList;
            item.theme = g.theme;
            item.partnerID = g.partnerID;
            item.gender = g.gender;
            item.roleid = g.roleid;
            return item;
        }

        private List<MoneyTransferCustomerEx> Transform(IQueryable<moneyTransferCustomer> aMoneyTransferCustomer)
        {
            List<MoneyTransferCustomerEx> l = new List<MoneyTransferCustomerEx>();
            foreach (moneyTransferCustomer b in aMoneyTransferCustomer)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public MoneyTransferCustomerEx GetCheckEmail(string str)
        {
            var data = db.moneyTransferCustomers.Where(x => x.email == str).FirstOrDefault();

            return Transform(data);
        }

        public MoneyTransferCustomerEx GetByMobileNumber(string mobileNumber)
        {
            var data = db.moneyTransferCustomers.Where(x => x.mobilenumber == mobileNumber).FirstOrDefault();
            return Transform(data);
        }

        public MoneyTransferCustomerEx GetCheckMobileEmail(string str)
        {
            var data = db.moneyTransferCustomers.Where(x => x.mobilenumber == str || x.email == str).FirstOrDefault();

            return Transform(data);
        }
    }
}
