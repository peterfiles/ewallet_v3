﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class TransactionTypeRepository : ITransactionTypeRepository
    {
        readonly IRepositoryEF<transactionType> repo;
        readonly CW_EntitiesAzure db;

        public TransactionTypeRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<transactionType>(db);

        }

        public int Count(int id, string TID, string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.transactionTypes.Where(sWhere).Where(c => c.id == id).Count() : db.transactionTypes.Where(c => c.id == id).Count();
        }

     

        public TransactionTypeEx Get(int Id)
        {
            var n = db.transactionTypes.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private TransactionTypeEx Transform(transactionType n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new TransactionTypeEx();
            l.id = n.id;
            l.code = n.code;
            l.description = n.description;
            l.name = n.name;
          
            return l;
        }

        private transactionType Transform(TransactionTypeEx n)
        {
            transactionType l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            
            l.id = n.id;
          //  l.code = n.code;
           // l.description = n.description;
           // l.name = n.name;


            return l;
        }

        public List<TransactionTypeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.transactionTypes.Where(sWhere).Where(c => c.id == id ).OrderBy(orderby) : db.transactionTypes.Where(c => c.id == id).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(TransactionTypeEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(TransactionTypeEx item)
        {
            //var g = Transform(item);
            //if (g.id == 0)
            //{
            //    repo.Add(g);
            //}
            //else
            //{
            //    repo.Update(g);
            //}
            //repo.SaveChanges();
            //item.id = g.id;
          
        }


      

        private List<TransactionTypeEx> Transform(IQueryable<transactionType> aTransaction)
        {
            List<TransactionTypeEx> l = new List<TransactionTypeEx>();
            foreach (transactionType b in aTransaction)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public TransactionTypeEx Create()
        {
            throw new NotImplementedException();
        }

        public TransactionTypeEx GetByCode(string code)
        {
            var r = db.transactionTypes.Where(x => x.code == code).FirstOrDefault();
            if (r != null)
            {
                return Transform(r);
            }
            return null;
        }
    }
}
