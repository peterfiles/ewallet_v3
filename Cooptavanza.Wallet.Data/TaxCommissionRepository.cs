﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class TaxCommissionRepository : ITaxCommissionRepository
    {
        readonly IRepositoryEF<taxcommission> repo;
        readonly CW_EntitiesAzure db;
        readonly SPConnection sp;

        public TaxCommissionRepository(CW_EntitiesAzure db, SPConnection sp) {
            this.db = db;
            this.repo = new IRepositoryEF<taxcommission>(db);
            this.sp = sp;

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.countries.Where(sWhere).Where(x => x.id > 0).Count() : db.countries.Where(x => x.id > 0).Count();
        }

        public TaxCommissionEx Create()
        {
            return new TaxCommissionEx();
        }

        public TaxCommissionEx Get(int Id)
        {
            var n = db.taxcommissions.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private TaxCommissionEx Transform(taxcommission n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new TaxCommissionEx();
            l.id = n.id;
            l.commissionamount = n.commissionamount;
            l.country = n.country;
            l.currencyTransacted = n.currencyTransacted;
            l.dateCreated = n.dateCreated;
            l.isCoopRedTransaction = n.isCoopRedTransaction;
            l.taxaccountno = n.taxaccountno;
            l.timestampoftransaction = n.timestampoftransaction;
            l.transactionID = n.transactionID;
            l.transactionIDResponse = n.transactionIDResponse;
            l.transactiontype = n.transactiontype;
            return l;
        }

        private taxcommission Transform(TaxCommissionEx n)
        {
            taxcommission l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.commissionamount = n.commissionamount;
            l.country = n.country;
            l.currencyTransacted = n.currencyTransacted;
            l.dateCreated = n.dateCreated;
            l.isCoopRedTransaction = n.isCoopRedTransaction;
            l.taxaccountno = n.taxaccountno;
            l.timestampoftransaction = n.timestampoftransaction;
            l.transactionID = n.transactionID;
            l.transactionIDResponse = n.transactionIDResponse;
            l.transactiontype = n.transactiontype;
            return l;
        }

        public List<TaxCommissionEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.taxcommissions.Where(sWhere).Where(x => x.id > 0).OrderBy(orderby) : db.taxcommissions.Where(x => x.id > 0).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
           {
                return Transform(r);
            }
            else
            {
               return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(TaxCommissionEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(TaxCommissionEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.commissionamount = g.commissionamount;
            item.country = g.country;
            item.currencyTransacted = g.currencyTransacted;
            item.dateCreated = g.dateCreated;
            item.isCoopRedTransaction = g.isCoopRedTransaction;
            item.taxaccountno = g.taxaccountno;
            item.timestampoftransaction = g.timestampoftransaction;
            item.transactionID = g.transactionID;
            item.transactionIDResponse = g.transactionIDResponse;
            item.transactiontype = g.transactiontype;
        }

        private List<TaxCommissionEx> Transform(IQueryable<taxcommission> aPerson)
        {
            List<TaxCommissionEx> l = new List<TaxCommissionEx>();
            foreach (taxcommission b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }
    }
}
