﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class BankDepositRepository : IBankDepositRepository
    {
        readonly IRepositoryEF<bankdeposit> repo;
        readonly CW_EntitiesAzure db;

        public BankDepositRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<bankdeposit>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.bankdeposits.Where(sWhere).Count() : db.bankdeposits.Count();
        }

        public BankDepositEx Create()
        {
            return new BankDepositEx();
        }

        public List<BankDepositEx> GetAllCountry()
        {
            var n = db.bankdeposits.Where(x => x.id > 0);
            return Transform(n);
        }

        public BankDepositEx Get(int Id)
        {
            var n = db.bankdeposits.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public List<BankDepositEx> GetBankNameByCountryID(int countryID)
        {   
            return new SPConnection(db).Sp_GetBankDepositorByCountryID(countryID);
        }

        public List<BankDepositEx> GetBankNameByCountryIDAndBankName(int countryID, string bankName)
        {
            return new SPConnection(db).Sp_GetBankDepositorByCountryIDAndBankName(countryID, bankName);
        }

        public List<BankDepositEx> GetBankByCountry() {
            return new SPConnection(db).Sp_getBankDepositCountry();
        }

        private BankDepositEx Transform(bankdeposit n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new BankDepositEx();

            l.id = n.id;
            l.bankName = n.bankName;
            l.countryid = n.countryid;
            l.currenciesID = n.currenciesID;
            l.currenciesISO = n.currenciesISO;
            l.dateCreated = n.dateCreated;
            l.userId = n.userId;
            l.userRole = n.userRole;
            l.rtcode = n.rtcode;

            l.countryName = db.countries.Where(x => x.id == l.countryid).FirstOrDefault().commonname;
            l.memberTypeName = db.roles.Where(x => x.id == l.userRole).FirstOrDefault().name;

            return l;
        }

        private bankdeposit Transform(BankDepositEx n)
        {
            bankdeposit l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.bankName = n.bankName;
            l.countryid = n.countryid;
            l.currenciesID = n.currenciesID;
            l.currenciesISO = n.currenciesISO;
            l.dateCreated = n.dateCreated;
            l.userId = n.userId;
            l.userRole = n.userRole;
            l.rtcode = n.rtcode;
            
            return l;
        }

        public List<BankDepositEx> GetPaged(string where, string orderby, int PageNo, int PageSize, int userID, int roleID)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            //if(roleID == 2)
            //{   
            //    var r = "" != sWhere ? db.bankdeposits.Where(sWhere)
            //                                     .Where(x => x.userId == userID && x.userRole == roleID)
            //                                     .OrderBy(orderby) : db.bankdeposits.Where(x => x.userId == userID && x.userRole == roleID).OrderBy(orderby);
            //    if (PageNo == 0 || PageSize == 0)
            //    {
            //        return Transform(r);
            //    }
            //    else
            //    {
            //        return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            //    }
            //}
            //else
            //{
                
                var r = "" != sWhere ? db.bankdeposits.Where(sWhere).OrderBy(orderby) : db.bankdeposits.OrderBy(orderby);
                if (PageNo == 0 || PageSize == 0)
                {
                    return Transform(r);
                }
                else
                {
                    return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
                }
            //}
        }

        public List<BankDepositEx> CustomListGetPaged(string str, int psize, int pno)
        {
            return new SPConnection(db).SP_getAllBankDepositorListGetPaged(str,psize,pno);
        }

        public int CustomListGetPagedCount(string str)
        {
            return new SPConnection(db).SP_getAllBankDepositorListGetPagedCount(str);
        }

        public void Remove(BankDepositEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(BankDepositEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();

            l.id = n.id;
            l.bankName = n.bankName;
            l.countryid = n.countryid;
            l.currenciesID = n.currenciesID;
            l.currenciesISO = n.currenciesISO;
            l.dateCreated = n.dateCreated;
            l.userId = n.userId;
            l.userRole = n.userRole;

        }

        private List<BankDepositEx> Transform(IQueryable<bankdeposit> aBiller)
        {
            List<BankDepositEx> l = new List<BankDepositEx>();
            foreach (bankdeposit b in aBiller)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public BankDepositEx GetByRtCode(string rtCode)
        {
            var d = db.bankdeposits.Where(x => x.rtcode.Equals(rtCode)).FirstOrDefault();

            return Transform(d);
        }
    }
}
