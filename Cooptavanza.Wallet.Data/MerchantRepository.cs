﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class MerchantRepository : IMerchantRepository
    {
        readonly IRepositoryEF<merchant> repo;
        readonly CW_EntitiesAzure db;

        public MerchantRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<merchant>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.merchants.Where(sWhere).Count() : db.merchants.Count();
        }

        public MerchantEx Create()
        {
            return new MerchantEx();
        }

        public MerchantEx Get(int Id)
        {
            var n = db.merchants.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private MerchantEx Transform(merchant n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new MerchantEx();
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.email = n.email;
            l.country = n.countryid != null ? n.country1.name : "";
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.url = n.url;
            l.state = n.stateid != null ? n.state1.name : "";
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.username = n.username;
            l.city = n.city;
            l.tid = n.tid;
            l.isInOFACList = n.isInOFACList;
            l.isVerified = n.isVerified;
            l.masterPartnerID = n.masterPartnerID;
            l.name = n.name;
            l.code = n.code;
            return l;
        }

        private merchant Transform(MerchantEx n)
        {
            merchant l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.lastname = n.lastname;
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.email = n.email;
            l.country = n.country;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.url = n.url;
            l.state = n.state;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.username = n.username;
            l.city = n.city;
            l.tid = n.tid;
            l.isInOFACList = n.isInOFACList;
            l.isVerified = n.isVerified;
            l.masterPartnerID = n.masterPartnerID;
            l.name = n.name;
            l.code = n.code;
            return l;
        }

        public List<MerchantEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.merchants.Where(sWhere).OrderBy(orderby) : db.merchants.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(MerchantEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(MerchantEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.accountnumber = g.accountnumber;
            item.firstname = g.firstname;
            item.lastname = g.lastname;
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.email = g.email;
            item.country = g.country;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.url = g.url;
            item.state = g.state;
            item.zipcode = g.zipcode;
            item.username = g.username;
            item.city = g.city;
            item.isInOFACList = g.isInOFACList;
            item.isVerified = g.isVerified;
            item.masterPartnerID = g.masterPartnerID;
            item.name = g.name;
            item.code = g.code;
        }


      

        private List<MerchantEx> Transform(IQueryable<merchant> aPerson)
        {
            List<MerchantEx> l = new List<MerchantEx>();
            foreach (merchant b in aPerson)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public int CountMerchant()
        {
            return db.merchants.Count();
        }
    }
}
