﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class PartnerCustomerRepository : IPartnerCustomerRepository
    {
        readonly IRepositoryEF<partnerCustomer> repo;
        readonly CW_EntitiesAzure db;

        public PartnerCustomerRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<partnerCustomer>(db);

        }

        public int Count(string where, int partnerId)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\") || accountnumber.Contains(\"" + where + "\") || Lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.partnerCustomers.Where(sWhere).Where(c => c.partnerId == partnerId).Count() : db.partnerCustomers.Where(c => c.partnerId == partnerId).Count();
        }

        public PartnerCustomerEx Create()
        {
            return new PartnerCustomerEx();
        }

        public PartnerCustomerEx Get(int Id)
        {
            var n = db.partnerCustomers.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public PartnerCustomerEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private PartnerCustomerEx Transform(partnerCustomer n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new PartnerCustomerEx();

            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.suffix = n.suffix;
            l.email = n.email;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.tid = n.tid;
            l.dateCreated = n.dateCreated;
            l.partnerId = n.partnerId;
            return l;
        }

        private partnerCustomer Transform(PartnerCustomerEx n)
        {
            partnerCustomer l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.accountnumber = n.accountnumber;
            l.firstname = n.firstname;
            l.middlename = n.middlename;
            l.lastname = n.lastname;
            l.suffix = n.suffix;
            l.email = n.email;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.tid = n.tid;
            l.dateCreated = n.dateCreated;
            l.partnerId = n.partnerId;
            
            return l;
        }

        public List<PartnerCustomerEx> GetPaged(string where, int partnerId, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\") || accountnumber.Contains(\"" + where + "\") || lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.partnerCustomers.Where(sWhere).Where(c => c.partnerId == partnerId).OrderBy(orderby) : db.partnerCustomers.Where(c => c.partnerId == partnerId).OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
           else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(PartnerCustomerEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(PartnerCustomerEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();

            item.id = g.id;


            item.partnerId = g.partnerId;

         
        }

        public PartnerCustomerEx SavePC(PartnerCustomerEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            
            item.id = g.id;
            
          
            item.partnerId = g.partnerId;

            return Get(item.id);
        }


      

        private List<PartnerCustomerEx> Transform(IQueryable<partnerCustomer> aPartnerCustomer)
        {
            List<PartnerCustomerEx> l = new List<PartnerCustomerEx>();
            foreach (partnerCustomer b in aPartnerCustomer)
            {
                l.Add(Transform(b));
            }

            return l;
        }

     

    }
}
