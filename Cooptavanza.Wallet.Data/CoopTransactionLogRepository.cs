﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System.Data.SqlClient;
using System.Data;

namespace Cooptavanza.Wallet.Data
{
    public class CoopTransactionLogRepository : ICoopTransactionLogRepository
    {
        readonly IRepositoryEF<cooptransactionLog> repo;
        readonly CW_EntitiesAzure db;

        public CoopTransactionLogRepository(CW_EntitiesAzure db)
        {
            this.db = db;
            this.repo = new IRepositoryEF<cooptransactionLog>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.cooptransactionLogs.Where(sWhere).Count() : db.cooptransactionLogs.Count();
        }

        public CoopTransactionLogEx Create()
        {
            return new CoopTransactionLogEx();
        }

        public CoopTransactionLogEx Get(int Id)
        {
            var n = db.cooptransactionLogs.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }
        
        public CoopTransactionLogEx GetByTransactionTID(string tid)
        {
            var d = db.cooptransactionLogs.Where(a => a.transactionTID.Equals(tid)).FirstOrDefault();
            if(d != null)
            {
                return Transform(d);
            }
            return null;
        }


        private CoopTransactionLogEx Transform(cooptransactionLog n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new CoopTransactionLogEx();
            l.id = n.id;
            l.amount = n.amount;
            l.currencyAccountFrom = n.currencyAccountFrom;
            l.currencyAccountTo = n.currencyAccountTo;
            l.date_created = n.date_created;
            l.giverTID = n.giverTID;
            l.giverUserId = n.giverUserId;
            l.giverUserRoles = n.giverUserRoles;
            l.isCredit = n.isCredit;
            l.receiverTID = n.receiverTID;
            l.receiverUserId = n.receiverUserId;
            l.receiverUserRoles = n.receiverUserRoles;
            l.transactionDescription = n.transactionDescription;
            l.transactionTID = n.transactionTID;
            l.transactionType = n.transactionType;
            return l;
        }

        private cooptransactionLog Transform(CoopTransactionLogEx n)
        {
            cooptransactionLog l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.amount = n.amount;
            l.currencyAccountFrom = n.currencyAccountFrom;
            l.currencyAccountTo = n.currencyAccountTo;
            l.date_created = n.date_created;
            l.giverTID = n.giverTID;
            l.giverUserId = n.giverUserId;
            l.giverUserRoles = n.giverUserRoles;
            l.isCredit = n.isCredit;
            l.receiverTID = n.receiverTID;
            l.receiverUserId = n.receiverUserId;
            l.receiverUserRoles = n.receiverUserRoles;
            l.transactionDescription = n.transactionDescription;
            l.transactionTID = n.transactionTID;
            l.transactionType = n.transactionType;

            return l;
        }

        public List<CoopTransactionLogEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ? db.cooptransactionLogs.Where(sWhere).OrderBy(orderby) : db.cooptransactionLogs.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(CoopTransactionLogEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Remove(int id)
        {
            CoopTransactionLogEx item = Get(id);
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(CoopTransactionLogEx l)
        {
            var tempid = l.id;
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);

            }
            else
            {
                repo.Update(n);
            }


            repo.SaveChanges();

            l.id = n.id;
            l.amount = n.amount;
            l.currencyAccountFrom = n.currencyAccountFrom;
            l.currencyAccountTo = n.currencyAccountTo;
            l.date_created = n.date_created;
            l.giverTID = n.giverTID;
            l.giverUserId = n.giverUserId;
            l.giverUserRoles = n.giverUserRoles;
            l.isCredit = n.isCredit;
            l.receiverTID = n.receiverTID;
            l.receiverUserId = n.receiverUserId;
            l.receiverUserRoles = n.receiverUserRoles;
            l.transactionDescription = n.transactionDescription;
            l.transactionTID = n.transactionTID;
            l.transactionType = n.transactionType;
        }

        private List<CoopTransactionLogEx> Transform(IQueryable<cooptransactionLog> aEnrolledAccount)
        {
            List<CoopTransactionLogEx> l = new List<CoopTransactionLogEx>();
            foreach (cooptransactionLog b in aEnrolledAccount)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public List<CoopTransactionListEx> getCoopTransactionList(string str, int psize, int pno, int userID, int roleID)
        {
            return new SPConnection(db).sp_getCoopTransactionList(str, psize, pno, userID, roleID);
        }

        public int getcountTransactionListUserIDAndRoleID(string str, int userID, int roleID){
            return new SPConnection(db).SP_sp_getcountTransactionListUserIDAndRoleID(str, userID, roleID);
        }
    }
}
