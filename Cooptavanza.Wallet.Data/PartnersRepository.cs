﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class PartnersRepository : IPartnersRepository
    {
        readonly IRepositoryEF<partner> repo;
        readonly CW_EntitiesAzure db;

        public PartnersRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<partner>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.partners.Where(sWhere).Count() : db.partners.Count();
        }

        public PartnersEx Create()
        {
            return new PartnersEx();
        }

        public PartnersEx Get(int Id)
        {
            var n = db.partners.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

      
        private PartnersEx Transform(partner n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new PartnersEx();
            l.id = n.id;
            l.finame = n.finame; // financial institution name
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.country = n.countryid != null ? n.country1.name : "";
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.state = n.stateid != null ? n.state1.name : "" ;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.servicetype = n.servicetype;
            l.tid = n.tid;
            l.expirationDate = n.expirationDate;
            l.license = n.license;
            l.partnerType = n.partnerType;
            l.partnerCode = n.partnerCode;
            l.key = n.key;
            l.masterPartnerID = n.masterPartnerID;
            return l;
        }

        private partner Transform(PartnersEx n)
        {
           partner l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.finame = n.finame; // financial institution name
            l.currency = n.currency;
            l.currencyid = n.currencyid;
            l.country = n.country;
            l.countryid = n.countryid;
            l.mobilenumber = n.mobilenumber;
            l.phonenumber = n.phonenumber;
            l.address = n.address;
            l.state = n.state;
            l.stateid = n.stateid;
            l.zipcode = n.zipcode;
            l.city = n.city;
            l.servicetype = n.servicetype;
            l.tid = n.tid;
            l.expirationDate = n.expirationDate;
            l.license = n.license;
            l.partnerType = n.partnerType;
            l.partnerCode = n.partnerCode;
            l.key = n.key;
            l.masterPartnerID = n.masterPartnerID;
            return l;
        }

        public List<PartnersEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(firstname.Contains(\"" + where + "\")||lastname.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.partners.Where(sWhere).OrderBy(orderby) : db.partners.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(PartnersEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(PartnersEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.finame = g.finame; // financial institution name
            item.currency = g.currency;
            item.currencyid = g.currencyid;
            item.country = g.country;
            item.countryid = g.countryid;
            item.mobilenumber = g.mobilenumber;
            item.phonenumber = g.phonenumber;
            item.address = g.address;
            item.state = g.state;
            item.stateid = g.stateid;
            item.zipcode = g.zipcode;
            item.city = g.city;
            item.servicetype = g.servicetype;
            item.tid = g.tid;
            item.expirationDate = g.expirationDate;
            item.license = g.license;
            item.partnerType = g.partnerType;
            item.partnerCode = g.partnerCode;
            item.key = g.key;
            item.masterPartnerID = g.masterPartnerID;
        }


      

        private List<PartnersEx> Transform(IQueryable<partner> aPartners)
        {
            List<PartnersEx> l = new List<PartnersEx>();
            foreach (partner b in aPartners)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public PartnersEx getByPartnerCode(string partnerCode)
        {
            var n = db.partners.Where(c => c.partnerCode == partnerCode).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public List<PartnersEx> getAllMasterPartner()
        {
            var n = db.partners.Where(c => c.partnerType.Equals("MASTER_PARTNER"));
            return Transform(n);
        }

        public int CountPartners()
        {
            return db.partners.Count();
        }
    }
}
