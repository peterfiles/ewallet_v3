//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Cooptavanza.Wallet.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class serviceFee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public serviceFee()
        {
            this.transactionFees = new HashSet<transactionFee>();
        }
    
        public int id { get; set; }
        public string serviceFeeType { get; set; }
        public Nullable<decimal> serviceFeeAmount { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
        public Nullable<int> productID { get; set; }
        public Nullable<decimal> serviceFeePercentage { get; set; }
    
        public virtual product product { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<transactionFee> transactionFees { get; set; }
    }
}
