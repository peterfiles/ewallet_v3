﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class BillerRepository : IBillerRepository
    {
        readonly IRepositoryEF<biller> repo;
        readonly CW_EntitiesAzure db;

        public BillerRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<biller>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public BillerEx Create()
        {
            return new BillerEx();
        }

        public BillerEx Get(int Id)
        {
            var n = db.billers.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private BillerEx Transform(biller n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new BillerEx();

            l.id = n.id;
            l.name = n.name;
            l.tid = n.tid;
            l.countryid = n.countryid;
            l.country = n.country;
            l.stateid = n.stateid;
            l.state = n.state;
            l.city = n.city;
            l.logo = n.logo;
            l.sid = n.sid;
            l.address = n.address;
            l.billerType = n.billerType;
            l.currencyiso = n.currencyiso;
            l.currencyid = n.currencyid;
            l.billerTypeName = db.billerTypes.Where(z => z.id == l.billerType).FirstOrDefault().name;
            return l;
        }

        private biller Transform(BillerEx n)
        {
            biller l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }

            l.id = n.id;
            l.name = n.name;
            l.tid = n.tid;
            l.countryid = n.countryid;
            l.country = n.country;
            l.stateid = n.stateid;
            l.state = n.state;
            l.city = n.city;
            l.logo = n.logo;
            l.sid = n.sid;
            l.address = n.address;
            l.billerType = n.billerType;
            l.currencyiso = n.currencyiso;
            l.currencyid = n.currencyid;
            return l;
        }

        public List<BillerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.billers.Where(sWhere).OrderBy(orderby) : db.billers.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(BillerEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(BillerEx l)
        {
            var n = Transform(l);
            if (n.id == 0)
            {
                repo.Add(n);
            }
            else
            {
                repo.Update(n);
            }
            repo.SaveChanges();

            l.id = n.id;
            l.name = n.name;
            l.tid = n.tid;
            l.countryid = n.countryid;
            l.country = n.country;
            l.stateid = n.stateid;
            l.state = n.state;
            l.city = n.city;
            l.logo = n.logo;
            l.sid = n.sid;
            l.address = n.address;
            l.billerType = n.billerType;
            l.currencyiso = n.currencyiso;
            l.currencyid = n.currencyid;

        }

        private List<BillerEx> Transform(IQueryable<biller> aBiller)
        {
            List<BillerEx> l = new List<BillerEx>();
            foreach (biller b in aBiller)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public object GetBillerCountry()
        {
            object d = db.billers.Select(x => new
                                  {
                                      x.countryid,
                                      x.country
                                  })
                                  .Distinct()
                                  .ToList();
            return d;
        }

        public object GetBillerTypeByCountry(int id)
        {
            var d = db.billers.Join(db.billerTypes,
                               a => a.billerType,
                               b => b.id,
                               (a,b) => new {
                                   a.countryid,
                                   billerTypeName = b.name,
                                   billerID = b.id
                                })
                              .Where(x => x.countryid == id)
                              .Distinct()
                              .ToList();

            return d;
        }

        public object GetBillerByCountryAndType(int cID, int bTID)
        {
            var d = db.billers.Where(x => x.countryid == cID && x.billerType == bTID);
            return Transform(d);
        }
    }
}
