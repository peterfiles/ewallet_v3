﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;
using System;

namespace Cooptavanza.Wallet.Data
{
    public class TransferMoneyRepository : ITransferMoneyRepository
    {
        readonly IRepositoryEF<transferMoney> repo;
        readonly CW_EntitiesAzure db;

        public TransferMoneyRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<transferMoney>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.transferMoneys.Where(sWhere).Count() : db.transferMoneys.Count();
        }

        public TransferMoneyEx Create()
        {
            return new TransferMoneyEx();
        }

        public TransferMoneyEx Get(int Id)
        {
            var n = db.transferMoneys.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public TransferMoneyEx GetLogin(string mobile_number, string password) {
            return null;
        }

        private TransferMoneyEx Transform(transferMoney n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new TransferMoneyEx();
            l.id = n.id;
            l.transactionType = n.transactionType;
            l.datecreated = n.dateCreated;
            l.customerid = n.customerid;
            l.customertid = n.customertid;
            l.transactionTID = n.transactionTID;
            l.amountto = n.amountto;
            l.amountfrom = n.amountfrom;
            l.benificiaryfullname = n.benificiaryfullname;
            l.benificiarymobileno = n.benificiarymobileno;
            l.benificiaryemail = n.benificiaryemail;
            l.status = n.status;
            l.question = n.question;
            l.answer = n.answer;
            l.currencyfromiso = n.currencyfromiso;
            l.currencytoiso = n.currencytoiso;
            l.accountNumber = n.accountNumber;
            l.senderFirstName = n.senderFirstName;
            l.senderLastName = n.senderLastName;
            l.partnerId = n.partnerId;
            l.partnerName = n.partnerName;
            l.senderMiddleName = n.senderMiddleName;
            l.sendersuffix = n.receiverSuffix;
            l.receiverFirstname = n.receiverFirstname;
            l.receiverLastname = n.receiverLastname;
            l.receiverMiddlename = n.receiverMiddlename;
            l.receiverSuffix = n.receiverSuffix;
            l.transactionId = n.transactionId;
            return l;
        }

        private transferMoney Transform(TransferMoneyEx n)
        {
            transferMoney l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.transactionType = n.transactionType;
            l.dateCreated = n.datecreated;
            l.customerid = n.customerid;
            l.customertid = n.customertid;
            l.transactionTID = n.transactionTID;
            l.amountto = n.amountto;
            l.amountfrom = n.amountfrom;
            l.benificiaryfullname = n.benificiaryfullname;
            l.benificiarymobileno = n.benificiarymobileno;
            l.benificiaryemail = n.benificiaryemail;
            l.status = n.status;
            l.question = n.question;
            l.answer = n.answer;
            l.currencyfromiso = n.currencyfromiso;
            l.currencytoiso = n.currencytoiso;
            l.accountNumber = n.accountNumber;
            l.senderFirstName = n.senderFirstName;
            l.senderLastName = n.senderLastName;
            l.partnerId = n.partnerId;
            l.partnerName = n.partnerName;
            l.senderMiddleName = n.senderMiddleName;
            l.sendersuffix = n.receiverSuffix;
            l.receiverFirstname = n.receiverFirstname;
            l.receiverLastname = n.receiverLastname;
            l.receiverMiddlename = n.receiverMiddlename;
            l.receiverSuffix = n.receiverSuffix;
            l.transactionId = n.transactionId;



            return l;
        }

        public List<TransferMoneyEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(description.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            //var r = "" != sWhere ?db.transferMoneys.Where(sWhere).Where(c => c.customerid == id && c.transactionTID == TID).OrderBy(orderby) : db.transferMoneys.Where(c => c.customerid == id && c.transactionTID== TID).OrderBy(orderby);
            var r = "" != sWhere ? db.transferMoneys.Where(sWhere).OrderBy(orderby) : db.transferMoneys.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
           else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(TransferMoneyEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(TransferMoneyEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
        }


      

        private List<TransferMoneyEx> Transform(IQueryable<transferMoney> aTransferMoney)
        {
            List<TransferMoneyEx> l = new List<TransferMoneyEx>();
            foreach (transferMoney b in aTransferMoney)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public TransferMoneyEx GetTransferMoney(string tk, int id)
        {
            var t = db.transactions.Where(x => x.id == id && x.status != "CLEARED").FirstOrDefault();
            if(t == null)
            {
                return null;
            }
            var n = db.transferMoneys.Where(c => c.transactionTID == tk && c.status != "CLEARED").FirstOrDefault();
            
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public TransferMoneyEx GetTransferMoney(string tk, int id, int p)
        {
            var t = db.transactions.Where(x => x.id == id && x.status != "CLEARED").FirstOrDefault();
            if (t == null)
            {
                return null;
            }
            var n = db.transferMoneys.Where(c => c.transactionTID == tk && c.status != "CLEARED" && c.partnerId == p).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public TransferMoneyEx GetTransferMoneyWithOtp(string o, string tk, int id)
        {
            var n = db.transferMoneys.Where(c => c.transactionTID == tk && c.status == o).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        public TransferMoneyEx GetTransferMoneyByTransactionTID(string tID)
        {
            var d = db.transferMoneys.Where(x => x.transactionTID.Equals(tID)).FirstOrDefault();
            return Transform(d);
        }
        
    }
}
