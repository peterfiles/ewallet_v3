﻿using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;
using System.Linq.Dynamic;

namespace Cooptavanza.Wallet.Data
{
    public class ImageUserRepository : IImageUserRepository
    {
        readonly IRepositoryEF<imageUser> repo;
        readonly CW_EntitiesAzure db;

        public ImageUserRepository(CW_EntitiesAzure db) {
            this.db = db;
            this.repo = new IRepositoryEF<imageUser>(db);

        }

        public int Count(string where)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }

            return "" != sWhere ? db.imagebills.Where(sWhere).Count() : db.imagebills.Count();
        }

        public ImageUserEx Create()
        {
            return new ImageUserEx();
        }

        public ImageUserEx Get(int Id)
        {
            var n = db.imageUsers.Where(c => c.id == Id).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

        private ImageUserEx Transform(imageUser n)
        {
            if (n == null)
            {
                return null;
            }

            var l = new ImageUserEx();

            l.id = n.id;
            l.userId = n.userId;
            l.userRoles = n.userRoles;
            l.imageString = n.imageString;
            l.path = n.path;
            return l;
        }

        private imageUser Transform(ImageUserEx n)
        {
            imageUser l;

            if (n.id > 0)
            {
                l = repo.Query().Where(v => v.id == n.id).FirstOrDefault();
            }
            else
            {
                l = repo.Create();
            }
            l.id = n.id;
            l.userId = n.userId;
            l.userRoles = n.userRoles;
            l.imageString = n.imageString;
            l.path = n.path;

            return l;
        }

        public List<ImageUserEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            List<string> aWhere = new List<string>();
            string sWhere = "";
            if (!string.IsNullOrEmpty(where))
            {
                aWhere.Add("(first_name.Contains(\"" + where + "\")");//||Url.Contains(\"" + where + "\"))");
            }
            if (aWhere.Count > 0)
            {
                sWhere = string.Join(" && ", aWhere);
            }
            var r = "" != sWhere ?db.imageUsers.Where(sWhere).OrderBy(orderby) : db.imageUsers.OrderBy(orderby);
            if (PageNo == 0 || PageSize == 0)
            {
                return Transform(r);
            }
            else
            {
                return Transform(r.Skip((PageNo - 1) * PageSize).Take(PageSize));
            }
        }

        public void Remove(ImageUserEx item)
        {
            var g = Transform(item);
            repo.Remove(g);
            repo.SaveChanges();
        }

        public void Save(ImageUserEx item)
        {
            var g = Transform(item);
            if (g.id == 0)
            {
                repo.Add(g);
            }
            else
            {
                repo.Update(g);
            }
            repo.SaveChanges();
            item.id = g.id;
            item.userId = g.userId;
            item.userRoles = g.userRoles;
            item.imageString = g.imageString;
            item.path = g.path;

        }




        private List<ImageUserEx> Transform(IQueryable<imageUser> aImageUser)
        {
            List<ImageUserEx> l = new List<ImageUserEx>();
            foreach (imageUser b in aImageUser)
            {
                l.Add(Transform(b));
            }

            return l;
        }

        public ImageUserEx GetByUserId(int Id, int r)
        {
            var n = db.imageUsers.Where(c => c.userId == Id && c.userRoles == r).FirstOrDefault();
            if (n != null)
            {
                return Transform(n);
            }
            return null;
        }

    }

}
