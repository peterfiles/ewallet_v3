COOPRED AND EWALLET API
=====================

This is an API for Coopred and Ewallet Services.

### Quotes we live
>Always code as if the guy who ends up maintaining your code 
>will be a violent psychopath who knows where you live. 
> By Martin Golding

>Experience is the name everyone gives to their mistakes.
> Oscar Wilde

>Only half of programming is coding. The other 90% is debugging.
> By Peter

>There is nothing quite so permanent as a quick fix.
> By Unknown

>Beta is Latin for still doesn’t work.

>Judge a man by his questions rather than by his answers.
>by Voltaire.

### Dev Version
| Version | Released Date |
| ------ | ------ |
| V1.09 | November 02, 2017 |
| V1.08 | November 02, 2017 |
| V1.07 | October 19, 2017 |
| V1.06 | October 18, 2017 |


### Production Version

| V1.08 | November 02, 2017 |
| V1.07 | October 19, 2017 |
| V1.04 | September 18, 2017 |
| V1.03 | August 30, 2017 |
| V1.02 | August 15, 2017 |
| V1.01 | August 01, 2017 |

### Version Released 

| Version | Fixes |
| ------ | ------ |
| V1.04 | Changes UI |
| V1.04 | Services API Fixes |
| V1.04 | Back End Changes On API fixes - partners |
| V1.04 | Back End Changes On API fixes - pulling of data |
| V1.04 | Back End Changes On API fixes - Encryption |
| V1.04 | Third Party API fixes |
| V1.04 | Email notification |
| V1.04 | SMS length fixes |
| V1.04 | Landing Page fixes and changes|
| V1.04 | Icons changes |
| V1.04 | Added Bank Deposit |
| V1.03 | Fast Loading on Images |
| V1.03 | Loading upon signin |
| V1.03 | UI changes |
| V1.03 | Email notification fixes to all services |
| V1.03 | SMS notification fixes to all services |
| V1.03 | Added Functionalities on Bill Payment telecom |
| V1.03 | Added Functionalities on Top up airtime |
| V1.03 | Backend fixes on - coopred signin |
| V1.03 | Backend fixes on - partners |
| V1.03 | Backend fixes on - encryption |
| V1.03 | Backend fixes on - money transfer |
| V1.03 | Backend fixes on - credit |
| V1.03 | Backend fixes on - Etransfer |
| V1.03 | Backend fixes on - token |
| V1.03 | Backend fixes on - useraccount |
| V1.03 | Backend fixes on - credit card products |
| V1.02 | UI changes |
| V1.02 | Plus sign in all mobile fields |
| V1.02 | Registration fixes |
| V1.02 | Login fixes |
| V1.02 | Themes Fixes for user |
| V1.02 | Backend fixes on - partners, agents, merchants, customers |
| V1.02 | Backend fixes on - user accounts |
| V1.02 | Backend fixes on - procedural |
| V1.02 | Backend fixes on - money transfer, transactions, etransfer |
| V1.02 | Backend fixes on - Email notification and sms notifications |
| V1.02 | Backend fixes on - China remit api |
| V1.02 | Additional services api (third party API integration) |
| V1.01 | Registration Fixes |
| V1.01 | customer data fixes |
| V1.01 | Etransfer Transactions fixes |
| V1.01 | Money Transfer Transaction fixes |
| V1.01 | Services Fixes |
| V1.01 | Backend fixes on - transactions |
| V1.01 | Email and SMS notifications fixes |
| V1.01 | Calculation of services fixes |
| V1.01 | UI changes |
| V1.01 | Removal of old themes template |
| V1.01 | Organizing the template |
| V1.01 | Fixes front end codes |
| V1.01 | Loading fixes |
| V1.01 | Backend fixes on - procedures |
| V1.01 | Backend fixes on - data manipulation |
| V1.01 | Backend fixes on - API dependency |
| V1.01 | Backend fixes on - 3rd party services fixes |
| V1.01 | Backend fixes on - Organization of code |
| V1.01 | Backend fixes on - removal of delete |
| V1.01 | Backend fixes on - transaction calculation |
| V1.01 | Login fixes |
| V1.01 | Based 64 of images |
| V1.01 | Started Project to live |
