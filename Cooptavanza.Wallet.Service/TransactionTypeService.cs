﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class TransactionTypeService
    {
        readonly ITransactionTypeRepository repo;
        public TransactionTypeService(ITransactionTypeRepository repo) {
            this.repo = repo;
        }

        public TransactionTypeEx Get(int Id) {
            return repo.Get(Id);
        }

       public TransactionTypeEx GetByCode(string code)
        {
            return repo.GetByCode(code);
        }

        public List<TransactionTypeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(id, TID, where, orderby, PageNo, PageSize);
        }

        public int Count(int id, string TID, string where)
        {
            return repo.Count(id, TID, where);
        }

        public void Save(TransactionTypeEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
    }
}
