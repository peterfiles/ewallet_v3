﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class UserAccountService
    {
        readonly IUserAccountRepository repo;
        public UserAccountService(IUserAccountRepository repo) {
            this.repo = repo;
        }

        public UserAccountEx Get(int Id) {
            return repo.Get(Id);
        }

        public UserAccountEx GetLogin(string mobile_number, string password) {
            return repo.GetLogin(mobile_number,password);
        }

        public List<UserAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(UserAccountEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public UserAccountEx GetAccountByUserId(int r, int id, string tid)
        {
            return repo.GetAccountByUserId(r, id, tid);
        }

        public UserAccountEx CheckUsername(string username)
        {
            return repo.CheckUsername(username);
        }

      

        public AuthGlobalEx GetAccountByUserPassword(AuthMobileEx n)
        {
           return repo.GetAccountByUserPassword(n);
        }

        public UserAccountEx GetAuthenticate(int v, string password, string tid, int id)
        {
            return repo.GetAuthentication(v, password, tid, id);
        }

        public UserAccountEx GetAdminAuthenticate(string username, string password)
        {
            return repo.GetAdminAuthenticate(username, password);
        }

        public UserAccountEx GetAdminAuthenticate(string username, string password, string corporatename)
        {
            return repo.GetAdminAuthenticate(username, password, corporatename);
        }

        public List<MemberEx> GetAllMember(string str, int psize, int pno)
        {
            return repo.GetAllMember(str,psize,pno);
        }
        public int countmember(string str)
        {
            return repo.countmember(str);
        }

        public List<MemberEx> GetAllMemberByParentID(string str, int psize, int pno, int parentID)
        {
            return repo.GetAllMemberByParentID(str, psize, pno, parentID);
        }
        public int countmemberFilterByParentID(string str, int parentID)
        {
            return repo.countmemberFilterByParentID(str,parentID);
        }

        public List<MemberEx> GetAllMemberByParentIDAndRoleID(string str, int psize, int pno, int parentID, int roleID)
        {
            return repo.GetAllMemberByParentIDAndRoleID(str, psize, pno, parentID, roleID);
        }
        public int countmemberFilterByParentIDAndRoleID(string str, int parentID, int roleID)
        {
            return repo.countmemberFilterByParentIDAndRoleID(str, parentID, roleID);
        }

        
    }
}
