﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class ImageIdCardService
    {
        readonly IImageIdCardRepository repo;
        public ImageIdCardService(IImageIdCardRepository repo) {
            this.repo = repo;
        }

        public ImageIdCardEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<ImageIdCardEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(ImageIdCardEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public ImageIdCardEx GetByUserId(int id)
        {
            return repo.GetByUserId(id);
        }
    }
}
