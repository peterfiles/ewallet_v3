﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{

    public class CountryService
    {
        readonly ICountryRepository repo;
        public CountryService(ICountryRepository repo)
        {
            this.repo = repo;
        }

        public CountryEx Get(int Id)
        {
            return repo.Get(Id);
        }
        public List<CountryEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }

        public List<CountryEx> GetDefaultCountry()
        {
            return repo.GetDefault();
        }

        public List<CountryEx> SearchCountry(string where = "", string orderBy = "")
        {
            return repo.SearchCountry(where, orderBy);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(CountryEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public object getCountryFilterByAgent()
        {
            return repo.getCountryFilterByAgent();
        }

        public CountryEx GetByCountryName(string cname)
        {
            return repo.GetByCountryName(cname);
        }

        public CountryEx GetByCurrencyCode(string curr)
        {
            return repo.GetByCurrencyCode(curr);
        }

        public List<CountryEx> GetCountries()
        {
            return repo.GetCountries();
        }
    }
}
