﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class CustomerService
    {
        readonly ICustomerRepository repo;
        readonly IEnrolledAccountRepository repoEnrolledAccount;
        readonly IImageBillsRepository repoImageBills;
        readonly IImageIdCardRepository repoImageIdCard;
        readonly IImageProfileRepository repoImageProfile;
        readonly IUserAccountRepository repoUserAccount;
        public CustomerService(ICustomerRepository repo, IEnrolledAccountRepository repoEnrolledAccount, IImageBillsRepository repoImageBills,
            IImageIdCardRepository repoImageIdCard, IImageProfileRepository repoImageProfile, IUserAccountRepository repoUserAccount) {
            this.repo = repo;
            this.repoEnrolledAccount = repoEnrolledAccount;
            this.repoImageBills = repoImageBills;
            this.repoImageIdCard = repoImageIdCard;
            this.repoImageProfile = repoImageProfile;
            this.repoUserAccount = repoUserAccount;
        }

        public CustomerEx Get(int Id)
        {
            var customerObject = repo.Get(Id);
            if (customerObject != null)
            {
                customerObject.EnrolledList = repoEnrolledAccount.GetByUserId(customerObject.id);
                customerObject.ImageBill = repoImageBills.GetByUserId(customerObject.id);
                customerObject.ImageIdCard = repoImageIdCard.GetByUserId(customerObject.id);
                customerObject.ImageProfile = repoImageProfile.GetByUserId(customerObject.id);
                return customerObject;
            }
            
            return null;

        }
        public List<CustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(CustomerEx item)
        {
            //if(item.ImageBill != null)
            //{
            //    item.ImageBill.date_created = DateTime.Now;
            //    repoImageBills.Save(item.ImageBill);
            //}
            //if(item.ImageIdCard != null)
            //{
            //    item.ImageIdCard.date_created = DateTime.Now;
            //    repoImageIdCard.Save(item.ImageIdCard);
            //}
            //if(item.ImageProfile != null) {
            //    item.ImageProfile.date_created = DateTime.Now;
            //    repoImageProfile.Save(item.ImageProfile); }
            if(item.EnrolledList != null)
            {
                foreach(var i in item.EnrolledList)
                {
                    if (i.saveStatus == "delete")
                    {
                        var r = repoEnrolledAccount.Get(i.id);
                        if (r != null)
                        {
                            repoEnrolledAccount.Remove(r);
                        }
                    }
                    else { repoEnrolledAccount.Save(i); }
                }
            }
            var tempid = item.id;
            var y = repo.SaveCustomer(item);
            if (tempid == 0)
            {
                var x = new EnrolledAccountEx();
                x.currenycid = item.currencyid;
                x.currency = item.currency;
                x.accountDefault = true;
                x.userId = y.id;
                x.userTID = y.tid;
                repoEnrolledAccount.Save(x);

                //  item.EnrolledList;
            }
           
        }

        public object GetBasic(int user_id)
        {
           return repo.Get(user_id);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
        
        public CustomerEx GetByMobileNumber(string mobileNumber)
        {
            return repo.GetByMobileNumber(mobileNumber);
        }

        public CustomerEx GetLogin(string mobile, string password)
        {
           var n = repo.GetLogin(mobile, password);
           if (n != null) {
                
                //n.EnrolledList = repoEnrolledAccount.GetByUserId(n.id);
                //n.ImageBill = repoImageBills.GetByUserId(n.id);
                //n.ImageIdCard = repoImageIdCard.GetByUserId(n.id);
              //  n.ImageProfile = repoImageProfile.GetByUserId(n.id);
                return n;
                //var ua = repoUserAccount.GetAuthentication(7, password, n.tid, n.id);
                //    if (ua != null)
                //    {
                //        return n;
                //    }
                //    else
                //    {
                //        return null;
                //    }

            }
            return null;
        }
        public CustomerEx GetCheckMobileEmail(string str)
        {
            return repo.GetCheckMobileEmail(str);
        }

        public CustomerEx GetCheckEmail(string str)
        {
            return repo.GetCheckEmail(str);
        }

        public int CountCustomer()
        {
            return repo.CountCustomer();
        }
    }
}
