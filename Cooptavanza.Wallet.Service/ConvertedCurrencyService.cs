﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class ConvertedCurrencyService
    {
        readonly IConvertedCurrencyRepository repo;
        public ConvertedCurrencyService(IConvertedCurrencyRepository repo) {
            this.repo = repo;
        }

        public void Save(ConvertedCurrencyEx item)
        {
            repo.Save(item);
        }
        
        public int Count()
        {
            return repo.Count();
        }

        public ConvertedCurrencyEx getDataByQoute(string qoute)
        {
            return repo.getDataByQoute(qoute);
        }

        public void RefreshConvertedCurrency()
        {
            repo.RefreshConvertedCurrency();
        }
    }
}
