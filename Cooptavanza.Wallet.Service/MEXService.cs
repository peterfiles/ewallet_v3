﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class MEXService
    {
        readonly IMEXAmlStatusRepository repoAmlStatus;
        readonly IMEXIncludeFeesRepository repoIncludeFees;
        readonly IMEXKycCheckRepository repoKycCheck;
        readonly IMEXSndIdTypeRepository repoSndIdType;
        readonly IMEXSndRemitPurposeRepository repoSndRemitPurpose;
        
        public MEXService(IMEXAmlStatusRepository repoAmlStatus,
        IMEXIncludeFeesRepository repoIncludeFees,
        IMEXKycCheckRepository repoKycCheck,
        IMEXSndIdTypeRepository repoSndIdType,
        IMEXSndRemitPurposeRepository repoSndRemitPurpose) {
            this.repoAmlStatus = repoAmlStatus;
            this.repoIncludeFees = repoIncludeFees;
            this.repoKycCheck = repoKycCheck;
            this.repoSndIdType = repoSndIdType;
            this.repoSndRemitPurpose = repoSndRemitPurpose;
        }

        public MEXEx MEXPage()
        {
            string where = "";
            string orderby = "ID Asc";
            int PageNo = 1;
            int PageSize = 30;
            var a = new MEXEx();
            a.MAS = repoAmlStatus.GetPaged(where, orderby, PageNo, PageSize);
            a.IF = repoIncludeFees.GetPaged(where, orderby, PageNo, PageSize);
            a.KC = repoKycCheck.GetPaged(where, orderby, PageNo, PageSize);
            a.SIT = repoSndIdType.GetPaged(where, orderby, PageNo, PageSize);
            a.SRP = repoSndRemitPurpose.GetPaged(where, orderby, PageNo, PageSize);

            return a;
        }

        public MEXAmlStatusEx GetAmlStatusById(int Id) {
            return repoAmlStatus.Get(Id);
        }

        public MEXIncludeFeesEx GetIncludeFeesById(int Id)
        {
            return repoIncludeFees.Get(Id);
        }
        public MEXKycCheckEx GetKycCheckById(int Id)
        {
            return repoKycCheck.Get(Id);
        }
        public MEXSndIdTypeEx GetSndIdTypeById(int Id)
        {
            return repoSndIdType.Get(Id);
        }
        public MEXSndRemitPurposeEx GetSndRemitPurposeById(int Id)
        {
            return repoSndRemitPurpose.Get(Id);
        }

        public List<MEXAmlStatusEx> GetAmlStatusPage(string where, string orderby, int PageNo, int PageSize)
        {
            return repoAmlStatus.GetPaged(where, orderby, PageNo, PageSize);
        }

        public List<MEXIncludeFeesEx> GetIncludeFeesPage(string where, string orderby, int PageNo, int PageSize)
        {
            return repoIncludeFees.GetPaged(where, orderby, PageNo, PageSize);
        }
        public List<MEXKycCheckEx> GetKycCheckPage(string where, string orderby, int PageNo, int PageSize)
        {
            return repoKycCheck.GetPaged(where, orderby, PageNo, PageSize);
        }
        public List<MEXSndIdTypeEx> GetSndIdTypePage(string where, string orderby, int PageNo, int PageSize)
        {
            return repoSndIdType.GetPaged(where, orderby, PageNo, PageSize);
        }
        public List<MEXSndRemitPurposeEx> GetSndRemitPurposePage(string where, string orderby, int PageNo, int PageSize)
        {
            return repoSndRemitPurpose.GetPaged(where, orderby, PageNo, PageSize);
        }

     
    }
}
