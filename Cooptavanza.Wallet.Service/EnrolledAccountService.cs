﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class EnrolledAccountService
    {
        readonly IEnrolledAccountRepository repo;
        public EnrolledAccountService(IEnrolledAccountRepository repo) {
            this.repo = repo;
        }

        public EnrolledAccountEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<EnrolledAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(EnrolledAccountEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public List<EnrolledAccountEx> GetByUserID(int userID)
        {
            return repo.GetByUserId(userID);
        }

        public List<EnrolledAccountEx> GetByIdAndISO(int id, string iso)
        {
            return repo.GetByIdAndISO(id, iso);
        }

        public int retID()
        {
            return repo.retID();
        }

        public List<EnrolledAccountEx> GetByUserId(int id)
        {

            return repo.GetByUserId(id);
        }

        public EnrolledAccountEx GetByUserIDAndCurrencyID(int userID, string iso)
        {
            return repo.GetByUserIDAndCurrencyID(userID, iso);
        }

        public EnrolledAccountEx GetByUserIDUserTIDAndCurrencyISO(int userID, string tID, string iso)
        {
            return repo.GetByUserIDUserTIDAndCurrencyISO(userID, tID, iso);
        }
    }
}
