﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class PartnersService
    {
        readonly IPartnersRepository repo;
        public PartnersService(IPartnersRepository repo) {
            this.repo = repo;
        }

        public PartnersEx Get(int Id) {
            return repo.Get(Id);
        }

      
        public List<PartnersEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(PartnersEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public PartnersEx getByPartnerCode(string partnerCode)
        {
            return repo.getByPartnerCode(partnerCode);
        }

        public List<PartnersEx> getAllMasterPartner()
        {
            return repo.getAllMasterPartner();
        }

        public int CountPartners()
        {
            return repo.CountPartners();
        }
    }
}
