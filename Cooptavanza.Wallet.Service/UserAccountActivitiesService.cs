﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class UserAccountActivitiesService
    {
        readonly IUserAccountActivitiesRepository repo;
        readonly ICustomerRepository repoCustomer;
        readonly IPartnersRepository repoPartner;
        readonly IAgentRepository repoAgent;
        readonly IMerchantRepository repoMerchant;
        readonly IUsersRepository repoUser;
        readonly ITransactionLogsRepository repoTxLog;

        public UserAccountActivitiesService(IUserAccountActivitiesRepository repo, ICustomerRepository repoCustomer,
            IPartnersRepository repoPartner,
            IAgentRepository repoAgent,
            IMerchantRepository repoMerchant,
            IUsersRepository repoUser,
            ITransactionLogsRepository repoTxLog) {
            this.repo = repo;
            this.repoCustomer = repoCustomer;
            this.repoPartner = repoPartner;
            this.repoAgent = repoAgent;
            this.repoMerchant = repoMerchant;
            this.repoUser = repoUser;
            this.repoTxLog = repoTxLog;
        }

        public void Save(UserAccountActivitiesEx item)
        {
            repo.Save(item);
        }
        
        public List<UserAccountActivitiesEx> GetPaged(string where, string orderBy, int pageNo, int pageSize)
        {
            List<UserAccountActivitiesEx> ret = new List<UserAccountActivitiesEx>();
            List<TransactionLogEx> retTx = new List<TransactionLogEx>();
            retTx = repoTxLog.GetPaged(where, "logDate Desc", pageNo, pageSize);


            ret = repo.GetPaged(where, orderBy, pageNo, pageSize);
            foreach(var r in ret)
            {
                r.type = "NEWENROLLED";
                switch (r.userRole)
                {
                    case 1:
                        break;
                    case 2:

                        break;
                    case 3:
                        var y = repoPartner.Get(r.userId);
                        r.name = y.finame;
                        break;
                    case 4:
                        var z = repoAgent.Get(r.userId);
                        r.name = z.name;
                        break;
                    case 5:
                        var w = repoMerchant.Get(r.userId);
                        r.name = w.firstname + " " + w.lastname;
                        break;
                    case 6:
                        break;
                    case 7:
                        var x = repoCustomer.Get(r.userId);
                        r.name = x.firstname + " " + x.lastname;
                        break;
                    default:
                    break;
                }
            }

            foreach (var r in retTx) {
                UserAccountActivitiesEx rr = new UserAccountActivitiesEx();
                rr.type = "TRANSACTION";
                rr.name = r.moduleName;
                rr.status = r.status;
                rr.userId = r.userId ?? 0;
                rr.userRole = r.userRole ?? 0;
                rr.dateCreated = r.logDate ?? DateTime.Now;
                ret.Add(rr);
            }
            return ret; 
        }

       
    }
}
