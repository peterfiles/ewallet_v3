﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class UsersService
    {
        readonly IUsersRepository repo;
        public UsersService(IUsersRepository repo) {
            this.repo = repo;
        }

        public UsersEx Get(int Id) {
            return repo.Get(Id);
        }

        public UsersEx GetLogin(string mobile_number, string password) {
            return repo.GetLogin(mobile_number,password);
        }

        public List<UsersEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(UsersEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
    }
}
