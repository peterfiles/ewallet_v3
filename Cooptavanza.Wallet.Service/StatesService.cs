﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class StatesService
    {
        readonly IStatesRepository repo;
        public StatesService(IStatesRepository repo) {
            this.repo = repo;
        }

        public StatesEx Get(int Id) {
            return repo.Get(Id);
        }

      
        public List<StatesEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(StatesEx item)
        {
            repo.Save(item);
        }

        public List<StatesEx> GetStatesByCountryId(int id)
        {
           return repo.GetStatesByCountryId(id);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public StatesEx GetByStateName(string sname)
        {
            return repo.GetByStateName(sname);
        }

        public object getAgentStatesByCountryID(int cID)
        {
            return repo.getAgentStatesByCountryID(cID);
        }
    }
}
