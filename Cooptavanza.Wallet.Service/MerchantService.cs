﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class MerchantService
    {
        readonly IMerchantRepository repo;
        public MerchantService(IMerchantRepository repo) {
            this.repo = repo;
        }

        public MerchantEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<MerchantEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(MerchantEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public int CountMerchant()
        {
            return repo.CountMerchant();
        }
    }
}
