﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class OTPAccountService
    {
        readonly IOTPAccountRepository repo;
        public OTPAccountService(IOTPAccountRepository repo) {
            this.repo = repo;
        }

        public OTPAccountEx Get(int Id) {
            return repo.Get(Id);
        }

        public List<OTPAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(OTPAccountEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public OTPAccountEx GetLogin(string username, string password, DateTime dateTime)
        {
            return repo.GetLogin(username, password, dateTime);
        }

        public OTPAccountEx getByDUR(string deviceId, int id, int role)
        {
            return repo.getByDUR(deviceId, id, role);
        }
        public OTPAccountEx getByUR(int id, int role)
        {
            return repo.getByUR(id, role);
        }
    }
}
