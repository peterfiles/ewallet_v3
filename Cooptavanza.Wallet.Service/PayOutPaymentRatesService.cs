﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class PayOutPaymentRatesService
    {
        readonly IPayOutPaymentRatesRepository payOutRepo;
        public PayOutPaymentRatesService(IPayOutPaymentRatesRepository payOutRepo) {
            this.payOutRepo = payOutRepo;
        }

        public PaymentRatesForPayoutsEx Get(int Id) {
            return payOutRepo.GetByID(Id);
        }
        
        public void Save(PaymentRatesForPayoutsEx item)
        {
            payOutRepo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = payOutRepo.GetByID(Id);
            payOutRepo.Remove(t);
        }
        
    }
}
