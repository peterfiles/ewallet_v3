﻿using System.Collections.Generic;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class PartnerCustomerService
    {
        readonly IPartnerCustomerRepository repo;
        public PartnerCustomerService(IPartnerCustomerRepository repo) {
            this.repo = repo;
        }

        public PartnerCustomerEx Get(int Id) {
            return repo.Get(Id);
        }

      
        public List<PartnerCustomerEx> GetPaged(string where, int partnerId, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, partnerId,orderby, PageNo, PageSize);
        }
        public int Count(string where, int partnerId)
        {
            return repo.Count(where, partnerId);
        }

        public void Save(PartnerCustomerEx item)
        {
           repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
    }
}
