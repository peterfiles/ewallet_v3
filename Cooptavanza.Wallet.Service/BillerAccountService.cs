﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{

    public class BillerAccountService
    {
        readonly IBillerAccountRepository repo;
        public BillerAccountService(IBillerAccountRepository repo) {
            this.repo = repo;
        }

        public BillerAccountEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<BillerAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(BillerAccountEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public BillerAccountEx GetBillerByAccountNumber(string bA)
        {
            return repo.GetBillerByAccountNumber(bA);
        }

        public BillerAccountEx GetBillerByCountryStateBranchISO(int cID, int sID, string branch, string iso)
        {
            return repo.GetBillerByCountryStateBranchISO(cID, sID, branch, iso);
        }

        public int CountBillerAccount()
        {
            return repo.CountBillerAccount();
        }
    }
}
