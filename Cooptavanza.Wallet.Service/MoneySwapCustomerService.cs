﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class MoneySwapCustomerService
    {
        readonly IMoneySwapCustomerRepository repo;
        readonly IEnrolledAccountRepository repoEnrolledAccount;
        readonly IImageBillsRepository repoImageBills;
        readonly IImageIdCardRepository repoImageIdCard;
        readonly IImageProfileRepository repoImageProfile;
        readonly IUserAccountRepository repoUserAccount;
        public MoneySwapCustomerService(IMoneySwapCustomerRepository repo, IEnrolledAccountRepository repoEnrolledAccount, IImageBillsRepository repoImageBills,
            IImageIdCardRepository repoImageIdCard, IImageProfileRepository repoImageProfile, IUserAccountRepository repoUserAccount) {
            this.repo = repo;
            this.repoEnrolledAccount = repoEnrolledAccount;
            this.repoImageBills = repoImageBills;
            this.repoImageIdCard = repoImageIdCard;
            this.repoImageProfile = repoImageProfile;
            this.repoUserAccount = repoUserAccount;
        }

        public MoneySwapCustomerEx Get(int Id)
        {
            var MoneySwapCustomerObject = repo.Get(Id);
            if (MoneySwapCustomerObject != null)
            {
                MoneySwapCustomerObject.EnrolledList = repoEnrolledAccount.GetByUserId(MoneySwapCustomerObject.id);
                MoneySwapCustomerObject.ImageBill = repoImageBills.GetByUserId(MoneySwapCustomerObject.id);
                MoneySwapCustomerObject.ImageIdCard = repoImageIdCard.GetByUserId(MoneySwapCustomerObject.id);
                MoneySwapCustomerObject.ImageProfile = repoImageProfile.GetByUserId(MoneySwapCustomerObject.id);
                return MoneySwapCustomerObject;
            }
            
            return null;

        }
        public List<MoneySwapCustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(MoneySwapCustomerEx item)
        {
            //if(item.ImageBill != null)
            //{
            //    item.ImageBill.date_created = DateTime.Now;
            //    repoImageBills.Save(item.ImageBill);
            //}
            //if(item.ImageIdCard != null)
            //{
            //    item.ImageIdCard.date_created = DateTime.Now;
            //    repoImageIdCard.Save(item.ImageIdCard);
            //}
            //if(item.ImageProfile != null) {
            //    item.ImageProfile.date_created = DateTime.Now;
            //    repoImageProfile.Save(item.ImageProfile); }
            if(item.EnrolledList != null)
            {
                foreach(var i in item.EnrolledList)
                {
                    if (i.saveStatus == "delete")
                    {
                        var r = repoEnrolledAccount.Get(i.id);
                        if (r != null)
                        {
                            repoEnrolledAccount.Remove(r);
                        }
                    }
                    else { repoEnrolledAccount.Save(i); }
                }
            }
            var tempid = item.id;
            var y = repo.SaveMoneySwapCustomer(item);
            if (tempid == 0)
            {
                var x = new EnrolledAccountEx();
                x.currenycid = item.currencyid;
                x.currency = item.currency;
                x.accountDefault = true;
                x.userId = y.id;
                x.userTID = y.tid;
                repoEnrolledAccount.Save(x);

                //  item.EnrolledList;
            }
           
        }

        public object GetBasic(int user_id)
        {
           return repo.Get(user_id);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
        
        public MoneySwapCustomerEx GetByMobileNumber(string mobileNumber)
        {
            return repo.GetByMobileNumber(mobileNumber);
        }

       
        public MoneySwapCustomerEx GetCheckMobileEmail(string str)
        {
            return repo.GetCheckMobileEmail(str);
        }

        public MoneySwapCustomerEx GetCheckEmail(string str)
        {
            return repo.GetCheckEmail(str);
        }
    }
}
