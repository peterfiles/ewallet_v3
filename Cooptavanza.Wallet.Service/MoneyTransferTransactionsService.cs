﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class MoneyTransferTransactionsService
    {
        readonly IMoneyTransferTransactionsRepository repo;
        public MoneyTransferTransactionsService(IMoneyTransferTransactionsRepository repo) {
            this.repo = repo;
        }

        public MoneyTransferTransactionsEx Get(int Id) {
            return repo.Get(Id);
        }

       

        public List<MoneyTransferTransactionsEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(id, TID, where, orderby, PageNo, PageSize);
        }
        public int Count(int id, string TID, string where)
        {
            return repo.Count(id, TID, where);
        }

        public void Save(MoneyTransferTransactionsEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public object getMoneyTransferTransactionsByTrackingNumberAndPassword(string accountNumber, string password)
        {
            return repo.getMoneyTransferTransactionsByTrackingNumberAndPassword(accountNumber, password);
        }
    }
}
