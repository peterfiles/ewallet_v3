﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class UsersTempService
    {
        readonly IUsersTempRepository repo;
        public UsersTempService(IUsersTempRepository repo) {
            this.repo = repo;
        }

        public UsersTempEx Get(int Id) {
            return repo.Get(Id);
        }

        public UsersTempEx GetLogin(string mobile_number, string password) {
            return repo.GetLogin(mobile_number,password);
        }

        public List<UsersTempEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(UsersTempEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
    }
}
