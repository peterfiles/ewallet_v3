﻿using System.Collections.Generic;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class PaymentBreakdownService
    {
        readonly IPaymentBreakdownRepository repo;
        public PaymentBreakdownService(IPaymentBreakdownRepository repo) {
            this.repo = repo;
        }

        public PaymentBreakdownEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<PaymentBreakdownEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(PaymentBreakdownEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public List<PaymentBreakdownEx> GetCommissionByPaymentBreakdown(int uID, int rID)
        {
            return repo.GetCommissionByPaymentBreakdown(uID, rID);
        }

        public List<PaymentBreakdownEx> GetPaymentBreakdownByTransactionID(string transactionID, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaymentBreakdownByTransactionID(transactionID,  orderby,  PageNo,  PageSize);
        }

    }
}
