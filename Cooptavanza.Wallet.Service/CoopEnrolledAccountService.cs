﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class CoopEnrolledAccountService
    {
        readonly ICoopEnrolledAccountRepository repo;
        public CoopEnrolledAccountService(ICoopEnrolledAccountRepository repo) {
            this.repo = repo;
        }
        
        public void Save(CoopEnrolledAccountEx item)
        {
            repo.Save(item);
        }
        
        public string getAccountNumberForCoopRed(int userID, int currID, int roleID, int coopEnrolledID)
        {
            return repo.getAccountNumberForCoopRed(userID, currID, roleID, coopEnrolledID);
        }

        public List<AccountListEx> getCoopAccountList(int userID, int roleID)
        {
            return repo.getCoopAccountList(userID, roleID);
        }

        public CoopEnrolledAccountEx getIdByUserIDRoleIDCurrency(int userID, int roleID, string iso)
        {
            return repo.getIdByUserIDRoleIDCurrency(userID, roleID, iso);
        }

        public CoopEnrolledAccountEx Get(int Id)
        {
            return repo.Get(Id);
        }
    }
}
