﻿using System.Collections.Generic;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class BankDepositService
    {
        readonly IBankDepositRepository repo;
        public BankDepositService(IBankDepositRepository repo) {
            this.repo = repo;
        }

        public BankDepositEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<BankDepositEx> GetPaged(string where, string orderby, int PageNo, int PageSize, int userID, int roleID)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize, userID, roleID);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(BankDepositEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public List<BankDepositEx> GetAllCountry()
        {
            return repo.GetAllCountry();
        }

        public List<BankDepositEx> GetBankNameByCountryID(int countryID)
        {
            return repo.GetBankNameByCountryID(countryID);
        }

        public List<BankDepositEx> GetBankNameByCountryIDAndBankName(int countryID, string bankName)
        {
            return repo.GetBankNameByCountryIDAndBankName(countryID, bankName);
        }

        public List<BankDepositEx> GetBankByCountry()
        {
            return repo.GetBankByCountry();
        }

        public List<BankDepositEx> CustomListGetPaged(string str, int psize, int pno)
        {
            return repo.CustomListGetPaged(str, psize, pno);
        }

        public int CustomListGetPagedCount(string str)
        {
            return repo.CustomListGetPagedCount(str);
        }

        public BankDepositEx GetByRtCode(string rtCode)
        {
            return repo.GetByRtCode(rtCode);
        }
    }
}
