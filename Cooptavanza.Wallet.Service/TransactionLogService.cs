﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class TransactionLogService
    {
        readonly ITransactionLogsRepository repo;
        public TransactionLogService(ITransactionLogsRepository repo) {
            this.repo = repo;
        }

        public void Save(TransactionLogEx item)
        {
            repo.Save(item);
        }
        
       public TransactionLogEx GetGetModuleName(string modname)
        {
            return repo.GetModuleName(modname);
        }

        public void SaveLog(string log, string moduleName)
        {
            repo.SaveLog(log, moduleName);
        }

    }
}
