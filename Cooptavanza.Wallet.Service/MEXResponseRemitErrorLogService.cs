﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class MEXResponseRemitErrorLogService
    {
        readonly IMEXResponseRemitErrorLogRepository repo;
        public MEXResponseRemitErrorLogService(IMEXResponseRemitErrorLogRepository repo) {
            this.repo = repo;
        }

        public MEXResponseRemitErrorLogEx Get(int Id) {
            return repo.Get(Id);
        }


        public List<MEXResponseRemitErrorLogEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public int GetCount()
        {
            return repo.GetCount();
        }

        public void Save(MEXResponseRemitErrorLogEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        
    }
}
