﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class MEXRemitValidateService
    {
        readonly IMEXRemitValidateRepository repo;
        public MEXRemitValidateService(IMEXRemitValidateRepository repo) {
            this.repo = repo;
        }

        public MoneyExpressRemitEx Get(int Id) {
            return repo.Get(Id);
        }


        public List<MoneyExpressRemitEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public int GetLastId()
        {
            return repo.GetLastId();
        }

        public void Save(MoneyExpressRemitEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        
    }
}
