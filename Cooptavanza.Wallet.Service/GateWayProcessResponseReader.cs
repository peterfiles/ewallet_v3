﻿using Cooptavanza.Wallet.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace Cooptavanza.Wallet.Service
{
    public class GateWayProcessResponseReader
    {
        public GateWayProcessResponseReader()
        {

        }
        public static GateWayProcessPaymentRequestEx data;
        public GateWayProcessPaymentRequestEx response(string res)
        {
            StringBuilder sb = new StringBuilder();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(res);

            //sb.Append("{data: [{");
            XmlNodeList elemList = doc.GetElementsByTagName("return");
            List<object> getAllObjectName = new List<object>();
            List<object> getAllObjectInnerXML = new List<object>();
            GateWayProcessPaymentRequestEx combineResult = new GateWayProcessPaymentRequestEx();
            for (int i = 0; i < elemList.Count; i++)
            {
                if (elemList[0].HasChildNodes)
                {
                    for (int s = 0; s < elemList[0].ChildNodes.Count; s++)
                    {
                        if (elemList[0].ChildNodes[s].HasChildNodes)
                        {
                            getAllObjectName.Add(elemList[0].ChildNodes[s].Name);
                            getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].InnerXml);

                                for (int t = 0;t< elemList[0].ChildNodes[s].ChildNodes.Count; t++)
                                {
                                    getAllObjectName.Add(elemList[0].ChildNodes[s].ChildNodes[t].Name);
                                    getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].ChildNodes[t].InnerXml);
                                }
                        }
                        else
                        {
                            getAllObjectName.Add(elemList[0].ChildNodes[s].Name);
                            getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].InnerXml);
                        }
                    }
                }
            }
            
            for (int u = 0; u < getAllObjectName.Count(); u++)
            {
                if (getAllObjectName.ElementAt(u).Equals("status")) { combineResult.responseStatus = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("txid")) { combineResult.responseTransactionID = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("tx_action")) { combineResult.responseTransactionAction = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("amount")) { combineResult.responseAmount = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("currency")) { combineResult.responseCurrency = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("type")) { combineResult.responseType = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("info")) { combineResult.responseInfo = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("code")) { combineResult.responseErrCode = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("comment")) { combineResult.response_comment = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("descriptor")) { combineResult.response_descriptor = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("rebillkey")) { combineResult.response_rebillkey = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("required")) { combineResult.response_required = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("TermUrl")) { combineResult.response_TermUrl = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("error")) { combineResult.response_error = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("sys")) { combineResult.response_sys = getAllObjectInnerXML.ElementAt(u).ToString(); }
                if (getAllObjectName.ElementAt(u).Equals("msg")) { combineResult.response_msg = getAllObjectInnerXML.ElementAt(u).ToString(); }

            }
            return combineResult;
        }

        public GateWayProcessPaymentRequestEx GateWayProcessPaymentSetParams(GateWayProcessPaymentRequestEx n)
        {
            data = new GateWayProcessPaymentRequestEx
            {
                address = n.address,
                amount_purchase = n.amount_purchase,
                amount_shipping = n.amount_shipping,
                amount_tax = n.amount_tax,
                amount_unit = n.amount_unit,
                amount_refund = n.amount_refund,
                amount_rebill = n.amount_rebill,
                amount_schedule = n.amount_schedule,

                card_ccv = n.card_ccv,
                card_exp_month = n.card_exp_month,
                card_exp_year = n.card_exp_year,
                card_name = n.card_name,
                card_no = n.card_no,
                country = n.country,
                currency_code = n.currency_code,
                card_types = n.card_types,
                currency = n.currency,

                description = n.description,

                email = n.email,

                firstname = n.firstname,
                filter_search = n.filter_search,
                filter_type = n.filter_type,
                frequency = n.frequency,
                frequencyunit = n.frequencyunit,
                from_timestamp = n.from_timestamp,

                itemName = n.itemName,
                itemQuantity = n.itemQuantity,
                item_desc = n.item_desc,
                item_no = n.item_no,
                item_no_sched = n.item_no_sched,

                lastname = n.lastname,
                limit = n.limit,

                merchantrebilling = n.merchantrebilling,

                payby = n.payby,
                phone = n.phone,
                postcode = n.postcode,
                parent_txid = n.parent_txid,
                postbackurl = n.postbackurl,
                fraud_amount_purchase = n.fraud_amount_purchase,

                quantity = n.quantity,

                rcode = n.rcode,
                reason = n.reason,
                rebillkey = n.rebillkey,
                reference = n.reference,
                repeat = n.repeat,
                responses = n.responses,
                rid = n.rid,

                state = n.state,
                suburb_city = n.suburb_city,
                sid = n.sid,
                sendNotification = n.sendNotification,
                startdate = n.startdate,

                txid = n.txid,
                to_timestamp = n.to_timestamp,
                tx_actions = n.tx_actions,

                uip = n.uip,

                verifyItemName = n.verifyItemName,
                verifyItemValue = n.verifyItemValue
            };
            return data;
        }

    }
}