﻿
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class UserThemeService
    {
        readonly IUserThemeRepository repo;
        public UserThemeService(IUserThemeRepository repo) {
            this.repo = repo;
        }

        public UserThemeEx Get(int Id) {
            return repo.Get(Id);
        }
        public UserThemeEx GetByUserId(int Id)
        {
            return repo.GetByUserId(Id);
        }

        public void Save(UserThemeEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
    }
}
