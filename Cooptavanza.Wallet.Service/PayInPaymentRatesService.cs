﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class PayInPaymentRatesService
    {
        readonly IPayInPaymentRatesRepository payInRepo;
        public PayInPaymentRatesService(IPayInPaymentRatesRepository payInRepo) {
            this.payInRepo = payInRepo;
        }

        public PaymentRatesForPayinsEx Get(int Id) {
            return payInRepo.GetByID(Id);
        }
        
        public void Save(PaymentRatesForPayinsEx item)
        {
            payInRepo.Save(item);
        }

        public List<PaymentRatesForPayinsEx> GetByCountryID(int id)
        {
            return payInRepo.GetByCountryID(id);
        }

        public List<PaymentRatesForPayinsEx> GetByCountryName(string name)
        {
            return payInRepo.GetByCountryName(name);
        }

        public List<PaymentRatesForPayinsEx> GetByMethod(string method)
        {
            return payInRepo.GetByMethod(method);
        }

        public List<PaymentRatesForPayinsEx> GetByLocalPaymentService(string localPaymentService)
        {
            return payInRepo.GetByLocalPaymentService(localPaymentService);
        }

        public void Remove(int Id)
        {
            var t = payInRepo.GetByID(Id);
            payInRepo.Remove(t);
        }
        
    }
}
