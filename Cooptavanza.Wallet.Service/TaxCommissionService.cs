﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{

    public class TaxCommissionService
    {
        readonly ITaxCommissionRepository repo;
        public TaxCommissionService(ITaxCommissionRepository repo)
        {
            this.repo = repo;
        }

        public TaxCommissionEx Get(int Id)
        {
            return repo.Get(Id);
        }
        public List<TaxCommissionEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(TaxCommissionEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }
    }
}
