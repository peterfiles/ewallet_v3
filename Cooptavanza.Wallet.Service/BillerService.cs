﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class BillerService
    {
        readonly IBillerRepository repo;
        public BillerService(IBillerRepository repo) {
            this.repo = repo;
        }

        public BillerEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<BillerEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(BillerEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public object GetBillerCountry()
        {
            return repo.GetBillerCountry();
        }

        public object GetBillerTypeByCountry(int id)
        {
            return repo.GetBillerTypeByCountry(id);
        }

        public object GetBillerByCountryAndType(int cID, int bTID)
        {
            return repo.GetBillerByCountryAndType(cID, bTID);
        }
    }
}
