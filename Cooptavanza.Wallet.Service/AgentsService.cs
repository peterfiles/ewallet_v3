﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class AgentsService
    {
        readonly IAgentRepository repo;
        public AgentsService(IAgentRepository repo) {
            this.repo = repo;
        }

        public AgentsEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<AgentsEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(AgentsEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public List<AgentsEx> getAgentByCountryIDAndStateID(int cID , int sID)
        {
            return repo.getAgentByCountryIDAndStateID(cID, sID);
        }

        public int CountAgents()
        {
            return repo.CountAgents();
        }
    }
}
