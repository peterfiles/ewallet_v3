﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class TransferMoneyService
    {
        readonly ITransferMoneyRepository repo;
        public TransferMoneyService(ITransferMoneyRepository repo) {
            this.repo = repo;
        }

        public TransferMoneyEx Get(int Id) {
            return repo.Get(Id);
        }

       

        public List<TransferMoneyEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            // return repo.GetPaged(id, TID, where, orderby, PageNo, PageSize);
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(TransferMoneyEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }


        public TransferMoneyEx GetTransferMoneyByTk(string tk, int id)
        {
            return repo.GetTransferMoney(tk, id);
        }

        public TransferMoneyEx GetTransferMoneyByTk(string tk, int id, int p)
        {
            return repo.GetTransferMoney(tk, id, p);
        }

        public TransferMoneyEx GetTransferMoneyWithOtp(string o, string tk, int id)
        {
            return repo.GetTransferMoneyWithOtp(o,tk, id);
        }
        
        public TransferMoneyEx GetTransferMoneyByTransactionTID(string tID)
        {
            return repo.GetTransferMoneyByTransactionTID(tID);
        }


    }
}
