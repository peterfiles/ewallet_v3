﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class TransactionService
    {
        readonly ITransactionRepository repo;
        public TransactionService(ITransactionRepository repo) {
            this.repo = repo;
        }
        
        public TransactionEx Get(int Id) {
            return repo.Get(Id);
        }

        public List<BalanceEx> GetBalance(int id)
        {
            return repo.GetBalance(id);
        }

        public List<BalanceEx> GetBalancePending(int id)
        {
            return repo.GetBalancePending(id);
        }
        public List<BalanceEx> GetBalanceOverall(int id)
        {
            return repo.GetBalanceOverall(id);
        }
        
        public List<TransactionEx> GetByPartnerIDAndTransactionType(int userID, int userRole, int PageNo, int PageSize, string ttype)
        {
            return repo.GetByPartnerIDAndTransactionType(userID, userRole, PageNo, PageSize, ttype);
        }


        public List<TransactionEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(id, TID, where, orderby, PageNo, PageSize);
        }
        public int Count(int id, string TID, string where)
        {
            return repo.Count(id, TID, where);
        }

        public void Save(TransactionEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public int GetTransactionLastIDNumber()
        {
            return repo.GetTransactionLastIDNumber();
        }

        public List<GetTypeTransactionEx> GetTransactionType()
        {
            return repo.GetTransactionType();
        }

        public List<GetTransactionListByTypeEx> GetTransactionFilterByTransactionType(string tType, string status)
        {
            return repo.GetTransactionFilterByTransactionType(tType, status);
        }

        public List<TransactionEx> GetFilterTransactionPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetFilterTransactionPaged(where, orderby, PageNo, PageSize);
        }
        public List<TransactionEx> GetTransactionByType(string SearchText, string orderby, int PageNo, int PageSize)
        {
            return repo.GetTransactionByType( SearchText, orderby,  PageNo,  PageSize);
        }

        public List<TransactionEx> GetByPartner(string searchText = "", int partnerId = 0,string dateStart = "", string dateEnd = "", string transactionType = "",string referenceCode = "", string orderBy = "Id Asc", int pageNo = 1, int pageSize = 10)
        {
            return repo.GetByPartner(searchText, partnerId, dateStart, dateEnd, transactionType, referenceCode, orderBy, pageNo, pageSize);
        }

        public bool CheckTransactionId(string referenceNo, int partnerid)
        {
            return repo.CheckTransactionId(referenceNo, partnerid);
        }

        public int CountByPartner(string searchText ="", int partnerId = 0, string dateStart = "", string dateEnd = "", string transactionType = "", string referenceCode = "")
        {
            return repo.CountByPartner(searchText, partnerId, dateStart, dateEnd, transactionType, referenceCode);
        }

        public TransactionEx GetStatusByPartner(int id, string referenceNo)
        {
            return repo.getStatusByPartner(id, referenceNo);
        }

        public Boolean verify(int Id, decimal txAmnt, string isoCurrency)
        {
            var ret = false;
            List<BalanceEx> BalanceList = GetBalanceOverall(Id);
            if (BalanceList != null)
            {
                foreach (var each in BalanceList)
                {
                    if (each.currency == isoCurrency)
                    {
                        ret = each.balance > txAmnt ? true : false;
                        if (ret)
                        {
                            return ret;
                        }
                    }
                    else
                    {
                        ret = false;
                    }
                }
            }
            else
            {
                ret = false;
            }

            return ret;
        }

        public List<ChartsEx> GetChartsList(DateTime d1, DateTime d2)
        {
            return repo.GetChartsList(d1, d2);
        }

        public List<TransactionEx> GetTransactionByTID(string tid, string orderby, int PageNo, int PageSize)
        {
            return repo.GetTransactionByTID(tid, orderby, PageNo, PageSize);
        }
    }
}
