﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class CommissionsService
    {
        readonly ICommissionsRepository repo;
        public CommissionsService(ICommissionsRepository repo) {
            this.repo = repo;
        }

        public void Save(CommissionsEx item)
        {
            repo.Save(item);
        }

        public List<CommissionsEx> GetCommissionListWithFilter(int userID, int userRole, string orderby, int PageNo, int PageSize)
        {
            return repo.GetCommissionListWithFilter(userID, userRole, orderby, PageNo, PageSize);
        }

        public List<CommissionsEx> GetCommissionListWithFilterByUserIDRoleIDMethod(int userID, int userRole, string method, string orderby, int PageNo, int PageSize)
        {
            return repo.GetCommissionListWithFilterByUserIDRoleIDMethod(userID, userRole, method, orderby, PageNo, PageSize);
        }

        public List<TotalCommissionEx> getTotalCommissionByUserIDRoleIDAndMethod(int userID, int roleID, string method)
        {
            return repo.getTotalCommissionByUserIDRoleIDAndMethod(userID, roleID, method);
        }

        public CommissionsEx GetCommissionAccountNumberByMethodAndCurrency(string currency, string method)
        {
            return repo.GetCommissionAccountNumberByMethodAndCurrency(currency, method);
        }

        public List<TotalCommissionByCurrencyEx> GetTotalCommByUidAndRidGroupByCurr(int uID, int rID)
        {
            return repo.GetTotalCommByUidAndRidGroupByCurr(uID, rID);
        }

    }
}
