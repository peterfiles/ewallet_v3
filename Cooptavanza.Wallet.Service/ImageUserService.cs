﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class ImageUserService
    {
        readonly IImageUserRepository repo;
        public ImageUserService(IImageUserRepository repo) {
            this.repo = repo;
        }

        public ImageUserEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<ImageUserEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(ImageUserEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public ImageUserEx GetByUserId(int id, int r)
        {
            return repo.GetByUserId(id,r);
        }

        
    }
}
