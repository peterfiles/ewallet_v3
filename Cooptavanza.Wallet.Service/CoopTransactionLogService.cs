﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class CoopTransactionLogService
    {
        readonly ICoopTransactionLogRepository repo;
        public CoopTransactionLogService(ICoopTransactionLogRepository repo) {
            this.repo = repo;
        }
       

        public void Save(CoopTransactionLogEx item)
        {
            repo.Save(item);
        }

        public List<CoopTransactionListEx> getCoopTransactionList(string str, int psize, int pno, int userID, int roleID)
        {
            return repo.getCoopTransactionList(str, psize, pno, userID, roleID);
        }

        public int getcountTransactionListUserIDAndRoleID(string str, int userID, int roleID)
        {
            return repo.getcountTransactionListUserIDAndRoleID(str, userID, roleID);
        }

        public void Remove(int id)
        {
            repo.Remove(repo.Get(id));
        }

        public CoopTransactionLogEx GetByTransactionTID(string tid)
        {
            return repo.GetByTransactionTID(tid);
        }



    }
}
