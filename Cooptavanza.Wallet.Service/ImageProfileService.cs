﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class ImageProfileService
    {
        readonly IImageProfileRepository repo;
        public ImageProfileService(IImageProfileRepository repo) {
            this.repo = repo;
        }

        public ImageProfileEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<ImageProfileEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(ImageProfileEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public ImageProfileEx GetByUserId(int id)
        {
           return repo.GetByUserId(id);
        }
    }
}
