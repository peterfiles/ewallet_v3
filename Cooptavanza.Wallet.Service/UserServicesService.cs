﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class UserServicesService
    {
        readonly IUserServicesRepository repo;
        readonly IProductsRepository iproduct;
        public UserServicesService(IUserServicesRepository repo, IProductsRepository iproduct) {
            this.repo = repo;
            this.iproduct = iproduct;
        }

        public UserServicesEx Get(int Id) {
            return repo.Get(Id);
        }

        public List<UserServicesEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }

        public List<UserServicesEx> GetByUserID(int Id)
        {
            return repo.GetByUserID(Id);
        }
        
        public object GetByUserIDToShowProduct(int Id, int roleID)
        {

            List<object> l = new List<object>();
            List<object> pName = new List<object>();
            List<UserServicesEx> p = repo.GetByUserIDAndRoleID(Id, roleID);

            StringBuilder sb = new StringBuilder();
            foreach (UserServicesEx a in p)
            {
                /*
                1   E - Wallet SMS Transfers
                2   Money Transfer
                3   E - Wallet Email Transfer
                4   Cash Out - ACH
                5   Cash In - Cash Location
                6   Cash In - EFT
                7   Top - up Airtime
                8   Cash In - Cash Member
                9   Cash In - Credit Card
                10  Cash In - Check 21
                11  China Remit
                12  Cash Out - Wire Transfer
                13  Cash Out - Property Card
                14  Bill Payment
                15  Cash Out - OCT
                */
                
                if (a.product_id == 1)
                {
                    l.Add(new
                    {
                        toshowEWallet = true,
                        productId = a.product_id,
                        id = a.ID
                });
                    pName.Add(new
                    {
                        EWallet = "E - Wallet SMS Transfers"
                    });
                }
                if (a.product_id == 2)
                {
                    l.Add(new
                    {
                        toshowMoneyTransfer = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        MoneyTransfer = "Money Transfer"
                    });
                }
                if (a.product_id == 3)
                {
                    l.Add(new
                    {
                        toshowEmailTransfer = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        EmailTransfer = "E - Wallet Email Transfer"
                    });
                }
                if (a.product_id == 4)
                {
                    l.Add(new
                    {
                        toshowbankDeposit = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        BankDeposit = "Bank Deposit"
                    });
                }
                if (a.product_id == 5)
                {
                    l.Add(new
                    {
                        toshowcashLocation = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashLocation = "Cash Location"
                    });
                }
                if (a.product_id == 6)
                {
                    l.Add(new
                    {
                        toshowEFT = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        EFT = "EFT"
                    });
                }
                if (a.product_id == 7)
                {
                    l.Add(new
                    {
                        toshowTopUpAirtime = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        TopUpAirTime = "Top-up Air Time"
                    });
                }
                if (a.product_id == 8)
                {
                    l.Add(new
                    {
                        toshowCashInCashMember = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashInCashMember = "Cash In - Cash Member"
                    });
                }
                if (a.product_id == 9)
                {
                    l.Add(new
                    {
                        toshowCashInCreditCard = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashInCreditCard = "Cash In - Credit Card"
                    });
                }
                if (a.product_id == 10)
                {
                    l.Add(new
                    {
                        toshowCashInCheck21 = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashInCheck21 = "Check 21"
                    });
                }
                if (a.product_id == 11)
                {
                    l.Add(new
                    {
                        toshowChinaRemit = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        ChinaRemit = "China Remit"
                    });
                }
                if (a.product_id == 12)
                {
                    l.Add(new
                    {
                        toshowCashOutWireTransfer = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashOutWireTransfer = "Cash Out - Wire Transfer"
                    });
                }
                if (a.product_id == 13)
                {
                    l.Add(new
                    {
                        toshowCashOutPropertyCard = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashOutPropertyCard = "Cash Out - Property Card"
                    });
                }
                if (a.product_id == 14)
                {
                    l.Add(new
                    {
                        toshowBillPayment = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        BillPayment = "Bill Payment"
                    });
                }
                if (a.product_id == 15)
                {
                    l.Add(new
                    {
                        toshowCashOutOCT = true,
                        productId = a.product_id,
                        id = a.ID
                    });
                    pName.Add(new
                    {
                        CashOutOCT = "Cash Out - OCT"
                    });
                }
            }
            object retData = new
            {
                toShowProducts = l,
                ProductName = pName
            };
            return retData;
   
        }



        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(UserServicesEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        
    }
}
