﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class CurrencyService
    {
        readonly ICurrencyRepository repo;
        public CurrencyService(ICurrencyRepository repo) {
            this.repo = repo;
        }

        public CurrencyEx Get(int Id) {
            return repo.Get(Id);
        }
        public List<CurrencyEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(CurrencyEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public List<CurrencyEx> GetDefaultValueList()
        {
            return repo.GetDefaultValueList();
        }

        public CurrencyEx GetByISO(string iso)
        {
            return repo.GetByISO(iso);
        }
    }
}
