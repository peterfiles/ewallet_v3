﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cooptavanza.Wallet.Repository.Repositories;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Repositories;

namespace Cooptavanza.Wallet.Service
{
   
    public class DummyLoadService
    {
        readonly IDummyLoadRepository repo;
        readonly ITransactionRepository repoTransaction;
        readonly CustomerService svcCustomer;
        public DummyLoadService(IDummyLoadRepository repo, ITransactionRepository repoTransaction, CustomerService svcCustomer) {
            this.repo = repo;
            this.repoTransaction = repoTransaction;
            this.svcCustomer = svcCustomer;
        }

        public DummyLoadEx Get(int Id) {
            return repo.Get(Id);
        }

     

        public List<DummyLoadEx> GetPaged(string where, string orderby, int PageNo, int PageSize)
        {
            return repo.GetPaged(where, orderby, PageNo, PageSize);
        }
        public int Count(string where)
        {
            return repo.Count(where);
        }

        public void Save(DummyLoadEx item)
        {
            repo.Save(item);
        }

        public void Remove(int Id)
        {
            var t = repo.Get(Id);
            repo.Remove(t);
        }

        public bool getByCredentials(string accountnumber, string userpin, int userid)
        {
            DummyLoadEx d = repo.getByCredentials(accountnumber, userpin);
            var n = svcCustomer.Get(userid);
            if(d != null)
            {
                TransactionEx t = new TransactionEx();
                t.credit = true;
                t.debit = false;
                t.userId = n.id;
                t.userTID = n.tid;
                t.transactionType = "LOAD_IN";
                t.description = "Recieved from Dummy loading";
                t.balance = d.credit;
                t.status = "CLEARED";
                t.dataCreated = DateTime.Now;
                t.transactionTID = ""+Guid.NewGuid();
                
                repoTransaction.Save(t);

                return true;
            }
            
            return false;
        }
    }
}
