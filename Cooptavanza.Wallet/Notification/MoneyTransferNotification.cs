﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using Cooptavanza.Wallet.Repository;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    internal class MoneyTransferNotification
    {

        string host = ConfigurationManager.AppSettings["HostId"].ToString();


        // Money Transfer Email Notification for Receiver
        public string rcvMTEmailNotifs(TransactionEx t, TransferMoneyEx tm, ETransferEx v)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            string rcvFullname = v.rcvLastName;
            string rcvEmail = v.rcvEmail;
            decimal amount = v.amount;
            string currencyTo = v.currencyTo;
            string rcvMobileno = v.rcvMobileno;
            string referenceNo = v.referenceNo;
            DateTime? date = t.dataCreated;

            string sndFullname = v.sndLastName;
            string sndEmail = v.sndEmail;
            string sndMobileNo = v.sndMobileNo;

            string question = v.question;
            string answer = v.answer;

            //Send SMS
            rcvSmsMoneyTransfer(rcvMobileno, t.id, rcvFullname, date, amount, currencyTo, sndFullname);

            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID + "&p=" + t.partnerId;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            //string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(rcvEmail, rcvFullname));

            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
           // LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
          //  unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + rcvFullname + ",</div><div></p>" + sndFullname + " has sent you a Money Transfer. </div>" +
                  "<div><p>Please see transaction details below for your reference.</p></div>" +
                  "<div><p>Amount:  " + currencyTo + " " + amount +
                  "</div><div>Transaction ID:  " + transactionId +
                  "</div><div>Transaction Date:  " + transactiondate +
                  "</div></p><div>" +
                  "</div><div>Sender Name:  " + sndFullname +
                  "</div> <div>" +
                  "</div><div>Sender Mobile No.:  " + sndMobileNo +
                  "</div> <div>" +
                  "</div><div>Sender Email id:  " + sndEmail +
                  "</div> <div>" +
                  "</div><div>Receiver Name:  " + rcvFullname +
                  "</div><div>" +
                  "</div><div> Receiver Email id.:  " + rcvEmail +
                  "</div><div>" +
                  "</div><div> Reference Number:  " + referenceNo +
                  "<div><p>Thanks & Regards </p></div><div><p><strong>COOP e - Wallet Management Team</strong></p></div>" +
                  "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
                  "</body> </html> ";


            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
           // AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);


            return "success";
        }

        // Money Transfer Email Notification for Sender
        public string sndMTEmailNotifs(TransactionEx t, TransferMoneyEx tm, ETransferEx v)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            string rcvFullname = v.rcvLastName;
            string rcvEmail = v.rcvEmail;
            decimal amount = v.amount;
            string currencyTo = v.currencyTo;
            string rcvMobileno = v.rcvMobileno;
            string referenceNo = v.referenceNo;
            DateTime? date = t.dataCreated;

            string sndFullname = v.sndLastName;
            string sndEmail = v.sndEmail;
            string sndMobileNo = v.sndMobileNo;

            string question = v.question;
            string answer = v.answer;

            //Send SMS
            sndSmsMoneyTransfer(sndMobileNo, t.id, rcvFullname, date, amount, currencyTo, sndFullname);

            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID + "&p=" + t.partnerId;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            //string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(sndEmail, sndFullname));

            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
           // LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
           // unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + sndFullname + ",</div><div></p> We had successfully processed the Money Transfer to " + rcvFullname + ". </div>" +                 
                  "<div><p>Please see transaction details below for your reference.</p></div>" +
                  "<div><p>Amount:  " + currencyTo + " " + amount +
                  "</div><div>Transaction ID:  " + transactionId +
                  "</div><div>Transaction Date:  " + transactiondate +
                  "</div></p><div>" +
                  "</div><div>Sender Name:  " + sndFullname +
                  "</div> <div>" +
                  "</div><div>Sender Mobile No.:  " + sndMobileNo +
                  "</div> <div>" +
                  "</div><div>Sender Email id:  " + sndEmail +
                  "</div> <div>" +
                  "</div><div>Receiver Name:  " + rcvFullname +
                  "</div><div>" +
                  "</div><div> Receiver Email id.:  " + rcvEmail +
                  "</div><div>" +
                  "</div><div> Reference Number:  " + referenceNo +
                  "<div><p>Your security is important to us. If you are not aware of this request, please contact us immediately at " + emailAddress + "</p></div> " +
                  "<div><p><strong>We appreciate the opportunity to serve you. Quality and service is top of mind.  </strong></p></div> " +
                  "<div><p>Thanks & Regards </p></div><div><p><strong>COOP e - Wallet Management Team</strong></p></div>" +
                  "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
                  "</body> </html> ";


            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
           // AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);


            return "success";
        }


        //Send SMS Money Transfer to Receiver
        public string rcvSmsMoneyTransfer(string mobilenumber, int transactionId, string rcvFullname, DateTime? Date, decimal amount, string currencyType, string sndFullname)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear " + rcvFullname + ",You have received an amount of " + amount + " " + currencyType + " on " + Date + ".-Thank you- Coop EWallet.");

                return "send successfull";
            }
            catch
            {
                return "illegal";
            }
        }

        //Send SMS Money Transfer to Sender
        public string sndSmsMoneyTransfer(string mobilenumber, int transactionId, string rcvFullname, DateTime? Date, decimal amount, string currencyType, string sndFullname)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear " + sndFullname + ",Your account has been debited an amount of " + amount + " " + currencyType + " on " + Date + ".-Thank you- Coop EWallet.");


                return "send successfull";
            }
            catch
            {
                return "illegal";
            }
        }


    }
}

