﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using Cooptavanza.Wallet.Repository;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    internal class ConfirmationSender
    {
        //string host = HttpContext.Current.Request.Url.Host;
        //dev = coop-ewallet.azurewebsites.net
        //prod = cooptavanza-wallet.azurewebsites.net
        //coopwallet.com <= with domain
        string host = ConfigurationManager.AppSettings["HostId"].ToString();

        //private string RName;

        //ach sender
        public string sendsmsEmailNotification(BankDepositACHTestEx d) {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            //string fullname = tm.receiverLastname != null || !tm.receiverLastname.Equals("") ? tm.receiverLastname : tm.benificiaryfullname;
            //string currencyname = tm.currencytoiso;
            //decimal amountsend = tm.amountto;

            sendsmsACH(d.fullname, d.mobilenumber);

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            //string link = "http://"+host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID; 
            //int transactionId = t.id;
            //DateTime? transactiondate = DateTime.Now;
            //string senderMNo = ach_mobilenumber;
            //string receiverEmail = ach_Email;
            ////string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(d.sendersEmail, d.fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
        //    string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
        //    LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
        //    unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            

              string str = "<!DOCTYPE html>" +
                  "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + d.fullname + ",</div><div></p> Your account has processed a BANK DEPOSIT TRANSACTION from " + d.bankName +
            ".<div><p></p></div>" +
            "</div><div>We appreciate your business.</div> <div>" +
            "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
            "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
            "</body> </html> ";



            //  string str = "<!DOCTYPE html>" +
            //      "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + ach_fullname + ",</div><div></p>The Email Funds transfer for " +
            //      receivername + " " + amountsend + " has been debited to your account. Please visit below link to deposit it to your own account. </p></div>" +
            //"<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>"+
            //"<div><p>Please Note Transaction reference for further correspondence</p></div>"+
            //"<div><p>Transaction ID:" + transactionId +
            //"</p></div><div>Transaction Date: " + transactiondate +
            //"</div> <div>" +
            //"Sender Mobile No.: " + senderMNo +
            //"</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
            //"Your Name: " + fullname +
            //"</div><div>" +
            //"Your email id.: " + receiverEmail +
            //"</div><div>" +
            //"ABA Routing Number: " + ach_ABaRouteNumber +
            //"</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
            //"</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
            //"<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'></div>" +
            //"</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
          //  AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }

        internal object test(TransactionEx t, TransferMoneyEx tM)
        {
            object retdata = new {transactiondata = t,transfermoneydata = tM } ;
            return retdata;
        }
        public string sendsmsACH(string fullname, string mobilenumber)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber("+" + mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear " + fullname + ", Your account has been debited From a bank desposit transaction. -Thank you- Coop EWallet.");

                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "illegal";
            }
        }
        







        public string sendWireTransferEmail(TransactionEx t, TransferMoneyEx tm)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
       //     string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
      //      LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);


            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
        //    unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>" +
          "<div><p>Please Note Transaction reference for further correspondence</p></div>" +
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Security Question :" + secretquestion +
          "</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
       //     AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }
        internal object Test(TransactionEx t, TransferMoneyEx tM)
        {
            object retdata = new { transactiondata = t, transfermoneydata = tM };
            return retdata;
        }

        //Money transfer received confirmation sending email
        public string sendMTReceivedEmail(TransactionEx t, TransferMoneyEx tm)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
       //     string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
         //   LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
       //     unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>" +
          "<div><p>Please Note Transaction reference for further correspondence</p></div>" +
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Security Question :" + secretquestion +
          "</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
          //  AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
       


            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }

        //Etransfer email sender when money is received by benificiary
        public string sendETransferEmail(TransactionEx t, TransferMoneyEx tm)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
          //  string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
     //       LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
    //        unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>" +
          "<div><p>Please Note Transaction reference for further correspondence</p></div>" +
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Security Question :" + secretquestion +
          "</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
     //       AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
          

            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }
        //Etransfer email confirmation when send money to benificiary
        public string sendEtransferConfirmationEmail(TransactionEx t, TransferMoneyEx tm)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
         //   string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
   //         LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
          //  unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>" +
          "<div><p>Please Note Transaction reference for further correspondence</p></div>" +
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Security Question :" + secretquestion +
          "</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
         //   AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);

            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
     
            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }

        //SMS sender to ewallet member notifying when ach money have been received.
        public string sendEmailConfirmationACH()
        {
            return "thank you";
        }

        //SMS sender to ewallet member notifying when wiretransfer money have been received.
        public string sendEmailConfirmationWireTransfer()
        {
            return "thank you";
        }

        //SMS sender to ewallet member notifying when e-transfer money have been received.
        public string sendEtransferConfirmationMoney(TransactionEx t, TransferMoneyEx tm, string mobilenumber) {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(tm.benificiarymobileno),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear Wallet Member, Your account has been credited " + tm.currencytoiso + " " + tm.amountto + ".: -Thank you- Coop EWallet.");
                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }
    }
}

