﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using Cooptavanza.Wallet.Repository;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    internal class ETransferNotification
    {

        string host = ConfigurationManager.AppSettings["HostId"].ToString();

        // ETransfer email notif for receiver
        public string EmailNotification(TransactionEx t, TransferMoneyEx tm, ETransferEx v) {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            string rcvFullname = v.rcvFirstName + " " + v.rcvMiddleName + " " + v.rcvLastName;
            string rcvEmail = v.rcvEmail;
            decimal amount = v.amount;
            string currencyTo = v.currencyTo;
            string rcvMobileno = v.rcvMobileno;
            string referenceNo = v.referenceNo;
            DateTime? date = t.dataCreated;

            string sndFullname = v.sndFirstName + " " + v.sndMiddleName + " " + v.sndLastName;
            string sndEmail = v.sndEmail;
            string sndMobileNo = v.sndMobileNo;

            string question = v.question;
            string answer = v.answer;


            //Send SMS
            //rcvSmsETransfer(rcvMobileno, t.id, rcvFullname, date, amount, currencyTo, sndFullname);

            string link = "http://" +host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID + "&p=" + t.partnerId; 
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            //string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(rcvEmail, rcvFullname));
 
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
         //   string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
          //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
          //  unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            

              string str = "<!DOCTYPE html>" +
                  "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + rcvFullname + ",</div><div></p>The Email Funds Tranfer has been successfully sent to your account with the amount of " + currencyTo + " " + amount + "" +
                  " from " + sndFullname + "   credited to your account on " + transactiondate + ". </p></div><div><p> We appreciate the opportunity to serve you. Quality and service is top of mind.  </p></div>" +            
                    "<div><p>Please see transaction details below for your reference.</p></div>" +
                    "<div>Transaction ID:  " + transactionId +
                    "</div><div>Transaction Date:  " + transactiondate +
                    "</div> <div>" +
                    "</div><div>Sender Mobile No.:  " + sndMobileNo +
                    "</div> <div>" +
                    "</div><div>Sender Email id:  " + sndEmail +
                    "</div> <div>" +
                    "</div><div>Receiver Name:  " + rcvFullname +
                    "</div><div>" +
                    "</div><div> Receiver Email id.:  " + rcvEmail +
                    "</div><div>" +
                    "</div><div> Reference Number:  " + referenceNo +
                    "<div><p> Please confirm the transaction in the link below. </p></div>" +
                    "<div><p> Link : <a href = \"" + link + "\">" + link + "</a></p></div>" +
                    "<p></div><div> Please ask the security answer from sender to deposit the funds on your account. </div><div><p>If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></p></div><div>We appreciate your business.</div> <div>" +
                    "</p></div><div><p></p><p> Thanks & Regards </p></div><div><strong>COOP e - Wallet Management Team</strong></div></p>" +
                    "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
                    "</body> </html> ";


            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
          //  AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }


        // ETransfer email notif for sender
        public string sndETransferEmailNotifs(TransactionEx t, TransferMoneyEx tm, ETransferEx v)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            string rcvFullname = v.rcvFirstName + " " + v.rcvMiddleName + " " + v.rcvLastName;
            string rcvEmail = v.rcvEmail;
            decimal amount = v.amount;
            string currencyTo = v.currencyTo;
            string rcvMobileno = v.rcvMobileno;
            string referenceNo = v.referenceNo;
            DateTime? date = t.dataCreated;

            string sndFullname = v.sndFirstName + " " + v.sndMiddleName + " " + v.sndLastName;
            string sndEmail = v.sndEmail;
            string sndMobileNo = v.sndMobileNo;

            string question = v.question;
            string answer = v.answer;

            //Send SMS ETransfer to sender
            //sndSmsETransfer(sndMobileNo, t.id, rcvFullname, date, amount, currencyTo, sndFullname);

            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID + "&p=" + t.partnerId;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            //string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(sndEmail, sndFullname));

            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
          //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
          //  unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + sndFullname + ",</div><div></p> We had successfully processed the Email Funds Transfer to " + rcvFullname +
                ".</div><div><p>Please see transaction details below for your reference.</p></div>" +
                  "<div><p>Amount:  " + currencyTo + " " + amount +
                  "</div><div>Transaction ID:  " + transactionId +
                  "</div><div>Transaction Date:  " + transactiondate +
                  "</div></p><div>" +
                  "</div><div>Sender Name:  " + sndFullname +
                  "</div> <div>" +
                  "</div><div>Sender Mobile No.:  " + sndMobileNo +
                  "</div> <div>" +
                  "</div><div>Sender Email id:  " + sndEmail +
                  "</div> <div>" +
                  "</div><div>Receiver Name:  " + rcvFullname +
                  "</div><div>" +
                  "</div><div> Receiver Email id.:  " + rcvEmail +
                  "</div><div>" +
                  "</div><div> Reference Number:  " + referenceNo +
                  "<div><p>Your security is important to us. If you are not aware of this request, please contact us immediately at " + emailAddress + "</p></div> " +
                  "<div><p><strong> We appreciate the opportunity to serve you. Quality and service is top of mind.  </strong></p></div> " +
                  "<div><p>Thanks & Regards </p></div><div><p><strong>COOP e - Wallet Management Team</strong></p></div>" +
                  "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
                  "</body> </html> ";


            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
          //  AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);


            return "success";
        }

        // SMS Notifs for ETransfer Sender
        public string sndrETransferSMSNotifs(TransactionEx t, TransferMoneyEx tm, ETransferEx v)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            string rcvFullname = v.rcvLastName;
            string rcvEmail = v.rcvEmail;
            decimal amount = v.amount;
            string currencyTo = v.currencyTo;
            string rcvMobileno = v.rcvMobileno;
            string referenceNo = v.referenceNo;
            DateTime? date = t.dataCreated;

            string sndFullname = v.sndLastName;
            string sndEmail = v.sndEmail;
            string sndMobileNo = v.sndMobileNo;

            string question = v.question;
            string answer = v.answer;

            //Send SMS ETransfer to sender
            sndSmsETransfer(sndMobileNo, t.id, rcvFullname, date, amount, currencyTo, sndFullname);

            return "success";
        }

        // SMS Notifs for ETransfer Receiver
        public string rcvrETransferSMSNotifs(TransactionEx t, TransferMoneyEx tm, ETransferEx v)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            string rcvFullname = v.rcvLastName;
            string rcvEmail = v.rcvEmail;
            decimal amount = v.amount;
            string currencyTo = v.currencyTo;
            string rcvMobileno = v.rcvMobileno;
            string referenceNo = v.referenceNo;
            DateTime? date = t.dataCreated;

            string sndFullname = v.sndLastName;
            string sndEmail = v.sndEmail;
            string sndMobileNo = v.sndMobileNo;

            string question = v.question;
            string answer = v.answer;

            //Send SMS ETransfer to receiver
            rcvSmsETransfer(rcvMobileno, t.id, rcvFullname, date, amount, currencyTo, sndFullname);

            return "success";
        }

              

        //Send SMS ETransfer to Receiver 
        public string rcvSmsETransfer(string mobilenumber, int transactionId, string rcvFullname, DateTime? Date, decimal amount, string currencyType, string sndFullname)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"), 
                    body: "Dear " + rcvFullname + ", Your account is credited with an amount of " + amount + " " + currencyType + " on " + Date + ".-Thank you- Coop EWallet.");
                return "send successfull";
             
            }
            catch
            {
                return "illegal";
            }
        }

        //Send SMS ETransfer to Sender
        public string sndSmsETransfer(string mobilenumber, int transactionId, string rcvFullname, DateTime? Date, decimal amount, string currencyType, string sndFullname)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear " + sndFullname + ", Your account has been debited an amount of " + amount + " " + currencyType + " on " + Date + ".-Thank you- Coop EWallet.");

                return "send successfull";
            }
            catch
            {
                return "illegal";
            }
        }




    }
}

