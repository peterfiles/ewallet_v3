﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using Cooptavanza.Wallet.Repository;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    internal class CBSWireTransferNotification
    {

        string host = ConfigurationManager.AppSettings["HostId"].ToString();

        public string sendWireTransferEmail(CBSWireTransferEx w)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            smtp.EnableSsl = true;
            smtp.Port = 587;

            //Get Data
            string sndAccntNo = w.sndAccntNo;
            string sndLastname = w.sndLastname;
            string sndFullname = w.sndFirstname + " " + w.sndLastname;
            string sndMobile = w.sndMobile;
            string sndEmail = w.sndEmail;

            string bankName = w.bankName;

            string rcvFullname = w.rcvName;
            string rcvAddress = w.rcvAddress;
            string rcvAcctNo = w.rcvAcctNo;

            decimal amountToTransfer = w.amountToTransfer;
            string currency = w.currency;

            DateTime d = DateTime.Now;
            string transactiondate = d.ToString("MMM dd, yyyy HH:mm");

            //Send SMS
            sndSmsCBSWireTransfer(sndMobile, sndLastname, amountToTransfer, currency, transactiondate);

            
            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(sndEmail, sndFullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
            // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
            //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
            //unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                    "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + sndLastname + ",</div><div></p>A wire transfer transaction has been made by your account with the currency of " +
                    currency + " and amount of " + amountToTransfer + ", to a beneficiary bank " + bankName + " and to a beneficiary customer " + rcvFullname + "." +
                    "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>Cash out - Wire Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
                    "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
                    "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
            // AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);

            return "success";
        }

        //Send SMS CBS Wire Transfer to Sender
        public string sndSmsCBSWireTransfer(string sndMobile, string sndLastname, decimal amount, string currency, string Date)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(sndMobile),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear " + sndLastname + ", You're account has been debited an amount of " + amount + " " + currency + " for your Wire transfer dated "+ Date + ".-Thank you- Coop CBS.");


                return "send successfull";
            }
            catch
            {
                return "illegal";
            }
        }


    }
}

