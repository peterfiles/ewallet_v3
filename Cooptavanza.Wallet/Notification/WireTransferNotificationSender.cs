﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using Cooptavanza.Wallet.Repository;
using Newtonsoft.Json.Linq;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    internal class WireTransferNotificationSender
    {
        //string host = HttpContext.Current.Request.Url.Host;
        //dev = coop-ewallet.azurewebsites.net
        //prod = cooptavanza-wallet.azurewebsites.net
        //coopwallet.com <= with domain
        string host = ConfigurationManager.AppSettings["HostId"].ToString();
        public string sendWireTransferEmail(object data)
        {
            string fullname = string.Empty;
            string mobilenumber = string.Empty;
            string email = string.Empty;
            string tamountToTransfer = string.Empty;
            string tcurrency = string.Empty;
            string bname = string.Empty;
            string cname = string.Empty;
            JObject jo = JObject.FromObject(data);
            foreach (JProperty jp in jo.Properties())
            {
                //if (jp.Name.Equals("aiRefMessage")) { }
                //if (jp.Name.Equals("bABARTN")) { }
                //if (jp.Name.Equals("bAddress")) { }
                if (jp.Name.Equals("bName")) { bname = jp.Value.ToString(); }
                //if (jp.Name.Equals("bSwiftBIC")) { }
                //if (jp.Name.Equals("cAccntIBAN")) { }
                //if (jp.Name.Equals("cAddress")) { }
                if (jp.Name.Equals("cName")) { cname = jp.Value.ToString(); }
                //if (jp.Name.Equals("checkDate")) { }
                //if (jp.Name.Equals("country")) { }
                //if (jp.Name.Equals("countryid")) { }
                //if (jp.Name.Equals("currency")) { }
                //if (jp.Name.Equals("currencyid")) { }
                //if (jp.Name.Equals("dfAccounts")) { }
                //if (jp.Name.Equals("otp")) { }
                //if (jp.Name.Equals("tdDescription")) { }
                if (jp.Name.Equals("tdamountToTransfer")) { tamountToTransfer = jp.Value.ToString(); }
                if (jp.Name.Equals("tdcurrency")) { tcurrency = jp.Value.ToString(); }
                if (jp.Name.Equals("mobilenumber")) { mobilenumber = jp.Value.ToString(); }
                if (jp.Name.Equals("email")) { email = jp.Value.ToString(); }
                if (jp.Name.Equals("fullname")) { fullname = jp.Value.ToString(); }

            }
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            
            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(email, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
          //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
            //unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>A wire transfer transaction has been made by your account with the currency of " +
                tcurrency + " and amount of " + tamountToTransfer + ", to a beneficiary bank " + bname + " and to a beneficiary customer " + cname + "."+
                "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>Cash out - Wire Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
           // AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);

            return "success";
        }
        internal object Test(TransactionEx t, TransferMoneyEx tM)
        {
            object retdata = new { transactiondata = t, transfermoneydata = tM };
            return retdata;
        }
        
        //SMS sender to ewallet member notifying when e-transfer money have been received.
        public string sendWireTransferSMSNotif(object data) {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            string fullname = string.Empty;
            string mobilenumber = string.Empty;
            string email = string.Empty;
            string tamountToTransfer = string.Empty;
            string tcurrency = string.Empty;
            string bname = string.Empty;
            string cname = string.Empty;
            JObject jo = JObject.FromObject(data);
            foreach (JProperty jp in jo.Properties())
            {
                //if (jp.Name.Equals("aiRefMessage")) { }
                //if (jp.Name.Equals("bABARTN")) { }
                //if (jp.Name.Equals("bAddress")) { }
                //if (jp.Name.Equals("bName")) { bname = jp.Value.ToString(); }
                //if (jp.Name.Equals("bSwiftBIC")) { }
                //if (jp.Name.Equals("cAccntIBAN")) { }
                //if (jp.Name.Equals("cAddress")) { }
                //if (jp.Name.Equals("cName")) { cname = jp.Value.ToString(); }
                //if (jp.Name.Equals("checkDate")) { }
                //if (jp.Name.Equals("country")) { }
                //if (jp.Name.Equals("countryid")) { }
                //if (jp.Name.Equals("currency")) { }
                //if (jp.Name.Equals("currencyid")) { }
                //if (jp.Name.Equals("dfAccounts")) { }
                //if (jp.Name.Equals("otp")) { }
                //if (jp.Name.Equals("tdDescription")) { }
                if (jp.Name.Equals("tdamountToTransfer")) { tamountToTransfer = jp.Value.ToString(); }
                if (jp.Name.Equals("tdcurrency")) { tcurrency = jp.Value.ToString(); }
                if (jp.Name.Equals("mobilenumber")) { mobilenumber = jp.Value.ToString(); }
                //if (jp.Name.Equals("email")) { email = jp.Value.ToString(); }
                //if (jp.Name.Equals("fullname")) { fullname = jp.Value.ToString(); }
            }
            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear Wallet Member, You're account has been debited an amount of " + tamountToTransfer + " " + tcurrency + ".-Thank you- Coop EWallet.");
                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }
    }
}

