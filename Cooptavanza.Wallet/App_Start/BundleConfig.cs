﻿using System.Web;
using System.Web.Optimization;

namespace Cooptavanza.Wallet
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = true;
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                       "~/Scripts/angular.js",
                        "~/Scripts/angular-ui-router.min.js",
                        "~/Scripts/angular-ui/*.js",
                        "~/Scripts/angular-growl.js", 
                        "~/Scripts/ui-bootstrap-tpls-2.5.0.js",
                        "~/Scripts/moment.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/ngCustomer").Include(
                       "~/Ng/CWC*", "~/Ng/Controller/CWC*", "~/Ng/Directive/*js", "~/Ng/Service/CWC*"));
            bundles.Add(new ScriptBundle("~/bundles/ngAdmin").Include(
                      "~/Ng/CWAdmin*", "~/Ng/Controller/CWAdmin*", "~/Ng/Directive/*js", "~/Ng/Service/CWAdmin*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/respond.js"));
          

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/fontawesome/css/*.css",
                      "~/Content/site.css",
                       "~/Content/customstyle.css"
                      ));

            bundles.Add(new StyleBundle("~/Content/growl").Include(
                    "~/Content/jquery.growl.css"
                ));
            bundles.Add(new ScriptBundle("~/bundles/growl").Include(
                    "~/Scripts/jquery.growl.js"));

            bundles.Add(new StyleBundle("~/bundles/template/css").Include(
                      "~/Content/templateFrontend/stylesheets/bootstrap.css",
                      "~/Content/templateFrontend/stylesheets/style.css",
                      "~/Content/templateFrontend/stylesheets/responsive.css",
                      "~/Content/templateFrontend/revolution/css/layers.css",
                      "~/Content/templateFrontend/revolution/css/settings.css",
                      "~/Content/template-frontend/stylesheets/colors/color1.css",
                      "~/Content/templateFrontend/stylesheets/animate.css"
                       
                      ));

            bundles.Add(new ScriptBundle("~/bundles/template/js").Include(
                      "~/Content/templateFrontend/javascript/jquery.min.js",
                      "~/Content/templateFrontend/javascript/bootstrap.min.js",
                      "~/Content/templateFrontend/javascript/jquery.easing.js",
                      "~/Content/templateFrontend/javascript/jquery.isotope.min.js",
                      "~/Content/templateFrontend/javascript/imagesloaded.min.js",
                      "~/Content/templateFrontend/javascript/jquery-waypoints.js",
                      "~/Content/templateFrontend/javascript/parallax.js",
                      "~/Content/templateFrontend/javascript/smoothscroll.js",
                      "~/Content/templateFrontend/javascript/jquery-countTo.js",
                      "~/Content/templateFrontend/javascript/owl.carousel.js",
                      "~/Content/templateFrontend/javascript/jquery.easypiechart.min.js",
                      "~/Content/templateFrontend/javascript/jquery.bxslider.js",
                      "~/Content/templateFrontend/javascript/main.js",
                      "~/Content/templateFrontend/javascript/jquery.tweet.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/template/revo").Include(
                      "~/Content/templateFrontend/revolution/js/jquery.themepunch.tools.min.js",
                      "~/Content/templateFrontend/revolution/js/jquery.themepunch.revolution.min.js",
                      "~/Content/templateFrontend/revolution/js/slider.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.actions.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.carousel.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.kenburn.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.layeranimation.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.migration.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.navigation.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.parallax.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.slideanims.min.js",
                      "~/Content/templateFrontend/revolution/js/extensions/revolution.extension.video.min.js"

                      ));

            //Customer Side CSS
            bundles.Add(new StyleBundle("~/bundles/templateFrontend/Css").Include(
                      "~/Content/templateFrontend/customerAdmin/bootstrap/css/bootstrap.min.css",
                      "~/Content/templateFrontend/customerAdmin/css/font-awesome.min.css",
                      "~/Content/templateFrontend/customerAdmin/css/pace.css",
                      "~/Content/templateFrontend/customerAdmin/css/app.min.css",
                      "~/Content/templateFrontend/customerAdmin/css/app-landing.min.css"
                    /* "~/Content/templateFrontend/customerAdmin/css/custom.css"*/
                      ));

            //Customer Side JS
            bundles.Add(new ScriptBundle("~/bundles/templateFrontend/Js").Include(
                      "~/Content/templateFrontend/customerAdmin/js/jquery-1.10.2.min.js",
                      "~/Content/templateFrontend/customerAdmin/bootstrap/js/bootstrap.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/waypoints.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.localscroll.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.scrollTo.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/modernizr.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/pace.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.popupoverlay.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.slimscroll.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.cookie.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/app/app.js"
                      ));

            //Customer-Admin Side CSS
            bundles.Add(new StyleBundle("~/bundles/templateFrontend/Admin/Css").Include(
                      "~/Content/templateFrontend/customerAdmin/bootstrap/css/bootstrap.min.css",
                      
                      "~/Content/templateFrontend/customerAdmin/css/pace.css",
                      "~/Content/templateFrontend/customerAdmin/css/app.min.css",
                      "~/Content/templateFrontend/customerAdmin/css/app-skin.css",
                      "~/Content/templateFrontend/customerAdmin/css/custom.css",
                      "~/Content/templateFrontend/customerAdmin/css/chosen/chosen.min.css",
                      "~/Content/templateFrontend/customerAdmin/css/datepicker.css",
                      "~/Content/templateFrontend/customerAdmin/css/bootstrap-timepicker.css",
                      "~/Content/templateFrontend/customerAdmin/css/slider.css",
                      "~/Content/templateFrontend/customerAdmin/css/jquery.tagsinput.css",
                      "~/Content/templateFrontend/customerAdmin/css/bootstrap-wysihtml5.css",
                      "~/Content/templateFrontend/customerAdmin/css/dropzone/dropzone.css"
                      ));

            //Customer-Admin Side JS
            bundles.Add(new ScriptBundle("~/bundles/templateFrontend/Admin/Js").Include(
                      "~/Content/templateFrontend/customerAdmin/js/jquery-1.10.2.min.js",
                      "~/Content/templateFrontend/customerAdmin/bootstrap/js/bootstrap.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/uncompressed/holder.js",
                      "~/Content/templateFrontend/customerAdmin/js/modernizr.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/pace.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.popupoverlay.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.slimscroll.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.cookie.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/app/app.js",
                      "~/Content/templateFrontend/customerAdmin/js/chosen.jquery.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.maskedinput.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/bootstrap-datepicker.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/bootstrap-timepicker.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/bootstrap-slider.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/jquery.tagsinput.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/wysihtml5-0.3.0.min.js",
                      "~/Content/templateFrontend/customerAdmin/js/uncompressed/bootstrap-wysihtml5.js",
                      "~/Content/templateFrontend/customerAdmin/js/dropzone.min.js"
                      ));
        }
    }
}
