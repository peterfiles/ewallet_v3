﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Cooptavanza.Wallet.Startup))]
namespace Cooptavanza.Wallet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
