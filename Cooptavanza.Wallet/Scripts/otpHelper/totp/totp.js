TOTP = function() {

    var dec2hex = function(s) {
        return (s < 15.5 ? "0" : "") + Math.round(s).toString(16);
    };
 
    var hex2dec = function(s) {
        return parseInt(s, 16);
    };
 
    var leftpad = function(s, l, p) {
        if(l + 1 >= s.length) {
            s = Array(l + 1 - s.length).join(p) + s;
        }
        return s;
    };
 
    var base32tohex = function(base32) {
        var base32chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        var bits = "";
        var hex = "";
        for(var i = 0; i < base32.length; i++) {
            var val = base32chars.indexOf(base32.charAt(i).toUpperCase());
            bits += leftpad(val.toString(2), 5, '0');
        }
        for(var i = 0; i + 4 <= bits.length; i+=4) {
            var chunk = bits.substr(i, 4);
            hex = hex + parseInt(chunk, 2).toString(16) ;
        }
        return hex;
    };

    this.getOTP = function(secret) {
        var run = true;
        while(run){
            try {console.log(secret);
            //  secret = "joy";new Date();//
                var key = base32tohex(secret.replace("-",""));
                var timenow = new Date();// moment("1503367608311");//
                var epoch = Math.round(1503367608311/1000.0);
                var time = leftpad(dec2hex(Math.floor(epoch / 30)), 16, "0");
                // updated for jsSHA v2.0.0 - http://caligatio.github.io/jsSHA/
                var shaObj = new jsSHA("SHA-1", "HEX");
                shaObj.setHMACKey(key, "HEX");
                shaObj.update(time);
                var hmac = shaObj.getHMAC("HEX");
                
                // var hmacObj = new jsSHA(time, "HEX");
                // var hmac = hmacObj.getHMAC(base32tohex(secret), "HEX", "SHA-1", "HEX");
                var offset = hex2dec(hmac.substring(hmac.length - 1));
                var otp = (hex2dec(hmac.substr(offset * 2, 8)) & hex2dec("7fffffff")) + "";
                otp = (otp).substr(otp.length - 6, 6);
                run = false;
                return otp;
        
            

            } catch (error) {
                run = true
               secret = secret + "a";
            //  throw error;
            }
        }
        return otp;
    };



}