﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using System.Configuration;
using System.IO;

namespace Cooptavanza.Wallet.Controllers
{
    internal class Generator 
    {
        //string host = HttpContext.Current.Request.Url.Host;
        //dev = coop-ewallet.azurewebsites.net
        //prod = cooptavanza-wallet.azurewebsites.net
        //coopwallet.com <= with domain
        string host = ConfigurationManager.AppSettings["HostId"].ToString();
        public string getAccount(int? countryid, int? stateid)
        {
           
                int lenthofpass = 12;
                string allowedChars = "";
                allowedChars += "1,2,3,4,5,6,7,8,9,0,";
                char[] sep = { ',' };
                string[] arr = allowedChars.Split(sep);
                string generatedAN = "";
                string temp = "";
                Random rand = new Random();
                for (int i = 0; i < lenthofpass; i++)
                {
                    temp = arr[rand.Next(0, arr.Length)];
                    generatedAN += temp;
                }

                return generatedAN;
           
        }

        public string getTransactionNo(int? countryid, int? stateid, int transactNo)
        {

            //int lenthofpass = 12;
            //string allowedChars = "";
            //allowedChars += "1,2,3,4,5,6,7,8,9,0,";
            //char[] sep = { ',' };
            //string[] arr = allowedChars.Split(sep);
            //string generatedAN = "";
            //string temp = "";
            //Random rand = new Random();
            //for (int i = 0; i < lenthofpass; i++)
            //{
            //    temp = arr[rand.Next(0, arr.Length)];
            //    generatedAN += temp;
            //}

            //return generatedAN;
            StringBuilder sb = new StringBuilder();

            //string f1 = appendRandomCharacter(Convert.ToString(DateTime.Now.Day));
            //sb.Append(f1);
            //string f2 = appendRandomCharacter(Convert.ToString(DateTime.Now.Month));
            //sb.Append(f2);
            //string f3 = appendRandomCharacter(Convert.ToString(countryid));
            //sb.Append(f3);
            //string f4 = appendRandomCharacter(Convert.ToString(stateid));
            
            int numToAppend =  4 - Convert.ToString(countryid).Length;
            string d1 = appendRandomCharacter(Convert.ToString(countryid), numToAppend);
            sb.Append(d1);
            string d2 = Convert.ToString(stateid);
            sb.Append(d2);
            int tNum = transactNo + 1;
            numToAppend = 4 - Convert.ToString(tNum).Length;
            string d3 = Convert.ToString(tNum).Length != 0 ? appendRandomCharacter(Convert.ToString(tNum), numToAppend) : Convert.ToString(tNum);
            sb.Append(d3);
            return sb.ToString();

        }

        public string appendRandomCharacter(string str, int toAppend)
        {
            int strLen = str.Length;
                str += appendMissChar(toAppend);
            return str;
        }
        public string appendMissChar(int lenthofpass)
        {
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string generatedAN = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < lenthofpass; i++)
            {
                temp = arr[rand.Next(1, 27)];
                generatedAN += retLetter(Convert.ToInt32(temp));
            }
            return generatedAN;
        }

        public char retLetter(int letID)
        {
            char let = 'A';
            switch (letID)
            {
                case 1: return let = 'A'; 
                case 2: return let = 'B'; 
                case 3: return let = 'C';
                case 4: return let = 'D';
                case 5: return let = 'E';
                case 6: return let = 'F'; 
                case 7: return let = 'G';
                case 8: return let = 'H';
                case 9: return let = 'I'; 
                case 11: return let = 'J'; 
                case 12: return let = 'K'; 
                case 13: return let = 'L';
                case 14: return let = 'M';
                case 15: return let = 'N';
                case 16: return let = 'O'; 
                case 17: return let = 'P'; 
                case 18: return let = 'Q'; 
                case 19: return let = 'R'; 
                case 20: return let = 'S'; 
                case 21: return let = 'T'; 
                case 22: return let = 'U'; 
                case 23: return let = 'V';
                case 24: return let = 'W'; 
                case 25: return let = 'X';
                case 26: return let = 'Y'; 
                case 27: return let = 'Z'; 
            }
            return let;
        }

    public string getNewAccount(int? countryid, int? stateid, string currency)
        {

            int lenthofpass = 12;
            string allowedChars = "";
            allowedChars += "1,2,3,4,5,6,7,8,9,0,";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string generatedAN = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < lenthofpass; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                generatedAN += temp;
            }

            return generatedAN;

        }
        public string OTP()
        {

            int lenthofpass = 6;
            string allowedChars = "";
          //  allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
          //  allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars = "1,2,3,4,5,6,7,8,9,0,";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < lenthofpass; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            return passwordString;

        }

        public string generatePartnerCode(string key, int partnerCount)
        {
            char[] c = key.ToCharArray();
            StringBuilder sb = new StringBuilder();
            string append1 = string.Empty;
            for(int x = 0; x < c.Length; x++)
            {
                append1 += Convert.ToByte(c[x]).ToString();
            }

            Random rand = new Random();
            char l1 = retLetter(rand.Next(1, 27));
            sb.Append(c.Length + "" + partnerCount + "" + l1 + "" + append1);

            int strlen = 16 - sb.ToString().Length;
            string appendZero = string.Empty;
            if (strlen > 0)
            {
                for(int y = 0; y < strlen; y++)
                {
                    appendZero += "0";
                }
                sb.Append(appendZero);
            }
            else
            {
                char[] trim = strlen.ToString().Replace("-", "").ToCharArray();
                int t = 16 - Convert.ToInt32(trim[0].ToString());
                StringBuilder sb2 = new StringBuilder();
                char[] s = sb.ToString().ToCharArray();
                for (int a = 0; a < t; a++)
                {
                    sb2.Append(s[a]);
                }
                strlen = 16 - sb2.ToString().Length;
                if (strlen > 0)
                {
                    for (int b = 0; b < strlen; b++)
                    {
                        appendZero += "0";
                    }
                }
                sb = new StringBuilder();
                sb.Append(sb2.ToString() + "" + appendZero);
            }
            
            int z = 0;
            string appendEnd = string.Empty;
            do
            {
                l1 = retLetter(rand.Next(1, 27));
                if (!appendEnd.Contains(l1.ToString()))
                {
                    appendEnd += l1;
                    z = z + 1;
                }
            } while (z != 3);

            sb.Append(appendEnd);
            return sb.ToString();
        }

        public string generateBillerAccountNumberByCountryStateBranchAndISO(int cID, int sID, string branchName, string iso, int bType, int bCount)
        {
            char[] c = branchName.ToCharArray();
            StringBuilder sb = new StringBuilder();
            string appendByte = string.Empty; Random rand = new Random();
            string l = string.Empty;
            l = retLetter(rand.Next(1, 27)).ToString();
            for (int x = 0; x < c.Length; x++)
            {
                appendByte += Convert.ToByte(c[x]).ToString();
                // rand = new Random();
                //if (x == 0)
                //{
                //    b = Convert.ToByte(c[x]);
                //    //sb.Append(c.Length + "" + retLetter(rand.Next(1,14)) + "" + b); //no. of characters
                //}
                //else {
                //    b = Convert.ToByte(c[x]);
                //    string g = string.Empty;
                //    do
                //    {
                //      g = retLetter(rand.Next(1, 27)).ToString();
                //    } while (sb.ToString().Contains(g));
                //    sb.Append(g + "" + b);;
                //}
                //if ((x + 1) == c.Length)
                //{
                //    b = Convert.ToByte(c[x]);
                //    string g = string.Empty;
                //    do
                //    {
                //        g = retLetter(rand.Next(1, 27)).ToString();
                //    } while (sb.ToString().Contains(g));
                //    sb.Append(g);
                //}
            }
            sb.Append(c.Length + "" + bCount + "" + l + "" + appendByte);
            string g = string.Empty;
            do
            {
                g = retLetter(rand.Next(1, 27)).ToString();
            } while (sb.ToString().Contains(g));

            sb.Append(g + "" + cID + "" + sID);
            string strlen = sb.ToString();
            int appLeadZero = 23 - strlen.Length;
            string appZero = string.Empty;
            if (appLeadZero > 0) //Append Leading Zero's
            {
                for(int a = 0; a < appLeadZero; a++)
                {
                    appZero += "0";
                }
                
                sb.Append(appZero);
            }
            else
            {
                char[] trim = appLeadZero.ToString().Replace("-", "").ToCharArray();
                int t = 23 - Convert.ToInt32(trim[0].ToString());
                StringBuilder sb2 = new StringBuilder();
                char[] s = sb.ToString().ToCharArray();
                for (int a = 0; a < t; a++)
                {
                    sb2.Append(s[a]);
                }
                int len = 23 - sb2.ToString().Length;
                if (len > 0)
                {
                    for (int b = 0; b < len; b++)
                    {
                        appZero += "0";
                    }
                }
                sb = new StringBuilder();
                sb.Append(sb2.ToString() + "" + appZero);
            }

            do
            {
                g = retLetter(rand.Next(1, 27)).ToString();
            } while (sb.ToString().Contains(g));
            sb.Append(g + "" + iso);
            return sb.ToString();
        }

        public string generateCommissionsAccountNumber(int cID, int sID, string currency, string countryISO, int userID)
        {
            char[] c = countryISO.ToCharArray();
            StringBuilder sb = new StringBuilder();
            string appendByte = string.Empty; Random rand = new Random();
            string l = string.Empty;
            l = retLetter(rand.Next(1, 27)).ToString();
            for (int x = 0; x < c.Length; x++)
            {
                appendByte += Convert.ToByte(c[x]).ToString();
            }
            sb.Append(c.Length + "" + userID + "" + l + "" + appendByte);
            string g = string.Empty;
            do
            {
                g = retLetter(rand.Next(1, 27)).ToString();
            } while (sb.ToString().Contains(g));

            sb.Append(g + "" + cID + "" + sID);
            string strlen = sb.ToString();
            int appLeadZero = 23 - strlen.Length;
            string appZero = string.Empty;
            if (appLeadZero > 0) //Append Leading Zero's
            {
                for (int a = 0; a < appLeadZero; a++)
                {
                    appZero += "0";
                }

                sb.Append(appZero);
            }
            else
            {
                char[] trim = appLeadZero.ToString().Replace("-", "").ToCharArray();
                int t = 23 - Convert.ToInt32(trim[0].ToString());
                StringBuilder sb2 = new StringBuilder();
                char[] s = sb.ToString().ToCharArray();
                for (int a = 0; a < t; a++)
                {
                    sb2.Append(s[a]);
                }
                int len = 23 - sb2.ToString().Length;
                if (len > 0)
                {
                    for (int b = 0; b < len; b++)
                    {
                        appZero += "0";
                    }
                }
                sb = new StringBuilder();
                sb.Append(sb2.ToString() + "" + appZero);
            }

            do
            {
                g = retLetter(rand.Next(1, 27)).ToString();
            } while (sb.ToString().Contains(g));
            sb.Append(g + "" + countryISO + "" + currency);
            return sb.ToString();
        }

        public string generateTransactionNo(string transactiontype, int lastTransactionNo, string currency)
        {
            StringBuilder sb = new StringBuilder();
            //Append Random leading letters
            string g = string.Empty;
            Random rand = new Random();
            int z = 17 - Convert.ToString(lastTransactionNo).Length;
            if (z > 0)
            {
                sb.AppendFormat("{0}{1}", lastTransactionNo, transactiontype);
                //for (int x = 0; x < z; x++)
                //{
                //    sb.Append("0");
                //}
                int x = 0;
                do
                {
                    g = retLetter(rand.Next(1, 27)).ToString();
                    if (!sb.ToString().Contains(g))
                    {
                        sb.Append(g);
                        x++;
                    }
                } while (x < z);
            }
            else
            {
                sb.AppendFormat("{0}{1}", lastTransactionNo, transactiontype);
            }

            if (sb.Length > 17)
            {
                char[] p = sb.ToString().ToCharArray();
                sb = new StringBuilder();
                for (int q = 0; q < 17; q++)
                {
                    sb.Append(p[q]);
                }
            }
            sb.Append(currency);
            return sb.ToString();
        }

        public string generateTaxAccountNo(string ttype)
        {
            return ttype;
        }

        #region Notification

        public string sendSMSOFACNotif(string mobile_number, string msg)
        {

            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobile_number),
                    from: new PhoneNumber("+17027896606"),
                    body: msg);
                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }

        }

        public string sendemail(TransactionEx t, TransferMoneyEx tm) {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;
           
            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            string link = "http://"+host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID; 
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail,fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
         //   string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
          //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);


            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
         //   unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>"+
          "<div><p>Please Note Transaction reference for further correspondence</p></div>"+
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Security Question :" + secretquestion +
          "</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
           // AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
            //  message.Body = "<!DOCTYPE html>" +
            //      "<html><body><div><img src='cid:" + sImage + "'></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
            //      currencyname + " " + amountsend + " has been credited to your account.Please visit below link to deposit you own account. </p></div>" +
            //"<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div><div><p>Please Note Transaction reference for further correspondence</p></div><div><p>" +
            //"Transaction ID:" + transactionId +
            //"</p></div><div>Transaction Date: " + transactiondate +
            //"</div> <div>" +
            //"Sender Mobile No.: " + senderMNo +
            //"</div><div>Sender email id: support@cooptavanza.com </div> <div>"+
            //"Your Name: " + fullname +
            //"</div><div>"+
            //"Your email id.:" + receiverEmail+
            //"</div><div> Security Question :" + secretquestion +
            //"</div><div> Please ask the security answer from sender to deposit the funds on your account.</div><div>We appreciate your business.</div> <div>"+
            //"</div><div><p></p><p> Thanks & Regards </p></div><div><strong>COOP e - Wellet Management Team</strong></div></p>"+
            //"</body> </html> ";



            message.IsBodyHtml = true;
            smtp.Send(message);
            
            return "success";
        }

        public string sendConfirmationToSenderMoney(TransactionEx t, TransferMoneyEx tm, CustomerEx customer)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587
            string fullname = customer.firstname.ToUpper() + " " + customer.lastname.ToUpper();
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;http://cooptavanza-wallet.azurewebsites.net/
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = customer.email;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
          //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
          //  unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div>"+
                "<div><p>Dear " + fullname + 
                ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to " + tm.benificiaryfullname + ".  </p></div>" +
          
          "<div><p>Please Note Transaction reference for further correspondence</p></div>" +
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Receiver Name: " + tm.benificiaryfullname +
          "</div><div>" +
          "Receiver email address.: " + tm.benificiaryemail +
          "</div><div> Security Question :" + secretquestion +
          "</div>"+
          "<div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
         //   AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
            //  message.Body = "<!DOCTYPE html>" +
            //      "<html><body><div><img src='cid:" + sImage + "'></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
            //      currencyname + " " + amountsend + " has been credited to your account.Please visit below link to deposit you own account. </p></div>" +
            //"<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div><div><p>Please Note Transaction reference for further correspondence</p></div><div><p>" +
            //"Transaction ID:" + transactionId +
            //"</p></div><div>Transaction Date: " + transactiondate +
            //"</div> <div>" +
            //"Sender Mobile No.: " + senderMNo +
            //"</div><div>Sender email id: support@cooptavanza.com </div> <div>"+
            //"Your Name: " + fullname +
            //"</div><div>"+
            //"Your email id.:" + receiverEmail+
            //"</div><div> Security Question :" + secretquestion +
            //"</div><div> Please ask the security answer from sender to deposit the funds on your account.</div><div>We appreciate your business.</div> <div>"+
            //"</div><div><p></p><p> Thanks & Regards </p></div><div><strong>COOP e - Wellet Management Team</strong></div></p>"+
            //"</body> </html> ";



            message.IsBodyHtml = true;
            smtp.Send(message);
            sendSMSNotify(t, tm, customer.mobilenumber);


            return "success";
        }


        public string sendemailWR(TransactionEx t, TransferMoneyEx tm)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
           // LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
           // unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host +"'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>"+
          "<div><p>Please Note Transaction reference for further correspondence</p></div>"+
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Please ask the PASSWORD from sender to cash out your funds. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
          //  AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
            //  message.Body = "<!DOCTYPE html>" +
            //      "<html><body><div><img src='cid:" + sImage + "'></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
            //      currencyname + " " + amountsend + " has been credited to your account.Please visit below link to deposit you own account. </p></div>" +
            //"<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div><div><p>Please Note Transaction reference for further correspondence</p></div><div><p>" +
            //"Transaction ID:" + transactionId +
            //"</p></div><div>Transaction Date: " + transactiondate +
            //"</div> <div>" +
            //"Sender Mobile No.: " + senderMNo +
            //"</div><div>Sender email id: support@cooptavanza.com </div> <div>"+
            //"Your Name: " + fullname +
            //"</div><div>"+
            //"Your email id.:" + receiverEmail+
            //"</div><div> Security Question :" + secretquestion +
            //"</div><div> Please ask the security answer from sender to deposit the funds on your account.</div><div>We appreciate your business.</div> <div>"+
            //"</div><div><p></p><p> Thanks & Regards </p></div><div><strong>COOP e - Wellet Management Team</strong></div></p>"+
            //"</body> </html> ";



            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }


        public string sendSenderEmail(TransactionEx t, TransferMoneyEx tm)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;
            string fullname = tm.benificiaryfullname;
            string currencyname = tm.currencytoiso;
            decimal amountsend = tm.amountto;

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;
            string link = "http://" + host + "/confirmation/#!/?id=" + t.id + "&tk=" + tm.transactionTID;
            int transactionId = t.id;
            DateTime? transactiondate = t.dataCreated;
            string senderMNo = tm.sendermobileno;
            string receiverEmail = tm.benificiaryemail;
            string secretquestion = tm.question;

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail, fullname));

            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
            //LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
          //  unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
                currencyname + " " + amountsend + " has been sent to you. Please visit below link to deposit it to your own account. </p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>"+
          "<div><p>Please Note Transaction reference for further correspondence</p></div>"+
          "<div><p>Transaction ID:" + transactionId +
          "</p></div><div>Transaction Date: " + transactiondate +
          "</div> <div>" +
          "Sender Mobile No.: " + senderMNo +
          "</div><div>Sender email id: support@cooptavanza.com </div> <div>" +
          "Your Name: " + fullname +
          "</div><div>" +
          "Your email id.: " + receiverEmail +
          "</div><div> Security Question :" + secretquestion +
          "</div><div> Please ask the security answer from sender to deposit the funds on your account. If you do not have an account please sign up for your free account at <a href='http://" + host + "/Registration#!/'> Sign up now. </a></div><div>We appreciate your business.</div> <div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
          //  AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
            //  message.Body = "<!DOCTYPE html>" +
            //      "<html><body><div><img src='cid:" + sImage + "'></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
            //      currencyname + " " + amountsend + " has been credited to your account.Please visit below link to deposit you own account. </p></div>" +
            //"<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div><div><p>Please Note Transaction reference for further correspondence</p></div><div><p>" +
            //"Transaction ID:" + transactionId +
            //"</p></div><div>Transaction Date: " + transactiondate +
            //"</div> <div>" +
            //"Sender Mobile No.: " + senderMNo +
            //"</div><div>Sender email id: support@cooptavanza.com </div> <div>"+
            //"Your Name: " + fullname +
            //"</div><div>"+
            //"Your email id.:" + receiverEmail+
            //"</div><div> Security Question :" + secretquestion +
            //"</div><div> Please ask the security answer from sender to deposit the funds on your account.</div><div>We appreciate your business.</div> <div>"+
            //"</div><div><p></p><p> Thanks & Regards </p></div><div><strong>COOP e - Wellet Management Team</strong></div></p>"+
            //"</body> </html> ";



            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }


        public string sendResetEmail(EmailEx EE)
        {
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;
         

            //string link = "http://coopwallet.azurewebsites.net/confirmation/?id="+t.id+"&tk="+t.transactionTID;
            //string link = "http://localhost:3983/#!/confirmation/?id=" + t.id + "&tk=" + t.transactionTID;
            string link = "http://" + host+"/resetpassword/#!/?id=" + EE.id + "&tk=" + EE.tID;
        
          
            string receiverEmail = EE.email;
          
            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(receiverEmail));

            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
          //  string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
           // LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
           // unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";

            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://"+host+"'><img src=cid:Logo></a></div>"+
                "<div><p>Please visit the link to Reset your password.</p></div>" +
          "<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div>" +
          
          "<div><p></p><p> Thanks & Regards </p></div><div><strong>E-Transfer</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
           // AV.LinkedResources.Add(unionpayImg);
            AV.LinkedResources.Add(pciImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);
            //  message.Body = "<!DOCTYPE html>" +
            //      "<html><body><div><img src='cid:" + sImage + "'></div><div><p>Dear " + fullname + ",</div><div></p>The Email Funds transfer for " +
            //      currencyname + " " + amountsend + " has been credited to your account.Please visit below link to deposit you own account. </p></div>" +
            //"<div><p> Link :</p><a href = \"" + link + "\">" + link + "</a></p></div><div><p>Please Note Transaction reference for further correspondence</p></div><div><p>" +
            //"Transaction ID:" + transactionId +
            //"</p></div><div>Transaction Date: " + transactiondate +
            //"</div> <div>" +
            //"Sender Mobile No.: " + senderMNo +
            //"</div><div>Sender email id: support@cooptavanza.com </div> <div>"+
            //"Your Name: " + fullname +
            //"</div><div>"+
            //"Your email id.:" + receiverEmail+
            //"</div><div> Security Question :" + secretquestion +
            //"</div><div> Please ask the security answer from sender to deposit the funds on your account.</div><div>We appreciate your business.</div> <div>"+
            //"</div><div><p></p><p> Thanks & Regards </p></div><div><strong>COOP e - Wellet Management Team</strong></div></p>"+
            //"</body> </html> ";



            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }

        //private AlternateView Mail_Body()
        //{
        //    string path = Server.MapPath(@"Images/photo.jpg");
        //    LinkedResource Img = new LinkedResource(path, MediaTypeNames.Image.Jpeg);
        //    Img.ContentId = "MyImage";
        //    string str = @"  
        //    <table>  
        //        <tr>  
        //            <td> '" + txtmessagebody.Text + @"'  
        //            </td>  
        //        </tr>  
        //        <tr>  
        //            <td>  
        //              <img src=cid:MyImage  id='img' alt='' width='100px' height='100px'/>   
        //            </td>  
        //        </tr></table>  
        //    ";
        //    AlternateView AV =
        //    AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
        //    AV.LinkedResources.Add(Img);
        //    return AV;
        //}

        

        private string sendSMSNotify(TransactionEx t, TransferMoneyEx tm, string mobilenumber)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);
            string productMessage = "";
            if(tm.transactionType == "11002" || tm.transactionType == "11001")
            {
                productMessage = "E-Transfer";
            }
            else if(tm.transactionType == "10002"){
                productMessage = "Money Transfer";
            }
            else if(tm.transactionType == "10003" || tm.transactionType == "10013" || tm.transactionType == "10023" || tm.transactionType == "10033" || tm.transactionType == "10043")
            {
                productMessage = "Money Swap";
            }
            

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear Wallet Member, You have sent " + tm.currencytoiso + " " + tm.amountto + " to " + tm.benificiarymobileno + "." + productMessage + ".-Thank you- Coop EWallet.");
                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }


        public string sendSMSCreditCard(string mobilenumber, string currency, decimal amount)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear Wallet Member, Your account has been credited " + currency + " " + amount + " from credit card. -Thank you- Coop EWallet.");
                
                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }

        public string sendSMSMoney(TransactionEx t, TransferMoneyEx tm, string mobilenumber)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(tm.benificiarymobileno),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear Wallet Member, Your account has been credited " + tm.currencytoiso + " " + tm.amountto + " " + ".: -Thank you- Coop EWallet.");

                sendSMSNotify(t, tm, mobilenumber);

                //  Console.WriteLine(message.Sid);
                return "success";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }

        public string sendSMSMoneyFromWalkIn(string cType, 
                                             string sendersmobilenumber,
                                             string receiversmobilenumber,
                                             string password,
                                             decimal amounttoReceive,
                                             string sendersFullName,
                                             string receiversFullName,
                                             string trackingNo)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);
            if (cType.Equals("sender"))
            {
                try
                {
                    var message = MessageResource.Create(
                        to: new PhoneNumber(sendersmobilenumber),
                        from: new PhoneNumber("+17027896606"),
                        body: "Dear " + sendersFullName + ", This is your Money Transfer tracking no:" + trackingNo + ".-Thank you- From Coop EWallet.");
                    //  Console.WriteLine(message.Sid);
                    return "send successfull";
                    //   Console.ReadKey();
                }
                catch
                {
                    return "Illegal";
                }
            }
            else
            {
                try
                {
                    var message = MessageResource.Create(
                         to: new PhoneNumber(receiversmobilenumber),
                         from: new PhoneNumber("+17027896606"),
                         body: "Dear " + receiversFullName + ", Claim the transfer using this tracking no:" + trackingNo + " with a valid ID. -Thank You- From Coop EWallet.");
                    //  Console.WriteLine(message.Sid);
                    return "send successfull";
                    //   Console.ReadKey();
                }
                catch
                {
                    return "Illegal";
                }
            }
        }
        #endregion Notification;
        #region Notif
        public string sendEmailNotif(string fullname, string email, string currency, string amount, string moduleNameToShow, string creditOrDebit)
        {

            string host = ConfigurationManager.AppSettings["HostId"].ToString();
            SmtpClient smtp = new SmtpClient("smtp.gmail.com");//"SMTPout.secureserver.net");
            //smtp.Host = "SMTPout.secureserver.net";
            smtp.EnableSsl = true;
            smtp.Port = 587;//3535;//587

            //string emailAddress = "noreply@coopwallet.com";
            //string appSpecificPassword = "Speedy5477";
            string emailAddress = "coopwallet.dev@gmail.com";//"coopwallet.dev@gmail.com";
            string appSpecificPassword = "Qwer!234";//"Qwer!234";
            smtp.Credentials = new NetworkCredential(emailAddress, appSpecificPassword);

            MailMessage message = new MailMessage();
            message.Sender = new MailAddress(emailAddress, "Co-op EWallet Support");
            message.From = new MailAddress(emailAddress, "Co-op EWallet support");
            message.To.Add(new MailAddress(email, fullname));
            //MailAddress copy = new MailAddress("N@test.com");
            //message.CC.Add(copy);
            string sImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/images/cw-logo.jpg");
            string redImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/coopred_logo.jpg");
            string cooptavanzaImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/COOPTAVANZAMPAGO02.jpg");
           // string unionpayImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/unionpay.jpeg");
            string pciImage = System.Web.HttpContext.Current.Server.MapPath("~/Content/templateFrontend/customerAdmin/img/pci_logo.png");
            LinkedResource Img = new LinkedResource(sImage, MediaTypeNames.Image.Jpeg);
            LinkedResource redImg = new LinkedResource(redImage, MediaTypeNames.Image.Jpeg);
            LinkedResource coopImg = new LinkedResource(cooptavanzaImage, MediaTypeNames.Image.Jpeg);
          //  LinkedResource unionpayImg = new LinkedResource(unionpayImage, MediaTypeNames.Image.Jpeg);
            LinkedResource pciImg = new LinkedResource(pciImage, MediaTypeNames.Image.Jpeg);

            Img.ContentId = "Logo";
            redImg.ContentId = "red";
            coopImg.ContentId = "coop";
         //   unionpayImg.ContentId = "unionpay";
            pciImg.ContentId = "pci";


            string str = "<!DOCTYPE html>" +
                "<html><body><div><a href='http://" + host + "'><img src=cid:Logo></a></div><div><p>Dear " + fullname + ",</div><div></p>Your account has been "+ creditOrDebit +
                currency + " " + amount + " from "+ moduleNameToShow +".  -Coop EWallet Management. </p></div>" +
          "</div><div><p></p><p> Thanks & Regards </p></div><div><strong>" + moduleNameToShow + "</strong> <img src=cid:Logo style='height:50px;'></div><div><strong>Transaction submitted by COOP e - Wallet Management Team</strong></div></p>" +
          "<div style='padding-top:20px;'><img src=cid:red style='height:50px;'><img src=cid:coop style='height:50px;'><img src=cid:pci style='height:50px;'></div>" +
          "</body> </html> ";

            AlternateView AV = AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            AV.LinkedResources.Add(redImg);
            AV.LinkedResources.Add(coopImg);
         //   AV.LinkedResources.Add(unionpayImg);


            message.Subject = "noreply";
            message.AlternateViews.Add(AV);

            message.IsBodyHtml = true;
            smtp.Send(message);



            return "success";
        }

        public string sendSMSNotif(string mobilenumber, string currency, string amount, string moduleNameToShow, string creditOrDebit)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                var message = MessageResource.Create(
                    to: new PhoneNumber(mobilenumber),
                    from: new PhoneNumber("+17027896606"),
                    body: "Dear Wallet Member, Your account has been "+ creditOrDebit +" "+ currency + " " + amount + " via "+ moduleNameToShow +". -Thank you- -From Coop EWallet.");



                //  Console.WriteLine(message.Sid);
                return "send successfull";
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }

        public string sendSMSNotifEtransfer(string ctype,string fullname, string tno, string mobilenumber)
        {
            const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
            const string authToken = "9185158d2ea6518e1178805630b68f0b";
            TwilioClient.Init(accountSid, authToken);

            try
            {
                if (ctype.Equals("sender"))
                {
                    var message = MessageResource.Create(
                        to: new PhoneNumber(mobilenumber),
                        from: new PhoneNumber("+17027896606"),
                        body: "Mr./Mrs. " + fullname + "," + "Your SMS-Transfer Transaction No: " + tno + ". -Thank you- -From Coop EWallet.");

                    //  Console.WriteLine(message.Sid);
                    return "success";
                }
                else
                {
                    var message = MessageResource.Create(
                        to: new PhoneNumber(mobilenumber),
                        from: new PhoneNumber("+17027896606"),
                        body: "Mr./Mrs. " + fullname + "," + "You have an SMS-Transfer with transaction No: " + tno + ". -Thank you- -From Coop EWallet.");

                    //  Console.WriteLine(message.Sid);
                    return "success";
                }
                //   Console.ReadKey();
            }
            catch
            {
                return "Illegal";
            }
        }
        #endregion Notif

        public void LogException(Exception exc, string source)
        {
            // Include logic for logging exceptions
            // Get the absolute path to the log file
            string logFile = "~/Content/ErrorLog.txt";
            logFile = HttpContext.Current.Server.MapPath(logFile);

            // Open the log file for append and write the log
            StreamWriter sw = new StreamWriter(logFile, true);
            sw.WriteLine("********** {0} **********", DateTime.Now);
            if (exc.InnerException != null)
            {
                sw.Write("Inner Exception Type: ");
                sw.WriteLine(exc.InnerException.GetType().ToString());
                sw.Write("Inner Exception: ");
                sw.WriteLine(exc.InnerException.Message);
                sw.Write("Inner Source: ");
                sw.WriteLine(exc.InnerException.Source);
                sw.WriteLine("********** {0} **********", DateTime.Now);
                sw.Write("Inner Inner Exception Type: ");
                sw.WriteLine(exc.InnerException.InnerException.GetType().ToString());
                sw.Write("Inner Inner Exception: ");
                sw.WriteLine(exc.InnerException.InnerException.Message);
                sw.Write("Inner Inner Source: ");
                sw.WriteLine(exc.InnerException.InnerException.Source);
                if (exc.InnerException.StackTrace != null)
                {
                    sw.WriteLine("Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.StackTrace);

                    sw.WriteLine("Inner Inner Stack Trace: ");
                    sw.WriteLine(exc.InnerException.InnerException.StackTrace);
                }
            }
            sw.Write("Exception Type: ");
            sw.WriteLine(exc.GetType().ToString());
            sw.WriteLine("Exception: " + exc.Message);
            sw.WriteLine("Source: " + source);
            sw.WriteLine("Stack Trace: ");
            if (exc.StackTrace != null)
            {
                sw.WriteLine(exc.StackTrace);
                sw.WriteLine();
            }
            sw.Close();
        }

        public void TLog(string exc, string source)
        {
            // Include logic for logging exceptions
            // Get the absolute path to the log file
            string logFile = "~/Content/TLog.txt";
            logFile = HttpContext.Current.Server.MapPath(logFile);

            // Open the log file for append and write the log
            StreamWriter sw = new StreamWriter(logFile, true);
            sw.WriteLine("********** {0} **********", DateTime.Now);
            sw.WriteLine("Transaction Name:{0}",source);
            sw.WriteLine("{0}",exc);
            sw.Close();
        }
    }
}