﻿
using Cooptavanza.Wallet;
using MoneySwap.com;
using System;

public class MEXMessageAppenderHelper
 {
       

        public string requestTrasaction(ChinaRemitEx n)
        {

            string message = "";
       
            string amlStatus = n.amlStatus != null ? n.amlStatus : "";
            string amountFrom = n.amountFrom != 0 ? n.amountFrom + "" : "";
            string amountTo = n.amountTo != 0 ? n.amountTo + "" : "";
            string answer = n.answer != null ? n.answer : "";
            string currencyFrom = n.currencyFrom != null ? "" + n.currencyFrom : "";
            string currencyTo = n.currencyTo != null ? "" + n.currencyTo : "";
            string date = String.Format("{0:yyyy-MM-dd}", n.date) != "0001-01-01" ? String.Format("{0:yyyy-MM-dd}", n.date) : "";
            string floatCurrency = n.floatCurrency != null ? n.floatCurrency : "";
            string kioskAddress = n.kioskAddress != null ? n.kioskAddress : "";
            string kioskName = n.kioskName != null ? n.kioskName : "";
            string kycCheck = n.kycCheck != null ? n.kycCheck : "";
            string MEXType = n.MEXType != null ? n.MEXType : "";
            string note = n.note != null ? n.note : "";
            string originatedUID = n.originatorUID != null ? n.originatorUID: "";
            string partnerCode = n.partnerCode != null ? n.partnerCode: "";
            string rcvAccntNo = n.rcvAccntNo != null ? n.rcvAccntNo : "";
            string rcvAccntType = n.rcvAccntType != null ? n.rcvAccntType :  "";
            string rcvEmail = n.rcvEmail != null ? n.rcvEmail : "";
            string rcvFirstName = n.rcvFirstName != null ? n.rcvFirstName : "";
            string rcvIdNumber = n.rcvIdNumber != null ? n.rcvIdNumber : "";
            string rcvIdType = n.rcvIdType != null ? n.rcvIdType : "";
            string rcvLastName = n.rcvLastName != null ? n.rcvLastName : "";
            string rcvMiddleName = n.rcvMiddleName != null ? n.rcvMiddleName : "";
            string rcvMobileNo = n.rcvMobileNo != null ? n.rcvMobileNo : "";
            string rcvSuffix = n.rcvSuffix != null ? n.rcvSuffix : "";
            string referenceNo = n.referenceNo != null ? n.referenceNo : "";
            string serviceFee = n.serviceFee != 0 ? "" + n.serviceFee : "";
            string sndAccntNo = n.sndAccntNo != null ? n.sndAccntNo : "";
            string sndCity = n.sndCity != null ? n.sndCity : "";
            string sndCountry = n.sndCountry != null ? n.sndCountry : "";
            string sndEmail = n.sndEmail != null ? n.sndEmail : "";
            string sndFirstName = n.sndFirstName != null ? n.sndFirstName : "";
            string sndIdNo = n.sndIdNo != null ? n.sndIdNo : "";
            string sndIdType = n.sndIdType != null ? n.sndIdType : "";
            string sndKycUrl = n.sndKycUrl != null ? n.sndKycUrl : "";
            string sndLastNAme = n.sndLastName != null ? n.sndLastName : "";
            string sndMiddleName = n.sndMiddleName != null ? n.sndMiddleName : "";
            string sndPhoneNo = n.sndPhoneNo != null ? n.sndPhoneNo : "";
            string sndRemitPurpose = n.sndRemitPurpose != null ? n.sndRemitPurpose : "";
            string sndState = n.sndState != null ? n.sndState : "";
            string sndStreet = n.sndStreet != null ? n.sndStreet : "";
            string sndSuffix = n.sndSuffix != null ? n.sndSuffix : "";
            string tellerFirstName = n.tellerFirstName != null ? n.tellerFirstName : "";
            string tellerLastName = n.tellerLastName != null ? n.tellerLastName : "";
            string tranAmnt = n.tranAmt != null ? n.tranAmt : "";
            string tranCurr = n.tranCurr != null ? n.tranCurr : "";
            string transactionType = n.transactionType != null ? n.transactionType : "";
            string validateReferenceId = n.validateReferenceId != null ? n.validateReferenceId : "";

        message = "" +
                    amlStatus +
                    amountFrom +
                    amountTo +
                    answer +
                    currencyFrom +
                    currencyTo +
                    date +
                    floatCurrency +
                    kioskAddress +
                    kioskName +
                    kycCheck +
                    MEXType +
                    note +
                    originatedUID +
                    partnerCode +
                    rcvAccntNo +
                    rcvAccntType +
                    rcvEmail +
                    rcvFirstName +
                    rcvIdNumber +
                    rcvIdType +
                    rcvLastName +
                    rcvMiddleName +
                    rcvMobileNo +
                    rcvSuffix +
                    referenceNo +
                    serviceFee +
                    sndAccntNo +
                    sndCity +
                    sndCountry +
                    sndEmail +
                    sndFirstName +
                    sndIdNo +
                    sndIdType +
                    sndKycUrl +
                    sndLastNAme +
                    sndMiddleName +
                    sndPhoneNo +
                    sndRemitPurpose +
                    sndState +
                    sndStreet +
                    sndSuffix +
                    tellerFirstName +
                    tellerLastName +
                    tranAmnt +
                    tranCurr +
                    transactionType +
                    validateReferenceId;

        return message.ToUpper();
        }

    public string requestRemit(MoneyExpressApiRemitRequestV04 x)
    {
        string message = "";
        message = x.AmlStatus +
            x.FloatCurrency +
            x.IncludeFees +
            x.KioskAddress +
            x.KioskName +
            x.KycCheck +
            x.MerchantId +
            x.Note +
            x.OriginatorUID +
            x.RcvAccntNo +
            x.RcvAccntType +
            x.RcvFirstName +
            x.RcvIdNumber +
            x.RcvIdType +
            x.RcvLastName +
            x.ReferenceId +
            x.SndAddress +
            x.SndFirstName +
            x.SndIdNo +
            x.SndIdType +
            x.SndKycUrl +
            x.SndLastName +
            x.SndPhoneNo +
            x.SndRemitPurpose +
            x.TellerFirstName +
            x.TellerLastName +
            x.TranAmt +
            x.TranCurr +
            x.ValidateReferenceId;

        return message.ToUpper();
    }
   

}

