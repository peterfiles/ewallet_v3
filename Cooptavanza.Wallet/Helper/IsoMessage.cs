﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Cooptavanza.Wallet.Controllers
{
    class IsoMessage
    {

        int[] DEVarLen = new int[130];
        int[] DEFixLen = new int[130];
      
        private string path = @"C:\";
        DateTime dt = DateTime.Now;
      
        public IsoMessage()
        {
           
            DEVarLen[2] = 2; DEVarLen[32] = 2; DEVarLen[33] = 2; DEVarLen[34] = 2; DEVarLen[35] = 2; DEVarLen[36] = 3;
            DEVarLen[44] = 2; DEVarLen[45] = 2; DEVarLen[46] = 3; DEVarLen[47] = 3; DEVarLen[48] = 3; DEVarLen[54] = 3;
            DEVarLen[55] = 3; DEVarLen[56] = 3; DEVarLen[57] = 3; DEVarLen[58] = 3; DEVarLen[59] = 3;
            DEVarLen[60] = 1; DEVarLen[61] = 3; DEVarLen[62] = 3; DEVarLen[63] = 3; DEVarLen[72] = 3; DEVarLen[99] = 2;
            DEVarLen[100] = 2; DEVarLen[102] = 2; DEVarLen[103] = 2; DEVarLen[104] = 3; DEVarLen[105] = 3;
            DEVarLen[106] = 3; DEVarLen[107] = 3; DEVarLen[108] = 3; DEVarLen[109] = 3; DEVarLen[110] = 3;
            DEVarLen[111] = 3; DEVarLen[112] = 3; DEVarLen[113] = 2; DEVarLen[114] = 3; DEVarLen[115] = 3;
            DEVarLen[116] = 3; DEVarLen[117] = 3; DEVarLen[118] = 3; DEVarLen[119] = 3; DEVarLen[120] = 2; DEVarLen[121] = 3;
            DEVarLen[122] = 3; DEVarLen[123] = 3; DEVarLen[124] = 3; DEVarLen[125] = 2; DEVarLen[126] = 1; DEVarLen[127] = 2; DEVarLen[31] = 2;
            //DEVarLen[43] = 2;
            DEVarLen[94] = 2; DEFixLen[95] = 10;
            // "-" means not numeric.

            DEFixLen[0] = 16; DEFixLen[1] = 16; DEFixLen[3] = 6; DEFixLen[4] = 12;
            DEFixLen[5] = 12; DEFixLen[6] = 12; DEFixLen[7] = 10; DEFixLen[8] = 8;
            DEFixLen[9] = 8; DEFixLen[10] = 8; DEFixLen[11] = 6; DEFixLen[12] = 6;
            DEFixLen[13] = 4; DEFixLen[14] = 4; DEFixLen[15] = 4; DEFixLen[16] = 4;
            DEFixLen[17] = 4; DEFixLen[18] = 4; DEFixLen[19] = 3; DEFixLen[20] = 3;
            DEFixLen[21] = 3; DEFixLen[22] = 3; DEFixLen[23] = 3; DEFixLen[24] = 3;
            DEFixLen[25] = 2; DEFixLen[26] = 2  ; DEFixLen[27] = 1; DEFixLen[28] = 8;
            DEFixLen[29] = 8; DEFixLen[30] = 24; DEFixLen[37] = 12;
            DEFixLen[38] = 6; DEFixLen[39] = 2; DEFixLen[40] = -3; DEFixLen[41] = 8;
            DEFixLen[42] = 15; DEFixLen[43] = 40; DEFixLen[49] = 3; DEFixLen[50] = 3;
            DEFixLen[51] = 3; DEFixLen[52] = 16; DEFixLen[53] = 16; DEFixLen[64] = -4;
            DEFixLen[65] = -16; DEFixLen[66] = 1; DEFixLen[67] = 2; DEFixLen[68] = 3;
            DEFixLen[69] = 3; DEFixLen[70] = 3; DEFixLen[71] = 8; DEFixLen[73] = 6;
            DEFixLen[74] = 10; DEFixLen[75] = 10; DEFixLen[76] = 10; DEFixLen[77] = 10;
            DEFixLen[78] = 10; DEFixLen[79] = 10; DEFixLen[80] = 10; DEFixLen[81] = 10;
            DEFixLen[82] = 12; DEFixLen[83] = 12; DEFixLen[84] = 12; DEFixLen[85] = 12;
            DEFixLen[86] = 15; DEFixLen[87] = 15; DEFixLen[88] = 15; DEFixLen[89] = 15;
            DEFixLen[90] = 44; DEFixLen[91] = -1; DEFixLen[92] = 2; DEFixLen[93] = 5;
            DEFixLen[96] = -8; DEFixLen[97] = 16;
            DEFixLen[98] = -25; DEFixLen[101] = -17; DEFixLen[128] = -16;
        
        }
        public string[] Parse(string ISOmsg)
        {
            string[] DE = new string[130];

            string de1Binary = "";
            string de2Binary = "";
            int FieldNo;
            int myPos;
            int myLenght;
            int len;
            //Get MTI
            myPos = 0;
            myLenght = 4;
            string MTI = ISOmsg.Substring(myPos, myLenght);
            Writelog("Request Message Type : " + MTI);

            //========BM 129 is the MTI============
            FieldNo = 129;


            DE[FieldNo] = MTI;
            //========================================
            //Get BitMap 1a
            myPos += myLenght;
            myLenght = 16;
            DE[0] = ISOmsg.Substring(myPos, myLenght);
            //Convert BM0 to Binary


            de1Binary = DEtoBinary(DE[0]);


            //BitMap #1

            FieldNo = 1;
            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght;
                myLenght = 16;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                de2Binary = DEtoBinary(DE[FieldNo]);

            }

            //------------BM2--------------
            FieldNo = 2;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, 2));//LLVAR
                myPos += 2;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }

            //------------BM3--------------
            FieldNo = 3;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }


            //------------BM4--------------
            FieldNo = 4;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM5--------------
            FieldNo = 5;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }


            //------------BM6--------------
            FieldNo = 6;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM7--------------
            FieldNo = 7;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM8--------------
            FieldNo = 8;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM9--------------
            FieldNo = 9;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 10--------------
            FieldNo = 10;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 11--------------
            FieldNo = 11;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 12--------------
            FieldNo = 12;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 13--------------
            FieldNo = 13;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 14--------------
            FieldNo = 14;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 15--------------
            FieldNo = 15;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 16--------------
            FieldNo = 16;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 17--------------
            FieldNo = 17;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 18--------------
            FieldNo = 18;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------BM 19--------------
            FieldNo = 19;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //------------BM 20--------------
            FieldNo = 20;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------BM 21--------------
            FieldNo = 21;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------BM 22--------------
            FieldNo = 22;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 23;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 24;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 25;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 2; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 26;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 27;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 1; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 28;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 29;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 30;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 31;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght;
                myLenght = 23;
                DE[FieldNo] = ISOmsg.Substring(myPos + 2, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
                myPos += 2;
            }
            //--------------------------
            FieldNo = 32;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, 2));
                myPos += 2;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }

            //--------------------------
            FieldNo = 33;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, 2));
                myPos += 2;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 34;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, 2));
                myPos += 2;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 35;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 36;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 37;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 38;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 39;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 2; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 40;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 41;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 16; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 42;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 15; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 43;
            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 40; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            //{
            //    myPos += myLenght; len = 2;
            //    myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
            //    myPos += len;
            //    DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
            //}

            //--------------------------
            FieldNo = 44;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 45;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 46;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 47;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 48;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len));
                myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }


            //--------------------------
            FieldNo = 49;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 50;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 51;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 52;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 16; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 53;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 18; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 54;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 55;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 56;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 57;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 58;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 59;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 60;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 1;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));

            }
            //--------------------------
            FieldNo = 61;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 62;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 63;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }

            //--------------------------
            FieldNo = 64;

            if (de1Binary.Substring(FieldNo - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            /*
            for (int I = 0; I <= 64; I++)
            {
                if (BM[I] != null)

                { Console.WriteLine("BM" + I + ": " + BM[I] + " = " + BMname[I]); }
            }
            */

            //===CHECK POINT FOR BM# 65 to 128============
            if (de2Binary == "") return DE; ///<----IF DATA ELEMENT <= 64
            if (de2Binary == "0000000000000000000000000000000000000000000000000000000000000000") return DE;
            //============================================

            //--------------------------
            int comp = 64;
            FieldNo = 65;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 16; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 66;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 1; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 67;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 2; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 68;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 69;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 70;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 3; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 71;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //-------------------------------------------------
            FieldNo = 72;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //--------------------------
            FieldNo = 73;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 6; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //--------------------------
            FieldNo = 74;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 75;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 76;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 77;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 78;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 79;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 80;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 81;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 10; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //-----------------------------
            FieldNo = 82;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 83;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 84;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 85;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 12; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 86;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 15; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 87;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 15; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 88;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 15; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 89;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 15; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            FieldNo = 90;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 44; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 91;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 1; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 92;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 2; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 93;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 5; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 94;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 7; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 95;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 42; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 96;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 8; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 97;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 16; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //------------------------------
            FieldNo = 98;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 25; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //-------------------------------------------------
            FieldNo = 99;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 100;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //------------------------------
            FieldNo = 101;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 17; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }

            //-------------------------------------------------
            FieldNo = 102;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 103;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //----------------------------------
            FieldNo = 104;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 105;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 106;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 107;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 108;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 109;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 110;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 111;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 112;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //----------------------------
            FieldNo = 113;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //-------------------------
            FieldNo = 114;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 115;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 116;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 117;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 118;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 119;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 120;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 121;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 122;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            FieldNo = 123;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }

            FieldNo = 124;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //---------------------------------
            FieldNo = 125;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 2;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //----------------------------------
            FieldNo = 126;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 1;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //-----------------------------------
            FieldNo = 127;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1")
            {
                myPos += myLenght; len = 3;
                myLenght = Convert.ToInt16(ISOmsg.Substring(myPos, len)); myPos += len;
                DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);
                 Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo]));
            }
            //------------------------------
            FieldNo = 128;

            if (de2Binary.Substring(FieldNo - comp - 1, 1) == "1") { myPos += myLenght; myLenght = 4; DE[FieldNo] = ISOmsg.Substring(myPos, myLenght);  Writelog("DE[" + FieldNo+"]: "+Convert.ToString(DE[FieldNo])); }
            //-------------------------------------------------



            //==========================================
            return DE;
            //==========================================




        }
        public string Build(string[] DE, string MTI)
        {
            string newISO = MTI;
            try
            {
                


                string newDE1 = "";
                for (int I = 2; I <= 64; I++) { if (DE[I] != null) { newDE1 += "1"; } else { newDE1 += "0"; } }


                string newDE2 = "";
                for (int I = 65; I <= 128; I++) { if (DE[I] != null) { newDE2 += "1"; } else { newDE2 += "0"; } }

                if (newDE2 == "0000000000000000000000000000000000000000000000000000000000000000")
                { newDE1 = "0" + newDE1; }
                else { newDE1 = "1" + newDE1; }



                string DE1Hex = String.Format("{0:X1}", Convert.ToInt64(newDE1, 2));
                DE1Hex = DE1Hex.PadLeft(16, '0'); //Pad-Left
                DE[0] = DE1Hex;

                string DE2Hex = String.Format("{0:X1}", Convert.ToInt64(newDE2, 2));
                DE2Hex = DE2Hex.PadLeft(16, '0'); //Pad-Left
                DE[1] = DE2Hex;

                if (DE2Hex == "0000000000000000") DE[1] = null;

                Writelog("Response Message Type : " + MTI);

                for (int I = 0; I <= 128; I++)
                {
                    if (DE[I] != null)
                    {
                        if (DEVarLen[I] < 1)
                        {

                            if (DEFixLen[I] < 0)
                            {
                                string BMPadded = DE[I].PadRight(Math.Abs(DEFixLen[I]), ' ');
                                string sBM = BMPadded.Substring(0, Math.Abs(DEFixLen[I]));
                                Writelog("DE[" + I.ToString() + "] : " + sBM);
                                newISO += sBM;

                            }
                            else
                            {
                                string BMPadded = DE[I].PadLeft(DEFixLen[I], '0');
                                string sBM = BMPadded.Substring(BMPadded.Length - Math.Abs(DEFixLen[I]), Math.Abs(DEFixLen[I]));
                                Writelog("DE[" + I.ToString() + "] : " + sBM);
                                newISO += sBM;
                            }
                        }
                        else
                        {
                            string li = DE[I].Length.ToString();
                            string paddedli = li.PadLeft(DEVarLen[I], '0');
                            Writelog("DE[" + I.ToString() + "] : " + paddedli);
                            newISO += (paddedli + DE[I]);
                        }
                    }
                }
                return newISO;
            }
            catch (Exception Ex)
            {
                Writelog("Exception: " + Ex.ToString() +"newISO "+newISO);
            
            }

            return newISO;
            


        }
        public string DEtoBinary(string HexDE)
        {
            string deBinary = "";
            for (int I = 0; I <= 15; I++)
            {
                deBinary = deBinary + Hex2Binary(HexDE.Substring(I, 1));

            }

            return deBinary;

        }
        public string Hex2Binary(string hexstring)
        {

            String binarystring = String.Join(String.Empty, hexstring.Select(c => Convert.ToString(Convert.ToInt32(c.ToString(), 16), 2).PadLeft(4, '0')));

            return binarystring;
        }
        public void Writelog(string LineText)
        {
            
            StreamWriter swExtractFile;
            if (File.Exists(path + "\\" + dt.ToString("dd-MMM-yyyy") + "\\" + "Log_" + dt.ToString("dd-MMM-yyyy")+".txt"))
            {
                swExtractFile = File.AppendText(path + "\\" + dt.ToString("dd-MMM-yyyy") + "\\" + "Log_" + dt.ToString("dd-MMM-yyyy")+".txt");
            }
            else
            {
                Directory.CreateDirectory( path + "\\" + dt.ToString("dd-MMM-yyyy"));
                swExtractFile = File.CreateText(path + "\\" + dt.ToString("dd-MMM-yyyy") + "\\" + "Log_" + dt.ToString("dd-MMM-yyyy")+".txt");
            }

            swExtractFile.WriteLine("{0},{1}",String.Format("{0:G}",dt ), LineText);
            swExtractFile.Close();
        }
       
      
    }
}
