﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cooptavanza.Wallet.Controllers
{
    public class WebRequestHelper
    {

        public string WebRequestResponse(string url, string method, string data)
       
        {


            WebRequest request = WebRequest.Create(url);
            // If required by the server, set the credentials.
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/x-www-form-urlencoded";
         
            string postData = data;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            request.Method = method;
            request.ContentLength = byteArray.Length;

           

           
            Stream newStream = request.GetRequestStream();
            newStream.Write(byteArray, 0, byteArray.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);

            dataStream = request.GetRequestStream();

            // Write the data to the request stream.
       //     dataStream.Write(byteArray, 0, byteArray.Length);
        
            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;
            
        }

        public object GetGeolocationGoogleAPI(string data, string method)
        {
            var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + data + "&key=" + "AIzaSyDurE1NoAcWUG-maYUkVakjBTJgmKT_d0U";
            string res = string.Empty;
            using (WebClient wb = new WebClient())
            {
                res = wb.DownloadString(url.Trim());
                res = res.ToString().Trim();
                res.ToString().Replace("\r\n", string.Empty);
                dynamic d = JsonConvert.DeserializeObject(res);
                return d;
            }
        }

        public object GetGeoLocation()
        {
            var url = "http://ip-api.com/json";
            string res = string.Empty;
            using (WebClient wb = new WebClient())
            {
                res = wb.DownloadString(url);
                res = res.ToString().Trim();
                res.ToString().Replace("\r\n", string.Empty);
                dynamic d = JsonConvert.DeserializeObject(res);
                return d;
            }
        }

    }
}