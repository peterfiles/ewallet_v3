﻿
using Cooptavanza.Wallet;
using System;

public class MessageAppenderHelper
 {
        public string requestStatus(RequestStatusEx n)
        {
            string message = "";
            string partnerCode = (n.partnerCode).ToUpper();
            string referenceNo = (n.referenceNo).ToUpper();

            message = "" + partnerCode + referenceNo;

            return message.ToUpper();
        }

        public string requestCommon(RequestCommonEx n)
        {
            string message = "";
            string code = n.idCode;
            string intparam1 = n.intparam1 != 0 ? "" + n.intparam1 : "";
            string intparam2 = n.intparam2 != 0 ? "" + n.intparam2 : "";
            string intparam3 = n.intparam3 != 0 ? "" + n.intparam3 : "";
            string intparam4 = n.intparam4 != 0 ? "" + n.intparam4 : "";
            string param1 = n.param1 != null ? n.param1 : "";
            string param2 = n.param2 != null ? n.param2 : "";
            string param3 = n.param3 != null ? n.param3 : "";
            string param4 = n.param4 != null ? n.param4 : "";

            message = code + intparam1 + intparam2 + intparam3 + intparam4
                    + param1 + param2 + param3 + param4;

            return message.ToUpper();
        }

        public string requestTransactionList(RequestListEx n)
        {
            string message = "";
            string dateEnd = String.Format("{0:yyyy-MM-dd}",n.dateEnd) != "0001-01-01" ? String.Format("{0:yyyy-MM-dd}",n.dateEnd) : "";
            string dateRequest = String.Format("{0:yyyy-MM-dd}", n.dateRequest) != "0001-01-01" ? String.Format("{0:yyyy-MM-dd}", n.dateRequest) : "";
            string dateStart = String.Format("{0:yyyy-MM-dd}", n.dateStart) != "0001-01-01" ? String.Format("{0:yyyy-MM-dd}", n.dateStart) : "";
            string orderby = n.orderBy != null ? n.orderBy : "";
            string pageno = n.pageNo != 0 ? "" + n.pageNo : "";
            string pageSize = n.pageSize != 0 ? "" + n.pageSize : "";
            string partnerCode = (n.partnerCode).ToUpper();
            string referenceNo = n.referenceNo != null ? (n.referenceNo).ToUpper() : "";
            string searchText = n.searchText != null ? (n.searchText).ToUpper() : "";
            string transactionType = n.transactionType != null ? n.transactionType : "";
           
            

            message = "" + dateEnd + dateRequest + dateStart + orderby + pageno + pageSize + partnerCode + referenceNo + searchText + transactionType;

            return message.ToUpper();
        }

        public string requestTrasaction(ETransferEx n)
        {

            string message = "";
            string amount = n.amount > 0 ? "" + n.amount : "";
            string answer = n.answer != null ? n.answer : "";
            string currencyFrom = n.currencyFrom != null ? n.currencyFrom : "";
            string currencyTo = n.currencyTo != null ? n.currencyTo : "";
            string date = String.Format("{0:yyyy-MM-dd}", n.date) != "0001-01-01" ? String.Format("{0:yyyy-MM-dd}", n.date) : "";
            string partnerCode = n.partnerCode != null ? "" + n.partnerCode : "";
            string question = n.question != null ? n.question : "";
            string rcvAddress = n.rcvAddress != null ? n.rcvAddress : "";
            string rcvCity = n.rcvCity != null ? n.rcvCity : "";
            string rcvCountry = n.rcvCountry != null ? n.rcvCountry : "";
            string rcvEmail = n.rcvEmail != null ? n.rcvEmail : "";
            string rcvFirstName = n.rcvFirstName != null ? n.rcvFirstName : "";
            string rcvLastName = n.rcvLastName != null ? n.rcvLastName : "";
            string rcvMiddleName = n.rcvMiddleName != null ? n.rcvMiddleName : "";
            string rcvMobileno = n.rcvMobileno != null ? n.rcvMobileno : "";
            string rcvPostalCode = n.rcvPostalCode != null ? n.rcvPostalCode : "";
            string rcvStates = n.rcvStates != null ? n.rcvStates : "";
            string rcvSuffix = n.rcvSuffix != null ? n.rcvSuffix : "";
            string referenceNo = n.referenceNo != null ? n.referenceNo : "";
            string serviceFee = n.serviceFee > 0 ? "" + n.serviceFee : "";
            string sndAccntNo = n.sndAccntNo != null ? "" + n.sndAccntNo : "";
            string sndAddress = n.sndAddress != null ? n.sndAddress : "";
            string sndCity = n.sndCity != null ? n.sndCity : "";
            string sndCountry = n.sndCountry != null ? n.sndCountry : "";
            string sndEmail = n.sndEmail != null ? "" + n.sndEmail : "";
            string sndFirstName = n.sndFirstName != null ? n.sndFirstName : "";
            string sndLastName = n.sndLastName != null ? n.sndLastName : "";
            string sndMiddleName = n.sndMiddleName != null ? n.sndMiddleName : "";
            string sndMobileNo = n.sndMobileNo != null ? n.sndMobileNo : "";
            string sndPhoneNo = n.sndPhoneNo != null ? n.sndPhoneNo : "";
            string sndPostalCode = n.sndPostalCode != null ? n.sndPostalCode : "";
            string sndStates = n.sndStates != null ? n.sndStates : "";
            string sndSuffix = n.sndSuffix != null ? n.sndSuffix : "";
            string transactionType = n.transactionType != null ? n.transactionType : "";

            message = "" 
                    + amount
                    + answer
                    + currencyFrom
                    + currencyTo
                    + date
                    + partnerCode 
                    + question
                    + rcvAddress
                    + rcvCity
                    + rcvCountry
                    + rcvEmail
                    + rcvFirstName
                    + rcvLastName
                    + rcvMiddleName
                    + rcvMobileno
                    + rcvPostalCode
                    + rcvStates
                    + rcvSuffix
                    + referenceNo
                    + serviceFee
                    + sndAccntNo
                    + sndAddress
                    + sndCity
                    + sndCountry
                    + sndEmail
                    + sndFirstName
                    + sndLastName
                    + sndMiddleName
                    + sndMobileNo
                    + sndPhoneNo
                    + sndPostalCode
                    + sndStates
                    + sndSuffix
                    + transactionType;

            return message.ToUpper();
        }

    public string requestLunexTrasaction(LunexCBSEx n)
    {

        string message = "";
        string amount = n.amount > 0 ? "" + n.amount : "";
        string billAccountNo = n.billAccountNo != null ? n.billAccountNo : "";
        string currency = n.currency != null ? n.currency : "";
        string date = String.Format("{0:yyyy-MM-dd}", n.date) != "0001-01-01" ? String.Format("{0:yyyy-MM-dd}", n.date) : "";
        string denomanation = n.denomanation != null ? n.denomanation : "";
        string partnerCode = n.partnerCode != null ? "" + n.partnerCode : "";
        string product = n.product != null ? n.product : "";
        string referenceNo = n.referenceNo != null ? n.referenceNo : "";
        string serviceFee = n.serviceFee > 0 ? "" + n.serviceFee : "";
        string sndAccntNo = n.sndAccntNo != null ? "" + n.sndAccntNo : "";
        string sndAddress = n.sndAddress != null ? n.sndAddress : "";
        string sndCity = n.sndCity != null ? n.sndCity : "";
        string sndCountry = n.sndCountry != null ? n.sndCountry : "";
        string sndEmail = n.sndEmail != null ? "" + n.sndEmail : "";
        string sndFirstName = n.sndFirstName != null ? n.sndFirstName : "";
        string sndLastName = n.sndLastName != null ? n.sndLastName : "";
        string sndMiddleName = n.sndMiddleName != null ? n.sndMiddleName : "";
        string sndMobileNo = n.sndMobileNo != null ? n.sndMobileNo : "";
        string sndPhoneNo = n.sndPhoneNo != null ? n.sndPhoneNo : "";
        string sndPostalCode = n.sndPostalCode != null ? n.sndPostalCode : "";
        string sndStates = n.sndStates != null ? n.sndStates : "";
        string sndSuffix = n.sndSuffix != null ? n.sndSuffix : "";
        string transactionType = n.transactionType != null ? n.transactionType : "";       

        message = ""
                + amount
                + billAccountNo
                + currency
                + date
                + denomanation
                + partnerCode
                + product
                + referenceNo
                + serviceFee
                + sndAccntNo
                + sndAddress
                + sndCity
                + sndCountry
                + sndEmail
                + sndFirstName
                + sndLastName
                + sndMiddleName
                + sndMobileNo
                + sndPhoneNo
                + sndPostalCode
                + sndStates
                + sndSuffix
                + transactionType;

        return message.ToUpper();
    }

    public string requestQoute(RequestQouteEx n)
    {
        string partnerCode = n.partnerCode.ToUpper();
        string transactionType = n.transactionType.ToUpper();

        
        return partnerCode + transactionType;
    }

    public string requestTopUpProductsBySKU(RequestTopupWithSKU n)
    {
        string message = "" +
                         n.id +
                         n.partnerCode +
                         n.transactionType;

        return message.ToUpper();
            
    }

    public string requestTopUpProcess(ProcessTopUpEx n)
    {
        string accountNumber = n.accountNumber != null ? n.accountNumber : "";
        string amount = n.amount > 0 ? "" + n.amount : "";
        string currency = n.currency != null ? n.currency : "";
        string email = n.email != null ? n.email : "";
        string firstname = n.firstname != null ? n.firstname : "";
        string lastname = n.lastname != null ? n.lastname : "";
        string middlename = n.middlename != null ? n.middlename : "";
        string partnerCode = n.partnerCode != null ? "" + n.partnerCode : "";
        string mobilenumber = n.mobilenumber != null ? n.mobilenumber : "";
        string phoneNumber = n.phoneNumber != null ? n.phoneNumber : "";
        string phonetoTopUp = n.phonetoTopUp != null ? n.phonetoTopUp : "";
        string referenceNo = n.referenceNo != null ? n.referenceNo : "";
        string skuid = n.skuid != null ? n.skuid : "";
        string suffix = n.suffix != null ? n.suffix : "";
        string transactionType = n.transactionType != null ? n.transactionType : "";

        string message = "" +
                         accountNumber +
                         amount +
                         currency +
                         email +
                         firstname +
                         lastname +
                         middlename +
                         mobilenumber +
                         partnerCode +
                         phoneNumber +
                         phonetoTopUp +
                         referenceNo +
                         skuid +
                         suffix +
                         transactionType;

        return message.ToUpper();
    }

}

