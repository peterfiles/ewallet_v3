﻿using System;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Xml;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cooptavanza.Wallet.Controllers
{
    public class WebRequestHelperLunex
    {

        public string WebRequestResponseAsync(string data, string method)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                // orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00";
                //var url = "http://test-extapi.lunexgroup.com/pos/sellers/CR01D999/" + data; // dev
                var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ContentType = "application/xml";

                sb.AppendFormat("URL:{0}", url);
                sb.AppendLine();
                //var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
                var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
                request.Method = method;

                sb.AppendFormat("Headers:{0}", request.Headers);
                sb.AppendLine();
                sb.AppendFormat("Method:{0}", request.Method);
                sb.AppendLine();
                sb.AppendFormat("HttpWebRequest:{0}", request);
                sb.AppendLine();

                Stream newStream = request.GetRequestStream();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                sb.AppendFormat("HttpWebResponse:{0}", response);
                sb.AppendLine();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                dataStream = request.GetRequestStream();

                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }catch(Exception ex)
            {
                if (ex is InvalidOperationException ||
                   ex is ProtocolViolationException ||
                   ex is WebException)
                {
                    return ex.ToString() + "\n" + "Request:" + sb.ToString();
                }
                else
                {
                    return ex.ToString() + "\n" + "Request:" + sb.ToString();
                }
            }

        }

        public string WebGetRequest(string data, string method)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                // orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00";
                //var url = "http://test-extapi.lunexgroup.com/pos/sellers/CR01D999/" + data; // dev
                var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ContentType = "application/xml";
                sb.AppendFormat("URL:{0}", url);
                sb.AppendLine();
                //ServicePointManager.ServerCertificateValidationCallback += new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);

                //var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
                var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                
                request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
                request.Method = method;

                sb.AppendFormat("Headers:{0}", request.Headers);
                sb.AppendLine();
                sb.AppendFormat("Method:{0}", request.Method);
                sb.AppendLine();
                sb.AppendFormat("HttpWebRequest:{0}", request);
                sb.AppendLine();
                //       request.Accept = "application/json";
                //  Stream newStream = request.GetRequestStream();
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                sb.AppendFormat("HttpWebResponse:{0}", response);
                sb.AppendLine();



                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);

                //     dataStream = request.GetRequestStream();

                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                dataStream.Close();
                response.Close();
                return responseFromServer;
            }

            catch (Exception ex)
            {
                if (ex is InvalidOperationException ||
                    ex is ProtocolViolationException ||
                    ex is WebException)
                {
                    return ex.ToString() + "\n" + "Request:" + sb.ToString(); 
                }
                else {
                    return ex.ToString() + "\n" + "Request:" + sb.ToString(); 
                }
            }


        }

       public static string log
        {
            get; set;
        }

        public  string logs()
        {
            return log;
        }
        

        public static bool ValidateServerCertificate(object sender, System.Security.Cryptography.X509Certificates.X509Certificate certification, System.Security.Cryptography.X509Certificates.X509Chain chain, System.Net.Security.SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public string WebRequestResponseAsyncCustom(string data, string method)
        {
            // orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00";
            //var url = "http://test-extapi.lunexgroup.com/pos/sellers/CR01D999/" + data; // dev
            var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
            WebRequest request = WebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/xml";

            //var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
            var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
            request.Method = method;

            Stream newStream = request.GetRequestStream();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            dataStream = request.GetRequestStream();

            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            return responseFromServer;

        }

        public string WebGetRequestCustom(string data, string method)
        {
            try
            {
                // orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00";
                //var url = "http://test-extapi.lunexgroup.com/pos/sellers/CR01D999/" + data; // dev
                //var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
                //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                //request.Credentials = CredentialCache.DefaultCredentials;
                //request.ContentType = "application/xml";

                ////var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
                //var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                //request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
                //request.Method = method;
                ////       request.Accept = "application/json";
                ////  Stream newStream = request.GetRequestStream();
                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Stream dataStream = response.GetResponseStream();
                //StreamReader reader = new StreamReader(dataStream);

                ////     dataStream = request.GetRequestStream();

                //string responseFromServer = reader.ReadToEnd();
                //reader.Close();
                //dataStream.Close();
                //response.Close();
                //return responseFromServer;
                var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
                string res = string.Empty;
                using (WebClient wb = new WebClient())
                {
                    var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                    wb.Headers.Add("Content-Type", "application/xml");
                    wb.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
                    res = wb.DownloadString(url.Trim());
                    res = res.ToString().Trim();
                    res.ToString().Replace("\r\n", string.Empty);
                    dynamic d = JsonConvert.DeserializeObject(res);
                    return d;
                }
            }

            catch (Exception ex)
            {
                if (ex is InvalidOperationException ||
                    ex is ProtocolViolationException ||
                    ex is WebException)
                {
                    return "";
                }
                else
                {
                    return ex.Message;
                }
            }


        }

        public object WebGetRequestCustomTest(int t)
        {
            try
            {
                if (t == 1)
                {
                    var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999";
                    string res = string.Empty;
                    using (WebClient wb = new WebClient())
                    {
                        //var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                        wb.Headers.Add("Content-Type", "application/xml");
                        wb.Headers.Add("Authorization", "Basic Y29vcHRhdmFuemE6YjdKNHFra0pYUA==");
                        return wb.DownloadString(url.Trim());
                        //res = res.ToString().Trim();
                        //res.ToString().Replace("\r\n", string.Empty);
                        //dynamic d = JsonConvert.DeserializeObject(res);
                        //return d;
                    }
                }
                else
                {
                    var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999";
                    HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                    request.Credentials = CredentialCache.DefaultCredentials;
                    request.ContentType = "application/xml";

                    //var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
                    //var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                    request.Headers.Add("Authorization", "Basic Y29vcHRhdmFuemE6YjdKNHFra0pYUA==");
                    request.Method = "GET";

                    Stream newStream = request.GetRequestStream();

                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);

                    dataStream = request.GetRequestStream();

                    string responseFromServer = reader.ReadToEnd();
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                    return responseFromServer;
                }
            }

            catch (Exception ex)
            {
                if (ex is InvalidOperationException ||
                    ex is ProtocolViolationException ||
                    ex is WebException)
                {
                    return "";
                }
                else
                {
                    return ex.Message;
                }
            }


        }

        public string WebGetRequestCustomCLARM(string data, string method)
        {
            try
            {
                // orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00";
                //var url = "http://test-extapi.lunexgroup.com/pos/sellers/CR01D999/" + data; // dev
                //var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
                //HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
                //request.Credentials = CredentialCache.DefaultCredentials;
                //request.ContentType = "application/xml";

                ////var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
                //var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                //request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
                //request.Method = method;
                ////       request.Accept = "application/json";
                ////  Stream newStream = request.GetRequestStream();
                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //Stream dataStream = response.GetResponseStream();
                //StreamReader reader = new StreamReader(dataStream);

                ////     dataStream = request.GetRequestStream();

                //string responseFromServer = reader.ReadToEnd();
                //reader.Close();
                //dataStream.Close();
                //response.Close();
                //return responseFromServer;
                //var url = "https://extapi.lunextelecom.com/pos/sellers/CRL01D999/" + data;
                string res = string.Empty;
                using (WebClient wb = new WebClient())
                {
                    var auth = Encoding.ASCII.GetBytes("cooptavanza:b7J4qkkJXP");
                    wb.Headers.Add("Content-Type", "application/xml");
                    wb.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
                    res = wb.DownloadString(data.Trim());
                    res = res.ToString().Trim();
                    res.ToString().Replace("\r\n", string.Empty);
                    dynamic d = JsonConvert.DeserializeObject(res);
                    return d;
                }
            }

            catch (Exception ex)
            {
                if (ex is InvalidOperationException ||
                    ex is ProtocolViolationException ||
                    ex is WebException)
                {
                    return "";
                }
                else
                {
                    return ex.Message;
                }
            }


        }

        public object WebGetRequestResponseAsync(string data, string method)
        {
            // orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00";
            var url = "http://test-extapi.lunexgroup.com/pos/sellers/CR01D999/" + data;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ContentType = "application/xml";

            var auth = Encoding.ASCII.GetBytes("cr060317:cr637");
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(auth));
            request.Method = method;

            //Stream newStream = request.GetRequestStream();

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);

            //  dataStream = request.GetRequestStream();

            string responseFromServer = reader.ReadToEnd();
            reader.Close();
            dataStream.Close();
            response.Close();
            //return responseFromServer;
            return productResult(responseFromServer);
        }



        public object productResult(string res)
        {
            StringBuilder sb = new StringBuilder();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(res);

            //sb.Append("{data: [{");
            XmlNodeList elemList = doc.GetElementsByTagName("List");
            List<object> getAllObjectName = new List<object>();
            List<object> getAllObjectInnerXML = new List<object>();
            List<object> Name = new List<object>();
            List<object> compileData = new List<object>();
            
            for (int i = 0; i < elemList.Count; i++)
            {
                if (elemList[0].HasChildNodes)
                {
                    for (int s = 0; s < elemList[0].ChildNodes.Count; s++)
                    {
                        if (elemList[0].ChildNodes[s].HasChildNodes)
                        {
                            if (!elemList[0].ChildNodes[s].Name.Equals("#text") && 
                                //!elemList[0].ChildNodes[s].Name.Equals("Amounts") && 
                                //!elemList[0].ChildNodes[s].Name.Equals("Amount") && 
                                !elemList[0].ChildNodes[s].Name.Equals("Product"))
                            {
                                getAllObjectName.Add(elemList[0].ChildNodes[s].Name);
                                getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].InnerXml);
                            }

                            for (int t = 0; t < elemList[0].ChildNodes[s].ChildNodes.Count; t++)
                            {

                                if (elemList[0].ChildNodes[s].ChildNodes[t].HasChildNodes)
                                {
                                    if (!elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("#text") && 
                                        //!elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("Amounts") && 
                                        //!elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("Amount") && 
                                        !elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("Product"))
                                    {
                                        getAllObjectName.Add(elemList[0].ChildNodes[s].ChildNodes[t].Name);
                                        getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].ChildNodes[t].InnerXml);
                                    }
                                    for (int u = 0; u < elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes.Count; u++)
                                    {
                                        if (elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].HasChildNodes)
                                        {
                                            if (!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("#text") && 
                                                //!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("Amounts") && 
                                                //!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("Amount") && 
                                                !elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("Product"))
                                            {
                                                getAllObjectName.Add(elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name);
                                                getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].InnerXml);
                                            }
                                            for (int v = 0; v < elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes.Count; v++)
                                            {
                                                if (!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes[v].Name.Equals("#text") && 
                                                    //!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes[v].Name.Equals("Amounts") && 
                                                    //!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes[v].Name.Equals("Amount") && 
                                                    !elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes[v].Name.Equals("Product"))
                                                {
                                                    getAllObjectName.Add(elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes[v].Name);
                                                    getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].ChildNodes[v].InnerXml);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("#text") && 
                                                //!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("Amounts") && 
                                                //!elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("Amount") && 
                                                !elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name.Equals("Product"))
                                            {
                                                getAllObjectName.Add(elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].Name);
                                                getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].ChildNodes[t].ChildNodes[u].InnerXml);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("#text") && 
                                        //!elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("Amounts") && 
                                        //!elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("Amount") && 
                                        !elemList[0].ChildNodes[s].ChildNodes[t].Name.Equals("Product"))
                                    {
                                        getAllObjectName.Add(elemList[0].ChildNodes[s].ChildNodes[t].Name);
                                        getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].ChildNodes[t].InnerXml);
                                    }
                                }

                            }
                        }
                        else
                        {
                            if (!elemList[0].ChildNodes[s].Name.Equals("#text") && 
                                //!elemList[0].ChildNodes[s].Name.Equals("Amounts") && 
                                //!elemList[0].ChildNodes[s].Name.Equals("Amount") && 
                                !elemList[0].ChildNodes[s].Name.Equals("Product"))
                            {
                                getAllObjectName.Add(elemList[0].ChildNodes[s].Name);
                                getAllObjectInnerXML.Add(elemList[0].ChildNodes[s].InnerXml);
                            }
                        }
                    }
                }
            }
            string prodName = string.Empty;
            string amnt = string.Empty;
            for (int u = 0; u < getAllObjectName.Count; u++)
            {
                if (getAllObjectName[u].Equals("Amounts"))
                {
                    // prodName = getAllObjectInnerXML[u].ToString();
                    amnt = getAllObjectInnerXML[u].ToString();
                }
                if (getAllObjectName[u].Equals("Name"))
                    {
                        prodName = getAllObjectInnerXML[u].ToString();
                    }
                if (getAllObjectName[u].Equals("Sku"))
                {
                    if (!(getAllObjectInnerXML[u].ToString().Equals("")))
                    {
                        if (!(amnt.Equals(string.Empty)))
                        {
                            compileData.Add(new
                            {
                                Name = prodName,
                                Sku = getAllObjectInnerXML[u],
                                Amount = amnt
                            });
                        }
                    }
                }
            }
            return compileData;
        }
    }
}

