﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Wallet.Data;
using Cooptavanza.Wallet.Repositories;
using Cooptavanza.Wallet.Repository.Repositories;

namespace Cooptavanza.Wallet.Models
{
    public class SiteModule : Ninject.Modules.NinjectModule
    {
       

        public override void Load()
        {
            //repositories
            Bind<CW_EntitiesAzure>().ToSelf().InThreadScope();
            Bind<IUsersRepository>().To<UsersRepository>();
            Bind<IStatesRepository>().To<StatesRepository>();
            Bind<IProductsRepository>().To<ProductsRepository>();
            Bind<IPartnersRepository>().To<PartnersRepository>();
            Bind<IAgentRepository>().To<AgentsRepository>();
            Bind<ICountryRepository>().To<CountryRepository>();
            Bind<ICurrencyRepository>().To<CurrencyRepository>();
            Bind<ICustomerRepository>().To<CustomerRepository>();
            Bind<IImageBillsRepository>().To<ImageBillRepository>();
            Bind<IImageProfileRepository>().To<ImageProfileRepository>();
            Bind<IMerchantRepository>().To<MerchantRepository>();
            Bind<IUsersTempRepository>().To<UsersTempRepository>();
            Bind<IUserAccountRepository>().To<UserAccountRepository>();
            Bind<ITransactionRepository>().To<TransactionRepository>();
            Bind<IOFAClistRepository>().To<OFACListRepository>();
            Bind<IDummyLoadRepository>().To<DummyLoadRepository>();
            Bind<ITransferMoneyRepository>().To<TransferMoneyRepository>();
            Bind<IImageIdCardRepository>().To<ImageIDCardRepository>();
            Bind<IBillerTypeRepository>().To<BillerTypeRepository>();
            Bind<IBillerRepository>().To<BillerRepository>();
            Bind<IEnrolledAccountRepository>().To<EnrolledAccountRepository>();
            Bind<IReceiversCustomerRepository>().To<ReceiversCustomerRepository>();
            Bind<IUserThemeRepository>().To<UserThemeRepository>();
            Bind<IServiceFeeRepository>().To<ServiceFeeRepository>();
            Bind<ITransactionFeeRepository>().To<TransactionFeeRepository>();
            Bind<IMoneyTransferTransactionsRepository>().To<MoneyTransferTransactionsRepository>();
            Bind<ITransactionLogsRepository>().To<TransactionLogRepository>();
            Bind<ICreditCardLogsRepository>().To<CreditCardLogsRepository>();
            Bind<IMoneySwapCustomerRepository>().To<MoneySwapCustomerRepository>();
            Bind<IMoneyTransferCustomerRepository>().To<MoneyTransferCustomerRepository>();
            Bind<IMEXAmlStatusRepository>().To<MEXAmlStatusRepository>();
            Bind<IMEXIncludeFeesRepository>().To<MEXIncludeFeesRepository>();
            Bind<IMEXKycCheckRepository>().To<MEXKycCheckRepository>();
            Bind<IMEXSndIdTypeRepository>().To<MEXSndIdTypeRepository>();
            Bind<IMEXSndRemitPurposeRepository>().To<MEXSndRemitPurposeRepository>();
  			Bind<IUserServicesRepository>().To<UserServicesRepository>();
            Bind<IMEXResponseRemitValidateRepository>().To<MEXResponseRemitValidateRepository>();
            Bind<IMEXRemitValidateRepository>().To<MEXRemitValidateRepository>();
            Bind<IOTPAccountRepository>().To<OTPAccountRepository>();
            Bind<IImageUserRepository>().To<ImageUserRepository>();
            Bind<ICoopEnrolledAccountRepository>().To<CoopEnrolledAccountRepository>();
            Bind<ICoopTransactionLogRepository>().To<CoopTransactionLogRepository>();
            Bind<IUserAccountActivitiesRepository>().To<UserAccountActivitiesRepository>();
            Bind<IBankDepositRepository>().To<BankDepositRepository>();
            Bind<IPartnerCustomerRepository>().To<PartnerCustomerRepository>();
            Bind<ITransactionTypeRepository>().To<TransactionTypeRepository>();
            Bind<IMEXResponseRemitErrorLogRepository>().To<MEXResponseRemitErrorLogRepository>();
            Bind<IBillerAccountRepository>().To<BillerAccountRepository>();
            Bind<IPayInPaymentRatesRepository>().To<PayInPaymentRatesRepository>();
            Bind<IPayOutPaymentRatesRepository>().To<PayOutPaymentRatesRepository>();
            Bind<ICommissionsRepository>().To<CommissionsRepository>();
            Bind<IPaymentBreakdownRepository>().To<PaymentBreakdownRepository>();
            Bind<IConvertedCurrencyRepository>().To<ConvertedCurrencyRepository>();
            Bind<ITaxCommissionRepository>().To<TaxCommissionRepository>();

            Bind<PaymentBreakdownService>().ToSelf(); 
            Bind<MEXResponseRemitErrorLogService>().ToSelf();
            Bind<TransactionTypeService>().ToSelf();
            Bind<PartnerCustomerService>().ToSelf();
            Bind<UserAccountActivitiesService>().ToSelf();
            Bind<ImageUserService>().ToSelf();
            Bind<OTPAccountService>().ToSelf();
            Bind<MEXRemitValidateService>().ToSelf();
            Bind<MEXResponseRemitValidateService>().ToSelf();
            Bind<MEXService>().ToSelf();
            Bind<MoneyTransferCustomerService>().ToSelf();
            Bind<MoneySwapCustomerService>().ToSelf();
            Bind<UserThemeService>().ToSelf();
            Bind<EnrolledAccountService>().ToSelf();
            Bind<BillerService>().ToSelf();
            Bind<BillerTypeService>().ToSelf();
            Bind<ImageIdCardService>().ToSelf();
            Bind<TransferMoneyService>().ToSelf();
            Bind<DummyLoadService>().ToSelf();
            Bind<TransactionService>().ToSelf();
            Bind<UserAccountService>().ToSelf();
            Bind<AgentsService>().ToSelf();
            Bind<CountryService>().ToSelf();
            Bind<CurrencyService>().ToSelf();
            Bind<CustomerService>().ToSelf();
            Bind<ImageBillService>().ToSelf();
            Bind<ImageProfileService>().ToSelf();
            Bind<MerchantService>().ToSelf();
            Bind<UsersService>().ToSelf();
            Bind<StatesService>().ToSelf();
            Bind<ProductsService>().ToSelf();
            Bind<PartnersService>().ToSelf();
            Bind<UsersTempService>().ToSelf();
            Bind<OFAClistService>().ToSelf();
            Bind<GateWayProcessResponseReader>().ToSelf();
            Bind<ReceiversCustomerService>().ToSelf();
            Bind<ServiceFeeService>().ToSelf();
            Bind<TransactionFeeService>().ToSelf();
            Bind<MoneyTransferTransactionsService>().ToSelf();
            Bind<TransactionLogService>().ToSelf();
            Bind<CreditCardLogService>().ToSelf();
            Bind<UserServicesService>().ToSelf();
            Bind<CoopTransactionLogService>().ToSelf();
            Bind<CoopEnrolledAccountRepository>().ToSelf();
            Bind<BankDepositRepository>().ToSelf();
            Bind<BillerAccountRepository>().ToSelf();
            Bind<PayInPaymentRatesService>().ToSelf();
            Bind<PayOutPaymentRatesService>().ToSelf();
            Bind<CommissionsService>().ToSelf();
            Bind<ConvertedCurrencyService>().ToSelf();
            Bind<TaxCommissionService>().ToSelf();
        }
    }
}
