﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet;

namespace Cooptavanza.Models.ViewModels
{
    public class UserAccountSearchViewModel
    {
        public int Count { get; set; }
        public List<MemberEx> Results { get; set; }
    }

    public class CoopTransactionLogListSearchViewModel
    {
        public int Count { get; set; }
        public List<CoopTransactionListEx> Results { get; set; }
    }
}