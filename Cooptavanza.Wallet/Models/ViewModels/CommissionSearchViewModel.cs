﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet;

namespace Cooptavanza.Models.ViewModels
{
    public class CommissionSearchViewModel
    {
        public int Count { get; set; }
        public List<CommissionsEx> Results { get; set; }
    }

    public class PaymentBreakdownSearchViewModel
    {
        public int Count { get; set; }
        public List<PaymentBreakdownEx> Results { get; set; }
    }
}