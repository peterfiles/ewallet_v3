﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet;

namespace Cooptavanza.Models.ViewModels
{
    public class PartnersSearchViewModel
    {
        public int Count { get; set; }
        public List<PartnersEx> Results { get; set; }
    }
}