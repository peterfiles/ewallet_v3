﻿CooptavanzaWalletDirective.directive('highlightonfocus', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on("focus", function () {
                $(this).val('');
            });
        }
    };
})
CooptavanzaWalletDirective.directive('inputcurrency', function () {
    return {
        require: 'ngModel',

        link: function (scope, element, attrs, ngModel) {

            ngModel.$formatters.unshift(
                function (value) {
                    if (!isNaN(value)) {
                        return Globalize.format(value, "n2");
                    } else {
                        return 0;
                    }
                }
            );
            ngModel.$parsers.push(
                function (value) {
                    return Globalize.parseFloat(value)
                }
            );
        }
    };
})
