﻿CooptavanzaWalletDirective.directive('sidebar', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            Layout.initSidebar();
        }
    };
})

CooptavanzaWalletDirective.directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                    scope.$apply(function(){
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });
                    //ng-enter=""
                    event.preventDefault();
                }
            });
        };
});


CooptavanzaWalletDirective.directive("fileinput", [function () {
    return {
        scope: {
            fileinput: "=",
            filepreview: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.fileinput = changeEvent.target.files[0];
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.filepreview = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(scope.fileinput);
            });
        }
    }
}]);