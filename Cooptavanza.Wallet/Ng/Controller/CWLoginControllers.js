﻿CooptavanzaWalletController.controller('AppLoginController', ['$scope', '$http', '$q', '$location', function ($scope, $http, $q, $location) {

    $scope.login = { Username: '', Password: '', RememberMe: false };
    $scope.message = '';
    $scope.validateCustomer = function () {
        $scope.message = '';
        $scope.session.loading = true;
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: '/login/login',
            data: $scope.login
        }).success(function (data) {
            $scope.session.loading = false;
            if (data.success) {
                $scope.session.isAuthenticated = true;
                $scope.session.customer = data.customer;
                $location.path("/");
            } else {
                $scope.message = data.message;
            }
            deferred.resolve('order saved');
        });
        return deferred.promise

    }
}]);
