﻿CooptavanzaWalletController.controller('CWCCreditCardController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'svcCommonServices', 'svcMoneyConversion', 'growl', '$uibModal', '$state', '$rootScope', 'svcDashboardCommon', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, svcCommonServices, svcMoneyConversion, growl, $uibModal, $state, $rootScope, svcDashboardCommon, alertify, SweetAlert, loader) {
/*Variables - Start*/
        $scope.creditcard = {};
        $scope.creditcard.checkDate = moment().format("DD MMMM YYYY");
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.ccData = {};
        $scope.disSub = true;
        $scope.errorMessage = "";
        $scope.defCurr;
        $scope.validString = true;
        $scope.accounts = {};
        /*Variables - End*/
/*Functions, Methods etc. - Start*/
        $scope.open1 = function () {
            $scope.popup1.opened = true;

        };

        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };

        $scope.loadState = function (countryid) {
            console.log(countryid);
            svcHome.searchStatedashboard(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };
        
        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });

        };

        $scope.selectedCurrency = function (currency) {
            $scope.creditcard.currencyid = currency.id;
            $scope.creditcard.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            console.log(country);
            $scope.creditcard['countryid'] = country.id;
            $scope.creditcard.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.creditcard.stateid = state.id;
            $scope.creditcard.state = state.name;
        };

        $scope.calculateFees = function () {
            var isValid = angular.equals($scope.ccData.currencyType, "USD") || angular.equals($scope.ccData.currencyType, "EUR") ? true : false;
            $scope.disSub = true;
            if (isValid) {
                //  setTimeout(function () {
                console.log("go here");
                svcCommonServices.initializeTaxTransactionCodeAndTransctionFee($scope.ccData.amount_purchase).then(function (result) {
                    $scope.ccData.tax = result.data.tax;
                    $scope.ccData.amount_tax = result.data.taxValue; // or tax value
                    $scope.ccData.item_no = result.data.transactionCode; // or transaction code
                    $scope.ccData.amount_shipping = result.data.transactionFee; //or transaction Fee
                    $scope.amount_plusFees = result.data.amountPlusFees;
                    //$scope.disSub = false;
                    $scope.disableSubmit();
                });
                //  }, 1000);
            }
        };
        
        //$scope.getConvertedValue = function () {
        //    var isValid = angular.equals($scope.ccData.currencyType, "USD") || angular.equals($scope.ccData.currencyType, "EUR") ? true : false;
        //    $scope.currSymbol = $scope.ccData.currencyType === "USD" ? "$" : "€";
        //    var len = $scope.ccData.amount_purchase;
        //    if (len.toString().length > 3) {
        //        //angular.element(document.getElementById("amnt"))[0].onkeypress = function () {
        //        //    return false;
        //        //}
        //        growl.success("3 digit amount is only available.");
        //        isValid = false;
        //        $scope.disSub = true;
        //        $scope.validString = false;
        //    } else {
        //        //angular.element(document.getElementById("amnt"))[0].onkeypress = function () {
        //        //    return true;
        //        //}
        //        $scope.disSub = false;
        //        isValid = true;
        //        $scope.validString = true;
        //    }
        //    if (isValid) {
        //        var extractEnrAccount = JSON.parse($scope.ccData.enrAccnt);
        //        var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
        //        //if (extractCustomerDetails.currency !== "USD" || extractCustomerDetails.currency !== "EUR") {
        //        var notDefaultCurr = angular.equals(extractEnrAccount.currency, $scope.ccData.currencyType) ? true : false;
        //        if (!notDefaultCurr) {
        //            svcMoneyConversion.convertCurrency(extractEnrAccount.currency, $scope.ccData.currencyType, $scope.ccData.amount_purchase).then(function (response) {
        //                //if (response.data.base !== "USD" || response.data.base !== "EUR") {
        //                $scope.ccData.usd_amount = angular.equals($scope.ccData.currencyType, "USD") ? response.data.rates.USD : response.data.rates.EUR;
        //                //} // amount is converted to us or eur

        //                $scope.calculateFees();
        //            });
        //        } else {
        //            $scope.ccData.usd_amount = $scope.ccData.amount_purchase;
        //            $scope.calculateFees();
        //        }
        //    } else {
        //        $scope.currSymbol = $scope.ccData.currencyType === "" ? "" : "";
        //        $scope.disSub = true;
        //    }
        //    $scope.disableSubmit();
        //};

        $scope.submit = function () {
            //$scope.openLoading("Processing Transaction");
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
            $scope.ccData.customerID = extractCustomerDetails.id;
            $scope.ccData.payby = $scope.payby;
            $scope.ccData.state = $scope.creditcard.state;
            $scope.disSub = true;
            //$scope.getConvertedValue();
            $scope.ccData.moduleCode = "ciCC";
            $scope.ccData.amount_purchase = $scope.amount_plusFees;
            svcCommonServices.processCreditCard($scope.ccData).then(function (response) {
                //$rootScope.close(); //closing the loading screen
                $scope.disSub = true;
                if (!response.data.IsAccepted) {
                    //growl.error(response.data.ErrorMessage);
                    //$scope.errorModal(response.data.ErrorMessage);
                    SweetAlert.swal("Error!", response.data.ErrorMessage, "error");
                    $scope.disSub = false;
                } else {
                    //growl.success("Successfully Processed!");
                    SweetAlert.swal("Success", "Successfully Processed!", "success");
                    //$scope.openTYModal("Successfully Processed!");
                    $scope.disSub = false;
                    $scope.ccData.card_no = '';
                    $scope.ccData.card_exp_month = '';
                    $scope.ccData.card_ccv = '';
                    $scope.ccData.amount_purchase = 0;
                    $scope.ccData.tax = 0;
                    $scope.ccData.amount_tax = 0; // or tax value
                    $scope.ccData.item_no = 0; // or transaction code
                    $scope.ccData.amount_shipping = 0; //or transaction Fee
                    $scope.ccData.card_name = '';
                    $scope.amount_plusFees = 0;
                    $scope.ccData.usd_amountTax = ''; // or tax value
                    $scope.ccData.usd_amountShipping = ''; //or transaction Fee
                    $scope.ccData.usd_amountFees = '';
                    $scope.ccData.usd_amount = '';
                    $scope.ccData.address = '';
                    $scope.ccData.suburb_city = '';
                    $scope.ccData.country = '';
                    $scope.ccData.state = '';
                    $scope.ccData.postcode = '';
                }
                if ($scope.ccData.amount_purchase === 0) {
                    $scope.disSub = true;
                }
            }, function () {
               //$rootScope.close();
               window.location.reload(true);
            });
        };
       
        $scope.disableSubmit = function () {
            $scope.disSub = false;
            var isValid = angular.equals($scope.ccData.currencyType, "USD") || angular.equals($scope.ccData.currencyType, "EUR") ? true : false;
            //var errMess = angular.element(document.getElementById("errorMessage"))[0].innerText;
            //console.log(angular.equals($scope.ccData.card_no, ""));
            //console.log(
            //    '1) ' + $scope.ccData.address + '\n' +
            //    '2) ' + angular.equals($scope.ccData.card_exp_month, "") + '\n' +
            //    '3) ' + angular.equals($scope.ccData.card_ccv, "") + '\n' +
            //    '4) ' + angular.equals($scope.ccData.address, "") + '\n' + 
            //    '5) ' + angular.equals($scope.ccData.suburb_city, "") + '\n' + 
            //    '6) ' + angular.equals($scope.ccData.country, "") + '\n' + 
            //    '7) ' + angular.equals($scope.ccData.state, "") + '\n' + 
            //    '8) ' + angular.equals($scope.ccData.postcode, "") + '\n' + 
            //    '9) ' + angular.equals($scope.ccData.amount_purchase, "") + '\n' +
            //    '10) ' + angular.equals($scope.ccData.isValid, "") + '\n');

            if (angular.equals($scope.ccData.card_no, "") || angular.equals($scope.ccData.card_no, undefined)) { $scope.disSub = true; console.log("1"); }
            else if (angular.equals($scope.ccData.card_exp_month, "") || angular.equals($scope.ccData.card_exp_month, undefined)) { $scope.disSub = true; console.log("2"); }
            else if (angular.equals($scope.ccData.card_name, "") || angular.equals($scope.ccData.card_name, undefined)) { $scope.disSub = true; console.log("3"); }
            else if (angular.equals($scope.ccData.card_ccv, "") || angular.equals($scope.ccData.card_ccv, undefined)) { $scope.disSub = true; console.log("4", $scope.ccData.card_ccv); }
            else if (angular.equals($scope.ccData.address, "") || angular.equals($scope.ccData.address, undefined)) { $scope.disSub = true; console.log("5"); }
            else if (angular.equals($scope.ccData.suburb_city, "") || angular.equals($scope.ccData.suburb_city, undefined)) { $scope.disSub = true; console.log("6"); }
            else if (angular.equals($scope.ccData.country, "") || angular.equals($scope.ccData.country, undefined)) { $scope.disSub = true; console.log("7"); }
            else if (angular.equals($scope.ccData.state, "") || angular.equals($scope.ccData.state, undefined)) { $scope.disSub = true; console.log("8"); }
            else if (angular.equals($scope.ccData.postcode, "") || angular.equals($scope.ccData.postcode, undefined)) { $scope.disSub = true; console.log("9"); }
            else if ($scope.ccData.amount_purchase === 0) { $scope.disSub = true; console.log("10"); }
            else if (!isValid) { $scope.disSub = true; }
            else if (!($scope.validString)) { $scope.disSub = true; }
           
        };

        $scope.initClientValidation = function () {
           // $scope.openLoading("Validating Details...");
            var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
            $scope.origCurr = extractCustomerDetails.currency;
            console.log(extractCustomerDetails.id);
            //$scope.errorMessage = "";
            //svcCommonServices.initClientValidation(extractCustomerDetails.id).then(function (result) {
                // console.log(result);
                //if (!result.data.IsValid) {
                //    angular.element(document.getElementById("errorMessage"))[0].innerText = result.data.ErrorMessage;
                //    $scope.errorMessage = result.data.ErrorMessage;
                //    //angular.element(document.getElementById("isDefaultAddress")).checked = false;
                //    $scope.ccData.isDefaultAddress = false;
                //    $scope.disFields = true;
                //} else {
                //    $scope.defCurr = extractCustomerDetails.currency;
                //    angular.element(document.getElementById("errorMessage"))[0].innerText = "";
                //    //angular.element(document.getElementById("isDefaultAddress")).checked = true;
                //    $scope.ccData.isDefaultAddress = true;
                //    $scope.errorMessage = "";
                //}
                //$scope.defaultAddress();
            // $rootScope.close();
            $scope.defCurr = extractCustomerDetails.currency;
            $scope.ccData.address = extractCustomerDetails.address;
            $scope.ccData.suburb_city = extractCustomerDetails.city;
            $scope.ccData.country = extractCustomerDetails.country;
            $scope.ccData.state = extractCustomerDetails.state;
            $scope.ccData.postcode = extractCustomerDetails.zipcode;
            $scope.ccData.card_name = extractCustomerDetails.firstname + " " + extractCustomerDetails.lastname;
            $scope.disableSubmit()
           // });
        };

        $scope.defaultAddress = function () {
            //console.log($scope.ccData.isDefaultAddress);
            if ($scope.ccData.isDefaultAddress) {
                document.getElementById("divNewAddress").style.display = "none";
            } else {
                document.getElementById("divNewAddress").style.display = "";
            }
        };

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: 'lg',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };
/*Functions, Methods etc. - End*/
        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () {
            $scope.disSub = true;
         //   console.log(angular.element(document.getElementById("errorMessage"))[0].innerText);
            $scope.initClientValidation();
        });
        
    }]);

/*Scrat*/
//$scope.calculateFeesUSD = function () {
//    //if (angular.equals($scope.ccData.currencyType, "") || angular.equals($scope.ccData.currencyType,"undefined")) {
//    //    console.log("cfu");
//    //    //   setTimeout(function () {
//    //        svcCommonServices.initializeTaxTransactionCodeAndTransctionFee($scope.ccData.usd_amount).then(function (result) {
//    //            $scope.ccData.usd_amountTax = result.data.taxValue; // or tax value
//    //            $scope.ccData.usd_amountShipping = result.data.transactionFee; //or transaction Fee
//    //            $scope.usd_amountFees = result.data.amountPlusFees;
//    //        });
//    // //   }, 1000);
//    //}
//    //$scope.disSub = false;
//    //$scope.disableSubmit();
//};
