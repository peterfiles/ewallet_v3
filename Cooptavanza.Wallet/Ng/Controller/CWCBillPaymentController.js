﻿CooptavanzaWalletController.controller('CWCBillPaymentController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'svcLunex', 'svcBillPayment', '$uibModal', '$rootScope', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, svcLunex, svcBillPayment, $uibModal, $rootScope, alertify, SweetAlert, loader) {

        $scope.billPayment = {};
        $scope.Skus = [];
        $scope.data = {};
        $scope.billLunex = {};
        $scope.billPayment.checkDate = moment().format("DD MMMM YYYY");
        $scope.billTypes = {};
        $scope.biller = {};
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcBillPayment.getBillerCountry().then(function (response) {
                $scope.countries = response.data;
                console.log($scope.countries);
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.billPayment.currencyid = currency.id;
            $scope.billPayment.currency = currency.iso;
            $scope.billPayment.currencyName = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.billPayment['countryid'] = country.id;
            $scope.billPayment.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.billPayment.stateid = state.id;
            $scope.billPayment.state = state.name;
        };

        $scope.getSku = function () {
            svcLunex.getSkuBillPayment().then(function (response) {
                $scope.Skus[0] = response.data.List;
            });
        }

        $scope.BillerTypeAll = function () {
            svcBillPayment.getBillerType().then(function (response) {
            });
        }

        $scope.selectedProduct = function () {
            $scope.billPayment.ProductSelected = JSON.parse($scope.billPayment.product);
            $scope.billPayment.ProductName = $scope.billPayment.ProductSelected.Name;
            if ($scope.billPayment.ProductSelected.Amounts.Amount.Denom === "OPEN"){
                $scope.billPayment.ProductDenom = $scope.billPayment.ProductSelected.Amounts.Amount.Denom
                $scope.billPayment.Range = $scope.billPayment.ProductSelected.Amounts.Amount.MinAmt + " to " + $scope.billPayment.ProductSelected.Amounts.Amount.MaxAmt + " " + $scope.billPayment.ProductSelected.Amounts.Amount.DestCurr;
            } 
        }

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.submit = function () {
            if ($scope.billPayment.company === "LUNEX") {
                //$scope.openLoading("Processing Transaction.");
                SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
                $scope.data.customer = $scope.customer;
                if ($scope.billPayment.ProductSelected !== undefined) {
                    $scope.billLunex['Sku'] = $scope.billPayment.ProductSelected.Sku;
                    $scope.billLunex['Phone'] = ($scope.customer.mobilenumber).replace("+", "");
                    $scope.billLunex['Amount'] = $scope.billPayment.amount;
                    $scope.billLunex['CustomerId'] = $scope.customer.id;
                    $scope.billLunex['ProductName'] = $scope.billPayment.ProductName;
                    $scope.billLunex['currencyfromiso'] = $scope.billPayment.currencyName;
                    $scope.data.billpayment = $scope.billLunex;
                    svcLunex.submitBillPayment($scope.billLunex).then(function (response) {
                        //$rootScope.close()
                        //$scope.openTYModal("Thank You for giving us the opportunity to serve you.");
                        SweetAlert.swal("Success", "Thank You for giving us the opportunity to serve you.", "success");
                    });
                } else {
                    SweetAlert.swal("Error", "There was an error. Please try again.", "error");
                }
            }
        }

        $scope.queryBillerType = function () {
            svcBillPayment.getBillerTypeByCountry($scope.billPayment.country).then(function (res) {
                $scope.billTypes = res.data;
            });
        }

        $scope.queryBiller = function () {
            svcBillPayment.getBillerByCountryAndType($scope.billPayment.country, $scope.billPayment.billerType).then(function (res) {
                $scope.biller = res.data;
                console.log($scope.biller);
            });
        }

        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency(), $scope.getSku(), $scope.BillerTypeAll()]).then(function () { })
}])