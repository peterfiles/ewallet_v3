﻿CooptavanzaWalletController.controller('CWCGlobalController', ['$scope', '$http', '$q', '$location', '$uibModal', 'data', '$state', '$rootScope', '$uibModalInstance',
    function ($scope, $http, $q, $location, $uibModal, data, $state, $rootScope, $uibModalInstance) {

        $scope.contentMessage = data.message;
        $scope.isloading = data.isloading;
        $scope.link = data.link;
        $scope.load1 = true;
        $scope.load2 = false;

        $scope.dismiss = function () {
            $uibModalInstance.close();
            if ($scope.link !== null) {
                window.location = $scope.link;
            }
            else if (!($scope.isloading)) {
                window.location.reload(true);
            }
        };

        $uibModalInstance.result.then(function () {
            if ($scope.link !== null) {
                window.location = $scope.link;
            }
            else if (!($scope.isloading)){
                window.location.reload(true);
            }
        }, function (response) {
            if ($scope.link !== null) {
                window.location = $scope.link;
            }
            else if (!($scope.isloading)) {
                window.location.reload(true);
            }
        });
        
        $q.all([]).then(function () {   });
    }
]);