﻿CooptavanzaWalletController.controller('CWCETransferController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcCommonServices', 'svcMoneyConversion', 'svcMinistatement', 'svcSendMoney', 'growl', '$uibModal', '$rootScope', 'svcDashboardCommon', 'config', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcCommonServices, svcMoneyConversion, svcMinistatement, svcSendMoney, growl, $uibModal, $rootScope, svcDashboardCommon, config, alertify, SweetAlert, loader) {//, 'ngAlertify'
        //alertify.confirm("Message", function () {
        //    // user clicked "ok"
        //    //alertify.success("You've clicked OK and typed: ");
        //    //alertify.successPosition("top left");
        //    alertify.logPosition("bottom right");
        //    alertify.success("bottom right");
        //}, function () {
        //    // user clicked "cancel"
        //    alertify.error("You've clicked Cancel");
        //    });


        // SweetAlert.swal('Error!', 'Please try again, account not found.', 'error');
        var SendMoney = $stateParams.SendMoney; //getting SendMoneyVal
        $scope.lockBtn = true;
        $scope.authenticated = false;
        $scope.startNotif = true;
        $scope.notifyZeroBalance = false;
        $scope.notifyExceededLimit = false;

        var data;
        $scope.etransfer = {};
        $scope.etransfer.transactionid = "";
        $scope.etransfer.benificiarymobileno = "";
        $scope.etransfer.amountfrom = 0;
        $scope.searchText = "";
        $scope.pageNo = 1;
        $scope.pageSize = 99999;
        $scope.balancelimit = 0;

        $scope.currencyName;
        $scope.convertedValue;
        $scope.getConvertedBalance;

        $scope.SendEmailMoney = {};
        $scope.message = '';
        $scope.result = "color-default";
        $scope.isViewLoading = false;

        $scope.currencyData;
        $scope.getConvertedForBenificiary;
        $scope.availableCurrencies;
        $scope.currencyValue = 0.00;
        $scope.formats = ['dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        //get called when user submits the form  
        $scope.submitForm = function () {
            $scope.lockBtn = true;
            $scope.isViewLoading = true;
            //growl.success("Please Wait Your transaction is being processed");
            //$scope.openLoading("Processing Transaction.");
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            //$scope.etransfer.currencyfromiso = $scope.customer.currency;
            // $scope.etransfer.currencytoiso = $scope.currency;
            $scope.etransfer.customerid = $scope.customer.id;
            $scope.etransfer.amount = $scope.etransfer.amountto;
            $scope.etransfer.datecreated = moment();

            svcSendMoney.SendMoney($scope.etransfer).then(function (response) {
                //$rootScope.close();
                if (response.data === "success") {
                    $scope.etransfer = null;
                    //growl.success("Your transaction have been sent.");
                    //$scope.openTYModal("Thank You for giving us the opportunity to serve you.");
                    $scope.lockBtn = false;
                    $scope.load();
                    SweetAlert.swal("Success", "Successfully Processed!", "success");
                    $scope.etransfer = {};
                } else {
                    $scope.lockBtn = false;
                    //growl.warning("Transaction failed.");
                    //$scope.errorModal("Transaction failed.");
                    SweetAlert.swal("Error", "Transaction Failed!", "error");
                }
            }, function (err) {
                //$rootScope.close();
                $scope.lockBtn = false;
                //growl.warning("Transaction failed. Please Try again.");
                //$scope.errorModal("Transaction failed. Please Try again.");
                SweetAlert.swal("Error", "Transaction Failed!", "error");
            });
            $scope.isViewLoading = false;
        };

        $scope.loadDefaultValues = function () {
            svcCommonServices.LoadDefaultValues().then(function (result) {
                $scope.currencyData = result.data;
            });
        };

        $scope.changeTT = function () {
            if ($scope.etransfer.transactionid === 0) { $scope.lockBtn = true; }
            else { $scope.lockBtn = false; }
        };

        $scope.setCurrency = function () {
            for (x = 0; x < $scope.currencyData.length; x++) {
                if ($scope.setCurrencyForConversion === $scope.currencyData[x].id) {
                    $scope.currencyName = $scope.currencyData[x].iso;
                    svcMoneyConversion.convertCurrency($scope.etransfer.currency, $scope.currencyName, 1).then(function (result) {
                        $scope.convertedValue = result.data.rates;
                        $scope.getConvertedBalance = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);
                    });
                    break;
                }
            }
        };

        $scope.setCurrencyForBenificiary = function () {
            // $scope.etransfer.currencyfromiso = $scope.customer.currency;
            if ($scope.etransfer.amountfrom > $scope.balancelimit) {
                $scope.notifyExceededLimit = true;
            }
            else {
                $scope.notifyExceededLimit = false;
                if ($scope.etransfer.amountfrom > 0 && $scope.etransfer.currencytoiso !== undefined && $scope.etransfer.currencyfromiso !== undefined) {
                    svcMoneyConversion.convertCurrency($scope.etransfer.currencyfromiso, $scope.etransfer.currencytoiso, $scope.etransfer.amountfrom).then(function (result) {
                        $scope.convertedValue = result.data.rates;
                        $scope.getConvertedForBenificiary = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);

                        if ($scope.etransfer.currencyfromiso === $scope.etransfer.currencytoiso) {
                            $scope.etransfer.amountto = $scope.etransfer.amountfrom;

                        } else {
                            $scope.etransfer.amountto = result.data.rates[$scope.etransfer.currencytoiso];
                        }
                    });
                }
            }
        };

        $scope.loadAllCurrencies = function () {
            svcMoneyConversion.getAllAvailableCurrency().then(function (result) {
                $scope.availableCurrencies = result.data.rates;
                $scope.conversionRateFrom = $scope.customer.currency;
            });
        };

        $scope.convertCurrency = function (from, to) {
            if (from === undefined) {
                from = $scope.customer.currency;
            }
            svcMoneyConversion.convertCurrency(from, to, 1).then(function (result) {
                $scope.currencyValue = result.data.rates;
            });
        };
        //$getConvertedBalance;
        $scope.threeDecimal = function (num) {
            //return parseFloat(num).toFixed(3);
            return num;
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };

        $scope.selectedCountry = function (country) {
            $scope.register['countryid'] = country.id;
            $scope.register.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.register.stateid = state.id;
            $scope.register.state = state.name;
            $scope.loadState(state.id);
        };

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.load = function () {
            if ($scope.customer) {
                svcDashboardCommon.getEnrolledListByUserId($scope.customer.id).then(function (res) {
                    $scope.customer.EnrolledList = res.data;
                    svcDashboardCommon.getBalance($scope.customer.id).then(function (resp) {
                        $scope.enrolledBalance = resp.data;
                        angular.forEach($scope.customer.EnrolledList, function (each, index) {
                            each['balance'] = 0;
                            angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                                if (eachEB.currency === each.currency) {
                                    each.balance = eachEB.balance;
                                }
                            });
                        });
                    });
                });
            }
        }

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.change = function () {
            var change = document.getElementById("mobilenumber").value;
            if (document.getElementById("mobilenumber").value[0] && document.getElementById("mobilenumber").value[0] !== "+") {
                document.getElementById("mobilenumber").value = "+" + change;
            }
        };

        $scope.selectCurrencyFrom = function (currency) {
            var found = false;
            angular.forEach($scope.customer.EnrolledList, function (each, index) {
                if (each.currency === currency) {
                    if (each.balance <= 0) {
                        $scope.lockBtn = true;
                        $scope.balancelimit = 0;
                        $scope.notifyZeroBalance = true;
                        $scope.startNotif = false;
                        $scope.etransfer.amountfrom = 0;
                        $scope.etransfer.amountto = 0;

                    } else {
                        $scope.lockBtn = false;
                        $scope.notifyZeroBalance = false;
                        $scope.startNotif = false;
                        $scope.balancelimit = each.balance;
                        found = true;

                    }
                } else if (each.currency !== currency && found === false) {
                    $scope.lockBtn = true;
                    $scope.notifyZeroBalance = true;
                    $scope.startNotif = false;
                    $scope.balancelimit = 0;
                }
            });
        };

        $scope.checkMobile = function (mobile) {
            if (mobile !== '' && mobile !== undefined) {
                var message = "";
                var id = config.id;
                var shaObj = new jsSHA("SHA-256", "TEXT");
                shaObj.setHMACKey(config.key, "TEXT");
                message = id + mobile;
                shaObj.update(message.toUpperCase());
                var hmac = shaObj.getHMAC("HEX");
                console.log(hmac);
                console.log(config);
                var data = {
                    signature: hmac,
                    param1: mobile,
                    idCode: id
                }
                svcCommonServices.getInfoByMobileNumber(data).then(function (res) {
                    $scope.openCheckEmailModal(res.data, "dd", mobile);
                });
            }

        }

        $scope.checkEmail = function (email) {
            if (email !== undefined && email !== '') {
                var message = "";
                var id = config.id;
                var shaObj = new jsSHA("SHA-256", "TEXT");
                shaObj.setHMACKey(config.key, "TEXT");
                message = id + email;
                shaObj.update(message.toUpperCase());
                var hmac = shaObj.getHMAC("HEX");
                console.log(hmac);
                console.log(config);
                var data = {
                    signature: hmac,
                    param1: email,
                    idCode: id
                }

                svcCommonServices.getInfoByEmail(data).then(function (res) {
                    $scope.openCheckEmailModal(res.data, "dd", email);
                });
            }
        }

        $scope.openCheckEmailModal = function (data,message, param) {
            //$rootScope = $uibModal.open({
            //    templateUrl: '/ng/view/common/modal/confirmEtransfer.html',
            //    controller: 'CWCConfirmationCommonController',
            //    size: 'md',
            //    resolve: {
            //        data: function () {
            //            return $scope.contentMessage = { message: message, isloading: false, data:data, param:param };
            //        }
            //    }
            //});
            //data.firstname / lastname
            //data.email
            //data.mobilenumber
            $scope.showMessage(data,param);
        };

        $scope.openCheckMobileModal = function (data,message,param) {
            //$rootScope = $uibModal.open({
            //    templateUrl: '/ng/view/common/modal/confirmEtransfer.html',
            //    controller: 'CWCConfirmationCommonController',
            //    size: 'md',
            //    resolve: {
            //        data: function () {
            //            return $scope.contentMessage = { message: message, isloading: false, data: data, param: param };
            //        }
            //    }
            //});
            $scope.showMessage(data, param);
        };

        $scope.showMessage = function (data, param) {
            if (data.code === "RTS") {
                SweetAlert.swal({
                    html: true,
                    title: 'Validate',
                    text: '<b>Please check your beneficiary email / mobile number if it is correct</b>'
                    + '<table align="center">'
                    + '<tr>'
                    + '<td><h4>Fullname: ' + data.data.firstname + " " + data.data.lastname + '</h4></td>'
                    + '</tr>'
                    + '<tr>'
                    + '<td><h4>Email: ' + data.data.email + '</h4></td>'
                    + '</tr>'
                    + '<tr>'
                    + '<td><h4>Mobile Number: ' + data.data.mobilenumber + '</h4></td>'
                    + '</tr>'
                    + '</table>'
                });
            } else {
                SweetAlert.swal('Validate', 'You are about to transfer funds to non E-wallet member with ' + param, 'info');
            }
        }

        $q.all([$scope.load(), $scope.loadAllCurrencies(), $scope.loadDefaultValues()]).then(function () {
            
            
        });

    }]);

