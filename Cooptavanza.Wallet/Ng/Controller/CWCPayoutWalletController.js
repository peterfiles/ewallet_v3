﻿CooptavanzaWalletController.controller('CWCPayoutWalletController', ['$scope', '$http', '$q', '$location', '$stateParams','svcMinistatement',
    function ($scope, $http, $q, $location, $stateParams, svcMinistatement) {

    $scope.authenticated = false;
    var data;
    $scope.searchText = "";
    $scope.pageNo = 1;
    $scope.pageSize = 99999;

    if (sessionStorage.getItem('cr')) {
        $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
        if ($scope.customer !== null) {
            $scope.authenticated = true;
            $location.path("/dashboard");
        }

    }
    else { $scope.authenticated = false; }
    $scope.logout = function () {
        sessionStorage.removeItem('cr');
        $location.path("/home");
    }

    $scope.load = function () {
        if ($scope.customer) {
            svcMinistatement.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
                .then(function (response) {
                    $scope.statements = response.data.Results;
                    $scope.totalbalance = 0;
                    angular.forEach($scope.statements, function (each) {
                        if (each.credit) {
                            each['withdrawals'] = each.balance;
                            each['deposits'] = 0;
                            $scope.totalbalance = $scope.totalbalance - each.balance;
                        } else {
                            each['deposits'] = each.balance;
                            each['withdrawals'] = 0;
                            $scope.totalbalance = $scope.totalbalance + each.balance;
                        }

                    });
                });
        }
    }

    $q.all([$scope.load()]).then(function () { });
}])