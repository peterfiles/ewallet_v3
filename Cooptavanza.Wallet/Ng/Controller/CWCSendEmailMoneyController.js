﻿CooptavanzaWalletController.controller('CWCSendEmailMoneyController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcCommonServices', 'svcMoneyConversion', 'svcMinistatement', 'svcSendMoney',
    function ($scope, $http, $q, $location, $stateParams, svcCommonServices, svcMoneyConversion, svcMinistatement, svcSendMoney) {

    var SendMoney = $stateParams.SendMoney; //getting SendMoneyVal

    $scope.authenticated = false;
    var data;
    $scope.searchText = "";
    $scope.pageNo = 1;
    $scope.pageSize = 99999;

    $scope.currencyName;
    $scope.convertedValue;
    $scope.getConvertedBalance;

    $scope.SendEmailMoney = {};
    $scope.message = '';
    $scope.result = "color-default";
    $scope.isViewLoading = false;
    //get called when user submits the form  
    $scope.submitForm = function () {
        $scope.isViewLoading = true;
        console.log('Form is submitted with:', $scope.SendEmailMoney);
        //$http service that send or receive data from the remote server
        svcSendMoney.SendEmailMoney().then(function (response) {
            $scope.errors = [];
            if (response.status == 200) {
                $scope.SendEmailMoney = {};
                $scope.message = 'Send Email Money Submitted!';
                $scope.result = "color-green";
             
                console.log(response);
            }
            
        });
        $scope.isViewLoading = false;
    }
    $scope.currencyData;
    $scope.emailMoneyData = function () {
        svcCommonServices.getById(9).then(function (result) {
            $scope.emailMoneyData = result.data;
            console.log($scope.emailMoneyData);
        });
    }
    $scope.loadDefaultValues = function () {
        svcCommonServices.LoadDefaultValues().then(function (result) {
            $scope.currencyData = result.data;
        });
    };

    $scope.changeTT = function () {
        console.log($scope.transferType);
    }
  

 
    $scope.setCurrency = function () {
        for (x = 0; x < $scope.currencyData.length; x++) {
            if ($scope.setCurrencyForConversion === $scope.currencyData[x].id) {
                $scope.currencyName = $scope.currencyData[x].iso;
                svcMoneyConversion.convertCurrency($scope.emailMoneyData.currency, $scope.currencyName,1).then(function (result) {
                    $scope.convertedValue = result.data.rates;
                    $scope.getConvertedBalance = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);
                    console.log($scope.getConvertedBalance);
                });
                break;
            }
        }
        //console.log("currencyID = " + $scope.currencyID)
        //console.log($scope.currencyData);
    };
    $scope.getConvertedForBenificiary;
    $scope.setCurrencyForBenificiary = function () {
        svcMoneyConversion.convertCurrency($scope.emailMoneyData.currency, $scope.currencyName, $scope.benificiaryAmount).then(function (result) {
            $scope.convertedValue = result.data.rates;
            $scope.getConvertedForBenificiary = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);
            console.log($scope.getConvertedForBenificiary);
        });
        //console.log("currencyID = " + $scope.currencyID)
        //console.log($scope.currencyData);
    };


    $scope.availableCurrencies;
    $scope.loadAllCurrencies = function () {
        svcMoneyConversion.getAllAvailableCurrency().then(function (result){
            $scope.availableCurrencies = result.data.rates;
            console.log(result);
        });
    };
    $scope.currencyValue = 0.00;
    $scope.convertCurrency = function (from, to) {
        svcMoneyConversion.convertCurrency(from, to,1).then(function (result) {
            $scope.currencyValue = result.data.rates;
        });     
    };
    //$getConvertedBalance;
    $scope.threeDecimal = function (num) {
        //return parseFloat(num).toFixed(3);
        return num;
    };



    if (sessionStorage.getItem('cr')) {
        $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
        if ($scope.customer !== null) {
            $scope.authenticated = true;
            $location.path("/dashboard");
        }


    }
    else { $scope.authenticated = false; }
    $scope.logout = function () {
        sessionStorage.removeItem('cr');
        //location.reload();
        $location.path("/home");
    }




    $scope.load = function () {

        if ($scope.customer) {
            svcMinistatement.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
                .then(function (response) {
                    $scope.statements = response.data.Results;
                    $scope.totalbalance = 0;
                    angular.forEach($scope.statements, function (each) {
                        if (each.credit) {
                            each['withdrawals'] = each.balance;
                            each['deposits'] = 0;
                            $scope.totalbalance = $scope.totalbalance - each.balance;
                        } else {
                            each['deposits'] = each.balance;
                            each['withdrawals'] = 0;
                            $scope.totalbalance = $scope.totalbalance + each.balance;
                        }

                    });
                });
        }
    }
   
    
    // $scope.loadDefaultValues();
    $q.all([$scope.load(), $scope.loadAllCurrencies(),$scope.emailMoneyData(), $scope.loadDefaultValues()]).then(function () {
        console.log("all data loaded succesfully");
        console.log($scope.emailMoneyData);
    });


}])