﻿CooptavanzaWalletController.controller('CWCMinistatementController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcMinistatement', 'NgTableParams', function ($scope, $http, $q, $location, $stateParams, svcMinistatement, NgTableParams) {

    $scope.authenticated = false;
    var dataLoad;
    $scope.searchText = "";
    $scope.pageNo = 1;
    $scope.pageSize = 10;
    
    $scope.loadmini = function () {
     
        if ($scope.customer) {
            svcMinistatement.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
                .then(function (response) {
                    $scope.statements = response.data.Results;
                    $scope.totalItems = response.data.Count;
                    $scope.totalbalance = 0;
                    angular.forEach($scope.statements, function (each) {
                        if (each.credit) {
                            each['withdrawals'] = 0;
                            each['deposits'] = each.balance;
                            $scope.totalbalance = $scope.totalbalance + each.balance;
                        } else {
                            each['deposits'] = 0;
                            each['withdrawals'] = each.balance;
                            $scope.totalbalance = $scope.totalbalance - each.balance;
                        }
                    });
            });
        }
    }
    $scope.pageChanged = function () {
        $scope.loadmini();
    }
    $q.all([$scope.loadmini()]).then(function () { });
}])