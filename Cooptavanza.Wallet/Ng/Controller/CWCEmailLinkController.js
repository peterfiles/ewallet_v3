﻿
CooptavanzaWalletController.controller('CWCEmailLinkController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcCommonServices', 'svcMoneyConversion', 'svcEmailLink', 'growl', '$uibModal', '$rootScope','alertify','SweetAlert','loader',
    function ($scope, $http, $q, $location, $stateParams, svcCommonServices, svcMoneyConversion, svcEmailLink, growl, $uibModal, $rootScope, alertify, SweetAlert, loader) {

        $scope.myAnswer = {};
        $scope.el = {};

        if ($stateParams.tk && $stateParams.id) {
            SweetAlert.swal({ html: true, title: 'Fetching Data...', text: loader.l3, showConfirmButton: false });
            svcEmailLink.getTF($stateParams.tk, $stateParams.id, $stateParams.p).then(function (response) {
                if(response.data != null){
                $scope.el = response.data;
                $scope.el.transactionid = $stateParams.id;
                }
                else {
                    $uibModal.open({
                        templateUrl: '/ng/view/common/modal/error.html',
                        controller: 'CWCGlobalController',
                        backdrop: 'static',
                        size: 'md',
                        keyboard: false,
                        resolve: {
                            data: function () {
                                return $scope.contentMessage = { message: "Transfer is already claimed!", isloading: true, link:"~/../../signin" };
                            }
                        }
                    });
                }
                SweetAlert.swal("Done!","Loaded data...","info");
            });
        }

        $scope.submit = function () {
            //$scope.openLoading("Loading...");
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            $scope.el.answer = $scope.answer;
            svcEmailLink.answer($scope.el).then(function (response) {
                //$rootScope.close();
                if (response.data === 'success') {
                    $scope.e = {};
                    $scope.e.email = $scope.el.benificiaryemail;
                    svcEmailLink.getCustomerByEmail($scope.e).then(function (res) {
                        if(res !== null){
                            //$scope.openTYModal("Success");
                            window.location = "~/../../signin";
                            SweetAlert.swal("Success","Success","success");
                        }
                        else {
                            window.location = "/registration#!/" + response.data + "&" + $stateParams.tk + "&" + $stateParams.id;
                        }
                    });
                } else if (response.data === 'failed') {
                    //$scope.errorModal("Please enter a valid answer");
                    SweetAlert.swal("Error!", "Please enter a valid answer.", "error");
                } else {
                    window.location = "/registration#!/" + response.data + "&" + $stateParams.tk + "&" + $stateParams.id;
                }
            });
        }

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };
        

    }])
