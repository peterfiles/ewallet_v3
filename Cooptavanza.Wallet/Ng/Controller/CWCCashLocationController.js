﻿CooptavanzaWalletController.controller('CWCCashLocationController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, alertify, SweetAlert, loader) {

        $scope.cashLocation = {};
        $scope.cashLocation.abaRouting = "";
        $scope.cashLocation.acctNum = "";
        $scope.cashLocation.checkNum = "";
        $scope.cashLocation.bankName = "";
        $scope.cashLocation.checkDate = moment().format("DD MMMM YYYY");
        $scope.cashLocation.mobilenumber = "";
        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.change = function () {
            var change = document.getElementById("mobilenumber").value;
            if (document.getElementById("mobilenumber").value[0] && document.getElementById("mobilenumber").value[0] != "+") {
                document.getElementById("mobilenumber").value = "+" + change;
            }
        }

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.cashLocation.currencyid = currency.id;
            $scope.cashLocation.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.cashLocation['countryid'] = country.id;
            $scope.cashLocation.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.cashLocation.stateid = state.id;
            $scope.cashLocation.state = state.name;
        };

        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { })
}])