﻿CooptavanzaWalletController.controller('CWCCoopMemberController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'svcCommonServices', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, svcCommonServices, alertify, SweetAlert, loader) {
        
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2120, 5, 22),
        minDate: new Date(1800, 1, 1),
        startingDay: 0
    };


    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.loadState = function (countryid) {
        svcHome.searchState(countryid).then(function (response) {
            $scope.coopmember.states = response.data;
        });
    };

    $scope.searchAllCountry = function () {
        svcHome.searchAllCountry("").then(function (response) {
            $scope.countries = response.data;
        });
    };

    $scope.searchDefaultCurrency = function () {
        svcHome.searchDefaultCurrency().then(function (response) {
            $scope.currencies = response.data;
        });
    };

    $scope.selectedCurrency = function (currency) {
        $scope.coopmember.currencyid = currency.id;
        $scope.coopmember.currency = currency.iso;
    };

    $scope.selectedCountry = function (country) {
        $scope.coopmember['countryid'] = country.id;
        $scope.coopmember.country = country.name;
        $scope.loadState(country.id);
    };

    $scope.selectedState = function (state) {
        $scope.coopmember.stateid = state.id;
        $scope.coopmember.state = state.name;
    };

    $scope.getPartner = function () {
        svcCommonServices.getPartners().then(function (response) {
            $scope.partners = response.data.Results;

        })
    };

    $scope.selectedPartner = function () {
        console.log($scope.coopmember.partner);
    }

    $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency(), $scope.getPartner()]).then(function () { })
    
}])