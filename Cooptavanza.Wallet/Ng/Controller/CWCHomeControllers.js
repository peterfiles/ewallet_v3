﻿CooptavanzaWalletController.controller('CWHomeController', ['$scope', '$http', '$q', '$location', 'svcHome','svcDashboardCommon', 'svcMinistatement', '$rootScope', '$uibModal', 'growl',
    function ($scope, $http, $q, $location, svcHome, svcMinistatement, $rootScope, $uibModal, growl, svcDashboardCommon) {

    $scope.authenticated = false;
    var data;
    $scope.searchText = "";
    $scope.pageNo = 1;
    $scope.pageSize = 99999;

    $scope.theme = function (theme) {
      //  document.cookie = "skin_color=" + theme + "; expires=Session; path=/Dashboard;";
        $scope.usertheme = {
            'theme' :theme, 'roles':7, 'id':$scope.customer.themeid, 'userid':$scope.customer.id
        };

        svcHome.saveTheme($scope.usertheme).then(function (response) {
            $scope.customer.themeid = response.data.id;
            $scope.customer.theme = $scope.usertheme.theme;
            sessionStorage.setItem('cr', JSON.stringify($scope.customer));
        });
       
    }

    $scope.logout = function () {
        sessionStorage.removeItem('cr');
        document.cookie = "skin_color=; expires=Thu, 01 Jan 1970 00:00:01 GMT;  path=/Dashboard;";
        window.location = "~/../../Signin";
    }

    $scope.calculate = function (statement) {
        $scope.statements = statement;
        angular.forEach($scope.customer.EnrolledList, function (each, index) {
            each['balance'] = 0;
            angular.forEach($scope.statements, function (eachstatement, index) {
                if (eachstatement.credit) {
                    if (each.currency === eachstatement.currencytoiso) {
                        each.balance = each.balance + eachstatement.balance;
                    }
                } else {
                    if (each.currency === eachstatement.currencyfromiso) {
                        each.balance = each.balance - eachstatement.balance;
                    }
                }
            });
            
        });
    }

    $scope.loadCustomer = function () {
        if ($scope.customer) {
            svcMinistatement.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
                .then(function (response) {
                    $scope.statements = response.data.Results;
                    $scope.totalbalance = 0;
                    angular.forEach($scope.statements, function (each) {
                        if (each.credit) {
                            each['withdrawals'] = 0;
                            each['deposits'] = each.balance;
                            $scope.totalbalance = $scope.totalbalance + each.balance;
                        } else {
                            each['deposits'] = 0;
                            each['withdrawals'] = each.balance;
                            $scope.totalbalance = $scope.totalbalance - each.balance;
                        }
                    });
                    $scope.calculate($scope.statements);
                });
        }
    }

    $scope.load = function () {
        if (sessionStorage.getItem('cr')) {
            $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
            svcHome.getThemeByUserId($scope.customer.id).then(function (res) {

                if (res.data !== null) {
                    $scope.customer.themeid = res.data.id;
                    $scope.customer.theme = res.data.theme;
                    document.cookie = "skin_color=" + res.data.theme + "; expires=Session; path=/Dashboard;";    
                }
            })
           
            svcHome.getProfileByUserId($scope.customer.id).then(function (response) {
                if (response.data !== null) {
                    $scope.customer.ImageProfile = response.data;
                } else { $scope.customer.ImageProfile = { 'id': 0, 'path': null, 'imageString': null };}
               
            });
            if ($scope.customer.ImageProfile) {
            }
            else { $scope.customer.ImageProfile = { 'id': 0, 'path':null, 'imageString': null }; }
                if ($scope.customer.ImageIdCard) {
                }
            else { $scope.customer.ImageIdCard = { 'id': 0, 'path':null,'imageString': null }; }
                if ($scope.customer.ImageBill) {
                }
            else { $scope.customer.ImageBill = { 'id': 0, 'path':null, 'imageString': null }; }
                
            if ($scope.customer !== null) {
                $scope.authenticated = true;
            }
        }
        else {
            $scope.authenticated = false;
            window.location = "~/../../signin/";
        }
        sessionStorage.setItem('cr', JSON.stringify($scope.customer));
    }
    $q.all([$scope.load()]).then(function () { });
   
 }])



CooptavanzaWalletController.controller('CWDashboardController', ['$scope', '$http', '$q', '$location', 'svcHome', 'svcMinistatement', 'svcDashboardCommon',
    function ($scope, $http, $q, $location, svcHome, svcMinistatement, svcDashboardCommon) {
    $scope.authenticated = false;
    var data;
    $scope.searchText = "";
    $scope.pageNo = 1;
    $scope.pageSize = 10;
    

    $scope.loadcustomer = function () {
        if (sessionStorage.getItem('cr')) {
            $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
            svcDashboardCommon.GetCustomerById($scope.customer.id).then(function (response) {
                $scope.customer = response.data;
                if ($scope.customer.ImageProfile) {
                }
                else { $scope.customer.ImageProfile = { 'id': 0, 'path':null, 'imageString': null }; }
                if ($scope.customer.ImageIdCard) {
                }
                else { $scope.customer.ImageIdCard = { 'id': 0,'path':null, 'imageString': null }; }
                if ($scope.customer.ImageBill) {
                }
                else { $scope.customer.ImageBill = { 'id': 0, 'path':null,'imageString': null }; }
                data = $scope.customer;
                sessionStorage.setItem('cr', JSON.stringify($scope.customer));
                if ($scope.customer.theme !== null) {
                    document.cookie = "skin_color=" + $scope.customer.theme + "; expires=Session; path=/Dashboard;";
                }
                $scope.load();
            });
        }
    }

    $scope.calculate = function (statement) {
        $scope.statements = statement;
        svcDashboardCommon.getBalance($scope.customer.id).then(function (resp) {
            $scope.enrolledBalance = resp.data;
            angular.forEach($scope.customer.EnrolledList, function (each, index) {
                each['balance'] = 0;
                angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                    if (eachEB.currency === each.currency) {
                        each.balance = eachEB.balance;
                    }
                });
            });
        });
        
    }
    $scope.pageChanged = function () {
        $scope.load();
    }

    $scope.load = function () {
        if ($scope.customer) {
            svcMinistatement.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
                .then(function (response) {
                    $scope.statements = response.data.Results;
                    $scope.totalItems = response.data.Count;
                    $scope.totalbalance = 0;

                    angular.forEach($scope.statements, function (each) {
                        if (each.credit) {
                            each['withdrawals'] = 0;
                            each['deposits'] = each.balance;
                            $scope.totalbalance = $scope.totalbalance + each.balance;
                        } else {
                            each['deposits'] = 0;
                            each['withdrawals'] = each.balance;
                            $scope.totalbalance = $scope.totalbalance - each.balance;
                        }

                    });
                    $scope.calculate($scope.statements);
                });
        }
    }
    $q.all([$scope.load(), $scope.loadcustomer() ]).then(function () { });

}])

//CooptavanzaWalletController.controller('CWNavController', ['$scope', '$http', '$q', '$location', '$uibModal', '$rootScope', 'svcHome', 'growl', function ($scope, $http, $q, $location, $uibModal, $rootScope, svcHome, growl) {
//    var backData;
//    console.log("nav run");
//    $scope.submit = function () {
//        svcHome.login($scope.login).then(function (response) {
//            console.log(response);
//            if (response.status == 200) {
//                growl.success("Welcome Back " + response.data.firstname + " " + response.data.lastname + " !");
//                sessionStorage.setItem("cr", JSON.stringify(response.data));
//                location.reload();
//            }else if(status == 500) {
//                growl.warning("Please enter correct mobile number or password!" );
//            }
//        });
//    };

//    $scope.openRegistrationForm = function (data) {
//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/registerForms.html',
//            controller: 'CWRegisterFormsController',
//            size: 'lg',
//            resolve: {
//               data: function () {
//                    return data;
//                }
//            }
//        });
//    };
//    $scope.openRegistration = function (size, parentSelector) {
//        if (sessionStorage.getItem("rr")) {
//            $scope.openRegistrationForm(JSON.parse(sessionStorage.getItem("rr")));
//        }
//        else {
//                $rootScope.modalInstance = $uibModal.open({
//                    animation: true,
//                    templateUrl: '/ng/view/customer/modal/register.html',
//                    controller: 'CWRegisterController',
//                    size: 'md',
//                    windowClass: 'center-modal',
//                    resolve: {
//                        backData: function () { }
//                    }
//                    // sm, md, lg

//                });

//                $rootScope.modalInstance.result.then(function (response) {
//                    $scope.openRegistrationForm(response);
//                }, function () {
//                    });
//        }
//    };

//    $scope.openLogin = function (data) {
//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/login.html',
//            controller: 'CWNavController',
//            size: 'lg'
          
//        });
//    };
    

//}])

//CooptavanzaWalletController.controller('CWNavAdminController', ['$scope', '$http', '$q', '$location', '$uibModal', '$rootScope', 'growl', function ($scope, $http, $q, $location, $uibModal, $rootScope, growl) {
//    var data = JSON.parse(sessionStorage.getItem('cr'));
   
//    $scope.load = function () { 
//            if (sessionStorage.getItem('cr')) {
//                $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
//            }   
//    }

//    $scope.openEmailMoney = function (size, parentSelector) {
//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/EmailMoney.html',
//            controller: 'CWCSendEmailMoneyController',
//            resolve: {
//                data: function () {
//                    return data;
//                }
//            }
//        });
//    };
//    $scope.openSendMoney = function (size, parentSelector) {
//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/SendMoney.html',
//            controller: 'CWCSendMoneyController'

//            /*resolve: {
//                items: function () {
//                    return $scope.items;
//                }
//            }*/
//        });
//    };
//    $scope.openMiniStatement = function (size, parentSelector) {
//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/ministatement.html',
//            controller: 'CWCMinistatementController',
//            size: 'lg',
//            resolve: {
//                data: function () {
//                    return $scope.customer;
//                }
//            }
//        });
//    };
//    $scope.openLoadWallet = function (size, parentSelector) {
//       $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/loadwallet.html',
//            controller: 'CWCLoadWalletController',
//            resolve: {
//                data: function () {
//                    return data;
//                }
//            }
//        });
//    };
//    $scope.openPayoutWallet = function (size, parentSelector) {
//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/PayoutWallet.html',
//            controller: 'CWCPayoutWalletController'

//            /*resolve: {
//                items: function () {
//                    return $scope.items;
//                }
//            }*/
//        });
//    };

//    $q.all([$scope.load]).then(function () { });

//}])



//CooptavanzaWalletController.controller('CWNavCheckController', ['$scope', '$http', '$q', '$location', 'svcHome', 'svcMinistatement', function ($scope, $http, $q, $location, svcHome, svcMinistatement) {
//    $scope.authenticated = false;
//    var data;
//    $scope.searchText = "";
//    $scope.pageNo = 1;
//    $scope.pageSize = 99999;

//    if (sessionStorage.getItem('cr')) {
//        $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
//        if ($scope.customer !== null) {
//            $scope.authenticated = true;
//            $location.path("/dashboard");
//        }
        
        
//    }
//    else { $scope.authenticated = false;}
//    $scope.logout = function () {
//        sessionStorage.removeItem('cr');
//        //location.reload();
//        $location.path("/home");
//    }

 

    
//    $scope.load = function () {
        
//        if ($scope.customer) {
//            svcMinistatement.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
//                .then(function (response) {
//                    $scope.statements = response.data.Results;
//                    $scope.totalbalance = 0;
//                    angular.forEach($scope.statements, function (each) {
//                        if (each.credit) {
//                            each['withdrawals'] = each.balance;
//                            each['deposits'] = 0;
//                            $scope.totalbalance = $scope.totalbalance - each.balance;
//                        } else {
//                            each['deposits'] = each.balance;
//                            each['withdrawals'] = 0;
//                            $scope.totalbalance = $scope.totalbalance + each.balance;
//                        }

//                    });
//                });
//        }
//    }
//    $q.all([$scope.load()]).then(function () { });


//}])