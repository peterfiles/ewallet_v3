﻿CooptavanzaWalletController.controller('CWCTopupAirtimeController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'svcLunex', '$uibModal', '$state', '$rootScope', 'alertify', 'SweetAlert', 'loader', 'svcCommonServices', 'svcDashboardCommon', 'svcMoneyConversion',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, svcLunex, $uibModal, $state, $rootScope, alertify, SweetAlert, loader, svcCommonServices, svcDashboardCommon, svcMoneyConversion) {

        $scope.topupAirtime = {};
        $scope.topupAirtime.topupProduct;
        $scope.showConversion = false;
        $scope.consolidatedData = {};
        $scope.countries;

        $scope.topUpProducts = {};

        $scope.enrolledAccountList = {};
        $scope.currentBalances = {};
        $scope.enrolledCurrencyDetails = [];
        $scope.defaultCur = [];
        $scope.enrolledAccount = [];

        $scope.showProceed = false;

        $scope.showInstruction = "";
        $scope.showInfo = false;

        var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));

    $scope.topupAirtime.checkDate = moment().format("DD MMMM YYYY");
    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2120, 5, 22),
        minDate: new Date(1800, 1, 1),
        startingDay: 0
    };


    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.loadState = function (countryid) {
        svcHome.searchState(countryid).then(function (response) {
            $scope.states = response.data;
        });
    };

    $scope.searchDefaultCurrency = function () {
        svcHome.searchDefaultCurrency().then(function (response) {
            $scope.currencies = response.data;
        });
    };

    $scope.selectedCurrency = function (currency) {
        $scope.topupAirtime.currencyid = currency.id;
        $scope.topupAirtime.currency = currency.iso;
    };

    $scope.selectedCountry = function (country) {
        $scope.topupAirtime['countryid'] = country.id;
        $scope.topupAirtime.country = country.name;
        $scope.loadState(country.id);
    };

    $scope.selectedState = function (state) {
        $scope.topupAirtime.stateid = state.id;
        $scope.topupAirtime.state = state.name;
    };
        
    $loadEnrolledAccounts = function (id) {
        svcDashboardCommon.getEnrolledListByUserId(id).then(function (response) {
            angular.forEach(response.data, function (v, k) {
                if (v.currency === "USD") {
                    $scope.enrolledAccountList = v;
                    svcDashboardCommon.getBalance(id).then(function (resp) {
                    $scope.enrolledBalance = resp.data;
                        angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                            if (eachEB.currency === $scope.enrolledAccountList.currency) {
                                $scope.enrolledAccountList.balance = eachEB.balance;
                                $scope.enrolledCurrencyDetails.push(eachEB.currency + " " + eachEB.balance);
                                $scope.defaultCur.push(eachEB.currency);
                                if ($scope.enrolledAccountList.currency === eachEB.currency) {
                                    $scope.enrolledAccount.push(v);
                                }
                            }
                        });
                    });
                }
            });
            
        });
    };

    $loadCountry = function () {
        svcCommonServices.getCountries().then(function (res) {
            $scope.countries = res.data;
        });
    }
        
    $scope.goToStep2 = function () {
        SweetAlert.swal({ html: true, title: 'Looking for Carriers...', text: loader.l3, showConfirmButton: false });
        var selectedCountry = $.parseJSON($scope.topupAirtime.country);
        var arr = [];
        var c = 0;
        svcLunex.getSkuTopUpProducts().then(function (response) {
            $scope.topUpProducts = response.data.List.Product;
            angular.forEach($scope.topUpProducts, function (v, k) {
                if (parseInt(v.CountryCode) === selectedCountry.phonecode) {
                    arr.push(v);
                }
            });
            $scope.topUpProducts = arr;
            SweetAlert.swal("Loaded all.", "", "info");
        });
    }

    $scope.openLoading = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/loading.html',
            controller: 'CWCGlobalController',
            backdrop: 'static',
            size: 'sm',
            cache: false,
            keyboard: false,
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: true };
                }
            }
        });
    };

    $scope.closeModal = function () {
        setTimeout(function () {
            $rootScope.close();
        }, 2000);
    };

    $scope.productDetails = {};
    $scope.productDetailAmounts = {};
    $scope.showTopUpBtn = false;
    $scope.showTopUpIfDefine = true;
    $scope.getProductSkuToLoadDetails = function () {
        $scope.productDetails = {};
        $scope.productDetailAmounts = {};
        svcLunex.getProductDataBySku($scope.topupAirtime.topupProduct).then(function (response) {
            $scope.productDetails = response.data.Product;
         
            if (response.data.Product.Amounts === null) {
                $scope.showTopUpBtn = true;
                $scope.showTopUpIfDefine = false;
            } else {
                $scope.showTopUpBtn = false;
                $scope.showTopUpIfDefine = true;
                angular.forEach(response.data.Product.Amounts, function (val, key) {
                    if (!(val === null)) {
                        $scope.productDetailAmounts = response.data.Product.Amounts.Amount.length > 0 ? response.data.Product.Amounts.Amount : response.data.Product.Amounts;
                        var x = 0;
                        $scope.toCnvAmnt = [];
                        $scope.compileModedArr = [];
                        $scope.compileModedScope = {};
                        angular.forEach($scope.productDetailAmounts, function (val, key) {
                            svcCommonServices.getConvertedAmount("USD", val.DestCurr, val.Amt).then(function (res) {
                                $scope.compileModedScope = {
                                    Amt: val.Amt,
                                    Denom: val.Denom,
                                    DestAmt: val.DestAmt,
                                    DestCurr: val.DestCurr,
                                    Fee: val.Fee,
                                    convAmt: res.data.convAmt,
                                    amtToPay: res.data.amntToPay,
                                    Instruction: val.Instruction
                                };
                                $scope.compileModedArr[x] = $scope.compileModedScope;
                                x++;
                            });
                        });
                        $scope.productDetailAmounts = $scope.compileModedArr;
                    }
                });
            }
            
        });
    };

    $scope.addFees = function (num1, num2) {
        var res = parseFloat(num1) + parseFloat(num2);
        return res.toFixed(2);
    };

    $scope.topUp = function (amount, currency, torcv, amountToDeduct, instruction) {
        $scope.consolidateDataTopUp(amount, $scope.topupAirtime.topupProduct, currency, torcv, amountToDeduct);
        $scope.showInstruction = instruction !== undefined ? "Instruction : " + instruction : '';
        $scope.showInfo = instruction !== undefined ? true : false;
    };

    $scope.consolidateDataTopUp = function (amount, sku, currency, torcv, amountToDeduct) {
        var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
        $scope.consolidatedData = {
            'amount': amount,
            'torcv': torcv,
            'sku': sku,
            'currency': currency,
            'title': "Confirm Top-Up",
            'id': extractCustomerDetails.id,
            'amountToDeduct': amountToDeduct
        };
        $scope.showProceed = true;
        //$scope.processTopUp($scope.data);
        //SweetAlert.swal("Get this !");
    };
        

    $scope.topUpOpenAmount = function () {
        $scope.consolidatedData = {
            'amount': amount,
            'torcv': torcv,
            'sku': sku,
            'currency': currency,
            'title': "Confirm Top-Up",
            'id': extractCustomerDetails.id,
            'amountToDeduct': amountToDeduct
        }
        //$scope.processTopUp($scope.data);
    };

    $scope.selectedCurrency = function () {
        var data = $scope.consolidatedData;
        $scope.lock = true;
        if ($scope.selectedEnrolledAccount === "" || $scope.selectedEnrolledAccount === undefined) {
            $scope.showConversion = false;
            $scope.convertedAmount = data.amount;
            $scope.convertedAmountToPass = data.amount;
            $scope.lock = false;
            $scope.showTopUpBtn = false;
        } else {
            $scope.showConversion = true;
            var parseForString = $.parseJSON($scope.selectedEnrolledAccount);
            if (data.currency === parseForString.currency) {
                $scope.convertedAmount = data.amount;
                $scope.convertedAmountToPass = data.amount;
                if (data.amount > parseForString.balance) {
                    $scope.convertedAmount = "Please select another account that has enough sufficient balance.";
                    $scope.lock = false;
                    //$scope.showTopUpBtn = false;
                }
            }
        }
    };

    $scope.proceedTopUp = function () {
        var enrolledAccountID = $.parseJSON($scope.selectedEnrolledAccount);
        if ($scope.phoneNumber !== "" && $scope.confirmPassword !== "" && enrolledAccountID.id > 0) {
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
            $scope.data = {
                'accountEnrolledID': enrolledAccountID.id,
                'phone': $scope.phoneNumber,
                'username': extractCustomerDetails.username,
                'password': $scope.confirmPassword,
                'amount': $scope.consolidatedData.amount,
                'skuid': $scope.consolidatedData.sku,
                'curr': $scope.consolidatedData.currency,
                'amountTodeductInEnrolledAccount': $scope.consolidatedData.amountToDeduct
            };
            //console.log($scope.data);
            svcLunex.processTopUp($scope.data).then(function (response) {
                //$rootScope.close();
                if (response.data === "Success") {
                    //$scope.openTYModal("Transacted Successfully");
                    SweetAlert.swal("Success", "Transacted Successfully.", "success");
                    window.location.reload(true);
                } else {
                    //$scope.errorModal(response.data);
                    SweetAlert.swal("Error!", response.data, "error");
                }
            }, function (error) {
                SweetAlert.swal("Error!", "Please try again.", "error");
            });
        } else {
            SweetAlert.swal("Error!", "Please fill all fields to process.", "error");
        }
    };

    $q.all([$loadCountry(), $scope.searchDefaultCurrency(), $loadEnrolledAccounts(extractCustomerDetails.id)]).then(function () {
    });
    
    }])

CooptavanzaWalletController.controller('CWCProcessTopController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcLunex', '$uibModal', '$state', '$rootScope', 'data', '$uibModalInstance', 'svcDashboardCommon', 'svcMoneyConversion', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcLunex, $uibModal, $state, $rootScope, data, $uibModalInstance, svcDashboardCommon, svcMoneyConversion, alertify, SweetAlert, loader) {

        $scope.message = data;
        $scope.lock = false;

        $scope.dismiss = function () {
            $uibModalInstance.close();
            if (!($scope.isloading)) {
                window.location.reload(true);
            }
        };

        $scope.enrolledAccountList = {};
        $scope.currentBalances = {};
        $scope.enrolledCurrencyDetails = [];
        $scope.defaultCur = [];
        $scope.enrolledAccount = [];
        $loadEnrolledAccounts = function () {
            svcDashboardCommon.getEnrolledListByUserId(data.id).then(function (response) {
                $scope.enrolledAccountList = response.data;
                svcDashboardCommon.getBalance(data.id).then(function (resp) {
                    $scope.enrolledBalance = resp.data;
                    angular.forEach($scope.enrolledAccountList, function (each, index) {
                        each['balance'] = 0;
                        angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                            if (eachEB.currency === each.currency) {
                                each.balance = eachEB.balance;
                                $scope.enrolledCurrencyDetails.push(eachEB.currency + " " + eachEB.balance);
                                $scope.defaultCur.push(eachEB.currency);
                                angular.forEach($scope.enrolledAccountList, function (eachEL, index) {
                                    if (eachEL.currency === eachEB.currency) {
                                        $scope.enrolledAccount.push(eachEL);
                                    }
                                });
                            }
                        });
                    });
                });
            });
            svcDashboardCommon.getBalance(data.id).then(function (response) {
            });
        };

        $scope.convertedAmountToPass = "";

        $scope.proceedTopUp = function () {
            //$scope.openLoading("Processing Top-Up...");
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
            var enrolledAccountID = $.parseJSON($scope.selectedEnrolledAccount);
            $scope.data = {
                'accountEnrolledID': enrolledAccountID.id,
                'phone': $scope.phoneNumber,
                'username': extractCustomerDetails.username,
                'password': $scope.confirmPassword,
                'amount': data.amount,
                'skuid': data.sku,
                'curr': data.currency,
                'amountTodeductInEnrolledAccount': $scope.convertedAmountToPass
            };
            svcLunex.processTopUp($scope.data).then(function (response){
                //$rootScope.close();
                if (response.data === "Success") {
                    //$scope.openTYModal("Transacted Successfully");
                    SweetAlert.swal("Success", "Transacted Successfully.", "success");
                    window.location.reload(true);
                } else {
                    //$scope.errorModal(response.data);
                    SweetAlert.swal("Error!", response.data, "error");
                }
            }, function (error) {
                    SweetAlert.swal("Error!", "Please try again.", "error");
                });
        };

        $scope.convertedAmount = "";
       

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                cache: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };


        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                backdrop: 'auto',
                size: 'lg',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                cache: false,
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $q.all([$loadEnrolledAccounts()]).then(function () {
        });

    }])
