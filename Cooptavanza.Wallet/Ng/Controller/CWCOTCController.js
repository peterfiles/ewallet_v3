﻿CooptavanzaWalletController.controller('CWCOTCController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', '$uibModal', 'svcDashboardCommon', '$rootScope', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, $uibModal, $state, $rootScope, alertify, SweetAlert, loader) {

        $scope.otc = {};
        $scope.otc.abaRouting = "";
        $scope.otc.acctNum = "";
        $scope.otc.checkNum = "";
        $scope.otc.bankName = "";
        $scope.otc.checkDate = moment().format("DD MMMM YYYY");

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.otc.currencyid = currency.id;
            $scope.otc.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.otc['countryid'] = country.id;
            $scope.otc.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.otc.stateid = state.id;
            $scope.otc.state = state.name;
        };

        //$scope.openTYModal = function (message) {
        //    $rootScope = $uibModal.open({
        //        templateUrl: '/ng/view/common/modal/success.html',
        //        controller: 'CWCGlobalController',
        //        resolve: {
        //            data: function () {
        //                return $scope.contentMessage = { message: message, isloading: false };
        //            }
        //        }
        //    });
        //};

        //$scope.openLoading = function (message) {
        //    $rootScope = $uibModal.open({
        //        templateUrl: '/ng/view/common/modal/loading.html',
        //        controller: 'CWCGlobalController',
        //        backdrop: 'static',
        //        size: 'sm',
        //        keyboard: false,
        //        resolve: {
        //            data: function () {
        //                return $scope.contentMessage = { message: message, isloading: true };
        //            }
        //        }
        //    });
        //};

        //$scope.errorModal = function (message) {
        //    $rootScope = $uibModal.open({
        //        templateUrl: '/ng/view/common/modal/error.html',
        //        controller: 'CWCGlobalController',
        //        size: 'lg',
        //        resolve: {
        //            data: function () {
        //                return $scope.contentMessage = { message: message, isloading: false };
        //            }
        //        }
        //    });
        //};

        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { })

    }])