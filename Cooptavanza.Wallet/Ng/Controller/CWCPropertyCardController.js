﻿CooptavanzaWalletController.controller('CWCPropertyCardController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, alertify, SweetAlert, loader) {

    $scope.propertyCard = {};
    $scope.propertyCard.abaRouting = "";
    $scope.propertyCard.acctNum = "";
    $scope.propertyCard.checkNum = "";
    $scope.propertyCard.bankName = "";
    $scope.propertyCard.checkDate = moment().format("DD MMMM YYYY");

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2120, 5, 22),
        minDate: new Date(1800, 1, 1),
        startingDay: 0
    };


    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.loadState = function (countryid) {
        svcHome.searchState(countryid).then(function (response) {
            $scope.states = response.data;
        });
    };

    $scope.searchAllCountry = function () {
        svcHome.searchAllCountry("").then(function (response) {
            $scope.countries = response.data;
        });
    };

    $scope.searchDefaultCurrency = function () {
        svcHome.searchDefaultCurrency().then(function (response) {
            $scope.currencies = response.data;
        });
    };

    $scope.selectedCurrency = function (currency) {
        $scope.propertyCard.currencyid = currency.id;
        $scope.propertyCard.currency = currency.iso;
    };

    $scope.selectedCountry = function (country) {
        $scope.propertyCard['countryid'] = country.id;
        $scope.propertyCard.country = country.name;
        $scope.loadState(country.id);
    };

    $scope.selectedState = function (state) {
        $scope.propertyCard.stateid = state.id;
        $scope.propertyCard.state = state.name;
    };



    $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { })
    
}])