﻿CooptavanzaWalletController.controller('CWMoneySwapRemitController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcCommonServices', 'svcMoneyConversion', 'svcMinistatement', 'svcSendMoney', 'growl', '$uibModal', '$rootScope', 'svcDashboardCommon', 'svcMoneySwap', 'svcHome', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcCommonServices, svcMoneyConversion, svcMinistatement, svcSendMoney, growl, $uibModal, $rootScope, svcDashboardCommon, svcMoneySwap, svcHome, alertify, SweetAlert, loader) {

        var SendMoney = $stateParams.SendMoney; //getting SendMoneyVal
        $scope.lockBtn = true;
        $scope.authenticated = false;
        $scope.startNotif = true;
        $scope.notifyZeroBalance = false;
        $scope.notifyExceededLimit = false;
        $scope.reason = false;

        $scope.SndRemitPurpose = "";
        $scope.RcvIdType = "";

        var data;
        $scope.moneySwap = {};
        $scope.ms = {};
        $scope.ms.DescriptionReason = "";
        $scope.moneySwap.transactionid = "";
        $scope.moneySwap.benificiarymobileno = "";
        $scope.amountfrom = 0;
        $scope.amountto = 0;
        $scope.SndIdType = "IDCARD";
        $scope.RcvIdType = "IDCARD";
        $scope.ms['RcvIdType'] = "IDCARD"
        $scope.searchText = "";
        $scope.pageNo = 1;
        $scope.pageSize = 99999;
        $scope.balancelimit = 0;
        $scope.ms.mobilenumber = "";

        $scope.currencyName;
        $scope.convertedValue;
        $scope.getConvertedBalance;

        $scope.SendEmailMoney = {};
        $scope.message = '';
        $scope.result = "color-default";
        $scope.isViewLoading = false;

        $scope.currencyfromiso = "USD";
        $scope.currencytoiso = "USD";

        $scope.currencyData;
        $scope.getConvertedForBenificiary;
        $scope.availableCurrencies;
        $scope.currencyValue = 0.00;
        
        $scope.mobileChange = function () {
            var change = document.getElementById("mobilenumber").value;
            if (document.getElementById("mobilenumber").value[0] && document.getElementById("mobilenumber").value[0] !== "+") {
                document.getElementById("mobilenumber").value = "+" + change;
            }
        }

        $scope.submitForm = function () {
            $scope.lockBtn = true;
            $scope.isViewLoading = true;
            //$scope.openLoading("Processing Transaction.");
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            $scope.moneySwap.customerid = $scope.customer.id;
            $scope.moneySwap.amount = $scope.amountto;
            $scope.moneySwap.datecreated = moment();
            $scope.ms['TranAmt'] = $scope.amountto;
            $scope.ms['SndRemitPurpose'] = $scope.ms.SndRemitPurpose + $scope.ms.DescriptionReason;
            $scope.ms['SndIdType'] = $scope.SndIdType;
            $scope.ms['RcvIdType'] = "IDCARD";
            $scope.moneySwap['MoneySwap'] = $scope.ms;
            $scope.moneySwap['Customer'] = $scope.customer;
            
            svcMoneySwap.SendMoney($scope.moneySwap).then(function (response) {
                //$rootScope.close();
                if (response.status === 200) {
                    if (response.data.RespCode === "00") {
                        SweetAlert.swal("Success", "Successfully Processed!", "success");
                        $scope.moneySwap = null;
                        $scope.data = {
                            'SenderFullName': $scope.customer.firstname + " " + $scope.customer.lastname,
                            'SenderMobileNumber': $scope.customer.mobilenumber,
                            'SenderEmail': $scope.customer.email,
                            'ReceiverFullName': $scope.ms.RcvFirstName + " " + $scope.ms.RcvLastName,
                            'ReceiverMobileNumber': $scope.ms.mobilenumber,
                            'ReceiverEmail': $scope.ms.email,
                            'ReceiverCrdNo': $scope.ms.RcvAccntNo,
                            'Fees': response.data.TotalFeeAmount,
                            'ConversionRate': response.data.ConversionRate,
                            'sentAmount': response.data.SentAmount,// in RMB
                            'AmountInUSD': response.data.FloatAmount,
                            'trackingNo': response.data.ReferenceId,//ReferenceId
                            'TellerFullName': "E-Wallet System Management Team",
                            'transactionId': response.data.TransactionId,
                            'state': $scope.ms.state, 'country': $scope.ms.country,
                            'date': moment()
                        }

                        $scope.printModal($scope.data);
                        $scope.lockBtn = false;
                        $scope.load();

                    } else if (response.data.RespCode === "05"){
                        //$scope.errorModal("Transaction failed. ID certification fail. If cardholder is a chinese national, please retry by swapping the names. If validation fails again, please verify the chinese pinyin names.",'lg');
                        SweetAlert.swal("Error", "Transaction failed. ID certification fail. If cardholder is a chinese national, please retry by swapping the names. If validation fails again, please verify the chinese pinyin names.", "error");
                    }
                    else if (response.data.RespCode === "40") {
                        //$scope.errorModal("Transaction failed. Transaction is not supported.", 'md');
                        SweetAlert.swal("Error", "Transaction failed. Transaction is not supported.", "error");
                    }
                    else if (response.data.RespCode === "14") {
                        //$scope.errorModal("Transaction failed. No such account.", 'md');
                        SweetAlert.swal("Error", "Transaction failed. No such account.", "error");
                    }
                    else if (response.data.RespCode === "68") {
                        //$scope.errorModal("Transaction failed. Issuer response time-out. Please try submitting again.", 'md');
                        SweetAlert.swal("Error", "Transaction failed. Issuer response time-out. Please try submitting again.", "error");
                    }
                    else if (response.data.RespCode === "MEX01") {
                        //$scope.errorModal("Transaction failed. Receiver Account No is missing or invalid", 'md');
                        SweetAlert.swal("Error", "Transaction failed. Receiver Account No is missing or invalid", "error");
                    }
                    else {
                        //$scope.errorModal("Transaction failed. Internal Error. Please try submitting again.", 'md');
                        SweetAlert.swal("Error", "Transaction failed. Internal Error. Please try submitting again.", "error");
                    }
                    $scope.lockBtn = false;
                } else {
                    $scope.lockBtn = false;
                    //$scope.errorModal("Transaction failed.");
                    SweetAlert.swal("Error", "Transaction failed.", "error");
                }
            }, function (err) {
                //$rootScope.close();
                $scope.lockBtn = false;
                //$scope.errorModal("Transaction failed. Please Try again.");
                SweetAlert.swal("Error", "Transaction failed. Please Try again.", "error");
            });
            $scope.isViewLoading = false;
        };

        $scope.loadDefaultValues = function () {
            svcCommonServices.LoadDefaultValues().then(function (result) {
                $scope.currencyData = result.data;
            });
        };

        $scope.changeTT = function () {
            if ($scope.moneySwap.transactionid === 0) { $scope.lockBtn = true; }
            else { $scope.lockBtn = false; }
        };

        $scope.setCurrency = function () {
            for (x = 0; x < $scope.currencyData.length; x++) {
                if ($scope.setCurrencyForConversion === $scope.currencyData[x].id) {
                    $scope.currencyName = $scope.currencyData[x].iso;
                    svcMoneyConversion.convertCurrency($scopep.currency, $scope.currencyName, 1).then(function (result) {
                        $scope.convertedValue = result.data.rates;
                        $scope.getConvertedBalance = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);
                    });
                    break;
                }
            }
        };

        $scope.setCurrencyForBenificiary = function (amount) {
            $scope.amountfrom = amount;
            if ($scope.amountfrom > $scope.balancelimit) {
                $scope.notifyExceededLimit = true;
            }
            else {
                $scope.notifyExceededLimit = false;
                if ($scope.amountfrom > 0 && $scope.currencytoiso !== undefined && $scope.currencyfromiso !== undefined) {
                    svcMoneyConversion.convertCurrency($scope.currencyfromiso, $scope.currencytoiso, $scope.amountfrom).then(function (result) {
                        $scope.convertedValue = result.data.rates;
                        $scope.getConvertedForBenificiary = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);
                       
                        if ($scope.currencyfromiso === $scope.currencytoiso) {
                            $scope.amountto = $scope.amountfrom;

                        } else {
                            $scope.amountto = result.data.rates[$scope.currencytoiso];
                        }
                    });
                }
            }
            
        };

        $scope.loadAllCurrencies = function () {
            svcMoneyConversion.getAllAvailableCurrency().then(function (result) {
                $scope.availableCurrencies = { 'USD':result.data.rates.USD };
                $scope.conversionRateFrom = $scope.customer.currency;
            });
        };

        $scope.convertCurrency = function (from, to) {
                if (from === undefined) {
                from = $scope.customer.currency;
            }
            svcMoneyConversion.convertCurrency(from, to, 1).then(function (result) {
                $scope.currencyValue = result.data.rates;
            });
        };
        $scope.threeDecimal = function (num) {
            return num;
        };

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };

        $scope.formats = ['dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.selectedCountry = function (country) {
            $scope.ms.countryid = country.id;
            $scope.ms['country'] = country.Name;
            $scope.loadState(country.id);
        };
        $scope.selectedState = function (state) {
            $scope.ms.stateid = state.id;
            $scope.ms['state'] = state.Name;
        };
        
        $scope.loadState = function (countryid) {
            svcHome.searchStatedashboard(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };
       
        $scope.searchAllCountry = function (countryid) {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.load = function () {
            if ($scope.customer) {
                svcDashboardCommon.getEnrolledListByUserId($scope.customer.id).then(function (res) {
                    $scope.customer.EnrolledList = res.data;
                    svcDashboardCommon.getBalance($scope.customer.id).then(function (resp) {
                        $scope.enrolledBalance = resp.data;
                        angular.forEach($scope.customer.EnrolledList, function (each, index) {
                            each['balance'] = 0;
                            angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                                if (eachEB.currency === each.currency) {
                                    each.balance = eachEB.balance;
                                }
                            });
                        });
                        $scope.selectCurrencyFrom($scope.currencyfromiso);
                    });
                });
            }

            svcMoneySwap.getMEXPredifined().then(function (response) {
                $scope.remitPurposes = response.data.SRP;
                $scope.remitIdType = response.data.SIT;
            });
        }

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message, size) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: size,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

       

        $scope.selectCurrencyFrom = function (currency) {
            var found = false;
            angular.forEach($scope.customer.EnrolledList, function (each, index) {
                if (each.currency === currency) {
                    if (each.balance <= 0) {
                        $scope.lockBtn = true;
                        $scope.balancelimit = 0;
                        $scope.notifyZeroBalance = true;
                        $scope.startNotif = false;
                        $scope.amountfrom = 0;
                        $scope.amountto = 0;

                    } else {
                        $scope.lockBtn = false;
                        $scope.notifyZeroBalance = false;
                        $scope.startNotif = false;
                        $scope.balancelimit = each.balance;
                        found = true;

                    }
                } else if (each.currency !== currency && found === false) {
                    $scope.lockBtn = true;
                    $scope.notifyZeroBalance = true;
                    $scope.startNotif = false;
                    $scope.balancelimit = 0;
                }
            });
        };

        $scope.selectedRemittance = function (remittance) {
            
            if (remittance.code === '1099-' || remittance.code === '1199-' || remittance.code === '1299-') {
                $scope.reason = true;
            } else {
                $scope.reason = false;
            }
            $scope.ms['SndRemitPurpose'] = remittance.code;
        }

        $scope.selectedRcvIdType = function (type) {
            $scope.ms['RcvIdType'] = type.status;
        }

        $scope.selectedSndIdType = function (type) {
            $scope.ms['SndIdType'] = type.status;
        }

        $scope.printthis = function () {
            $scope.data = {
                'SenderFullName': $scope.customer.firstname + " " + $scope.customer.lastname,
                'SenderMobileNumber': $scope.customer.mobileNumber,
                'SenderEmail': $scope.customer.email,
                'ReceiverFullName': $scope.ms.RcvFirstName + " " + $scope.ms.RcvLastName,
                'ReceiverMobileNumber': $scope.ms.mobilenumber,
                'ReceiverEmail': $scope.ms.email,
                'ReceiverCrdNo': $scope.ms.RcvAccntNo,
                'Fees': 5,//TotalFeeAmount
                'ConversionRate': 6.0479,//ConversionRate
                'sentAmount': 3024.31,//SentAmount in RMB
                'AmountInUSD': 500,
                'trackingNo': "54646465464",//ReferenceId
                'TellerFullName': "E-Wallet System Management Team",
                'date':moment()
            }
            $scope.printModal($scope.data);
        }

        $scope.printModal = function (data) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/print/moneySwapReceipt.html',
                controller: 'CWCMSReceiptController',
                size: 'md',
                resolve: {
                    data: function () {
                        return data
                    }
                }
            });

            $rootScope.result.then(function (response) {
                //$scope.openTYModal("Thank You for giving us the opportunity to serve you.");
                SweetAlert.swal("Success","Thank You for giving us the opportunity to serve you.","success");
            }, function () {
                    });
        };

        $q.all([$scope.load(), $scope.loadAllCurrencies(), $scope.loadDefaultValues(), $scope.searchAllCountry()]).then(function () {
        });

    }]);


CooptavanzaWalletController.controller('CWCMoneySwapValidationController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams) {

        $scope.topupAirtime = {};
        $scope.topupAirtime.checkDate = moment().format("DD MMMM YYYY");
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.topupAirtime.currencyid = currency.id;
            $scope.topupAirtime.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.topupAirtime['countryid'] = country.id;
            $scope.topupAirtime.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.topupAirtime.stateid = state.id;
            $scope.topupAirtime.state = state.name;
        };

        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { })

    }])

CooptavanzaWalletController.controller('CWCMSReceiptController', ['$scope', '$http', '$q', '$location', '$stateParams', 'data', 
    function ($scope, $http, $q, $location, $stateParams,data) {
        $scope.data = data;
        $scope.close = function () {
            this.$close();
        }

        $scope.download = function () {
            html2canvas(document.getElementById('exportthis'), {
                onrendered: function (canvas) {
                    var data = canvas.toDataURL();
                    var docDefinition = {
                        content: [{
                            image: data,
                            width: 500,
                        }]
                    };
                    pdfMake.createPdf(docDefinition).download("China_Remit.pdf");
                }
            });
        }

    }])