﻿CooptavanzaWalletController.controller('CWCEFTController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, alertify, SweetAlert, loader) {

        $scope.eft = {};
        $scope.eft.abaRouting = "";
        $scope.eft.acctNum = "";
        $scope.eft.checkNum = "";
        $scope.eft.bankName = "";
        $scope.eft.checkDate = moment().format("DD MMMM YYYY");

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.eft.currencyid = currency.id;
            $scope.eft.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.eft['countryid'] = country.id;
            $scope.eft.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.eft.stateid = state.id;
            $scope.eft.state = state.name;
        };



        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { })

    }])