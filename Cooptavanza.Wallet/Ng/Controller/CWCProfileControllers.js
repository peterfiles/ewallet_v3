﻿CooptavanzaWalletController.controller('CWProfileController', ['$scope', '$http', '$q', '$location', 'svcDashboardCommon', '$rootScope', '$uibModal', 'growl', '$uibModal', '$stateParams', 'NgTableParams', 'Upload', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, svcDashboardCommon, $rootScope, $uibModal, growl, $uibModal, $stateParams, NgTableParams, Upload, alertify, SweetAlert, loader) {

        var data = $scope.customer;
        $scope.today = new Date();
        $scope.countries = {};
        $scope.currCountry = 0;
        $scope.currState = 0;
        $scope.notEnter = true;
        $scope.password = {};
        $scope.isSave = true;
        $scope.customer = {};
        $scope.customer.ImageIdCard = {};
        $scope.eDate = "";
        $scope.showSaveDetailsBtn = true;
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.uploadProfile = function (blob) {
            var request = new XMLHttpRequest();
         //   var size = blob.$ngfOrigSize;
    
           // var file = new File([blob], blob.$ngfName, {'size': size, 'type': blob.type });
            var file = document.getElementById("file-Profile").files[0];
            var formData = new FormData();
            formData.append('Files', file);
            formData.append('userId', $scope.customer.id);
           
            formData.append('imageProfileId', $scope.customer.ImageProfile.id);

            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                        formData.forEach(function (val, key, fD) {
                            formData.delete(key)
                    });
                        $scope.customer.ImageProfile = request.reponseText;
                        sessionStorage.setItem('cr', JSON.stringify($scope.customer));
                        window.location.reload(true);
                }
            };

            request.open("POST", '../ImageUploadProfile/save', true);
            request.send(formData);
        };

        $scope.myFunction = function (event) {
            if (event.clientX > 0 && event.clientY > 0) {
                var x = document.getElementById('myDIV');
                if (x.style.display === 'none') {
                    x.style.display = 'block';
                } else {
                    x.style.display = 'none';
                }
            }
        }

        $scope.changeDateFormat = function (val) {
            //console.log(moment(val).diff($scope.today, 'day'));
            if (moment(val).diff($scope.today, 'day') < 0){
                SweetAlert.swal("Warning!", "You have an Expired Id Card.", "warning");
                $scope.showSaveDetailsBtn = false;
            } else {
                $scope.showSaveDetailsBtn = true;
                $scope.customer.ImageIdCard.expiryDate = moment(val).format('DD/MM/YYYY');
                $scope.eDate = moment(val).format('DD/MM/YYYY h:mm A');
            }
        };

        $scope.saveDetails = function () {
            $scope.customer.ImageIdCard.id = $scope.customer.ImageIdCard.id != null ? $scope.customer.ImageIdCard.id : 0;
            $scope.customer.ImageIdCard.userId = $scope.customer.id;
            $scope.customer.ImageIdCard.imageString = $scope.customer.ImageIdCard.imageString;
            $scope.customer.ImageIdCard.path = $scope.customer.ImageIdCard.path;
            $scope.customer.ImageIdCard.idCardNo = $scope.customer.ImageIdCard.idCardNo;
            $scope.customer.ImageIdCard.idCardType = $scope.customer.ImageIdCard.idCardType;
            $scope.customer.ImageIdCard.ed = $scope.customer.ImageIdCard.expiryDate;
            svcDashboardCommon.SaveImageIdCardDetailsOnly($scope.customer.ImageIdCard).then(function (res) {
                $scope.customer.ImageIdCard = res.data;
                sessionStorage.setItem('cr', JSON.stringify($scope.customer));
                window.location.reload(true);
            });
        };
        

        $scope.uploadCardId = function () {
            var request = new XMLHttpRequest();
            var file = document.getElementById("file-CardId").files[0];
            var formData = new FormData();
            formData.append('Files', file);
            formData.append('userId', $scope.customer.id);

            formData.append('imageCardId', $scope.customer.ImageIdCard.id);

            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    formData.forEach(function (val, key, fD) {
                        formData.delete(key);
                        $scope.customer.ImageIdCard = request.reponseText;
                        window.location.reload(true);
                    });
                }
            };

            request.open("POST", '../ImageUploadCardId/save', true);
            request.send(formData);
        };

        $scope.uploadBill = function () {
            var request = new XMLHttpRequest();
            var file = document.getElementById("file-Bill").files[0];
            var formData = new FormData();
            formData.append('Files', file);
            formData.append('userId', $scope.customer.id);

            formData.append('imageBillId', $scope.customer.ImageBill.id);

            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    formData.forEach(function (val, key, fD) {
                        formData.delete(key)
                    });
                    $scope.customer.ImageBill = request.reponseText;
                    window.location.reload(true);
                }
            };

            request.open("POST", '../ImageUploadBill/save', true);
            request.send(formData);
        };


        $scope.customer.phonenumber = "";
        $scope.customer.mobilenumber = "";

        $scope.changeMobile = function () {

            var change = document.getElementById("mobilenumber").value;
            if (document.getElementById("mobilenumber").value[0] && document.getElementById("mobilenumber").value[0] != "+") {
                document.getElementById("mobilenumber").value = "+" + change;
            }
        }

        $scope.changePhone = function () {
            var change = document.getElementById("phonenumber").value;
            if (document.getElementById("phonenumber").value[0] && document.getElementById("phonenumber").value[0] != "+") {
                document.getElementById("phonenumber").value = "+" + change;
            }
        }

        $scope.load = function () {
            if (data) {
                //svcDashboardCommon.GetCustomerById(data.id).then(function (response) {
                $scope.customer = JSON.parse(sessionStorage.getItem('cr'));
                    if ($scope.customer.ImageProfile) {
                    }
                    else { $scope.customer.ImageProfile = { 'id': 0, 'path': null, 'imageString': null }; }
                    if ($scope.customer.ImageIdCard) {
                    }
                    else { $scope.customer.ImageIdCard = { 'id': 0, 'path': null, 'imageString': null }; }
                    if ($scope.customer.ImageBill) {
                    }
                    else { $scope.customer.ImageBill = { 'id': 0, 'path': null, 'imageString': null }; }
                    $scope.customer.ImageIdCard = $scope.customer.ImageIdCard;
                    $scope.customer.ImageIdCard.expiryDate = moment($scope.customer.ImageIdCard.expiryDate).format('DD/MM/YYYY');
                    $scope.ImageBill = $scope.customer.ImageBill;
                    data = $scope.customer;
                    $scope.customer = data;
                    svcDashboardCommon.searchAllCountry("").then(function (response) {
                        $scope.countries = response.data;
                        angular.forEach($scope.countries, function (val, key) {
                            if (val.id === $scope.customer.countryid) {
                                $scope.currCountry = val;
                                svcDashboardCommon.searchState($scope.customer.countryid).then(function (response) {
                                    $scope.states = response.data;
                                    angular.forEach($scope.states, function (val2, key2) {
                                        if (val2.id === $scope.customer.stateid) {
                                            $scope.currState = val2.id;
                                        }
                                    });
                                });
                            }
                        });
                    });
                //});
            }
        }

        $scope.loadState = function (countryid) {
            svcDashboardCommon.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        
        //$scope.searchAllCountry = function () {
        //    svcDashboardCommon.searchAllCountry("").then(function (response) {
        //        $scope.countries = response.data;
        //    });
        //};
        

        $scope.searchDefaultCurrency = function () {
            svcDashboardCommon.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.customer.currencyid = currency.id;
            $scope.customer.currency = currency.iso;
        };

        $scope.selectedCountry = function () {
            angular.forEach($scope.countries, function (val, key) {
                if (val.commonname === $scope.customer.country) {
                    $scope.customer.country = val.commonname;
                    $scope.customer.countryid = val.id;
                    $scope.loadState(val.id);
                }
            });
        };

        $scope.selectedState = function () {
            angular.forEach($scope.states, function (val, key) {
                if (val.name === $scope.customer.state) {
                    $scope.customer.stateid = val.id;
                }
            });
            
        };
        //<input type='file' ng-model='imageBill' base-sixty-four-input>
        $scope.validSave = function () {
            var valid = true;
            if ($scope.customer.phonenumber === "" || $scope.customer.phonenumber === undefined) { return false };
            if ($scope.customer.mobilenumber === "" || $scope.customer.mobilenumber === undefined) { return false };
            if ($scope.customer.email === "" || $scope.customer.email === undefined) { return false };
            if ($scope.customer.address === "" || $scope.customer.address === undefined) { return false };
            if ($scope.customer.city === "" || $scope.customer.city === undefined) { return false };
            if ($scope.customer.country === "" || $scope.customer.country === undefined) { return false };
            if ($scope.customer.state === "" || $scope.customer.state === undefined) { return false };
            if ($scope.customer.zipcode === "" || $scope.customer.zipcode === undefined) { return false };

            console.log($scope.customer.phonenumber,
                $scope.customer.mobilenumber,
                $scope.customer.email,
                $scope.customer.address,
                $scope.customer.city,
                $scope.customer.country,
                $scope.customer.state,
                $scope.customer.zipcode
            );

            return valid;
        };

        $scope.saveProfile = function () {
            $scope.lockBtn = true;
            //$scope.openLoading("Saving Changes.");
            if ($scope.validSave()) {
                SweetAlert.swal({ html: true, title: 'Saving Changes....', text: loader.l3, showConfirmButton: false });
                $scope.customer.ImageBill = {
                    'id': $scope.customer.ImageBill.id != null ? $scope.customer.ImageBill.id : 0,
                    'userId': $scope.customer.id,
                    'date_created': moment($scope.today).format('YYYY/MM/DD h:mm A'),
                    'imageString': JSON.stringify($scope.customer.ImageBill.imageString),
                    'path': $scope.customer.ImageBill.path
                }

                $scope.customer.ImageProfile = {
                    'id': $scope.customer.ImageProfile.id != null ? $scope.customer.ImageProfile.id : 0,
                    'userId': $scope.customer.id,
                    'date_created': moment($scope.today).format('YYYY/MM/DD h:mm A'),
                    'imageString': JSON.stringify($scope.customer.ImageProfile.imageString),
                    'path': $scope.customer.ImageProfile.path
                }

                $scope.customer.ImageIdCard = {
                    'id': $scope.customer.ImageIdCard.id != null ? $scope.customer.ImageIdCard.id : 0,
                    'userId': $scope.customer.id,
                    'date_created': moment($scope.today).format('YYYY/MM/DD h:mm A'),
                    'imageString': JSON.stringify($scope.customer.ImageIdCard.imageString),
                    'path': $scope.customer.ImageIdCard.path,
                    'idCardNo': $scope.customer.ImageIdCard.idCardNo,
                    'idCardType': $scope.customer.ImageIdCard.idCardType,
                    'expiryDate': moment($scope.customer.ImageIdCard.expiryDate).format('YYYY/MM/DD h:mm A')
                }

                //$scope.customer.countryid = $scope.customer.country;
                var d = $scope.customer;
                if ($scope.password.new !== undefined) {
                    if ($scope.password.new.length > 3) {
                        var old = $scope.password.old;
                        svcDashboardCommon.passwordCheck(d.mobilenumber, old).then(function (response) {
                            if (response.data === "valid") {
                                $scope.customer.password = $scope.password.new;
                                $scope.save($scope.customer);
                            } else {
                                SweetAlert.swal("Warning!", "Kindly input your old password.", "warning");
                                $scope.lockBtn = false;
                            }
                        });

                    } else if ($scope.password.new.length > 0 && $scope.password.new.length < 3) {
                        SweetAlert.swal("Warning!", "Kindly put atleast 4 characters in new password.", "warning");
                        $scope.lockBtn = false;
                    } else {
                        $scope.save($scope.customer);
                    }
                } else {
                    $scope.save($scope.customer);
                }
            } else {
                $scope.lockBtn = false;
                SweetAlert.swal("Error!", "Please fill-up all fields.", "error");
            }
        }

        $scope.save = function (d) {
            svcDashboardCommon.SaveCustomer(d).then(function (response) {
                //$rootScope.close();
                if (response.data.ErrorMessage.toString().length === 0) { // is not empty
                    SweetAlert.swal("Success", "Your profile have been updated successfully.", "success");
                    growl.success("Your profile have been updated successfully.");
                    sessionStorage.setItem('cr', JSON.stringify(response.data.cData));
                    $scope.customer = response.data.cData;
                    window.location.reload(true);
                } else {
                    //growl.error(response.data.ErrorMessage);
                    SweetAlert.swal("Error!", response.data.ErrorMessage, "error");
                }
                $scope.lockBtn = false;
            }, function (err) {
                //$rootScope.close();
                $scope.lockBtn = false;
                //growl.warning("Your account was not updated. Please try again saving.")
                SweetAlert.swal("Error!", "Your account was not updated. Please try again saving.", "error");
            });
        };
           

        $scope.onAfterImageBill = function () {
            $scope.lockImageBillBtn = true;
            //growl.success("Updating your Bill picture.");
            SweetAlert.swal({ html: true, title: 'Updating your Bill picture....', text: loader.l3, showConfirmButton: false });
            $scope.customer.ImageBill = {
                'id': $scope.customer.ImageBill.id != null ? $scope.customer.ImageBill.id : 0,
                'userId': $scope.customer.id,
                'date_created': moment($scope.today).format('YYYY/MM/DD h:mm A'),
                'imageString': JSON.stringify($scope.customer.ImageBill.imageString)

            }
            var d = $scope.customer.ImageBill;
            svcDashboardCommon.SaveImageBill(d).then(function (response) {
                $scope.customer.ImageBill = response.data; 
                //growl.success("Your latest bill picture have been updated successfully.");
                SweetAlert.swal("Success", "Your latest bill picture have been updated successfully.", "success");
                if ($scope.customer.ImageBill) {
                    if ($scope.customer.ImageBill.imageString !== null) {
                        $scope.customer.ImageBill.imageString = typeof $scope.customer.ImageBill.imageString === 'object' ? $scope.customer.ImageBill.imageString : JSON.parse($scope.customer.ImageBill.imageString);
                    }
                }
                else { $scope.customer.ImageBill = { 'id': 0, 'imageString': null }; }
                sessionStorage.setItem('cr', JSON.stringify($scope.customer));

                location.reload();
                $scope.lockImageBillBtn = false;
            }, function (err) {
                $scope.lockImageBillBtn = false;
                //growl.warning("Your account was not updated. Please try again saving.")
                SweetAlert.swal("Error", "Your account was not updated. Please try again saving.", "error");
            });
        }

        $scope.onchangeImageProfile = function () {
        }
        

        $scope.onAfterImageProfile = function () {
            //growl.success("Updating your profile picture.");
            SweetAlert.swal({ html: true, title: 'Updating your profile picture....', text: loader.l3, showConfirmButton: false });
            $scope.lockImageProfileBtn = true;
            $scope.customer.ImageProfile = {
                'id': $scope.customer.ImageProfile.id != null ? $scope.customer.ImageProfile.id : 0,
                'userId': $scope.customer.id,
                'date_created': moment($scope.today).format('YYYY/MM/DD h:mm A'),
                'imageString': JSON.stringify($scope.customer.ImageProfile.imageString)
                //'path': $scope.ImageProfile.filetype
            }

            var d = $scope.customer.ImageProfile;
            svcDashboardCommon.SaveImageProfile(d).then(function (response) {
                //growl.success("Your profile image have been updated successfully.");
                SweetAlert.swal("Success", "Your profile image have been updated successfully.", "success");
                $scope.customer.ImageProfile = response.data;
                if ($scope.customer.ImageProfile) {
                    if ($scope.customer.ImageProfile.imageString !== null) {
                        $scope.customer.ImageProfile.imageString = typeof $scope.customer.ImageProfile.imageString === 'object' ? $scope.customer.ImageProfile.imageString : JSON.parse($scope.customer.ImageProfile.imageString);
                    }
                }
                sessionStorage.setItem('cr', JSON.stringify($scope.customer));
                location.reload();
                $scope.lockImageProfileBtn = false;
            }, function (err) {
                $scope.lockImageProfileBtn = false;
                //growl.warning("Your account was not updated. Please try again saving.")
                SweetAlert.swal("Error", "Your account was not updated. Please try again saving.", "error");
            });
        }

        $scope.onAfterImageIdCard = function () {
            //growl.success("Updating your ID Card Picture.");
            SweetAlert.swal({ html: true, title: 'Updating your ID Card Picture....', text: loader.l3, showConfirmButton: false });
            $scope.customer.ImageIdCard = {
                'id': $scope.customer.ImageIdCard.id != null ? $scope.customer.ImageIdCard.id : 0,
                'userId': $scope.customer.id,
                'date_created': moment($scope.today).format('YYYY/MM/DD h:mm A'),
                'imageString': JSON.stringify($scope.customer.ImageIdCard.imageString)
            }

            var d = $scope.customer.ImageIdCard;
            svcDashboardCommon.SaveImageIdCard(d).then(function (response) {
                
                $scope.customer.ImageIdCard = response.data; 
                //growl.success("Your latest ID Card Picture have been updated successfully.");
                SweetAlert.swal("Success", "Your latest ID Card Picture have been updated successfully.", "success");
                if ($scope.customer.ImageIdCard) {
                    if ($scope.customer.ImageIdCard.imageString !== null) {
                        $scope.customer.ImageIdCard.imageString = typeof $scope.customer.ImageIdCard.imageString === 'object' ? $scope.customer.ImageIdCard.imageString : JSON.parse($scope.customer.ImageIdCard.imageString);
                    }
                }
                else { $scope.customer.ImageIdCard = { 'id': 0, 'imageString': null }; }
                sessionStorage.setItem('cr', JSON.stringify($scope.customer));
                location.reload();
                $scope.lockImageBillBtn = false;
            }, function (err) {
                $scope.lockImageBillBtn = false;
                //growl.warning("Your account was not updated. Please try again saving.")
                SweetAlert.swal("Error", "Your account was not updated. Please try again saving.", "error");
            });
        }

        $scope.openRegistrationForm = function () {
            $rootScope.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/ng/view/customer/modal/newAccountForm.html',
                controller: 'CWAddNewAccountFormController',
                size: 'md',
                resolve: {
                    data: function () {
                        return data;
                    }
                }
            });
                $rootScope.modalInstance.result.then(function (response) {
                    $scope.load();
            }, function () {
            });
        };

        $scope.addNewAccount = function () {
            $scope.openRegistrationForm();
        }

        $scope.authenticated = false;
        var dataLoad;
        $scope.searchText = "";
        $scope.pageNo = 1;
        $scope.pageSize = 10;
        $scope.loadmini = function () {
     
            if ($scope.customer) {
                svcDashboardCommon.Search($scope.customer.id, $scope.customer.tid, $scope.searchText, $scope.pageNo, $scope.pageSize)
                    .then(function (response) {
                        $scope.statements = response.data.Results;
                        $scope.totalItems = response.data.Count;
                    });
            }
        }
        $scope.pageChanged = function () {
            $scope.loadmini();
        }

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $q.all([$scope.searchDefaultCurrency(), $scope.loadmini(), $scope.load()]).then(function () {  });
    }])



CooptavanzaWalletController.controller('CWAddNewAccountFormController', ['$scope', '$http', '$q', '$location', 'svcDashboardCommon', '$rootScope', '$uibModal', 'growl', '$uibModal', '$stateParams', '$uibModalInstance', 'NgTableParams', 'data', 'svcCommonServices', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, svcDashboardCommon, $rootScope, $uibModal, growl, $uibModal, $stateParams, $uibModalInstance, NgTableParams, data, svcCommonServices, alertify, SweetAlert, loader) {

        $scope.currencies = {};

        $scope.loadState = function (countryid) {
            svcDashboardCommon.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };
        
        $scope.loadAccount = function () {
            $scope.accounlist = response.data;
        }

        $scope.searchDefaultCurrency = function () {
            var d = [];
            svcDashboardCommon.searchDefaultCurrencyCustomer(data.id).then(function (response) {
                angular.forEach(response.data, function (each, index) {
                    d.push(each);
                });
                $scope.currencies = d;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.newAccount.currency = currency.iso;
            $scope.newAccount.currenycid = currency.id;
            if ($scope.newAccount.currenycid === 0 || angular.equals($scope.newAccount.currenycid, undefined)) {
                $scope.disSub = true;
            } else {
                $scope.disSub = false;
            }
        };

        $scope.isValidAccount = function () {
                $scope.disSub = true;
        }

        $scope.saveProfile = function () {
        }

        $scope.close = function () {
            try {
                $uibModalInstance.close();
            } catch (Exception) {
            };
        }

        $scope.saveAccount = function () {
            $scope.newAccount.accountDefault = false;
            $scope.newAccount.userId = data.id;
            $scope.newAccount.userTID = data.tid;
            $scope.newAccount.saveStatus = "SAVE";
            //$scope.openLoading("Enrolling Account...");
            SweetAlert.swal({ html: true, title: 'Enrolling Account...', text: loader.l3, showConfirmButton: false });
            svcDashboardCommon.saveAccount($scope.newAccount).then(function (response) {
                $scope.newAccount = response.data;
                //$uibModalInstance.close();
                //$rootScope.close();
                if (response.data != null) {
                    $scope.enrolledaccount = response.data;
                }
                //Display Object Value
        
                if (response.data != null) {
                    //$scope.openTYModal("New account successfully added");
                    if (response.data.id > 0) {
                        SweetAlert.swal("Success", "Account saved.", "success");
                    } else {
                        SweetAlert.swal("Error!", response.data, "error");
                    }
                } else {
                    SweetAlert.swal("Error!", "Error saving data.", "error");
                }
            });
        };

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $q.all([$scope.searchDefaultCurrency()]).then(function () { $scope.disSub = true; });
    }])

