﻿CooptavanzaWalletController.controller('CWRegisterController', ['$scope', '$http', '$q', '$location', 'svcHome', 'growl', '$stateParams', 'svcEmailLink', '$uibModal', '$rootScope', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, svcHome, growl, $stateParams, svcEmailLink, $uibModal, $rootScope, alertify, SweetAlert, loader) {
        /*Variables - Start*/
        $scope.register = {};
        $scope.RegisterResponseData = {};
        $scope.triggerOnClickTab = false;
        $scope.registerUserTemp = {};
        $scope.register.mobilenumber = "";
        $scope.register.email = "";
        $scope.register.password = "";
        $scope.register.confirmpassword = "";
        $scope.formats = ['dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.errMessage = "";
        /*Variables - End*/
        /*functions, Methods etc. - Start*/
        $scope.send = function () {
            if ($scope.errMessage.toString().length === 0) {
                if (document.getElementById("phone").value !== undefined) {
                    if ($scope.validateFields && $scope.register) {
                        $scope.registerUserTemp.mobile_number = document.getElementById("phone").value;
                        $scope.registerUserTemp.email = $scope.register.email;
                        $scope.registerUserTemp.password = $scope.register.password;
                        //$scope.openLoading("Creating account.");
                        swal({ html: true, title: 'Creating account...', text: loader.l3, showConfirmButton: false });
                        //growl.warning("Please wait for while. Transaction is being processed.");
                        setTimeout(function () {
                            svcHome.Register($scope.registerUserTemp).then(function (response) {
                                //$rootScope.close();
                                if (response.status === 200) {
                                    sessionStorage.setItem("rr", JSON.stringify(response.data))
                                    //growl.success("SMS Has been sent to " + response.data.mobile_number);
                                    //$scope.openTYModal("SMS Has been sent to " + response.data.mobile_number);
                                    swal("Success", "SMS Has been sent to " + response.data.mobile_number, "success");
                                    $scope.RegisterResponseData = response.data;
                                    $scope.RegisterResponseData.mobilenumber = response.mobile_number;
                                    $scope.registerUserTemp.id = response.data.id;
                                    $scope.resetTags();

                                    console.log("otp:" + response.data.OTP);
                                }
                            }, function (err) {
                                //growl.warning("Please fill the fields correctly.");
                                //$rootScope.close();
                                //$scope.errorModal("Please fill the fields correctly.");
                                swal("Error!", "Please fill the fields correctly.", "error");
                            });
                        }, 1000);
                    } else {
                        // growl.error("Please fill up all fields.");
                        //$rootScope.close();
                        swal("Error!", "Please fill the fields correctly.", "error");
                    }
                } else {
                    swal("Error!", "Please fill the fields correctly.", "error");
                }
            } else {
                //growl.error("Please change invalid fields");
                alertify.error("Please change invalid fields");
            }
        };
        //check number for phone -- need to fix on the plus sign

        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };

        $scope.clearTags = function () {
            $scope.tabActive1 = "";
            $scope.tab1 = "";
            $scope.inAction1 = "";
            $scope.toggle1 = "";

            $scope.tabActive2 = "";
            $scope.inAction2 = "";
            $scope.tab2 = "";
            $scope.toggle2 = "";

            $scope.tab3 = "";
            $scope.toggle3 = "";
        };

        $scope.assignTags = function () {
            $scope.tabActive1 = "";
            $scope.tab1 = "#wizardContent1";
            $scope.inAction1 = "tab-pane fade";
            $scope.toggle1 = "tab";

            $scope.tabActive2 = "active";
            $scope.inAction2 = "tab-pane fade in active";
            $scope.tab2 = "#wizardContent2";
            $scope.toggle2 = "tab";

            $scope.tab3 = "#wizardContent3";
            $scope.toggle3 = "tab";
        };

        $scope.resetTags = function () {
            $scope.clearTags();
            $scope.assignTags();
            $scope.triggerOnClickTab = true;
            $scope.loadTab2();
        };

        $scope.nextFromPage1 = function () {

        };

        $scope.loadTab1 = function () {
            if ($scope.triggerOnClickTab) {
                $scope.tabActive1 = "active";
                $scope.inAction1 = "tab-pane fade in active";
                $scope.toggle1 = "tab";
                $scope.tab1 = "#wizardContent1";

                $scope.tabActive2 = "";
                $scope.inAction2 = "tab-pane fade";

                $scope.tabActive3 = "";
                $scope.inAction3 = "tab-pane fade";
            }
        };

        $scope.loadTab2 = function () {
            if ($scope.triggerOnClickTab) {
                $scope.tabActive2 = "active";
                $scope.inAction2 = "tab-pane fade in active";
                $scope.toggle2 = "tab";
                $scope.tab2 = "#wizardContent2";

                $scope.tabActive1 = "";
                $scope.inAction1 = "tab-pane fade";

                $scope.tabActive3 = "";
                $scope.inAction3 = "tab-pane fade";
            }
        };

        $scope.loadTab3 = function () {
            if ($scope.triggerOnClickTab) {
                $scope.tabActive3 = "active";
                $scope.inAction3 = "tab-pane fade in active";
                $scope.toggle3 = "tab";
                $scope.tab3 = "#wizardContent3"

                $scope.tabActive1 = "";
                $scope.inAction1 = "tab-pane fade";

                $scope.tabActive2 = "";
                $scope.inAction2 = "tab-pane fade";
            }
        };

        $scope.Confirm = function () {
            if ($scope.RegisterResponseData.OTP === $scope.register.OTP) {
                $scope.authenticated = true;
                $scope.ifOTPNotSet = false;
                $scope.lockSubmit = true;
            } else {
                $scope.authenticated = false;
                $scope.lockSubmit = false;
                $scope.ifOTPNotSet = true;
            }
        };

        $scope.confirmBD = function (birthdate) {
            $scope.yearold = moment().diff(birthdate, 'years')

        }

        $scope.ResendOTP = function () {
            $scope.authenticated = false;
            svcHome.Register($scope.RegisterResponseData).then(function (response) {
                if (response.status === 200) {
                    growl.success("SMS Has been resent to " + response.data.mobile_number);
                    $scope.RegisterResponseData = response.data;
                    $scope.RegisterResponseData.OTP = response.data.OTP;
                }
            });
        };


        $scope.submit = function () {
            if ($scope.validateFields($scope.register)) {
                //$scope.openLoading("Creating Customer.");
                swal({ html: true, title: 'Creating Customer...', text: loader.l3, showConfirmButton: false });
                if ($scope.register) {
                    $scope.register.email = $scope.RegisterResponseData.email;
                    $scope.register.password = $scope.RegisterResponseData.password;
                    $scope.register.mobilenumber = $scope.RegisterResponseData.mobile_number;
                    $scope.register.countryid = 226;
                    $scope.register.stateid = 7985;
                    $scope.register.address = "";
                    //var dateTime = new Date("2015-06-17 14:24:36");
                    //dateTime = moment(dateTime).format("YYYY-MM-DD HH:mm:ss");

                    $scope.register['birthdate'] = moment($scope.register.bod).format("YYYY-MM-DD HH:mm:ss");
                    svcHome.registerCustomer($scope.register).then(function (response) {
                        //$rootScope.close();
                        if (response.status === 200) {
                            if ($stateParams.o !== null && $stateParams.id !== null && $stateParams.tk !== null) {
                                svcEmailLink.getTFA(response.data.id, $stateParams.o, $stateParams.tk, $stateParams.id).then(function (response) {

                                });
                            }

                            sessionStorage.setItem("cr", JSON.stringify(response.data.cData));
                            //growl.success("Thank you for enrolling in ewallet!");
                            window.location = "/dashboard/";
                            //$scope.openTYModal("Thank you for enrolling in ewallet!");
                            swal("Success", "Thank you for enrolling in ewallet!", "success");
                        } else {
                            //growl.warning("Please try to submit again. Thank you.");
                            //$scope.errorModal("Please try to submit again. Thank you.");
                            swal("Error!", "Please try to submit again. Thank you.", "error");
                        }
                    });
                } else {
                    //growl.error("Please Fill all field properly.");
                    //$rootScope.close();
                    //$scope.errorModal("Please Fill all field properly.");
                    swal("Error!", "Please Fill all field properly.", "error");
                }
                //$rootScope.close();
            } else {
                //$scope.errorModal("Please Fill all field properly.");
               swal("Error!", "Please Fill all field properly.", "error");
            }
        };

        $scope.checkMobileNumber = function () {
            if ($scope.register.mobilenumber !== undefined) {
                svcHome.checkMobileNumber($scope.register.mobilenumber).then(function (response) {
                    if (response.data === true) {
                        alertify.error("This mobile number is already enrolled in the E-wallet. Please sign in.");
                        $scope.errMessage = "This mobile number is already enrolled in the E-wallet. Please sign in.";
                        $scope.lockSubmit = true;
                    }
                    else { $scope.lockSubmit = false; $scope.errMessage = "";}
                });
            } else {
                $scope.lockSubmit = true;
            }
            var change = document.getElementById("phone").value;

            if (document.getElementById("phone").value[0] && document.getElementById("phone").value[0] !== "+") {
                document.getElementById("phone").value = "+" + change;
            }
        };


        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };
        
        $scope.selectedCurrency = function (currency) {
            $scope.register.currencyid = currency.id;
            $scope.register.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.register['countryid'] = country.id;
            $scope.register.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.register.stateid = state.id;
            $scope.register.state = state.name;
            $scope.loadState(state.id);
        };

        $scope.enableSubmit = function () {
            if ($scope.register.mobile_number !== "" &&
                $scope.register.email !== "" &&
                $scope.register.password !== "" &&
                $scope.register.confirmpassword !== "" &&
                ($scope.register.confirmAge) &&
                $scope.errMessage.toString().length === 0
            ) {
                $scope.lockSubmit = false;
                $scope.errMessage = "";
            } else {
                $scope.lockSubmit = true;
            }
        };

        $scope.validateFields = function (data) {
            var valid = true;
            if (data.mobilenumber === "" || data.mobilenumber === undefined) { valid = false; }
            if (data.email === "") { valid = false; }
            if (data.password === "") { valid = false; }
            if (data.confirmpassword === "") { valid = false; }
            if (!data.confirmAge) { valid = false; }
            if (data.OTP === "" || data.OTP === undefined) { valid = false; }
            return valid;
        };

        $scope.intializeWizard = function () {
            $scope.tab1 = "#wizardContent1";
            $scope.tab2 = "#";
            $scope.tab3 = "#";
            $scope.toggle1 = "tab";
            $scope.toggle2 = "#";
            $scope.toggle3 = "#";
            $scope.allowSubmit = true;
            $scope.tabActive1 = "active";
            $scope.inAction1 = "tab-pane fade in active";
            $scope.inAction2 = "tab-pane fade";
            $scope.inAction3 = "tab-pane fade";
        };

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.checkEmailValidation = function () {
            if ($scope.register.email !== undefined) {
                svcHome.checkEmailValidation($scope.register.email).then(function (response) {
                    if (response.data === true) {
                        growl.info("This Email is already enrolled in the E-wallet, you cannot use it anymore.");
                        $scope.errMessage = "This Email is already enrolled in the E-wallet. Please sign in.";
                        $scope.lockSubmit = true;
                    } else { $scope.lockSubmit = false; $scope.errMessage = ""; }
                });
            } else {
                $scope.lockSubmit = true;
            }
        };

        $q.all([$scope.intializeWizard(), $scope.searchAllCountry(), $scope.searchDefaultCurrency(), $scope.enableSubmit()]).then(function () {

        });
        /*functions, Methods etc. - End*/
    }]);

/*Scrat*/
        //$scope.checkEmail = function () {
        //    if ($scope.register.email !== undefined) {
        //        svcHome.checkEmail($scope.register.email).then(function (response) {
        //            if (response.data === true) {
        //                growl.info("This Email is already enrolled in the E-wallet, you cannot use it anymore.");
        //                $scope.lockSubmit = true;
        //            } else { $scope.lockSubmit = false;}

        //        });

        //    }
        //}


//CooptavanzaWalletController.controller('CWRegisterFormsController', ['$scope', '$http', '$q', '$location', 'svcHome', '$rootScope', 'data', 'growl', '$uibModal', function ($scope, $http, $q, $location, svcHome, $rootScope, data, growl, $uibModal) {
//    $scope.authenticated = false;
//    $scope.open1 = function () {
//        $scope.popup1.opened = true;
//    };
//    $scope.popup1 = {
//        opened: false
//    };

//    $scope.dateOptions = {
//        formatYear: 'yy',
//        maxDate: new Date(2120, 5, 22),
//        minDate: new Date(1800, 1, 1),
//        startingDay: 0
//    };

//    $scope.back = function () {
//        var backData;
//        backData = JSON.parse(sessionStorage.getItem("rr"));


//        $rootScope.modalInstance = $uibModal.open({
//            animation: true,
//            templateUrl: '/ng/view/customer/modal/register.html',
//            controller: 'CWRegisterController',
//            size: 'md',
//            resolve: {
//                backData: function () {
//                    return backData;
//                }
//            }

//        });

//        $rootScope.modalInstance.result.then(function (response) {
//            $scope.openRegistrationForm(response);
//        }, function () {
//        });

//    }

//    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
//    $scope.format = $scope.formats[0];
//    //$scope.Confirm = function () {
//    //    if (data.OTP === $scope.register.OTP) {
//    //        $scope.authenticated = true;
//    //    }
        
//    //};

//    //$scope.confirmBD = function (birthdate) {
//    //    $scope.yearold = moment().diff(birthdate, 'years')

//    //}

//    //$scope.Resend = function () {
//    //    svcHome.register($scope.RegisterResponseData).then(function (response) {
//    //        if (response.status === 200) {
//    //            //up notification
//    //            growl.success("SMS Has been resent to " + reponse.data.mobilenumber);
//    //            $scope.RegisterResponseData.OTP = response.data.OTP;
                
//    //        }
//    //    });
//    //};


//    $scope.submit = function () {
//        //if ($scope.register) {

//        //    $scope.register.email = data.email;
//        //    $scope.register.password = data.password;
//        //    $scope.register.mobilenumber = data.mobile_number;
//        //    svcHome.registerCustomer($scope.register).then(function (response) {
//        //        if (response.status === 200) {
//        //            sessionStorage.setItem("cr", JSON.stringify(response.data));
//        //            growl.success("Thank you for enrolling in ewallet!");
//        //            $rootScope.modalInstance.close(response.data);
//        //            location.reload();
//        //        } else {
//        //            growl.warning("Please try to submit again. Thank you.");
//        //        }

//        //    });


//        //} else {
//        //    growl.error("Please Fill all field properly.");
//        //}
//    };

//    $scope.searchAllCountry = function () {
//        svcHome.searchAllCountry("").then(function (response) {
//            $scope.countries = response.data;
//        });
//    };

//    $scope.searchDefaultCurrency = function () {
//        svcHome.searchDefaultCurrency().then(function (response) {
//            $scope.currencies = response.data;
//        });
//    };

//    $scope.loadState = function (countryid) {
//        svcHome.searchState(countryid).then(function (response) {
//            $scope.states = response.data;
//        });
//    };



//    $scope.selectedCurrency = function (currency) {
//        $scope.register.currencyid = currency.id;
//        $scope.register.currency = currency.iso;
//    };

//    $scope.selectedCountry = function (country) {
//        $scope.register.countryid = country.id;
//        $scope.register.country = country.name;
//        $scope.loadState(country.id);
//    };

//    $scope.selectedState = function (state) {
//        $scope.register.stateid = state.id;
//        $scope.register.state = state.name;
//    };



//    $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { });

//}])