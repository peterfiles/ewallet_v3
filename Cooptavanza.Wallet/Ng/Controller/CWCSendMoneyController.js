﻿CooptavanzaWalletController.controller('CWCSendMoneyController', ['$scope', '$http', '$q', '$location', '$uibModal', '$uibModalInstance', '$stateParams', 'svcCommonServices', 'svcMoneyConversion',
    function ($scope, $http, $q, $location, $uibModal, $uibModalInstance, $stateParams, svcCommonServices, svcMoneyConversion) {

    var SendMoney = $stateParams.SendMoney; //getting SendMoneyVal

    $scope.SendMoney = {};
    $scope.message = '';
    $scope.result = "color-default";
    $scope.isViewLoading = false;
    //get called when user submits the form  
    $scope.submitForm = function () {
        $scope.isViewLoading = true;
        $http(
            {
                method: 'POST',
                url: '/Home/SendMoney/',
                data: $scope.SendMoney
            }).success(function (data, status, headers, config) {
                $scope.errors = [];
                if (data.success === true) {
                    $scope.SendMoney = {};
                    $scope.message = 'Send Money Submitted!';
                    $scope.result = "color-green";
                    $location.path(data.redirectUrl);
                    $window.location.reload();
                }
                else {
                    $scope.errors = data.errors;
                }
            }).error(function (data, status, headers, config) {
                $scope.errors = [];
                $scope.message = 'Unexpected Error while saving data!!';
            });
        $scope.isViewLoading = false;
    }

    $scope.smsMoneyData = function () {
        svcCommonServices.getById(9).then(function (result) {
            $scope.smsMoneyData = result.data;
        });
    };

    $scope.smsMoneyClose = function () {
        $uibModalInstance.close();
    };

    $scope.smsConvertBalance = function () {
    }

    $scope.smsMoneyData();

    $scope.beneficiaryMobileNumber;
    $scope.beneficiaryData;
    $scope.convertedRate1;
    $scope.loadDetails = function () {
        svcCommonServices.getByMobileNumber($scope.beneficiaryMobileNumber).then(function (result) {
            $scope.beneficiaryData = result.data;
            svcMoneyConversion.convertCurrency($scope.smsMoneyData.currency, $scope.beneficiaryData.currency, 1).then(function (result2) {
                $scope.convertedRate1 = result2.data.rates[$scope.beneficiaryData.currency];
            });
        });
    };

    $scope.convertedRate2;
    $scope.amountToConvert = function () {
        if ($scope.beneficiaryData !== null && $scope.amntToConvert > 0) {
            svcMoneyConversion.convertCurrency($scope.smsMoneyData.currency, $scope.beneficiaryData.currency, $scope.amntToConvert).then(function (result) {
                $scope.convertedRate2 = result.data.rates[$scope.beneficiaryData.currency];
            });
        } else {
            alert("Please click Load Details!");
        }
    };
}])