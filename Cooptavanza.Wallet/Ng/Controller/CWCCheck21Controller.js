﻿CooptavanzaWalletController.controller('CWCheck21Controller', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, alertify, SweetAlert, loader) { 

    $scope.check21 = {};
    $scope.check21.abaRouting = "";
    $scope.check21.acctNum = "";
    $scope.check21.checkNum = "";
    $scope.check21.bankName = "";
    $scope.check21.checkDate = moment().format("DD MMMM YYYY");

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2120, 5, 22),
        minDate: new Date(1800, 1, 1),
        startingDay: 0
    };


    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.loadState = function (countryid) {
        svcHome.searchState(countryid).then(function (response) {
            $scope.states = response.data;
        });
    };

    $scope.searchAllCountry = function () {
        svcHome.searchAllCountry("").then(function (response) {
            $scope.countries = response.data;
        });
    };

    $scope.searchDefaultCurrency = function () {
        svcHome.searchDefaultCurrency().then(function (response) {
            $scope.currencies = response.data;
        });
    };

    $scope.selectedCurrency = function (currency) {
        $scope.check21.currencyid = currency.id;
        $scope.check21.currency = currency.iso;
    };

    $scope.selectedCountry = function (country) {
        $scope.check21['countryid'] = country.id;
        $scope.check21.country = country.name;
        $scope.loadState(country.id);
    };

    $scope.selectedState = function (state) {
        $scope.check21.stateid = state.id;
        $scope.check21.state = state.name;
    };



    $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () { })
    
}])