﻿CooptavanzaWalletController.controller('CWCWireTransferController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'svcWireTransfer', '$uibModal', '$state', '$rootScope', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, svcWireTransfer, $uibModal, $state, $rootScope, alertify, SweetAlert, loader) {

        $scope.wt = {};
        $scope.wt.checkDate = moment().format("DD MMMM YYYY");
        $scope.open1 = function () {
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            maxDate: new Date(2120, 5, 22),
            minDate: new Date(1800, 1, 1),
            startingDay: 0
        };


        $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];

        $scope.loadState = function (countryid) {
            svcHome.searchState(countryid).then(function (response) {
                $scope.states = response.data;
            });
        };

        $scope.searchAllCountry = function () {
            svcHome.searchAllCountry("").then(function (response) {
                $scope.countries = response.data;
            });
        };

        $scope.searchDefaultCurrency = function () {
            svcHome.searchDefaultCurrency().then(function (response) {
                $scope.currencies = response.data;
            });
        };

        $scope.selectedCurrency = function (currency) {
            $scope.wt.currencyid = currency.id;
            $scope.wt.currency = currency.iso;
        };

        $scope.selectedCountry = function (country) {
            $scope.wt['countryid'] = country.id;
            $scope.wt.country = country.name;
            $scope.loadState(country.id);
        };

        $scope.selectedState = function (state) {
            $scope.wt.stateid = state.id;
            $scope.wt.state = state.name;
        };

        $scope.submit = function () {
            console.log($scope.wt);
            //$scope.openLoading("Processing transfer...");
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            var extractDataFromStorage = $.parseJSON(sessionStorage.getItem("cr"));
            $scope.wt.mobilenumber = extractDataFromStorage.mobilenumber;
            $scope.wt.email = extractDataFromStorage.email;
            $scope.wt.fullname = extractDataFromStorage.firstname + " " + extractDataFromStorage.middlename + " " + extractDataFromStorage.lastname;
            svcWireTransfer.sendNotif($scope.wt).then(function (response) {
                //$rootScope.close();
                if (response !== null) {
                    SweetAlert.swal("Success", "A notification is being send to your registered email and mobile number. Thank you!", "success");
                    //$scope.openTYModal("A notification is being send to your registered email and mobile number. Thank you!");
                }
            }, function () {
                //$scope.errorModal("Transaction failed. Please Try again.");
                SweetAlert.swal("Error", "Transaction failed. Please Try again.", "error");
            });
        };

        $scope.openTYModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                size:'lg',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.errorModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/error.html',
                controller: 'CWCGlobalController',
                size: 'lg',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message: message, isloading: false };
                    }
                }
            });
        };


        $q.all([$scope.searchAllCountry(), $scope.searchDefaultCurrency()]).then(function () {  })

    
}])