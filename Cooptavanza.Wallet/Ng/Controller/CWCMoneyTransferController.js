﻿CooptavanzaWalletController.controller('CWCMoneyTransferController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcCommonServices', 'svcMoneyConversion', 'svcMinistatement', 'svcSendMoney', 'growl', 'svcHome', 'svcMoneyTransfer', '$uibModal', 'svcDashboardCommon', '$rootScope', 'alertify', 'SweetAlert', 'loader',
    function ($scope, $http, $q, $location, $stateParams, svcCommonServices, svcMoneyConversion, svcMinistatement, svcSendMoney, growl, svcHome, svcMoneyTransfer, $uibModal, svcDashboardCommon, $rootScope, alertify, SweetAlert, loader) {

    var SendMoney = $stateParams.SendMoney; //getting SendMoneyVal
    $scope.lockBtn = true;
    $scope.authenticated = false;
    $scope.notifyZeroBalance = true;
    $scope.notifyExceededLimit = false;
    $scope.warning = "Please fill all fields.";
    var data;
    $scope.etransfer = {};
    $scope.etransfer.transactionid = "";
    $scope.etransfer.amountfrom = 0;
    $scope.searchText = "";
    $scope.pageNo = 1;
    $scope.pageSize = 99999;

    $scope.currencyName;
    $scope.convertedValue;
    $scope.getConvertedBalance;

    $scope.SendEmailMoney = {};
    $scope.message = '';
    $scope.result = "color-default";
    $scope.isViewLoading = false;

    $scope.agentLocation = {};

    $scope.etransfer.benificiarymobileno = "";

    $scope.getConvertedForBenificiary;

    $scope.availableCurrencies;

    $scope.currencyValue = 0.00;
    $scope.currencyData;

    $scope.showAgentData = false;

    $scope.change = function () {
        var change = document.getElementById("mobilenumber").value;
        if (document.getElementById("mobilenumber").value[0] && document.getElementById("mobilenumber").value[0] !== "+") {
            document.getElementById("mobilenumber").value = "+" + change;
        }
    }

    //get called when user submits the form  
    $scope.submitForm = function () {
        $scope.lockBtn = true;
        $scope.isViewLoading = true;
        $scope.warning = "Still processing your transaction. Please wait.";
        //$scope.openLoading("Processing Transaction.");
        SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
        $scope.etransfer.customerid = $scope.customer.id;
        $scope.etransfer.amount = $scope.etransfer.amountto;
        $scope.etransfer.datecreated = moment();
        $scope.etransfer.benificiaryfullname = $scope.etransfer.benificiaryfirstname + " " + $scope.etransfer.benificiarylastname;
        $scope.etransfer.question = "Password";
        $scope.rctransfer = {
            'firstname': $scope.etransfer.benificiaryfirstname, 'middlename': $scope.etransfer.benificiarymiddlename,
            'lastname': $scope.etransfer.benificiarylastname, 'currency': $scope.etransfer.currencytoiso, 'expiryDate': moment($scope.etransfer.expiryDate).format('YYYY/MM/DD h:mm A'),
            'email': $scope.etransfer.benificiaryemail, 'mobilenumber': $scope.etransfer.benificiarymobileno, 'address': $scope.etransfer.benificiaryaddress,
            'username': $scope.etransfer.benificiarymobileno, 'zipcode': $scope.etransfer.benificiarypostal, 'cityid': 0, 'profileimageId': 0, 'recentbillimageId': "",
            'currencyid': 147, 'stateid': $scope.etransfer.benificiarystateid, 'countryid': $scope.etransfer.benificiarycountryid,
            'state': $scope.etransfer.benificiarystate, 'country': $scope.etransfer.benificiarycountry,
            'city': $scope.etransfer.benificiarycity,
            'agentID': $scope.agentLocation.agent
        }
        var transfer = {
            'tm': $scope.etransfer, 'rc': $scope.rctransfer
        }
        console.log($scope.etransfer.benificiarymobileno === "");
        if ($scope.etransfer.benificiarymobileno !== "") {
            svcMoneyTransfer.SendMoney(transfer).then(function (response) {
                //$rootScope.close();
                if (response.data === "success") {
                    $scope.etransfer = null;
                    //$scope.openTYModal("Thank You for giving us the opportunity to serve you.");
                    SweetAlert.swal("Success", "Successfully Processed!", "success");
                    //alertify.success("Authenticated");
                    $scope.lockBtn = false;
                    //$scope.warning = "Please Fill all fields for new transaction.";
                } else {
                    $scope.lockBtn = false;
                    $scope.warning = "Please Try again.";
                    //$scope.errorModal("Please Try again.");
                    SweetAlert.swal("Error!", "Please Try again.", "error");
                }
            }, function (err) {
                $scope.lockBtn = false;
                //$rootScope.close();
                $scope.warning = "Please Try again.";
                SweetAlert.swal("Error!", "Please Try again.", "error");
                //$scope.errorModal("Transaction failed. Please Try again.");
            });
        } else {
            //$rootScope.close();
            //$scope.errorModal("Please put Mobile Number.");
            SweetAlert.swal("Error!", "Please put Mobile Number.", "error");
        }
        $scope.isViewLoading = false;
    }
    
    
    $scope.loadDefaultValues = function () {
        svcCommonServices.LoadDefaultValues().then(function (result) {
            $scope.currencyData = result.data;
        });
    };

    $scope.setCurrencyForBenificiary = function () {
        if ($scope.etransfer.amountfrom > $scope.balancelimit) {
            $scope.notifyExceededLimit = true;
            $scope.warning = "Amount to transfer exceeded the limit of account balance left.";
            $scope.lockBtn = true;
        }
        else {
            $scope.notifyExceededLimit = false;
            $scope.lockBtn = false;
            if ($scope.etransfer.amountfrom > 0 && $scope.etransfer.currencytoiso !== undefined && $scope.etransfer.currencyfromiso !== undefined) {
                svcMoneyConversion.convertCurrency($scope.etransfer.currencyfromiso, $scope.etransfer.currencytoiso, $scope.etransfer.amountfrom).then(function (result) {
                    $scope.convertedValue = result.data.rates;
                    $scope.getConvertedForBenificiary = $scope.threeDecimal($scope.convertedValue[$scope.currencyName]);

                    if ($scope.etransfer.currencyfromiso === $scope.etransfer.currencytoiso) {
                        $scope.etransfer.amountto = $scope.etransfer.amountfrom;
                        $scope.warning = "";
                    } else {
                        $scope.etransfer.amountto = result.data.rates[$scope.etransfer.currencytoiso];
                        $scope.warning = "";
                    }
                    if ($scope.etransfer.amountto <= 0 || $scope.etransfer.amountto === undefined) {
                        $scope.lockBtn = true;
                        $scope.warning = "Amount to Received is Zero. Please fill the currency to transfer correctly.";
                    } else { $scope.lockBtn = false; }
                });
                
            }
        }
    };


   
    $scope.loadAllCurrencies = function () {
        svcMoneyConversion.getAllAvailableCurrency().then(function (result){
            $scope.availableCurrencies = result.data.rates;
            $scope.conversionRateFrom = $scope.customer.currency;
        });
    };
    
    $scope.convertCurrency = function (from, to) {
        if (from === undefined) {
            from = $scope.customer.currency;
        }
        svcMoneyConversion.convertCurrency(from, to,1).then(function (result) {
            $scope.currencyValue = result.data.rates;
        });     
    };
    
    $scope.threeDecimal = function (num) {
        return num;
    };

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2120, 5, 22),
        minDate: new Date(1800, 1, 1),
        startingDay: 0
    };

    $scope.formats = ['dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];


    $scope.selectedCountry = function (country) {
        $scope.etransfer.benificiarycountryid = country.id;
        $scope.etransfer['benificiarycountry'] = country.Name;
       // $scope.loadState(country.id);
        $scope.searchStateByCountryIDWithAgent(country.id);
    };

    $scope.selectedState = function (state) {
        $scope.etransfer.benificiarystateid = state.id;
        $scope.etransfer['benificiarystate'] = state.Name;
    };

    $scope.searchStateByCountryIDWithAgent = function (countryid) {
        svcCommonServices.getStateFilterByCountryAndAgent(countryid).then(function (response) {
            $scope.states = response.data;
            console.log(response);
        });
    };

    $scope.searchCountryWithAgent = function () {
        svcCommonServices.getCountryFilterByAgent().then(function (response) {
            $scope.countries = response.data;
        });
    };

    $scope.load = function () {
        if ($scope.customer) {
            svcDashboardCommon.getEnrolledListByUserId($scope.customer.id).then(function (res) {
                $scope.customer.EnrolledList = res.data;
                svcDashboardCommon.getBalance($scope.customer.id).then(function (resp) {
                    $scope.enrolledBalance = resp.data;
                    angular.forEach($scope.customer.EnrolledList, function (each, index) {
                        each['balance'] = 0;
                        angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                            if (eachEB.currency === each.currency) {
                                each.balance = eachEB.balance;
                            }
                        });
                    });
                });
            });
        }
    }

    $scope.selectCurrencyFrom = function (currency) {
        var found = false;
        angular.forEach($scope.customer.EnrolledList, function (each, index) {
            if (each.currency === currency) {
                if (each.balance <= 0) {
                    $scope.lockBtn = true;
                    $scope.balancelimit = 0;
                    $scope.notifyZeroBalance = true;
                    $scope.etransfer.amountfrom = 0;
                    $scope.etransfer.amountto = 0;
                    $scope.warning = "Zero balance left.";

                } else {
                    $scope.lockBtn = false;
                    $scope.notifyZeroBalance = false;
                    $scope.balancelimit = each.balance;
                    $scope.warning = "";
                    found = true;

                }
            } else if (each.currency !== currency && found === false) {
                $scope.lockBtn = true;
                $scope.notifyZeroBalance = true;
                $scope.balancelimit = 0;
                $scope.warning = "No Account enrolled or Zero Balance Left for this currency.";
            }
        });
    }


    $scope.openTYModal = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/success.html',
            controller: 'CWCGlobalController',
            size: 'md',
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: false };
                }
            }
        });
    };

    $scope.openLoading = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/loading.html',
            controller: 'CWCGlobalController',
            backdrop: 'static',
            size: 'sm',
            keyboard: false,
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: true };
                }
            }
        });
    };

    $scope.errorModal = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/error.html',
            controller: 'CWCGlobalController',
            size: 'md',
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: false };
                }
            }
        });
    };

    $q.all([$scope.load(), $scope.loadAllCurrencies(), $scope.searchCountryWithAgent(), $scope.loadDefaultValues()]).then(function () { $scope.showAgentData = false;});
}])