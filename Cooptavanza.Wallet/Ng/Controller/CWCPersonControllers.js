﻿CooptavanzaWalletController.controller('CWPersonController', ['$scope', '$http', '$q', '$location', 'svcPerson',
    function ($scope, $http, $q, $location, svcPerson) {
    svcPerson.LoadPerson().then(function (r) {
        $scope.Persons = r.data.Results;
    });
    $scope.query = '';

}]);

CooptavanzaWalletController.controller('CWPersonListController', ['$scope', '$http', '$q', '$location', 'svcPerson', '$filter', 'growl', '$uibModal', '$window', '$stateParams',
    function ($scope, $http, $q, $location, svcPerson, $filter, growl, $uibModal, $window, $stateParams) {

    $scope.PageNo = 1;
    $scope.PageSize = 10;
    $scope.list = [];
    $scope.order('Name', true);
    $scope.formData = {};

    $scope.load = function () {
        svcPerson.Search($scope.searchText, $scope.PageNo, $scope.PageSize).then(function (r) {
            $scope.list = r.Results;
            $scope.count = r.Count;
            if ($scope.count > 0) { $scope.messageAlert = false; } else { $scope.messageAlert = true; }
        });
    }

    $scope.pageChanged = function () {
        console.log($scope.currentPage);
    }

    if ($scope.searchText === undefined) {
        $scope.searchText = ''
    }

    $scope.order = function (predicate) {
        $scope.predicate = predicate;
        $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
    };
 

    $scope.deleteModal = function (id) {
        var modal = $uibModal.open({
            animation: true,
            templateUrl: '/ng/views/common/modal/delete.html',
            controller: 'CooptavanzaWalletPersonDeleteModalController',
            resolve: {
                PersonId: function () {
                    return id;
                }
            }
        });

        modal.result.then(function () {
            $scope.load()
        }, function () {
        });
    }

    $scope.Delete = function (id) {
        $scope.deleteModal(id);
    }

    $scope.getById = function (Id) {
        svcPerson.getById(Id).then(function (r) {
            $scope.formData = r;
        }, function (error) {
        });
    }


    $scope.pageChanged = function () {
        $scope.load();
    }

    $scope.load();
}]);
CooptavanzaWalletController.controller('CWPersonDetailsController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcPerson', 'growl',
    function ($scope, $http, $q, $location, $stateParams, svcPerson, growl) {
    $scope.Person = {};

    $scope.load = function (Id) {
        svcPerson.getById(Id).then(function (r) {
            $scope.Person = r;
        });
    }

    if ($stateParams.id > 0) {
        $scope.load($stateParams.id);
    }

}]);
CooptavanzaWalletController.controller('CWPersonEditController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcPerson', 'growl',
    function ($scope, $http, $q, $location, $stateParams, svcPerson, growl) {
    $scope.Person = {};
    $scope.Categories = {};
    $scope.Category = function () {
        svcPerson.LoadCategory().then(function (r) {
            $scope.Categories = r;
            if ($stateParams.id > 0) {
                $scope.load($stateParams.id);
            }
        });
    }

    $scope.load = function (Id) {
        svcPerson.getById(Id).then(function (r) {
            $scope.Person = r;
        });
    }
    $scope.Save = function () {
        svcPerson.Save($scope.Person).then(function (r) {
           growl.success('Data Saved Successfully.')
            if ($stateParams.id === 0) {
                $location.path("/Person/" + r.Id);
            }

        }, function (error) {
        });
    }
     $scope.Category();

}]);
CooptavanzaWalletController.controller('CWPersonFormController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcPerson', 'growl',
    function ($scope, $http, $q, $location, $stateParams, svcPerson, growl) {
    $scope.Person = {};

    $scope.load = function (Id) {
        svcPerson.getById(Id).then(function (r) {
            $scope.Person = r;
        });
    }
    $scope.Save = function () {
        svcPerson.Save($scope.Person).then(function (r) {
            growl.success('Data Saved Successfully.')
            if ($stateParams.id ===  0) {
                $location.path("/Person/" + r.Id);
            }
        }, function (error) {

        });
    }

    if ($stateParams.id > 0) {
        $scope.load($stateParams.id);
    }

}]);
CooptavanzaWalletController.controller('CWPersonDeleteModalController', ['$scope', '$http', '$q', '$location', '$filter', 'svcPerson', '$stateParams', 'growl', 'PersonId', '$uibModalInstance',
    function ($scope, $http, $q, $location, $filter, svcPerson, $stateParams, growl, PersonId, $uibModalInstance) {

    $scope.id = PersonId;
    $scope.message = {
        title: "Confirm Delete",
        description: "Are you sure to delete this Person?"
    }

    $scope.dismiss = function () {
        $uibModalInstance.close();
    }

    $scope.delete = function () {
        svcPerson.Delete($scope.id).then(function (r) {
            $uibModalInstance.close();
            growl.success('Deleted Successfully.')
        })
    }
}]);