﻿CooptavanzaWalletController.controller('CWForgotPasswordController', ['$scope', '$http', '$q', 'svcHome', 'svcMinistatement', 'growl',
    function ($scope, $http, $q,  svcHome, svcMinistatement, growl) {
        $scope.forgetPass = {}
        $scope.forgetPass.email = "";
      
        $scope.submitForgetPass = function () {
            console.log($scope.forgetPass);
            if (($scope.forgetPass.email !== "") ){
                svcHome.forgetPassword($scope.forgetPass).then(function (response) {
                    if (response.status === 200) {
                        if (response.data) {
                            growl.success("Please check your email at ");
                            sessionStorage.setItem("cr", JSON.stringify(response.data));
                            window.location = "~/../../Signin/";
                            //location.reload();
                        } else {
                            
                            growl.warning("Please enter your email address");
                        }

                    } else if (status === 500) {
                        growl.warning("Please enter your email address");
                    }
                }, function (err) {
                    growl.warning("Please Try again.");
                    });
            } else {
                growl.warning("Please enter your email address");
            }
        };



    }])

CooptavanzaWalletController.controller('CWResetPasswordController', ['$scope', '$http', '$q', 'svcHome', 'svcMinistatement', 'growl', '$stateParams',
    function ($scope, $http, $q, svcHome, svcMinistatement, growl, $stateParams) {

        $scope.reset = {};

        $scope.resetFunc = function () {
            var newp = $scope.password === $scope.confirmpassword ? "match" : "unmatch";
        };

        $scope.submitForgetPass = function () {
            if ($scope.password === $scope.confirmpassword) {
                if ($stateParams.id > 0) {
                    $scope.reset.id = $stateParams.id;
                    $scope.reset.tID = $stateParams.tk;
                    $scope.reset.password = $scope.password;
                    svcHome.resetPassword($scope.reset).then(function (response) {
                        if (response.status === 200) {
                            if (response.data) {
                                growl.success("You Successfully updated your password.");
                                window.location = "~/Signin/";
                            } else {
                            }

                        } else if (status === 500) {
                        }
                    }, function (err) {
                        });

                    window.location = "~/Signin/";
                }
            } else {
                growl.warning("Please match password.");
                return false;
            }
        }
    }])