﻿CooptavanzaWalletController.controller('CWLoginController', ['$scope', '$http', '$q', 'svcHome', 'svcMinistatement', 'growl', '$rootScope', '$uibModal', 'alertify','SweetAlert','loader',
    function ($scope, $http, $q, svcHome, svcMinistatement, growl, $rootScope, $uibModal,alertify, SweetAlert, loader) {
        $scope.login = {}
        $scope.login.password = "";
        $scope.login.mobilenumber = "";
     
        $scope.change = function () {
            var change = document.getElementById("mobilenumber").value;
            if (document.getElementById("mobilenumber").value[0] && document.getElementById("mobilenumber").value[0] !== "+") {
                document.getElementById("mobilenumber").value = "+" + change;
            }
        }

        $scope.pressEnter = function (event)
        {
            if (event.charCode === 13) {
                $scope.submitLogin();
            }
        }

        $scope.submitLogin = function () {
            SweetAlert.swal({ html: true, title: 'Authenticating', text: loader.l3, showConfirmButton: false });
          
            //$scope.openLoading("Loggin In...."); 
            if (($scope.login.mobilenumber !== "" || $scope.login.mobilenumber !== undefined) && $scope.password !== ""){
                svcHome.login($scope.login).then(function (response) {
                   // $rootScope.close();
                    if (response.status === 200) {
                        if (response.data !== null) {
                            if (response.data.id > 0) {
                                SweetAlert.swal("Authenticated", "Welcome Back!!!", "success");
                                alertify.success("Authenticated");
                                alertify.success("Welcome Back " + response.data.firstname + " " + response.data.lastname + " !");
                                //   growl.success("Welcome Back " + response.data.firstname + " " + response.data.lastname + " !");
                                sessionStorage.setItem("cr", JSON.stringify(response.data));
                                if (response.data.themeid > 0) {
                                    document.cookie = "skin_color=" + response.data.theme + "; expires=Session; path=/Dashboard;";
                                }
                                window.location = "~/../Dashboard/";
                            } else {
                                SweetAlert.swal("Authentication Failed", "User name and password are incorrect.", "error");
                                //growl.warning("Please enter correct mobile number or password!");
                                alertify.error("Authentication Failed");
                            }
                        }
                        else {
                            SweetAlert.swal("Authentication Failed", "User name and password are incorrect.", "error");
                            alertify.error("Authentication Failed");
                        }
                    } else if (status === 500) {
                        SweetAlert.swal("Authentication Failed", "User name and password are incorrect.", "error");
                        alertify.error("Authentication Failed");
                        ///growl.warning("Please enter correct mobile number or password!");
                    }
                }, function (err) {
                  //  $rootScope.close();
                    //growl.warning("Please Try again.");
                    SweetAlert.swal("Authentication Failed", "Please Try Again", "error");
                    alertify.error("Authentication Failed");
                    });
            } else {
                SweetAlert.swal("Authentication Failed", "User name and password are incorrect.", "error");
                alertify.error("Authentication Failed");
                //growl.warning("Please enter correct mobile number or password!");
            }
        };

        $scope.openLoading = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/loading.html',
                controller: 'CWCGlobalController',
                backdrop: 'static',
                size: 'sm',
                keyboard: false,
                resolve: {
                    data: function () {
                        return { message: message, isloading: true };
                    }
                }
            });
        };

        $scope.openTyModal = function (message) {
            $rootScope = $uibModal.open({
                templateUrl: '/ng/view/common/modal/success.html',
                controller: 'CWCGlobalController',
                size: 'md',
                resolve: {
                    data: function () {
                        return $scope.contentMessage = { message : message, isloading : false };
                    }
                }
            });
        };
       
        
        
    }])