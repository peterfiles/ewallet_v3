﻿CooptavanzaWalletController.controller('CWC-ACHController', ['$scope', '$http', '$q', '$location', '$stateParams', 'svcHome', 'NgTableParams', 'svcCommonServices', '$uibModal', '$state', '$rootScope', 'svcACH', 'alertify', 'SweetAlert', 'loader', 'svcMoneyConversion', 'svcDashboardCommon',
    function ($scope, $http, $q, $location, $stateParams, svcHome, NgTableParams, svcCommonServices, $uibModal, $state, $rootScope, svcACH, alertify, SweetAlert, loader, svcMoneyConversion, svcDashboardCommon) {

    $scope.ach = {};
    //$scope.ach.abaRouting = "";
    //$scope.ach.acctNum = "";
    //$scope.ach.checkNum = "";
    $scope.ach.bankName = "";
    $scope.ach.rtcode = "";
    $scope.bank = {};
    $scope.transferIn = "";
    $scope.ach.checkDate = moment();
    $scope.showSubmit = true;

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        maxDate: new Date(2120, 5, 22),
        minDate: new Date(1800, 1, 1),
        startingDay: 0
    };


    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.loadState = function (countryid) {
        svcHome.searchState(countryid).then(function (response) {
            $scope.states = response.data;
        });
    };

    $scope.searchDefaultCurrency = function () {
        svcHome.searchDefaultCurrency().then(function (response) {
            $scope.currencies = response.data;
        });
    };

    $scope.selectedCurrency = function (currency) {
        $scope.ach.currencyid = currency.id;
        $scope.ach.currency = currency.iso;
    };

    $scope.getAllBankDepositCountry = function () {
       
        SweetAlert.swal({ html: true, title: 'Loading Content...', text: loader.l2, showConfirmButton: false });
        svcACH.getBankDepositAllCountry().then(function (response, status) {
            $scope.countries = response.data;
            swal.close()
        });
    };

    $scope.defaultAddress = function () {
        if ($scope.ach.isDefaultAddress) {
            document.getElementById("divNewAddress").style.display = "none";
        } else {
            document.getElementById("divNewAddress").style.display = "";
        }
    };

    $scope.load = function () {
        if ($scope.customer) {
            svcDashboardCommon.getEnrolledListByUserId($scope.customer.id).then(function (res) {
                $scope.customer.EnrolledList = res.data;
                svcDashboardCommon.getBalance($scope.customer.id).then(function (resp) {
                    $scope.enrolledBalance = resp.data;
                    angular.forEach($scope.customer.EnrolledList, function (each, index) {
                        each['balance'] = 0;
                        angular.forEach($scope.enrolledBalance, function (eachEB, index) {
                            if (eachEB.currency === each.currency) {
                                each.balance = eachEB.balance;
                            }
                        });
                    });
                });
            });
        }
    }

    $scope.submit = function () {
        var extractCustomerDetails = $.parseJSON(sessionStorage.getItem("cr"));
        //$scope.openLoading("Processing Transaction...");
        $scope.ach.sendersEmail = extractCustomerDetails.email;
        $scope.ach.fullname = extractCustomerDetails.firstname + " " + extractCustomerDetails.lastname;
        $scope.ach.mobilenumber = extractCustomerDetails.mobilenumber;
        $scope.ach.bankName = $scope.bank.bankName;
        $scope.ach.userID = extractCustomerDetails.id;
        $scope.ach.ach_receive_name = $scope.ach.ach_receive_name;
        console.log($scope.isValid($scope.ach));
        if ($scope.isValid($scope.ach)) {
            //console.log($scope.ach);
            SweetAlert.swal({ html: true, title: 'Processing Transaction...', text: loader.l3, showConfirmButton: false });
            svcACH.achTransact($scope.ach).then(function (response) {
                if (response.data === "success") {
                    SweetAlert.swal("Success", "Successfully Processed!", "success");
                    $scope.ach = {};
                } else {
                    SweetAlert.swal("Error", "Please Try Again!", "error");
                }
                });
        } else {
            SweetAlert.swal("Warning", "Please fill-up all fields.", "warning");
        }
    } 

    $scope.isValid = function (data) {
        var a = true;
        angular.forEach(data, function (val, key) {
            if (val === null) {
                a = false;
            }
            if (val === "") {
                a = false;
            }
            if (val === undefined) {
                a = false;
            }
        });
        return a;
    }

    $scope.loadAllCurrencies = function () {
        svcMoneyConversion.getAllAvailableCurrency().then(function (result) {
            console.log(result.data.rates,"available curr")
            $scope.availableCurrencies = result.data.rates;
            $scope.conversionRateFrom = $scope.customer.currency;
        });
    };

    $scope.selectCurrencyFrom = function (currency) {
        var found = false;
        angular.forEach($scope.customer.EnrolledList, function (each, index) {
            if (each.currency === currency) {
                if (each.balance <= 0) {
                    $scope.lockBtn = true;
                    $scope.balancelimit = 0;
                    $scope.notifyZeroBalance = true;
                    $scope.startNotif = false;
                    $scope.ach.amountfrom = 0;
                    $scope.ach.amountto = 0;
                    $scope.showSubmit = false;

                } else {
                    $scope.lockBtn = false;
                    $scope.notifyZeroBalance = false;
                    $scope.startNotif = false;
                    $scope.balancelimit = each.balance;
                    found = true;

                }
            } else if (each.currency !== currency && found === false) {
                $scope.lockBtn = true;
                $scope.notifyZeroBalance = true;
                $scope.startNotif = false;
                $scope.balancelimit = 0;
                $scope.showSubmit = false;
            }
        });
    };

    $scope.setCurrencyForBenificiary = function () {
        if ($scope.ach.amount_purchase > $scope.balancelimit) {
            $scope.notifyExceededLimit = true;
            $scope.showSubmit = false;
        }
        else {
            //$scope.ach.currencytoiso = JSON.parse(JSON.stringify($scope.ach.currencytoiso));
            //console.log($scope.ach.currencytoiso.currenciesISO);
            var extracted = JSON.parse($scope.ach.currencytoiso);
            $scope.transferIn = extracted.currenciesISO;
            $scope.notifyExceededLimit = false;
            if ($scope.ach.amount_purchase > 0 && extracted.currenciesISO !== undefined && $scope.ach.currencyfromiso !== undefined) {
                svcMoneyConversion.convertCurrency($scope.ach.currencyfromiso, extracted.currenciesISO, $scope.ach.amount_purchase).then(function (result) {
                    $scope.convertedValue = result.data.rates;
                    $scope.getConvertedForBenificiary = $scope.convertedValue[$scope.currencyName];

                    $scope.showSubmit = true;
                    if ($scope.ach.currencyfromiso === extracted.currenciesISO) {
                        $scope.ach.amountto = $scope.ach.amount_purchase;
                    } else {
                        $scope.ach.amountto = result.data.rates[extracted.currenciesISO];
                    }
                });
            }
        }
    };

    $scope.openTYModal = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/success.html',
            controller: 'CWCGlobalController',
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: false };
                }
            }
        });
    };

    $scope.openLoading = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/loading.html',
            controller: 'CWCGlobalController',
            backdrop: 'static',
            size: 'sm',
            keyboard: false,
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: true };
                }
            }
        });
    };

    $scope.errorModal = function (message) {
        $rootScope = $uibModal.open({
            templateUrl: '/ng/view/common/modal/error.html',
            controller: 'CWCGlobalController',
            size: 'lg',
            resolve: {
                data: function () {
                    return $scope.contentMessage = { message: message, isloading: false };
                }
            }
        });
    };

    $scope.cID = 0;
    $scope.dropdownBank = function () {
        angular.forEach($scope.countries, function (val, keys) {
            var extractData = JSON.parse($scope.ach.bankCountry);
            if (val.countryid === extractData.countryid) {
                $scope.cID = val.countryid;
                svcACH.getBankNameByCountryID(val.countryid).then(function (res, status) {
                    $scope.bankNameData = res.data;
                    
                });
            }
        });
    };

    

    $scope.dropdownCurrency = function (data) {
        
        $scope.ach.rtcode = data.rtcode;
        svcACH.getBankNameByCountryIDAndbankName($scope.cID, data.bankName).then(function (res, status) {
            $scope.currencies = res.data;
        });
    };

    $q.all([$scope.getAllBankDepositCountry(), $scope.searchDefaultCurrency(), $scope.loadAllCurrencies(), $scope.load()]).then(function () {
        $scope.ach.isDefaultAddress = true;})
    
    }])


