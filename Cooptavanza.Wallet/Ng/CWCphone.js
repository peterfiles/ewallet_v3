﻿'use strict';

angular.module("ngIntlTelInput", []), angular.module("ngIntlTelInput").provider("ngIntlTelInput", function () {
    var a = this,
        b = {},
        c = function (a) {
            if ("object" == typeof a)
                for (var c in a) b[c] = a[c]
        };
    a.set = c, a.$get = ["$log", function (c) { return Object.create(a, { init: { value: function (a) { window.intlTelInputUtils || c.warn("intlTelInputUtils is not defined. Formatting and validation will not work."), a.intlTelInput(b) } } }) }]
}), angular.module("ngIntlTelInput").directive("ngIntlTelInput", ["ngIntlTelInput", "$log", "$window", "$parse", function (a, b, c, d) {
    return {
        restrict: "A",
        require: "ngModel",
        link: function (e, f, g, h) {
            function i(a) {
                var b = d(a),
                    c = b.assign;
                c(e, f.intlTelInput("getSelectedCountryData"))
            }

            function j() { i(g.selectedCountry) }

            function k() { angular.element(c).off("countrychange", j) }
            return g.type && "text" !== g.type && "tel" !== g.type || "INPUT" !== f[0].tagName ? void b.warn("ng-intl-tel-input can only be applied to a *text* or *tel* input") : (g.initialCountry && a.set({ initialCountry: g.initialCountry }), a.init(f), g.selectedCountry && (i(g.selectedCountry), angular.element(c).on("countrychange", j), e.$on("$destroy", k)), h.$validators.ngIntlTelInput = function (a) { return a || f[0].value.length > 0 ? f.intlTelInput("isValidNumber") : !0 }, h.$parsers.push(function (a) { return f.intlTelInput("getNumber") }), void h.$formatters.push(function (a) { return a && ("+" !== a.charAt(0) && (a = "+" + a), f.intlTelInput("setNumber", a)), a }))
        }
    }
}]);