﻿CooptavanzaWalletService.factory('svcLunex', ['$rootScope', '$http', '$q', 'growl', function ($rootScope, $http, $q, growl) {
    $this = {
        getSkuBillPayment: function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../LEP/getSKU'
                //url: 'http://coop-ewallet.azurewebsites.net/LEP/getSKU'
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        submitBillPayment: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../lep/bp/neworder',
                //url: 'http://coop-ewallet.azurewebsites.net/lep/bp/neworder',
                data:data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getSkuTopUpProducts: function () {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            //url: '~/../../LEP/topup/GetAlltopUPProductsNameWithSkU'
            url: 'http://74.122.128.215/coopwallet.com/LEP/topup/GetAlltopUPProductsNameWithSkU'
            //url: 'http://coop-ewallet.azurewebsites.net/LEP/topup/GetAlltopUPProductsNameWithSkU'
        }).then(function successcallback(data, status) {
            deferred.resolve(data);
        }, function errorcallback(data, status) {
            deferred.reject(data);
        });
        return deferred.promise;
    },
    getProductDataBySku: function (id) {
        var deferred = $q.defer();
        $http({
            method: 'GET',
            //url: '~/../../LEP/topup/GetProductDataBySku/' + id
            url: 'http://74.122.128.215/coopwallet.com/LEP/topup/GetProductDataBySku/' + id
            //url: 'http://coop-ewallet.azurewebsites.net/LEP/topup/GetProductDataBySku/' + id
        }).then(function successcallback(data, status) {
            deferred.resolve(data);
        }, function errorcallback(data, status) {
            deferred.reject(data);
        });
        return deferred.promise;
    },
    processTopUp: function (data) {
        console.log("from services");
        console.log(data);
        var deferred = $q.defer();
        $http({
            method: 'POST',
            //url: '~/../../LEP/topup/processTopUp/',
             //url: '~/../../LEP/topup/processTopUp/?accountEnrolledID=' + data.accountEnrolledID
            url: 'http://74.122.128.215/coopwallet.com/LEP/topup/processTopUp/?accountEnrolledID='
            //url: 'http://coop-ewallet.azurewebsites.net/LEP/topup/processTopUp/?accountEnrolledID=' + data.accountEnrolledID
            + '&phone=' + data.phone
            + '&username=' + data.username
            + '&password=' + data.password
            + '&amount=' + data.amount
            + '&skuid=' + data.skuid
            + '&curr=' + data.curr
            + '&amountTodeductInEnrolledAccount=' + data.amountTodeductInEnrolledAccount,
            data: data
           
        }).then(function successcallback(data, status) {
            deferred.resolve(data);
        }, function errorcallback(data, status) {
            deferred.reject(data);
        });
        return deferred.promise;
    },
       
    };
return $this;
}]);