﻿CooptavanzaWalletService.factory('svcBillPayment', ['$rootScope', '$http', '$q', 'growl', function ($rootScope, $http, $q, growl) {
    $this = {
        getBillerType: function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../BillerType'
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getBillerCountry: function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../Biller/GetBillerCountry'
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getBillerTypeByCountry: function (cID) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../Biller/GetBillerTypeByCountry/?id=' + cID
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                }, function errorcallback(data, status) {
                    deferred.reject(data);
                });
            return deferred.promise;
        },
        getBillerByCountryAndType: function (cID, bTID) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../Biller/GetBillerByCountryAndType/?id=' + cID + '&bTID=' + bTID
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                }, function errorcallback(data, status) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
    return $this;
}]);