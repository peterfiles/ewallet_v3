﻿CooptavanzaWalletService.factory('svcPerson', function ($rootScope, $http, $q) {
    $this = {
        Search: function (searchText, PageNo, PageSize) {
            var deferred = $q.defer();
            $http.get('~/../../Person?SearchText=' + searchText + '&PageNo=' + PageNo + '&PageSize=' + PageSize).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        Save: function (Postdata) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../Person/Save',
                data: Postdata
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            },function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getById: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../Person/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            },function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        }, Delete: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: '~/../../Person/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        LoadPerson: function () {
            var defered = $q.defer();
            $http.get('~/../../Person/').then(function successcallback(data) {
                defered.resolve(data);
            },function errorcallback(xhr) { defered.reject(xhr) });
            return defered.promise;
        },
        LoadCategory: function () {
            var defered = $q.defer();
            $http.get('~/../../Category/').then(function successcallback(data) {
                defered.resolve(data.Results);
            },function errorcallback(xhr) { defered.reject(xhr) });
            return defered.promise;
        }

    };
    return $this;
});