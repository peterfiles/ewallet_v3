﻿CooptavanzaWalletService.factory('svcDashboardCommon', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        GetCustomerById: function (id) {
            var deferred = $q.defer();
            $http.get('../Customer/' + id).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },

        SaveImageBill: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '../ImageBill/Save',
                data: data
            }).then(function successcallback(data) {
                deferred.resolve(data);
            }, function errorcallback(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        },
        SaveImageProfile: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '../ImageProfile/Save',
                data: data
            }).then(function successcallback(data) {
                deferred.resolve(data);
            }, function errorcallback(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        },
        SaveImageIdCard: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '../ImageIdCard/Save',
                data: data
            }).then(function successcallback(data) {
                deferred.resolve(data);
            }, function errorcallback(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        },
        SaveImageIdCardDetailsOnly: function (data) {
            var deferred = $q.defer();
            //$http({
            //    method: 'POST',
            //    url: '../ImageIdCard/Save',
            //    data: data
            //}).
            $http.get('../ImageIdCard/SaveDetails?ed=' + data.ed + '&icn=' + data.idCardNo + '&ict=' + data.idCardType + '&id=' + data.id)
                 .then(function successcallback(data) {
                 deferred.resolve(data);
            }, function errorcallback(err) {
                deferred.reject(err);
            });
            return deferred.promise;
        },
        SaveCustomer: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '../Customer/Save',
                data: data
            }).then(function successcallback(data) {
                deferred.resolve(data);
            }, function errorcallback(err) {
                deferred.reject(err);
                });
            return deferred.promise;
        },
        Register: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '../Register',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            },function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
         getCountries: function (id) {
            var deferred = $q.defer();
            $http.get('../../Country/'+ id).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
         getCountryById: function (id) {
             var deferred = $q.defer();
             $http.get('../../Country/' + id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
        searchAllCountry: function (item) {
            var deferred = $q.defer();
            $http.get('../../Country/Search/?SearchText=' + item).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
         },
        searchState: function (countryid) {
            var deferred = $q.defer();
            $http.get('../../States/country/?id=' + countryid).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchDefaultCurrency: function () {
            var deferred = $q.defer();
            $http.get('../../Currency/GetDefaultValueList').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchDefaultCurrencyCustomer: function (id) {
            var deferred = $q.defer();
            $http.get('../../Currency/GetDefaultValueListCustomer/'+id).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchDefaultCurrencyAdminDashboard: function () {
            var deferred = $q.defer();
            $http.get('../../Currency/GetDefaultValueList').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        }, 
      
      
         registerCustomer: function (data) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '../Customer/Save',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
                 });
             return deferred.promise;
        },
         sendSms: function (data) {
             var deferred = $q.defer();
             $http.get('../sendsms/?id').then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         }, 
         registerCustomerFromAdmin: function (Postdata) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '../Customer/adminsave',
                 data: Postdata
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
         login: function (data) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '../Customer/login',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
         saveAccount: function (data) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '../EnrolledAccount/',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },

         getBalance: function (id) {
             var deferred = $q.defer();
            $http.get('../balance/'+id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },

         getBalancePending: function (id) {
             var deferred = $q.defer();
             $http.get('../balancep/' + id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },

         getBalanceOverall: function (id) {
             var deferred = $q.defer();
             $http.get('../balanceoa/' + id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         }, 
         getEnrolledListByUserId: function (id) {
             var deferred = $q.defer();
             $http.get('../EnrolledAccount/ui/' + id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
         Search: function (id, TID, searchText, PageNo, PageSize) {
             var deferred = $q.defer();
             $http.get('../EnrolledAccount/' + id + '?TID=' + TID + '&SearchText=' + searchText + '&PageNo=' + PageNo + '&PageSize=' + PageSize)
                 .then(function successcallback(data, status) {
                     deferred.resolve(data);

                 }, function errorcallback(data, status) {

                     deferred.reject(data);
                 });
             return deferred.promise;
         },
         passwordCheck: function (mobileNumber, password) {
             var def = $q.defer();
             $http.get('../Customer/passwordCheck/?mobileNumber=' + mobileNumber + '&password=' + password).then(function successcallback(data, status) {
                 def.resolve(data);
             }, function errorcallback(data, status) {
                 def.resolve(data);
                 });
             return def.promise;
         }
    };
    return $this;
}]);