﻿CooptavanzaWalletService.factory('svcEmailLink', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        getCustomerByEmail: function (email) {
            var deferred = $q.defer();
            $http.post('~/../../rec/', email).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getTFA: function (i, o, tk, id) {
            var deferred = $q.defer();
            $http.get('~/../../TFA/?tk=' + tk + '&id=' + id+'&o='+o+'&ui='+i).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getTF: function (tk, id, p) {
            var deferred = $q.defer();
            $http.get('~/../../TF/?tk=' + tk + '&id='+id + '&p='+p).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        answer: function (data) {
            var deferred = $q.defer();
            $http({
                method:'POST', url:'~/../../answer', data:data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        
    };
    return $this;
}]);