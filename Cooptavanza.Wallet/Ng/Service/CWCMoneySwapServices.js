﻿CooptavanzaWalletService.factory('svcMoneySwap', ['$rootScope', '$http', '$q', 'growl', function ($rootScope, $http, $q, growl) {
    $this = {
        getMEXPredifined: function () {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../MEX'
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        validate: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ms/validate',
                data:data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        remit: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ms/remit',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        qoute: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ms/qoute',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getBalance: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'get',
                url: '~/../../ms/balance'
               
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        SendMoney: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ms/sendmoney',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        }
    };
    return $this;
}]);