﻿CooptavanzaWalletService.factory('svcMoneyTransfer', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        getPart: function (data) {
            var deferred = $q.defer();
            $http.get('~/../../Customer/mobilenumber/?str=' + data).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        SendMoney: function (data) {
            var deferred = $q.defer();
            $http(
                {
                    method: 'POST',
                    url: '~/../../mt/',
                    data: data
                }).then(function successcallback(data, status) {
                    deferred.resolve(data);
                }, function errorcallback(data) {
                    deferred.reject(data);
                });
            return deferred.promise;
        }
    };
    return $this;
}]);