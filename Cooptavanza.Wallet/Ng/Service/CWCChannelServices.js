﻿CooptavanzaWalletService.factory('svcChannel', function ($rootScope, $http, $q) {
    $this = {
        Search: function (CompanyId, searchText, PageNo, PageSize) {
            var deferred = $q.defer();
            $http.get('~/../../Channel?CompanyId=' + CompanyId + 'SearchText=' + searchText + '&PageNo=' + PageNo + '&PageSize=' + PageSize)
                .then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data;
                $this.count = data.length;
            }, function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        },
        Save: function (Postdata) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../Channel/Save',
                data: Postdata
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data;
                $this.count = data.length;
            },function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getById: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../Channel/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data.Results;
                $this.count = data.Count;
            },function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        }, Delete: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: '~/../../Channel/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data.Results;
                $this.count = data.Count;
            },function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        }
    };
    return $this;
});