﻿CooptavanzaWalletService.factory('svcLoadin', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
      
        Search: function (id,TID, searchText, PageNo, PageSize) {
            var deferred = $q.defer();
            $http.get('~/../../Transaction/?id=' + id + '&TID=' +TID + '&SearchText=' + searchText + '&PageNo=' + PageNo + '&PageSize=' + PageSize)
                .then(function successcallback(data, status) {
                    deferred.resolve(data);
                  
                }, function errorcallback(data, status) {
                  
                    deferred.reject(data);
                });
            return deferred.promise;
        },
         getCountryById: function (id) {
             var deferred = $q.defer();
             $http.get('~/../../Country/' + id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
        searchAllCountry: function (item) {
            var deferred = $q.defer();
            $http.get('~/../../Country/Search/?SearchText=' + item).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
         },
        searchState: function (countryid) {
            var deferred = $q.defer();
            $http.get('~/../../States/country/?id=' + countryid).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchDefaultCurrency: function () {
            var deferred = $q.defer();
            $http.get('~/../../Currency/GetDefaultValueList').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
       
         registerCustomer: function (data) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '~/../../Customer/Save',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
                 });
             return deferred.promise;
        },
         sendSms: function (data) {
             var deferred = $q.defer();
             $http.get('~/../../sendsms/?id').then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         }, 
         registerCustomerFromAdmin: function (Postdata) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '~/../../Customer/adminsave',
                 data: Postdata
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
         loadin: function (data) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '~/../../LoadinDummy',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         }
    };
    return $this;
}]);