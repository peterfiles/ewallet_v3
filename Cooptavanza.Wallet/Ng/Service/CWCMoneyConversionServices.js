﻿CooptavanzaWalletService.factory('svcMoneyConversion', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        getAllAvailableCurrency: function () {
            var deferred = $q.defer();
            $http.get('https://api.fixer.io/latest?base=AUD&symbols=GBP,CAD,COP,EUR,MXN,PHP,USD').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        convertCurrency: function (from, to, amount) {
            var deferred = $q.defer();
            $http.get('https://api.fixer.io/latest?from='+from+'&to='+to+'&amount='+amount).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        }
    };
    return $this;
}]);