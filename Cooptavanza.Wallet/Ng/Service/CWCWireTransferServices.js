﻿CooptavanzaWalletService.factory('svcWireTransfer', ['$rootScope', '$http', '$q', 'growl', function ($rootScope, $http, $q, growl) {
    $this = {
        sendmoney: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ms/sendmoney',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        sendNotif: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../Wiretransfer/SendNotif',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },

    };
    return $this;
}]);