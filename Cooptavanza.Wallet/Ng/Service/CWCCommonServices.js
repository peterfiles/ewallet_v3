﻿CooptavanzaWalletService.factory('svcCommonServices', ['$rootScope', '$http', '$q', 'growl', function ($rootScope, $http, $q, growl) {
    $this = {
        getById: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../SendEmailMoneyGetCustomerData/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        }, getPartners: function () {
            var deferred = $q.defer();
            $http.get('~/../../Partners').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        LoadDefaultValues: function () {
            var def = $q.defer();
            $http.get('~/../../Currency/GetDefaultValueList').then(function successcallback(data) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        getByMobileNumber: function (mobileNumber) {
            var def = $q.defer();
            $http.get('~/../../Customer/GetCustomerByMobileNumber/' + mobileNumber).then(function successcallback(data) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        getInfoByMobileNumber: function (data) {
            var def = $q.defer();
            $http.post('~/../../Customer/mobileNo/', data).then(function successcallback(data) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        getInfoByEmail: function (data) {
            var def = $q.defer();
            $http.post('~/../../Customer/email/', data).then(function successcallback(data) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        testingachemail: function (data) {
            var def = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../smsEmailNotification/',
                data: data
            }).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
            });
            return def.promise;
        },
        customerIfIsInOFAC: function (Postdata) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../Customer/CustomerIfIsInOFAC',
                data: Postdata
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        initializeTaxTransactionCodeAndTransctionFee: function (amount) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ProcessTransaction/initializeTaxTransactionCodeAndTransctionFee',
                data: amount
            }).then(function successcallback(data) {
                deferred.resolve(data);
                console.log(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
                console.log("Please try again.");
            });
            return deferred.promise;
        },
        processCreditCard: function (postData) {
            console.log("@Services");
            var def = $q.defer();
            console.log(postData);
            $http({
                method: 'POST',
                url: '~/../../ProcessTransaction/ProcessPayment',
                data: postData
            }).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! But there's an error trying to process your Cash in.");
            });
            return def.promise;
        },
        validateCustomerBasicInformation: function (id) {
            var def = $q.defer();
            $http.get('~/../../ProcessTransaction/ValidateCustomerProfile/' + id).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! But there's an error trying to process your Cash in.");
            });
            return def.promise;
        },
        getAgentByCountryIDAndStateID: function (cID, sID) {
            var def = $q.defer();
            $http.get('~/../../Agents/getAgentByCountryIDAndStateID/?cID=' + cID + '&sID=' + sID).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! But there's an error trying to process your Cash in.");
            });
            return def.promise;
        },
        getCountryFilterByAgent: function () {
            var def = $q.defer();
            $http.get('~/../../Country/getCountryFilterByAgent').then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! Kindly Refresh the website.");
            });
            return def.promise;
        },
        getStateFilterByCountryAndAgent: function (id) {
            var def = $q.defer();
            $http.get('~/../../States/getStateFilterByCountryAndAgent/?id=' + id).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! Kindly Refresh the website.");
            });
            return def.promise;
        },
        getConvertedAmount: function (fc, tc, amnt) {
            var def = $q.defer();
            //$http.get('./Currency/ConvertMoney?fc=' + fc + '&tc=' + tc + '&a=' + amnt).then(function successcallback(data, status) {
            $http.get('~/../../Currency/ConvertMoney?fc=' + fc + '&tc=' + tc + '&a=' + amnt).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! Kindly Refresh the website.");
                });
            return def.promise;
        },
        getCountries: function () {
            var def = $q.defer();
            $http.get('~/../../Country/GetAll').then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject(data);
                growl.error("Sorry for inconvenience! Kindly Refresh the website.");
                });
            return def.promise;
        }
    };
    return $this;
}]);