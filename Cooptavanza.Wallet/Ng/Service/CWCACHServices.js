﻿CooptavanzaWalletService.factory('svcACH', ['$rootScope', '$http', '$q', 'growl', function ($rootScope, $http, $q, growl) {
    $this = {
        sendmoney: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../ms/sendmoney',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getBankDepositAllCountry: function () {
            var def = $q.defer();
            $http({
                method: 'GET',
                url:'~/../../BankDeposit/GetCountry'
            }).then(function successcallback(data, status) {
                    def.resolve(data);
                }, function errorcallback(data, status) {
                    def.reject();
                });
            return def.promise;
        },
        getBankNameByCountryID: function (id) {
            var def = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../BankDeposit/GetBankNameByCountryID?countryID=' + id 
            }).then(function successcallback(data, status) {
                    def.resolve(data);
                }, function errorcallback(data, status) {
                    def.reject();
                });
            return def.promise;
        },
        getBankNameByCountryIDAndbankName: function (id,bankName) {
            var def = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../BankDeposit/GetCurrencyByCountryIDAndBankName?countryID=' + id + '&bankName=' + bankName
            }).then(function successcallback(data, status) {
                def.resolve(data);
            }, function errorcallback(data, status) {
                def.reject();
            });
            return def.promise;
        },
        achTransact: function (data) {
            var def = $q.defer();
            //console.log(data);
            $http({
                method: 'POST',
                url: '~/../../ACH/Transact',
                data:data
            }).then(function successcallback(data, status) {
                    def.resolve(data);
                }, function errorcallback(data, status) {
                    def.reject();
                });
            return def.promise;
        }
    };
    return $this;
}]);