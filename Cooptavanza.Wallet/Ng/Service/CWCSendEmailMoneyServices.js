﻿CooptavanzaWalletService.factory('svcSendEmailMoney', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        SendEmailMoney: function (data) {
            var deferred = $q.defer();
            $http(
                {
                    method: 'POST',
                    url: '~/../../sendemail/',
                    data: data
                }).then(function successcallback(data, status) {
                    deferred.resolve(data);
                   
                },function errorcallback(data) {
                    deferred.reject(data);
                    $scope.errors = [];
                    $scope.message = 'Unexpected Error while saving data!!';
                });
            return deferred.promise;
        },
        SendSmsMoney: function (data) {
            var deferred = $q.defer();
            $http(
                {
                    method: 'POST',
                    url: '~/../../sendsms/',
                    data: data
                }).then(function successcallback(data, status) {
                    deferred.resolve(data);

                }, function errorcallback(data) {
                    deferred.reject(data);
                    $scope.errors = [];
                    $scope.message = 'Unexpected Error while saving data!!';
                });
            return deferred.promise;
        },

        Search: function (searchText, PageNo, PageSize) {
            var deferred = $q.defer();
            $http.get('~/../../SendEmailMoney?SearchText=' + searchText + '&PageNo=' + PageNo + '&PageSize=' + PageSize).then(function successcallback(data, status) {
                deferred.resolve(data);
               
            }, function errorcallback(data, status) {
              
                deferred.reject(data);
            });
            return deferred.promise;
        },
        Save: function (Postdata) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../SendEmailMoney/Save',
                data: Postdata
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
              
            },function errorcallback(data, status) {
               
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getById: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../SendEmailMoney/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
             
            },function errorcallback(data, status) {
              
                deferred.reject(data);
            });
            return deferred.promise;
        }, Delete: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: '~/../../SendEmailMoney/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
               
            }, function errorcallback(data, status) {
               
                deferred.reject(data);
            });
            return deferred.promise;
        },
        LoadPerson: function () {
            var defered = $q.defer();
            $http.get('~/../../SendEmailMoney/').then(function successcallback(data) {
                defered.resolve(data);
            },function errorcallback(xhr) { defered.reject(xhr) });
            return defered.promise;
        },
        LoadCategory: function () {
            var defered = $q.defer();
            $http.get('~/../../Category/').then(function successcallback(data) {
                defered.resolve(data.Results);
            },function errorcallback(xhr) { defered.reject(xhr) });
            return defered.promise;
        }
    };
    return $this;
}]);