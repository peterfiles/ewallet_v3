﻿CooptavanzaWalletService.factory('svcSendMoney', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        SendMoney: function (data) {
            var deferred = $q.defer();
            $http(
                {
                    method: 'POST',
                    url: '~/../../sendmoney/',
                    data: data
                }).then(function successcallback(data, status) {
                    deferred.resolve(data);

                }, function errorcallback(data) {
                    deferred.reject(data);
                    
                    //$scope.message = 'Unexpected Error while saving data!!';
                });
            return deferred.promise;
        },
        Search: function (searchText, PageNo, PageSize) {
            var deferred = $q.defer();
            $http.get('~/../../SendMoney?SearchText=' + searchText + '&PageNo=' + PageNo + '&PageSize=' + PageSize).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data;
                $this.count = data.length;
            }, function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        },
        Save: function (Postdata) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../SendMoney/Save',
                data: Postdata
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data;
                $this.count = data.length;
            },function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getById: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: '~/../../SendMoney/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data.Results;
                $this.count = data.Count;
            },function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        }, Delete: function (Id) {
            var deferred = $q.defer();
            $http({
                method: 'DELETE',
                url: '~/../../SendMoney/' + Id
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
                $this.objects = data.Results;
                $this.count = data.Count;
            }, function errorcallback(data, status) {
                $this.objectes = [];
                $this.count = 0;
                deferred.reject(data);
            });
            return deferred.promise;
        },
        LoadPerson: function () {
            var defered = $q.defer();
            $http.get('~/../../SendMoney/').then(function successcallback(data) {
                defered.resolve(data);
            },function errorcallback(xhr) { defered.reject(xhr) });
            return defered.promise;
        },
        LoadCategory: function () {
            var defered = $q.defer();
            $http.get('~/../../Category/').then(function successcallback(data) {
                defered.resolve(data.Results);
            },function errorcallback(xhr) { defered.reject(xhr) });
            return defered.promise;
        }

    };
    return $this;
}]);