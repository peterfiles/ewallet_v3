﻿CooptavanzaWalletService.factory('svcHome', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {
    $this = {
        getThemeByUserId: function (id) {
            var deferred = $q.defer();
            $http.get('~/../../UserTheme/u/' + id
            ).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getProfileByUserId: function (id) {
            var deferred = $q.defer();
            $http.get('~/../../GetProfileByUI/'+id          
            ).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        forgetPassword: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../forgetPassword',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        resetPassword: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../reset',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        saveTheme: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../UserTheme/Save',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        getCustomerById: function (id) {
            var deferred = $q.defer();
            $http.get('~/../../Customer/' + id).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        Register: function (data) {
            var deferred = $q.defer();
            $http({
                method: 'POST',
                url: '~/../../Register',
                data: data
            }).then(function successcallback(data, status) {
                deferred.resolve(data);
            },function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
         getCountries: function (id) {
            var deferred = $q.defer();
            $http.get('~/../../Country/'+ id).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
         getCountryById: function (id) {
             var deferred = $q.defer();
             $http.get('~/../../Country/' + id).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
        searchAllCountry: function (item) {
            var deferred = $q.defer();
            $http.get('~/../../Country/Search/?SearchText=' + item).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
         },
        searchState: function (countryid) {
            var deferred = $q.defer();
            $http.get('~/../../States/country/?id=' + countryid).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchStatedashboard: function (countryid) {
            var deferred = $q.defer();
            $http.get('~/../../States/country/?id=' + countryid).then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchDefaultCurrency: function () {
            var deferred = $q.defer();
            $http.get('~/../../Currency/GetDefaultValueList').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
        searchDefaultCurrencyAdminDashboard: function () {
            var deferred = $q.defer();
            $http.get('~/../../Currency/GetDefaultValueList').then(function successcallback(data, status) {
                deferred.resolve(data);
            }, function errorcallback(data, status) {
                deferred.reject(data);
            });
            return deferred.promise;
        },
       
        registerCustomer: function (data) {
            console.log(data);
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '~/../../Customer/Save',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
                 });
             return deferred.promise;
        },
         sendSms: function (data) {
             var deferred = $q.defer();
             $http.get('~/../../sendsms/?id').then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         }, 
         registerCustomerFromAdmin: function (Postdata) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '~/../../Customer/adminsave',
                 data: Postdata
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
         login: function (data) {
             var deferred = $q.defer();
             $http({
                 method: 'POST',
                 url: '~/../Customer/login',
                 data: data
             }).then(function successcallback(data, status) {
                 deferred.resolve(data);
                 console.log(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
                 console.log(data);
             });
             return deferred.promise;
         },
         checkMobileNumber: function (data) {
             var deferred = $q.defer();
             $http.get('~/../../Customer/mobilenumber/?str='+data).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         },
         checkEmailValidation: function (data) {
             var deferred = $q.defer();
             $http.get('~/../../Customer/email/?str=' + data).then(function successcallback(data, status) {
                 deferred.resolve(data);
             }, function errorcallback(data, status) {
                 deferred.reject(data);
             });
             return deferred.promise;
         }
    };
    return $this;
}]);