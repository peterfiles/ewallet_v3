﻿//Globalize.culture("da-DK");
//moment.lang('da-dk', {
//    week: {
//        dow: 3, // Wednesday is the first day of the week
//        doy: 4
//    }
//});
var app = angular.module('CW', ['ui.router', 'ui.bootstrap', 'angular-growl', 'ngIntlTelInput', 'CW.controller', 'CW.service', 'CW.directive', 'ngTable', 'base64', 'naif.base64', 'ui.mask', 'ngFileUpload', 'ngAlertify', 'oitozero.ngSweetAlert'])//'oitozero.ngSweetAlert', 'ng-sweet-alert'
app.run(['$rootScope', '$location', function ($rootScope, $location) {

}]);
app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /
    $urlRouterProvider.otherwise("/")
    $stateProvider
        .state('openMoneySwap',
        {
            url: '/moneySwap',
            templateUrl: "../ng/view/home/openMoneySwapRemit.html",
            controller: "CWMoneySwapRemitController"
        })
        .state('home',
        {
            url: '/',
            templateUrl: "../ng/view/home/dashboard.html",
            controller: "CWDashboardController"
        })
        .state('etransfer',
        {
            url: '/etransfer',
            templateUrl: "../ng/view/home/openETransfer.html",
            controller: "CWCETransferController"
        })
        .state('moneyTransfer',
        {
            url: '/moneytransfer',
            templateUrl: "../ng/view/home/openMoneyTransfer.html",
            controller: "CWCMoneyTransferController"
        })
        .state('wiretransfer',
        {
            url: '/wiretransfer',
            templateUrl: "../ng/view/home/wiretransfer.html",
            controller: "CWCWireTransferController"
        })
        .state('check21',
        {
            url: '/check21',
            templateUrl: "../ng/view/home/check-21.html",
            controller: "CWCheck21Controller"
        })
        .state('creditcard',
        {
            url: '/creditcard',
            templateUrl: "../ng/view/home/credit-card.html",
            controller: "CWCCreditCardController"
        })
        .state('eft',
        {
            url: '/eft',
            templateUrl: "../ng/view/home/eft.html",
            controller: "CWCEFTController"
        })
        .state('cashlocation',
        {
            url: '/cashlocation',
            templateUrl: "../ng/view/home/cash-location.html",
            controller: "CWCCashLocationController"
        })
        .state('coopmember',
        {
            url: '/coopmember',
            templateUrl: "../ng/view/home/coop-member.html",
            controller: "CWCCoopMemberController"
        })
        .state('otc',
        {
            url: '/otc',
            templateUrl: "../ng/view/home/otc.html",
            controller: "CWCOTCController"
        })
        .state('unionpaycard',
        {
            url: '/unionpaycard',
            templateUrl: "../ng/view/home/unionpay-card.html",
            controller: "CWCUnionPayController"
        })
        .state('ach',
        {
            url: '/ach',
            templateUrl: "../ng/view/home/ach.html",
            controller: "CWC-ACHController"
        })
        .state('propertycard',
        {
            url: '/propertycard',
            templateUrl: "../ng/view/home/property-card.html",
            controller: "CWCPropertyCardController"
        })

        .state('billpayment',
        {
            url: '/billpayment',
            templateUrl: "../ng/view/home/bill-payment.html",
            controller: "CWCBillPaymentController"
        })

        .state('topupairtime',
        {
            url: '/topupairtime',
            templateUrl: "../ng/view/home/topup-airtime.html",
            controller: "CWCTopupAirtimeController"
        })

        .state('login',
        {
            url: '/login',
            templateUrl: "../ng/view/home/login.html",
            controller: "CWLoginController"
        })
        .state('register',
        {
            url: '/register',
            templateUrl: "../ng/view/home/register.html",
            controller: "CWRegisterController"
        })
        .state('index',
        {
            url: '/index',
            templateUrl: "../ng/view/home/index.html",
            controller: "CWHomeController"
        })
        .state('dashboard',
        {
            url: '/dashboard',
            templateUrl: "../ng/view/home/dashboard.html",
            controller: "CWDashboardController"
        })
    
      //  })
      //  .state('openSendMoney',
      //  {
      //      url: '/send-money',
      //      templateUrl: "../ng/view/home/openSendMoney.html",
      //      controller: "CWCSendMoneyController"
      //  })
        .state('openMiniStatement',
        {
            url: '/openMiniStatement',
            templateUrl: "../ng/view/home/openMiniStatement.html",
            controller: "CWCMinistatementController"
        })
      //  .state('openLoadWallet',
      //  {
      //      url: '/load-wallet',
      //      templateUrl: "../ng/view/home/openLoadWallet.html",
      //      controller: "CWCLoadWalletController"
      //  })
      //  .state('openPayoutWallet',
      //  {
      //      url: '/payout-wallet',
      //      templateUrl: "../ng/view/home/openPayoutWallet.html",
      //      controller: "CWCPayoutWalletController"
      //  })
      //  .state('openBillPayment',
      //  {
      //      url: '/bill-payment',
      //      templateUrl: "../ng/view/home/openBillPayment.html",
      //      controller: "CWCBillPaymentController"
      //  })
        .state('openProfile',
        {
            url: '/profile',
            templateUrl: "../ng/view/home/openProfile.html",
            controller: "CWProfileController"
        })
        .state('emaillink',
        {
            url: '/confirmation/:emailid&:emailtk',
            templateUrl: "../ng/view/customer/emaillink.html",
            controller: "CWCEmailLinkController"
        })
        .state('terms',
        {
            url: '/terms',
            templateUrl: "../ng/view/home/terms.html",
            controller: "CWCTermsController"
        })
        .state('privacy',
        {
            url: '/privacy',
            templateUrl: "../ng/view/home/privacy.html",
            controller: "CWCTermsController"
        })
        .state('check21serv',
        {
            url: '/check21serv',
            templateUrl: "/ng/view/services/check21serv.html",
            controller: "CWCChek21servController"
        })
        
}]);

app.config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.globalDisableCountDown(true);
}]);
app.constant('config', {
    id: 'EwallET30072017',
    key: 'ewallet@VANZA',
    designation: 'ewallet'
});
app.constant('loader', {
    l1: '<div class="load-wrapp"><div class="load-1" ><div class="line"></div><div class="line"></div><div class="line"></div></div><div><p><strong>Please Wait...</strong></p></div></div>',
    l2: '<div class="load-wrapp"><div class="load-4 col-md-12"><div class="ring-1"></div></div></div><div class="col-md-12"><p><strong>Please Wait...</strong></p></div>',
    l3: '<div class="load-wrapp"><div class="load-3"><div class="line"></div><div class="line"></div><div class="line"></div></div><div><p><strong>Please Wait...</strong></p></div></div>',
    
});
app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.directive('highlightonfocus', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on("focus", function () {
                $(this).val('');
            });
        }
    };
})
app.controller('AppMainController', ['$scope', '$http', '$q', '$location', function ($scope, $http, $q, $location) {
    with ((console && console._commandLineAPI) || {}) {
        console.log("%cWarning!", "font: 2em sans-serif; color: yellow; background-color: red;");
        console.log("%cThis is a browser feature intended for developers.If someone told you to copy- paste something here to enable a feature or \"hack\" someone's account, it is a scam and will give them access to your account.", "font: 2em sans-serif;")
    }
    //$scope.session = { customer: {}, isAuthenticated: false, group: '', freshgroup: '', loading: false };
    //$scope.formatDate = function (s) {
    //    var date;
    //    try {
    //        if (s.getMonth) {
    //            date = s;
    //        } else {
    //            date = Globalize.parseDate(s, "yyyy-MM-ddTHH:mm:ss"); //try parsing with this format
    //            if (Object.prototype.toString.call(date) !== "[object Date]") {
    //                date = new Date(parseInt(s.substr(6)));
    //            }
    //        }
    //        return Globalize.format(date, "d");
    //    } catch (err) {
    //        return '';
    //    }
    //}
    //$scope.formatNumber = function (n) {
    //    var date = Globalize.format(n, "n2");
    //    return date;
    //}

    //$scope.init = function (isAuthenticated) {
    //    $scope.session.isAuthenticated = isAuthenticated;
    //    if (!$scope.session.isAuthenticated) {
    //        $location.path("/login");
    //    }
    //}
    //console.log("appMain run");



}]);