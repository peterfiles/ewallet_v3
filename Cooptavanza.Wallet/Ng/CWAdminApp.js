﻿//Globalize.culture("da-DK");
//moment.lang('da-dk', {
//    week: {
//        dow: 3, // Wednesday is the first day of the week
//        doy: 4
//    }
//});
var app = angular.module('CWAdmin', ['ui.router', 'ui.bootstrap', 'angular-growl', 'CW.controller', 'CW.service', 'CW.directive', 'ngAlertify','oitozero.ngSweetAlert'])
app.run(['$rootScope', '$location', function ($rootScope, $location) {

}]);
app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /
    $urlRouterProvider.otherwise("/")
    $stateProvider
       
        .state('dashboard',
        {
            url: '/',
            templateUrl: "ng/view/admin/index.html"
          
        })
       
       
}]);

app.config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.globalDisableCountDown(true);
}]);
app.constant('config', {
    id: 'EwallET30072017',
    key: 'ewallet@VANZA',
    designation: 'ewallet'
});
app.directive('highlightonfocus', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on("focus", function () {
                $(this).val('');
            });
        }
    };
})
app.controller('AppMainController', ['$scope', '$http', '$q', '$location', function ($scope, $http, $q, $location) {
    with ((console && console._commandLineAPI) || {}) {
        console.log("%cWarning!", "font: 2em sans-serif; color: yellow; background-color: red;");
        console.log("%cThis is a browser feature intended for developers.If someone told you to copy- paste something here to enable a feature or \"hack\" someone's account, it is a scam and will give them access to your account.", "font: 2em sans-serif;")
    }
    //$scope.session = { customer: {}, isAuthenticated: false, group: '', freshgroup: '', loading: false };
    //$scope.formatDate = function (s) {
    //    var date;
    //    try {
    //        if (s.getMonth) {
    //            date = s;
    //        } else {
    //            date = Globalize.parseDate(s, "yyyy-MM-ddTHH:mm:ss"); //try parsing with this format
    //            if (Object.prototype.toString.call(date) !== "[object Date]") {
    //                date = new Date(parseInt(s.substr(6)));
    //            }
    //        }
    //        return Globalize.format(date, "d");
    //    } catch (err) {
    //        return '';
    //    }
    //}
    //$scope.formatNumber = function (n) {
    //    var date = Globalize.format(n, "n2");
    //    return date;
    //}

    //$scope.init = function (isAuthenticated) {
    //    $scope.session.isAuthenticated = isAuthenticated;
    //    if (!$scope.session.isAuthenticated) {
    //        $location.path("/login");
    //    }
    //}




}]);