﻿//Globalize.culture("da-DK");
//moment.lang('da-dk', {
//    week: {
//        dow: 3, // Wednesday is the first day of the week
//        doy: 4
//    }
//});
var app = angular.module('CWRegister', ['ui.router', 'ui.bootstrap', 'angular-growl', 'ngIntlTelInput', 'CW.controller', 'CW.service', 'CW.directive', 'ngTable', 'base64', 'naif.base64', 'ui.mask', 'ngAlertify', 'oitozero.ngSweetAlert'])
app.run(['$rootScope', '$location', function ($rootScope, $location) {

}]);
app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /
    $urlRouterProvider.otherwise("/")
    $stateProvider
       
        .state('register',
        {
            url: '/:o&:tk&:id',
            templateUrl: "../ng/view/home/register.html"//,
            //controller: "CWRegisterController"
        })
     
       
}]);

app.config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.globalDisableCountDown(true);
}]);

app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.directive('highlightonfocus', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on("focus", function () {
                $(this).val('');
            });
        }
    };
});
app.constant('loader', {
    l1: '<div class="load-wrapp"><div class="load-1" ><div class="line"></div><div class="line"></div><div class="line"></div></div><div><p><strong>Please Wait...</strong></p></div></div>',
    l2: '<div class="load-wrapp">' + '<div class="load-10">' + '<div class="bar"></div></div><div><p><strong>Please Wait...</strong></p></div>' + ' </div>',
    l3: '<div class="load-wrapp"><div class="load-3"><div class="line"></div><div class="line"></div><div class="line"></div></div><div><p><strong>Please Wait...</strong></p></div></div>',

});
app.controller('AppMainController', ['$scope', '$http', '$q', '$location', function ($scope, $http, $q, $location) {

    $scope.session = { customer: {}, isAuthenticated: false, group: '', freshgroup: '', loading: false };
    $scope.formatDate = function (s) {
        var date;
        try {
            if (s.getMonth) {
                date = s;
            } else {
                date = Globalize.parseDate(s, "yyyy-MM-ddTHH:mm:ss"); //try parsing with this format
                if (Object.prototype.toString.call(date) !== "[object Date]") {
                    date = new Date(parseInt(s.substr(6)));
                }
            }
            return Globalize.format(date, "d");
        } catch (err) {
            return '';
        }
    }
    $scope.formatNumber = function (n) {
        var date = Globalize.format(n, "n2");
        return date;
    }

    $scope.init = function (isAuthenticated) {
        $scope.session.isAuthenticated = isAuthenticated;
        if (!$scope.session.isAuthenticated) {
            $location.path("/login");
        }
    }
    console.log("appMain run");



}]);