﻿//Globalize.culture("da-DK");
//moment.lang('da-dk', {
//    week: {
//        dow: 3, // Wednesday is the first day of the week
//        doy: 4
//    }
//});
var app = angular.module('CWReset', ['ui.router', 'ui.bootstrap', 'angular-growl','ngIntlTelInput', 'CW.controller', 'CW.service', 'CW.directive'])
app.run(['$rootScope', '$location', function ($rootScope, $location) {

}]);
app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /
    $urlRouterProvider.otherwise("/")
    $stateProvider
       
        .state('reset',
        {
            url: '/?id=:idn&tk=:tkn'//,
          //  templateUrl: "/ng/view/customer/emaillink.html",
          //  controller: "CWCEmailLinkController"
        })
     
       
}]);

app.config(['growlProvider', function (growlProvider) {
    growlProvider.globalTimeToLive(5000);
    growlProvider.globalDisableCountDown(true);
}]);

app.filter('capitalize', function () {
    return function (input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
app.directive('highlightonfocus', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on("focus", function () {
                $(this).val('');
            });
        }
    };
})
app.controller('AppMainController', ['$scope', '$http', '$q', '$location', function ($scope, $http, $q, $location) {

    $scope.session = { customer: {}, isAuthenticated: false, group: '', freshgroup: '', loading: false };
    $scope.formatDate = function (s) {
        var date;
        try {
            if (s.getMonth) {
                date = s;
            } else {
                date = Globalize.parseDate(s, "yyyy-MM-ddTHH:mm:ss"); //try parsing with this format
                if (Object.prototype.toString.call(date) !== "[object Date]") {
                    date = new Date(parseInt(s.substr(6)));
                }
            }
            return Globalize.format(date, "d");
        } catch (err) {
            return '';
        }
    }
    $scope.formatNumber = function (n) {
        var date = Globalize.format(n, "n2");
        return date;
    }

    $scope.init = function (isAuthenticated) {
        $scope.session.isAuthenticated = isAuthenticated;
        if (!$scope.session.isAuthenticated) {
            $location.path("/login");
        }
    }
    console.log("appMain run");



}]);