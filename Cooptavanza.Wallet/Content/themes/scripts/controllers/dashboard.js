'use strict';

function dashboardCtrl($scope, $interval, COLORS, $http, Journals) {

    var vm = this;
    var months = [
        'Jan',
        'Feb',
        'Mar',
        'Apr',
        'May',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Oct',
        'Nov',
        'Dec',
    ]
    $scope.dashboardDataset = [];
    var forDashboard = [
        [months[0], 0],
        [months[1], 0],
        [months[2], 0],
        [months[3], 0],
        [months[4], 0],
        [months[5], 0],
        [months[6], 0],
        [months[7], 0],
        [months[8], 0],
        [months[9], 0],
        [months[10], 0],
        [months[11], 0]
    ];

    $scope.dashboardDataset = [{
        data: forDashboard,
        color: '#fff'
    }];

    $scope.annualTotal = 0;

    $scope.figures = {
        daily: 0,
        weekly: 0,
        monthly: 0,
        yearly: 0
    }



    function getDashbordData(data, id) {
        var filteredData = [];
        var dashboardData = [];
        var barChartData = [];
        var annualTotal = 0;
        var result = [];

        // Income(id = 4), Expense(id=5)

        for (var i = 0; i < months.length; i++) {
            filteredData[i] = Enumerable.From(data)
                .Where("$.acc_type_cla_id == " + id + " && $.month_name == '" + months[i] + "'")
                .OrderBy("$.acct_name")
                .GroupBy("$.acct_name", null,
                    function(key, g) {
                        return {
                            acct_name: key,
                            entry_credit: g.Sum("$.entry_credit"),
                            entry_debit: g.Sum("$.entry_debit"),
                            account_type_cla_name: g.account_type_cla_name
                        }
                    })
                .ToArray();


            // Get the sum of entry_debit for each month
            var monthlyTotal = 0;

            // Expense
            if (id == 5) {
                for (var j = 0; j < filteredData[i].length; j++) {
                    // console.log(filteredData[i][j].entry_debit);
                    monthlyTotal = monthlyTotal + filteredData[i][j].entry_debit;

                }
            } else {
                for (var j = 0; j < filteredData[i].length; j++) {
                    // console.log(filteredData[i][j].entry_debit);
                    monthlyTotal = monthlyTotal + filteredData[i][j].entry_credit;

                }
            }


            annualTotal = annualTotal + monthlyTotal;

            // Dashboard - Income or Expense
            dashboardData[i] = [months[i], monthlyTotal]

            // Bar Chart - Income or Expense
            barChartData[i] = [i, monthlyTotal];

        }

        // console.clear();
        var currentMonth = new Date().getMonth(); // Get current month
        var currentMonthTotal = barChartData[currentMonth] // Get the total for the current month

        result = [dashboardData, barChartData, annualTotal, currentMonthTotal[1]]

        return (result);
    }



    function init() {
        var data = [];

        Journals.getByYear().success(function(data, status) {
            vm.yearlyData = data;

            // Get annual expense data
            var class_no = 5; // Expense
            var expenseResult = getDashbordData(vm.yearlyData, class_no);
            var expenseForDashboard = expenseResult[0]; // Dataset for dashboard
            var expenseForBarChart = expenseResult[1]; // Dataset for barhart
            var annualExpense = expenseResult[2]; //
            var monthlyExpense = expenseResult[3];

            // Get annual income data
            class_no = 4; // Income
            var incomeResult = getDashbordData(vm.yearlyData, class_no)
            var incomeForDashBoard = incomeResult[0];
            var incomeForBarChart = incomeResult[1];
            var annualSales = incomeResult[2];
            var monthlyIncome = incomeResult[3];


            console.log(incomeForBarChart);


            // Initialize dashboard
            initDashboardData(expenseForDashboard, expenseForBarChart, annualExpense, monthlyExpense, incomeForDashBoard, incomeForBarChart, annualSales, monthlyIncome);

        });


    }

    init();


    function initDashboardData(expenseForDashboard, expenseForBarChart, annualExpense, monthlyExpense, incomeForDashBoard, incomeForBarChart, annualSales, monthlyIncome) {

        // 1. Initialize Dashboard
        $scope.dashboardDataset = [{
            data: incomeForDashBoard,
            color: '#fff'
        }];

        // 2. Initialize Bar Chart
        $scope.barODataset = [{
                label: "Income",
                data: incomeForBarChart,
                color: COLORS.success,
                order: 1,
                bars: {
                    // align: "left",
                    barWidth: 0.2

                }

            },
            {
                label: "Expense",
                data: expenseForBarChart,
                color: COLORS.danger,
                order: 2,
                bars: {
                    align: "right",
                    barWidth: 0.2
                },


            }
        ];

        // 3. Initialize annual total Expense/Income
        $scope.annualTotal = annualSales; // Income

        // 4. Monthly total
        $scope.figures.monthly = monthlyIncome;
    }






    var ticks = [
        [0, "Jan"],
        [1, "Feb"],
        [2, "Mar"],
        [3, "Apr"],
        [4, "May"],
        [5, "Jun"],
        [6, "Jul"],
        [7, "Aug"],
        [8, "Sep"],
        [9, "Oct"],
        [10, "Nov"],
        [11, "Dec"],

    ];

    $scope.barOOptions = {

        series: {
            bars: {
                show: true
            }
        },

        xaxis: {
            axisLabel: "Months",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10,
            ticks: ticks
        },
        yaxis: {
            axisLabel: "PHP",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 3,
            tickFormatter: function(v, axis) {
                return v + "PHP";
            }
        },
        legend: {
            noColumns: 0,
            labelBoxBorderColor: "#000000",
            position: "nw"
        },
        grid: {
            hoverable: true,
            borderWidth: 2,
            // backgroundColor: { colors: ["#ffffff", "#EDF5FF"] }
        },

        // colors: [COLORS.success, COLORS.danger],
        // stack: true
    };



    $scope.settings = function() {
        console.log("settings");
    }
    var visits = [
        [0, $scope.getRandomArbitrary()],
        [1, $scope.getRandomArbitrary()],
        [2, $scope.getRandomArbitrary()],
        [3, $scope.getRandomArbitrary()],
        [4, $scope.getRandomArbitrary()],
        [5, $scope.getRandomArbitrary()],
        [6, $scope.getRandomArbitrary()],
        [7, $scope.getRandomArbitrary()],
        [8, $scope.getRandomArbitrary()]
    ];
    $scope.lineDataset = [{
        data: visits,
        color: COLORS.success
    }];
    $scope.lineOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 0,
                fill: false
            },
            points: {
                show: true,
                radius: 3,
                lineWidth: 1,
                fillColor: COLORS.success
            },
            splines: {
                show: true,
                //tension: 0.5,
                lineWidth: 1,
                //fill: 0.2,
            },
            shadowSize: 0
        },
        grid: {
            borderWidth: 0,
            hoverable: true,
        },
        xaxis: {
            show: false
        },
        yaxis: {
            show: false
        }
    };
    $scope.pieDataset = [{
        label: 'IE',
        data: 34,
        color: COLORS.primary
    }, {
        label: 'Safari',
        data: 14,
        color: COLORS.info
    }, {
        label: 'Chrome',
        data: 15,
        color: COLORS.warning
    }];
    $scope.pieOptions = {
        series: {
            pie: {
                show: true,
                //innerRadius: 0.6,
                stroke: {
                    width: 0
                },
                label: {
                    show: false,
                }
            }
        },
        legend: {
            show: false
        },
    };
    $scope.barDataset = [{
        data: visits,
        bars: {
            show: true,
            barWidth: 0.05,
            align: 'center',
            fill: true,
            lineWidth: 0,
            fillColor: '#fff'
        }
    }];
    $scope.barOptions = {
        bars: {
            fillColor: { colors: [{ opacity: 0.5 }, { opacity: 1 }] },
            lineWidth: 1
        },
        grid: {
            borderWidth: 0,
            aboveData: true,
        },
        yaxis: {
            color: 'rgba(255,255,255,0.1)',
        },
        xaxis: {
            mode: 'categories',
            tickLength: 0,
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Roboto',
            axisLabelPadding: 5,
            reserveSpace: true
        }
    };
    var seriesData = [
        [],
        [],
        []
    ];

    var visitors = [
        [0, $scope.getRandomArbitrary()],
        [1, $scope.getRandomArbitrary()],
        [2, $scope.getRandomArbitrary()],
        [3, $scope.getRandomArbitrary()],
        [4, $scope.getRandomArbitrary()],
        [5, $scope.getRandomArbitrary()],
        [6, $scope.getRandomArbitrary()],
        [7, $scope.getRandomArbitrary()],
        [8, $scope.getRandomArbitrary()]
    ];

    $scope.dashboardOptions = {
        series: {
            lines: {
                show: true,
                lineWidth: 0,
            },
            splines: {
                show: true,
                lineWidth: 1,
            }
        },
        grid: {
            borderWidth: 1,
            color: 'rgba(255,255,255,0.2)',
        },
        yaxis: {
            color: 'rgba(255,255,255,0.1)',
        },
        xaxis: {
            mode: 'categories'
        }
    };




    $scope.myInterval = 5000;
    $scope.active = 0;
    var slides = $scope.slides = [];
    var currIndex = 0;

    $scope.addSlide = function() {
        var newWidth = 600 + slides.length + 1;
        slides.push({
            image: 'http://lorempixel.com/' + newWidth + '/400',
            id: currIndex++
        });
    };
    for (var b = 0; b < 4; b++) {
        $scope.addSlide();
    }
}
angular.module('app').controller('dashboardCtrl', ['$scope', '$interval', 'COLORS', '$http', 'Journals',
    dashboardCtrl
]);