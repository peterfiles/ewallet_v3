'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description
 * # app
 *
 * Main module of the application.
 */
angular
    .module('app', [
        'ui.router',
        'ngAnimate',
        'ui.bootstrap',
        'ngStorage',
        'ngCookies',
        'ngSanitize',
        'ui.jq',
        'ngTouch',
        'pascalprecht.translate',
        'n3-line-chart',
        'textAngular',
        'ui.select',
        'oitozero.ngSweetAlert',
        'angular-flot'

    ])
    .constant('COLORS', {
        font: 'Arial, "Helvetica Neue", Helvetica, sans-serif',
        'default': $('<div>').appendTo('body').addClass('bg-default').css('background-color'),
        primary: $('<div>').appendTo('body').addClass('bg-primary').css('background-color'),
        success: $('<div>').appendTo('body').addClass('bg-success').css('background-color'),
        warning: $('<div>').appendTo('body').addClass('bg-warning').css('background-color'),
        danger: $('<div>').appendTo('body').addClass('bg-danger').css('background-color'),
        info: $('<div>').appendTo('body').addClass('bg-info').css('background-color'),
        white: $('<div>').appendTo('body').addClass('bg-white').css('background-color'),
        dark: $('<div>').appendTo('body').addClass('bg-dark').css('background-color'),
        border: '#e4e4e4',
        bodyBg: $('body').css('background-color'),
        textColor: '#6B6B6B',
    })
    .constant('CONFIG', {
        'COMPACCT_URL': 'http://compacct.api.hybrain.co/api/v1',
        'INVENTORY_URL': 'http://compacct.inventory.api.hybrain.co/api/v1',
        'LOGIN_URL': 'http://compacct.api.hybrain.co/api',
        'BASE_URL': '',
        'SYSTEM_LANGUAGE': ''
    })
    .directive('showFocus', function($timeout) {
        return function(scope, element, attrs) {
            scope.$watch(attrs.showFocus,
                function(newValue) {
                    $timeout(function() {
                        newValue && element[0].focus();
                    });
                }, true);
        };
    })
    .directive('ngConfirmClick', [
        function() {
            return {
                link: function(scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Are you sure?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click', function(event) {
                        if (window.confirm(msg)) {
                            scope.$eval(clickAction)
                        }
                    });
                }
            };
        }
    ]);