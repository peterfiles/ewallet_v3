'use strict';
angular.module('app').controller('AppCtrl', ['$scope', '$state', '$http', '$localStorage', '$timeout', '$translate', 'SweetAlert', '$location', 'Corporations', 'Businesses',
        function AppCtrl($scope, $state, $http, $localStorage, $timeout, $translate, SweetAlert, $location, Corporations, Businesses) {




            $scope.mobileView = 767;
            $scope.app = {
                name: 'CompAcct',
                year: (new Date()).getFullYear(),
                layout: {
                    isSmallSidebar: false,
                    isChatOpen: false,
                    isFixedHeader: true,
                    isFixedFooter: false,
                    isBoxed: false,
                    isStaticSidebar: false,
                    isRightSidebar: false,
                    isOffscreenOpen: false,
                    isConversationOpen: false,
                    isQuickLaunch: false,
                    sidebarTheme: '',
                    headerTheme: ''
                },
                isMessageOpen: false,
                isContactOpen: false,
                isConfigOpen: false,
            };

            function init() {
                // $scope.selectedBusiness = 0;
                // $scope.selectedCorporation = 0;



                Corporations.getAll().success(function(data, status) {
                    $scope.corps = data.data;
                    //console.log("Corporations: " + JSON.stringify($scope.corps));
                })
                Businesses.getAll().success(function(data, status) {
                    $scope.businesses = data.data;
                    //console.log("Businesses: " + JSON.stringify($scope.businesses));
                })

                if ($localStorage.token != undefined) {

                    $scope.corp_name = $localStorage.corp_name;
                    $scope.buss_name = $localStorage.buss_name;

                }

                if ($localStorage.user_role == 'ADMIN') {
                    $scope.userIsAdmin = true;
                }


            }
            init();

            // alert($localStorage.user.corp_name);

            $scope.signout = function() {

                $localStorage.$reset();
                $state.go('auth.signin');
            }

            $scope.savePreferences = function(config) {
                /**
                 * config= { corp_id, bsns, id, user_id } 
                 */
                console.log(config);
                // console.log($scope.selectedBusiness, $scope.selectedCorporation);
                // $localStorage.config = config;
            }




            console.log($localStorage.user);
            $scope.user = {
                name: $localStorage.user,
                jobDesc: $localStorage.corp_name,
                avatar: 'images/avatar.jpg',
            };

            if (angular.isDefined($localStorage.layout)) {
                $scope.app.layout = $localStorage.layout;
            } else {
                $localStorage.layout = $scope.app.layout;
            }

            $scope.$watch('app.layout', function() {
                $localStorage.layout = $scope.app.layout;
            }, true);

            $scope.$on('$viewContentLoaded', function() {
                angular.element('.pageload').fadeOut(1500);
                $timeout(function() {
                    angular.element('body').removeClass('page-loading');
                }, 2000);
            });

            $scope.getRandomArbitrary = function() {
                return Math.round(Math.random() * 100);
            };

            $scope.setLang = function(langKey) {
                $translate.use(langKey);
            };

            $scope.searchFocus = false;
            $scope.focusOn = function() {
                $scope.searchFocus = true;
            };
            $scope.focusOff = function() {
                $timeout(function() {
                    $scope.searchFocus = false;
                }, 100);
            };
        }
    ])
    .run(function($rootScope, $localStorage, SweetAlert, $location, $state, $http) {


        // alert('Token : ' + $localStorage.token);
        // alert('Business name : ' + $localStorage.buss_name);
        // alert('Business Id : ' + $localStorage.buss_id);
        // alert('Corporation name : ' + $localStorage.corp_name);
        // alert('Corporation Id : ' + $localStorage.corp_id);



        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, error) {

            if ($localStorage.token != undefined) {

                // add a global authentication header for every http request 
                // passing the authentication token generated during auth
                $http.defaults.headers.common.Authorization = 'Bearer ' + $localStorage.token;

            }

            // console.log('Error : ' + JSON.stringify(error));

            if (typeof $localStorage.token == 'undefined' || typeof $localStorage.token == null) {

                // SweetAlert.swal('Oopps..', 'Looks like you\'re token has expired or you\'re not signed in! Pleas signin now.', 'error');
                //  $state.go('auth.signin');

                // // / throw him/her to the garbage can
                $location.path('/');

                // $location.path('/auth/signin');
                // event.preventDefault();

            }

            // alert($location.path());

            // check if the user selects the business to manage

            // if ($location.path() != '/settings/main' && $location.path() != '/auth/signin') {

            //     if ($localStorage.corp_id == undefined) {

            //         SweetAlert.swal('Hey !..', 'Looks like you did not select the organization to manage !', 'error');

            //         // grab a rope and tie the user to the tree of settings
            //         $location.path('/settings/main');

            //     }

            // }

        });



    });