﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using System;
using System.Net;
using MoneySwap.com;
using System.Xml;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Net.Mime;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Configuration;
using System.Text;

namespace Cooptavanza.Wallet.Controllers
{
    public class LunexTopUpApiController : ApiController
    {

        readonly CustomerService svcCustomer;
        readonly TransactionService svcTransactionService;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly TransactionLogService svcTLogService;
        readonly PartnersService svcPartner;
        readonly PartnerCustomerService svcPartnerCustomer;
        readonly CoopEnrolledAccountService svcCEAS;
        readonly UserAccountService svsUAS;
        readonly AgentsService svcAgent;
        readonly MerchantService svcMerchant;
        readonly CoopTransactionLogService svcCTLS;
        readonly CurrencyService svcCur;
        readonly PaymentBreakdownService svcPaymentBreakdown;
        readonly CommissionsService svcCommissionService;
        readonly CountryService svcCountry;

        public LunexTopUpApiController(CustomerService svcCustomer, 
            TransactionService svcTransactionService, 
            EnrolledAccountService svcEnrolledAccount,
            TransactionLogService svcTLogService,
            PartnersService svcPartner,
            PartnerCustomerService svcPartnerCustomer,
            CoopEnrolledAccountService svcCEAS,
            UserAccountService svsUAS,
            AgentsService svcAgent,
            MerchantService svcMerchant,
            CoopTransactionLogService svcCTLS,
            CurrencyService svcCur,
            PaymentBreakdownService svcPaymentBreakdown,
            CommissionsService svcCommissionService,
            CountryService svcCountry)
        {
            this.svcCustomer = svcCustomer;
            this.svcTransactionService = svcTransactionService;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcTLogService = svcTLogService;
            this.svcPartner = svcPartner;
            this.svcPartnerCustomer = svcPartnerCustomer;
            this.svcCEAS = svcCEAS;
            this.svsUAS = svsUAS;
            this.svcMerchant = svcMerchant;
            this.svcCTLS = svcCTLS;
            this.svcAgent = svcAgent;
            this.svcCur = svcCur;
            this.svcPaymentBreakdown = svcPaymentBreakdown;
            this.svcCommissionService = svcCommissionService;
            this.svcCountry = svcCountry;

        }

        [Route("LEP/topup/LunexTest")]
        [HttpGet]
        public string Test()
        {
            try {
                string method = "POST";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync("orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00", method);
                return r;
            }
            catch (Exception e)
            {
                new Generator().LogException(e,"LunexTopUp-Log");
                return null;
            }
        }

        /*E-Wallet API STARTS HERE*/
        //API STARTS WITH topup/
        [Route("LEP/topup/GetAlltopUPProductsNameWithSkU")]
        [HttpGet]
        public object GetAlltopUPProducts()
        {
            try
            {
                string method = "GET";
                string data = "skus?type=TOPUP";
                string r = new WebRequestHelperLunex().WebGetRequest(data, method);
                //string r = new WebRequestHelperLunex().WebGetRequestCustom(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = JsonConvert.SerializeXmlNode(doc);
                object re = JsonConvert.DeserializeXmlNode(ret);
                return re;
            }catch(Exception e)
            {
                new Generator().LogException(e, "LunexTopUp-Log");
                return null;
            }
        }

        [Route("LEP/topup/GetProductDataBySku/{id}")]
        [HttpGet]
        public object GetProductBySku(int id)
        {
            try
            {
                string method = "GET";
                string data = "skus/" + id;
                string r = new WebRequestHelperLunex().WebGetRequest(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = JsonConvert.SerializeXmlNode(doc);
                object re = JsonConvert.DeserializeXmlNode(ret);
                svcTLogService.SaveLog(r, "GetProductsBySkuID");
                return re;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "LunexTopUp-Log");
                return null;
            }
        }


        [Route("LEP/topup/processTopUp")]
        [HttpPost]//int accountEnrolledID, string phone, string username, string password, decimal amount, string skuid, string curr, string amountTodeductInEnrolledAccount
        public object processTopUp(LunexTopUpEx d)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                string mobile = d.username.Replace(" ", "+");
                if (d.password != null)
                {
                    d.password = new EncryptionHelper().Encrypt(d.password);
                }
                CustomerEx data = svcCustomer.GetLogin(mobile, d.password);

                if (data.id > 0)
                {
                    string fullname = data.firstname + " " + data.middlename + " " + data.lastname;
                    string method = "POST";
                    string CID = "" + Guid.NewGuid();
                    string req = "orders/"
                        + CID
                        + "?sku=" + d.skuid
                        + "&phone=" + d.phone
                        + "&amount=" + d.amount;


                    string r = new WebRequestHelperLunex().WebRequestResponseAsync(req, method);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(r);
                    var ret = JsonConvert.SerializeXmlNode(doc);
                    object re = JsonConvert.DeserializeXmlNode(ret);


                    svcTLogService.SaveLog(r, "TOP-UP_AIRTIME");

                    XmlNodeList elemList = doc.GetElementsByTagName("Response");
                    int code = 1;
                    if (elemList[0].HasChildNodes)
                    {
                        code = Convert.ToInt32(elemList[0].ChildNodes[0].InnerText); //Code is the second element of The parent
                    }
                    if (code > 1)
                    {

                        TransactionEx tEx = new TransactionEx();
                        EnrolledAccountEx eAEx = svcEnrolledAccount.Get(d.accountEnrolledID);
                        tEx.accountNumber = eAEx.accountnumber; // account to be debited
                        tEx.balance = Convert.ToDecimal(d.amount);
                        tEx.credit = false;
                        tEx.currencyfromiso = eAEx.currency;
                        tEx.currencytoiso = d.curr;
                        tEx.dataCreated = DateTime.Now;
                        tEx.debit = true;
                        tEx.description = "TOP-UP AIR TIME Transaction id :" + CID;
                        tEx.enrolledAccountsCurrencyID = eAEx.currenycid;
                        tEx.enrolledAccountsID = d.accountEnrolledID;
                        tEx.receiversID = data.id;
                        tEx.status = "CLEARED";
                        tEx.transactionTID = CID;
                        tEx.transactionType = "10004";
                        tEx.userId = data.id;
                        tEx.userTID = data.tid;

                        svcTransactionService.Save(tEx);

                        new Generator().sendSMSNotif(data.mobilenumber, eAEx.currency, d.amountTodeductInEnrolledAccount, "Top-Up AirTime", "debited");
                        new Generator().sendEmailNotif(fullname, data.email, eAEx.currency, d.amountTodeductInEnrolledAccount, "Top-Up AirTime", "debited");

                        return "Success";

                    }
                    else
                    {
                        return errorCodeResponse(code);
                    }
                }
                else
                {
                    return "Please use your password and try again.";

                }
            }catch(Exception e)
            {
                sb.Append("TOP-UP");
                sb.AppendFormat("1-{0}-{1}-{2}-{3}-***********-{4}-{5}-{6}-{7}", 
                    d.accountEnrolledID,d.phone,d.username,d.amount,d.skuid,d.curr,d.amountTodeductInEnrolledAccount);
                new Generator().LogException(e, sb.ToString());
                return e;
            }
            
        }

        [Route("LEP/topup/coopred/processTopUp")]
        [HttpPost]//int accountEnrolledID, string phone, string username, string password, decimal amount, string skuid, string curr, string amountTodeductInEnrolledAccount
        public object processTopUpCoopRed(LunexTopUpCoopRedEx d)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                CoopEnrolledAccountEx ceaEx = svcCEAS.getIdByUserIDRoleIDCurrency(d.userID, d.roleID, d.currTo);
                string tID = string.Empty;
                int pID = 0;
                string pName = string.Empty;
                int cntryID = 0;
                int steID = 0;
                switch (d.roleID)
                {
                    case 2:
                        tID = svsUAS.Get(d.userID).tID;
                        break;
                    case 3:
                        PartnersEx pEx = svcPartner.Get(d.userID);
                        tID = pEx.tid;
                        pName = pEx.finame;
                        pID = Convert.ToInt32(pEx.masterPartnerID);
                        cntryID = Convert.ToInt32(pEx.countryid);
                        steID = Convert.ToInt32(pEx.stateid);
                        break;
                    case 4:
                        AgentsEx aEx = svcAgent.Get(d.userID);
                        tID = aEx.tid;
                        pID = Convert.ToInt32(aEx.partnerID);
                        pName = aEx.name;
                        cntryID = Convert.ToInt32(aEx.countryid);
                        steID = Convert.ToInt32(aEx.stateid);
                        break;
                    case 5:
                        MerchantEx mEx = svcMerchant.Get(d.userID);
                        tID = mEx.tid;
                        pID = Convert.ToInt32(mEx.masterPartnerID);
                        pName = mEx.name;
                        cntryID = Convert.ToInt32(mEx.countryid);
                        steID = Convert.ToInt32(mEx.stateid);
                        break;
                }
                if (ceaEx.id > 0)
                {
                    string method = "POST";
                    string CID = "" + Guid.NewGuid();
                    string req = "orders/"
                        + CID
                        + "?sku=" + d.skuID
                        + "&phone=" + d.phoneToLoad
                        + "&amount=" + d.amount;


                    string r = new WebRequestHelperLunex().WebRequestResponseAsync(req, method);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(r);
                    var ret = JsonConvert.SerializeXmlNode(doc);
                    object re = JsonConvert.DeserializeXmlNode(ret);


                    svcTLogService.SaveLog(r, "TOP-UP_AIRTIME");

                    XmlNodeList elemList = doc.GetElementsByTagName("Response");
                    int code = 1;
                    if (elemList[0].HasChildNodes)
                    {
                        code = Convert.ToInt32(elemList[0].ChildNodes[0].InnerText); //Code is the second element of The parent
                    }
                    if (code > 1)
                    {
                        //To monitor Credit
                        CoopTransactionLogEx tEx = new CoopTransactionLogEx();
                    tEx.amount = Convert.ToDecimal(d.amounToDeduct);
                    tEx.currencyAccountFrom = d.currFrom;
                    tEx.currencyAccountTo = d.currTo;
                    tEx.date_created = DateTime.Now;
                    tEx.giverTID = tID;
                    tEx.giverUserId = d.userID;
                    tEx.giverUserRoles = d.roleID;
                    tEx.isCredit = true;
                    tEx.receiverTID = "00000000-0000-0000-0000-000000000000";
                    tEx.receiverUserId = 0;
                    tEx.receiverUserRoles = 0;
                    tEx.transactionDescription = "Top-Up Airtime transaction. Amount: (" + d.currFrom
                                               + " - " + d.amount + ") loaded to Mobile No.: "
                                               + d.phoneToLoad + ". Debited to account Amount: (" + d.currTo + " - " + d.amounToDeduct + ").";
                    tEx.transactionTID = new Generator().generateTransactionNo("10004", svcTransactionService.GetTransactionLastIDNumber(), d.currFrom);
                    string tCode = tEx.transactionTID;
                    tEx.transactionType = "TOP-UP";
                    svcCTLS.Save(tEx);

                    //To monitor Debit
                    tEx = new CoopTransactionLogEx();
                    tEx.amount = Convert.ToDecimal(d.amountRemaining);
                    tEx.currencyAccountFrom = d.currFrom;
                    tEx.currencyAccountTo = d.currTo;
                    tEx.date_created = DateTime.Now;
                    tEx.giverTID = tID;
                    tEx.giverUserId = d.userID;
                    tEx.giverUserRoles = d.roleID;
                    tEx.isCredit = false;
                    tEx.receiverTID = "00000000-0000-0000-0000-000000000000";
                    tEx.receiverUserId = 0;
                    tEx.receiverUserRoles = 0;
                    tEx.transactionDescription = "Top-Up Airtime transaction. Amount: (" + d.currFrom
                                               + " - " + d.amount + ") loaded to Mobile No.: "
                                               + d.phoneToLoad + ". Debited to account Amount: (" + d.currTo + " - " + d.amounToDeduct + ").";
                    tEx.transactionTID = tCode;
                    tEx.transactionType = "TOP-UP";
                    svcCTLS.Save(tEx);

                    TransactionEx t = new TransactionEx();
                    t.accountNumber = ceaEx.accountnumber;
                    t.balance = Convert.ToDecimal(d.amountRemaining);
                    t.credit = false;
                    t.debit = true;
                    t.currencyfromiso = d.currFrom;
                    t.currencytoiso = d.currTo;
                    t.dataCreated = DateTime.Now;
                    t.description = "Top-Up Airtime transaction. Amount: (" + d.currFrom
                                               + " - " + d.amount + ") loaded to Mobile No.: "
                                               + d.phoneToLoad + ". Debited to account Amount: (" + d.currTo + " - " + d.amounToDeduct + "). -Coop Red-";
                    t.enrolledAccountsCurrencyID = ceaEx.currenycid;
                    t.enrolledAccountsID = ceaEx.id;
                    t.partnerId = pID;
                    t.receiversID = 0;
                    t.status = "CLEARED";
                    t.transactionTID = tCode;
                    t.transactionType = "10004";
                    t.userId = d.userID;
                    t.userTID = tID;

                    svcTransactionService.Save(t);

                        #region SaveCommission
                    new GlobalCallForServicesController(
                        svcCur,
                        svcPaymentBreakdown,
                        svcCommissionService,
                        svcCTLS,
                        svcCountry).SaveCommission(t, d.scac, tID, pName, pID, d.roleID, cntryID, steID);
                    #endregion SaveCommission

                        return "Success";

                    }
                    else
                    {
                        return errorCodeResponse(code);
                    }
                }
                else
                {
                    return "There is an error, Please try again.";
                }
            }catch(Exception ex)
            {
                new Generator().LogException(ex, "Top-up");
                return "There is an error, Please try again.";
            }

        }

        /*E-Wallet API ENDS HERE*/

        /*CBS API STARTS HERE*/
        //API STARTS WITH topup/CBS/
        [Route("LEP/topup/CBS/GetAlltopUPProductsNameWithSkU")]
        [HttpPost]
        public object GetAlltopUPProductsProd(RequestQouteEx req)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                PartnersEx P = svcPartner.getByPartnerCode(req.partnerCode);
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                if (P != null)
                {
                    var message = EH.HMAC_SHA256(MAH.requestQoute(req), P.key);
                    if (req.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestQoute(req), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = req.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }
                    else
                    {

                        if (req.transactionType == "10004")
                        {
                            string method = "GET";
                            string data = "skus?type=TOPUP";
                            string r = new WebRequestHelperLunex().WebGetRequest(data, method);
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(r);
                            var ret = JsonConvert.SerializeXmlNode(doc);
                            object re = JsonConvert.DeserializeXmlNode(ret);
                            return re;

                        }
                        else
                        {
                            RE.code = "RTF";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                signature = req.signature
                            };
                            RE.message = "INVALID TRANSACTION CODE";
                            return RE;
                        }
                    }
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = req.signature
                    };
                    RE.message = "PARTNER CODE NOT EXISTING";
                    return RE;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "LunexTopUp-Log");
                return null;
            }
        }

        [Route("LEP/topup/CBS/GetProductDataBySku")]
        [HttpPost]
        public object GetCBSProductBySku(RequestTopupWithSKU d)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                PartnersEx P = svcPartner.getByPartnerCode(d.partnerCode);
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                if (P != null)
                {
                    var message = EH.HMAC_SHA256(MAH.requestTopUpProductsBySKU(d), P.key);
                    if (d.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTopUpProductsBySKU(d), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = d.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }
                    else
                    {
                        if (d.transactionType == "10004")
                        {

                            string method = "GET";
                            string data = "skus/" + d.id;
                            string r = new WebRequestHelperLunex().WebGetRequest(data, method);
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(r);
                            var ret = JsonConvert.SerializeXmlNode(doc);
                            object re = JsonConvert.DeserializeXmlNode(ret);


                            return re;
                        }
                        else
                        {
                            RE.code = "RTF";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                signature = d.signature
                            };
                            RE.message = "INVALID TRANSACTION CODE";
                            return RE;
                        }
                    }
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = d.signature
                    };
                    RE.message = "PARTNER CODE NOT EXISTING";
                    return RE;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "LunexTopUp-Log");
                return null;
            }
        }

        [Route("LEP/topup/CBS/processTopUp")]
        [HttpPost]
        public object processTopUpCBS(ProcessTopUpEx d)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                PartnersEx P = svcPartner.getByPartnerCode(d.partnerCode);
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                PartnerCustomerEx PC = new PartnerCustomerEx();

                if (P != null)
                {
                    var message = EH.HMAC_SHA256(MAH.requestTopUpProcess(d), P.key);
                    if (d.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTopUpProcess(d), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = d.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }
                    else
                    {
                        if (d.transactionType == "10004")
                        {
                            if (!d.phonetoTopUp.Equals(""))
                            {
                                bool rs = svcTransactionService.CheckTransactionId(d.referenceNo, P.id);
                                if (rs == true)
                                {
                                    RE.code = "RTF";
                                    RE.date = DateTime.Now;
                                    RE.data = new
                                    {
                                        referenceNo = d.referenceNo,
                                        signature = d.signature,
                                    };
                                    RE.message = "REFERENCE NUMBER ALREADY EXIST";
                                    return RE;
                                }
                                else
                                {
                                    PC.accountnumber = d.accountNumber;
                                    PC.firstname = d.firstname;
                                    PC.lastname = d.lastname;
                                    PC.middlename = d.middlename;
                                    PC.mobilenumber = d.mobilenumber;
                                    PC.partnerId = P.id;
                                    PC.phonenumber = d.phoneNumber;
                                    PC.suffix = d.suffix;
                                    PC.tid = "" + Guid.NewGuid();
                                    PC.email = d.email;
                                    PC.dateCreated = DateTime.Now;

                                    string fullname = PC.firstname + " " + PC.middlename + " " + PC.lastname;

                                    svcPartnerCustomer.Save(PC);
                                    PartnerCustomerEx PCE = svcPartnerCustomer.Get(PC.id);

                                    string mobile = d.phonetoTopUp.Replace(" ", "+");
                                    string method = "POST";
                                    string CID = "" + Guid.NewGuid();
                                    string req = "orders/"
                                        + d.referenceNo
                                        + "?sku=" + d.skuid
                                        + "&phone=" + mobile
                                        + "&amount=" + d.amount;


                                    string r = new WebRequestHelperLunex().WebRequestResponseAsync(req, method);
                                    XmlDocument doc = new XmlDocument();
                                    doc.LoadXml(r);
                                    var ret = JsonConvert.SerializeXmlNode(doc);
                                    object re = JsonConvert.DeserializeXmlNode(ret);


                                    svcTLogService.SaveLog(r, "TOP-UP_AIRTIME");

                                    XmlNodeList elemList = doc.GetElementsByTagName("Response");
                                    int code = 1;
                                    if (elemList[0].HasChildNodes)
                                    {
                                        code = Convert.ToInt32(elemList[0].ChildNodes[0].InnerText); //Code is the second element of The parent
                                    }
                                    if (code > 1)
                                    {

                                        TransactionEx tEx = new TransactionEx();
                                        tEx.accountNumber = d.accountNumber; // account to be credited
                                        tEx.balance = d.amount;
                                        tEx.credit = false;
                                        tEx.currencyfromiso = d.currency;
                                        tEx.currencytoiso = d.currency;
                                        tEx.dataCreated = DateTime.Now;
                                        tEx.debit = true;
                                        tEx.description = "TOP-UP AIR TIME reference ID :" + d.referenceNo;
                                        tEx.enrolledAccountsCurrencyID = 0;
                                        tEx.enrolledAccountsID = 0;
                                        tEx.receiversID = 0;
                                        tEx.status = "CLEARED";
                                        tEx.transactionTID = d.referenceNo;
                                        tEx.transactionType = "10004";
                                        tEx.userId = PCE.id;
                                        tEx.userTID = PCE.tid;

                                        svcTransactionService.Save(tEx);
                                        try
                                        {
                                            new Generator().sendSMSNotif(d.mobilenumber, d.currency, d.amount.ToString(), "Top-Up AirTime", "debited");
                                            new Generator().sendEmailNotif(fullname, d.email, d.currency, d.amount.ToString(), "Top-Up AirTime", "debited");
                                            RE.code = "RTV";
                                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                            RE.data = new
                                            {
                                                signature = d.signature
                                            };
                                            RE.message = "SUCCESS TRANSACTION";
                                            return RE;

                                        }
                                        catch (Exception ex)
                                        {
                                            RE.code = "RTF";
                                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                            RE.data = new
                                            {
                                                signature = d.signature
                                            };
                                            RE.message = "ERROR SENDING NOTIFICATION";

                                            Console.WriteLine(ex);
                                            return RE;
                                        }
                                    }
                                    else
                                    {
                                        RE.code = "RTF";
                                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                        RE.data = new
                                        {
                                            signature = d.signature
                                        };
                                        RE.message = errorCodeResponse(code);
                                        return RE;
                                    }
                                }
                            }
                            else
                            {
                                RE.code = "RTF";
                                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                RE.data = new
                                {
                                    signature = d.signature
                                };
                                RE.message = "INVALID PHONE NUMBER";
                                return RE;
                            }
                        }
                        else
                        {
                            RE.code = "RTF";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                signature = d.signature
                            };
                            RE.message = "INVALID TRANSACTION CODE";
                            return RE;
                        }
                    }
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = d.signature
                    };
                    RE.message = "PARTNER CODE NOT EXISTING";
                    return RE;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "LunexTopUp-Log");
                return null;
            }

        }

        /*CBS API STARTS HERE*/

        /*RESPONSE CODES HERE*/
        public string errorCodeResponse(int code)
        {
            switch (code)
            {
                case -1:
                    return "Failed, Kindly check again you phone number and please try again.";
                case -10:
                    return "Invalid transaction id.";
                case -11:
                    return "Invalid code.";
                case -12:
                    return "Invalid user";
                case -13:
                    return "Insufficient Credit.";
                case -14:
                    return "Invalid Amount.";
                case -20:
                    return "Account in use.";
                case -21:
                    return "Account not found.";
                case -23:
                    return "Account already exist.";
                case -24:
                    return "Invalid Pin.";
                case -25:
                    return "Pin is used.";
                case -26:
                    return "Invalid phone.";
                case -100:
                    return "System error.";
                default:
                    return "";
            }
        }
        /*END OF RESPONSE CODES HERE*/


    }
}
