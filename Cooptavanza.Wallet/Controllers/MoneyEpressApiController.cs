﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using System;
using System.Net;
using MoneySwap.com;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    public class MoneyExpressApicontroller : ApiController
    {

        readonly CustomerService svcCustomer;
        readonly UserAccountService svcUserAccount;
        readonly TransactionService svcTransaction;
        readonly TransferMoneyService svcTransferMoney;
        readonly CurrencyService svcCur;
        readonly MEXResponseRemitValidateService svcMResRemitValidate;
        readonly MEXRemitValidateService svcMRemitValidate;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly MEXResponseRemitErrorLogService svcRemitErrorLog;

        MoneyExpressApiV04Client client = new MoneyExpressApiV04Client();
        string merchantId = ConfigurationManager.AppSettings["MEXMId"].ToString();//"507000590000103";// "001678020000103";
        string key = ConfigurationManager.AppSettings["MEXKey"].ToString();//"testkey@VANZA";
        public MoneyExpressApicontroller(
            CustomerService svcCustomer, 
            UserAccountService svcUserAccount, 
            TransactionService svcTransaction, 
            TransferMoneyService svcTransferMoney, 
            CurrencyService svcCur,
            MEXResponseRemitValidateService svcMResRemitValidate,
            MEXRemitValidateService svcMRemitValidate,
            EnrolledAccountService svcEnrolledAccount,
            MEXResponseRemitErrorLogService svcRemitErrorLog)
        {
            this.svcUserAccount = svcUserAccount;
            this.svcCustomer = svcCustomer;
            this.svcTransaction = svcTransaction;
            this.svcTransferMoney = svcTransferMoney;
            this.svcCur = svcCur;
            this.svcMRemitValidate = svcMRemitValidate;
            this.svcMResRemitValidate = svcMResRemitValidate;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcRemitErrorLog = svcRemitErrorLog;
        }

        [Route("ms/sendMoney")]
        [HttpPost]
        public object SendMoney([FromBody] MEXSendMoneyEx sm )
        {
            try
            {
                string refId = "" + (100000000500 + svcMRemitValidate.GetLastId());
                if (sm.Customer == null) { return null; }
                if (sm.MoneySwap == null) { return null; }
                CustomerEx c = svcCustomer.Get(sm.Customer.id);
                MoneyExpressRemitEx n = sm.MoneySwap;
                string plaintext = "";
                string address = c.address + "\n" + c.city + "\n" + c.state + "\n" + c.country;
                n.AmlStatus = ("AML_CHECK_PASSED").ToUpper();
                n.FloatCurrency = ("USD").ToUpper();
                n.IncludeFees = ("EXCLUDE_FEE").ToUpper();
                n.KioskAddress = ("NNNNN").ToUpper();
                n.KioskName = ("NNNNN").ToUpper();
                n.KycCheck = ("SENDER_KYC_PASSED").ToUpper();

                n.Note = ("Developer Money Send Test").ToUpper();
                n.OriginatorUID = ((Guid.NewGuid() + "").Replace("-", "") + "-100016").ToUpper();
                n.RcvAccntType = ("BANK_CARD").ToUpper();



                n.ReferenceId = (refId).ToUpper();
                n.SndIdNo = n.SndIdNo.ToUpper();//sender id document number
                n.SndIdType = n.SndIdType.ToUpper();
                //n.RcvIdType = "BANK_CARD";
                n.SndAddress = address.ToUpper();
                n.SndKycUrl = ("EASYOFAC.COM").ToUpper();
                n.SndLastName = (c.lastname).ToUpper();
                n.SndPhoneNo = c.mobilenumber.Replace("+", "");
                n.TellerFirstName = ("EWALLET").ToUpper();
                n.TellerLastName = ("EWALLET").ToUpper();
                n.TranCurr = ("USD").ToUpper();
                //     n.ValidateReferenceId: 123456789012
                n.SndFirstName = (c.firstname).ToUpper();




                MoneyExpressApiRemitRequestV04 x = new MoneyExpressApiRemitRequestV04();
                DateTime startdate = DateTime.Now.AddDays(-4);
                DateTime enddate = DateTime.Now;

                x.AmlStatus = n.AmlStatus.ToUpper();
                x.FloatCurrency = n.FloatCurrency.ToUpper();
                x.IncludeFees = n.IncludeFees.ToUpper();
                x.KioskAddress = n.KioskAddress.ToUpper();
                x.KioskName = n.KioskAddress.ToUpper();
                x.KycCheck = n.KycCheck.ToUpper();
                x.MerchantId = merchantId.ToUpper();
                x.Note = n.Note != null ? n.Note.ToUpper() : "";
                x.OriginatorUID = n.OriginatorUID.ToUpper();
                x.RcvAccntNo = n.RcvAccntNo.ToUpper();
                x.RcvAccntType = n.RcvAccntType.ToUpper();
                x.RcvFirstName = n.RcvFirstName.ToUpper();
                x.RcvIdNumber = n.RcvIdNumber != null ? n.RcvIdNumber.ToUpper() : "";
                x.RcvIdType = n.RcvIdType != null ? n.RcvIdType.ToUpper() : "";
                x.RcvLastName = n.RcvLastName.ToUpper();
                x.ReferenceId = n.ReferenceId.ToUpper();
                x.SndAddress = n.SndAddress != null ? n.SndAddress.ToUpper().Replace("\n", "\r\n") : "";
                x.SndFirstName = n.SndFirstName.ToUpper();
                x.SndIdNo = n.SndIdNo.ToUpper();
                x.SndIdType = n.SndIdType.ToUpper();
                x.SndKycUrl = n.SndKycUrl != null ? n.SndKycUrl.ToUpper() : "";
                x.SndLastName = n.SndLastName.ToUpper();
                x.SndPhoneNo = n.SndPhoneNo != null ? n.SndPhoneNo.ToUpper() : "";
                x.SndRemitPurpose = n.SndRemitPurpose.ToUpper();
                x.TellerFirstName = n.TellerFirstName.ToUpper();
                x.TellerLastName = n.TellerLastName.ToUpper();
                x.TranAmt = n.TranAmt.ToUpper();
                x.TranCurr = n.TranCurr.ToUpper();
                x.ValidateReferenceId = n.ValidateReferenceId != null ? n.ValidateReferenceId.ToUpper() : "";

                plaintext = x.AmlStatus +
                x.FloatCurrency +
                x.IncludeFees +
                x.KioskAddress +
                x.KioskName +
                x.KycCheck +
                x.MerchantId +
                x.Note +
                x.OriginatorUID +
                x.RcvAccntNo +
                x.RcvAccntType +
                x.RcvFirstName +
                x.RcvIdNumber +
                x.RcvIdType +
                x.RcvLastName +
                x.ReferenceId +
                x.SndAddress +
                x.SndFirstName +
                x.SndIdNo +
                x.SndIdType +
                x.SndKycUrl +
                x.SndLastName +
                x.SndPhoneNo +
                x.SndRemitPurpose +
                x.TellerFirstName +
                x.TellerLastName +
                x.TranAmt +
                x.TranCurr +
                x.ValidateReferenceId;

                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);
                var y = client.Remit(x);



                MEXResponseRemitValidateEx r = new MEXResponseRemitValidateEx();
                r.ActionCode = y.ActionCode;
                r.ConversionRate = y.ConversionRate != null ? y.ConversionRate : "";
                r.FeeCurrency = y.FeeCurrency != null ? y.FeeCurrency : "";
                r.FloatAmount = y.FloatAmount != null ? y.FloatAmount : "";
                r.FloatCurrency = y.FloatCurrency != null ? y.FloatCurrency : "";
                r.KioskFeeAmount = y.KioskFeeAmount != null ? y.KioskFeeAmount : "";
                r.LastUpdateTime = y.LastUpdateTime != null ? y.LastUpdateTime : "";
                r.MEXType = "VALIDATE";
                r.Note = y.Note != null ? y.Note : "";
                r.OriginatorUID = y.OriginatorUID != null ? y.OriginatorUID : "";
                r.PaidFeeAmount = y.PaidFeeAmount != null ? y.PaidFeeAmount : "";



                r.RcvAccntNo = y.RcvAccntNo != null ? y.RcvAccntNo : "";
                r.RcvFirstName = y.RcvFirstName != null ? y.RcvFirstName : "";
                r.RcvLastName = y.RcvLastName != null ? y.RcvLastName : "";
                r.ReferenceId = y.ReferenceId != null ? y.ReferenceId : "";
                r.RespCode = y.RespCode != null ? y.RespCode : "";
                r.RespMessage = y.RespMessage != null ? y.RespMessage : "";
                r.SentAmount = y.SentAmount != null ? y.SentAmount : "";
                r.SentCurrency = y.SentCurrency != null ? y.SentCurrency : "";
                r.Signature = y.Signature != null ? y.Signature : "";
                r.SndKycUrl = y.SndKycUrl != null ? y.SndKycUrl : "";
                r.SubmitTime = y.SubmitTime != null ? y.SubmitTime : "";
                r.TotalFeeAmount = y.TotalFeeAmount != null ? y.TotalFeeAmount : "";
                r.TransactionId = y.TransactionId != null ? y.TransactionId : "";

                if (y.RespCode == "00")
                {
                    r.QuoteAmt = y.FloatAmount != null ? y.FloatAmount : "";
                    r.QuoteCurr = "USD";
                    r.RcvAccntBankCode = "EMPTY";
                    r.RcvAccntBankName = "EMPTY";
                    new MoneyExpressNotificationSender().sendMoneyExpressEmail(c.firstname + " " + c.lastname, c.email, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate),
                    x.RcvFirstName + " " + x.RcvLastName, x.ReferenceId);
                    new MoneyExpressNotificationSender().sendMoneySwapConfirmationMoney(c.firstname + " " + c.lastname, c.mobilenumber, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate), n.ReferenceId);
                    new MoneyExpressNotificationSender().RcvMoneySwapConfirmationMoney(c.firstname + " " + c.lastname, x.RcvFirstName + " " + x.RcvLastName, c.mobilenumber, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate), n.ReferenceId);
                }
                else
                {
                    r.QuoteAmt = y.FloatAmount != null ? y.FloatAmount : "";
                    r.QuoteCurr = "USD";
                    r.RcvAccntBankCode = "EMPTY";
                    r.RcvAccntBankName = "EMPTY";
                }


                n.MEXType = "VALIDATE";
                n.SndAddress = n.SndAddress.ToUpper().Replace("\n", "__");
                n.Signature = "signature";//x.Signature;
                n.RcvIdNumber = n.RcvIdNumber != null ? n.RcvIdNumber.ToUpper() : "00";
                n.MerchantId = "00";
                n.ValidateReferenceId = n.ValidateReferenceId != null ? n.ValidateReferenceId.ToUpper() : "00";

                TransactionEx t = new TransactionEx();
                t.transactionType = "10013";
                t.userId = c.id;
                t.userTID = c.tid;
                t.status = "PENDING";

                t.balance = decimal.Parse(n.TranAmt);
                t.currencyfromiso = "USD";
                t.currencytoiso = "USD";
                t.dataCreated = DateTime.Now;
                t.credit = false;
                t.debit = true;
                t.description = "CHINA REMIT SEND MONEY TO " + n.RcvFirstName.ToUpper() + " " + n.RcvLastName.ToUpper() + " Reference No. " + n.ReferenceId;
                t.transactionTID = n.ReferenceId;
                t.enrolledAccountsCurrencyID = 147;
                var eai = svcEnrolledAccount.GetByIdAndISO(c.id, "USD");
                if (eai != null)
                {
                    foreach (var i in eai)
                    {
                        t.enrolledAccountsID = i.id;
                        t.accountNumber = i.accountnumber;
                    }
                }

                if (y.RespCode == "00")
                {
                    svcMResRemitValidate.Save(r);
                    svcMRemitValidate.Save(n);
                    svcTransaction.Save(t);
                }
                else
                {
                    MEXResponseRemitErrorLogEx RREL = new MEXResponseRemitErrorLogEx();

                    RREL.ActionCode = y.ActionCode;
                    RREL.MEXType = y.RespCode;
                    // = y.MEXT
                    //      var dd = (y.GetType()).GetProperty("QouteAmt",String,string[]).GetValue(y,null);
                    //     = (string)y.GetType().GetProperty("QouteAmtField").GetValue(y,null);//.GetValue(y, null);
                    RREL.QuoteAmt = y.FloatAmount != null ? y.FloatAmount : "0";
                    RREL.QuoteCurr = "USD";
                    RREL.Signature = y.Signature != null ? y.Signature : "0000";

                    RREL.RcvAccntBankCode = "EMPTY";
                    RREL.RcvAccntBankName = "EMPTY";
                    RREL.RcvAccntNo = y.RcvAccntNo;
                    RREL.RcvFirstName = y.RcvFirstName;
                    RREL.RcvLastName = y.RcvLastName;
                    RREL.ReferenceId = y.ReferenceId;
                    RREL.RespCode = y.RespCode;
                    RREL.RespMessage = y.RespMessage;
                    RREL.SubmitTime = y.SubmitTime;
                    RREL.TransactionId = y.TransactionId != null ? y.TransactionId : "EMPTY";
                    svcRemitErrorLog.Save(RREL);
                }


                client.Close();

                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }


        [Route("ms/remit")]
        [HttpPost]
        public object Remit([FromBody] MoneyExpressRemitEx n)
        {
            try { 
            string plaintext = "";
            
                MoneyExpressApiRemitRequestV04 x = new MoneyExpressApiRemitRequestV04();
                DateTime startdate = DateTime.Now.AddDays(-4);
                DateTime enddate = DateTime.Now;

                x.AmlStatus = n.AmlStatus.ToUpper();
                x.FloatCurrency = n.FloatCurrency.ToUpper();
                x.IncludeFees = n.IncludeFees.ToUpper();
                x.KioskAddress = n.KioskAddress.ToUpper();
                x.KioskName = n.KioskAddress.ToUpper();
                x.KycCheck = n.KycCheck.ToUpper();
                x.MerchantId = merchantId.ToUpper();
                x.Note = n.Note != null ? n.Note.ToUpper() : "";
                x.OriginatorUID = n.OriginatorUID.ToUpper();
                x.RcvAccntNo = n.RcvAccntNo.ToUpper();
                x.RcvAccntType = n.RcvAccntType.ToUpper();
                x.RcvFirstName = n.RcvFirstName.ToUpper();
                x.RcvIdNumber = n.RcvIdNumber != null ? n.RcvIdNumber.ToUpper() : "";
                x.RcvIdType = n.RcvIdType != null ? n.RcvIdType.ToUpper() : "";
                x.RcvLastName = n.RcvLastName.ToUpper();
                x.ReferenceId = n.ReferenceId.ToUpper();
                x.SndAddress = n.SndAddress != null ? n.SndAddress.ToUpper().Replace("\n", "\r\n") : "";
                x.SndFirstName = n.SndFirstName.ToUpper();
                x.SndIdNo = n.SndIdNo.ToUpper();
                x.SndIdType = n.SndIdType.ToUpper();
                x.SndKycUrl = n.SndKycUrl != null ? n.SndKycUrl.ToUpper() : "";
                x.SndLastName = n.SndLastName.ToUpper();
                x.SndPhoneNo = n.SndPhoneNo != null ? n.SndPhoneNo.ToUpper() : "";
                x.SndRemitPurpose = n.SndRemitPurpose.ToUpper();
                x.TellerFirstName = n.TellerFirstName.ToUpper();
                x.TellerLastName = n.TellerLastName.ToUpper();
                x.TranAmt = n.TranAmt.ToUpper();
                x.TranCurr = n.TranCurr.ToUpper();
                x.ValidateReferenceId = n.ValidateReferenceId != null ? n.ValidateReferenceId.ToUpper() : "";

                plaintext = x.AmlStatus +
                x.FloatCurrency +
                x.IncludeFees +
                x.KioskAddress +
                x.KioskName +
                x.KycCheck +
                x.MerchantId +
                x.Note +
                x.OriginatorUID +
                x.RcvAccntNo +
                x.RcvAccntType +
                x.RcvFirstName +
                x.RcvIdNumber +
                x.RcvIdType +
                x.RcvLastName +
                x.ReferenceId +
                x.SndAddress +
                x.SndFirstName +
                x.SndIdNo +
                x.SndIdType +
                x.SndKycUrl +
                x.SndLastName +
                x.SndPhoneNo +
                x.SndRemitPurpose +
                x.TellerFirstName +
                x.TellerLastName +
                x.TranAmt +
                x.TranCurr +
                x.ValidateReferenceId;

                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);
                var y = client.Remit(x);

                client.Close();
                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }

        [Route("ms/validate")]
        [HttpPost]
        public object Validate([FromBody] MoneyExpressRemitEx n)
        {
            try { 
            string plaintext = "";
            
                MoneyExpressApiRemitRequestV04 x = new MoneyExpressApiRemitRequestV04();
                DateTime startdate = DateTime.Now.AddDays(-4);
                DateTime enddate = DateTime.Now;

                x.AmlStatus = n.AmlStatus;
                x.FloatCurrency = n.FloatCurrency;
                x.IncludeFees = n.IncludeFees;
                x.KioskAddress = n.KioskAddress;
                x.KioskName = n.KioskAddress;
                x.KycCheck = n.KycCheck;
                x.MerchantId = merchantId;
                x.Note = n.Note != null ? n.Note.ToUpper() : "";
                x.OriginatorUID = n.OriginatorUID.ToUpper();
                x.RcvAccntNo = n.RcvAccntNo.ToUpper();
                x.RcvAccntType = n.RcvAccntType.ToUpper();
                x.RcvFirstName = n.RcvFirstName.ToUpper();
                x.RcvIdNumber = n.RcvIdNumber.ToUpper();
                x.RcvIdType = n.RcvIdType.ToUpper();
                x.RcvLastName = n.RcvLastName.ToUpper();
                x.ReferenceId = n.ReferenceId.ToUpper();
                x.SndAddress = n.SndAddress.ToUpper().Replace("\n", "\r\n");
                x.SndFirstName = n.SndFirstName.ToUpper();
                x.SndIdNo = n.SndIdNo.ToUpper();
                x.SndIdType = n.SndIdType.ToUpper();
                x.SndKycUrl = n.SndKycUrl != null ? n.SndKycUrl.ToUpper() : "";
                x.SndLastName = n.SndLastName.ToUpper();
                x.SndPhoneNo = n.SndPhoneNo != null ? n.SndPhoneNo.ToUpper() : "";
                x.SndRemitPurpose = n.SndRemitPurpose.ToUpper();
                x.TellerFirstName = n.TellerFirstName.ToUpper();
                x.TellerLastName = n.TellerLastName.ToUpper();
                x.TranAmt = n.TranAmt.ToUpper();
                x.TranCurr = n.TranCurr.ToUpper();
                x.ValidateReferenceId = n.ValidateReferenceId != null ? n.ValidateReferenceId.ToUpper() : "";


                plaintext = x.AmlStatus +
                x.FloatCurrency +
                x.IncludeFees +
                x.KioskAddress +
                x.KioskName +
                x.KycCheck +
                x.MerchantId +
                x.Note +
                x.OriginatorUID +
                x.RcvAccntNo +
                x.RcvAccntType +
                x.RcvFirstName +
                x.RcvIdNumber +
                x.RcvIdType +
                x.RcvLastName +
                x.ReferenceId +
                x.SndAddress +
                x.SndFirstName +
                x.SndIdNo +
                x.SndIdType +
                x.SndKycUrl +
                x.SndLastName +
                x.SndPhoneNo +
                x.SndRemitPurpose +
                x.TellerFirstName +
                x.TellerLastName +
                x.TranAmt +
                x.TranCurr +
                x.ValidateReferenceId;


                x.Signature = (new EncryptionHelper().HMAC_MD5(plaintext, key)).ToUpper();
                // string test = "565854f700745795b18136abcdd8d04f";
                //x.Signature = test.ToUpper();
                var y = client.Validate(x);
                client.Close();


                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }

        [Route("ms/status")]
        [HttpPost]
        public object Status([FromBody] MoneyExpressStatusEx n)
        {
            try
            {
                string plaintext = "";
                MoneyExpressApiStatusRequestV04 x = new MoneyExpressApiStatusRequestV04();
                //DateTime SearchStartDate = DateTime.Now.AddDays(-4);
                //DateTime SearchEndDate = DateTime.Now;

                x.MerchantId = merchantId;
                x.PageIndex = n.PageIndex != null ? n.PageIndex.ToUpper() : "";
                x.PageSize = n.PageSize != null ? n.PageSize.ToUpper() : "";
                x.ReferenceId = n.ReferenceId != null ? n.ReferenceId.ToUpper() : "";
                x.SearchEndDate = String.Format("{0:yyyyMMddHHmm}", n.SearchEndDate).ToUpper();
                x.SearchStartDate = String.Format("{0:yyyyMMddHHmm}", n.SearchStartDate).ToUpper();
                x.TransactionId = n.TransactionId != null ? n.TransactionId.ToUpper() : "";

                plaintext = x.MerchantId +
                    x.PageIndex +
                    x.PageSize +
                    x.ReferenceId +
                    x.SearchEndDate +
                    x.SearchStartDate +
                    x.TransactionId;

                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);
                var y = client.Status(x);

                client.Close();
                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }

        [Route("ms/qoute")]
        [HttpPost]
        public object Qoute([FromBody] MoneyExpressRemitEx n)
        {
            try
            {
                string plaintext = "";

                MoneyExpressApiRemitRequestV04 x = new MoneyExpressApiRemitRequestV04();
                DateTime startdate = DateTime.Now.AddDays(-4);
                DateTime enddate = DateTime.Now;

                x.FloatCurrency = n.FloatCurrency.ToUpper();
                x.IncludeFees = n.IncludeFees.ToUpper();
                x.MerchantId = merchantId.ToUpper();
                x.TranAmt = n.TranAmt.ToUpper();
                x.TranCurr = n.TranCurr.ToUpper();

                plaintext = x.FloatCurrency +
                x.IncludeFees +
                x.MerchantId +
                x.TranAmt +
                x.TranCurr;

                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);
                var y = client.Quote(x);

                client.Close();
                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }
        [Route("ms/GTL")]
        [HttpPost]
        public object GetTransactionLimits([FromBody] MoneyExpressRemitEx n)
        {
            try
            {
                string plaintext = "";
                MoneyExpressApiTransactionLimitRequest x = new MoneyExpressApiTransactionLimitRequest();

                x.MerchantId = merchantId.ToUpper();
                x.TranCurr = n.TranCurr.ToUpper();

                plaintext = x.MerchantId + x.TranCurr;
                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);
                var y = client.GetTransactionLimits(x);

                client.Close();
                return y;
            }catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }


        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Headers.Add("SOAPAction", action);
                webRequest.ContentType = "text/xml;charset=\"utf-8\"";
                webRequest.Accept = "text/xml";
                webRequest.Method = "POST";
                return webRequest;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }



        [Route("ms/balance")]
        [HttpGet]
        public object GetBalance()
        {
            try
            {
                string plaintext = "";
                MoneyExpressApiGetBalanceRequest x = new MoneyExpressApiGetBalanceRequest();

                x.MerchantId = "507000590000103";

                plaintext = x.MerchantId.ToUpper();

                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);

                //HttpWebRequest webRequest = CreateWebRequest("http://testapi.payto86.com/MoneyExpressApiV04.svc", "GetBalance");

                //XmlDocument soapEnvelop = new XmlDocument();


                //soapEnvelop.LoadXml(string.Format(@"<soapenv:Envelope xmlns:mon=""MoneySwap.com""
                //    xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">
                //    <soapenv:Header></soapenv:Header>< soapenv:Body >
                //    < mon:GetBalance >
                //    < mon:req >< mon:MerchantId > 507000590000103 </ mon:MerchantId >
                //    < mon:Signature > 1F0704CA636F0082853CD709FE95D471 </ mon:Signature >
                //    </ mon:req ></ mon:GetBalance ></ soapenv:Body >
                //    </ soapenv:Envelope > "));

                //string responseString;
                //using (Stream stream = webRequest.GetRequestStream())
                //{
                //   soapEnvelop.Save(stream);
                //}
                //try
                //{
                //    HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
                //    responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                //    return response;
                //}
                //catch (WebException ex)
                //{
                //    responseString = (new StreamReader(ex.Response.GetResponseStream())).ReadToEnd();
                //}
                //catch (Exception ex)
                //{
                //    responseString = string.Empty;
                //}
                //  re

                var y = client.GetBalance(x);
                //client.GetBalance(soapEnvelop.ToString());
                client.Close();
                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }

        [Route("ms/fstatment")]
        [HttpPost]
        public object FloatStatment([FromBody] MoneyExpressFloatStatementEx n)
        {
            try
            {
                string plaintext = "";
                MoneyExpressApiFloatStatementRequest x = new MoneyExpressApiFloatStatementRequest();

                x.FloatCurrency = n.FloatCurrency.ToUpper();
                x.MerchantId = merchantId.ToUpper();
                x.PageIndex = n.PageIndex.ToUpper();
                x.PageSize = n.PageSize.ToUpper();
                x.SearchEndDate = String.Format("{0:yyyyMMddHHmm}", n.SearchEndDate).ToUpper();
                x.SearchStartDate = String.Format("{0:yyyyMMddHHmm}", n.SearchStartDate).ToUpper();

                plaintext = x.FloatCurrency +
                x.MerchantId +
                x.PageIndex +
                x.PageSize +
                x.SearchEndDate +
                x.SearchStartDate;

                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);

                var y = client.FloatStatement(x);

                client.Close();
                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }

        [Route("ms/gbt")]
        [HttpPost]
        public object GetBinTable([FromBody] MoneyExpressBinEx n)
        {
            try
            {
                string plaintext = "";
                MoneyExpressApiGetBinTableRequest x = new MoneyExpressApiGetBinTableRequest();
                x.MerchantId = merchantId.ToUpper();
                x.PageIndex = n.PageIndex.ToUpper();
                x.PageSize = n.PageSize.ToUpper();

                plaintext = x.MerchantId + x.PageIndex + x.PageSize;
                x.Signature = new EncryptionHelper().HMAC_MD5(plaintext, key);
                var y = client.GetBinTable(x);

                client.Close();
                return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyExpressAPI-Log");
                return null;
            }
        }

        


    }
}
