﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System.Collections.Generic;
using System;

namespace Cooptavanza.Wallet.Controllers
{
    public class TCPApiController : ApiController
    {
       
       
        public TCPApiController()
        {
           
        }

        public class tcpresponse
        {
            public string message { get; set; }
            public string code { get; set; }
            public string[] data { get; set; }
        }

        public class tcprequest
        {
            public string message { get; set; }

        }

        [Route("TCP")]
        [HttpPost]
        public object Get([FromBody] tcprequest m)
        {
            try { 
            tcpresponse d = new tcpresponse();
            string len = String.Format("{0:0000}", (m.message).Length);
            m.message = len + m.message;
            TCPClient tcp = new TCPClient();
            IsoMessage im = new IsoMessage();
           // var ddd = im.Parse((m.message).Replace("\n",""));
                string dd = tcp.Connect("192.168.1.247",(m.message).Replace("\n", ""));
            d.message = dd.Substring(4);
            d.code = "";
      //      d.data = ddd;
            return d;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }
      
    }
}
