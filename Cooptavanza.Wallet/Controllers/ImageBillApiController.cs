﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Web;
using System.IO;

namespace Cooptavanza.Wallet.Controllers
{
    public class ImageBillApiController : ApiController
    {
        readonly ImageBillService svcImageBill;
        public ImageBillApiController(ImageBillService svcImageBill)
        {
            this.svcImageBill = svcImageBill;
        }

        [Route("ImageBill")]
        [HttpGet]
        public ImageBillSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new ImageBillSearchViewModel() { Results = svcImageBill.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcImageBill.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageBill-Log");
                return null;
            }
        }


        [Route("ImageBill/{Id}")]
        [HttpGet]
        public ImageBillEx GetPerson(int Id)
         {
            try
            {
                return svcImageBill.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageBill-Log");
                return null;
            }
            //return null;
        }

        [Route("GetImageBillByUI/{Id}")]
        [HttpGet]
        public ImageBillEx GetBillById(int Id)
        {
            try
            {
                return svcImageBill.GetByUserId(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageBill-Log");
                return null;
            }
            //return null;
        }


        [Route("ImageUploadBill/save")]
        [HttpPost]
        public ImageBillEx UploadBillImage()
        {
            try
            {
                var form = HttpContext.Current.Request.Form;
                string userId = "0";
                string imageProfileId = "0";

                foreach (string key in form.AllKeys)
                {
                    if (key == "userId") { userId = form[key]; }
                    if (key == "imageBillId") { imageProfileId = form[key]; }
                }
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                if (userId != "0")
                {

                    ImageBillEx n = new ImageBillEx();
                    n.id = Int32.Parse(imageProfileId);
                    n.userId = Int32.Parse(userId);

                    if (file != null && file.ContentLength > 0)
                    {
                        string filename = file.FileName;
                        //string folder = 
                        if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("../Upload/" + userId + "/")))
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("../Upload/" + userId + "/").ToString());
                        }

                        string path = HttpContext.Current.Server.MapPath("../Upload/" + userId + "/");

                        string guid = "";
                        var profile = svcImageBill.Get(n.id);
                        if (profile != null)
                        {
                            guid = profile.imageString != null ? profile.imageString : "" + Guid.NewGuid();
                            File.Delete(path + guid + "." + filename.Split('.')[1].ToString());
                            guid = "" + Guid.NewGuid();
                        }
                        else { guid = "" + Guid.NewGuid(); }

                        string u = (path + guid + "." + filename.Split('.')[1]).ToString();

                        file.SaveAs(u);
                        n.imageString = guid;
                        n.path = "/Upload/" + userId + "/" + guid + "." + filename.Split('.')[1];
                        svcImageBill.Save(n);
                        
                        return svcImageBill.Get(n.id);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageBill-Log");
                return null;
            }
        }

        [Route("ImageBill/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public ImageBillEx Save([FromBody] ImageBillEx n)
        {
            try
            {
                svcImageBill.Save(n);
                return svcImageBill.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageBill-Log");
                return null;
            }
        }
       

        [Route("ImageBill/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcImageBill.Remove(Id);
        }
    }
}
