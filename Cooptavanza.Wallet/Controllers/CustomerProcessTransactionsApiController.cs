﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;

using Cooptavanza.Wallet.Repository;
using System.Text;
using System.Text.RegularExpressions;

namespace Cooptavanza.Wallet.Controllers
{
    public class CustomerProcessTransactionsApiController : ApiController
    {
        readonly CustomerService svcCustomer;
        readonly OFAClistService svcOFAC;
        readonly TransactionService svcTransaction;
        readonly GateWayProcessResponseReader gwprr;
        readonly MoneyExpressApiV04Client client;
        readonly CurrencyService svcCur;
        readonly EnrolledAccountService svcEnrAcc;
        readonly ServiceFeeService svcFeeService;
        readonly CreditCardLogService svcCreditCardLog;
        readonly TransactionLogService svcTLog;


        public CustomerProcessTransactionsApiController(
            CustomerService svcCustomer,
            OFAClistService svcOFAC,
            TransactionService svcTransaction,
            GateWayProcessResponseReader gwprr,
            MoneyExpressApiV04Client client,
            CurrencyService svcCur,
            EnrolledAccountService svcEnrAcc,
            ServiceFeeService svcFeeService,
            CreditCardLogService svcCreditCardLog,
            TransactionLogService svcTLog)
        {
            this.svcCustomer = svcCustomer;
            this.svcOFAC = svcOFAC;
            this.svcTransaction = svcTransaction;
            this.gwprr = gwprr;
            this.client = client;
            this.svcCur = svcCur;
            this.svcEnrAcc = svcEnrAcc;
            this.svcFeeService = svcFeeService;
            this.svcCreditCardLog = svcCreditCardLog;
            this.svcTLog = svcTLog;
        }


        [Route("ProcessTransaction/ProcessPayment")]
        [HttpPost]
        public object ProcessPayment([FromBody] GateWayProcessPaymentRequestEx data)
        {
            try
            {
                CustomerEx customerData = new CustomerEx();
                int processID = 0;
                GateWayProcessController.CardTypeValue(data.card_no);
                //string errMessageForAmount = data.amount_purchase.ToString().Length > 3 ?  "Amount must only be 3 digit number" : "";
                switch (data.moduleCode)
                {
                    case "ciCC":
                        processID = 1; //processPayment
                        customerData = svcCustomer.Get(data.customerID);
                        data.payby = GateWayProcessController.cardType;
                        break;
                    //case "ciEFT": processID = 18; // processPaymentWithEFT
                    //    data.payby = "EFT";
                    //    break;
                    case "ciACH":
                        processID = 19; // ach
                        customerData = svcCustomer.Get(data.customerID);
                        data.payby = "ach";
                        break;
                }
                if (data.currencyType.Equals("EUR")) { data.sid = 300; data.rcode = "5def110e0e00d02ff5679bf2c559122ca4dd9e9c"; }//test For EUR currency //for test sid = 298 //live=300
                else if (data.currencyType.Equals("USD")) { data.sid = 301; data.rcode = "7a046d08e3a075b0c01c1abc715af924f60f1cb0"; }//test For USD currency with KYC //data.sid = 299 no kyc live =301
                data.card_no = data.card_no.Replace(" ", "");
                data.firstname = customerData.firstname;

                data.uip = "52.163.122.160";//GetIP();
                data.lastname = customerData.lastname;
                data.email = customerData.email;
                data.phone = customerData.mobilenumber;
                //if (data.isDefaultAddress)
                //{
                //data.address = customerData.address;
                //data.suburb_city = customerData.city;
                //data.state = customerData.state;
                //data.postcode = customerData.zipcode;
                //data.country = customerData.country;
                //}
                data.quantity = 1;
                string[] breakMonthAndYear = data.card_exp_month.Split('/');
                data.item_no = customerData.tid;
                data.card_exp_month = breakMonthAndYear[0];
                data.card_exp_year = Convert.ToInt32(breakMonthAndYear[1]);
                data.item_desc = "Cash In";
                data.itemName = "Cash In";
                data.itemQuantity = 1;
                data.currency_code = data.currencyType;
                decimal originalAmount = data.amount_purchase;
                data.amount_purchase = originalAmount;
                data.amount_unit = originalAmount;
                //data.amount_shipping = data.usd_amountShipping;
                //data.amount_tax = data.usd_amountTax;
                gwprr.GateWayProcessPaymentSetParams(data);
                GateWayProcessController myProxy = new GateWayProcessController(gwprr.GateWayProcessPaymentSetParams(data));
                /*
                 Note amount_purchase that is passed in https://cfmerc.epspci.com/soap/tx3.php is converted in USD
                 */
                string response = myProxy.SubmitSOAPPaymentTransaction("https://cfmerc.epspci.com/soap/tx3.php", processID);
                GateWayProcessPaymentRequestEx res = gwprr.response(response);
                svcTLog.SaveLog(response, "CASH-IN_CREDIT-CARD");
                /*
                 Logging Credit Card Start
                 */
                CreditCardLogsEx ccle = new CreditCardLogsEx();
                ccle.transactionNo = res.responseTransactionID;
                ccle.logs = response;
                ccle.dateLogged = DateTime.Now;
                ccle.status = res.responseStatus;
                svcCreditCardLog.Save(ccle);
                /*Logging Credit Card End*/
                TransactionEx trans = new TransactionEx();
                bool isAccept = true;
                string errMessage = string.Empty;
                if (res.responseStatus.Equals("OK"))
                {
                    List<EnrolledAccountEx> eaEx = svcEnrAcc.GetByUserID(customerData.id);
                    EnrolledAccountEx enAcc = new EnrolledAccountEx();
                    int enAccID = 0;
                    bool addToEnrollecAccount = false;
                    addToEnrollecAccount = svcEnrAcc.GetByIdAndISO(customerData.id, data.currencyType).Count > 0 ? false : true;
                    if (addToEnrollecAccount || eaEx.Count == 0)
                    {
                        enAcc.id = 0;
                        enAcc.currency = data.currencyType;
                        enAcc.currenycid = svcCur.GetByISO(data.currencyType).id;
                        enAcc.accountDefault = false;
                        enAcc.userId = customerData.id;
                        enAcc.userTID = customerData.tid;
                        svcEnrAcc.Save(enAcc);
                        enAccID = svcEnrAcc.retID();
                    }
                    else
                    {
                        foreach (EnrolledAccountEx d in svcEnrAcc.GetByIdAndISO(customerData.id, data.currencyType))
                        {
                            //enAccID = d.id;
                            enAcc = d;
                            break;
                        }
                    }

                    trans.balance = data.amount_purchase; // covertedValue 
                    trans.credit = true;
                    trans.debit = false;
                    trans.dataCreated = DateTime.Now;
                    trans.description = "Cash-In Transaction. ID:" + res.responseTransactionID /*res.responseInfo + ". Code:" + res.responseErrCode*/;
                    trans.status = res.responseType == null ? "Approved" : res.responseType;
                    trans.transactionTID = res.responseTransactionID;
                    trans.transactionType = "30001"; //+ res.responseTransactionAction;
                    trans.userId = customerData.id;
                    trans.userTID = customerData.tid;
                    trans.currencyfromiso = data.currency_code;/*svcCur.Get(customerData.currencyid).iso;*/
                    trans.currencytoiso = data.currency_code;
                    trans.enrolledAccountsCurrencyID = svcCur.GetByISO(data.currency_code).id;
                    trans.enrolledAccountsID = enAcc.id;
                    trans.accountNumber = enAcc.accountnumber;
                    trans.receiversID = 0;
                    svcTransaction.Save(trans);
                    string fullname = customerData.firstname + " " + customerData.middlename + " " + customerData.lastname;
                    new CreditCardNotificationSender().sendSMSCreditCard(customerData.mobilenumber, data.currency_code, data.amount_purchase);
                    new CreditCardNotificationSender().sendCreditCardEmail(fullname, customerData.email, data.currency_code, data.amount_purchase);
                }
                else
                {
                    isAccept = false;
                    if (!(res.responseType.Equals(string.Empty)))
                    {
                        errMessage = "Sorry but your transaction is " + res.responseType + " due to " + res.responseInfo + ". Please try again!";
                    }
                    else
                    {
                        if (res.response_msg.Equals("Acquirer Error"))
                        {
                            errMessage = "Please use the Card Name, Address, City, Country, State or Postal Code of registered Credit Card and try again.";
                        }
                        else if (res.response_msg.Equals("DECLINE message"))
                        {
                            errMessage = "Please set an amount greater than $1.00 or €1.00";
                        }
                        else if (res.response_msg.Equals("Value expected for 'cart['summary']['amount_purchase']'."))
                        {
                            errMessage = "Please set an amount greater than $1.00 or €1.00";
                        }
                        else
                        {
                            errMessage = "Sorry but there is an " + res.response_msg + ". Please try again!";
                        }

                    }
                }
                object obj = new
                {
                    IsAccepted = isAccept,
                    ErrorMessage = errMessage
                };
                return obj;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "CustomerProcessTransaction-Log");
                return null;
            }
        }
        
        public string GetIP()
        {
            string mess = string.Empty;
            string ip = string.Empty;
            IPAddress[] localIPs = Dns.GetHostAddresses(Dns.GetHostName());
            try {
                ip = localIPs[2].ToString();
            }catch(Exception ex)
            {
                ip = localIPs[0].ToString();
                mess = ex.ToString();
            }
            return ip;
        }

        
        //MoneySwapAPI
        [Route("ProcessTransaction/MoneySwap")]
        public bool validate()
        {
            try
            {
                MoneySwap.com.MoneyExpressApiRemitRequestV04 valid = new MoneySwap.com.MoneyExpressApiRemitRequestV04
                {
                    AmlStatus = "",
                    FloatCurrency = "",
                    IncludeFees = "",
                    KioskAddress = "",
                    KioskName = "",
                    KycCheck = "",
                    MerchantId = "",
                    OriginatorUID = "",
                    RcvAccntNo = "",
                    RcvAccntType = "",
                    ReferenceId = "",
                    SndAddress = "",
                    SndFirstName = "",
                    SndIdNo = "",
                    SndIdType = "",
                    SndLastName = "",
                    SndRemitPurpose = "",
                    TellerFirstName = "",
                    TellerLastName = "",
                    TranAmt = "",
                    TranCurr = "",
                    Signature = ""
                };

                MoneySwap.com.MoneyExpressApiRemitResponse response = client.Validate(valid);
                StringBuilder sb = null;
                sb.Append(response);
                client.Close();
                return true;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ProcessTransactionMoneySwap-Log");
                return false;
            }
        }
        //initializeTaxTransactionCodeAndTransctionFee
        private static string code = "";
        [Route("ProcessTransaction/initializeTaxTransactionCodeAndTransctionFee")]
        [HttpPost]
        public object DefaultTransaction(object amount)
        {
            try
            {
                decimal amnt = Convert.ToDecimal(amount.ToString());
                decimal tv = Convert.ToDecimal(svcFeeService.GetByServiceFeeType("Cash In - Credit Card").serviceFeePercentage);
                decimal tf = Convert.ToDecimal(svcFeeService.GetByServiceFeeType("Cash In - Credit Card").serviceFeeAmount);
                decimal amountminusTransactionFeeAndTax = amnt - ((amnt * (tv / 100)) + tf);
                if (code.Equals(string.Empty))
                {
                    code = new Generator().OTP();
                }
                object ret = new
                {
                    tax = tv / 100,
                    taxValue = (amnt * tv),
                    transactionCode = code,
                    transactionFee = tf,
                    amountPlusFees = amountminusTransactionFeeAndTax
                };
                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ProcessTransaction-Log");
                return null;
            }
        }

        [Route("ProcessTransaction/ValidateCustomerProfile/{Id}")]
        [HttpGet]
        //[Authorize(Roles = "administrators")]
        public object ValidateCustomerProfile(int Id)
        {
            try
            {
                object obj = null;
                bool isValid = true;
                StringBuilder errMessage = new StringBuilder();
                CustomerEx det = svcCustomer.Get(Id);

                if (String.IsNullOrEmpty(det.address)) { isValid = false; errMessage.Append("*Please Add your Address in Personal Information. \n").ToString(); }
                if (String.IsNullOrEmpty(det.city)) { isValid = false; errMessage.Append("*Please Add your City in Personal Information. \n").ToString(); }
                if (String.IsNullOrEmpty(det.state)) { isValid = false; errMessage.Append("*Please Add your State in Personal Information. \n").ToString(); }
                if (String.IsNullOrEmpty(det.country)) { isValid = false; errMessage.Append("*Please Add your Country in Personal Information. \n").ToString(); }
                if (String.IsNullOrEmpty(det.zipcode)) { isValid = false; errMessage.Append("*Please Add your Postal Code in Personal Information. \n").ToString(); }
                if ((String.IsNullOrEmpty(det.mobilenumber) && String.IsNullOrEmpty(det.phonenumber)) || (String.IsNullOrEmpty(det.mobilenumber) && String.IsNullOrEmpty(det.phonenumber))) { isValid = false; errMessage.Append("*Please Add either your Phone or Mobile Number in Basic Information. \n \n").ToString(); }

                if (String.IsNullOrEmpty(det.address) &&
                    String.IsNullOrEmpty(det.city) &&
                    String.IsNullOrEmpty(det.state) &&
                    String.IsNullOrEmpty(det.country) &&
                    String.IsNullOrEmpty(det.zipcode) &&
                    String.IsNullOrEmpty(det.mobilenumber) &&
                    String.IsNullOrEmpty(det.phonenumber))
                { isValid = false; errMessage.Append("*Please Add either your Phone or Mobile Number in Basic Information. \n"); }
                obj = new
                {
                    IsValid = isValid,
                    ErrorMessage = errMessage.ToString()
                };
                return obj;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ProcessTransaction-Log");
                return null;
            }
        }

        [Route("TestApiTransactNo")]
        [HttpPost]
        public string transactionNo(int? countryid, int? stateid)
        {
            Generator g = new Generator();
            int lastTransactionNumber = svcTransaction.GetTransactionLastIDNumber();
            return g.getTransactionNo(countryid, stateid, lastTransactionNumber);
        }

        [Route("Decrypt")]
        [HttpPost]
        public object decryptme(string text)
        {
            EncryptionHelper eh = new EncryptionHelper();
            return eh.Decrypt(text);
        }

    }
}
