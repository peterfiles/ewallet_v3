﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;


namespace Cooptavanza.Wallet.Controllers
{
    public class BankDepositApiController : ApiController
    {
        //readonly DummyLoadService svcDummyLoad;
        readonly BankDepositService svcBankDeposit;

        public BankDepositApiController(BankDepositService svcBankDeposit)
        {
            this.svcBankDeposit = svcBankDeposit;
        }

        [Route("BankDeposit")]
        [HttpGet]
        public BankDepositSearchViewModel Get(string SearchText = "", string SortBy = "Id Desc", int PageNo = 1, int PageSize = 10, int userID=0, int roleID=0)
        {
            try
            {
                SearchText = SearchText == null ? "" : SearchText;
                return new BankDepositSearchViewModel()
                {
                    //Results = svcBankDeposit.GetPaged(SearchText, SortBy, PageNo, PageSize, userID ,roleID),
                    //Count = svcBankDeposit.Count(SearchText)
                    Results = svcBankDeposit.CustomListGetPaged(Convert.ToString(SearchText), PageSize, PageNo),
                    Count = svcBankDeposit.CustomListGetPagedCount(SearchText)
                };
            }
            catch (Exception e)
            {
                new Generator().LogException(e,"BankDeposit-Log");
                return null;
            }
        }

        [Route("BankDeposit/adminsave")]
        [HttpPost]
        public BankDepositEx SaveBankDeposit(BankDepositEx ex)
        {
            try
            {
                ex.dateCreated = DateTime.Now;
                svcBankDeposit.Save(ex);
                return ex;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "BankDeposit-Log");
                return null;
            }
        }

        [Route("BankDeposit/GetCountry")]
        [HttpGet]
        public List<BankDepositEx> GetCountry()
        {
            try
            {
                return svcBankDeposit.GetBankByCountry();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "BankDeposit-Log");
                return null;
            }
        }

        [Route("BankDeposit/GetBankNameByCountryID")]
        [HttpGet]
        public List<BankDepositEx> GetBankNameByCountryID(int countryID)
        {
            try
            {
                return svcBankDeposit.GetBankNameByCountryID(countryID);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "BankDeposit-Log");
                return null;
            }
        }

        [Route("BankDeposit/GetCurrencyByCountryIDAndBankName")]
        [HttpGet]
        public List<BankDepositEx> GetCurrencyByCountryIDAndBankName(int countryID, string bankName)
        {
            try
            {
                return svcBankDeposit.GetBankNameByCountryIDAndBankName(countryID, bankName);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "BankDeposit-Log");
                return null;
            }
        }
        //[Route("Biller/{Id}")]
        //[HttpGet]
        //public BillerEx GetBiller(int Id)
        //{
        //    return svcBiller.Get(Id);
        //    //return null;
        //}


        //[Route("Biller/adminsave")]
        //[HttpPost]
        //public BillerEx AdminSave([FromBody] BillerEx n)
        //{
        //    n.tid = ""+Guid.NewGuid();
        //    svcBiller.Save(n);
        //    return svcBiller.Get(n.id);
        //}
        //[Route("Biller/{Id}")]
        //[HttpDelete]
        //public void Delete(int Id)
        //{
        //    svcBiller.Remove(Id);
        //}

    }
}
