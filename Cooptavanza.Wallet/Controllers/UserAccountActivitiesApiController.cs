﻿using Cooptavanza.Wallet.Service;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Cooptavanza.Wallet.Controllers
{
    public class UserAccountAcvititiesApiController : ApiController
    {
        readonly UserAccountActivitiesService svcUserAccntActivities;
        public UserAccountAcvititiesApiController(UserAccountActivitiesService svcUserAccntActivities)
        {
            this.svcUserAccntActivities = svcUserAccntActivities;
        }

        [Route("UserAccntActivities")]
        [HttpGet]
        public List<UserAccountActivitiesEx> Get(string where = "", string SortBy = "dateCreated Desc", int PageNo = 1, int PageSize = 100)
        {
            try
            {
                return svcUserAccntActivities.GetPaged(where, SortBy, PageNo, PageSize);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccntActivities-Log");
                return null;
            }
        }

        




      
    }
}
