﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using Cooptavanza.Wallet.Repository;
using System.Text;

namespace Cooptavanza.Wallet.Controllers
{
    public class UserServicesApiController : ApiController
    {
        readonly UserServicesService svcUserServices;


        public UserServicesApiController(UserServicesService svcUserServices)
        {
            this.svcUserServices = svcUserServices;
        }


       

        [Route("UserServices")]
        [HttpGet]
        public UserServicesSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            UserServicesSearchViewModel x = new UserServicesSearchViewModel() { Results = svcUserServices.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcUserServices.Count(SearchText) };
            return x;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserServices-Log");
                return null;
            }
        }

        [Route("UserServices/GetByid/{id}")]
        [HttpGet]
        public UserServicesEx GetGetByid(int id)
        {
            try { 
            return svcUserServices.Get(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserServices-Log");
                return null;
            }
        }

        [Route("UserServices/GetByUR/")]
        [HttpGet]
        public object GetGetByid(int id, int role)
        {
            try { 
            return svcUserServices.GetByUserIDToShowProduct(id, role);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserServices-Log");
                return null;
            }
        }

        [Route("UserServices/saveProductByUR")]
        [HttpPost]
        public ResponseEx saveProductByUR([FromBody] UserProductsEx n)
        {
            ResponseEx e = new ResponseEx();
            try
            {
                foreach (var p in n.products)
                {
                    if (p.check == "true")
                    {
                        UserServicesEx US = new UserServicesEx();
                        US.ID = p.serviceId;
                        US.product_id = p.id;
                        US.roles_id = n.userRole;
                        US.user_id = n.userId;
                        svcUserServices.Save(US);
                    }
                }
                e.code = "1001";
                e.date = DateTime.Now;
                e.data = null;
                e.message = "SUCCESSFULLY UPDATED";
            }
            catch(Exception ex)
            {
                e.code = "3000";
                e.date = DateTime.Now;
                e.data = null;
                e.message = "UPDATE FAILED";

                new Generator().LogException(ex, "UserServiceApi-Log");
            }
            
            
            return e;

        }

    }
}
