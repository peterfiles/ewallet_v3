﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Cooptavanza.Wallet.Controllers
{
    public class ServicesController : Controller
    {
        // GET: Services
        public ActionResult BankDeposit()
        {
            return View();
        }

        public ActionResult MoneyTransfer()
        {
            return View();
        }

        public ActionResult ChinaRemit()
        {
            return View();
        }

        public ActionResult Check21()
        {
            return View();
        }

        public ActionResult CreditCard()
        {
            return View();
        }

        public ActionResult EFT()
        {
            return View();
        }

        public ActionResult CashLocation()
        {
            return View();
        }

        public ActionResult CoopMember()
        {
            return View();
        }

        public ActionResult OCT()
        {
            return View();
        }

        public ActionResult WireTransfer()
        {
            return View();
        }

        public ActionResult PropertyCard()
        {
            return View();
        }

        public ActionResult TopupAirtime()
        {
            return View();
        }

        public ActionResult EmailTransfer()
        {
            return View();
        }

        public ActionResult SMSTransfer()
        {
            return View();
        }

        public ActionResult ContactUs()
        {
            return View();
        }
    }
}