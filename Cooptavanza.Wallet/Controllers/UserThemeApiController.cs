﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using System;

namespace Cooptavanza.Wallet.Controllers
{
    public class UserThemeApiController : ApiController
    {
        readonly UserThemeService svcUserTheme;
        public UserThemeApiController(UserThemeService svcUserTheme)
        {
            this.svcUserTheme = svcUserTheme;
        }

      
        [Route("UserTheme/{Id}")]
        [HttpGet]
        public UserThemeEx GetUserTheme(int Id)
         {
            try { 
             return svcUserTheme.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserTheme-Log");
                return null;
            }
            //return null;
        }

        [Route("UserTheme/u/{Id}")]
        [HttpGet]
        public UserThemeEx GetByUserId(int Id)
        {
            try{ 
            return svcUserTheme.GetByUserId(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserTheme-Log");
                return null;
            }
            //return null;
        }


        [Route("UserTheme/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public UserThemeEx Save([FromBody] UserThemeEx n)
        {
            try { 
            svcUserTheme.Save(n);
            return svcUserTheme.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserTheme-Log");
                return null;
            }
        }



    }
}
