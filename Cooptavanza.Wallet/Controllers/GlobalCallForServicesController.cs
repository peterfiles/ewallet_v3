﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Cooptavanza.Wallet.Models;
using Cooptavanza.Wallet.Service;
using System.Collections.Generic;

namespace Cooptavanza.Wallet.Controllers
{
    public class GlobalCallForServicesController : Controller
    {
        readonly AgentsService svcAgent;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;
        readonly PartnersService svcPartner;
        readonly CoopEnrolledAccountService svcCEAS;
        readonly CoopTransactionLogService svcCTLS;

        readonly PayInPaymentRatesService svcPayIn;
        readonly CurrencyService svcCurrency;

        readonly PaymentBreakdownService svcPaymentBreakdown;
        readonly CommissionsService svcCommissionService;

        readonly CountryService svcCountry;

        public GlobalCallForServicesController(
            AgentsService svcAgent,
            MerchantService svcMerchant,
            UsersService svcUsers,
            PartnersService svcPartner)
        {
            this.svcAgent = svcAgent;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcPartner = svcPartner;
        }

        public GlobalCallForServicesController(
            CoopEnrolledAccountService svcCEAS)
        {
            this.svcCEAS = svcCEAS;
        }

        public GlobalCallForServicesController(
            CoopTransactionLogService svcCTLS)
        {
            this.svcCTLS = svcCTLS;
        }

        public GlobalCallForServicesController(
            PayInPaymentRatesService svcPayIn)
        {
            this.svcPayIn = svcPayIn;
        }

        public GlobalCallForServicesController(
            CurrencyService svcCurrency,
            PaymentBreakdownService svcPaymentBreakdown,
            CommissionsService svcCommissionService,
            CoopTransactionLogService svcCTLS,
            CountryService svcCountry
            )
        {
            this.svcCurrency = svcCurrency;
            this.svcPaymentBreakdown = svcPaymentBreakdown;
            this.svcCommissionService = svcCommissionService;
            this.svcCTLS = svcCTLS;
            this.svcCountry = svcCountry;
        }

        public class userTIDandID
        {
            public string tID { get; set; }
            public int id { get; set; }
        }

        public userTIDandID getTIDandIDByUserRole(int roleID, int id)
        {
            userTIDandID uti = new userTIDandID();
            switch (roleID)
            {
                case 1:
                    var x = svcUsers.Get(id);
                    return uti = new userTIDandID { tID = x.tid, id = x.id };
                case 2:
                    var y = svcUsers.Get(id);
                    return uti = new userTIDandID { tID = y.tid, id = y.id };
                case 3:
                    var z = svcPartner.Get(id);
                    return uti = new userTIDandID { tID = z.tid, id = z.id };
                case 4:
                    var a = svcAgent.Get(id);
                    return uti = new userTIDandID { tID = a.tid, id = a.id };
                case 5:
                    var b = svcMerchant.Get(id);
                    return uti = new userTIDandID { tID = b.tid, id = b.id };
            }
            return uti;
        }

        public object getCoopAccountList(int userID, int rolesID)
        {
            return svcCEAS.getCoopAccountList(userID, rolesID);
        }

        public List<CoopTransactionListEx> getTransctionList(string str, int psize, int pno, int userID, int roleID)
        {
            return svcCTLS.getCoopTransactionList(str, psize, pno, userID, roleID);
        }

        public int getCountTransactionList(string str, int userID, int roleID)
        {
            return svcCTLS.getcountTransactionListUserIDAndRoleID(str, userID, roleID);
        }

        public List<PaymentRatesForPayinsEx> PaymentRatesGetByStrAndMet(string str = "", int met = 0)
        {
            try
            {
                List<object> compiled = new List<object>();
                List<PaymentRatesForPayinsEx> pCEx = new List<PaymentRatesForPayinsEx>();
                PayInsBreakDownEx pBEx = new PayInsBreakDownEx();
                List<PaymentRatesForPayinsEx> l1 = new List<PaymentRatesForPayinsEx>();
                switch (met)
                {
                    case 1: //Select By Country 
                        l1 = svcPayIn.GetByCountryName(str);
                        break;
                    case 2: //Select By LocalPaymentService
                        l1 = svcPayIn.GetByLocalPaymentService(str);
                        break;
                    case 3: //Select By Method
                        l1 = svcPayIn.GetByMethod(str);
                        break;
                }
                if (l1.Count > 0)
                {
                    foreach (PaymentRatesForPayinsEx pdata in l1)
                    {
                        string[] tfc = pdata.txFeeCurrency.Split('/');
                        string[] tf = pdata.txFee.Split('/');
                        string[] c = pdata.currency.Split('/');
                        pdata.d2 = new List<PayInsBreakDownEx>();
                        if (tfc.Length > 1)
                        {
                            for (int x = 0; x < tfc.Length; x++)
                            {
                                pBEx = new PayInsBreakDownEx();
                                pBEx.currency = c[x];
                                pBEx.id = pdata.id;
                                pBEx.txfee = tf[x];
                                pBEx.txfeeCurrency = tfc[x];
                                pdata.d2.Add(pBEx);
                            }
                        }
                        else
                        {
                            pBEx = new PayInsBreakDownEx();
                            pBEx.currency = c[0];
                            pBEx.id = pdata.id;
                            pBEx.txfee = tf[0];
                            pBEx.txfeeCurrency = tfc[0];
                            pdata.d2.Add(pBEx);
                        }
                        pCEx.Add(pdata);
                    }
                }
                return pCEx;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }


        public void SaveCommission(
            TransactionEx t,
            serviceChargesAndCommissionsEx scac,
            string userTID,
            string businessname,
            int agentID,
            int roleID,
            int cntryID,
            int steID)
        {

            #region SavePaymentBreakdownForCommissionPurpose
            CurrencyEx ccEx = svcCurrency.GetByISO(scac.fees.currency);
            PaymentBreakdownEx pBex = new PaymentBreakdownEx(); //Save Merchant Fee
            pBex.amount = scac.cR.mcf;
            pBex.currencyID = ccEx.id;
            pBex.currencyISO = ccEx.iso;
            pBex.isMerchant = true;
            pBex.mdr = scac.fees.mdr.ToString();
            pBex.transactionTID = t.transactionTID;
            pBex.txFee = scac.fees.txFee.ToString();
            pBex.txFeeCurrency = ccEx.iso;
            pBex.userID = agentID;
            pBex.userRole = roleID;
            pBex.dateCreated = t.dataCreated;
            pBex.remarks = "Merchant Commission Fee";
            pBex.transactionType = t.transactionType;
            svcPaymentBreakdown.Save(pBex);

            pBex = new PaymentBreakdownEx();//Save Transaction Fee
            pBex.amount = scac.cR.dmdr;
            pBex.currencyID = ccEx.id;
            pBex.currencyISO = ccEx.iso;
            pBex.isMerchant = false;
            pBex.mdr = scac.fees.mdr.ToString();
            pBex.transactionTID = t.transactionTID;
            pBex.txFee = scac.fees.txFee.ToString();
            pBex.txFeeCurrency = ccEx.iso;
            pBex.userID = agentID;
            pBex.userRole = roleID;
            pBex.dateCreated = t.dataCreated;
            pBex.remarks = "Transaction Commission Fee";
            pBex.transactionType = t.transactionType;
            svcPaymentBreakdown.Save(pBex);
            #endregion SavePaymentBreakdownForCommissionPurpose

            #region Commissions
            CommissionsEx commEx = new CommissionsEx();
            commEx.commissionAccountNumber = svcCommissionService.GetCommissionAccountNumberByMethodAndCurrency(scac.fees.currency, t.transactionType) != null ? svcCommissionService.GetCommissionAccountNumberByMethodAndCurrency(scac.fees.currency, t.transactionType).commissionAccountNumber : string.Empty;
            if (commEx.commissionAccountNumber.Equals(string.Empty) || commEx.commissionAccountNumber == null)
            {
                commEx.commissionAccountNumber = new Generator().generateCommissionsAccountNumber(cntryID, steID, pBex.currencyISO, svcCountry.GetByCountryName(scac.fees.country).iso, agentID);
            }
            commEx.country = scac.fees.country;
            commEx.currency = scac.fees.currency;
            commEx.date = t.dataCreated;
            commEx.localpaymentservice = businessname;
            commEx.mdr = scac.fees.mdr.ToString();
            commEx.method = t.transactionType;
            commEx.status = t.status;
            commEx.txRate = scac.fees.txFee.ToString();
            commEx.txRateFeeCurrency = scac.fees.currency;
            commEx.userID = agentID;
            commEx.userRole = roleID;
            commEx.userTID = userTID;
            commEx.transactionTID = t.transactionTID;
            svcCommissionService.Save(commEx);
            #endregion Commissions
        }


    }
}