﻿using Cooptavanza.Models.ViewModels;
using Cooptavanza.Wallet.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cooptavanza.Wallet.Controllers
{
    public class TransactionsApiController : ApiController
    {
        readonly TransactionService svcTransaction;
        readonly AgentsService svcAgent;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;
        readonly PartnersService svcPartner;
        readonly UserAccountService svcUserAccount;

        public TransactionsApiController(
            TransactionService svcTransaction,
            AgentsService svcAgent,
            MerchantService svcMerchant,
            UsersService svcUsers,
            PartnersService svcPartner,
            UserAccountService svcUserAccount)
        {
            this.svcTransaction = svcTransaction;
            this.svcAgent = svcAgent;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcPartner = svcPartner;
            this.svcUserAccount = svcUserAccount;
        }

        [Route("Transaction")]
        [HttpGet]
        public TransactionSearchViewModel Get(int id, string TID, string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new TransactionSearchViewModel() { Results = svcTransaction.GetPaged(id, TID, SearchText, SortBy, PageNo, PageSize), Count = svcTransaction.Count(id, TID, SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }


        [Route("Transaction/{Id}")]
        [HttpGet]
        public TransactionEx GetSTransaction(int Id)
        {
            try { 
            return svcTransaction.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }

        [Route("Balance/{Id}")] // balance cleared status transaction
        [HttpGet]
        public List<BalanceEx> GetBalance(int Id)
        {
            try
            {
                return svcTransaction.GetBalance(Id);
            }catch(Exception ex)
            {
                //new Generator().LogException(ex, "Transaction:Balance");
                new Generator().LogException(ex, "Transaction.InnerTransaction:Balance");
                return svcTransaction.GetBalance(Id);
            }
        }

        [Route("BalanceOA/{Id}")] // balance overall both pending and cleared status
        [HttpGet]
        public List<BalanceEx> GetBalanceOvarall(int Id)
        {
            try
            {
                return svcTransaction.GetBalanceOverall(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
}

        [Route("BalanceP/{Id}")] // balance pending status transaction
        [HttpGet]
        public List<BalanceEx> GetBalancePending(int Id)
        {
            try { 
            return svcTransaction.GetBalancePending(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }

        [Route("Transactions/GetAllTransactionType")]
        [HttpGet]
        public List<GetTypeTransactionEx> GetAllTransactionType(string passcode)
        {
            try
            {
                if (passcode.Equals("$uP3RaDm!N"))
                {
                    return svcTransaction.GetTransactionType();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }

        [Route("Transactions/GetTransactionsFilterByTransactionType")]
        [HttpGet]
        public List<TransactionEx> GetTransactionsFilterByTransactionType(string SearchText = "", string SortBy = "Id Desc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return svcTransaction.GetFilterTransactionPaged(SearchText, SortBy, PageNo, PageSize);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }

        [Route("Transactions/GetTransactionByType")]
        [HttpGet]
        public TransactionSearchViewModel GetTransactionByType(int id, string TID, string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new TransactionSearchViewModel() { Results = svcTransaction.GetPaged(id, TID, SearchText, SortBy, PageNo, PageSize), Count = svcTransaction.Count(id, TID, SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }

        [Route("Transactions/GetByPartnerIDAndTransactionType")]
        [HttpGet]
        public TransactionSearchViewModel GetByPartnerIDAndTransactionType(int id, int rid, string ttype = "11001", int PageNo = 0, int PageSize = 10)
        {
            try { 
            return new TransactionSearchViewModel() { Results = svcTransaction.GetByPartnerIDAndTransactionType(id, rid, PageNo, PageSize, ttype), Count = svcTransaction.GetByPartnerIDAndTransactionType(id, rid, PageNo, PageSize, ttype).Count };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }
        

        [Route("Transactions/GetChart")]
        [HttpGet]
        public Object GetChart(DateTime d1, DateTime d2)
        {
            try { 
            List<ChartsEx> cEx = svcTransaction.GetChartsList(d1, d2);
            List<object> d = new List<object>();
            List<object> c1 = new List<object>();
            foreach (ChartsEx ce in cEx)
            {
                c1 = new List<object>();
                c1.Add(new
                {
                    v = ce.productName
                });
                c1.Add(new
                {
                    v = ce.transacted,
                    f = ce.transacted
                });
                d.Add(new
                {
                    c = c1
                });
            }

            return d;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }
        


        //[Route("Test/GenerateAccountNumber")]
        //[HttpGet]
        //public object test(int cID = 0, int sID = 0, string curr = "", string cISO = "", int userID=0)
        //{
        //    return new Generator().generateCommissionsAccountNumber(cID,sID, curr, cISO, userID);
        //}
       

    }
}
