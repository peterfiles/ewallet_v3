﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System.Collections.Generic;
using System;

namespace Cooptavanza.Wallet.Controllers
{
    public class StatesApiController : ApiController
    {
        readonly StatesService svcStates;
       
        public StatesApiController(StatesService svcStates)
        {
            this.svcStates = svcStates;
        }

        [Route("States")]
        [HttpGet]
        public StatesSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new StatesSearchViewModel() { Results = svcStates.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcStates.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
}
        [Route("Admin/Dashboard/States")]
        [HttpGet]
        public StatesSearchViewModel AdminDashboardGet(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new StatesSearchViewModel() { Results = svcStates.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcStates.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }

        [Route("Admin/Dashboard/States/country")]
        [HttpGet]
        public List<StatesEx> GetStatesByCountryIdAdminDashboard(int id)
        {
            try { 
            return svcStates.GetStatesByCountryId(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }

        [Route("Admin/States/country")]
        [HttpGet]
        public List<StatesEx> GetStatesByCountryIdAdmin(int id)
        {
            try { 
            return svcStates.GetStatesByCountryId(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }

        [Route("States/country")]
        [HttpGet]
        public List<StatesEx> GetStatesByCountryId(int id)
        {
            try { 
            return svcStates.GetStatesByCountryId(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }




        [Route("States/{Id}")]
        [HttpGet]
        public StatesEx GetStates(int Id)
         {
            try { 
            return svcStates.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }


        [Route("States/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public StatesEx Save([FromBody] StatesEx n)
        {
            try { 
            svcStates.Save(n);
            return svcStates.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }
       

        [Route("States/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcStates.Remove(Id);
        }

        [Route("States/getStateFilterByCountryAndAgent")]
        [HttpGet]
        public object getStateFilterByCountryAndAgent(int id)
        {
            try { 
            return svcStates.getAgentStatesByCountryID(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "StatesAPi-Log");
                return null;
            }
        }
    }
}
