﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using System;

using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Cooptavanza.Wallet.Controllers
{
    public class LunexApicontroller : ApiController
    {

        readonly CustomerService svcCustomer;
   
        public LunexApicontroller(CustomerService svcCustomer)
        {
            this.svcCustomer = svcCustomer;
        }

        [Route("LunexTest")]
        [HttpGet]
        public string Test()
        {
            try
            {
                string method = "POST";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync("orders/83d52be7-2f91-4119-80a0-adb14fa14d2a?sku=7000&phone=2111111111&amount=16.00", method);
                return r;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/getSKU")]
        [HttpGet]
        public object getSKU()
        {
            try
            {
                string method = "GET";
                string data = "skus";
                string r = new WebRequestHelperLunex().WebGetRequest(data, method);
                XmlDocument doc = new XmlDocument();
                try
                {
                    doc.LoadXml(r);
                    var ret = JsonConvert.SerializeXmlNode(doc);
                    object re = JsonConvert.DeserializeXmlNode(ret);
                    //  var fs = JsonConvert.DeserializeObject<LEPListEx>(ret);

                    //   etvar ssd = ((JObject)ret).Children();
                    //   var stuff = ssd.Select(x => new { AreaCode = x.Path.Split('.')[1], List = x.First()["List"], State = x.Last()["state"] });
                    //var x = JsonConvert.DeserializeObject<LEPListEx>(r);

                    return re;
                }
                catch (Exception ex)
                {

                    return ex;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("CLAR")]//CustomLunexApiRequest
        [HttpPost]
        public object clar(LunexCustomURLEx d)
        {
            try
            {
                return new WebRequestHelperLunex().WebGetRequestCustom(d.url, d.m);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }

        }

        [Route("CLARM")]//CustomLunexApiRequestModed
        [HttpGet]
        public object clarm(string url="")
        {
            try
            {
                return new WebRequestHelperLunex().WebGetRequestCustom(url, "POST");
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }

        }

        [Route("LEP /neworder")]
        [HttpPost]
        public object newOrder()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                //   ret.
                // se.Message = ret.Contains("Message") != null ? ret.Contains("Message") : "";

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/getOrderstatus")]
        [HttpPost]
        public object getOrderStatus()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/voidTx")]
        [HttpPost]
        public object voidTx()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/ListProduct")]
        [HttpPost]
        public object ListProduct()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/ListAccount")]
        [HttpPost]
        public object ListAccount()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/getAccount")]
        [HttpPost]
        public object getAccount()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/createAccount")]
        [HttpPost]
        public object createAccount()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/updateAccount")]
        [HttpPost]
        public object updateAccount()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/ListSpeedDial")]
        [HttpPost]
        public object ListSpeedDial()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/updateSpeedDial")]
        [HttpPost]
        public object updateSpeedDial()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/DeleteSpeedDial")]
        [HttpPost]
        public object deleteSpeedDial()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/ListReguestPhone")]
        [HttpPost]
        public object ListRequestPhone()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("LEP/UpdateRegisterPhone")]
        [HttpPost]
        public object UpdateRegisterPhone()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/DeleteRegisterPhone")]
        [HttpPost]
        public object DeleteRegisterPhone()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/GetSeller")]
        [HttpPost]
        public object getSeller()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);


                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/ListTransaction")]
        [HttpPost]
        public object ListTransaction()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/PreOrder")]
        [HttpPost]
        public object PreOrder()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }


        [Route("LEP/PostOrder")]
        [HttpPost]
        public object PostOrder()
        {
            try
            {
                string method = "POST";
                string data = "orders/" + Guid.NewGuid() + "?sku=7000&phone=2111111111&amount=16.00";
                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);

                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

        [Route("CLARM")]//CustomLunexApiRequest
        [HttpPost]
        public object clarm(LunexCustomURLEx d)
        {
            try
            {
                return new WebRequestHelperLunex().WebGetRequestCustomCLARM(d.url, d.m);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }

        }

        [Route("TestConnection")]
        [HttpGet]
        public object tc(int t)
        {
            try
            {
                return new WebRequestHelperLunex().WebGetRequestCustomTest(t);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Lunex-Log");
                return null;
            }
        }

    }
}
