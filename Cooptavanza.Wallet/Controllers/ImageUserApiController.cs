﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Web;
using System.IO;

namespace Cooptavanza.Wallet.Controllers
{
    public class ImageUserApiController : ApiController
    {
        readonly ImageUserService svcImageUser;
        public ImageUserApiController(ImageUserService svcImageUser)
        {
            this.svcImageUser = svcImageUser;
        }

        //[Route("ImageUser")]
        //[HttpGet]
        //public ImageUserSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        //{
        //       return new ImageUserSearchViewModel() { Results = svcImageUser.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcImageUser.Count(SearchText) };
        //}


        [Route("ImageUser/{Id}")]
        [HttpGet]
        public ImageUserEx GetPerson(int Id)
         {
            try
            {
                return svcImageUser.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageUser-Log");
                return null;
            }
            //return null;
        }

        [Route("GetImageUserByUI/")]
        [HttpGet]
        public ImageUserEx GetUserById(int Id, int r)
        {
            try
            {
                return svcImageUser.GetByUserId(Id, r);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageUser-Log");
                return null;
            }
            //return null;
        }


        [Route("ImageUploadUser/save")]
        [HttpPost]
        public ImageUserEx UploadUserImage()
        {
            try
            {
                var form = HttpContext.Current.Request.Form;
                string userId = "0";
                string imageProfileId = "0";
                string userRole = "0";

                foreach (string key in form.AllKeys)
                {
                    if (key == "userId") { userId = form[key]; }
                    if (key == "imageUserId") { imageProfileId = form[key]; }
                    if (key == "userRole") { userRole = form[key]; }
                }
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                if (userId != "0")
                {

                    ImageUserEx n = new ImageUserEx();
                    n.id = Int32.Parse(imageProfileId);
                    n.userId = Int32.Parse(userId);
                    n.userRoles = Int32.Parse(userRole);
                    if (file != null && file.ContentLength > 0)
                    {
                        string filename = file.FileName;
                        //string folder = 
                        if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("../Upload/" + userRole + "_nc/" + userId + "/")))
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("../Upload/" + userRole + "_nc/" + userId + "/").ToString());
                        }

                        string path = HttpContext.Current.Server.MapPath("../Upload/" + userRole + "_nc/" + userId + "/");

                        string guid = "";
                        var profile = svcImageUser.Get(n.id);
                        if (profile != null)
                        {
                            guid = profile.imageString != null ? profile.imageString : "" + Guid.NewGuid();
                            File.Delete(path + guid + "." + filename.Split('.')[1].ToString());
                            guid = "" + Guid.NewGuid();
                        }
                        else { guid = "" + Guid.NewGuid(); }

                        string u = (path + guid + "." + filename.Split('.')[1]).ToString();

                        file.SaveAs(u);
                        n.imageString = guid;
                        n.path = "/Upload/" + userRole + "_nc/" + userId + "/" + guid + "." + filename.Split('.')[1];
                        svcImageUser.Save(n);
                        return svcImageUser.Get(n.id);
                    }
                    else { return null; }
                }
                else { return null; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageUser-Log");
                return null;
            }
        }

        [Route("ImageUser/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public ImageUserEx Save([FromBody] ImageUserEx n)
        {
            try
            {
                svcImageUser.Save(n);
                return svcImageUser.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageUser-Log");
                return null;
            }
        }
       

        [Route("ImageUser/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcImageUser.Remove(Id);
        }
    }
}
