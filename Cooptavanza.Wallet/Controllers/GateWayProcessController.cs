﻿using Cooptavanza.Wallet.Data;
using Cooptavanza.Wallet.Repository;
using Cooptavanza.Wallet.Service;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace Cooptavanza.Wallet.Controllers
{
    public class GateWayProcessController : GateWayProcessRequest
    {

        static GateWayProcessPaymentRequestEx gwpprExs;

        public GateWayProcessController(GateWayProcessPaymentRequestEx gwpprEx)
        {
            gwpprExs = gwpprEx;
        }

        private static HttpWebRequest CreateWebRequest(string url, string action)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }

        private static string formatBody(int process)
        {
            //sid and rcode are required and are set to default
            string format = string.Empty;
            format = processRequest(gwpprExs, process);
            return format;

        }

        private static string GetTag(string tagname, string value)
        {
            return string.Format("<{0}>{1}</{0}>", tagname, value);
        }

        private static XmlDocument CreateSoapEnvelope(int method)
        {
            XmlDocument soapEnvelop = new XmlDocument();
            string bodyInner = formatBody(method); // setting default temporarily

            soapEnvelop.LoadXml(string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?>
            <SOAP-ENV:Envelope
                xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/""
                xmlns:ns1=""https://cfmerc.epspci.com/soap/""
                xmlns:xsd=""http://www.w3.org/2001/XMLSchema""
                xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
                xmlns:SOAP-ENC=""http://schemas.xmlsoap.org/soap/encoding/""
                SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                <SOAP-ENV:Body>
                    {0}
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>", bodyInner));
            return soapEnvelop;
        }

        public string SubmitSOAPPaymentTransaction(string host, int method)
        {
            HttpWebRequest webRequest = CreateWebRequest(host, "action");
            XmlDocument env = CreateSoapEnvelope(method);
            string responseString;
            using (Stream stream = webRequest.GetRequestStream())
            {
                env.Save(stream);
            }
            try
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.GetResponse();
                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            }
            catch (WebException ex)
            {
                responseString = (new StreamReader(ex.Response.GetResponseStream())).ReadToEnd();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                responseString = string.Empty;
            }
            return responseString;

        }
        public static string cardType { get; set; }
        public static string CardTypeValue(string cardNo)
        {
            cardType = string.Empty;
            string[] regExForCardTypes =
            {
                "(^4)",//visa
                "(^5[1-5])|(^2[2-7])",//mastercard
                "(^3[4|7])",//americanexpress
                "(^3[0|6|8])",//dinersclub
                "(^60110[0-9])|(^6011[2-4][0-9])|(^601174)|(^60117[7-9])|(^6011[8|9][6-9])|(^6[4-5][4-9][0-9][0-9][0-9])"//discover
                //"^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"//jcb
            };
            for (int x = 0; x < regExForCardTypes.Count(); x++)
            {
                Regex rEx = new Regex(regExForCardTypes[x]);
                if (x == 0 && rEx.Match(cardNo).Success) { cardType = "visa"; }
                if (x == 1 && rEx.Match(cardNo).Success) { cardType = "mastercard"; }
                if (x == 2 && rEx.Match(cardNo).Success) { cardType = "amex"; }
                if (x == 3 && rEx.Match(cardNo).Success) { cardType = "dinersclub"; }
                if (x == 4 && rEx.Match(cardNo).Success) { cardType = "discover"; }
                //if (x == 5 && rEx.IsMatch(cardNo)) { cardType = "jcb"; }
            }

            return cardType;
        }

    }

}