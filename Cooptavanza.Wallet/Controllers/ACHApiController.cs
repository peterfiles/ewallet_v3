﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Cooptavanza.Wallet.Repository;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace Cooptavanza.Wallet.Controllers
{
    public class ACHApiController : ApiController
    {

        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly TransactionService svcTransaction;
        readonly CurrencyService svcCurr;
        readonly BankDepositService svcBankDeposit;
        readonly CustomerService svcCustomer;
        readonly CoopTransactionLogService svcCTLS;
        readonly EnrolledAccountService svcEnrolledAccounts;

        
        public ACHApiController(
            UserAccountService svcUserAccount, 
            OFAClistService svcOFAC, 
            TransactionService svcTransaction,
            CurrencyService svcCurr,
            BankDepositService svcBankDeposit,
            CustomerService svcCustomer,
            CoopTransactionLogService svcCTLS,
            EnrolledAccountService svcEnrolledAccounts)
        {
            this.svcUserAccount = svcUserAccount;
            this.svcTransaction = svcTransaction;
            this.svcOFAC = svcOFAC;
            this.svcCurr = svcCurr;
            this.svcBankDeposit = svcBankDeposit;
            this.svcCustomer = svcCustomer;
            this.svcCTLS = svcCTLS;
            this.svcEnrolledAccounts = svcEnrolledAccounts;
        }

        [Route("smsEmailNotification")]
        [HttpPost]
        public object sendEmailConfirmationACH(BankDepositACHTestEx d)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                    var TM = new TransferMoneyEx();
                var T = new TransactionEx();

                T.id = 1;
                T.receiversID = 1;
                T.status = "y";
                T.transactionTID = "p";
                T.transactionType = "q";
                T.userId = 1;
                T.userTID = "i";

                //   TM.benificiaryfullname = pValRname;
                TM.id = 1;
                TM.question = "a";
                TM.sendermobileno = "s";
                TM.status = "d";
                TM.transactionid = 1;
                TM.transactionTID = "as";
                TM.transactionType = "11001";
                //TM.benificiaryemail = pValSemail;
                TM.currencyfromiso = "ad";

                if (d.fullname == null)
                {
                    return "error";
                }
                else
                {
                    d.mobilenumber = d.mobilenumber.Replace(' ', '+');
                    return new ConfirmationSender().sendsmsEmailNotification(d);
                }
            }
            catch (Exception e)
            {
                sb.Append("ACH-Logs");
                new Generator().LogException(e, sb.ToString());
                return e;
            }

        }

        [Route("EmailWireTransfer")]
        [HttpGet]
        public object sendEmailConfirmationWireTransfer()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                var TM = new TransferMoneyEx();
                var T = new TransactionEx();
                T.id = 1;
                T.receiversID = 1;
                T.status = "y";
                T.transactionTID = "p";
                T.transactionType = "q";
                T.userId = 1;
                T.userTID = "i";


                TM.id = 1;
                TM.question = "a";
                TM.sendermobileno = "s";
                TM.status = "d";
                TM.transactionid = 1;
                TM.transactionTID = "as";
                TM.transactionType = "40002";
                TM.benificiaryemail = "coopdevteam@gmail.com";
                TM.currencyfromiso = "ad";
                TM.benificiaryfullname = "af";

                return new ConfirmationSender().Test(T, TM);
            }
            catch (Exception e)
            {
                sb.Append("ACH-Logs");
                new Generator().LogException(e, sb.ToString());
                return e;
            }
        }

        [Route("ACH/Transact")]
        [HttpPost]
        public object Transact(ACHTransactEx data)
        {
            try
            {
                TransactionEx tEx = new TransactionEx();
                BankDepositEx bdEx = svcBankDeposit.GetByRtCode(data.rtcode);
                CustomerEx custEx = svcCustomer.Get(data.userID);
                CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                CurrencyEx cEx = svcCurr.Get(bdEx.currenciesID);
                EnrolledAccountEx eaEx = svcEnrolledAccounts.GetByUserIDUserTIDAndCurrencyISO(custEx.id,custEx.tid,data.currencyfromiso);


                tEx.accountNumber = eaEx.accountnumber;
                tEx.balance = data.amount_purchase;
                tEx.credit = false;
                tEx.debit = true;
                tEx.currencyfromiso = data.currencyfromiso;
                tEx.currencytoiso = cEx.iso;
                tEx.dataCreated = DateTime.Now;
                tEx.description = "ACH Transfer By " + custEx.firstname + " " + custEx.lastname + " to Bank :" + bdEx.bankName + " --- Account No. : " + data.ach_account_no + " --- Amount ("+ data.currencyfromiso +"):" + data.amount_purchase;
                tEx.enrolledAccountsCurrencyID = eaEx.currenycid;
                tEx.enrolledAccountsID = eaEx.id;
                tEx.partnerId = bdEx.id;
                tEx.receiversID = 0;
                tEx.status = "PENDING";
                tEx.transactionTID = new Generator().generateTransactionNo("40001", svcTransaction.GetTransactionLastIDNumber(), cEx.iso);
                tEx.transactionType = "40001";
                tEx.userId = custEx.id;
                tEx.userTID = custEx.tid;
                svcTransaction.Save(tEx);
                //Debit to Giver
                ctlEx.amount = data.amount_purchase;
                ctlEx.currencyAccountFrom = data.currencyfromiso;
                ctlEx.currencyAccountTo = cEx.iso;
                ctlEx.date_created = DateTime.Now;
                ctlEx.giverTID = custEx.tid;
                ctlEx.giverUserId = custEx.id;
                ctlEx.giverUserRoles = 7;
                ctlEx.receiverTID = bdEx.rtcode;
                ctlEx.receiverUserId = bdEx.id;
                ctlEx.receiverUserRoles = 3;
                ctlEx.isCredit = false;
                ctlEx.transactionDescription = tEx.description;
                ctlEx.transactionTID = tEx.transactionTID;
                ctlEx.transactionType = tEx.transactionType;
                svcCTLS.Save(ctlEx);
                //Credit to Receiver
                ctlEx = new CoopTransactionLogEx();
                ctlEx.amount = data.amountto;
                ctlEx.currencyAccountFrom = data.currencyfromiso;
                ctlEx.currencyAccountTo = cEx.iso;
                ctlEx.date_created = DateTime.Now;
                ctlEx.giverTID = custEx.tid;
                ctlEx.giverUserId = custEx.id;
                ctlEx.giverUserRoles = 7;
                ctlEx.receiverTID = bdEx.rtcode;
                ctlEx.receiverUserId = bdEx.id;
                ctlEx.receiverUserRoles = 3;
                ctlEx.isCredit = true;
                ctlEx.transactionDescription = tEx.description;
                ctlEx.transactionTID = tEx.transactionTID;
                ctlEx.transactionType = tEx.transactionType;
                svcCTLS.Save(ctlEx);
                return "success";
            }catch(Exception ex)
            {
                new Generator().LogException(ex, "ACH-Logs");
                return "error";
            }
        }

    }
}
 