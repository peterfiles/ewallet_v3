﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace Cooptavanza.Wallet.Controllers
{
    public class MoneyTransferApicontroller : ApiController
    {
        readonly TransactionService svcTransaction;
        readonly PartnersService svcPartner;
        readonly PartnerCustomerService svcPartnerCustomer;
        readonly TransferMoneyService svcTransferMoney;
        public MoneyTransferApicontroller(
            TransactionService svcTransaction,
            PartnersService svcPartner,
            PartnerCustomerService svcPartnerCustomer,
            TransferMoneyService svcTransferMoney
            )
        {
            this.svcTransaction = svcTransaction;
            this.svcPartner = svcPartner;
            this.svcPartnerCustomer = svcPartnerCustomer;
            this.svcTransferMoney = svcTransferMoney;
        }



        [Route("MoneyTransfer/maketransaction")]
        [HttpPost]
        public ResponseEx makeTransaction([FromBody] ETransferEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                PartnerCustomerEx PC = new PartnerCustomerEx();
                PartnersEx P = svcPartner.getByPartnerCode(n.partnerCode);
                TransferMoneyEx TM = new TransferMoneyEx();
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                if (P != null)
                {
                    TransactionEx T = new TransactionEx();
                    var message = EH.HMAC_SHA256(MAH.requestTrasaction(n), P.key);
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTrasaction(n), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }

                    if (n.transactionType == "10002")
                    {
                        T.description = "Money tranfer to " + (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper() + " from " +
                            (n.sndFirstName).ToUpper() + " " + (n.sndLastName).ToUpper() + " transaction no: " + n.referenceNo;
                    }
                    else
                    {
                        RE.code = "RTF";
                        RE.date = DateTime.Now;
                        RE.data = new
                        {
                            referenceNo = n.referenceNo,
                            signature = n.signature,
                        };
                        RE.message = "WRONG TRANSACTION TYPE";
                        return RE;
                    }

                    bool r = svcTransaction.CheckTransactionId(n.referenceNo, P.id);
                    if (r == true)
                    {
                        RE.code = "RTF";
                        RE.date = DateTime.Now;
                        RE.data = new
                        {
                            referenceNo = n.referenceNo,
                            signature = n.signature,
                        };
                        RE.message = "REFERENCE NUMBER ALREADY EXIST";
                        return RE;
                    }

                    PC.accountnumber = n.sndAccntNo;
                    PC.firstname = n.sndFirstName;
                    PC.lastname = n.sndLastName;
                    PC.middlename = n.sndMiddleName;
                    PC.mobilenumber = n.sndMobileNo;
                    PC.partnerId = P.id;
                    PC.phonenumber = n.sndPhoneNo;
                    PC.suffix = n.sndSuffix;
                    PC.tid = "" + Guid.NewGuid();
                    PC.email = n.sndEmail;
                    PC.dateCreated = DateTime.Now;


                    svcPartnerCustomer.Save(PC);
                    PartnerCustomerEx PCE = svcPartnerCustomer.Get(PC.id);

                    T.accountNumber = n.sndAccntNo;
                    T.balance = n.amount;
                    T.credit = true;
                    T.currencyfromiso = n.currencyFrom;
                    T.currencytoiso = n.currencyTo;
                    T.dataCreated = n.date;
                    T.debit = false;


                    //T.enrolledAccountsCurrencyID
                    T.partnerId = P.id;
                    T.status = "PENDING";
                    //T.receiversID

                    T.transactionTID = n.referenceNo;
                    T.transactionType = n.transactionType;
                    T.userId = PC.id;
                    T.userTID = PC.tid;
                    T.dataCreated = DateTime.Now;
                    svcTransaction.Save(T);


                    TM.accountNumber = n.sndAccntNo;
                    TM.amountfrom = n.amount;
                    TM.amountto = n.amount;
                    TM.answer = n.answer;
                    TM.question = "Password";
                    TM.transactionType = n.transactionType;
                    TM.transactionTID = n.referenceNo;
                    TM.benificiaryemail = n.rcvEmail;
                    TM.benificiaryfullname = (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper() + " ";
                    // TM.benificiaryid = 
                    TM.benificiarymobileno = n.rcvMobileno;
                    TM.benificiaryemail = n.rcvEmail;
                    TM.currencyfromiso = n.currencyFrom;
                    TM.currencytoiso = n.currencyTo;
                    TM.transactionId = T.id;

                    TM.customerid = PCE.id;
                    TM.customertid = PCE.tid;
                    TM.dateCreated = DateTime.Now;
                    TM.partnerId = P.id;
                    TM.partnerName = P.finame;
                    TM.receiverFirstname = n.rcvFirstName;
                    TM.receiverLastname = n.rcvLastName;
                    TM.receiverMiddlename = n.rcvMiddleName;
                    TM.receiverSuffix = n.rcvSuffix;
                    TM.senderFirstName = n.sndFirstName;
                    TM.senderLastName = n.sndLastName;
                    TM.senderMiddleName = n.sndMiddleName;
                    TM.sendermobileno = n.sndMobileNo;
                    TM.status = "PENDING";
                    svcTransferMoney.Save(TM);
                    RE.code = "RTS";
                    RE.data = new
                    {
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        amounttransfered = n.amount,
                        currencyfrom = n.currencyFrom,
                        currencyto = n.currencyTo,
                        fees = "" + ((0.01 * (double)n.amount) + 5)
                    };
                    RE.message = "SUCCESS";
                    RE.date = DateTime.Now;

                    MoneyTransferNotification MTN = new MoneyTransferNotification();
                    MTN.rcvMTEmailNotifs(T, TM, n);
                    MTN.sndMTEmailNotifs(T, TM, n);

                }
                else
                {
                    RE.code = "RTF";
                    RE.date = DateTime.Now;
                    RE.data = new
                    {
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        partnerCode = n.partnerCode
                    };
                    RE.message = "NO PARTNER AVAILABLE";

                }
                return RE;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyTransfer-Log");
                return null;
            }
        }

       

        [Route("MoneyTransfer/transaction")]
        [HttpPost]
        public ResponseEx listTransaction([FromBody] RequestListEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            MessageAppenderHelper MAH = new MessageAppenderHelper();
            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTransactionList(n), E.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }

                    string dateStr = n.dateStart != null ? string.Format("{0:yyyy-MM-dd}", n.dateStart) : null;
                    string dateEn = n.dateEnd != null ? string.Format("{0:yyyy-MM-dd}", n.dateEnd) : null;
                    if (dateStr != null)
                    {
                        dateEn = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
                    }
                    List<TransactionEx> result = svcTransaction.GetByPartner(n.searchText, E.id, dateStr, dateEn, n.transactionType, n.referenceNo, n.orderBy, n.pageNo, n.pageSize);
                    int count = svcTransaction.CountByPartner(n.searchText, E.id, dateStr, dateEn, n.transactionType, n.referenceNo);

                    RE.code = "RTS";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        Result = result,
                        Count = count,
                        signature = n.signature
                    };
                    RE.message = "SUCCESS";
                    return RE;
                    
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    //  error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";

                new Generator().LogException(e, "MoneyTransfer-Log");
            }

            return RE;
        }

        [Route("MoneyTransfer/qoute")]
        [HttpPost]
        public ResponseEx qoute([FromBody] RequestQouteEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    //calculation of service

                    RE.code = "RTS";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {

                        signature = n.signature
                    };
                    RE.message = "SUCCESS";
                    return RE;
                   
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    //  error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";

                new Generator().LogException(e, "MoneyTransferAPI-Log");
            }

            return RE;
        }



        [Route("MoneyTransfer/status")]
        [HttpPost]
        public ResponseEx status([FromBody] RequestStatusEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                TransactionEx T = new TransactionEx();
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                string message = MAH.requestStatus(n);
                try
                {
                    PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                    if (E != null)
                    {
                        if (n.signature.ToUpper() == EH.HMAC_SHA256(MAH.requestStatus(n), E.key))
                        {
                            TransactionEx result = svcTransaction.GetStatusByPartner(E.id, n.referenceNo);

                            if (result != null)
                            {
                                RE.code = "RTS";
                                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                RE.data = new
                                {
                                    referenceno = n.referenceNo,
                                    status = result.status,
                                    trasactiontype = result.transactionType,
                                    signature = n.signature
                                };
                                RE.message = "SUCCESS";
                                return RE;
                            }
                            else
                            {
                                RE.code = "RTF";
                                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                RE.data = new
                                {
                                    referenceno = n.referenceNo,
                                    signature = n.signature
                                };
                                RE.message = "NO TRANSACTION FOUND";
                                return RE;
                            }


                        }
                        else
                        {
                            RE.code = "RTF";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                signature = n.signature
                            };
                            RE.message = "INVALID SIGNATURE";

                        }
                    }
                    else
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "PARTNER IS NOT ALLOWED";

                    }
                }
                catch (Exception e)
                {
                    RE.code = "RTT";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        //  error = "" + e.Message,
                        signature = n.signature
                    };
                    RE.message = "SYSTEM TIMED OUT";

                    Console.WriteLine(e);
                }

                return RE;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneyTransfer-Log");
                return null;
            }
        }
    }
}
