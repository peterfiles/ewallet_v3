﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Cooptavanza.Wallet.Repository;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace Cooptavanza.Wallet.Controllers
{
    public class MerchantApiController : ApiController
    {
        readonly MerchantService svcMerchant;
        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly UserServicesService svcUserServices;

        readonly PartnersService svcPartners;
        readonly AgentsService svcAgents;
        readonly UsersService svcUsers;
        
        readonly CoopTransactionLogService svcCTLS;
        readonly CoopEnrolledAccountService svcCEA;

        readonly CurrencyService svcCurr;

        readonly UserAccountActivitiesService svcUAA;
        public MerchantApiController(
            MerchantService svcMerchant, 
            UserAccountService svcUserAccount, 
            OFAClistService svcOFAC, 
            UserServicesService svcUserServices,
            PartnersService svcPartners,
            AgentsService svcAgents,
            UsersService svcUsers,
            CoopTransactionLogService svcCTLS,
            CoopEnrolledAccountService svcCEA,
            UserAccountActivitiesService svcUAA,
            CurrencyService svcCurr)
        {
            this.svcUserAccount = svcUserAccount;
            this.svcMerchant = svcMerchant;
            this.svcOFAC = svcOFAC;
            this.svcUserServices = svcUserServices;
            this.svcPartners = svcPartners;
            this.svcAgents = svcAgents;
            this.svcUsers = svcUsers;
            this.svcCTLS = svcCTLS;
            this.svcCEA = svcCEA;
            this.svcUAA = svcUAA;
            this.svcCurr = svcCurr;
        }

        [Route("Merchant")]
        [HttpGet]
        public MerchantSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new MerchantSearchViewModel() { Results = svcMerchant.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcMerchant.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Merchant-Log");
                return null;
            }
        }


        [Route("Merchant/{Id}")]
        [HttpGet]
        public MerchantEx GetPerson(int Id)
         {
            try
            {
                MerchantEx mEx = svcMerchant.Get(Id);
                UserAccountEx uaEx = svcUserAccount.GetAccountByUserId(5, mEx.id, mEx.tid);
                if (uaEx != null)
                {

                    mEx.username = uaEx.username;
                    mEx.password = new EncryptionHelper().Decrypt(uaEx.password);
                }
                return mEx;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Merchant-Log");
                return null;
            }
            //return null;
        }


        [Route("Merchant/adminsave")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public MerchantEx Save([FromBody] MerchantEx n)
        {
            try
            {
                var tempid = n.id;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "default";
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.email = n.email == null ? svcMerchant.CountMerchant() + "merchant@gmail.com" : n.email;
                bool debitToGiver = false;
                if (n.coopAccountListData.coopRootAccountUserIDAndRole != null)
                {
                    if (n.coopAccountListData.coopRootAccountUserIDAndRole.roleID == 3 && n.coopAccountListData.coopAccountListAndcurencyData != null)
                    {
                        debitToGiver = true;
                    }
                }

                MerchantEx p = svcMerchant.Get(n.id);
                if (p != null)
                {
                    if (p.id > 0)
                    {
                        n.address = n.address != null ? n.address : p.address;
                        n.city = n.city != null ? n.city : p.city;
                        n.cityid = n.cityid != null ? n.cityid : p.cityid;

                        n.country = n.country != null ? n.country : p.country;
                        n.countryid = n.countryid != null && n.countryid > 0 ? n.countryid : p.countryid;

                        n.state = n.state != null ? n.state : p.state;
                        n.stateid = n.stateid != null && n.stateid > 0 ? n.stateid : p.stateid;

                        List<AccountListEx> ceaEx = svcCEA.getCoopAccountList(p.id, 5);
                        n.currency = ceaEx.Count == 0 || ceaEx[0].currencyAccountTo == null ? "D0F" : ceaEx[0].currencyAccountTo;
                        n.currencyid = svcCurr.GetByISO(n.currency).id;

                        n.mobilenumber = n.mobilenumber != null ? n.mobilenumber : p.mobilenumber;
                        n.phonenumber = n.phonenumber != null ? n.phonenumber : p.phonenumber;

                        n.zipcode = n.zipcode != null ? n.zipcode : p.zipcode;
                    }
                }

                svcMerchant.Save(n);

                MerchantEx data = svcMerchant.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                UserServicesEx us = new UserServicesEx { user_id = n.id, roles_id = 5 };

                List<coopAccountListAndCurrencyData> coopALACD = n.coopAccountListData.coopAccountListAndcurencyData;
                coopRootAccountUserIDAndRole coopRAUAR = n.coopAccountListData.coopRootAccountUserIDAndRole;
                if (coopALACD != null)
                {
                    foreach (coopAccountListAndCurrencyData resData in coopALACD)
                    {
                        CoopEnrolledAccountEx ceaEx = new CoopEnrolledAccountEx();
                        ceaEx = svcCEA.getIdByUserIDRoleIDCurrency(n.id, 5, resData.currency.iso);
                        if (ceaEx.id == 0)
                        {
                            #region Coop enrolled Account EX;
                            ceaEx.id = 0;
                            ceaEx.accountDefault = true;
                            ceaEx.accountnumber = "";
                            ceaEx.currency = resData.currency.iso;
                            ceaEx.currenycid = resData.currency.id;
                            ceaEx.roleID = Convert.ToString(5);
                            ceaEx.userId = n.id;
                            ceaEx.userTID = n.tid;
                            #endregion Coop enrolled Account EX;

                            svcCEA.Save(ceaEx);
                            ceaEx.accountnumber = svcCEA.getAccountNumberForCoopRed(n.id, ceaEx.currenycid, 5, ceaEx.id);
                            svcCEA.Save(ceaEx);
                        }

                        if (resData.status.Equals("added"))
                        {
                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = resData.amount;
                            ctlEx.currencyAccountFrom = resData.currency.iso;
                            ctlEx.currencyAccountTo = resData.currency.iso;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = true;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 5;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + resData.amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "CREDIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);
                        }

                    }
                }

                if (debitToGiver)
                {
                    for (int q = 0; q < n.coopAccountListgiver.Length; q++)
                    {
                        object s = n.coopAccountListData.coopAccountListAndcurencyData.Find(x => x.currency.iso.Equals(n.coopAccountListgiver[q].currencyAccountTo));
                        if (s != null)
                        {
                            //if (clacd.currency.iso.Equals(n.coopAccountListgiver[q].currencyAccountTo))
                            //{
                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = n.coopAccountListgiver[q].amount;
                            ctlEx.currencyAccountFrom = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.currencyAccountTo = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = false;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 5;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + n.coopAccountListgiver[q].amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "DEBIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);
                            //}
                        }
                    }
                }

                List<compiledProductsToFilter> cptf = new List<compiledProductsToFilter>();
                if (n.compiledProductData != null)
                {
                    foreach (compiledProductData cpa in n.compiledProductData)
                    {
                        if (cptf.Exists(x => x.id == cpa.compiledProductsToFilter.id))
                        {
                            cptf.RemoveAt(cptf.FindIndex(y => y.id == cpa.compiledProductsToFilter.id));
                            cptf.Add(cpa.compiledProductsToFilter);
                        }
                        else
                        {
                            cptf.Add(cpa.compiledProductsToFilter);
                        }
                    }
                }

                if (cptf != null || cptf.Count > 0)
                {
                    foreach (compiledProductsToFilter cptf2 in cptf)
                    {
                        if (cptf2 != null)
                        {
                            if (cptf2.status.Equals("checked"))
                            {
                                us.product_id = cptf2.id;
                                svcUserServices.Save(us);
                                us.ID = 0;
                            }
                        }
                    }
                }

                UserAccountActivitiesEx UAA = new UserAccountActivitiesEx();

                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 5;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary

                    UAA.token_id = "" + Guid.NewGuid();
                    UAA.status = false;
                    UAA.userId = n.id;
                    UAA.userRole = 5;
                    UAA.dateCreated = DateTime.Now;

                    svcUserAccount.Save(ua);
                    svcUAA.Save(UAA);
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(5, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password.Equals("") ? x.password : password;
                    ua.user_id = n.id;
                    ua.roles_id = 5;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }
                return svcMerchant.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Merchant-Log");
                return null;
            }
        }
       

        [Route("Merchant/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcMerchant.Remove(Id);
        }

        [Route("Merchant/MerchantIfIsInOFAC")]
        [HttpPost]
        public bool checkIfMerchantIsInOFAC([FromBody] MerchantEx n)
        {
            try
            {
                string res = string.Empty;
                bool isVerified = false;
                bool isInOFAC = false;
                List<OFAClistEx> list = new List<OFAClistEx>();
                using (WebClient wb = new WebClient())
                {
                    wb.Headers.Add("Content-Type", "application/json");
                    res = wb.DownloadString("https://easyofac.com/api/nameSearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&first_name=" + n.firstname + "&last_name=" + n.lastname + "&test=1");
                    list = JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
                    if (list.Count > 0)
                    {
                        foreach (OFAClistEx d in list)
                        {
                            d.userID = n.id;
                            d.userCode = 1005;
                            d.userIsVerified = false;
                            d.isCompany = false;
                            svcOFAC.Save(d);
                        }
                        isInOFAC = true;
                    }
                    else
                    {
                        isInOFAC = false;
                        isVerified = true;
                    }
                }
                n.isInOFACList = isInOFAC;
                n.isVerified = isVerified;
                svcMerchant.Save(n);
                return isInOFAC;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Merchant-Log");
                return false;
            }
        }
    }
}
