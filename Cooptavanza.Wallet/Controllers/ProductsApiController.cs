﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace Cooptavanza.Wallet.Controllers
{
    public class ProductsApiController : ApiController
    {
        readonly ProductsService svcProducts;
        readonly ServiceFeeService svcServiceFee;

        public ProductsApiController(ProductsService svcProducts, ServiceFeeService svcServiceFee)
        {
            this.svcProducts = svcProducts;
            this.svcServiceFee = svcServiceFee;
        }

        [Route("Products")]
        [HttpGet]
        public ProductsSearchViewModel Get(string SearchText = "", string SortBy = "code Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new ProductsSearchViewModel() { Results = svcProducts.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcProducts.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Products-Log");
                return null;
            }
        }

        [Route("Products/{Id}")]
        [HttpGet]
        public ProductsEx GetProducts(int Id)
         {
            try
            {
                ProductsEx productsData = svcProducts.Get(Id);
                ServiceFeeEx sfx = svcServiceFee.GetByProductID(productsData.id);
                productsData.amount = sfx == null ? 0 : sfx.serviceFeeAmount;
                productsData.percentage = sfx == null ? 0 : sfx.serviceFeePercentage;
                return productsData;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Products-Log");
                return null;
            }
        }


        [Route("Products/adminsave")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public ProductsEx Save([FromBody] ProductsEx n)
        {
            try { 
            n.tokenid = n.id != 0 ? n.tokenid : "" + Guid.NewGuid();
            svcProducts.Save(n);
            ServiceFeeEx servicefee = new ServiceFeeEx();
            int serviceID = svcServiceFee.GetByProductID(n.id) == null ? 0: svcServiceFee.GetByProductID(n.id).id;
            servicefee.id = serviceID > 0 ? serviceID : 0;
            servicefee.productID = n.id;
            servicefee.dateCreated = DateTime.Now;
            servicefee.serviceFeeType = n.name;
            servicefee.serviceFeeAmount = Convert.ToDecimal(n.amount);
            servicefee.serviceFeePercentage = Convert.ToDecimal(n.percentage);
            svcServiceFee.Save(servicefee);
            return svcProducts.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Products-Log");
                return null;
            }

        }
       

        [Route("Products/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcProducts.Remove(Id);
        }
    }
}
