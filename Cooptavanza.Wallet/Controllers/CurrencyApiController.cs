﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;

namespace Cooptavanza.Wallet.Controllers
{
    public class CurrencyApiController : ApiController
    {
        readonly CurrencyService svcCurrency;
        readonly EnrolledAccountService svcEas;
        public static HttpClient test = new HttpClient();
        readonly ConvertedCurrencyService svcConvCurr;
        readonly CountryService svcCountry;

        public CurrencyApiController(
            CurrencyService svcCurrency,
            EnrolledAccountService svcEas, 
            ConvertedCurrencyService svcConvCurr,
            CountryService svcCountry)
        {
            this.svcCurrency = svcCurrency;
            this.svcEas = svcEas;
            this.svcConvCurr = svcConvCurr;
            this.svcCountry = svcCountry;
        }

        [Route("Currency")]
        [HttpGet]
        public CurrencySearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            return new CurrencySearchViewModel() { Results = svcCurrency.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcCurrency.Count(SearchText) };
        }


        [Route("Currency/{Id}")]
        [HttpGet]
        public CurrencyEx GetPerson(int Id)
        {
            return svcCurrency.Get(Id);
            //return null;
        }

        [Route("Currency/GetDefaultValueList")]
        [HttpGet]
        public List<CurrencyEx> GetDefaultValueList()
        {
            return svcCurrency.GetDefaultValueList();
        }

        [Route("Currency/GetDefaultValueListCustomer/{Id}")]
        [HttpGet]
        public List<CurrencyEx> GetDefaultValueListCustomer(int Id)
        {
            List<CurrencyEx> lCex = new List<CurrencyEx>();
            foreach(CurrencyEx cEx in svcCurrency.GetDefaultValueList())
            {
                if(svcEas.GetByIdAndISO(Id, cEx.iso).Count == 0) { lCex.Add(cEx); }
            }
            return lCex;
        }

        [Route("Customer/Dashboard/Currency/GetDefaultValueList")]
        [HttpGet]
        public List<CurrencyEx> AdminDashboardGetDefaultValueList()
        {
            return svcCurrency.GetDefaultValueList();
        }

        [Route("Currency/ConvertMoney")]
        [HttpGet]
        public object convertMoney(string fc="", string tc="", string a="")
        {
            try
            {
                object o = new object();
                //using (WebClient wc = new WebClient())
                //{
                //    var json = wc.DownloadString("http://apilayer.net/api/live?access_key=d2b29b38291f24623aa0bb0fcf517d29&currencies=" + tc + "&source=" + fc + "&format=1");
                //    JObject res = JObject.Parse(JsonConvert.DeserializeObject(json).ToString());
                //    JToken resJT = res["quotes"];
                //    ApiLayerResEx alrEx = new ApiLayerResEx();
                //    alrEx.quotes = resJT[fc + tc].ToString();
                //    decimal convAmnt = Convert.ToDecimal(alrEx.quotes) * Convert.ToDecimal(a);
                //    o = Convert.ToInt32(convAmnt);
                //}
                //Note: Get Sales tax rate for Every Country
                ConvertedCurrencyEx ccEx = svcConvCurr.getDataByQoute(fc + tc);
                decimal taxRate = Convert.ToDecimal(svcCountry.GetByCurrencyCode(tc).salestaxrate.ToString());
                decimal convAmnt = Convert.ToDecimal(ccEx.amount) * Convert.ToDecimal(a);
                decimal amntToAdd = Convert.ToDecimal(a) * taxRate;
                decimal amntToPay = Convert.ToDecimal(a) + amntToAdd;
                o = new
                {
                    convAmt = Convert.ToInt32(convAmnt),
                    amntToPay = amntToPay
                };
                return o;
            }catch(Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        [Route("Currency/UpdateCurrencyByCountry")] //Suppose to Execute Every 12:Midnight or everyday
        [HttpGet]
        public object ucbc()
        {
            try
            {
                ConvertedCurrencyEx ccEx = new ConvertedCurrencyEx();
                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString("http://apilayer.net/api/live?access_key=a5aac613bcc3a012369f958727be8da8&source=USD");
                    JToken outer = JToken.Parse(json);

                    JObject inner = outer["quotes"].Value<JObject>();
                    ccEx.timestamp = outer["timestamp"].ToString();
                    ccEx.@base = outer["source"].ToString();

                    List<string> keys = inner.Properties().Select(p => p.Name).ToList();
                    List<string> values = inner.Properties().Select(q => q.Value.ToString()).ToList();

                    bool toUpdate = svcConvCurr.Count() == 0 ? false:true;

                    if (toUpdate)
                    {
                        svcConvCurr.RefreshConvertedCurrency();
                    }
                    
                    for(int x = 0; x < keys.Count; x++)
                    {
                        ccEx.id = 0;
                        ccEx.qoutes = keys[x];
                        ccEx.amount = Convert.ToDecimal(double.Parse(values[x]));
                        svcConvCurr.Save(ccEx);
                    }
                    return outer["source"].ToString();
                }
            }
            catch(Exception ex)
            {
                return ex;
            }
        }
        //[Route("Currency/GetConversionDetails")]
        //[HttpGet]
        //public void Result()
        //{
        //    ////RunAPIConversion().Wait();
        //    //test.BaseAddress = new Uri("https://api.fixer.io/");
        //    //test.DefaultRequestHeaders.Accept.Add(
        //    //    new MediaTypeWithQualityHeaderValue("application/json"));

        //    //HttpResponseMessage response = test.GetAsync("latest?base=USD").Result;

        //    //if (response.IsSuccessStatusCode)
        //    //{
        //    //    // var data = response.Content.ReadAsAsync();
        //    //    Console.WriteLine(response);

        //    //}
        //    //else
        //    //{
        //    //    //MessageBox.Show("Error Code" +
        //    //    //response.StatusCode + " : Message - " + response.ReasonPhrase);
        //    //}
        //    //Generate();
        //    //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //    //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.fixer.io/latest?base=USD");
        //    //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    //request.ContentType = "application/json; charset=utf-8";
        //    //request.Method = "GET";

        //    //Console.WriteLine(request.GetResponse());
        //    //Stream stream = MemoryStream();

        //    //    using (var client = new HttpClient(new HttpClientHandler { AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate }))
        //    //    {
        //    //        client.BaseAddress = new Uri("https://api.fixer.io/");
        //    //        HttpResponseMessage response = client.GetAsync("latest?base=USD").Result;
        //    //        response.EnsureSuccessStatusCode();
        //    //        string result = response.Content.ReadAsStringAsync().Result;
        //    //        Console.WriteLine("Result: " + result);
        //    //    }
        //    using (WebClient wc = new WebClient())
        //    {
        //        var json = wc.DownloadString("https://api.fixer.io/latest?base=USD");
        //        Console.WriteLine(json);
        //    }
        //}

        //static void Generate()
        //{
        //    RunAsync().Wait();
        //}

        //static async Task RunAsync()
        //{
        //    // New code:
        //    test.BaseAddress = new Uri("https://api.fixer.io/");
        //    test.DefaultRequestHeaders.Accept.Clear();
        //    test.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //    string dd = string.Empty;
        //    string res = string.Empty;
        //    try
        //    {
        //        var url = await CreateDataSync(dd);
        //        res = await GetCurrencies(url.PathAndQuery);
        //        Console.WriteLine(res);
        //    }catch(Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }

        //    Console.ReadLine();
        //}
        //static async Task<Uri> CreateDataSync(string product)
        //{
           
        //    HttpResponseMessage response = await test.PostAsJsonAsync("latest?base=USD", product);
        //    response.EnsureSuccessStatusCode();

        //    // return URI of the created resource.
        //    return response.Headers.Location;
        //}

        //static async Task<string> GetCurrencies(string path)
        //{
        //    string data = string.Empty;
        //    HttpResponseMessage response = await test.GetAsync(path);
        //    if (response.IsSuccessStatusCode)
        //    {
        //        data = await response.Content.ReadAsAsync<string>();
        //    }
        //    return data;
        //}
        ////static async Task<Uri> CreateProductAsync()
        ////{
        ////    string s = string.Empty;
        ////    HttpResponseMessage response = test.PostAsJsonAsync("/latest?base=USD", s).Result;
        ////    response.EnsureSuccessStatusCode();

        ////    // return URI of the created resource.
        ////    return response.Headers.Location;
        ////}

        ////static async Task RunAPIConversion()
        ////{
        ////    test.BaseAddress = new Uri("https://api.fixer.io");
        ////    test.DefaultRequestHeaders.Accept.Clear();
        ////    test.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        ////    var url = await CreateProductAsync();
        ////}

        ////static async Task<string> GetMoneyConversionDetails(string path)
        ////{
        ////    string d = string.Empty;
        ////    HttpResponseMessage response = await test.GetAsync(path);
        ////    if (response.IsSuccessStatusCode)
        ////    {
        ////        d = await response.Content.ReadAsStringAsync();
        ////    }
        ////    return d;
        ////}

        ////[Route("Currency/Save")]
        ////[HttpPost]
        ////// [Authorize(Roles = "administrators")]
        ////public CurrencyEx Save([FromBody] CurrencyEx n)
        ////{
        ////    svcCurrency.Save(n);
        ////    return svcCurrency.Get(n.id);
        ////}


        ////[Route("Currency/{Id}")]
        ////[HttpDelete]
        //////[Authorize(Roles = "administrators")]
        ////public void Delete(int Id)
        ////{
        ////    svcCurrency.Remove(Id);
        ////}
    }
}
