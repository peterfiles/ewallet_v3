﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;

namespace Cooptavanza.Wallet.Controllers
{
    public class BillerTypeApiController : ApiController
    {
        readonly BillerTypeService svcBillerType;
        
        public BillerTypeApiController(BillerTypeService svcBillerType)
        {
            this.svcBillerType = svcBillerType;
        }

        [Route("BillerType")]
        [HttpGet]
        public BillerTypeSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new BillerTypeSearchViewModel() { Results = svcBillerType.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcBillerType.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "BillerType-Log");
                return null;
            }
        }


        [Route("BillerType/{Id}")]
        [HttpGet]
        public BillerTypeEx GetBillerType(int Id)
        {
            try
            {
                return svcBillerType.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "BillerType-Log");
                return null;
            }
            //return null;
        }

       
    }
}
