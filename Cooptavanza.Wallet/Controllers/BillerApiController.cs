﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;
using System.Text;

namespace Cooptavanza.Wallet.Controllers
{
    public class BillerApicontroller : ApiController
    {
        //readonly DummyLoadService svcDummyLoad;
        readonly BillerService svcBiller;
        readonly BillerAccountService svcBillerAccount;

        public BillerApicontroller(BillerService svcBiller, BillerAccountService svcBillerAccount)
        {
            this.svcBiller = svcBiller;
            this.svcBillerAccount = svcBillerAccount;
        }
        [Route("Biller")]
        [HttpGet]
        public BillerSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try {
                return new BillerSearchViewModel() { Results = svcBiller.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcBiller.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Biller-Log");
                return null;
            }
        }


        [Route("Biller/{Id}")]
        [HttpGet]
        public BillerEx GetBiller(int Id)
        {
            try
            {
                return svcBiller.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Biller-Log");
                return null;
            }
            //return null;
        }


        [Route("Biller/adminsave")]
        [HttpPost]
        public object AdminSave([FromBody] BillerEx n)
        {
            try
            {
                if (n.id > 0)
                {
                    if (n.currency != null)
                    {
                        n.currencyid = n.currency[0].id;
                        n.currencyiso = n.currency[0].iso;
                    }
                    svcBiller.Save(n); //Update
                    addBillerAccounts(n);
                }
                else
                {
                    n.tid = "" + Guid.NewGuid();
                    if (n.currency != null)
                    {
                        n.currencyid = n.currency[0].id;
                        n.currencyiso = n.currency[0].iso;
                    }
                    svcBiller.Save(n);
                    addBillerAccounts(n);
                }
                return "success";
            }catch(Exception ex)
            {
                new Generator().LogException(ex, "Biller-Log");
                return ex;
            }
            //return svcBiller.Get(n.id);
        }
        [Route("Biller/{Id}")]
        [HttpDelete]
        public void Delete(int Id)
        {
            svcBiller.Remove(Id);
        }

        public object addBillerAccounts(BillerEx e)
        {
            try
            {
                foreach (CurrencyEx cEx in e.currency)
                {
                    BillerAccountEx baEx = new BillerAccountEx();
                    //baEx.billerAccount1 = new Generator().getAccount(e.countryid, e.stateid) + "-" +  + cEx.iso;
                    baEx = svcBillerAccount.GetBillerByCountryStateBranchISO(e.countryid, e.stateid, e.branch, cEx.iso);
                    if (baEx == null)
                    {
                    baEx = new BillerAccountEx();
                        baEx.billerBranch = e.branch;
                        baEx.billerID = e.id;
                        baEx.billerTID = e.tid;
                        baEx.billerType = e.billerType;
                        baEx.countryID = e.countryid;
                        baEx.stateID = e.stateid;
                        baEx.currencyID = cEx.id;
                        baEx.currencyISO = cEx.iso;
                        baEx.billerAccount1 = new Generator().generateBillerAccountNumberByCountryStateBranchAndISO(e.countryid, e.stateid, e.branch, cEx.iso, baEx.billerType,svcBillerAccount.CountBillerAccount());
                        svcBillerAccount.Save(baEx);
                    }
                }
                return "success";
            } catch (Exception ex) {
                new Generator().LogException(ex, "Biller-Log");
                return "Error Adding Biller Account.";
            }
        }

        [Route("Biller/GetBillerCountry")]
        [HttpGet]
        public object GetBillerCountry()
        {
            try
            {
                return svcBiller.GetBillerCountry();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Biller-Log");
                return null;
            }
        }

        [Route("Biller/GetBillerTypeByCountry")]
        [HttpGet]
        public object GetBillerTypeByCountry(int id)
        {
            try
            {
                return svcBiller.GetBillerTypeByCountry(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Biller-Log");
                return null;
            }
        }

        [Route("Biller/GetBillerByCountryAndType")]
        [HttpGet]
        public object GetBillerByCountryAndType(int id, int bTID)
        {
            try
            {
                return svcBiller.GetBillerByCountryAndType(id, bTID);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Biller-Log");
                return null;
            }
        }
    }
}
