﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;


namespace Cooptavanza.Wallet.Controllers
{
    public class EnrolledAccountController : ApiController
    {
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly CustomerService svcCustomer;

        public EnrolledAccountController(
            CustomerService svcCustomer,
            EnrolledAccountService svcEnrolledAccount
            )
        {
            this.svcCustomer = svcCustomer;
            this.svcEnrolledAccount = svcEnrolledAccount;
        }

      
        [Route("EnrolledAccount")]
        [HttpPost]
        public object Save([FromBody] EnrolledAccountEx n)
        {
            try
            {
                EnrolledAccountEx eaEx = new EnrolledAccountEx();
                if (n.currenycid > 0)
                {
                    eaEx = svcEnrolledAccount.GetByUserIDAndCurrencyID(n.userId, n.currency);
                    if (eaEx != null)
                    {
                        return "Account already added.";
                    }
                    else
                    {
                        svcEnrolledAccount.Save(n);
                        eaEx = svcEnrolledAccount.Get(n.id);
                    }
                }
                return eaEx;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "EnrolledAccount-Log");
                return null;
            }
        }

        [Route("EnrolledAccount/{Id}")]
        [HttpGet]
        public CustomerEx GetPerson(int Id)
        {
            try
            {
                return svcCustomer.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "EnrolledAccount-Log");
                return null;
            }
        }

        [Route("EnrolledAccount/ui/{Id}")]
        [HttpGet]
        public List<EnrolledAccountEx> GetByUserId(int Id)
        {
            try
            {
                return svcEnrolledAccount.GetByUserId(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "EnrolledAccount-Log");
                return null;
            }
        }

    }
}
