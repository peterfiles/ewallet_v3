﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Web;
using System.Net.Http;
using System.IO;

namespace Cooptavanza.Wallet.Controllers
{
    public class ImageProfileApiController : ApiController
    {
        readonly ImageProfileService svcImageProfile;
      
        public ImageProfileApiController(ImageProfileService svcImageProfile)
        {
            this.svcImageProfile = svcImageProfile;
        }

        [Route("ImageProfile")]
        [HttpGet]
        public ImageProfileSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new ImageProfileSearchViewModel() { Results = svcImageProfile.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcImageProfile.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageProfile-Log");
                return null;
            }
        }

        [Route("ImageUploadProfile/save")]
        [HttpPost]
        public ImageProfileEx UploadProfileImage()
        {
            try
            {
                var form = HttpContext.Current.Request.Form;
                string userId = "0";
                string imageProfileId = "0";

                foreach (string key in form.AllKeys)
                {
                    if (key == "userId") { userId = form[key]; }
                    if (key == "imageProfileId") { imageProfileId = form[key]; }
                }
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                if (userId != "0")
                {

                    ImageProfileEx n = new ImageProfileEx();
                    n.id = Int32.Parse(imageProfileId);
                    n.userId = Int32.Parse(userId);

                    if (file != null && file.ContentLength > 0)
                    {
                        string filename = file.FileName;
                        //string folder = 
                        if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("../Upload/" + userId + "/")))
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("../Upload/" + userId + "/").ToString());
                        }

                        string path = HttpContext.Current.Server.MapPath("../Upload/" + userId + "/");

                        string guid = "";
                        var profile = svcImageProfile.Get(n.id);
                        if (profile != null)
                        {
                            guid = profile.imageString != null ? profile.imageString : "" + Guid.NewGuid();
                            File.Delete(path + guid + "." + filename.Split('.')[1].ToString());
                            guid = "" + Guid.NewGuid();
                        }
                        else { guid = "" + Guid.NewGuid(); }

                        string u = (path + guid + "." + filename.Split('.')[1]).ToString();
                        file.SaveAs(u);
                        n.imageString = guid;
                        n.path = "/Upload/" + userId + "/" + guid + "." + filename.Split('.')[1];
                        svcImageProfile.Save(n);

                        return svcImageProfile.Get(n.id);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageProfile-Log");
                return null;
            }
        }

        [Route("GetProfileByUI/{Id}")]
        [HttpGet]
        public ImageProfileEx GetProfileById(int Id)
        {
            try
            {
                return svcImageProfile.GetByUserId(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageProfile-Log");
                return null;
            }
            //return null;
        }

        [Route("ImageProfile/{Id}")]
        [HttpGet]
        public ImageProfileEx GetPerson(int Id)
         {
            try
            {
                return svcImageProfile.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageProfile-Log");
                return null;
            }
            //return null;
        }


        [Route("ImageProfile/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public ImageProfileEx Save([FromBody] ImageProfileEx n)
        {
            try
            {
                svcImageProfile.Save(n);
                return svcImageProfile.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageProfile-Log");
                return null;
            }
        }
       

        [Route("ImageProfile/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcImageProfile.Remove(Id);
        }
    }
}
