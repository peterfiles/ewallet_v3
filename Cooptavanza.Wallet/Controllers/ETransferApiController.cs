﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace Cooptavanza.Wallet.Controllers
{
    public class ETransferApicontroller : ApiController
    {
        readonly TransactionService svcTransaction;
        readonly PartnersService svcPartner;
        readonly PartnerCustomerService svcPartnerCustomer;
        readonly TransferMoneyService svcTransferMoney;
        readonly CustomerService svcCustomer;
        readonly CurrencyService svcCurrency;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly TransactionTypeService svcTransactionType;

        public ETransferApicontroller(
            TransactionService svcTransaction,
            PartnersService svcPartner,
            PartnerCustomerService svcPartnerCustomer,
            TransferMoneyService svcTransferMoney,
            CustomerService svcCustomer,
            CurrencyService svcCurrency,
            EnrolledAccountService svcEnrolledAccount,
            TransactionTypeService svcTransactionType
            )
        {
            this.svcTransaction = svcTransaction;
            this.svcPartner = svcPartner;
            this.svcPartnerCustomer = svcPartnerCustomer;
            this.svcTransferMoney = svcTransferMoney;
            this.svcCustomer = svcCustomer;
            this.svcCurrency = svcCurrency;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcTransactionType = svcTransactionType;
        }



        [Route("ETransfer")]
        [HttpPost]
        public ResponseEx makeTransaction([FromBody] ETransferEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                PartnerCustomerEx PC = new PartnerCustomerEx();
                PartnersEx P = svcPartner.getByPartnerCode(n.partnerCode);
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                TransferMoneyEx TM = new TransferMoneyEx();
                if (P != null)
                {
                    TransactionEx T = new TransactionEx();
                    var message = EH.HMAC_SHA256(MAH.requestTrasaction(n), P.key);
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTrasaction(n), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }

                    if (n.transactionType == "11002")
                    {
                        T.transactionTID = "" + Guid.NewGuid();

                        T.description = "Email money tranfer to " + (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper() + " from " +
                            (n.sndFirstName).ToUpper() + " " + (n.sndLastName).ToUpper() + " transaction no: " + T.transactionTID;

                    }
                    else if (n.transactionType == "11001")
                    {
                        //TE.partnerId = 
                        T.transactionTID = "" + Guid.NewGuid();

                        T.description = "SMS money tranfer to " + (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper() + " from " +
                            (n.sndFirstName).ToUpper() + " " + (n.sndLastName).ToUpper() + " transaction no: " + T.transactionTID;
                    }
                    else
                    {
                        RE.code = "RTF";
                        RE.date = DateTime.Now;
                        RE.data = new
                        {
                            referenceNo = n.referenceNo,
                            signature = n.signature,
                        };
                        RE.message = "WRONG TRANSACTION TYPE";
                        return RE;
                    }
                    bool r = svcTransaction.CheckTransactionId(n.referenceNo, P.id);
                    if (r == true)
                    {
                        RE.code = "RTF";
                        RE.date = DateTime.Now;
                        RE.data = new
                        {
                            referenceNo = n.referenceNo,
                            signature = n.signature,
                        };
                        RE.message = "REFERENCE NUMBER ALREADY EXIST";
                        return RE;
                    }
                    PC.accountnumber = n.sndAccntNo;
                    PC.firstname = n.sndFirstName;
                    PC.lastname = n.sndLastName;
                    PC.middlename = n.sndMiddleName;
                    PC.mobilenumber = n.sndMobileNo;
                    PC.partnerId = P.id;
                    PC.phonenumber = n.sndPhoneNo;
                    PC.suffix = n.sndSuffix;
                    PC.tid = "" + Guid.NewGuid();
                    PC.email = n.sndEmail;
                    PC.dateCreated = DateTime.Now;

                    svcPartnerCustomer.Save(PC);
                    PartnerCustomerEx PCE = svcPartnerCustomer.Get(PC.id);

                    T.accountNumber = n.sndAccntNo;
                    T.balance = n.amount;
                    T.credit = false;
                    T.currencyfromiso = n.currencyFrom;
                    T.currencytoiso = n.currencyTo;
                    T.dataCreated = n.date;
                    T.debit = true;
                    T.enrolledAccountsCurrencyID = svcCurrency.GetByISO(n.currencyFrom).id;
                    CustomerEx c = svcCustomer.GetCheckEmail(n.sndEmail);
                    T.enrolledAccountsID = svcEnrolledAccount.GetByUserIDUserTIDAndCurrencyISO(c.id, c.tid, n.currencyFrom).id;


                    //T.enrolledAccountsCurrencyID
                    T.partnerId = P.id;
                    T.status = "PENDING";
                    //T.receiversID

                    //T.transactionTID = n.referenceNo;
                    T.transactionType = n.transactionType;
                    T.userId = c.id;
                    T.userTID = c.tid;
                    T.dataCreated = DateTime.Now;
                    TransactionEx t = new TransactionEx();
                    if (n.transactionType == "11001")
                    {
                        CustomerEx rcvC = new CustomerEx();
                        TransactionEx TE = new TransactionEx();
                        rcvC = svcCustomer.GetByMobileNumber(n.rcvMobileno);
                        if (rcvC != null)
                        {
                            var cu = svcCurrency.GetByISO(n.currencyTo);
                            EnrolledAccountEx eaEXC = svcEnrolledAccount.GetByUserIDAndCurrencyID(rcvC.id, n.currencyTo);
                            if (eaEXC != null)
                            {
                                TE.accountNumber = eaEXC.accountnumber;
                                TE.enrolledAccountsCurrencyID = cu.id;
                                TE.enrolledAccountsID = eaEXC.id;
                            }
                            else
                            {
                                var e = new EnrolledAccountEx();

                                e.currenycid = cu.id;
                                e.currency = n.currencyTo;
                                e.accountDefault = false;
                                e.userId = rcvC.id;
                                e.userTID = rcvC.tid;
                                svcEnrolledAccount.Save(e);
                                var ss = svcEnrolledAccount.GetByUserIDAndCurrencyID(rcvC.id, n.currencyTo);
                                TE.accountNumber = ss.accountnumber;
                                TE.enrolledAccountsCurrencyID = cu.id;
                                TE.enrolledAccountsID = ss.id;



                            }
                            TE.balance = n.amount;
                            TE.credit = true;
                            TE.debit = false;
                            TE.currencyfromiso = n.currencyFrom;
                            TE.currencytoiso = n.currencyTo;
                            TE.dataCreated = DateTime.Now;
                            //TE.description = "SMS money tranfer to " + (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper();
                            //TE.partnerId =
                            TE.description = "SMS money tranfer to " + (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper() + " from " +
                            (n.sndFirstName).ToUpper() + " " + (n.sndLastName).ToUpper() + " transaction no: " + T.transactionTID;
                            TE.status = "CLEARED";
                            TE.transactionType = "11001";
                            TE.userId = rcvC.id;
                            TE.userTID = rcvC.tid;
                            svcTransaction.Save(TE);

                        }
                        else
                        {
                            svcTransaction.Save(T);
                        }
                        T = TE != null && TE.id != 0 ? TE : T;
                        //Deduct to Sender
                        CustomerEx cEx = svcCustomer.GetByMobileNumber(n.sndMobileNo);
                        EnrolledAccountEx eEx = svcEnrolledAccount.GetByUserIDUserTIDAndCurrencyISO(cEx.id, cEx.tid, n.currencyFrom);
                        TransactionEx tEx = new TransactionEx();
                        if (cEx != null && eEx != null)
                        {
                            tEx.accountNumber = eEx.accountnumber;
                            tEx.balance = n.amountConverted;
                            tEx.currencyfromiso = n.currencyFrom;
                            tEx.currencytoiso = n.currencyTo;
                            tEx.credit = false;
                            tEx.debit = true;
                            tEx.description = "SMS money tranfer From " + (n.sndFirstName).ToUpper() + " " + (n.sndLastName).ToUpper();
                            tEx.enrolledAccountsCurrencyID = eEx.currenycid;
                            tEx.enrolledAccountsID = eEx.id;
                            tEx.partnerId = 0;
                            tEx.receiversID = 0;
                            tEx.status = "CLEARED";
                            tEx.transactionTID = "" + Guid.NewGuid();
                            tEx.transactionType = "11001";
                            tEx.userId = cEx.id;
                            tEx.userTID = cEx.tid;
                            tEx.dataCreated = DateTime.Now;
                            svcTransaction.Save(tEx);
                        }
                        //tEx.accountNumber = svcE
                        //new Generator().sendSMSMoney(t, TM, n.sndMobileNo);
                    }
                    else
                    {
                        svcTransaction.Save(T);
                        //new Generator().sendSMSMoney(T, TM, n.sndMobileNo);
                    }
                    TM.accountNumber = n.sndAccntNo;
                    TM.amountfrom = n.amount;
                    TM.amountto = n.amount;
                    TM.answer = n.answer;
                    TM.question = n.question;
                    TM.transactionType = n.transactionType;
                    TM.transactionTID = T.transactionTID;
                    TM.benificiaryemail = n.rcvEmail;
                    TM.benificiaryfullname = (n.rcvFirstName).ToUpper() + " " + (n.rcvLastName).ToUpper() + " ";
                    // TM.benificiaryid = 
                    TM.benificiarymobileno = n.rcvMobileno;
                    TM.benificiaryemail = n.rcvEmail;
                    TM.currencyfromiso = n.currencyFrom;
                    TM.currencytoiso = n.currencyTo;
                    TM.transactionId = T.id;

                    TM.customerid = T.userId;
                    TM.customertid = T.userTID;
                    TM.dateCreated = DateTime.Now;
                    TM.partnerId = P.id;
                    TM.partnerName = P.finame;
                    TM.receiverFirstname = n.rcvFirstName;
                    TM.receiverLastname = n.rcvLastName;
                    TM.receiverMiddlename = n.rcvMiddleName;
                    TM.receiverSuffix = n.rcvSuffix;
                    TM.senderFirstName = n.sndFirstName;
                    TM.senderLastName = n.sndLastName;
                    TM.senderMiddleName = n.sndMiddleName;
                    TM.sendermobileno = n.sndMobileNo;
                    TM.status = n.transactionType == "11001" ? "CLEARED" : "PENDING";
                    svcTransferMoney.Save(TM);
                    RE.code = "RTS";
                    RE.data = new
                    {
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        amounttransfered = n.amount,
                        currencyfrom = n.currencyFrom,
                        currencyto = n.currencyTo,
                        fees = "" + ((0.01 * (double)n.amount) + 5)
                    };
                    RE.message = "SUCCESS";
                    RE.date = DateTime.Now;

                    if (n.transactionType == "11001")
                    {
                        ETransferNotification ETN = new ETransferNotification();
                        ETN.sndrETransferSMSNotifs(T, TM, n);
                        ETN.rcvrETransferSMSNotifs(T, TM, n);
                    }
                    else
                    {
                        ETransferNotification ETN = new ETransferNotification();
                        ETN.EmailNotification(T, TM, n);
                        ETN.sndETransferEmailNotifs(T, TM, n);
                    }


                    //RE.code = EH.HMAC_SHA256((P.partnerCode).ToUpper(), (P.key).ToUpper());
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = DateTime.Now;
                    RE.data = new
                    {
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        partnerCode = n.partnerCode
                    };
                    RE.message = "NO PARTNER AVAILABLE";

                }
                return RE;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ETransferAPI-Log");
                return null;
            }
        }

     


        [Route("ETransfer/transaction")]
        [HttpPost]
        public ResponseEx listTransaction([FromBody] RequestListEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            MessageAppenderHelper MAH = new MessageAppenderHelper();
            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTransactionList(n), E.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }

                    string dateStr = n.dateStart != null ? string.Format("{0:yyyy-MM-dd}", n.dateStart) : null;
                    string dateEn = n.dateEnd != null ? string.Format("{0:yyyy-MM-dd}", n.dateEnd) : null;
                    if (dateStr != null)
                    {
                        dateEn = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
                    }
                    List<TransactionEx> result = svcTransaction.GetByPartner(n.searchText, E.id, dateStr, dateEn, n.transactionType, n.referenceNo, n.orderBy, n.pageNo, n.pageSize);
                    int count = svcTransaction.CountByPartner(n.searchText, E.id, dateStr, dateEn, n.transactionType, n.referenceNo);

                    RE.code = "RTS";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        Result = result,
                        Count = count,
                        signature = n.signature
                    };
                    RE.message = "SUCCESS";
                  
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    //  error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";

                new Generator().LogException(e, "ETransferApi-Log");
            }

            return RE;
        }

        [Route("ETransfer/qoute")]
        [HttpPost]
        public ResponseEx qoute([FromBody] RequestQouteEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            MessageAppenderHelper MAH = new MessageAppenderHelper();

            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    var x = svcTransactionType.GetByCode(n.transactionType);
                    if (x == null)
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID TRANSACTION";
                        return RE;
                    }
                    if(n.signature == null)
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }
                    //calculation of service
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestQoute(n), E.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }
                    RE.code = "RTS";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        currency="USD",
                        amount= 10,
                        signature = n.signature
                    };
                    RE.message = "SUCCESS";
                    return RE;
               
                 
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";

                new Generator().LogException(e, "ETransferApi-Logs");
            }

            return RE;
        }

    

        [Route("ETransfer/status")]
        [HttpPost]
        public ResponseEx status([FromBody] RequestStatusEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            MessageAppenderHelper MAH = new MessageAppenderHelper();
            string message = MAH.requestStatus(n);
            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    if (n.signature.ToUpper() == EH.HMAC_SHA256(MAH.requestStatus(n), E.key))
                    {
                        TransactionEx result = svcTransaction.GetStatusByPartner(E.id, n.referenceNo);

                        if (result != null)
                        {
                            RE.code = "RTS";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                referenceno = n.referenceNo,
                                status = result.status,
                                trasactiontype = result.transactionType,
                                signature = n.signature
                            };
                            RE.message = "SUCCESS";
                            return RE;
                        }
                        else
                        {
                            RE.code = "RTF";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                referenceno = n.referenceNo,
                                signature = n.signature
                            };
                            RE.message = "NO TRANSACTION FOUND";
                            return RE;
                        }
                

                    }
                    else
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";

                    }
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    //  error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";

                new Generator().LogException(e, "ETransferApi-Log");
            }

            return RE;
        }
    }
}
