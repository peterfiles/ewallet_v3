﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using System;
using System.Net;
using MoneySwap.com;
using System.Xml;
using System.Collections.Generic;

namespace Cooptavanza.Wallet.Controllers
{
    public class LunexBillPaymentApiController : ApiController
    {

        readonly CustomerService svcCustomer;
        readonly TransactionService svcTransaction;
        readonly TransactionLogService svcTLog;
        readonly PartnersService svcPartner;
        readonly PartnerCustomerService svcPartnerCustomer;

        public LunexBillPaymentApiController(
            CustomerService svcCustomer,
            TransactionService svcTransaction,
            TransactionLogService svcTLog,
            PartnersService svcPartner,
            PartnerCustomerService svcPartnerCustomer)
        {
            this.svcTransaction = svcTransaction;
            this.svcCustomer = svcCustomer;
            this.svcTLog = svcTLog;
            this.svcPartner = svcPartner;
            this.svcPartnerCustomer = svcPartnerCustomer;
        }


        [Route("LEP/BP/neworder")]
        [HttpPost]
        public object newOrder(LUNEXOrderEx n)
        {
            try
            {
                CustomerEx c = new CustomerEx();
                TransactionEx t = new TransactionEx();
                if (n.CustomerId > 0)
                {
                    c = svcCustomer.Get(n.CustomerId);
                }

                t.accountNumber = "";
                t.balance = n.Amount;
                t.credit = false;
                t.debit = true;
                t.dataCreated = DateTime.Now;
                t.description = "BILL Payment on " + n.ProductName;
                t.transactionType = "10005";// + n.Sku;
                t.userId = c.id;
                t.userTID = c.tid;
                t.status = "CLEARED";
                t.currencyfromiso = n.currencyfromiso;
                t.currencytoiso = "USD";

                string method = "POST";
                string CID = "" + Guid.NewGuid();
                t.transactionTID = CID;
                string data = "orders/"
                    + CID
                    + "?sku=" + n.Sku
                    + "&phone=" + n.Phone
                    + "&amount=" + n.Amount;

                svcTransaction.Save(t);

                var r = new WebRequestHelperLunex().WebRequestResponseAsync(data, method);
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(r);
                var ret = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.None);
                string fullname = c.firstname + " " + c.middlename + "" + c.lastname;
                svcTLog.SaveLog(ret.ToString(), "BILL_PAYMENT");
                new Generator().sendSMSNotif(c.mobilenumber, n.currencyfromiso, n.Amount.ToString(), "BILL PAYMENT", "debited");
                new Generator().sendEmailNotif(fullname, c.email, n.currencyfromiso, n.Amount.ToString(), "BILL PAYMENT", "debited");
                //   ret.
                // se.Message = ret.Contains("Message") != null ? ret.Contains("Message") : "";

                return doc;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "LunexBillPayment-Log");
                return null;
            }
        }

        [Route("LEP/BP/CBS/maketransaction")]
        [HttpPost]
        public ResponseEx makeTransaction([FromBody] LunexCBSEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                PartnerCustomerEx PC = new PartnerCustomerEx();
                PartnersEx P = svcPartner.getByPartnerCode(n.partnerCode);
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                TransactionEx T = new TransactionEx();

                if (P != null)
                {
                    var message = EH.HMAC_SHA256(MAH.requestLunexTrasaction(n), P.key);
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestLunexTrasaction(n), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }

                    if (n.transactionType == "11105")
                    {
                        T.description = "LUNEX with product" + " " + (n.product).ToUpper() + " transaction no: " + n.referenceNo;
                    }
                    else
                    {
                        RE.code = "RTF";
                        RE.date = DateTime.Now;
                        RE.data = new
                        {
                            referenceNo = n.referenceNo,
                            signature = n.signature,
                        };
                        RE.message = "WRONG TRANSACTION TYPE";
                        return RE;
                    }

                    bool r = svcTransaction.CheckTransactionId(n.referenceNo, P.id);
                    if (r == true)
                    {
                        RE.code = "RTF";
                        RE.date = DateTime.Now;
                        RE.data = new
                        {
                            referenceNo = n.referenceNo,
                            signature = n.signature,
                        };
                        RE.message = "REFERENCE NUMBER ALREADY EXIST";
                        return RE;
                    }

                    PC.accountnumber = n.sndAccntNo;
                    PC.firstname = n.sndFirstName;
                    PC.lastname = n.sndLastName;
                    PC.middlename = n.sndMiddleName;
                    PC.mobilenumber = n.sndMobileNo;
                    PC.partnerId = P.id;
                    PC.phonenumber = n.sndPhoneNo;
                    PC.suffix = n.sndSuffix;
                    PC.tid = "" + Guid.NewGuid();
                    PC.email = n.sndEmail;
                    PC.dateCreated = DateTime.Now;


                    svcPartnerCustomer.Save(PC);
                    PartnerCustomerEx PCE = svcPartnerCustomer.Get(PC.id);

                    T.accountNumber = n.sndAccntNo;
                    T.balance = n.amount;
                    T.credit = true;
                    T.currencytoiso = n.currency;
                    T.dataCreated = n.date;
                    T.debit = false;

                    T.partnerId = P.id;
                    T.status = "PENDING";

                    T.transactionTID = n.referenceNo;
                    T.transactionType = n.transactionType;
                    T.userId = PC.id;
                    T.userTID = PC.tid;
                    T.dataCreated = DateTime.Now;
                    svcTransaction.Save(T);

                    RE.code = "RTS";
                    RE.data = new
                    {
                        transactionType = n.transactionType,
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        amounttransfered = n.amount,
                        currencyto = n.currency,
                        fees = "" + ((0.01 * (double)n.amount) + 5)
                    };
                    RE.message = "SUCCESS";
                    RE.date = DateTime.Now;

                    LunexBPNotification LBPN = new LunexBPNotification();
                    LBPN.sndLunexBPEmailNotifs(T, PC, n);

                }
                else
                {
                    RE.code = "RTF";
                    RE.date = DateTime.Now;
                    RE.data = new
                    {
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        partnerCode = n.partnerCode
                    };
                    RE.message = "NO PARTNER AVAILABLE";

                }

                return RE;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "LunexBillPayment-Log");
                return null;
            }
        }

        [Route("LEP/BP/CBS/transaction")]
        [HttpPost]
        public ResponseEx listTransaction([FromBody] RequestListEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            MessageAppenderHelper MAH = new MessageAppenderHelper();
            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MAH.requestTransactionList(n), E.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }

                    string dateStr = n.dateStart != null ? string.Format("{0:yyyy-MM-dd}", n.dateStart) : null;
                    string dateEn = n.dateEnd != null ? string.Format("{0:yyyy-MM-dd}", n.dateEnd) : null;
                    if (dateStr != null)
                    {
                        dateEn = string.Format("{0:yyyy-MM-dd}", DateTime.Now);
                    }
                    List<TransactionEx> result = svcTransaction.GetByPartner(n.searchText, E.id, dateStr, dateEn, n.transactionType, n.referenceNo, n.orderBy, n.pageNo, n.pageSize);
                    int count = svcTransaction.CountByPartner(n.searchText, E.id, dateStr, dateEn, n.transactionType, n.referenceNo);

                    RE.code = "RTS";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        Result = result,
                        Count = count,
                        signature = n.signature
                    };
                    RE.message = "SUCCESS";
                    return RE;

                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    //  error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";
                new Generator().LogException(e, "LunexBillPayment-Log");
            }

            return RE;
        }

        [Route("LEP/BP/CBS/qoute")]
        [HttpPost]
        public ResponseEx qoute([FromBody] RequestQouteEx n)
        {
            ResponseEx RE = new ResponseEx();
            EncryptionHelper EH = new EncryptionHelper();
            TransactionEx T = new TransactionEx();
            try
            {
                PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                if (E != null)
                {
                    //calculation of service

                    RE.code = "RTS";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {

                        signature = n.signature
                    };
                    RE.message = "SUCCESS";
                    return RE;

                }
                else
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "PARTNER IS NOT ALLOWED";

                }
            }
            catch (Exception e)
            {
                RE.code = "RTT";
                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                RE.data = new
                {
                    //  error = "" + e.Message,
                    signature = n.signature
                };
                RE.message = "SYSTEM TIMED OUT";

                new Generator().LogException(e, "LunexBillPayment-Log");
            }

            return RE;
        }


        [Route("LEP/BP/CBS/status")]
        [HttpPost]
        public ResponseEx status([FromBody] RequestStatusEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                EncryptionHelper EH = new EncryptionHelper();
                TransactionEx T = new TransactionEx();
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                string message = MAH.requestStatus(n);
                try
                {
                    PartnersEx E = svcPartner.getByPartnerCode(n.partnerCode);
                    if (E != null)
                    {
                        if (n.signature.ToUpper() == EH.HMAC_SHA256(MAH.requestStatus(n), E.key))
                        {
                            TransactionEx result = svcTransaction.GetStatusByPartner(E.id, n.referenceNo);

                            if (result != null)
                            {
                                RE.code = "RTS";
                                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                RE.data = new
                                {
                                    referenceno = n.referenceNo,
                                    status = result.status,
                                    trasactiontype = result.transactionType,
                                    signature = n.signature
                                };
                                RE.message = "SUCCESS";
                                return RE;
                            }
                            else
                            {
                                RE.code = "RTF";
                                RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                                RE.data = new
                                {
                                    referenceno = n.referenceNo,
                                    signature = n.signature
                                };
                                RE.message = "NO TRANSACTION FOUND";
                                return RE;
                            }


                        }
                        else
                        {
                            RE.code = "RTF";
                            RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                            RE.data = new
                            {
                                signature = n.signature
                            };
                            RE.message = "INVALID SIGNATURE";

                        }
                    }
                    else
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "PARTNER IS NOT ALLOWED";

                    }
                }
                catch (Exception e)
                {
                    RE.code = "RTT";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        //  error = "" + e.Message,
                        signature = n.signature
                    };
                    RE.message = "SYSTEM TIMED OUT";
                    new Generator().LogException(e, "LunexBillPayment-Log");
                }
                return RE;
            }
            catch (Exception e) { 
                new Generator().LogException(e, "LunexBillPayment-Log");
                return null;
            }
        }
    }
}
