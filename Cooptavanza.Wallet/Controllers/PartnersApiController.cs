﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Cooptavanza.Wallet.Repository;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json;

namespace Cooptavanza.Wallet.Controllers
{
    public class PartnersApiController : ApiController
    {
        readonly PartnersService svcPartners;
        readonly OFAClistService svcOFAC;
        readonly UserAccountService svcUserAccount;
        readonly UserServicesService svcUserServices;
        readonly CoopTransactionLogService svcCTLS;
        readonly CoopEnrolledAccountService svcCEA;

        readonly AgentsService svcAgent;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;
        readonly UserAccountActivitiesService svcUAA;

        readonly CurrencyService svcCurr;

        public PartnersApiController(
            PartnersService svcPartners, 
            OFAClistService svcOFAC, 
            UserAccountService svcUserAccount, 
            UserServicesService svcUserServices,
            CoopTransactionLogService svcCTLS,
            CoopEnrolledAccountService svcCEA,
            AgentsService svcAgent,
            MerchantService svcMerchant,
            UsersService svcUsers,
            UserAccountActivitiesService svcUAA,
            CurrencyService svcCurr)
        {
            this.svcPartners = svcPartners;
            this.svcOFAC = svcOFAC;
            this.svcUserAccount = svcUserAccount;
            this.svcUserServices = svcUserServices;
            this.svcCTLS = svcCTLS;
            this.svcCEA = svcCEA;
            this.svcAgent = svcAgent;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcUAA = svcUAA;
            this.svcCurr = svcCurr;
        }

        [Route("Partners")]
        [HttpGet]
        public PartnersSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new PartnersSearchViewModel() { Results = svcPartners.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcPartners.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "PartnersAPI-Log");
                return null;
            }
        }


        [Route("Partners/{Id}")]
        [HttpGet]
        public PartnersEx GetPartners(int Id)
        {
            try
            {
                PartnersEx pEx = svcPartners.Get(Id);
                UserAccountEx uaEx = svcUserAccount.GetAccountByUserId(3, pEx.id, pEx.tid);
                pEx.accountNumber = svcCEA.getIdByUserIDRoleIDCurrency(pEx.id, 3, pEx.currency).accountnumber;
                if (uaEx != null)
                {
                    pEx.username = uaEx.username;
                    pEx.password = new EncryptionHelper().Decrypt(uaEx.password);
                }
                return pEx;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "PartnersAPI-Log");
                return null;
            }
        }

        [Route("Partners/adminsave")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public PartnersEx Save([FromBody] PartnersEx n)
        {
            try
            {
                var tempid = n.id;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.expirationDate = DateTime.Now;
                bool debitToGiver = false;
                if (n.coopAccountListData.coopRootAccountUserIDAndRole != null)
                {
                    if (n.coopAccountListData.coopRootAccountUserIDAndRole.roleID == 3 && n.coopAccountListData.coopAccountListAndcurencyData != null)
                    {
                        n.masterPartnerID = n.coopAccountListData.coopRootAccountUserIDAndRole.loggedID;
                        debitToGiver = true;
                    }
                }
                PartnersEx p = svcPartners.Get(n.id);
                if (p != null)
                {
                    if (p.id > 0)
                    {
                        n.address = n.address != null ? n.address : p.address;
                        n.city = n.city != null ? n.city : p.city;
                        n.cityid = n.cityid != null ? n.cityid : p.cityid;

                        n.country = n.country != null ? n.country : p.country;
                        n.countryid = n.countryid != null && n.countryid > 0 ? n.countryid : p.countryid;

                        n.state = n.state != null ? n.state : p.state;
                        n.stateid = n.stateid != null && n.stateid > 0 ? n.stateid : p.stateid;

                        List<AccountListEx> ceaEx = svcCEA.getCoopAccountList(p.id, 3);
                        n.currency = ceaEx.Count == 0 || ceaEx[0].currencyAccountTo == null ? "D0F" : ceaEx[0].currencyAccountTo;
                        n.currencyid = svcCurr.GetByISO(n.currency).id;

                        n.finame = n.finame != null ? n.finame : p.finame;
                        n.license = n.license != null ? n.license : p.license;
                        n.mobilenumber = n.mobilenumber != null ? n.mobilenumber : p.mobilenumber;
                        n.phonenumber = n.phonenumber != null ? n.phonenumber : p.phonenumber;
                        n.servicetype = n.servicetype != null ? n.servicetype : p.servicetype;

                        n.zipcode = n.zipcode != null ? n.zipcode : p.zipcode;
                    }
                }

                svcPartners.Save(n);

                //n.password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "default";
                var s = p == null ? n : p;
                UserAccountEx ua = new UserAccountEx();
                UserServicesEx us = new UserServicesEx { user_id = n.id, roles_id = 3 };
                List<coopAccountListAndCurrencyData> coopALACD = n.coopAccountListData.coopAccountListAndcurencyData;
                coopRootAccountUserIDAndRole coopRAUAR = n.coopAccountListData.coopRootAccountUserIDAndRole;
                if (coopALACD != null)
                {
                    foreach (coopAccountListAndCurrencyData resData in coopALACD)
                    {
                        CoopEnrolledAccountEx ceaEx = new CoopEnrolledAccountEx();
                        ceaEx = svcCEA.getIdByUserIDRoleIDCurrency(n.id, 3, resData.currency.iso);
                        if (ceaEx.id == 0)
                        {
                            #region Coop enrolled Account EX;
                            ceaEx.id = 0;
                            ceaEx.accountDefault = true;
                            ceaEx.accountnumber = "";
                            ceaEx.currency = resData.currency.iso;
                            ceaEx.currenycid = resData.currency.id;
                            ceaEx.roleID = Convert.ToString(3);
                            ceaEx.userId = n.id;
                            ceaEx.userTID = n.tid;
                            #endregion Coop enrolled Account EX;

                            svcCEA.Save(ceaEx);
                            ceaEx.accountnumber = svcCEA.getAccountNumberForCoopRed(n.id, ceaEx.currenycid, 3, ceaEx.id);
                            svcCEA.Save(ceaEx);
                        }
                        if (resData.status.Equals("added"))
                        {
                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = resData.amount;
                            ctlEx.currencyAccountFrom = resData.currency.iso;
                            ctlEx.currencyAccountTo = resData.currency.iso;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgent,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgent,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = true;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 3;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + resData.amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "CREDIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);
                        }

                    }
                }

                if (debitToGiver)
                {
                    for (int q = 0; q < n.coopAccountListgiver.Length; q++)
                    {
                        object a = n.coopAccountListData.coopAccountListAndcurencyData.Find(x => x.currency.iso.Equals(n.coopAccountListgiver[q].currencyAccountTo));
                        if (a != null)
                        {

                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = n.coopAccountListgiver[q].amount;
                            ctlEx.currencyAccountFrom = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.currencyAccountTo = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgent,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgent,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = false;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 3;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + n.coopAccountListgiver[q].amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "DEBIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);
                        }
                    }
                }

                List<compiledProductsToFilter> cptf = new List<compiledProductsToFilter>();
                if (n.compiledProductData != null)
                {
                    foreach (compiledProductData cpa in n.compiledProductData)
                    {
                        if (cptf.Exists(x => x.id == cpa.compiledProductsToFilter.id))
                        {
                            cptf.RemoveAt(cptf.FindIndex(y => y.id == cpa.compiledProductsToFilter.id));
                            cptf.Add(cpa.compiledProductsToFilter);
                        }
                        else
                        {
                            cptf.Add(cpa.compiledProductsToFilter);
                        }
                    }
                }

                if (cptf != null || cptf.Count > 0)
                {
                    foreach (compiledProductsToFilter cptf2 in cptf)
                    {
                        if (cptf2 != null)
                        {
                            if (cptf2.status.Equals("checked"))
                            {
                                us.product_id = cptf2.id;
                                svcUserServices.Save(us);
                                us.ID = 0;
                            }
                        }
                    }
                }

                UserAccountActivitiesEx UAA = new UserAccountActivitiesEx();

                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = s.id;
                    ua.roles_id = 3;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    //UAA adding of user account activity
                    UAA.userId = s.id;
                    UAA.userRole = 3;
                    UAA.dateCreated = DateTime.Now;
                    UAA.status = false;
                    UAA.token_id = "" + Guid.NewGuid();


                    svcUserAccount.Save(ua);
                    svcUAA.Save(UAA);
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(3, n.id, n.tid);
                    if (x != null)
                    {
                        ua.id = x.id;
                        ua.tID = n.tid;
                        ua.password = password.Equals("") ? x.password : password;
                        ua.user_id = n.id;
                        ua.roles_id = 3;
                        ua.username = n.username;
                        //ua.roles_id = svcRole.; temporary
                        svcUserAccount.Save(ua);
                    }
                }
                return svcPartners.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "PartnersAPI-Log");
                return null;
            }
            //checkIfPartnersIsInOFAC(n);
            //return s;
            //return null;
        }
       

        [Route("Partners/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcPartners.Remove(Id);
        }

        [Route("Partners/PartnersIfIsInOFAC")]
        [HttpPost]
        public bool checkIfPartnersIsInOFAC([FromBody] PartnersEx n)
        {
            try
            {
                string res = string.Empty;
                //bool isVerified = false;
                bool isInOFAC = false;
                //List<OFAClistEx> list = new List<OFAClistEx>();
                //using (WebClient wb = new WebClient())
                //{
                //    wb.Headers.Add("Content-Type", "application/json");
                //    #region Check if company name is in OFAC;
                //    res = wb.DownloadString("https://easyofac.com/api/companySearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&name=" + n.finame + "&test=1");
                //    list = JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
                //    if (list.Count > 0)
                //    {
                //        foreach (OFAClistEx d in list)
                //        {
                //            d.isCompany = true;
                //            d.userID = n.id;
                //            d.userCode = 1003;
                //            d.userIsVerified = false;
                //            svcOFAC.Save(d);
                //        }
                //        isInOFAC = true;
                //    }
                //    #endregion;
                //    #region Check if Fist name and Last name is in OFAC;
                //    res = string.Empty;
                //    list = new List<OFAClistEx>();
                //    res = wb.DownloadString("https://easyofac.com/api/nameSearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&first_name=" + n.firstname + "&last_name=" + n.lastname + "&test=1");
                //    list = JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
                //    if (list.Count > 0)
                //    {
                //        foreach (OFAClistEx d in list)
                //        {
                //            d.isCompany = false;
                //            d.userID = n.id;
                //            d.userCode = 1003;
                //            d.userIsVerified = false;
                //            svcOFAC.Save(d);
                //        }
                //        isInOFAC = true;
                //    }
                //}
                //#endregion;
                //if (isInOFAC)
                //{
                //    n.isInOFACList = isInOFAC;
                //    n.isVerified = isVerified;
                //}
                //else
                //{
                //    n.isVerified = true;
                //}
                svcPartners.Save(n);
                return isInOFAC;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "PartnersAPI-Log");
                return false;
            }
        }

        [Route("Partners/getAllMasterPartner")]
        [HttpGet]
        public List<PartnersEx> getAllMasterPartner()
        {
            try
            {
                return svcPartners.getAllMasterPartner();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "PartnersAPI-Log");
                return null;
            }
        }

        [Route("Partners/getPartnerCode")]
        [HttpGet]
        public object getPartnerCode(string key)
        {
            try
            {
                return new Generator().generatePartnerCode(key, svcPartners.CountPartners());
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "PartnersAPI-Log");
                return null;
            }
        }
    }
}
