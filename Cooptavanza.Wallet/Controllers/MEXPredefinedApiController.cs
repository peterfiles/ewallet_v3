﻿using Cooptavanza.Models.ViewModels;
using Cooptavanza.Wallet.Service;
using System;
using System.Web.Http;

namespace Cooptavanza.Wallet.Controllers
{
    public class MEXPredefinedApiController : ApiController
    {
        readonly MEXService svcMEX;
       
        public MEXPredefinedApiController(MEXService svcMEX)
        {
            this.svcMEX = svcMEX;
        }

        [Route("MEX")]
        [HttpGet]
        public MEXEx Get()
        {
            try
            {
                return svcMEX.MEXPage();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Mex-Log");
                return null;
            }

        }

    
      

    }
}
