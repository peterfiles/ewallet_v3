﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using Cooptavanza.Wallet.Repository;
using System.Text;
using System.Xml;

namespace Cooptavanza.Wallet.Controllers
{
    public class UserAccountController : ApiController
    {
        readonly DummyLoadService svcDummyLoad;
        readonly CustomerService svcCustomer;
        readonly UsersTempService svcUsersTemp;
        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly AgentsService svcAgent;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;
        readonly PartnersService svcPartner;
        readonly UserServicesService svcUserServicesService;
        readonly CoopEnrolledAccountService svcCEAS;
        readonly CoopTransactionLogService svcCTLS;
        readonly PaymentBreakdownService svcPBEx;
        readonly CommissionsService svcCommission;
        readonly PaymentBreakdownService svcPaymentBreakdown;
        readonly TransactionService svcTransaction;
        readonly TransferMoneyService svcTransferMoney;

        private static int userID;
        private static int roleID;

        public UserAccountController(
            CustomerService svcCustomer,
            UsersTempService svcUsersTemp,
            UserAccountService svcUserAccount,
            OFAClistService svcOFAC,
            DummyLoadService svcDummyLoad,
            AgentsService svcAgent,
            MerchantService svcMerchant,
            UsersService svcUsers,
            PartnersService svcPartner,
            UserServicesService svcUserServicesService,
            CoopEnrolledAccountService svcCEAS,
            CoopTransactionLogService svcCTLS,
            PaymentBreakdownService svcPBEx,
            CommissionsService svcCommission,
            PaymentBreakdownService svcPaymentBreakdown,
            TransactionService svcTransaction,
            TransferMoneyService svcTransferMoney
            )
        {
            this.svcDummyLoad = svcDummyLoad;
            this.svcUserAccount = svcUserAccount;
            this.svcCustomer = svcCustomer;
            this.svcUsersTemp = svcUsersTemp;
            this.svcOFAC = svcOFAC;
            this.svcAgent = svcAgent;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcPartner = svcPartner;
            this.svcUserServicesService = svcUserServicesService;
            this.svcCEAS = svcCEAS;
            this.svcCTLS = svcCTLS;
            this.svcPBEx = svcPBEx;
            this.svcCommission = svcCommission;
            this.svcPaymentBreakdown = svcPaymentBreakdown;
            this.svcTransaction = svcTransaction;
            this.svcTransferMoney = svcTransferMoney;
        }




        [Route("ua")]
        [HttpGet]
        public UserAccountSearchViewModel Get(string SearchText = "", int pageno = 1, int pagesize = 10)
        {
            try
            {
                return new UserAccountSearchViewModel() { Results = svcUserAccount.GetAllMember(SearchText, pagesize, pageno), Count = svcUserAccount.countmember(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("uaFilterByParentID")]
        [HttpGet]
        public UserAccountSearchViewModel GetFilterByParentID(string SearchText = "", int pageno = 1, int pagesize = 10, int userID = 0)
        {
            try
            {
                return new UserAccountSearchViewModel() { Results = svcUserAccount.GetAllMemberByParentID(SearchText, pagesize, pageno, userID), Count = svcUserAccount.countmemberFilterByParentID(SearchText, userID) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("uaFilterByParentIDAndRoleID")]
        [HttpGet]
        public UserAccountSearchViewModel GetFilterByParentIDAndRoleId(string SearchText = "", int pageno = 1, int pagesize = 10, int userID = 0, int roleID = 0)
        {
            try { 
            return new UserAccountSearchViewModel() { Results = svcUserAccount.GetAllMemberByParentIDAndRoleID(SearchText, pagesize, pageno, userID, roleID), Count = svcUserAccount.countmemberFilterByParentIDAndRoleID(SearchText, userID, roleID) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/login")]
        [HttpPost]
        public Object login([FromBody] UserAccountEx n)
        {
            try { 
            UserAccountEx ua = svcUserAccount.GetAdminAuthenticate(n.username, new EncryptionHelper().Encrypt(n.password), n.corporatename);
            if (ua != null) {
                userID = ua.user_id;
                roleID = ua.roles_id;
                switch (ua.roles_id) {
                    case 1:
                        var x = svcUsers.Get(ua.user_id);
                        x.membertype = "user";
                        object px = new
                        {
                            data = x,
                            product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id),
                            rID = ua.roles_id
                        };
                        return px;

                    case 2:
                        var y = svcUsers.Get(ua.user_id);
                        y.membertype = "sa";
                        object py = new
                        {
                            data = y,
                            product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id),
                            rID = ua.roles_id
                        };
                        return py;

                    case 3:
                        var z = svcPartner.Get(ua.user_id);
                        z.membertype = "partner";
                        object p = new
                        {
                            data = z,
                            product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id),
                            rID = ua.roles_id
                        };

                        return p;

                    case 4:
                        var a = svcAgent.Get(ua.user_id);
                        a.membertype = "agent";
                        object c = new
                        {
                            data = a,
                            product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id),
                            rID = ua.roles_id
                        };

                        return c;

                    case 5:
                        var b = svcMerchant.Get(ua.user_id);
                        b.membertype = "merchant";
                        object m = new
                        {
                            data = b,
                            product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id),
                            rID = ua.roles_id
                        };

                        return m;
                        default:
                            return "no data";
                        /* case 6:
                             return svcMerchant.Get(ua.user_id);
                             break;*/
                        //case 7:
                        //    var c = svcCustomer.Get(ua.user_id);
                        //    c.membertype = "customer";
                        //    return c;
                }

                }
                else { return null; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/getAccountList")]
        [HttpGet]
        public object CoopAccountList(int userID, int roleID)
        {
            try
            {
                return new GlobalCallForServicesController(svcCEAS).getCoopAccountList(userID, roleID);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
}

        [Route("Admin/getTransactionList")]
        [HttpGet]
        public CoopTransactionLogListSearchViewModel CoopTransactionList(string str = "", int psize = 10, int pno = 1, int userID = 0, int roleID = 0)
        {
            try
            {
                return new CoopTransactionLogListSearchViewModel { Results = new GlobalCallForServicesController(svcCTLS).getTransctionList(str, psize, pno, userID, roleID), Count = new GlobalCallForServicesController(svcCTLS).getCountTransactionList(str, userID, roleID) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/GetCountryByGeolocation")]
        [HttpGet]
        public object GetCountryByGeolocation(string lng, string lat)
        {
            try { 
            string method = "GET";
            string data = lat + "," + lng;
            object r = new WebRequestHelper().GetGeolocationGoogleAPI(data, method);
            return r;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gcbygl")]//GetCountryByGeoLocation
        [HttpGet]
        public object gcbygl()
        {
            try { 
            object r = new WebRequestHelper().GetGeoLocation();
            return r;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/GetCommissionTransactionByUserIDAndRoleID")]
        [HttpGet]
        public List<PaymentBreakdownEx> GetCommissionListByUserAndRoleID(int uID, int rID)
        {
            try { 
            return svcPBEx.GetCommissionByPaymentBreakdown(uID, rID);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gclbyur")]//GetCommissionListByUserAndRoleID
        [HttpGet]
        public CommissionSearchViewModel gclbyur(int userID = 0, int userRole = 0, string orderby = "id DESC", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new CommissionSearchViewModel { Results = svcCommission.GetCommissionListWithFilter(userID, userRole, orderby, PageNo, PageSize), Count = svcCommission.GetCommissionListWithFilter(userID, userRole, orderby, PageNo, PageSize).Count };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gclbyurm")]//GetCommissionListByUserAndRoleIDAndMethod
        [HttpGet]
        public CommissionSearchViewModel gclbyurm(int userID = 0, int userRole = 0, string method = "", string orderby = "id DESC", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new CommissionSearchViewModel { Results = svcCommission.GetCommissionListWithFilterByUserIDRoleIDMethod(userID, userRole, method, orderby, PageNo, PageSize), Count = svcCommission.GetCommissionListWithFilterByUserIDRoleIDMethod(userID, userRole, method, orderby, PageNo, PageSize).Count };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gcbytid")]//GetCommissionByTID
        [HttpGet]
        public PaymentBreakdownSearchViewModel gcbytid(string transactionID = "", string orderby = "id DESC", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new PaymentBreakdownSearchViewModel { Results = svcPaymentBreakdown.GetPaymentBreakdownByTransactionID(transactionID, orderby, PageNo, PageSize), Count = svcPaymentBreakdown.GetPaymentBreakdownByTransactionID(transactionID, orderby, PageNo, PageSize).Count };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gctbtid")]//GetCommissionTransactionByTID
        [HttpGet]
        public TransactionSearchViewModel gctbtid(string tid = "", string orderby = "id DESC", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new TransactionSearchViewModel { Results = svcTransaction.GetTransactionByTID(tid, orderby, PageNo, PageSize), Count = svcTransaction.GetTransactionByTID(tid, orderby, PageNo, PageSize).Count };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gtcbyuram")]//GetTotalCommisionByUserIDRoleIDAndMethod
        [HttpGet]
        public List<TotalCommissionEx> gtcbyuram(int u = 0, int r = 0, string m="")
        {
            try{
            return svcCommission.getTotalCommissionByUserIDRoleIDAndMethod(u, r, m);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gtmtbytid")]//GetTransferMoneyTransctionByTID
        [HttpGet]
        public object gtmtbytid(string tid = "")
        {
            try { 
            TransferMoneyEx tmEx = svcTransferMoney.GetTransferMoneyByTransactionTID(tid);
            return new
            {
                amntF = tmEx.amountfrom,
                currF = tmEx.currencyfromiso,
                convAmntT = tmEx.amountto,
                currT = tmEx.currencytoiso
            };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }

        [Route("Admin/gtcbyuidridgbyc")]//GetTotalCommissionByUserIDAndRoledIDGroupByCurr
        [HttpGet]
        public object gtcbyuidridgbyc(int uID, int rID)
        {
            try { 
            return svcCommission.GetTotalCommByUidAndRidGroupByCurr(uID, rID);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UserAccountAPI-Log");
                return null;
            }
        }
    }
}
