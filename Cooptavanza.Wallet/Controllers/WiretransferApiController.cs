﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;


namespace Cooptavanza.Wallet.Controllers
{
    public class WireTransferApicontroller : ApiController
    {
        //readonly DummyLoadService svcDummyLoad;
        readonly CustomerService svcCustomer;
        readonly UsersTempService svcUsersTemp;
        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly TransactionService svcTransaction;
        readonly TransferMoneyService svcTransferMoney;
        readonly CurrencyService svcCur;
        readonly ImageIdCardService svcImageIdCard;
        readonly ReceiversCustomerService svcReceiverCustomer;
        readonly MoneyTransferTransactionsService svcMoneyTansferTransaction;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly CurrencyService svcCurrency;
        readonly TransactionLogService svcError;


        public WireTransferApicontroller(
            CustomerService svcCustomer, 
            UsersTempService svcUsersTemp, 
            UserAccountService svcUserAccount, 
            OFAClistService svcOFAC, 
            //DummyLoadService svcDummyLoad, 
            TransactionService svcTransaction, 
            TransferMoneyService svcTransferMoney, 
            CurrencyService svcCur, 
            ImageIdCardService svcImageIdCard,
            ReceiversCustomerService svcReceiverCustomer,
            MoneyTransferTransactionsService svcMoneyTansferTransaction,
            EnrolledAccountService svcEnrolledAccount,
            CurrencyService svcCurrency,
            TransactionLogService svcError)
        {
            //this.svcDummyLoad = svcDummyLoad;
            this.svcReceiverCustomer = svcReceiverCustomer;
            this.svcUserAccount = svcUserAccount;
            this.svcCustomer = svcCustomer;
            this.svcUsersTemp = svcUsersTemp;
            this.svcOFAC = svcOFAC;
            this.svcTransaction = svcTransaction;
            this.svcTransferMoney = svcTransferMoney;
            this.svcImageIdCard = svcImageIdCard;
            this.svcCur = svcCur;
            this.svcMoneyTansferTransaction = svcMoneyTansferTransaction;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcCurrency = svcCurrency;
            this.svcError = svcError;
        }

        [Route("Wiretransfer/SendNotif")]
        [HttpPost]
        public object SendNotif([FromBody] object data)
        {
            try
            {
                object retMessage = new
                {
                    emailRetMessage = new WireTransferNotificationSender().sendWireTransferEmail(data),
                    smsRetMessage = new WireTransferNotificationSender().sendWireTransferSMSNotif(data)
                };
                return retMessage;
            } 
            catch (Exception e)
            {
                new Generator().LogException(e, "wireTransferApi-Log");
                return null;
            }
}


     

    }
}
