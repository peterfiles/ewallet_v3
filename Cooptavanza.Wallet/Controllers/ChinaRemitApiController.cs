﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;
using MoneySwap.com;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    public class ChinaRemitApicontroller : ApiController
    {
        readonly TransactionService svcTransaction;
        readonly PartnersService svcPartner;
        readonly PartnerCustomerService svcPartnerCustomer;
        readonly TransferMoneyService svcTransferMoney;
        readonly CustomerService svcCustomer;
        readonly CurrencyService svcCurrency;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly TransactionTypeService svcTransactionType;
        readonly MEXRemitValidateService svcMRemitValidate;
        readonly MEXResponseRemitValidateService svcMResRemitValidate;
        readonly MEXResponseRemitErrorLogService svcRemitErrorLog;

        string merchantId = ConfigurationManager.AppSettings["MEXMId"].ToString();//"507000590000103";// "001678020000103";
        string key = ConfigurationManager.AppSettings["MEXKey"].ToString();//"testkey@VANZA";
        MoneyExpressApiV04Client client = new MoneyExpressApiV04Client();

        public ChinaRemitApicontroller(
            TransactionService svcTransaction,
            PartnersService svcPartner,
            PartnerCustomerService svcPartnerCustomer,
            TransferMoneyService svcTransferMoney,
            CustomerService svcCustomer,
            CurrencyService svcCurrency,
            EnrolledAccountService svcEnrolledAccount,
            TransactionTypeService svcTransactionType,
            MEXResponseRemitValidateService svcMResRemitValidate,
            MEXRemitValidateService svcMRemitValidate,
            MEXResponseRemitErrorLogService svcRemitErrorLog
            )
        {
            this.svcTransaction = svcTransaction;
            this.svcPartner = svcPartner;
            this.svcPartnerCustomer = svcPartnerCustomer;
            this.svcTransferMoney = svcTransferMoney;
            this.svcCustomer = svcCustomer;
            this.svcCurrency = svcCurrency;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcTransactionType = svcTransactionType;
            this.svcMRemitValidate = svcMRemitValidate;
            this.svcMResRemitValidate = svcMResRemitValidate;
            this.svcRemitErrorLog = svcRemitErrorLog;
        }



        [Route("ChinaRemit")]
        [HttpPost]
        public ResponseEx requetRemit([FromBody] ChinaRemitEx n)
        {
            try
            {
                MEXMessageAppenderHelper MEXMAH = new MEXMessageAppenderHelper();
                EncryptionHelper EH = new EncryptionHelper();
                ResponseEx RE = new ResponseEx();
                var P = svcPartner.getByPartnerCode(n.partnerCode);
                if (P != null)
                {

                    //  n.sndPhoneNo = n.sndPhoneNo != null ? "+" + n.sndPhoneNo.Trim().Replace("+", "") : "";
                    // n.rcvMobileNo = n.rcvMobileNo != null ? "+" + n.rcvMobileNo.Trim().Replace("+", "") : "";
                    var message = EH.HMAC_SHA256(MEXMAH.requestTrasaction(n), P.key);
                    if (n.signature.ToUpper() != EH.HMAC_SHA256(MEXMAH.requestTrasaction(n), P.key))
                    {
                        RE.code = "RTF";
                        RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                        RE.data = new
                        {
                            signature = n.signature
                        };
                        RE.message = "INVALID SIGNATURE";
                        return RE;
                    }


                    string address = n.sndStreet + "\n" + n.sndCity + "\n" + n.sndState + "\n" + n.sndCountry;
                    n.amlStatus = ("AML_CHECK_PASSED").ToUpper();
                    n.floatCurrency = ("USD").ToUpper();
                    string includeFees = ("EXCLUDE_FEE").ToUpper();
                    n.kioskAddress = ("NNNNN").ToUpper();
                    n.kioskName = ("NNNNN").ToUpper();
                    n.kycCheck = ("SENDER_KYC_PASSED").ToUpper();
                    n.note = ("Developer Money Send Test Partner: " + P.finame).ToUpper();
                    n.originatorUID = ((Guid.NewGuid() + "").Replace("-", "") + "-100016").ToUpper();
                    n.rcvAccntType = ("BANK_CARD").ToUpper();
                    n.sndKycUrl = ("EASYOFAC.COM").ToUpper();
                    n.tranCurr = ("USD").ToUpper();

                    MoneyExpressApiRemitRequestV04 x = new MoneyExpressApiRemitRequestV04();
                    string refId = "" + (100000000500 + svcMRemitValidate.GetLastId());
                    x.AmlStatus = n.amlStatus.ToUpper();
                    x.FloatCurrency = n.floatCurrency.ToUpper();
                    x.IncludeFees = includeFees.ToUpper();
                    x.KioskAddress = n.kioskAddress.ToUpper();
                    x.KioskName = n.kioskAddress.ToUpper();
                    x.KycCheck = n.kycCheck.ToUpper();
                    x.MerchantId = merchantId.ToUpper();
                    x.Note = n.note != null ? n.note.ToUpper() : "";
                    x.OriginatorUID = n.originatorUID.ToUpper();
                    x.RcvAccntNo = n.rcvAccntNo.ToUpper();
                    x.RcvAccntType = n.rcvAccntType.ToUpper();
                    x.RcvFirstName = n.rcvFirstName.ToUpper();
                    x.RcvIdNumber = n.rcvIdNumber != null ? n.rcvIdNumber.ToUpper() : "";
                    x.RcvIdType = n.rcvIdType != null ? n.rcvIdType.ToUpper() : "";
                    x.RcvLastName = n.rcvLastName.ToUpper();
                    x.ReferenceId = refId.ToUpper();
                    x.SndAddress = address.ToUpper().Replace("\n", "\r\n");
                    x.SndFirstName = n.sndFirstName.ToUpper();
                    x.SndIdNo = n.sndIdNo.ToUpper();
                    x.SndIdType = n.sndIdType.ToUpper();
                    x.SndKycUrl = n.sndKycUrl != null ? n.sndKycUrl.ToUpper() : "";
                    x.SndLastName = n.sndLastName.ToUpper();
                    x.SndPhoneNo = n.sndPhoneNo != null ? n.sndPhoneNo.ToUpper().Replace("+", "") : "";
                    x.SndRemitPurpose = n.sndRemitPurpose.ToUpper();
                    x.TellerFirstName = n.tellerFirstName.ToUpper();
                    x.TellerLastName = n.tellerLastName.ToUpper();
                    x.TranAmt = n.tranAmt.ToUpper();
                    x.TranCurr = n.tranCurr.ToUpper();
                    x.ValidateReferenceId = n.validateReferenceId != null ? n.validateReferenceId.ToUpper() : "";


                    x.Signature = new EncryptionHelper().HMAC_MD5(MEXMAH.requestRemit(x), key);
                    var y = client.Remit(x);

                    MEXResponseRemitValidateEx r = new MEXResponseRemitValidateEx();
                    r.ActionCode = y.ActionCode;
                    r.ConversionRate = y.ConversionRate != null ? y.ConversionRate : "";
                    r.FeeCurrency = y.FeeCurrency != null ? y.FeeCurrency : "";
                    r.FloatAmount = y.FloatAmount != null ? y.FloatAmount : "";
                    r.FloatCurrency = y.FloatCurrency != null ? y.FloatCurrency : "";
                    r.KioskFeeAmount = y.KioskFeeAmount != null ? y.KioskFeeAmount : "";
                    r.LastUpdateTime = y.LastUpdateTime != null ? y.LastUpdateTime : "";
                    r.MEXType = "VALIDATE";
                    r.Note = y.Note != null ? y.Note : "";
                    r.OriginatorUID = y.OriginatorUID != null ? y.OriginatorUID : "";
                    r.PaidFeeAmount = y.PaidFeeAmount != null ? y.PaidFeeAmount : "";



                    r.RcvAccntNo = y.RcvAccntNo != null ? y.RcvAccntNo : "";
                    r.RcvFirstName = y.RcvFirstName != null ? y.RcvFirstName : "";
                    r.RcvLastName = y.RcvLastName != null ? y.RcvLastName : "";
                    r.ReferenceId = y.ReferenceId != null ? y.ReferenceId : "";
                    r.RespCode = y.RespCode != null ? y.RespCode : "";
                    r.RespMessage = y.RespMessage != null ? y.RespMessage : "";
                    r.SentAmount = y.SentAmount != null ? y.SentAmount : "";
                    r.SentCurrency = y.SentCurrency != null ? y.SentCurrency : "";
                    r.Signature = y.Signature != null ? y.Signature : "";
                    r.SndKycUrl = y.SndKycUrl != null ? y.SndKycUrl : "";
                    r.SubmitTime = y.SubmitTime != null ? y.SubmitTime : "";
                    r.TotalFeeAmount = y.TotalFeeAmount != null ? y.TotalFeeAmount : "";
                    r.TransactionId = y.TransactionId != null ? y.TransactionId : "";

                    if (y.RespCode == "00")
                    {
                        r.QuoteAmt = y.FloatAmount != null ? y.FloatAmount : "";
                        r.QuoteCurr = "USD";
                        r.RcvAccntBankCode = "EMPTY";
                        r.RcvAccntBankName = "EMPTY";
                        new MoneyExpressNotificationSender().sendMoneyExpressEmail(n.sndFirstName + " " + n.sndLastName, n.sndEmail, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate),
                        x.RcvFirstName + " " + x.RcvLastName, x.ReferenceId);
                        new MoneyExpressNotificationSender().sendMoneySwapConfirmationMoney(n.sndFirstName + " " + n.sndLastName, "+" + n.sndPhoneNo, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate), n.referenceNo);
                        new MoneyExpressNotificationSender().RcvMoneySwapConfirmationMoney(n.sndFirstName + " " + n.sndLastName, x.RcvFirstName + " " + x.RcvLastName, "+" + n.sndPhoneNo, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate), n.referenceNo);
                    }
                    else
                    {
                        r.QuoteAmt = y.FloatAmount != null ? y.FloatAmount : "";
                        r.QuoteCurr = "USD";
                        r.RcvAccntBankCode = "EMPTY";
                        r.RcvAccntBankName = "EMPTY";
                    }

                    PartnerCustomerEx sndPC = new PartnerCustomerEx();
                    PartnerCustomerEx rcvPC = new PartnerCustomerEx();

                    sndPC.accountnumber = n.sndAccntNo;
                    sndPC.dateCreated = DateTime.Now;
                    sndPC.email = n.sndEmail;
                    sndPC.firstname = n.sndFirstName;
                    sndPC.lastname = n.sndLastName;
                    sndPC.middlename = n.sndMiddleName;
                    sndPC.mobilenumber = "+" + n.sndPhoneNo.Replace("+", "");
                    sndPC.partnerId = P.id;
                    sndPC.phonenumber = "NA";
                    sndPC.suffix = n.sndSuffix;
                    sndPC.tid = ("" + Guid.NewGuid()).Replace("-", "");

                    rcvPC.accountnumber = n.rcvAccntNo;
                    rcvPC.dateCreated = DateTime.Now;
                    rcvPC.email = n.rcvEmail;
                    rcvPC.firstname = n.rcvFirstName;
                    rcvPC.lastname = n.rcvLastName;
                    rcvPC.middlename = n.rcvMiddleName;
                    rcvPC.mobilenumber = "+" + n.rcvMobileNo.Replace("+", "");
                    rcvPC.partnerId = P.id;
                    rcvPC.phonenumber = "NA";
                    rcvPC.suffix = n.rcvSuffix;
                    rcvPC.tid = ("" + Guid.NewGuid()).Replace("-", "");

                    svcPartnerCustomer.Save(sndPC);
                    svcPartnerCustomer.Save(rcvPC);

                    MoneyExpressRemitEx MER = new MoneyExpressRemitEx();

                    MER.AmlStatus = n.amlStatus;
                    MER.answer = n.answer;
                    MER.FloatCurrency = n.floatCurrency;
                    MER.IncludeFees = includeFees;
                    MER.KioskAddress = n.kioskAddress;
                    MER.KioskName = n.kioskName;
                    MER.KycCheck = n.kycCheck;
                    MER.MerchantId = "00";
                    MER.MEXType = "VALIDATE";
                    MER.Note = n.note;
                    MER.OriginatorUID = n.originatorUID;
                    MER.RcvAccntNo = n.rcvAccntNo;
                    MER.RcvAccntType = n.rcvAccntType;
                    MER.RcvFirstName = n.rcvFirstName;
                    MER.RcvIdNumber = n.rcvIdNumber != null ? n.rcvIdNumber.ToUpper() : "00";
                    MER.RcvIdType = n.rcvIdType;
                    MER.RcvLastName = n.rcvLastName;
                    MER.ReferenceId = refId;
                    MER.Signature = "signature";
                    MER.SndAddress = address.ToUpper().Replace("\n", "__");
                    MER.SndEmail = n.sndEmail;
                    MER.SndFirstName = n.sndFirstName;
                    MER.SndIdNo = n.sndIdNo;
                    MER.SndIdType = n.sndIdType;
                    MER.SndKycUrl = n.sndKycUrl;
                    MER.SndLastName = n.sndLastName;
                    MER.SndPhoneNo = n.sndPhoneNo;
                    MER.SndRemitPurpose = n.sndRemitPurpose;
                    MER.TellerFirstName = n.tellerFirstName;
                    MER.TellerLastName = n.tellerLastName;
                    MER.TranAmt = n.tranAmt;
                    MER.TranCurr = n.tranCurr;
                    MER.ValidateReferenceId = n.validateReferenceId != null ? n.validateReferenceId.ToUpper() : "00";


                    TransactionEx t = new TransactionEx();
                    t.transactionType = n.transactionType;
                    t.userId = rcvPC.id;
                    t.userTID = rcvPC.tid;
                    t.status = "PENDING";

                    t.balance = decimal.Parse(n.tranAmt);
                    t.currencyfromiso = "USD";
                    t.currencytoiso = "USD";
                    t.dataCreated = DateTime.Now;
                    t.credit = true;
                    t.debit = false;
                    t.description = "CHINA REMIT SEND MONEY TO " + n.rcvFirstName.ToUpper() + " " + n.rcvLastName.ToUpper() + " Reference No. " + refId;
                    t.transactionTID = refId;
                    t.enrolledAccountsCurrencyID = 147;
                    t.partnerId = P.id;
                    //var eai = svcEnrolledAccount.GetByIdAndISO(c.id, "USD");
                    //if (eai != null)
                    //{
                    //    foreach (var i in eai)
                    //    {
                    //        t.enrolledAccountsID = i.id;
                    //        t.accountNumber = t.accountnumber;
                    //    }
                    //}

                    if (y.RespCode == "00")
                    {
                        svcMResRemitValidate.Save(r);
                        svcMRemitValidate.Save(MER);
                        svcTransaction.Save(t);
                        RE.code = "RTS";
                        RE.data = new
                        {
                            referenceNo = refId,
                            signature = n.signature,
                            transactionAmt = n.tranAmt,
                            amountsent = y.SentAmount,
                            currencyfrom = n.floatCurrency,
                            currencyto = y.SentCurrency,
                            fees = "" + ((0.01 * Convert.ToDouble(n.tranAmt)) + 5)
                        };
                        RE.message = "SUCCESS";
                        RE.date = DateTime.Now;

                        MoneyExpressNotificationSender MENS = new MoneyExpressNotificationSender();
                        MENS.sendMoneyExpressEmail(n.sndFirstName + " " + n.sndLastName, n.sndEmail, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate),
                        x.RcvFirstName + " " + x.RcvLastName, refId);
                        MENS.sendMoneySwapConfirmationMoney(n.sndFirstName + " " + n.sndLastName, "+" + n.sndPhoneNo, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate), refId);
                        MENS.RcvMoneySwapConfirmationMoney(n.sndFirstName + " " + n.sndLastName, x.RcvFirstName + " " + x.RcvLastName, "+" + n.sndPhoneNo, r.SentCurrency, decimal.Parse(r.FloatAmount) * decimal.Parse(r.ConversionRate), refId);
                    }
                    else
                    {
                        MEXResponseRemitErrorLogEx RREL = new MEXResponseRemitErrorLogEx();

                        RREL.ActionCode = y.ActionCode;
                        RREL.MEXType = y.RespCode;
                        // = y.MEXT
                        //      var dd = (y.GetType()).GetProperty("QouteAmt",String,string[]).GetValue(y,null);
                        //     = (string)y.GetType().GetProperty("QouteAmtField").GetValue(y,null);//.GetValue(y, null);
                        RREL.QuoteAmt = y.FloatAmount != null ? y.FloatAmount : "0";
                        RREL.QuoteCurr = "USD";
                        RREL.Signature = y.Signature != null ? y.Signature : "0000";

                        RREL.RcvAccntBankCode = "EMPTY";
                        RREL.RcvAccntBankName = "EMPTY";
                        RREL.RcvAccntNo = y.RcvAccntNo;
                        RREL.RcvFirstName = y.RcvFirstName;
                        RREL.RcvLastName = y.RcvLastName;
                        RREL.ReferenceId = y.ReferenceId;
                        RREL.RespCode = y.RespCode;
                        RREL.RespMessage = y.RespMessage;
                        RREL.SubmitTime = y.SubmitTime;
                        RREL.TransactionId = y.TransactionId != null ? y.TransactionId : "EMPTY";
                        svcRemitErrorLog.Save(RREL);

                        RE.code = "RTF";
                        RE.data = new
                        {
                            transactionType = n.transactionType,
                            referenceNo = refId,
                            signature = n.signature,
                            rcvAccntNo = n.rcvAccntNo,
                            code = y.RespCode.Replace("MEX", "CRE")

                        };
                        RE.message = "FAILED: " + y.RespMessage;
                        RE.date = DateTime.Now;

                    }


                    client.Close();


                    return RE;
                }
                else
                {
                    RE.code = "RTF";
                    RE.date = DateTime.Now;
                    RE.data = new
                    {
                        referenceNo = n.referenceNo,
                        signature = n.signature,
                        partnerCode = n.partnerCode
                    };
                    RE.message = "NO PARTNER AVAILABLE";
                    return RE;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ChinaRemit-Log");
                return null;
            }



        }

     


    
    }
}
