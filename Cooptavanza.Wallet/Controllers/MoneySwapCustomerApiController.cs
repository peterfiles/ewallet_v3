﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;


namespace Cooptavanza.Wallet.Controllers
{
    public class MoneySwapCustomerApicontroller : ApiController
    {
        //readonly DummyLoadService svcDummyLoad;
        readonly MoneySwapCustomerService svcMoneySwapCustomer;
        readonly UsersTempService svcUsersTemp;
        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly TransactionService svcTransaction;
        readonly TransferMoneyService svcTransferMoney;
        readonly CurrencyService svcCur;
        readonly ImageIdCardService svcImageIdCard;
        readonly ReceiversCustomerService svcReceiverMoneySwapCustomer;
        readonly MoneyTransferTransactionsService svcMoneyTansferTransaction;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly CurrencyService svcCurrency;
        readonly TransactionLogService svcError;


        public MoneySwapCustomerApicontroller(
            MoneySwapCustomerService svcMoneySwapCustomer, 
            UsersTempService svcUsersTemp, 
            UserAccountService svcUserAccount, 
            OFAClistService svcOFAC, 
            //DummyLoadService svcDummyLoad, 
            TransactionService svcTransaction, 
            TransferMoneyService svcTransferMoney, 
            CurrencyService svcCur, 
            ImageIdCardService svcImageIdCard,
            ReceiversCustomerService svcReceiverMoneySwapCustomer,
            MoneyTransferTransactionsService svcMoneyTansferTransaction,
            EnrolledAccountService svcEnrolledAccount,
            CurrencyService svcCurrency,
            TransactionLogService svcError)
        {
            //this.svcDummyLoad = svcDummyLoad;
            this.svcReceiverMoneySwapCustomer = svcReceiverMoneySwapCustomer;
            this.svcUserAccount = svcUserAccount;
            this.svcMoneySwapCustomer = svcMoneySwapCustomer;
            this.svcUsersTemp = svcUsersTemp;
            this.svcOFAC = svcOFAC;
            this.svcTransaction = svcTransaction;
            this.svcTransferMoney = svcTransferMoney;
            this.svcImageIdCard = svcImageIdCard;
            this.svcCur = svcCur;
            this.svcMoneyTansferTransaction = svcMoneyTansferTransaction;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcCurrency = svcCurrency;
            this.svcError = svcError;
        }

        [Route("MoneySwapCustomer")]
        [HttpGet]
        public MoneySwapCustomerSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new MoneySwapCustomerSearchViewModel() { Results = svcMoneySwapCustomer.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcMoneySwapCustomer.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return null;
            }
        }

        [Route("sndMoney")]
        [HttpGet]
        public string SendMoney([FromBody] CustomerEx c)
        {
            return "";
        }

        [Route("MoneySwapCustomer/{Id}")]
        [HttpGet]
        public MoneySwapCustomerEx GetPerson(int Id)
         {
            try
            {
                return svcMoneySwapCustomer.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return null;
            }
        }

        [Route("MoneySwapCustomer/mobilenumber/")]
        [HttpGet]
        public Boolean CheckMoneySwapCustomerMobile(string str ="")
        {
            try
            {
                str = "+" + str.Trim();
                MoneySwapCustomerEx data = svcMoneySwapCustomer.GetCheckMobileEmail(str);
                if (data != null)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return false;
            }
        }

       

        [Route("MoneySwapCustomer/email/")]
        [HttpGet]
        public Boolean CheckEmail(string str)
        {
            //str = "+" + str.Trim();
            try
            {
                MoneySwapCustomerEx data = svcMoneySwapCustomer.GetCheckEmail(str);
                if (data != null)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return false;
            }
        }

      

        [Route("MoneySwapCustomer/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public object Save([FromBody] MoneySwapCustomerEx n)
        {
            try
            {
                object retData = null;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                string errmessage = string.Empty;
                var tempid = n.id;
                n.username = n.mobilenumber;
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.gender = Convert.ToBoolean(n.gender);
                if (n.id != 0)
                {
                    MoneySwapCustomerEx ce = svcMoneySwapCustomer.GetCheckEmail(n.email);
                    if (ce.id != n.id)
                    {
                        errmessage = "Existing Email found!";
                        return retData = new
                        {
                            ErrorMessage = errmessage,
                            cData = n
                        };
                    }
                    else
                    {
                        svcMoneySwapCustomer.Save(n);
                    }
                }
                else
                {
                    MoneySwapCustomerEx ce = svcMoneySwapCustomer.GetCheckEmail(n.email);
                    if (ce == null)
                    {
                        svcMoneySwapCustomer.Save(n);
                    }
                    else
                    {
                        return retData = new
                        {
                            ErrorMessage = errmessage,
                            cData = n
                        };
                    }

                }

                if (checkIfClientIsInOFAC(n))
                {
                    Generator g = new Generator();
                    g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
                }
                MoneySwapCustomerEx data = svcMoneySwapCustomer.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password != "" ? password : x.password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }
                retData = new
                {
                    ErrorMessage = errmessage,
                    cData = data
                };
                return retData;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return null;
            }

        }

       
        


        [Route("MoneySwapCustomer/MoneySwapCustomer/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public MoneySwapCustomerEx Saveadmin([FromBody] MoneySwapCustomerEx n)
        {
            try
            {
                var tempid = n.id;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                n.username = n.mobilenumber;
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.gender = Convert.ToBoolean(n.gender);
                svcMoneySwapCustomer.Save(n);
                if (checkIfClientIsInOFAC(n))
                {
                    Generator g = new Generator();
                    g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
                }
                MoneySwapCustomerEx data = svcMoneySwapCustomer.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password != "" ? password : x.password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }

                return data;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return null;
            }

        }

        [Route("MoneySwapCustomer/adminsave")]
        [HttpPost]
        public MoneySwapCustomerEx AdminSave([FromBody] MoneySwapCustomerEx n)
        {
            try
            {
                var tempid = n.id;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                n.username = n.mobilenumber;
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.gender = Convert.ToBoolean(n.gender);
                svcMoneySwapCustomer.Save(n);
                if (checkIfClientIsInOFAC(n))
                {
                    Generator g = new Generator();
                    g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
                }
                MoneySwapCustomerEx data = svcMoneySwapCustomer.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password != "" ? password : x.password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }

                return data;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return null;
            }
        }
      

        [Route("MoneySwapCustomer/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcMoneySwapCustomer.Remove(Id);
        }

        [Route("MoneySwapCustomer/GetMoneySwapCustomerByMobileNumber/{mobileNumber}")]
        [HttpGet]
        public MoneySwapCustomerEx GetByMobileNumber(string mobileNumber)
        {
            try
            {
                return svcMoneySwapCustomer.GetByMobileNumber(mobileNumber);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return null;
            }
        }

       
      

        [Route("MoneySwapCustomer/MoneySwapCustomerIfIsInOFAC")]
        [HttpPost]
        public bool checkIfClientIsInOFAC([FromBody] MoneySwapCustomerEx n)
        {
            try
            {
                string res = string.Empty;
                bool isVerified = false;
                bool isInOFAC = false;
                List<OFAClistEx> list = new List<OFAClistEx>();
                using (WebClient wb = new WebClient())
                {
                    wb.Headers.Add("Content-Type", "application/json");
                    res = wb.DownloadString("https://easyofac.com/api/nameSearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&first_name=" + n.firstname + "&last_name=" + n.lastname + "&test=1");
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
                    if (list.Count > 0)
                    {
                        foreach (OFAClistEx d in list)
                        {
                            d.userID = n.id;
                            d.userCode = 1007;
                            d.userIsVerified = false;
                            d.isCompany = false;
                            svcOFAC.Save(d);
                        }
                        isInOFAC = true;
                    }
                    else
                    {
                        isInOFAC = false;
                        isVerified = true;
                    }
                }
                n.isInOFACList = isInOFAC;
                n.isVerified = isVerified;
                svcMoneySwapCustomer.Save(n);
                return isInOFAC;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "MoneySwap-Log");
                return false;
            }
        }

    }
}
