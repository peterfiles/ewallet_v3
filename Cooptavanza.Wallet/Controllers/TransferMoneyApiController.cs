﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;

using Cooptavanza.Wallet.Repository;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;

namespace Cooptavanza.Wallet.Controllers
{
    public class TransferMoneyApicontroller : ApiController
    {
        
        readonly CustomerService svcCustomer;
        readonly TransactionService svcTransaction;
        readonly TransferMoneyService svcTransferMoney;
        readonly MoneyTransferTransactionsService svcMoneyTransferSvc;
        readonly CurrencyService svcCurrency;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly CoopTransactionLogService svcCTLS;
        readonly CoopEnrolledAccountService svcEAS;


        public TransferMoneyApicontroller(
            CustomerService svcCustomer, 
            TransactionService svcTransaction, 
            TransferMoneyService svcTransferMoney,
            MoneyTransferTransactionsService svcMoneyTransferSvc,
            CurrencyService svcCurrency,
            EnrolledAccountService svcEnrolledAccount,
            CoopTransactionLogService svcCTLS,
            CoopEnrolledAccountService svcEAS
            )
        {
            
            this.svcCustomer = svcCustomer;
            this.svcCurrency = svcCurrency;
            this.svcTransaction = svcTransaction;
            this.svcTransferMoney = svcTransferMoney;
            this.svcMoneyTransferSvc = svcMoneyTransferSvc;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcCTLS = svcCTLS;
            this.svcEAS = svcEAS;
        }

        [Route("TransferMoney")]
        [HttpGet]
        public TransferMoneySearchViewModel Get(string SearchText = "", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new TransferMoneySearchViewModel() { Results = svcTransferMoney.GetPaged(SearchText, "Id Asc", PageNo, PageSize), Count = svcTransferMoney.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransferMoney-Log");
                return null;
            }
        }



        [Route("answer")]
        [HttpPost]
        public string ValidateAnswer([FromBody] TransferMoneyEx n)
        {
            try
            {
                //answer need encryption
                var ans = svcTransferMoney.Get(n.id);
                if (n.answer == ans.answer)
                {

                    if (n.benificiaryemail != null)
                    {
                        var i = svcCustomer.GetCheckEmail(n.benificiaryemail);
                        if (i != null)
                        {
                            var x = svcTransaction.Get(n.transactionid);
                            CustomerEx c = new CustomerEx();
                            CoopTransactionLogEx ctlEx = svcCTLS.GetByTransactionTID(x.transactionTID);
                            if (ctlEx != null)
                            {
                                ctlEx.id = 0;
                                ctlEx.amount = x.balance;
                                ctlEx.currencyAccountFrom = ctlEx.currencyAccountFrom;
                                ctlEx.currencyAccountTo = ctlEx.currencyAccountTo;
                                ctlEx.date_created = DateTime.Now;
                                ctlEx.giverTID = ctlEx.giverTID;
                                ctlEx.giverUserId = ctlEx.giverUserId;
                                ctlEx.giverUserRoles = ctlEx.giverUserRoles;
                                ctlEx.isCredit = true;
                                ctlEx.receiverTID = ctlEx.receiverTID;
                                ctlEx.receiverUserId = ctlEx.receiverUserId;
                                ctlEx.receiverUserRoles = ctlEx.receiverUserRoles;
                                ctlEx.transactionDescription = ctlEx.transactionDescription.Replace("PENDING", "CLEARED");
                                ctlEx.transactionTID = ctlEx.transactionTID;
                                svcCTLS.Save(ctlEx);


                                x.status = "CLEARED";
                                x.description = ctlEx.transactionDescription;
                                svcTransaction.Save(x);

                                ans.status = "CLEARED";
                                svcTransferMoney.Save(ans);

                            }
                            else
                            {
                                c = svcCustomer.Get(n.customerid);
                                x.status = "CLEARED";
                                svcTransaction.Save(x);
                                TransactionEx t = new TransactionEx();
                                t.dataCreated = DateTime.Now;
                                t.credit = true;
                                t.debit = false;
                                t.balance = n.amountto;
                                t.currencytoiso = n.currencytoiso;
                                t.currencyfromiso = n.currencyfromiso;
                                t.userId = i.id;
                                t.userTID = i.tid;
                                t.status = "CLEARED";
                                t.transactionTID = "" + Guid.NewGuid();
                                t.transactionType = "11012";
                                t.description = "Email money tranfer received from " + (c.firstname).ToUpper();
                                svcTransaction.Save(t);
                                var cus = svcCustomer.Get(i.id);
                                Boolean content = false;
                                foreach (var o in cus.EnrolledList)
                                {
                                    if (o.currency == x.currencytoiso)
                                    {
                                        content = true;
                                    }
                                }
                                if (content == false)//create new account for user
                                {
                                    var e = new EnrolledAccountEx();
                                    var cu = svcCurrency.GetByISO(x.currencytoiso);
                                    e.currenycid = cu.id;
                                    e.currency = x.currencytoiso;
                                    e.accountDefault = false;
                                    e.userId = cus.id;
                                    e.userTID = cus.tid;
                                    svcEnrolledAccount.Save(e);
                                }

                                ans.status = "CLEARED";
                                svcTransferMoney.Save(ans);
                            }
                            return "success";
                        }
                        else
                        {
                            n.status = new Generator().OTP();
                            svcTransferMoney.Save(n);
                            return n.status;
                        }
                    }
                    else if (n.benificiarymobileno != null)
                    {

                        var i = svcCustomer.GetByMobileNumber(n.benificiarymobileno);
                        if (i != null)
                        {
                            var c = svcCustomer.Get(n.customerid);
                            var x = svcTransaction.Get(n.transactionid);
                            x.status = "CLEARED";
                            svcTransaction.Save(x);
                            TransactionEx t = new TransactionEx();
                            t.dataCreated = DateTime.Now;
                            t.credit = true;
                            t.debit = false;
                            t.balance = n.amountto;
                            t.currencytoiso = n.currencytoiso;
                            t.currencyfromiso = n.currencyfromiso;
                            t.userId = i.id;
                            t.status = "CLEARED";
                            t.transactionTID = "" + Guid.NewGuid();
                            t.transactionType = "11011";
                            t.description = "SMS money tranfer received from " + (c.firstname).ToUpper();
                            svcTransaction.Save(t);

                            var cus = svcCustomer.Get(i.id);
                            Boolean content = false;
                            foreach (var o in cus.EnrolledList)
                            {
                                if (o.currency == x.currencytoiso)
                                {
                                    content = true;
                                }
                            }
                            if (content == false)//create new account for user
                            {
                                var e = new EnrolledAccountEx();
                                var cu = svcCurrency.GetByISO(x.currencytoiso);
                                e.currenycid = cu.id;
                                e.currency = x.currencytoiso;
                                e.accountDefault = false;
                                e.userId = cus.id;
                                e.userTID = cus.tid;
                                svcEnrolledAccount.Save(e);
                            }

                            return "success";
                        }
                        else { return "error"; }
                    }
                    else { return "error"; }
                }
                else { return "error"; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransferMoney-Log");
                return "failed";
            }
        }


        [Route("TransferMoney/ValidateAccountToQueryTransactions")] //Transactions
        [HttpGet]
        public object ValidateAccountToQueryTransactions(string accountNo, string password)
        {
            try
            {
                return svcMoneyTransferSvc.getMoneyTransferTransactionsByTrackingNumberAndPassword(accountNo, password);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransferMoney-Log");
                return null;
            }
        }

        [Route("TransferMoney/ClaimMoneyTransfer")] //Transactions
        [HttpGet]
        public object ClaimMoneyTransfer(int mtTID, int agentID, string idType, string idNumber)
        {
            try
            {
                object retData = null;

                bool toUpdate = true;
                if (idType.Equals(string.Empty) || idType == null)
                {
                    toUpdate = false;
                }
                if (idNumber.Equals(string.Empty) || idNumber == null)
                {
                    toUpdate = false;
                }

                MoneyTransferTransactionsEx mtte = svcMoneyTransferSvc.Get(mtTID);
                mtte.status = "CLEARED";
                mtte.idType = idType;
                mtte.idNumber = idNumber;
                mtte.agentID = agentID;

                TransactionEx te = svcTransaction.Get(mtte.transactionID);
                te.status = mtte.status;
                if (toUpdate)
                {
                    svcTransaction.Save(te);
                    svcMoneyTransferSvc.Save(mtte);
                    retData = new
                    {
                        messageRes = "Success",
                        data = svcMoneyTransferSvc.Get(mtTID)
                    };

                }
                else
                {
                    retData = new
                    {
                        messageRes = "Failed",
                        data = ""
                    };
                }

                return retData;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransferMoney-Log");
                return null;
            }
        }


    }
}
