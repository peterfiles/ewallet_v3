﻿using Cooptavanza.Models.ViewModels;
using Cooptavanza.Wallet.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Cooptavanza.Wallet.Controllers
{
    public class ServicesFeeApiController : ApiController
    {
        readonly ServiceFeeService svcServiceFeeService;
        readonly PayInPaymentRatesService svcPayInPaymentRates;

        public ServicesFeeApiController(ServiceFeeService svcServiceFeeService,
                                        PayInPaymentRatesService svcPayInPaymentRates)
        {
            this.svcServiceFeeService = svcServiceFeeService;
            this.svcPayInPaymentRates = svcPayInPaymentRates;
        }


        [Route("ServiceFees")]
        [HttpGet]
        public ServiceFeeSearchViewModel GetServiceList(int id, string TID, string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 1)
        {
            try
            {
                return new ServiceFeeSearchViewModel() { Results = svcServiceFeeService.GetPaged(id, TID, SearchText, SortBy, PageNo, PageSize), Count = svcServiceFeeService.Count(id, TID, SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ServiceFees-Log");
                return null;
            }
        }


        [Route("ServiceFee/{Id}")]
        [HttpGet]
        public ServiceFeeEx GetServiceFee(int Id)
        {
            try { 
            return svcServiceFeeService.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ServiceFees-Log");
                return null;
            }
        }

        [Route("Service/adminsave")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public ServiceFeeEx Save([FromBody] ServiceFeeEx n)
        {
            try { 
            svcServiceFeeService.Save(n);
            return svcServiceFeeService.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ServiceFees-Log");
                return null;
            }
        }


        [Route("service/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {

        }
        
        [Route("PayInPaymentRates/GetData")]
        [HttpGet]
        public object PayInPaymentRatesGetByCountryName(string str="", int met=0)
        {
            try { 
            return new GlobalCallForServicesController(svcPayInPaymentRates).PaymentRatesGetByStrAndMet(str,met);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ServiceFees-Log");
                return null;
            }
        }

        [Route("Fees/GetAdditionalFees")]
        [HttpGet]
        public object getAdditionalFees(string country, int productId)
        {
            try
            {
                List<PaymentRatesForPayinsEx> ppEx = new GlobalCallForServicesController(svcPayInPaymentRates).PaymentRatesGetByStrAndMet(country, 1);

                decimal mdr = Convert.ToDecimal(ppEx[0].mdr);
                decimal txFee = Convert.ToDecimal(ppEx[0].d2[0].txfee);
                string currency = ppEx[0].d2[0].currency;
                string cntry = ppEx[0].country;


                return new
                {
                    mdr = mdr,
                    txFee = txFee,
                    currency = currency,
                    cntry = cntry
                };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ServiceFees-Log");
                return null;
            }
        }

        [Route("Fees/CalculateCharges")]
        [HttpPost]
        public object calculateCharges(CalculateChargesEx data)
        {
            try { 
            decimal txChargeFee = calculateChargeFee(data.txFee, data.senderConvertedAmount);
            decimal mdrChargeFee = calculateChargeFee(data.mdr, txChargeFee);
            decimal deductxChargeByMdr = txChargeFee - mdrChargeFee;
            //decimal srvChargeFee = calculateChargeFee(data.srvPrcnt, data.senderConvertedAmount);

            return new
            {
                mcf = mdrChargeFee, //mdrChargeFee
                txcf = txChargeFee, //txChargeFee
                dmdr = deductxChargeByMdr,
                //scf = srvChargeFee, //srvChargeFee
                //tcf = txChargeFee, // totalChargeFee
                amntToSend = toAddInTotalAmountToSend(txChargeFee, data.senderAmount) //toDeductInTotalAmountToSend
            };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ServiceFees-Log");
                return null;
            }
        }

        public decimal calculateChargeFee(decimal percent, decimal amount)
        {
            return ((percent * 100) / 100) * amount;
        }

        //public decimal totalChargeFee(decimal mdrFee, decimal txFee)
        //{
        //    decimal totalFees = mdrFee + txFee;
        //    return Math.Round(totalFees);
        //}

        public decimal toAddInTotalAmountToSend(decimal totalChargeFee, decimal amountToSend)
        {
            return amountToSend + totalChargeFee;
        }

    }
}
