﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;


namespace Cooptavanza.Wallet.Controllers
{
    public class ReceiversCustomerApiController : ApiController
    {
        //readonly DummyLoadService svcDummyLoad;
        //readonly CustomerService svcCustomer;
        //readonly UsersTempService svcUsersTemp;
        //readonly UserAccountService svcUserAccount;
        //readonly OFAClistService svcOFAC;
        //readonly TransactionService svcTransaction;
        //readonly TransferMoneyService svcTransferMoney;
        //readonly CurrencyService svcCur;
        //readonly ImageIdCardService svcImageIdCard;

        //public ReceiversCustomerApiController(
        //    CustomerService svcCustomer, 
        //    UsersTempService svcUsersTemp, 
        //    UserAccountService svcUserAccount, 
        //    OFAClistService svcOFAC, 
        //    //DummyLoadService svcDummyLoad, 
        //    TransactionService svcTransaction, 
        //    TransferMoneyService svcTransferMoney, 
        //    CurrencyService svcCur, 
        //    ImageIdCardService svcImageIdCard)
        //{
        //    //this.svcDummyLoad = svcDummyLoad;
        //    this.svcUserAccount = svcUserAccount;
        //    this.svcCustomer = svcCustomer;
        //    this.svcUsersTemp = svcUsersTemp;
        //    this.svcOFAC = svcOFAC;
        //    this.svcTransaction = svcTransaction;
        //    this.svcTransferMoney = svcTransferMoney;
        //    this.svcImageIdCard = svcImageIdCard;
        //    this.svcCur = svcCur;
        //}

        //[Route("ReceiversCustomer")]
        //[HttpGet]
        //public CustomerSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        //{
        //        return new CustomerSearchViewModel() { Results = svcCustomer.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcCustomer.Count(SearchText) };
        //}



        //[Route("ReceiversCustomer/{Id}")]
        //[HttpGet]
        //public ReceiversCustomerEx GetPerson(int Id)
        // {
        //    // return svcCustomer.Get(Id);
        //    return null;
        //}

        //[Route("ReceiversCustomer/mobilenumber/")]
        //[HttpGet]
        //public Boolean CheckCustomerMobile(string str ="")
        //{
        //    str = "+" + str.Trim();
        //    R//eceiversCustomerEx data = svcCustomer.GetCheckMobileEmail(str);
        //    if (data != null)
        //    {
        //        return true;
        //    }
        //    else { return false; }
        //}

        //[Route("ReceiversCustomer/email/")]
        //[HttpGet]
        //public Boolean CheckEmail(string str = "")
        //{
        //    //str = "+" + str.Trim();
        //    CustomerEx data = svcCustomer.GetCheckMobileEmail(str);
        //    if (data != null)
        //    {
        //        return true;
        //    }
        //    else { return false; }
        //}

        //[Route("ReceiversCustomer/login")]
        //[HttpPost]
        //public CustomerEx GetPerson([FromBody] CustomerEx n)
        //{
        //    string mobile = n.mobilenumber;
        //    string password = "";
        //    if (n.password != null)
        //    {
        //        password = new EncryptionHelper().Encrypt(n.password);
        //    }
        //    var data = svcCustomer.GetLogin(mobile, password);
        //    if (data != null)
        //    {
        //        return data;

        //    }
        //    return null;
        //}

        //[Route("ReceiversCustomer/Save")]
        //[HttpPost]
        //// [Authorize(Roles = "administrators")]
        //public CustomerEx Save([FromBody] CustomerEx n)
        //{
        //    string password =n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
        //    var tempid = n.id;
        //    n.username = n.mobilenumber;
        //    n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
        //    n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
        //    svcCustomer.Save(n);
        //    if (checkIfClientIsInOFAC(n))
        //    {
        //        Generator g = new Generator();
        //        g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
        //    }
        //    CustomerEx data = svcCustomer.Get(n.id);
        //    UserAccountEx ua = new UserAccountEx();
        //    if (tempid == 0)
        //    {
        //        ua.tID = n.tid;
        //        ua.password = password;
        //        ua.user_id = n.id;
        //        ua.roles_id = 7;
        //        ua.username = n.username;
        //        //ua.roles_id = svcRole.; temporary
        //        svcUserAccount.Save(ua);
        //    }
        //    else
        //    {
        //        var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
        //        ua.id = x.id;
        //        ua.tID = n.tid;
        //        ua.password = password != "" ? password : x.password;
        //        ua.user_id = n.id;
        //        ua.roles_id = 7;
        //        ua.username = n.username;
        //        //ua.roles_id = svcRole.; temporary
        //        svcUserAccount.Save(ua);
        //    }

        //    return data;

        //}


        //[Route("ImageIdCard/Save")]
        //[HttpPost]
        //// [Authorize(Roles = "administrators")]
        //public ImageIdCardEx saveImageIdCard([FromBody] ImageIdCardEx n)
        //{

        //    svcImageIdCard.Save(n);
        //    return svcImageIdCard.Get(n.id);

        //}


        //[Route("ReceiversCustomer/ReceiversCustomer/Save")]
        //[HttpPost]
        //// [Authorize(Roles = "administrators")]
        //public CustomerEx Saveadmin([FromBody] CustomerEx n)
        //{
        //    var tempid = n.id;
        //    string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
        //    n.username = n.mobilenumber;
        //    n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
        //    n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
        //    svcCustomer.Save(n);
        //    if (checkIfClientIsInOFAC(n))
        //    {
        //        Generator g = new Generator();
        //        g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
        //    }
        //    CustomerEx data = svcCustomer.Get(n.id);
        //    UserAccountEx ua = new UserAccountEx();
        //    if (tempid == 0)
        //    {
        //        ua.tID = n.tid;
        //        ua.password = password;
        //        ua.user_id = n.id;
        //        ua.roles_id = 7;
        //        ua.username = n.username;
        //        //ua.roles_id = svcRole.; temporary
        //        svcUserAccount.Save(ua);
        //    }
        //    else
        //    {
        //        var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
        //        ua.id = x.id;
        //        ua.tID = n.tid;
        //        ua.password = password != "" ? password : x.password;
        //        ua.user_id = n.id;
        //        ua.roles_id = 7;
        //        ua.username = n.username;
        //        //ua.roles_id = svcRole.; temporary
        //        svcUserAccount.Save(ua);
        //    }

        //    return data;

        //}

        //[Route("ReceiversCustomer/adminsave")]
        //[HttpPost]
        //public CustomerEx AdminSave([FromBody] CustomerEx n)
        //{
        //    var tempid = n.id;
        //    string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
        //    n.username = n.mobilenumber;
        //    n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
        //    n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
        //    svcCustomer.Save(n);
        //    if (checkIfClientIsInOFAC(n))
        //    {
        //        Generator g = new Generator();
        //        g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
        //    }
        //    CustomerEx data = svcCustomer.Get(n.id);
        //    UserAccountEx ua = new UserAccountEx();
        //    if(tempid == 0)
        //    {
        //        ua.tID = n.tid;
        //        ua.password = password;
        //        ua.user_id = n.id;
        //        ua.roles_id = 7;
        //        ua.username = n.username;
        //        //ua.roles_id = svcRole.; temporary
        //        svcUserAccount.Save(ua);
        //    }
        //    else
        //    {
        //        var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
        //        ua.id = x.id;
        //        ua.tID = n.tid;
        //        ua.password = password != "" ? password : x.password;
        //        ua.user_id = n.id;
        //        ua.roles_id = 7;
        //        ua.username = n.username;
        //        //ua.roles_id = svcRole.; temporary
        //        svcUserAccount.Save(ua);
        //    }

        //    return data;

        //}


        //[Route("ReceiversCustomer/{Id}")]
        //[HttpDelete]
        ////[Authorize(Roles = "administrators")]
        //public void Delete(int Id)
        //{
        //    svcCustomer.Remove(Id);
        //}

        //[Route("ReceiversCustomer/GetCustomerByMobileNumber/{mobileNumber}")]
        //[HttpGet]
        //public CustomerEx GetByMobileNumber(string mobileNumber)
        //{
        //    return svcCustomer.GetByMobileNumber(mobileNumber);
        //}



        //[Route("TF")]
        //[HttpGet]
        //public TransferMoneyEx GetTransferMoney(string tk="",int id = 0)
        //{
        //    return svcTransferMoney.GetTransferMoneyByTk(tk, id);
        //}
        //[Route("TFA")]
        //[HttpGet]
        //public string GetTransferMoneyWithOtp(string o = "", string tk = "", int id = 0, int ui = 0)
        //{
        //    var i = svcTransferMoney.GetTransferMoneyWithOtp(o, tk, id);
        //    if (i != null)
        //    {
        //        CustomerEx r = svcCustomer.Get(i.customerid);
        //        CustomerEx c = svcCustomer.Get(ui);
        //        TransactionEx x = svcTransaction.Get(id);

        //        x.status = "CLEARED";
        //        svcTransaction.Save(x);
        //        TransactionEx t = new TransactionEx();
        //        t.dataCreated = DateTime.Now;
        //        t.credit = true;
        //        t.debit = false;
        //        t.balance = i.amountto;
        //        t.currencytoiso = i.currencytoiso;
        //        t.currencyfromiso = i.currencyfromiso;
        //        t.userId = r.id;
        //        t.userTID = r.tid;
        //        t.status = "CLEARED";
        //        t.transactionTID = "" + Guid.NewGuid();
        //        t.transactionType = "EMAIL_MONEY_TRANSFER_RECEIVED";
        //        t.description = "Email money tranfer received from " + (c.firstname).ToUpper() + " " + (c.lastname).ToUpper();
        //        svcTransaction.Save(t);
        //        return "success";
        //    }

        //    return "failed";
        //}


        ////[Route("LoadinDummy")]
        ////[HttpPost]
        ////public bool Loadin([FromBody] DummyLoadEx n)
        ////{
        ////    return svcDummyLoad.getByCredentials(n.accountnumber, n.userpin, n.userid);
        ////}


        //[Route("Register/")]
        //[HttpPost]
        //// [Authorize(Roles = "administrators")]
        //public UsersTempEx Register([FromBody] UsersTempEx n)
        //{
        //    n.OTP = new Generator().OTP();

        //    Guid tok = Guid.NewGuid();
        //    n.token_id = "" + tok;
        //    svcUsersTemp.Save(n);
        //    var ret = svcUsersTemp.Get(n.id);
        //    SendSms(ret.mobile_number, ret.OTP);
        //    return ret;

        //}



        //[Route("sendmoney/")]
        //[HttpPost]
        //// [Authorize(Roles = "administrators")]
        //public string sendmoney([FromBody] TransferMoneyEx n)
        //{
        //    CustomerEx c = new CustomerEx();
        //    TransactionEx t = new TransactionEx();

        //    c = svcCustomer.Get(n.customerid);
        //    n.customerid = c.id;
        //    n.customertid = c.tid;
        //    t.dataCreated = n.datecreated;
        //    t.credit = false;
        //    t.debit = true;
        //    t.balance = n.amountfrom;

        //    t.userId = c.id;
        //    t.userTID = c.tid;
        //    t.status = "PENDING";
        //    t.transactionTID = "" + Guid.NewGuid();
        //    n.transactionTID = t.transactionTID;
        //    if (n.transactionid == 1)
        //    {
        //        t.transactionType = "EMAIL_MONEY_TRANSFER";
        //        t.description = "Email money tranfer to " + n.benificiaryfullname + " Email: " + n.benificiaryemail;
        //        n.transactiontype = 1;
        //        svcTransaction.Save(t);
        //        svcTransferMoney.Save(n);
        //        return new Generator().sendemail(t, n);
        //    }
        //    else if(n.transactionid == 2)
        //    {
        //        t.transactionType = "SMS_MONEY_TRANSFER";
        //        t.description = "SMS money tranfer to " + n.benificiaryfullname + " Mobile Number:" + n.benificiarymobileno ;
        //        n.transactiontype = 2;
        //        svcTransaction.Save(t);
        //        svcTransferMoney.Save(n);
        //        return new Generator().sendSMSMoney(t, n, c.mobilenumber);
        //    }
        //    return "failed";
        //}



        //public string SendSms(string mobile_number, string OTP)
        //{

        //    const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
        //    const string authToken = "9185158d2ea6518e1178805630b68f0b";
        //    TwilioClient.Init(accountSid, authToken);

        //    try { 
        //    var message = MessageResource.Create(
        //        to: new PhoneNumber(mobile_number),
        //        from: new PhoneNumber("+17027896606"),
        //        body: "Please use this OTP:" + OTP + " to verify your EWallet Account.");
        //        //  Console.WriteLine(message.Sid);
        //        return "send successfull";
        //        //   Console.ReadKey();
        //    }
        //    catch
        //    {
        //        return "Illegal";
        //    }

        //}

        //[Route("SendEmailMoneyGetCustomerData/{id}")]
        //[HttpGet]
        //public CustomerEx Get(int id)
        //{
        //    return svcCustomer.Get(id);
        //}

        //[Route("ReceiversCustomer/CustomerIfIsInOFAC")]
        //[HttpPost]
        //public bool checkIfClientIsInOFAC([FromBody] CustomerEx n)
        //{
        //    string res = string.Empty;
        //    bool isVerified = false;
        //    bool isInOFAC = false;
        //    List<OFAClistEx> list = new List<OFAClistEx>();
        //    using (WebClient wb = new WebClient())
        //    {
        //        wb.Headers.Add("Content-Type", "application/json");
        //        res = wb.DownloadString("https://easyofac.com/api/nameSearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&first_name=" + n.firstname + "&last_name=" + n.lastname + "&test=1");
        //        list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
        //        if (list.Count > 0)
        //        {
        //            foreach (OFAClistEx d in list)
        //            {
        //                d.userID = n.id;
        //                d.userCode = 1007;
        //                d.userIsVerified = false;
        //                d.isCompany = false;
        //                svcOFAC.Save(d);
        //            }
        //            isInOFAC = true;
        //        }
        //        else
        //        {
        //            isInOFAC = false;
        //            isVerified = true;
        //        }
        //    }
        //    n.isInOFACList = isInOFAC;
        //    n.isVerified = isVerified;
        //    svcCustomer.Save(n);
        //    return isInOFAC;
        //}

    }
}
