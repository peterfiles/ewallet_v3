﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Newtonsoft.Json;
using System.Web.UI;
using System.Security.Cryptography;
using System.Text;

namespace Cooptavanza.Wallet.Controllers
{
    public class OTPAccountApiController : ApiController
    {
        readonly OTPAccountService svcOTPAccount;
        readonly UserAccountService svcUserAccount;

        readonly CustomerService svcCustomer;
        readonly UsersTempService svcUsersTemp;
   
        readonly OFAClistService svcOFAC;
        readonly AgentsService svcAgent;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;
        readonly PartnersService svcPartner;
        readonly UserServicesService svcUserServicesService;

        public OTPAccountApiController(OTPAccountService svcOTPAccount, 
            CustomerService svcCustomer, UsersTempService svcUsersTemp,
            UserAccountService svcUserAccount, OFAClistService svcOFAC, DummyLoadService svcDummyLoad,
            AgentsService svcAgent, MerchantService svcMerchant, UsersService svcUsers,
            PartnersService svcPartner, UserServicesService svcUserServicesService)
        {
            this.svcOTPAccount = svcOTPAccount;
            this.svcUserAccount = svcUserAccount;
            this.svcCustomer = svcCustomer;
            this.svcUsersTemp = svcUsersTemp;
            this.svcOFAC = svcOFAC;
            this.svcAgent = svcAgent;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcPartner = svcPartner;
            this.svcUserServicesService = svcUserServicesService;
        }
        string offSetnow = "";
        //[Route("OTPAccount")]
        //[HttpGet]
        //public OTPAccountSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        //{
        //    return new OTPAccountSearchViewModel() { Results = svcOTPAccount.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcOTPAccount.Count(SearchText) };
        //}

        [Route("LoginWithOTP")]
        [HttpPost]
        public object Login([FromBody] AuthMobileEx n)
        {
            try
            {
                string OTP = n.password != null ? n.password : "";
                string username = n.username != null ? n.username : "";
                UserReturnEx ret = new UserReturnEx();
                UserAccountEx ua = svcUserAccount.CheckUsername(username);
                if (ua != null)
                {
                    OTPAccountEx oa = svcOTPAccount.getByUR(ua.user_id, ua.roles_id);
                    if (oa != null)
                    {
                        DateTime dateServer = oa.dateTime ?? DateTime.UtcNow;
                        DateTime dateMobile = oa.mobileUTC ?? DateTime.UtcNow;
                        long offset = (long)(dateServer - dateMobile).TotalMilliseconds;
                        var r = getOTPString(oa.deviceId, offset);
                        if (r == n.password)
                        {
                            switch (ua.roles_id)
                            {
                                case 1:
                                    var x = svcUsers.Get(ua.user_id);
                                    x.membertype = "user";
                                    object px = new
                                    {
                                        data = x,
                                        product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id)
                                    };
                                    return px;

                                case 2:
                                    var y = svcUsers.Get(ua.user_id);
                                    y.membertype = "sa";
                                    object py = new
                                    {
                                        data = y,
                                        product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id)
                                    };
                                    return py;

                                case 3:
                                    var z = svcPartner.Get(ua.user_id);
                                    z.membertype = "partner";
                                    object p = new
                                    {
                                        data = z,
                                        product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id)
                                    };

                                    return p;

                                case 4:
                                    var a = svcAgent.Get(ua.user_id);
                                    a.membertype = "agent";
                                    object c = new
                                    {
                                        data = a,
                                        product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id)
                                    };

                                    return c;

                                case 5:
                                    var b = svcMerchant.Get(ua.user_id);
                                    b.membertype = "merchant";
                                    object m = new
                                    {
                                        data = b,
                                        product = svcUserServicesService.GetByUserIDToShowProduct(ua.user_id, ua.roles_id)
                                    };

                                    return m;
                                default:
                                    return "no data";

                                    /* case 6:
                                         return svcMerchant.Get(ua.user_id);
                                         break;*/
                                    //case 7:
                                    //    var c = svcCustomer.Get(ua.user_id);
                                    //    c.membertype = "customer";
                                    //    return c;
                            }
                        }
                        else { return "wrong password"; }

                    }
                    else{ return null; }
                }
                else { return null; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "OTPAccount-Log");
                return null;
            }
        }

        public class testResult {
            public string otp { get; set; }
            public string offset { get; set; }
            public string servertimenow { get; set; }
            public string mobiletime { get; set; }
            public string servercreatetime { get; set; }
            public string offsetnow { get; set; }
        }

        [Route("OTPTestResult")]
        [HttpPost]
        public object otpTestResult([FromBody] AuthMobileEx n)
        {
            try
            {
                string OTP = n.password != null ? n.password : "";
                string username = n.username != null ? n.username : "";
                UserReturnEx ret = new UserReturnEx();
                UserAccountEx ua = svcUserAccount.CheckUsername(username);
                if (ua != null)
                {
                    OTPAccountEx oa = svcOTPAccount.getByUR(ua.user_id, ua.roles_id);
                    if (oa != null)
                    {
                        DateTime dateServer = oa.dateTime ?? DateTime.UtcNow;
                        DateTime dateMobile = oa.mobileUTC ?? DateTime.UtcNow;
                        long offset = (long)(dateServer - dateMobile).TotalMilliseconds;
                        var r = getOTPString(oa.deviceId, offset);
                        testResult rets = new testResult();
                        rets.otp = r;
                        rets.offset = "" + offset;
                        rets.mobiletime = "" + dateMobile;
                        rets.servertimenow = "" + DateTime.UtcNow;
                        rets.servercreatetime = "" + dateServer;
                        rets.offsetnow = offSetnow;
                        return rets;


                    }
                    else { return "error"; }
                }
                else { return "error";}
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "OTPAccount-Log");
                return null;
            }
        }


        private string getOTPString(string deviceId, long mobileOffset)
        {
            try
            {
                DateTime utcNow = DateTime.UtcNow;// - mobileOffset
                DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                var timeNow = (long)(((utcNow - epoch).TotalMilliseconds));
                var offset = Math.Truncate(Math.Floor(Math.Round((timeNow - mobileOffset) / 1000.0)) / 60) - 480;// - Math.Truncate(Math.Floor(Math.Round(mobileOffset / 1000.0)) / 60);
                this.offSetnow = "" + offset;
                string time = offset + "";
                EncryptionHelper EH = new EncryptionHelper();
                string sha1Generated = EH.HMAC_SHA1(deviceId, time);
                string hexOutput = "";
                string hexOutputl = "";

                char[] values = sha1Generated.ToCharArray();
                var val = "";
                int count = 0;
                foreach (char letter in values)

                {

                    int value = Convert.ToUInt16(letter);
                    val = val + " " + Convert.ToUInt16(letter);
                    if (count < 3)
                    {
                        hexOutput = hexOutput + " " + String.Format("{0:X}", value);
                    }
                    if (count >= 3 && count < 6)
                    {
                        hexOutputl = hexOutputl + " " + String.Format("{0:X}", value);
                    }
                    count++;
                }
                var ds = hexOutput;
                var dds = val;
                string hexString = "" + (Int32.Parse(hexOutput.Replace(" ", "")) + Int32.Parse(hexOutputl.Replace(" ", "")));
                int count2 = 0;
                if (hexString.Length > 6)
                {
                    hexString = hexString.Substring(0, 6);
                }
                while (hexString.Length < 6)
                {
                    hexString = hexString + "" + count2;
                    count2++;
                }

                return hexString;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "OTPAccount-Log");
                return null;
            }
        }

     
 
       
      



        [Route("LoginMobile")]
        [HttpPost]
        public string FirstLoginMobile([FromBody] AuthMobileEx n)
         {
            try
            {
                if (n != null)
                {
                    if (n.password != null)
                    {
                        n.password = new EncryptionHelper().Encrypt(n.password);

                    }
                    else { return "failed"; }
                }
                else { return "failed"; }
                AuthGlobalEx r = svcUserAccount.GetAccountByUserPassword(n);
                if (r != null)
                {
                    OTPAccountEx p = new OTPAccountEx();
                    OTPAccountEx rec = svcOTPAccount.getByUR(r.id, r.role);//DUR(n.deviceId, r.id, r.role);
                    if (rec != null)
                    {
                        p.id = rec.id;
                    }


                    p.deviceId = n.deviceId;
                    p.userId = r.id;
                    p.userRoles = r.role;
                    DateTime t = DateTime.Now;
                    p.dateTime = DateTime.UtcNow;// DateTime.UtcNow;
                    p.mobileUTC = n.mobileUTC;

                    svcOTPAccount.Save(p);
                    return "success";
                }
                else { return "failed"; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "OTPAccount-Log");
                return null;
            }

        }

        


        [Route("OTPAccount/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public OTPAccountEx Save([FromBody] OTPAccountEx n)
        {
            try
            {
                // n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                /// n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                svcOTPAccount.Save(n);
                return svcOTPAccount.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "OTPAccount-Log");
                return null;
            }
        }
       

        [Route("OTPAccount/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcOTPAccount.Remove(Id);
        }
    }
}
