﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;

namespace Cooptavanza.Wallet.Controllers
{
    public class UsersApiController : ApiController
    {
        readonly UsersService svcUsers;
       
        public UsersApiController(UsersService svcUsers)
        {
            this.svcUsers = svcUsers;
        }

        [Route("Users")]
        [HttpGet]
        public UsersSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new UsersSearchViewModel() { Results = svcUsers.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcUsers.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e,"UsersAPI-Log");
                return null;
            }
        }

        [Route("Login")]
        [HttpPost]
        public UsersEx Login([FromBody] AuthEx n)
        {
            try{
            var y = svcUsers.GetLogin( n.mobile_number, n.password);
            return y;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UsersAPI-Log");
                return null;
            }
        }

        [Route("Users/{Id}")]
        [HttpGet]
        public UsersEx GetUsers(int Id)
         {
            try { 
            return svcUsers.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e,"UsersAPI-Log");
                return null;
            }
}


        [Route("Users/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public UsersEx Save([FromBody] UsersEx n)
        {
            try { 
            n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
            n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
            svcUsers.Save(n);
            return svcUsers.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "UsersAPI-Log");
                return null;
            }
        }
       

        [Route("Users/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcUsers.Remove(Id);
        }
    }
}
