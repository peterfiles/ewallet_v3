﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Collections.Generic;

namespace Cooptavanza.Wallet.Controllers
{
    public class CountryApiController : ApiController
    {
        readonly CountryService svcCountry;
        public CountryApiController(CountryService svcCountry)
        {
            this.svcCountry = svcCountry;
        }

        [Route("Country")]
        [HttpGet]
        public CountrySearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new CountrySearchViewModel() { Results = svcCountry.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcCountry.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
        }

        [Route("Country/Search")]
        [HttpGet]
        public List<CountryEx> SearchItems(string SearchText = "")
        {
            try
            {
                return svcCountry.SearchCountry(SearchText, "Id");
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
        }
        

        [Route("Dashboard/Country/Search")]
        [HttpGet]
        public List<CountryEx> AdminDashboardSearchItems(string SearchText = "")
        {
            try
            {
                return svcCountry.SearchCountry(SearchText, "Id");
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
        }

        [Route("Country/{Id}")]
        [HttpGet]
        public CountryEx GetPerson(int Id)
         {
            try
            {
                return svcCountry.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
            //return null;
        }
        
        [Route("Country/getCountryFilterByAgent")]
        [HttpGet]
        public object getCountryFilterByAgent()
        {
            try
            {
                return svcCountry.getCountryFilterByAgent();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
        }

        [Route("Country/getDefaultCountry")]
        [HttpGet]
        public List<CountryEx> GetDefault()
        {
            try
            {
                return svcCountry.GetDefaultCountry();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
        }

        [Route("Country/GetAll")]
        [HttpGet]
        public List<CountryEx> getCountr()
        {
            try
            {
                return svcCountry.GetCountries();
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Country-Log");
                return null;
            }
        }


        //[Route("Country/Save")]
        //[HttpPost]
        //// [Authorize(Roles = "administrators")]
        //public CountryEx Save([FromBody] CountryEx n)
        //{
        //    svcCountry.Save(n);
        //    return svcCountry.Get(n.id);
        //}


        //[Route("Country/{Id}")]
        //[HttpDelete]
        ////[Authorize(Roles = "administrators")]
        //public void Delete(int Id)
        //{
        //    svcCountry.Remove(Id);
        //}
    }
}
