﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using System.Web;
using System.IO;

namespace Cooptavanza.Wallet.Controllers
{
    public class ImageIdCardApiController : ApiController
    {
        readonly ImageIdCardService svcImageIdCard;
        public ImageIdCardApiController(ImageIdCardService svcImageIdCard)
        {
            this.svcImageIdCard = svcImageIdCard;
        }

        //[Route("ImageIdCard")]
        //[HttpGet]
        //public ImageIdCardSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        //{
        //       return new ImageIdCardSearchViewModel() { Results = svcImageIdCard.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcImageIdCard.Count(SearchText) };
        //}


        [Route("ImageIdCard/{Id}")]
        [HttpGet]
        public ImageIdCardEx GetPerson(int Id)
         {
            try
            {
                return svcImageIdCard.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageIdCard-Log");
                return null;
            }
            //return null;
        }


        [Route("GetImageIdByUI/{Id}")]
        [HttpGet]
        public ImageIdCardEx GetImageIdCardById(int Id)
        {
            try
            {
                return svcImageIdCard.GetByUserId(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageIdCard-Log");
                return null;
            }
            //return null;
        }

        [Route("ImageUploadCardId/save")]
        [HttpPost]
        public ImageIdCardEx UploadBillImage()
        {
            try
            {
                var form = HttpContext.Current.Request.Form;
                string userId = "0";
                string imageProfileId = "0";

                foreach (string key in form.AllKeys)
                {
                    if (key == "userId") { userId = form[key]; }
                    if (key == "imageCardId") { imageProfileId = form[key]; }
                }
                var file = HttpContext.Current.Request.Files.Count > 0 ? HttpContext.Current.Request.Files[0] : null;
                if (userId != "0")
                {

                    ImageIdCardEx n = new ImageIdCardEx();
                    n.id = Int32.Parse(imageProfileId);
                    n.userId = Int32.Parse(userId);

                    if (file != null && file.ContentLength > 0)
                    {
                        string filename = file.FileName;
                        //string folder = 
                        if (!System.IO.Directory.Exists(HttpContext.Current.Server.MapPath("../Upload/" + userId + "/")))
                        {
                            System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("../Upload/" + userId + "/").ToString());
                        }

                        string path = HttpContext.Current.Server.MapPath("../Upload/" + userId + "/");
                        string guid = "";
                        var profile = svcImageIdCard.Get(n.id);
                        if (profile != null)
                        {
                            guid = profile.imageString != null ? profile.imageString : "" + Guid.NewGuid();
                            File.Delete(path + guid + "." + filename.Split('.')[1].ToString());
                            guid = "" + Guid.NewGuid();
                        }
                        else { guid = "" + Guid.NewGuid(); }
                        string u = (path + guid + "." + filename.Split('.')[1]).ToString();

                        file.SaveAs(u);
                        n.imageString = guid;
                        n.path = "/Upload/" + userId + "/" + guid + "." + filename.Split('.')[1];
                        svcImageIdCard.Save(n);
                        return svcImageIdCard.Get(n.id);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageIdCard-Log");
                return null;
            }
        }

        [Route("ImageIdCard/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public ImageIdCardEx Save([FromBody] ImageIdCardEx n)
        {
            try
            {
                n.date_created = DateTime.Now;
                n.imageString = n.imageString.Replace("\"", "");
                n.adminID = 0;
                n.adminRoleID = 0;
                svcImageIdCard.Save(n);
                return svcImageIdCard.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageIdCard-Log");
                return null;
            }
        }

        [Route("ImageIdCard/SaveDetails")]
        [HttpGet]
        public ImageIdCardEx sd(string ed="", string icn="", string ict="", int id = 0)
        {
            try
            {
                ImageIdCardEx idceX = svcImageIdCard.Get(id);
                if (idceX.id > 0)
                {
                    idceX.idCardNo = icn;
                    idceX.idCardType = ict;
                    idceX.date_created = DateTime.Now;
                    idceX.expiryDate = DateTime.ParseExact(ed, "dd/MM/yyyy", null);
                    idceX.status = "NV";//Not-Verified;
                    idceX.adminID = 0;
                    idceX.adminRoleID = 0;
                    svcImageIdCard.Save(idceX);
                }
                return idceX;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageIdCard-Log");
                return null;
            }
        }





        [Route("ImageIdCard/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcImageIdCard.Remove(Id);
        }

        //Card Update Status
        [Route("ImageIdCard/UpdateStatus")]
        [HttpPost]
        public ImageIdCardEx updatestat( int idCard, int userRole, int userID)
        {
            try
            {
                ImageIdCardEx iceX = svcImageIdCard.Get(idCard);
                if (iceX.id > 0)
                {

                    iceX.status = "NV";//Not-Verified;
                    iceX.adminID = userRole;
                    iceX.adminRoleID = userID;
                    svcImageIdCard.Save(iceX);
                }
                return iceX;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "ImageIdCard-Log");
                return null;
            }
        }
    }
}
