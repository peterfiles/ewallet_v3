﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;
using System.Text;
using System.Configuration;

namespace Cooptavanza.Wallet.Controllers
{
    public class CustomerApicontroller : ApiController
    {
        //readonly DummyLoadService svcDummyLoad;
        readonly CustomerService svcCustomer;
        readonly UsersTempService svcUsersTemp;
        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly TransactionService svcTransaction;
        readonly TransferMoneyService svcTransferMoney;
        readonly CurrencyService svcCur;
        readonly ImageIdCardService svcImageIdCard;
        readonly ReceiversCustomerService svcReceiverCustomer;
        readonly MoneyTransferTransactionsService svcMoneyTansferTransaction;
        readonly EnrolledAccountService svcEnrolledAccount;
        readonly CurrencyService svcCurrency;
        readonly TransactionLogService svcTLog;
        readonly MoneyTransferCustomerService svcMTCS;
        readonly CountryService svcCountry;
        readonly StatesService svcState;
        readonly AgentsService svcAgents;
        readonly PartnersService svcPartners;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;
        readonly PaymentBreakdownService svcPaymentBreakdown;
        readonly CommissionsService svcCommissionService;
 
        readonly CoopTransactionLogService svcCTLS;
        readonly CoopEnrolledAccountService svcCEA;

        readonly UserAccountActivitiesService svcUserAcctActivities;

        readonly CurrencyService svcCurr;

        string coopredId = ConfigurationManager.AppSettings["CoopredId"].ToString();
        string coopredkey = ConfigurationManager.AppSettings["CoopredKey"].ToString();

        string EwalletId = ConfigurationManager.AppSettings["EwalletId"].ToString();
        string EwalletKey = ConfigurationManager.AppSettings["EwalletKey"].ToString();

        public CustomerApicontroller(
            CustomerService svcCustomer,
            UsersTempService svcUsersTemp,
            UserAccountService svcUserAccount,
            OFAClistService svcOFAC,
            //DummyLoadService svcDummyLoad, 
            TransactionService svcTransaction,
            TransferMoneyService svcTransferMoney,
            CurrencyService svcCur,
            ImageIdCardService svcImageIdCard,
            ReceiversCustomerService svcReceiverCustomer,
            MoneyTransferTransactionsService svcMoneyTansferTransaction,
            EnrolledAccountService svcEnrolledAccount,
            CurrencyService svcCurrency,
            TransactionLogService svcTLog,
            MoneyTransferCustomerService svcMTCS,
            CountryService svcCountry,
            StatesService svcState,
            AgentsService svcAgents,
            PartnersService svcPartners,
            MerchantService svcMerchant,
            UsersService svcUsers,
            CoopTransactionLogService svcCTLS,
            CoopEnrolledAccountService svcCEA,
            UserAccountActivitiesService svcUserAcctActivities,
            CurrencyService svcCurr,
            PaymentBreakdownService svcPaymentBreakdown,
            CommissionsService svcCommissionService)
        {
            //this.svcDummyLoad = svcDummyLoad;
            this.svcReceiverCustomer = svcReceiverCustomer;
            this.svcUserAccount = svcUserAccount;
            this.svcCustomer = svcCustomer;
            this.svcUsersTemp = svcUsersTemp;
            this.svcOFAC = svcOFAC;
            this.svcTransaction = svcTransaction;
            this.svcTransferMoney = svcTransferMoney;
            this.svcImageIdCard = svcImageIdCard;
            this.svcCur = svcCur;
            this.svcMoneyTansferTransaction = svcMoneyTansferTransaction;
            this.svcEnrolledAccount = svcEnrolledAccount;
            this.svcCurrency = svcCurrency;
            this.svcTLog = svcTLog;
            this.svcMTCS = svcMTCS;
            this.svcCountry = svcCountry;
            this.svcState = svcState;
            this.svcCTLS = svcCTLS;
            this.svcCEA = svcCEA;
            this.svcPartners = svcPartners;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcAgents = svcAgents;
            this.svcUserAcctActivities = svcUserAcctActivities;
            this.svcCurr = svcCurr;
            this.svcPaymentBreakdown = svcPaymentBreakdown;
            this.svcCommissionService = svcCommissionService;
        }

        [Route("Customer")]
        [HttpGet]
        public CustomerSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                return new CustomerSearchViewModel() { Results = svcCustomer.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcCustomer.Count(SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        public class FieldExchange{
            public string[] field { get; set; }
        }

        [Route("cbsRequire")]
        [HttpPost]
        public Object testReturnCBS(FieldExchange FE)
        {
            try
            {
                return svcCustomer.Get(251);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "CustomerCBSRequire-Log");
                return null;
            }
        }

        [Route("cbsRequire")]
        [HttpGet]
        public Object testReturnCBSs(string str)
        {
            try
            {
                var x = str;

                return svcCustomer.Get(251);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "CustomerCBSRequire-Log");
                return null;
            }
        }



        [Route("Customer/{Id}")]
        [HttpGet]
        public CustomerEx GetPerson(int Id)
        {
            try
            {
                return svcCustomer.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("Customer/mobilenumber/")]
        [HttpGet]
        public Boolean CheckCustomerMobile(string str = "")
        {
            try
            {
                str = "+" + str.Trim();
                CustomerEx data = svcCustomer.GetCheckMobileEmail(str);
                if (data != null)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return false;
            }
        }

        [Route("Customer/mobilenumberdata/")]
        [HttpGet]
        public CustomerEx CheckCustomerMobiledata(string str = "")
        {
            try
            {
                str = "+" + str.Trim();
                CustomerEx data = svcCustomer.GetCheckMobileEmail(str);
                if (data != null)
                {
                    return data;
                }
                else { return null; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("Customer/mobilenumberdatawithType/")]
        [HttpGet]
        public object CheckCustomerMobiledataWithType(string str = "", string ctype = "")
        {
            try
            {
                str = "+" + str.Trim();
                object data = new object();
                if (ctype.Equals("sender"))
                {
                    data = svcCustomer.GetCheckMobileEmail(str);
                }
                else
                {
                    data = svcReceiverCustomer.GetCheckMobileEmail(str);
                }
                if (data != null)
                {
                    return data;
                }
                else { return null; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }



        [Route("Customer/email/")]
        [HttpGet]
        public Boolean CheckEmail(string str)
        {
            try
            {
                //str = "+" + str.Trim();
                CustomerEx data = svcCustomer.GetCheckEmail(str);
                if (data != null)
                {
                    return true;
                }
                else { return false; }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return false;
            }
        }

        [Route("Customer/mobileNo/")]
        [HttpPost]
        public ResponseEx GetCustomerByMobile(RequestCommonEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                EncryptionHelper EH = new EncryptionHelper();
                string key = n.idCode == coopredId ? coopredkey : n.idCode == EwalletId ? EwalletKey : "";
                string message = MAH.requestCommon(n);
                string me = EH.HMAC_SHA256(message, key);
                if ((n.signature).ToUpper() != EH.HMAC_SHA256(message, key))
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "INVALID SIGNATURE";
                    return RE;
                }

                CustomerEx data = svcCustomer.GetCheckMobileEmail(n.param1);
                if (data != null)
                {
                    RE.code = "RTS";
                    RE.data = data;
                    RE.date = DateTime.Now;
                    RE.message = "SUCCESS";
                    RE.id = "0";

                    return RE;
                }
                else
                {
                    RE.code = "RTF";
                    RE.data = null;
                    RE.date = DateTime.Now;
                    RE.message = "NO DATA FOUND";
                    RE.id = "0";
                    return RE;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("Customer/email/")]
        [HttpPost]
        public ResponseEx GetCustomerByEmail(RequestCommonEx n)
        {
            try
            {
                ResponseEx RE = new ResponseEx();
                MessageAppenderHelper MAH = new MessageAppenderHelper();
                EncryptionHelper EH = new EncryptionHelper();
                string key = n.idCode == coopredId ? coopredkey : n.idCode == EwalletId ? EwalletKey : "";
                string message = MAH.requestCommon(n);

                if ((n.signature).ToUpper() != EH.HMAC_SHA256(message, key))
                {
                    RE.code = "RTF";
                    RE.date = Convert.ToDateTime(string.Format("{0:F}", DateTime.Now));
                    RE.data = new
                    {
                        signature = n.signature
                    };
                    RE.message = "INVALID SIGNATURE";
                    return RE;
                }

                CustomerEx data = svcCustomer.GetCheckEmail(n.param1);
                if (data != null)
                {
                    RE.code = "RTS";
                    RE.data = data;
                    RE.date = DateTime.Now;
                    RE.message = "SUCCESS";
                    RE.id = "0";

                    return RE;
                }
                else
                {
                    RE.code = "RTF";
                    RE.data = null;
                    RE.date = DateTime.Now;
                    RE.message = "NO DATA FOUND";
                    RE.id = "0";
                    return RE;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }


        [Route("rec")]
        [HttpPost]
        public object GetCustomerByEmail([FromBody] EmailEx n)
        {
            try
            {
                CustomerEx data = svcCustomer.GetCheckEmail(n.email);
                if (data != null)
                {
                    n.id = data.id;
                    n.tID = data.tid;
                    return n;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("Customer/login")]
        [HttpPost]
        public CustomerEx GetPerson([FromBody] CustomerEx n)
        {
            try {
                string mobile = n.mobilenumber != null ? "+" + n.mobilenumber.Replace("+", "").Trim() : "";
                string password = "";
                if (n.password != null)
                {
                    password = new EncryptionHelper().Encrypt(n.password);
                }
                var data = svcCustomer.GetLogin(mobile, password);
                if (data != null)
                {
                    return data;

                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }
        [Route("CustomerMobile/login")]
        [HttpPost]
        public CustomerEx GetCustomer([FromBody] CustomerEx n)
        {
            try
            {
                string mobile = n.mobilenumber != null ? "+" + n.mobilenumber.Replace("+", "").Trim() : "";
                string password = "";
                if (n.password != null)
                {
                    password = new EncryptionHelper().Encrypt(n.password);
                }
                var data = svcCustomer.GetLogin(mobile, password);
                if (data != null)
                {
                    return data;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("Customer/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public object Save([FromBody] CustomerEx n)
        {
            try
            {
                object retData = null;
                string password = string.Empty;
                if (n.password != "")
                {
                    password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                }
                string errmessage = string.Empty;
                var tempid = n.id;
                n.username = n.mobilenumber;
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.email = n.email == null ? svcCustomer.CountCustomer() + "customer@gmail.com" : n.email;
                n.gender = Convert.ToBoolean(n.gender);
                if (n.id != 0)
                {
                    CustomerEx ce = svcCustomer.GetCheckEmail(n.email);
                    if (ce != null)
                    {
                        if (ce.id == n.id)
                        {
                            //errmessage = "";
                            svcCustomer.Save(n);
                        }
                        else
                        {
                            errmessage = "Existing Email found!";
                            return retData = new
                            {
                                ErrorMessage = errmessage,
                                cData = n
                            };
                        }

                    }
                    else
                    {
                        svcCustomer.Save(n);
                    }
                }
                else
                {
                    CustomerEx ce = svcCustomer.GetCheckEmail(n.email);
                    if (ce == null)
                    {
                        svcCustomer.Save(n);
                    }
                    else
                    {
                        return retData = new
                        {
                            ErrorMessage = errmessage,
                            cData = n
                        };
                    }

                }

                if (checkIfClientIsInOFAC(n))
                {
                    Generator g = new Generator();
                    g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
                }
                CustomerEx data = svcCustomer.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                UserAccountActivitiesEx UAA = new UserAccountActivitiesEx();
                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary

                    // User account activity log on new customer made
                    UAA.status = false;
                    UAA.token_id = "" + Guid.NewGuid();
                    UAA.userId = n.id;
                    UAA.userRole = 7;
                    UAA.dateCreated = DateTime.Now;

                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                        svcUserAcctActivities.Save(UAA);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password != "" ? password : x.password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }
                retData = new
                {
                    ErrorMessage = errmessage,
                    cData = data
                };
                return retData;
            } catch (Exception ex)
            {
                object retData = new
                {
                    ErrorMessage = ex,
                    cData = "Error"
                };
                new Generator().LogException(ex, "Customer-Log");
                return retData;
            }

        }

        [Route("Customer/passwordCheck")]
        [HttpGet]
        public object passwordCheck(string mobileNumber = "", string password = "")
        {
            try
            {
                string mNo = "+" + mobileNumber.Trim();
                string p = password != null ? new EncryptionHelper().Encrypt(password) : "";
                CustomerEx cEx = svcCustomer.GetLogin(mNo, p);
                if (cEx.id > 0)
                {
                    return "valid";
                }
                else
                {
                    return "error";
                }
            }
            catch (Exception ex)
            {
                new Generator().LogException(ex, "Custoemr-Log");
                return ex;
            }

        }

        [Route("Customer/Customer/Save")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public CustomerEx Saveadmin([FromBody] CustomerEx n)
        {
            try
            {
                var tempid = n.id;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                n.username = n.mobilenumber;
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.gender = Convert.ToBoolean(n.gender);
                n.email = n.email == null ? svcCustomer.CountCustomer() + "customer@gmail.com" : n.email;
                svcCustomer.Save(n);
                if (checkIfClientIsInOFAC(n))
                {
                    Generator g = new Generator();
                    g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
                }
                CustomerEx data = svcCustomer.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                UserAccountActivitiesEx UAA = new UserAccountActivitiesEx();
                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary


                    // User account activity log on new customer made
                    UAA.status = false;
                    UAA.token_id = "" + Guid.NewGuid();
                    UAA.userId = n.id;
                    UAA.userRole = 7;
                    UAA.dateCreated = DateTime.Now;

                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                        svcUserAcctActivities.Save(UAA);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password != "" ? password : x.password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }

                return data;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }

        }

        [Route("Customer/adminsave")]
        [HttpPost]
        public CustomerEx AdminSave([FromBody] CustomerEx n)
        {
            try
            {
                var tempid = n.id;
                //return null;
                //DateTime.ParseExact(n.sbirthdate, "dd/MM/yyyy", null);
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "";
                n.username = n.mobilenumber;
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);
                n.gender = Convert.ToBoolean(n.gender);
                n.email = n.email == null ? svcCustomer.CountCustomer() + "customer@gmail.com" : n.email;
                n.partnerID = n.coopAccountListData.coopRootAccountUserIDAndRole.loggedID;
                bool debitToGiver = false;
                if (n.coopAccountListData.coopRootAccountUserIDAndRole != null)
                {
                    if (n.coopAccountListData.coopRootAccountUserIDAndRole.roleID == 3 && n.coopAccountListData.coopAccountListAndcurencyData != null)
                    {
                        debitToGiver = true;
                    }
                }

                CustomerEx a = svcCustomer.Get(n.id);
                if (a != null)
                {
                    if (a.id > 0)
                    {
                        n.address = n.address != null ? n.address : a.address;
                        n.city = n.city != null ? n.city : a.city;
                        n.cityid = n.cityid != null ? n.cityid : a.cityid;
                        n.country = n.country != null ? n.country : a.country;
                        n.countryid = n.countryid > 0 ? n.countryid : a.countryid;
                        n.state = n.state != null ? n.state : a.state;
                        n.stateid = n.stateid > 0 ? n.stateid : a.stateid;

                        List<AccountListEx> ceaEx = svcCEA.getCoopAccountList(a.id, 7);
                        n.currency = ceaEx.Count == 0 || ceaEx[0].currencyAccountTo == null ? "D0F" : ceaEx[0].currencyAccountTo;
                        n.currencyid = svcCurr.GetByISO(n.currency).id;
                        //a.currency = n.currency != null ? n.currency : a.currency;
                        //a.currencyid = n.currencyid > 0 ? n.currencyid : a.currencyid;
                        n.firstname = n.firstname != null ? n.firstname : a.firstname;
                        n.lastname = n.lastname != null ? n.lastname : a.lastname;

                        n.mobilenumber = n.mobilenumber != null ? n.mobilenumber : a.mobilenumber;
                        n.phonenumber = n.phonenumber != null ? n.phonenumber : a.phonenumber;

                        n.zipcode = n.zipcode != null ? n.zipcode : a.zipcode;
                    }
                }


                svcCustomer.Save(n);

                if (checkIfClientIsInOFAC(n))
                {
                    Generator g = new Generator();
                    g.sendSMSOFACNotif(n.mobilenumber, "Thank you for enrolling in our system. But we need to further verify your account. Thank you!");
                }
                CustomerEx data = svcCustomer.Get(n.id);
                UserAccountEx ua = new UserAccountEx();

                List<coopAccountListAndCurrencyData> coopALACD = n.coopAccountListData.coopAccountListAndcurencyData;
                coopRootAccountUserIDAndRole coopRAUAR = n.coopAccountListData.coopRootAccountUserIDAndRole;
                UserAccountActivitiesEx UAA = new UserAccountActivitiesEx();
                if (coopALACD != null)
                {
                    foreach (coopAccountListAndCurrencyData resData in coopALACD)
                    {
                        EnrolledAccountEx eaEx = new EnrolledAccountEx();
                        CoopEnrolledAccountEx ceaEx = new CoopEnrolledAccountEx();
                        ceaEx = svcCEA.getIdByUserIDRoleIDCurrency(n.id, 7, resData.currency.iso);
                        if (ceaEx.id == 0)
                        {
                            #region Coop enrolled Account EX;
                            ceaEx.id = 0;
                            ceaEx.accountDefault = true;
                            ceaEx.accountnumber = "";
                            ceaEx.currency = resData.currency.iso;
                            ceaEx.currenycid = resData.currency.id;
                            ceaEx.roleID = Convert.ToString(7);
                            ceaEx.userId = n.id;
                            ceaEx.userTID = n.tid;
                            #endregion Coop enrolled Account EX;

                            svcCEA.Save(ceaEx);
                            ceaEx.accountnumber = svcCEA.getAccountNumberForCoopRed(n.id, ceaEx.currenycid, 7, ceaEx.id);
                            svcCEA.Save(ceaEx);

                            eaEx = svcEnrolledAccount.GetByUserIDUserTIDAndCurrencyISO(n.id, n.tid, ceaEx.currency);
                            eaEx.accountnumber = ceaEx.accountnumber;
                            svcEnrolledAccount.Save(eaEx);

                        }
                        if (resData.status.Equals("added"))
                        {
                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();

                            eaEx = svcEnrolledAccount.GetByUserIDUserTIDAndCurrencyISO(n.id, n.tid, ceaEx.currency);
                            eaEx.accountnumber = ceaEx.accountnumber;
                            svcEnrolledAccount.Save(eaEx);

                            ctlEx.amount = resData.amount;
                            ctlEx.currencyAccountFrom = resData.currency.iso;
                            ctlEx.currencyAccountTo = resData.currency.iso;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = true;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 7;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + resData.amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "CREDIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);

                            TransactionEx tEx = new TransactionEx();
                            tEx.accountNumber = ceaEx.accountnumber;
                            tEx.balance = Convert.ToDecimal(ctlEx.amount);
                            tEx.credit = true;
                            tEx.currencyfromiso = resData.currency.iso;
                            tEx.currencytoiso = resData.currency.iso;
                            tEx.dataCreated = DateTime.Now;
                            tEx.debit = false;
                            tEx.enrolledAccountsCurrencyID = eaEx.currenycid;
                            tEx.enrolledAccountsID = eaEx.id;
                            tEx.partnerId = coopRAUAR.loggedID;
                            tEx.receiversID = n.id;
                            tEx.status = "CLEARED";
                            string compName = string.Empty;
                            switch (coopRAUAR.roleID)
                            {
                                case 1:
                                    var x = svcUsers.Get(coopRAUAR.loggedID);
                                    compName = x.firstname;
                                    break;

                                case 2:
                                    var y = svcUsers.Get(coopRAUAR.loggedID);
                                    compName = y.firstname;
                                    break;

                                case 3:
                                    var z = svcPartners.Get(coopRAUAR.loggedID);
                                    compName = z.finame;
                                    break;

                                case 4:
                                    var ag = svcAgents.Get(coopRAUAR.loggedID);
                                    compName = ag.name;
                                    break;

                                case 5:
                                    var b = svcMerchant.Get(coopRAUAR.loggedID);
                                    compName = b.name;
                                    break;

                            }
                            tEx.description = "Credited By : " + compName;
                            tEx.transactionTID = ctlEx.transactionTID;
                            tEx.transactionType = "30005";
                            tEx.userId = n.id;
                            tEx.userTID = n.tid;

                            svcTransaction.Save(tEx);

                        }

                    }
                }

                if (debitToGiver)
                {
                    for (int q = 0; q < n.coopAccountListgiver.Length; q++)
                    {
                        object s = n.coopAccountListData.coopAccountListAndcurencyData.Find(x => x.currency.iso.Equals(n.coopAccountListgiver[q].currencyAccountTo));
                        if (s != null)
                        {
                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = n.coopAccountListgiver[q].amount;
                            ctlEx.currencyAccountFrom = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.currencyAccountTo = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = false;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 7;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + n.coopAccountListgiver[q].amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "DEBIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);
                        }
                    }
                }


                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary

                    // User account activity log on new customer made
                    UAA.status = false;
                    UAA.token_id = "" + Guid.NewGuid();
                    UAA.userId = n.id;
                    UAA.userRole = 7;
                    UAA.dateCreated = DateTime.Now;

                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                        svcUserAcctActivities.Save(UAA);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password != "" ? password : x.password;
                    ua.user_id = n.id;
                    ua.roles_id = 7;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }

                return data;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }

        }


        [Route("Customer/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcCustomer.Remove(Id);
        }

        [Route("Customer/GetCustomerByMobileNumber/{mobileNumber}")]
        [HttpGet]
        public CustomerEx GetByMobileNumber(string mobileNumber)
        {
            try
            {
                return svcCustomer.GetByMobileNumber(mobileNumber);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }


        [Route("Customer/decrypt")]
        [HttpGet]
        public string Decrypts(string str)
        {
            try
            {
                return new EncryptionHelper().Decrypt(str);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }

        }

        [Route("Customer/encrypt")]
        [HttpGet]
        // [Authorize(Roles = "administrators")]
        public string Encrypt(string str)
        {
            try
            {
                //  svcImageIdCard.Save(n);
                return new EncryptionHelper().Encrypt(str);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }

        }


        [Route("TF")] //transfer funds
        [HttpGet]
        public TransferMoneyEx GetTransferMoney(string tk = "", int id = 0, int p = 0)
        {
            try
            {
                if (p > 0)
                {
                    return svcTransferMoney.GetTransferMoneyByTk(tk, id, p);
                }

                return svcTransferMoney.GetTransferMoneyByTk(tk, id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }
        [Route("TFA")]
        [HttpGet]
        public string GetTransferMoneyWithOtp(string o = "", string tk = "", int id = 0, int ui = 0)
        {
            try
            {
                var i = svcTransferMoney.GetTransferMoneyWithOtp(o, tk, id);
                if (i != null)
                {
                    CustomerEx r = svcCustomer.Get(i.customerid);
                    CustomerEx c = svcCustomer.Get(ui);
                    TransactionEx x = svcTransaction.Get(id);

                    x.status = "CLEARED";
                    svcTransaction.Save(x);
                    TransactionEx t = new TransactionEx();
                    t.dataCreated = DateTime.Now;
                    t.credit = true;
                    t.debit = false;
                    t.balance = i.amountto;
                    t.currencytoiso = i.currencytoiso;
                    t.currencyfromiso = i.currencyfromiso;
                    t.userId = c.id;
                    t.userTID = c.tid;
                    t.status = "CLEARED";
                    t.transactionTID = "" + Guid.NewGuid();
                    t.transactionType = "11012";
                    t.description = "Email money tranfer received from " + (c.firstname).ToUpper() + " " + (c.lastname).ToUpper();
                    i.status = "CLEARED";
                    svcTransaction.Save(t);
                    i.transactionId = t.id;
                    svcTransferMoney.Save(i);

                    Boolean content = false;
                    foreach (var n in c.EnrolledList)
                    {
                        if (n.currency == i.currencytoiso)
                        {
                            content = true;
                        }
                    }
                    if (content == false)//create new account for user
                    {
                        var e = new EnrolledAccountEx();
                        var cu = svcCurrency.GetByISO(i.currencytoiso);
                        e.currenycid = cu.id;
                        e.currency = i.currencytoiso;
                        e.accountDefault = false;
                        e.userId = c.id;
                        e.userTID = c.tid;
                        svcEnrolledAccount.Save(e);
                    }
                    return "success";
                }

                return "failed";
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return "failed";
            }
        }


        //[Route("LoadinDummy")]
        //[HttpPost]
        //public bool Loadin([FromBody] DummyLoadEx n)
        //{
        //    return svcDummyLoad.getByCredentials(n.accountnumber, n.userpin, n.userid);
        //}


        [Route("Register/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public UsersTempEx Register([FromBody] UsersTempEx n)
        {
            //try { 
            try
            {
                n.OTP = new Generator().OTP();

                Guid tok = Guid.NewGuid();
                n.token_id = "" + tok;
                svcUsersTemp.Save(n);
                var ret = svcUsersTemp.Get(n.id);
                SendSms(ret.mobile_number, ret.OTP);
                return ret;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
            //}
            //catch (Exception ex)
            //{
            //    svcError.SaveErrorLog(ex, "Register/");
            //    return null;
            //}
        }


        [Route("forgetPassword/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public string ForgetPassword([FromBody] EmailEx n)
        {
            try
            {
                var c = svcCustomer.GetCheckEmail(n.email);
                if (c != null)
                {
                    var ua = svcUserAccount.GetAccountByUserId(7, c.id, c.tid);
                    if (ua != null)
                    {
                        EmailEx r = new EmailEx();

                        r.email = n.email;
                        r.tID = ua.tID;
                        r.id = c.id;

                        return new Generator().sendResetEmail(r);
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("reset/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public string resetPassword([FromBody] UserAccountEx n)
        {
            try
            {
                var x = svcUserAccount.GetAccountByUserId(7, n.id, n.tID);
                if (x != null)
                {
                    x.password = new EncryptionHelper().Encrypt(n.password);
                    svcUserAccount.Save(x);
                    return "success";
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }


        [Route("mt/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")] //money transfer not etransfer
        public string sendmtransfer([FromBody] TransferMoneyWREx p)
        {
            try
            {
                CustomerEx c = new CustomerEx();
                TransactionEx t = new TransactionEx();
                TransferMoneyEx n = new TransferMoneyEx();
                ReceiversCustomerEx m = new ReceiversCustomerEx();
                MoneyTransferTransactionsEx mte = new MoneyTransferTransactionsEx();
                string res = string.Empty;
                n = p.tm;
                m = p.rc;

                Boolean verified = svcTransaction.verify(n.customerid, n.amountfrom, n.currencyfromiso);
                if (!verified)
                {
                    return "error";
                }

                c = svcCustomer.Get(n.customerid);
                n.customerid = c.id;
                n.customertid = c.tid;
                t.dataCreated = n.datecreated;
                t.credit = false;
                t.debit = true;
                t.balance = n.amountfrom;

                t.userId = c.id;
                t.userTID = c.tid;
                t.status = "PENDING";
                t.transactionTID = "" + Guid.NewGuid();
                n.transactionTID = t.transactionTID;
                n.transactionid = 3;
                m.accountnumber = new Generator().getAccount(m.countryid, m.stateid);

                t.transactionType = "10002";
                string trackingnumber = new Generator().getTransactionNo(m.countryid, m.stateid, svcTransaction.GetTransactionLastIDNumber());

                t.description = "Money tranfer to " + n.benificiaryfullname + " Email: " + n.benificiaryemail + " Mobile Number: " + n.benificiarymobileno + " Tracking No.: " + trackingnumber;
                n.transactionType = "10002";
                m.agentID = 0;
                //if (m.agentID != 0)
                //{
                m.tid = "" + Guid.NewGuid();
                svcReceiverCustomer.Save(m);

                t.receiversID = m.id;
                t.accountNumber = m.accountnumber;

                n.accountNumber = t.accountNumber;
                EnrolledAccountEx eaEX = svcEnrolledAccount.GetByUserIDAndCurrencyID(c.id, n.currencyfromiso);
                t.currencyfromiso = n.currencyfromiso;
                t.currencytoiso = n.currencytoiso;
                t.enrolledAccountsID = eaEX.id;
                t.enrolledAccountsCurrencyID = eaEX.currenycid;
                t.accountNumber = eaEX.accountnumber;
                svcTransaction.Save(t);
                n.transactionId = t.id;
                svcTransferMoney.Save(n);

                mte.accountNumber = m.accountnumber;
                mte.agentID = m.agentID;
                mte.receiversID = m.id;
                mte.status = t.status;
                mte.transactionID = t.id;
                mte.userID = c.id;
                mte.trackingnumber = trackingnumber;
                mte.walkin = false;
                svcMoneyTansferTransaction.Save(mte);
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("tID:{0}", t.id).AppendLine();
                sb.AppendFormat("transactionID:{0}", t.transactionTID).AppendLine();
                sb.AppendFormat("userID:{0}", t.userId).AppendLine();
                svcTLog.SaveLog(sb.ToString(), "MONEY_TRANSFER");

                //return new Generator().sendSMSMoney(t, n, c.mobilenumber);
                try
                {

                    decimal receiversAmount = n.amountto;
                    string sendersFullName = c.lastname;
                    string receiversFullName = n.receiverLastname != null ? n.receiverLastname : n.benificiaryfullname;

                    new Generator().sendSMSMoneyFromWalkIn("sender", c.mobilenumber, n.benificiarymobileno, n.answer, receiversAmount, sendersFullName, receiversFullName, trackingnumber);
                    new Generator().sendSMSMoneyFromWalkIn("receiver", c.mobilenumber, n.benificiarymobileno, n.answer, receiversAmount, sendersFullName, receiversFullName, trackingnumber);
                    res = "success";
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    res = "error";
                }
                //}else{ return "Please Select Agent."; }
                //  return new Generator().sendSMSMoney(t, n, c.mobilenumber);
                return res;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return "error";
            }
        }

        [Route("admin/mt/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")] //money transfer not etransfer
        public string adminsendmtransfer([FromBody] TransferMoneyWREx p)
        {
            string errMessage = string.Empty;
            try
            {
                int senderCurrID = svcCurrency.GetByISO(p.ce.currency).id;
                int receiverCurrID = svcCurrency.GetByISO(p.rc.currency).id;
                int agentID = p.agentID;
                decimal sendersAmount = p.sendersAmount;
                decimal receiversAmount = p.receiversAmount;
                int scountryID = svcCountry.GetByCountryName(p.sCountryName).id;
                int sstateID = svcState.GetByStateName(p.sStateName).id;
                int rcountryID = svcCountry.GetByCountryName(p.rCountryName).id;
                int rstateID = svcState.GetByStateName(p.rStateName).id;
                p.memberType = p.memberType.Replace("\"","");
                int roleID = 0;
                int cntryID = 0;
                int steID = 0;
                string businessName = string.Empty;
                string userTid = string.Empty;
                switch (p.memberType) {
                    case "user":
                        UsersEx usrEx = svcUsers.Get(agentID);
                        cntryID = Convert.ToInt32(usrEx.countryid);
                        steID = Convert.ToInt32(usrEx.stateid);
                        businessName = usrEx.username;
                        userTid = usrEx.tid;
                        //stte    
                        roleID = 6;
                        break;
                    case "sa":
                        UsersEx uEx2 = svcUsers.Get(agentID);
                        cntryID = Convert.ToInt32(uEx2.countryid);
                        steID = Convert.ToInt32(uEx2.stateid);
                        businessName = uEx2.username;
                        userTid = uEx2.tid;
                        roleID = 2;
                        break;
                    case "partner":
                        PartnersEx partEx = svcPartners.Get(agentID);
                        cntryID = Convert.ToInt32(partEx.countryid);
                        steID = Convert.ToInt32(partEx.stateid);
                        businessName = partEx.finame;
                        userTid = partEx.tid;
                        roleID = 3;
                        break;
                    case "agent":
                        AgentsEx ageEx = svcAgents.Get(agentID);
                        cntryID = Convert.ToInt32(ageEx.countryid);
                        steID = Convert.ToInt32(ageEx.stateid);
                        businessName = ageEx.name;
                        userTid = ageEx.tid;
                        roleID = 4;
                        break;
                    case "merchant":
                        MerchantEx merEx = svcMerchant.Get(agentID);
                        cntryID = Convert.ToInt32(merEx.countryid);
                        steID = Convert.ToInt32(merEx.stateid);
                        businessName = merEx.name;
                        userTid = merEx.tid;
                        roleID = 5;
                        break;
                }
                CustomerEx c = p.ce;
                ReceiversCustomerEx rc = p.rc;
                MoneyTransferCustomerEx smtce = new MoneyTransferCustomerEx();
                MoneyTransferCustomerEx rmtce = new MoneyTransferCustomerEx();

                TransactionEx t = new TransactionEx();
                TransferMoneyEx n = new TransferMoneyEx();
                MoneyTransferTransactionsEx mte = new MoneyTransferTransactionsEx();

                smtce = svcMTCS.GetByMobileNumber(c.mobilenumber) == null ? new MoneyTransferCustomerEx() : svcMTCS.GetByMobileNumber(c.mobilenumber);
                smtce.accountnumber = c.id != 0 ? c.accountnumber : new Generator().getAccount(c.countryid, c.stateid);
                smtce.firstname = c.firstname;
                smtce.middlename = c.middlename;
                smtce.lastname = c.lastname;
                smtce.currency = p.ce.currency;
                smtce.birthdate = c.birthdate == DateTime.MinValue ? DateTime.Now : c.birthdate;
                smtce.email = c.email;
                smtce.mobilenumber = c.mobilenumber;
                smtce.phonenumber = c.phonenumber;
                smtce.address = c.address;
                smtce.username = c.mobilenumber;
                smtce.zipcode = c.zipcode;
                smtce.currencyid = senderCurrID;
                smtce.stateid = sstateID;
                smtce.countryid = scountryID;
                smtce.city = c.city;
                smtce.tid = c.id != 0 ? c.tid : "" + Guid.NewGuid();
                smtce.isInOFACList = false;
                smtce.isVerified = true;
                smtce.partnerID = agentID;
                smtce.roleid = roleID;
                svcMTCS.SaveFromWalkIN(smtce);

                //receiver
                rmtce = svcMTCS.GetByMobileNumber(rc.mobilenumber) == null ? new MoneyTransferCustomerEx() : svcMTCS.GetByMobileNumber(rc.mobilenumber);
                rmtce.accountnumber = rc.id != 0 ? rc.accountnumber : new Generator().getAccount(rc.countryid, rc.stateid);
                rmtce.firstname = rc.firstname;
                rmtce.middlename = rc.middlename;
                rmtce.lastname = rc.lastname;
                rmtce.currency = p.rc.currency;
                rmtce.birthdate = DateTime.Now;
                rmtce.email = rc.email;
                rmtce.mobilenumber = rc.mobilenumber;
                rmtce.phonenumber = rc.phonenumber;
                rmtce.address = rc.address;
                rmtce.username = rc.mobilenumber;
                rmtce.zipcode = rc.zipcode;
                rmtce.currencyid = receiverCurrID;
                rmtce.stateid = rstateID;
                rmtce.countryid = rcountryID;
                rmtce.city = rc.city;
                rmtce.tid = rc.id != 0 ? rc.tid : "" + Guid.NewGuid();
                rmtce.isInOFACList = false;
                rmtce.isVerified = true;
                rmtce.partnerID = agentID;
                rmtce.roleid = roleID;
                svcMTCS.SaveFromWalkIN(rmtce);

                saveSenderDetailsAndTransaction(
                    c, 
                    rc, 
                    smtce, 
                    rmtce, 
                    t, 
                    n, 
                    mte, 
                    senderCurrID, 
                    agentID, 
                    roleID, 
                    cntryID, 
                    steID, 
                    businessName,
                    userTid,
                    sendersAmount, 
                    receiversAmount, 
                    p.serviceChargesAndCommissions);
                errMessage = "Success";
            } catch (Exception ex)
            {
                errMessage = ex.Message.ToString();
                new Generator().LogException(ex, "Customer-Log");
            }

            return errMessage;
        }


        public void saveSenderDetailsAndTransaction(
            CustomerEx c,
            ReceiversCustomerEx rc,
            MoneyTransferCustomerEx smtce,
            MoneyTransferCustomerEx rmtce,
            TransactionEx t,
            TransferMoneyEx tme,
            MoneyTransferTransactionsEx mtte,
            int senderCurrID,
            int agentID,
            int roleID,
            int cntryID,
            int steID,
            string businessname,
            string userTID,
            decimal sendersAmount,
            decimal receiversAmount,
            serviceChargesAndCommissionsEx scac
            )
        {
            try
            {
                #region SaveTransaction
                t.dataCreated = DateTime.Now;
                t.credit = false;
                t.debit = true;
                t.balance = scac.fees.senderAmount;

                t.userId = smtce.id;
                t.userTID = smtce.tid;
                t.status = "PENDING";
                //t.transactionTID = "" + Guid.NewGuid();
                t.transactionType = "10002";
                t.transactionTID = new Generator().generateTransactionNo(t.transactionType, svcTransaction.GetTransactionLastIDNumber(), scac.fees.currency);

                t.partnerId = agentID;


                string trackingnumber = new Generator().getTransactionNo(smtce.countryid, smtce.stateid, svcTransaction.GetTransactionLastIDNumber());
                string recieverFullName = rc.firstname + " " + rc.middlename + " " + rc.lastname;
                t.description = "Walk in Money tranfer to " + recieverFullName + " Email: " + rc.email + " Mobile Number: " + rc.mobilenumber + " Tracking No.: " + trackingnumber;
                //n.transactiontype = 3;
                //if (m.agentID != 0)
                //{

                t.receiversID = rmtce.id;
                t.accountNumber = rmtce.accountnumber;

                //n.accountNumber = t.accountNumber;
                //EnrolledAccountEx eaEX = svcEnrolledAccount.GetByUserIDAndCurrencyID(c.id, n.currencyfromiso);
                t.currencyfromiso = c.currency;
                t.currencytoiso = rc.currency;
                svcTransaction.Save(t);
                #endregion SaveTransaction

                //Save Record For CoopTransaction Credit
                //Debit only upon Claim
                #region SaveCoopTransactionLog 
                CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                ctlEx.amount = t.balance;
                ctlEx.currencyAccountFrom = t.currencyfromiso;
                ctlEx.currencyAccountTo = t.currencytoiso;
                ctlEx.date_created = t.dataCreated;
                ctlEx.giverTID = userTID;
                ctlEx.giverUserId = agentID;
                ctlEx.giverUserRoles = roleID;
                ctlEx.isCredit = true;
                ctlEx.receiverTID = "" + Guid.Empty;
                ctlEx.receiverUserId = 0;
                ctlEx.receiverUserRoles = 7;
                ctlEx.transactionDescription = t.description;
                ctlEx.transactionTID = t.transactionTID;
                ctlEx.transactionType = t.transactionType;
                svcCTLS.Save(ctlEx);
                #endregion SaveCoopTransactionLog

                #region SaveCommission
                new GlobalCallForServicesController(
                    svcCur,
                    svcPaymentBreakdown,
                    svcCommissionService,
                    svcCTLS,
                    svcCountry).SaveCommission(t, scac, userTID, businessname, agentID, roleID, cntryID, steID);
                #endregion SaveCommission

                #region SaveTransferMoney
                tme.transactionType = t.transactionType;
                tme.datecreated = t.dataCreated;
                tme.customerid = smtce.id;
                tme.customertid = smtce.tid;
                tme.transactionTID = t.transactionTID;
                tme.amountto = receiversAmount;
                tme.amountfrom = sendersAmount;
                tme.benificiaryfullname = rmtce.firstname + " " + rmtce.middlename + " " + rmtce.lastname;
                tme.benificiarymobileno = rmtce.mobilenumber;
                tme.benificiaryemail = rmtce.email;
                tme.question = "Password";
                tme.answer = c.password;
                tme.currencyfromiso = c.currency;
                tme.currencytoiso = rc.currency;
                tme.accountNumber = t.accountNumber;
                tme.transactionId = t.id;
                svcTransferMoney.Save(tme);
                #endregion TransferMoney

                #region SaveMoneyTransferTransaction
                mtte.userID = smtce.id;
                mtte.receiversID = rmtce.id;
                mtte.agentID = agentID;
                mtte.transactionID = t.id;
                mtte.accountNumber = t.accountNumber;
                mtte.status = t.status;
                mtte.trackingnumber = trackingnumber;
                mtte.walkin = true;
                svcMoneyTansferTransaction.Save(mtte);
                #endregion SaveMoneyTransferTransaction

                #region SendNotification
                string sendersFullName = smtce.lastname;
                string receiversFullName = rmtce.lastname;
                new Generator().sendSMSMoneyFromWalkIn("sender", smtce.mobilenumber, rmtce.mobilenumber, c.password, receiversAmount, sendersFullName, receiversFullName, mtte.trackingnumber);
                new Generator().sendSMSMoneyFromWalkIn("receiver", smtce.mobilenumber, rmtce.mobilenumber, c.password, receiversAmount, sendersFullName, receiversFullName, mtte.trackingnumber);
                #endregion SendNotification
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
            }
        }

        [Route("test/pb")]
        [HttpGet]
        public object test()
        {
            try
            {
                PaymentBreakdownEx pBex = new PaymentBreakdownEx(); //Save Merchant Fee
                pBex.amount = 20;
                pBex.currencyID = 147;
                pBex.currencyISO = "USD";
                pBex.isMerchant = true;
                pBex.mdr = "0.005";
                pBex.transactionTID = "" + Guid.NewGuid();
                pBex.txFee = "0.006";
                pBex.txFeeCurrency = "USD";
                pBex.userID = 147;
                pBex.userRole = 3;
                pBex.remarks = "Merchant Commission Fee";
                svcPaymentBreakdown.Save(pBex);

                return pBex;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("sendmoney/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")] // E Transfer module
        public string sendmoney([FromBody] TransferMoneyEx n)
        {
            try
            {
                CustomerEx c = new CustomerEx();
                TransactionEx t = new TransactionEx();
                CustomerEx rcvC = new CustomerEx();
                if (n == null) { return "failed"; }
                Boolean verified = svcTransaction.verify(n.customerid, n.amountfrom, n.currencyfromiso);
                if (!verified)
                {
                    return "failed";
                }

                c = svcCustomer.Get(n.customerid);
                n.customerid = c.id;
                n.customertid = c.tid;
                t.dataCreated = n.datecreated;
                t.credit = false;
                t.debit = true;
                t.balance = n.amountfrom;
                t.currencyfromiso = n.currencyfromiso;
                t.currencytoiso = n.currencytoiso;

                t.userId = c.id;
                t.userTID = c.tid;
                t.status = "PENDING";
                t.transactionTID = "" + Guid.NewGuid();
                n.transactionTID = t.transactionTID;
                EnrolledAccountEx eaEX = svcEnrolledAccount.GetByUserIDAndCurrencyID(c.id, n.currencyfromiso);
                t.enrolledAccountsID = eaEX.id;
                t.enrolledAccountsCurrencyID = eaEX.currenycid;
                t.accountNumber = eaEX.accountnumber;
                t.receiversID = 0;
                if (n.transactionid == 1)
                {
                    t.transactionType = "11002";
                    t.description = "Email money tranfer to " + n.benificiaryfullname + " Email: " + n.benificiaryemail;
                    n.transactionType = "11002";
                    svcTransaction.Save(t);
                    n.transactionId = t.id;
                    svcTransferMoney.Save(n);
                    string ret = new Generator().sendemail(t, n);

                    return new Generator().sendConfirmationToSenderMoney(t, n, c);

                }
                else if (n.transactionid == 2)
                {
                    t.transactionType = "11001";
                    t.description = "SMS money tranfer to " + n.benificiaryfullname + " Mobile Number:" + n.benificiarymobileno;
                    n.transactionType = "11001";
                    t.status = "CLEARED";
                    svcTransaction.Save(t);
                    n.transactionId = t.id;
                    svcTransferMoney.Save(n);
                    rcvC = svcCustomer.GetByMobileNumber(n.benificiarymobileno);
                    if (rcvC != null)
                    {
                        TransactionEx TE = new TransactionEx();
                        var cu = svcCurrency.GetByISO(n.currencytoiso);
                        EnrolledAccountEx eaEXC = svcEnrolledAccount.GetByUserIDAndCurrencyID(rcvC.id, n.currencytoiso);
                        if (eaEXC != null)
                        {
                            TE.accountNumber = eaEXC.accountnumber;
                            TE.enrolledAccountsCurrencyID = cu.id;
                            TE.enrolledAccountsID = eaEXC.id;
                        }
                        else
                        {
                            var e = new EnrolledAccountEx();

                            e.currenycid = cu.id;
                            e.currency = n.currencytoiso;
                            e.accountDefault = false;
                            e.userId = rcvC.id;
                            e.userTID = rcvC.tid;
                            svcEnrolledAccount.Save(e);
                            var ss = svcEnrolledAccount.GetByUserIDAndCurrencyID(rcvC.id, n.currencytoiso);
                            TE.accountNumber = ss.accountnumber;
                            TE.enrolledAccountsCurrencyID = cu.id;
                            TE.enrolledAccountsID = ss.id;



                        }
                        TE.balance = n.amountto;
                        TE.credit = true;
                        TE.debit = false;
                        TE.currencyfromiso = n.currencyfromiso;
                        TE.currencytoiso = n.currencytoiso;
                        TE.dataCreated = DateTime.Now;
                        TE.description = "SMS money tranfer From " + (c.firstname).ToUpper() + " " + (c.lastname).ToUpper();
                        //TE.partnerId = 
                        TE.status = "CLEARED";
                        TE.transactionTID = "" + Guid.NewGuid();
                        TE.transactionType = "11001";
                        TE.userId = rcvC.id;
                        TE.userTID = rcvC.tid;
                        svcTransaction.Save(TE);

                    }
                    else
                    {
                     
                    }
                    return new Generator().sendSMSMoney(t, n, c.mobilenumber);
                }
                return "";
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return "failed";
            }
        }


        [Route("sendmoney/admin")]
        [HttpPost]
        public object SendMoneyAdminSide(AdminETransferEx data)
        {
            try
            {
                int pID = 0;
                string tID = string.Empty;
                string pName = string.Empty;
                CoopEnrolledAccountEx cEx = svcCEA.getIdByUserIDRoleIDCurrency(data.userID, data.roleID, data.currencyFrom);
                CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                TransactionEx tEx = new TransactionEx();
                TransferMoneyEx tmEx = new TransferMoneyEx();
                StringBuilder sb = new StringBuilder();
                CustomerEx recustEx = new CustomerEx();
                CurrencyEx currToEx = svcCurr.GetByISO(data.currencyTo);
                EnrolledAccountEx eaEx = new EnrolledAccountEx();
                PaymentBreakdownEx pbEx = new PaymentBreakdownEx();
                try
                {
                    /*
                     * Save Transaction
                     * Save TransferMoney
                     * Save CoopTransactionLog
                     * Send Notification
                     * Suppose to add commissions
                     * */
                    if (data.transactionType.Equals("11002"))//EMAIL_MONEY_TRANSFER
                    {//IF Mobile Number is not existing do not create a transaction.
                     //recustEx = svcCustomer.GetCheckEmail(data.email);
                     //if(recustEx == null)
                     //{
                     //        recustEx.id = 0;
                     //        recustEx.tid = new Guid("").ToString();
                     //}

                        sb.Append("*E-Transfer Transaction WALK-IN --(EMAIL-MONEY-(PENDING))--");
                        sb.AppendLine();
                        sb.AppendFormat("From: {0}", data.senderFullname);
                        sb.AppendFormat("---To: {0}", data.receiverFullName);
                        sb.AppendLine();
                        sb.AppendFormat("Sender's Mobile No: {0}", data.senderMobileNo);
                        tEx.description = sb.ToString();
                        tEx.status = "PENDING";

                        tmEx.status = "PENDING";

                        ctlEx.transactionType = "11002";
                    }
                    else if (data.transactionType.Equals("11001"))//SMS_MONEY_TRANSFER
                    {
                        data.mobile = data.mobile.Trim();
                        data.mobile = data.mobile.Contains("+") ? data.mobile : "+" + data.mobile;
                        recustEx = svcCustomer.GetByMobileNumber(data.mobile);
                        if (recustEx == null)
                        {
                            //custEx.tid = new Guid("").ToString();
                            //custEx.id = 0;
                            return "Invalid SMS-Transfer. User Must Have existing e-wallet account mobile number.";
                        }
                        eaEx = svcEnrolledAccount.GetByUserIDUserTIDAndCurrencyISO(recustEx.id, recustEx.tid, data.currencyTo);
                        if (eaEx == null)
                        {
                            eaEx = new EnrolledAccountEx();
                            eaEx.accountDefault = false;
                            eaEx.currency = data.currencyTo;
                            eaEx.currenycid = svcCur.GetByISO(data.currencyTo).id;
                            eaEx.userId = recustEx.id;
                            eaEx.userTID = recustEx.tid;
                            svcEnrolledAccount.Save(eaEx);
                            eaEx = svcEnrolledAccount.Get(eaEx.id);
                        }
                        sb.Append("*E-Transfer Transaction WALK-IN --(SMS-MONEY)--");
                        sb.AppendLine();
                        sb.AppendFormat("From: {0}", data.senderFullname);
                        sb.AppendFormat("---To: {0}", data.receiverFullName);
                        sb.AppendLine();
                        sb.AppendFormat("Sender's Mobile No: {0}", data.senderMobileNo);
                        tEx.description = sb.ToString();
                        tEx.status = "CLEARED";


                        tmEx.status = "CLEARED";

                        ctlEx.transactionType = "11001";
                    }
                    else
                    {
                        return "Unknown Transction Type.";
                    }


                    int cntryID = 0;
                    int steID = 0;
                    switch (data.roleID)
                    {
                        case 3:
                            PartnersEx pEx = svcPartners.Get(data.userID);
                            pID = Convert.ToInt32(pEx.partnerType.Equals("MASTER_PARTNER") ? pEx.id : pEx.masterPartnerID);
                            tID = pEx.tid;
                            cntryID = Convert.ToInt32(pEx.countryid);
                            steID = Convert.ToInt32(pEx.stateid);
                            pName = pEx.partnerType.Equals("MASTER_PARTNER") ? pEx.finame : svcPartners.Get(pID).finame;
                            break;
                        case 4:
                            AgentsEx aEx = svcAgents.Get(data.userID);
                            pID = Convert.ToInt32(aEx.partnerID);
                            tID = aEx.tid;
                            cntryID = Convert.ToInt32(aEx.countryid);
                            steID = Convert.ToInt32(aEx.stateid);
                            pName = svcPartners.Get(pID).finame;
                            break;
                        case 5:
                            MerchantEx mEx = svcMerchant.Get(data.userID);
                            pID = Convert.ToInt32(mEx.masterPartnerID);
                            cntryID = Convert.ToInt32(mEx.countryid);
                            steID = Convert.ToInt32(mEx.stateid);
                            tID = mEx.tid;
                            pName = svcPartners.Get(pID).finame;
                            break;
                    }
                    //Transaction
                    tEx.accountNumber = cEx.accountnumber;
                    tEx.balance = Convert.ToDecimal(data.scac.fees.senderAmount);
                    tEx.credit = false;
                    tEx.debit = true;
                    tEx.currencyfromiso = data.currencyFrom;
                    tEx.currencytoiso = data.currencyTo;
                    tEx.dataCreated = DateTime.Now;
                    tEx.enrolledAccountsCurrencyID = cEx.currenycid;
                    tEx.enrolledAccountsID = cEx.id;
                    tEx.partnerId = pID;
                    tEx.receiversID = 0;
                    tEx.transactionTID = new Generator().generateTransactionNo(data.transactionType, svcTransaction.GetTransactionLastIDNumber(), data.currencyFrom);
                    tEx.transactionType = data.transactionType;
                    tEx.userId = data.userID;
                    tEx.userTID = tID;
                    svcTransaction.Save(tEx);

                    //PaymentBreakDown
                    //pbEx.
                    //---
                    //TransferMoney
                    tmEx.accountNumber = tEx.accountNumber;
                    tmEx.amountfrom = Convert.ToDecimal(data.amount);
                    tmEx.amountto = Convert.ToDecimal(data.convertedAmount);
                    tmEx.answer = data.answer;
                    tmEx.question = data.question;
                    tmEx.benificiaryemail = data.email;
                    tmEx.benificiaryfullname = data.receiverFullName;
                    tmEx.benificiaryid = 0;
                    tmEx.benificiarymobileno = data.mobile;
                    tmEx.currencyfromid = cEx.currenycid;
                    tmEx.currencyfromiso = cEx.currency;
                    tmEx.currencytoid = currToEx.id;
                    tmEx.currencytoiso = currToEx.iso;
                    tmEx.customerid = 0;
                    tmEx.customertid = Guid.Empty.ToString();
                    tmEx.datecreated = tEx.dataCreated;
                    tmEx.dateCreated = tEx.dataCreated;
                    tmEx.partnerId = pID;
                    tmEx.partnerName = pName;
                    tmEx.question = data.question;
                    tmEx.receiverFirstname = data.receiverFullName;
                    tmEx.receiverLastname = string.Empty;
                    tmEx.receiverMiddlename = string.Empty;
                    tmEx.receiverSuffix = string.Empty;
                    tmEx.senderFirstName = data.senderFullname;
                    tmEx.senderLastName = string.Empty;
                    tmEx.senderMiddleName = string.Empty;
                    tmEx.sendermobileno = data.senderMobileNo;
                    tmEx.sendersuffix = string.Empty;
                    tmEx.transactionId = tEx.id;
                    tmEx.transactionid = tEx.id;
                    tmEx.transactionTID = tEx.transactionTID;
                    tmEx.transactionType = data.transactionType;
                    svcTransferMoney.Save(tmEx);
                    //CoopTranasactionLog -- Credit Amount to someone
                    //CoopTranasactionLog -- Debit Amount To user
                    if (data.transactionType.Equals("11001"))
                    {
                        //Transaction
                        tEx.id = 0;
                        tEx.accountNumber = eaEx.accountnumber;
                        tEx.balance = Convert.ToDecimal(data.convertedAmount);
                        tEx.credit = true;
                        tEx.debit = false;
                        tEx.currencyfromiso = data.currencyFrom;
                        tEx.currencytoiso = data.currencyTo;
                        tEx.dataCreated = DateTime.Now;
                        tEx.enrolledAccountsCurrencyID = eaEx.currenycid;
                        tEx.enrolledAccountsID = eaEx.id;
                        tEx.partnerId = pID;
                        tEx.receiversID = 0;
                        tEx.transactionTID = tEx.transactionTID;
                        tEx.transactionType = data.transactionType;
                        tEx.userId = recustEx.id;
                        tEx.userTID = recustEx.tid;
                        svcTransaction.Save(tEx);

                        ctlEx.amount = Convert.ToDecimal(data.scac.fees.senderAmount);
                        ctlEx.currencyAccountFrom = tEx.currencyfromiso;
                        ctlEx.currencyAccountTo = tEx.currencytoiso;
                        ctlEx.date_created = DateTime.Now;
                        ctlEx.giverTID = tID;
                        ctlEx.giverUserId = data.userID;
                        ctlEx.giverUserRoles = data.roleID;
                        ctlEx.isCredit = true;
                        ctlEx.receiverTID = recustEx.tid;
                        ctlEx.receiverUserId = recustEx.id;
                        ctlEx.receiverUserRoles = 7;
                        ctlEx.transactionDescription = tEx.description + "---" + "Converted Amount to (" + data.currencyTo + "):" + data.convertedAmount;
                        ctlEx.transactionTID = tmEx.transactionTID;
                        svcCTLS.Save(ctlEx);

                        ctlEx = new CoopTransactionLogEx();

                        ctlEx.amount = Convert.ToDecimal(data.remainingBalance);
                        ctlEx.currencyAccountFrom = tEx.currencyfromiso;
                        ctlEx.currencyAccountTo = tEx.currencytoiso;
                        ctlEx.date_created = DateTime.Now;
                        ctlEx.giverTID = tID;
                        ctlEx.giverUserId = data.userID;
                        ctlEx.giverUserRoles = data.roleID;
                        ctlEx.isCredit = false;
                        ctlEx.receiverTID = recustEx.tid;
                        ctlEx.receiverUserId = recustEx.id;
                        ctlEx.receiverUserRoles = 7;
                        ctlEx.transactionDescription = tEx.description + "---" + "Converted Amount to (" + data.currencyTo + "):" + data.convertedAmount;
                        ctlEx.transactionTID = tEx.transactionTID;
                        ctlEx.transactionType = tEx.transactionType;
                        svcCTLS.Save(ctlEx);

                        #region SaveCommission
                        new GlobalCallForServicesController(
                            svcCur,
                            svcPaymentBreakdown,
                            svcCommissionService,
                            svcCTLS,
                            svcCountry).SaveCommission(tEx, data.scac, tID, pName, pID, data.roleID, cntryID, steID);
                        #endregion SaveCommission


                    }
                    else
                    {
                        #region SaveCoopTransactionLog 
                        CoopTransactionLogEx ctlEx1 = new CoopTransactionLogEx();
                        ctlEx1.amount = tEx.balance;
                        ctlEx1.currencyAccountFrom = tEx.currencyfromiso;
                        ctlEx1.currencyAccountTo = tEx.currencytoiso;
                        ctlEx1.date_created = tEx.dataCreated;
                        ctlEx1.giverTID = tID;
                        ctlEx1.giverUserId = data.userID;
                        ctlEx1.giverUserRoles = data.roleID;
                        ctlEx1.isCredit = true;
                        ctlEx1.receiverTID = "" + Guid.Empty;
                        ctlEx1.receiverUserId = 0;
                        ctlEx1.receiverUserRoles = 7;
                        ctlEx1.transactionDescription = tEx.description;
                        ctlEx1.transactionTID = tEx.transactionTID;
                        ctlEx1.transactionType = tEx.transactionType;
                        svcCTLS.Save(ctlEx1);
                        #endregion SaveCoopTransactionLog

                        ctlEx.amount = Convert.ToDecimal(tEx.balance);
                        ctlEx.currencyAccountFrom = tEx.currencyfromiso;
                        ctlEx.currencyAccountTo = tEx.currencytoiso;
                        ctlEx.date_created = DateTime.Now;
                        ctlEx.giverTID = tID;
                        ctlEx.giverUserId = data.userID;
                        ctlEx.giverUserRoles = data.roleID;
                        ctlEx.isCredit = false;
                        ctlEx.receiverTID = Guid.Empty.ToString();
                        ctlEx.receiverUserId = tmEx.benificiaryid;
                        ctlEx.receiverUserRoles = 7;
                        ctlEx.transactionDescription = tEx.description;
                        ctlEx.transactionTID = tmEx.transactionTID;
                        svcCTLS.Save(ctlEx);

                        #region SaveCommission
                        new GlobalCallForServicesController(
                            svcCur,
                            svcPaymentBreakdown,
                            svcCommissionService,
                            svcCTLS,
                            svcCountry).SaveCommission(tEx, data.scac, tID, pName, pID, data.roleID, cntryID, steID);
                        #endregion SaveCommission
                    }

                    if (data.transactionType.Equals("11002"))
                    {
                        return new Generator().sendemail(tEx, tmEx);
                    }
                    else
                    {
                        new Generator().sendSMSNotifEtransfer("sender", data.senderFullname, Convert.ToString(tEx.id), data.senderMobileNo);
                        return new Generator().sendSMSNotifEtransfer("receiver", data.receiverFullName, Convert.ToString(tEx.id), data.mobile);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                    return "failed";
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return "failed";
            }
        }


        public string SendSms(string mobile_number, string OTP)
        {
            try
            {
                const string accountSid = "AC58bc3668ea1544a8bb69f745f642a933";
                const string authToken = "9185158d2ea6518e1178805630b68f0b";
                TwilioClient.Init(accountSid, authToken);

                try
                {
                    var message = MessageResource.Create(
                        to: new PhoneNumber(mobile_number),
                        from: new PhoneNumber("+17027896606"),
                        body: "Please use this OTP:" + OTP + " to verify your EWallet Account.");
                    //  Console.WriteLine(message.Sid);
                    return "send successfull";
                    //   Console.ReadKey();
                }
                catch
                {
                    return "Illegal";
                }
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }

        }
        
        [Route("SendEmailMoneyGetCustomerData/{id}")]
        [HttpGet]
        public CustomerEx Get(int id)
        {
            try
            {
                return svcCustomer.Get(id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        [Route("Customer/CustomerIfIsInOFAC")]
        [HttpPost]
        public bool checkIfClientIsInOFAC([FromBody] CustomerEx n)
        {
            try
            {
                string res = string.Empty;
                bool isVerified = false;
                bool isInOFAC = false;
                List<OFAClistEx> list = new List<OFAClistEx>();
                using (WebClient wb = new WebClient())
                {
                    wb.Headers.Add("Content-Type", "application/json");
                    res = wb.DownloadString("https://easyofac.com/api/nameSearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&first_name=" + n.firstname + "&last_name=" + n.lastname + "&test=1");
                    list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
                    if (list.Count > 0)
                    {
                        foreach (OFAClistEx d in list)
                        {
                            d.userID = n.id;
                            d.userCode = 1007;
                            d.userIsVerified = false;
                            d.isCompany = false;
                            svcOFAC.Save(d);
                        }
                        isInOFAC = true;
                    }
                    else
                    {
                        isInOFAC = false;
                        isVerified = true;
                    }
                }
                n.isInOFACList = isInOFAC;
                n.isVerified = isVerified;
                svcCustomer.Save(n);
                return isInOFAC;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return false;
            }
        }
        /*
         *Mobile API 
         */
        [Route("Mobile/Save")]
        [HttpPost]
        public object mobileSave([FromBody] CustomerMobileEx cmEX)
        {
            try
            {
                CustomerEx cEx = svcCustomer.Get(cmEX.userID);
                if (cEx != null)
                {
                    cEx.address = cmEX.address != null || cmEX.address != string.Empty ? cmEX.address : cEx.address;
                    cEx.city = cmEX.city != null || cmEX.city != string.Empty ? cmEX.city : cEx.city;
                    cEx.birthdate = cmEX.date_of_birth != null || cmEX.date_of_birth != string.Empty ? Convert.ToDateTime(cmEX.date_of_birth) : cEx.birthdate;
                    cEx.email = cmEX.email != null || cmEX.email != string.Empty ? cmEX.email : cEx.email;
                    cEx.firstname = cmEX.first_name != null || cmEX.first_name != string.Empty ? cmEX.first_name : cEx.firstname;
                    cEx.lastname = cmEX.last_name != null || cmEX.last_name != string.Empty ? cmEX.last_name : cEx.lastname;
                    cEx.middlename = cmEX.middle_name != null || cmEX.middle_name != string.Empty ? cmEX.middle_name : cEx.middlename;
                    cEx.mobilenumber = cmEX.mobile != null || cmEX.mobile != string.Empty ? "+"+cmEX.mobile : cEx.mobilenumber;
                    cEx.gender = cmEX.gender;
                    cEx.zipcode = cmEX.zipcode != null || cmEX.zipcode != string.Empty ? cmEX.zipcode : cEx.zipcode;

                    svcCustomer.Save(cEx);
                    object res = new
                    {
                        ret = "success",
                        retData = cEx
                    };
                    return res;
                }
                else
                {
                    object res = new
                    {
                        ret = "error"
                    };
                    return res;
                }
            }catch(Exception ex)
            {
                object res = new
                {
                    ret = "error"
                };
                new Generator().LogException(ex, "Customer-Log");
                return res;
            };
        }

        [Route("Mobile/mt/")]
        [HttpPost]
        // [Authorize(Roles = "administrators")] //money transfer not etransfer
        public string mobiletransfer([FromBody] TransferMoneyMobileEx p)
        {
            try
            {
                CustomerEx c = new CustomerEx();
                TransactionEx t = new TransactionEx();
                TransferMoneyEx n = new TransferMoneyEx();
                ReceiversCustomerEx m = new ReceiversCustomerEx();
                MoneyTransferTransactionsEx mte = new MoneyTransferTransactionsEx();
                string res = string.Empty;
                if (validateField(p).Equals("valid"))
                {
                    c = svcCustomer.Get(p.customerID);
                    n.customerid = c.id;
                    n.customertid = c.tid;
                    t.dataCreated = DateTime.Now;
                    t.credit = false;
                    t.debit = true;
                    t.balance = p.amount;

                    t.userId = c.id;
                    t.userTID = c.tid;
                    t.status = "PENDING";
                    t.transactionTID = "" + Guid.NewGuid();
                    n.transactionTID = t.transactionTID;
                    n.transactionid = 3;

                    m.accountnumber = new Generator().getAccount(p.rCountryID, p.rStateID);
                    m.address = p.rAddress;
                    m.city = p.rCity;
                    m.countryid = p.rCountryID;
                    m.stateid = p.rStateID;
                    m.currency = p.rCurrISO;
                    m.currencyid = svcCurr.GetByISO(p.rCurrISO).id;
                    m.email = p.rEmail;
                    m.firstname = p.rFirstName;
                    m.lastname = p.rLastName;
                    m.middlename = p.rMiddleName;
                    m.mobilenumber = p.rMobileNo != null ? "+" + p.rMobileNo : "";
                    m.phonenumber = string.Empty;
                    m.username = p.rMobileNo;
                    m.zipcode = p.rPostalCode;
                    m.expiryDate = DateTime.Now.AddYears(2);

                    t.transactionType = "10002";
                    string trackingnumber = new Generator().getTransactionNo(p.rCountryID, p.rStateID, svcTransaction.GetTransactionLastIDNumber());
                    string benificiaryfullname = p.rFirstName + " " + p.rMiddleName + " " + p.rLastName;
                    t.description = "Money tranfer to " + benificiaryfullname + " Email: " + p.rEmail + " Mobile Number: " + p.rMobileNo + " Tracking No.: " + trackingnumber;
                    n.transactionType = "10002";
                    m.agentID = 0;
                    //if (m.agentID != 0)
                    //{
                    m.tid = "" + Guid.NewGuid();
                    svcReceiverCustomer.Save(m);

                    t.receiversID = m.id;
                    t.accountNumber = m.accountnumber;

                    n.accountNumber = t.accountNumber;
                    EnrolledAccountEx eaEX = svcEnrolledAccount.GetByUserIDAndCurrencyID(c.id, p.accountCurrISO);
                    t.currencyfromiso = p.accountCurrISO;
                    t.currencytoiso = p.rCurrISO;
                    t.enrolledAccountsID = eaEX.id;
                    t.enrolledAccountsCurrencyID = eaEX.currenycid;
                    t.accountNumber = eaEX.accountnumber;
                    svcTransaction.Save(t);
                    n.transactionId = t.id;

                    n.amountfrom = p.amount;
                    n.amountto = Convert.ToDecimal(p.amountConverted);
                    n.answer = p.rSecretCode;
                    n.benificiarymobileno = m.mobilenumber;
                    n.benificiaryfullname = m.firstname + " " + m.middlename + " " + m.lastname;
                    n.benificiaryid = m.id;
                    n.benificiarytid = m.tid;
                    n.benificiaryemail = p.rEmail;
                    n.currencyfromid = svcCurr.GetByISO(p.accountCurrISO).id;
                    n.currencyfromiso = p.accountCurrISO;
                    n.currencytoid = svcCurr.GetByISO(p.rCurrISO).id;
                    n.currencytoiso = p.rCurrISO;
                    n.datecreated = DateTime.Now;
                    n.dateCreated = DateTime.Now;
                    n.receiverFirstname = p.rFirstName;
                    n.receiverLastname = p.rLastName;
                    n.receiverMiddlename = p.rMiddleName;
                    n.receiverSuffix = string.Empty;
                    n.senderFirstName = c.firstname;
                    n.senderLastName = c.lastname;
                    n.senderMiddleName = c.middlename;
                    n.sendermobileno = c.mobilenumber;
                    n.sendersuffix = string.Empty;
                    n.question = "Password";
                    n.status = t.status;
                    svcTransferMoney.Save(n);

                    mte.accountNumber = m.accountnumber;
                    mte.agentID = m.agentID;
                    mte.receiversID = m.id;
                    mte.status = t.status;
                    mte.transactionID = t.id;
                    mte.userID = c.id;
                    mte.trackingnumber = trackingnumber;
                    mte.walkin = false;
                    mte.idType = string.Empty;
                    mte.idNumber = string.Empty;
                    svcMoneyTansferTransaction.Save(mte);
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat("tID:{0}", t.id).AppendLine();
                    sb.AppendFormat("transactionID:{0}", t.transactionTID).AppendLine();
                    sb.AppendFormat("userID:{0}", t.userId).AppendLine();
                    svcTLog.SaveLog(sb.ToString(), "MONEY_TRANSFER");

                    //return new Generator().sendSMSMoney(t, n, c.mobilenumber);
                    try
                    {

                        decimal receiversAmount = n.amountto;
                        string sendersFullName = c.lastname;
                        string receiversFullName = n.receiverLastname != null ? n.receiverLastname : n.benificiaryfullname;

                        new Generator().sendSMSMoneyFromWalkIn("sender", c.mobilenumber, m.mobilenumber, n.answer, receiversAmount, sendersFullName, receiversFullName, trackingnumber);
                        new Generator().sendSMSMoneyFromWalkIn("receiver", c.mobilenumber, m.mobilenumber, n.answer, receiversAmount, sendersFullName, receiversFullName, trackingnumber);
                        res = "success";
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        res = "error";
                    }
                }
                else
                {
                    res = validateField(p);
                }
                //}else{ return "Please Select Agent."; }
                //  return new Generator().sendSMSMoney(t, n, c.mobilenumber);
                return res;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Customer-Log");
                return null;
            }
        }

        public string validateField(TransferMoneyMobileEx p) { 
           if(p == null) { return "Please fill up all fields"; }
           if(p.accountCurrISO.Equals("") || p.accountCurrISO == null) { return "Please select account currency."; }
           if(Convert.ToDecimal(p.amount) < 0 || p.accountCurrISO.Equals("") || p.accountCurrISO == null) { return "Please add amount."; }
           if(p.rAddress.Equals("") || p.rAddress == null) { return "Please add address."; }
           if(p.rCity.Equals("") || p.rCity == null) { return "Please add city."; }
           if(p.rCountryID.Equals("") || p.rCountryID == 0) { return "Please select country."; }
           if(p.rCurrISO.Equals("") || p.rCurrISO == null) { return "Plese select currency for receiver."; }
           if(p.rMobileNo.Equals("") || p.rMobileNo == null) { return "Please fill-up mobile number."; }
           if(p.rPostalCode.Equals("") || p.rPostalCode == null) { return "Please fill-up postal code."; }
           if(p.rSecretCode.Equals("") || p.rSecretCode == null) { return "Please fill-up secret code."; }
           if(p.rStateID.Equals("") || p.rStateID == 0) { return "Please select state for receivier."; }
           else{ return "valid"; }
                
        }

        [Route("AccountFunding/AddAccount")]
        [HttpPost]
        public object FundAccountMobile(AccountFundingEx data)
        {
            try
            {
                CustomerEx cEx = svcCustomer.Get(data.userID);
                EnrolledAccountEx eaEx = svcEnrolledAccount.GetByUserIDUserTIDAndCurrencyISO(cEx.id, cEx.tid, data.toISO);
                TransactionEx tEx = new TransactionEx();
                if (eaEx == null)
                {
                    eaEx = new EnrolledAccountEx();
                    eaEx.accountDefault = false;
                    eaEx.currency = data.toISO;
                    eaEx.currenycid = svcCurr.GetByISO(data.toISO).id;
                    eaEx.userId = cEx.id;
                    eaEx.userTID = cEx.tid;
                    svcEnrolledAccount.Save(eaEx);
                    eaEx = svcEnrolledAccount.Get(eaEx.id);
                }
                tEx.accountNumber = eaEx.accountnumber;
                tEx.balance = data.toAmount;
                tEx.currencyfromiso = data.fromISO;
                tEx.currencytoiso = data.toISO;
                tEx.credit = true;
                tEx.debit = false;
                tEx.description = "Account Credit For " + data.toISO + " from Mobile transaction.";
                tEx.enrolledAccountsCurrencyID = eaEx.currenycid;
                tEx.enrolledAccountsID = eaEx.id;
                tEx.partnerId = 0;
                tEx.receiversID = 0;
                tEx.status = "CLEARED";
                tEx.transactionTID = "" + Guid.NewGuid();
                tEx.transactionType = "50001";
                tEx.userId = cEx.id;
                tEx.userTID = cEx.tid;
                tEx.dataCreated = DateTime.Now;
                svcTransaction.Save(tEx);

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("tID:{0}", tEx.id).AppendLine();
                sb.AppendFormat("transactionID:{0}", tEx.transactionTID).AppendLine();
                sb.AppendFormat("userID:{0}", tEx.userId).AppendLine();
                svcTLog.SaveLog(sb.ToString(), "ACCOUNT_FUNDING_CREDIT");

                tEx = new TransactionEx();
                tEx.accountNumber = eaEx.accountnumber;
                tEx.balance = data.toAmount;
                tEx.currencyfromiso = data.fromISO;
                tEx.currencytoiso = data.toISO;
                tEx.credit = false;
                tEx.debit = true;
                tEx.description = "Account Debit From " + data.fromISO + " from Mobile transaction.";
                tEx.enrolledAccountsCurrencyID = eaEx.currenycid;
                tEx.enrolledAccountsID = eaEx.id;
                tEx.partnerId = 0;
                tEx.receiversID = 0;
                tEx.status = "CLEARED";
                tEx.transactionTID = "" + Guid.NewGuid();
                tEx.transactionType = "50001";
                tEx.userId = cEx.id;
                tEx.userTID = cEx.tid;
                tEx.dataCreated = DateTime.Now;
                svcTransaction.Save(tEx);

                sb = new StringBuilder();
                sb.AppendFormat("tID:{0}", tEx.id).AppendLine();
                sb.AppendFormat("transactionID:{0}", tEx.transactionTID).AppendLine();
                sb.AppendFormat("userID:{0}", tEx.userId).AppendLine();
                svcTLog.SaveLog(sb.ToString(), "ACCOUNT_FUNDING_DEBIT");
                return "Success";

            }catch(Exception Ex)
            {
                new Generator().LogException(Ex, "AccountFunding-Customer-Logs");
                return Ex;
            }
        }

    }
}
