﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Cooptavanza.Wallet.Repository;
using System.Net;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Cooptavanza.Wallet.Controllers
{
    public class AgentsApiController : ApiController
    {
        readonly AgentsService svcAgents;
        readonly UserAccountService svcUserAccount;
        readonly OFAClistService svcOFAC;
        readonly UserServicesService svcUserServices;
        readonly CoopTransactionLogService svcCTLS;
        readonly CoopEnrolledAccountService svcCEA;

        readonly PartnersService svcPartners;
        readonly MerchantService svcMerchant;
        readonly UsersService svcUsers;

        readonly CurrencyService svcCurr;

        readonly UserAccountActivitiesService svcUAA;

        public AgentsApiController(
            AgentsService svcAgents, 
            UserAccountService svcUserAccount, 
            OFAClistService svcOFAC, 
            UserServicesService svcUserServices,
            CoopTransactionLogService svcCTLS,
            CoopEnrolledAccountService svcCEA,
            PartnersService svcPartners,
            MerchantService svcMerchant,
            UsersService svcUsers,
            UserAccountActivitiesService svcUAA,
            CurrencyService svcCurr)
        {
            this.svcUserAccount = svcUserAccount;
            this.svcAgents = svcAgents;
            this.svcOFAC = svcOFAC;
            this.svcUserServices = svcUserServices;
            this.svcCTLS = svcCTLS;
            this.svcCEA = svcCEA;
            this.svcPartners = svcPartners;
            this.svcMerchant = svcMerchant;
            this.svcUsers = svcUsers;
            this.svcUAA = svcUAA;
            this.svcCurr = svcCurr;
        }

        [Route("Agents")]
        [HttpGet]
        public AgentSearchViewModel Get(string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try
            {
                AgentSearchViewModel x = new AgentSearchViewModel() { Results = svcAgents.GetPaged(SearchText, SortBy, PageNo, PageSize), Count = svcAgents.Count(SearchText) };
                return x;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Agents-Log");
                return null;
            }
        }


        [Route("Agents/{Id}")]
        [HttpGet]
        public AgentsEx GetPerson(int Id)
         {
            try
            {
                AgentsEx aEx = svcAgents.Get(Id);
                UserAccountEx uaEx = svcUserAccount.GetAccountByUserId(4, aEx.id, aEx.tid);
                if (uaEx != null)
                {
                    aEx.username = uaEx.username;
                    aEx.password = new EncryptionHelper().Decrypt(uaEx.password);
                }
                return aEx;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Agents-Log");
                return null;
            }
            //return null;;
        }


        [Route("Agents/adminsave")]
        [HttpPost]
        // [Authorize(Roles = "administrators")]
        public AgentsEx Save([FromBody] AgentsEx n)
        {
            try
            {
                var tempid = n.id;
                string password = n.password != null ? new EncryptionHelper().Encrypt(n.password) : "default";
                n.tid = n.id != 0 ? n.tid : "" + Guid.NewGuid();
                n.accountnumber = n.id != 0 ? n.accountnumber : new Generator().getAccount(n.countryid, n.stateid);

                n.email = n.email == null ? svcAgents.CountAgents() + "agent@gmail.com" : n.email;
                n.address = n.address == null ? "agent address" : n.address;
                n.partnerID = n.coopAccountListData.coopRootAccountUserIDAndRole.loggedID;
                bool debitToGiver = false;
                if (n.coopAccountListData.coopRootAccountUserIDAndRole != null)
                {
                    if (n.coopAccountListData.coopRootAccountUserIDAndRole.roleID == 3 && n.coopAccountListData.coopAccountListAndcurencyData != null)
                    {
                        debitToGiver = true;
                    }
                }

                AgentsEx a = svcAgents.Get(n.id);
                if (a != null)
                {
                    if (a.id > 0)
                    {
                        n.address = n.address != null ? n.address : a.address;
                        n.city = n.city != null ? n.city : a.city;
                        n.cityid = n.cityid != null ? n.cityid : a.cityid;
                        n.country = n.country != null ? n.country : a.country;
                        n.countryid = n.countryid != null && n.countryid > 0 ? n.countryid : a.countryid;
                        n.state = n.state != null ? n.state : a.state;
                        n.stateid = n.stateid != null && n.stateid > 0 ? n.stateid : a.stateid;

                        List<AccountListEx> ceaEx = svcCEA.getCoopAccountList(a.id, 4);
                        n.currency = ceaEx.Count == 0 || ceaEx[0].currencyAccountTo == null ? "D0F" : ceaEx[0].currencyAccountTo;
                        n.currencyid = svcCurr.GetByISO(n.currency).id;
                        //a.currency = n.currency != null ? n.currency : a.currency;
                        //a.currencyid = n.currencyid > 0 ? n.currencyid : a.currencyid;
                        n.name = n.name != null ? n.name : a.name;
                        n.mobilenumber = n.mobilenumber != null ? n.mobilenumber : a.mobilenumber;
                        n.phonenumber = n.phonenumber != null ? n.phonenumber : a.phonenumber;

                        n.zipcode = n.zipcode != null ? n.zipcode : a.zipcode;
                    }
                }

                svcAgents.Save(n);

                AgentsEx data = svcAgents.Get(n.id);
                UserAccountEx ua = new UserAccountEx();
                UserServicesEx us = new UserServicesEx { user_id = n.id, roles_id = 4 };

                List<coopAccountListAndCurrencyData> coopALACD = n.coopAccountListData.coopAccountListAndcurencyData;
                coopRootAccountUserIDAndRole coopRAUAR = n.coopAccountListData.coopRootAccountUserIDAndRole;
                if (coopALACD != null)
                {
                    foreach (coopAccountListAndCurrencyData resData in coopALACD)
                    {
                        CoopEnrolledAccountEx ceaEx = new CoopEnrolledAccountEx();
                        ceaEx = svcCEA.getIdByUserIDRoleIDCurrency(n.id, 4, resData.currency.iso);
                        if (ceaEx.id == 0)
                        {
                            #region Coop enrolled Account EX;
                            ceaEx.id = 0;
                            ceaEx.accountDefault = true;
                            ceaEx.accountnumber = "";
                            ceaEx.currency = resData.currency.iso;
                            ceaEx.currenycid = resData.currency.id;
                            ceaEx.roleID = Convert.ToString(4);
                            ceaEx.userId = n.id;
                            ceaEx.userTID = n.tid;
                            #endregion Coop enrolled Account EX;

                            svcCEA.Save(ceaEx);
                            ceaEx.accountnumber = svcCEA.getAccountNumberForCoopRed(n.id, ceaEx.currenycid, 4, ceaEx.id);
                            svcCEA.Save(ceaEx);
                        }

                        if (resData.status.Equals("added"))
                        {
                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = resData.amount;
                            ctlEx.currencyAccountFrom = resData.currency.iso;
                            ctlEx.currencyAccountTo = resData.currency.iso;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = true;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 4;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + resData.amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "CREDIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);
                        }

                    }
                }

                if (debitToGiver)
                {
                    for (int q = 0; q < n.coopAccountListgiver.Length; q++)
                    {
                        object s = n.coopAccountListData.coopAccountListAndcurencyData.Find(x => x.currency.iso.Equals(n.coopAccountListgiver[q].currencyAccountTo));
                        if (s != null)
                        {

                            CoopTransactionLogEx ctlEx = new CoopTransactionLogEx();
                            ctlEx.amount = n.coopAccountListgiver[q].amount;
                            ctlEx.currencyAccountFrom = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.currencyAccountTo = n.coopAccountListgiver[q].currencyAccountTo;
                            ctlEx.date_created = DateTime.Now;
                            ctlEx.giverTID = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).tID;
                            ctlEx.giverUserId = new GlobalCallForServicesController(
                                svcAgents,
                                svcMerchant,
                                svcUsers,
                                svcPartners).getTIDandIDByUserRole(coopRAUAR.roleID, coopRAUAR.loggedID).id;
                            ctlEx.giverUserRoles = coopRAUAR.roleID;
                            ctlEx.isCredit = false;
                            ctlEx.receiverTID = n.tid;
                            ctlEx.receiverUserId = n.id;
                            ctlEx.receiverUserRoles = 4;
                            ctlEx.transactionDescription = "Add amount to client with userTID = " + n.tid + " amount of " + n.coopAccountListgiver[q].amount + ", from userTID = " + ctlEx.giverTID;
                            ctlEx.transactionTID = "" + Guid.NewGuid();
                            ctlEx.transactionType = "DEBIT_ACCOUNT";
                            svcCTLS.Save(ctlEx);

                        }
                    }
                }

                List<compiledProductsToFilter> cptf = new List<compiledProductsToFilter>();
                if (n.compiledProductData != null)
                {
                    foreach (compiledProductData cpa in n.compiledProductData)
                    {
                        if (cptf.Exists(x => x.id == cpa.compiledProductsToFilter.id))
                        {
                            cptf.RemoveAt(cptf.FindIndex(y => y.id == cpa.compiledProductsToFilter.id));
                            cptf.Add(cpa.compiledProductsToFilter);
                        }
                        else
                        {
                            cptf.Add(cpa.compiledProductsToFilter);
                        }
                    }
                }

                if (cptf != null || cptf.Count > 0)
                {
                    foreach (compiledProductsToFilter cptf2 in cptf)
                    {
                        if (cptf2 != null)
                        {
                            if (cptf2.status.Equals("checked"))
                            {
                                us.product_id = cptf2.id;
                                svcUserServices.Save(us);
                                us.ID = 0;
                            }
                        }
                    }
                }

                UserAccountActivitiesEx UAA = new UserAccountActivitiesEx();
                if (tempid == 0)
                {
                    ua.tID = n.tid;
                    ua.password = password;
                    ua.user_id = n.id;
                    ua.roles_id = 4;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary

                    // User account activity log on new customer made
                    UAA.status = false;
                    UAA.token_id = "" + Guid.NewGuid();
                    UAA.userId = n.id;
                    UAA.userRole = 4;
                    UAA.dateCreated = DateTime.Now;

                    if (n.id > 0)
                    {
                        svcUserAccount.Save(ua);
                        svcUAA.Save(UAA);
                    }
                }
                else
                {
                    var x = svcUserAccount.GetAccountByUserId(4, n.id, n.tid);
                    ua.id = x.id;
                    ua.tID = n.tid;
                    ua.password = password.Equals("") ? x.password : password;
                    ua.user_id = n.id;
                    ua.roles_id = 4;
                    ua.username = n.username;
                    //ua.roles_id = svcRole.; temporary
                    svcUserAccount.Save(ua);
                }
                return svcAgents.Get(n.id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Agents-Log");
                return null;
            }
        }
        
        [Route("Agents/{Id}")]
        [HttpDelete]
        //[Authorize(Roles = "administrators")]
        public void Delete(int Id)
        {
            svcAgents.Remove(Id);
        }

        [Route("Agents/AgentsIfIsInOFAC")]
        [HttpPost]
        public bool checkIfAgentsIsInOFAC([FromBody] AgentsEx n)
        {
            try
            {
                string res = string.Empty;
                bool isVerified = false;
                bool isInOFAC = false;
                List<OFAClistEx> list = new List<OFAClistEx>();
                using (WebClient wb = new WebClient())
                {
                    wb.Headers.Add("Content-Type", "application/json");
                    res = wb.DownloadString("https://easyofac.com/api/nameSearch?api_key=OTA1ZTU1NDc5MThjMTViOTg1NzdhMWIw&first_name=" + n.firstname + "&last_name=" + n.lastname + "&test=1");
                    list = JsonConvert.DeserializeObject<List<OFAClistEx>>(res);
                    if (list.Count > 0)
                    {
                        foreach (OFAClistEx d in list)
                        {
                            d.userID = n.id;
                            d.userCode = 1004;
                            d.userIsVerified = false;
                            d.isCompany = false;
                            svcOFAC.Save(d);
                        }
                        isInOFAC = true;
                    }
                    else
                    {
                        isInOFAC = false;
                        isVerified = true;
                    }
                }
                n.isInOFACList = isInOFAC;
                n.isVerified = isVerified;
                svcAgents.Save(n);
                return isInOFAC;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Agents-Log");
                return false;
            }
        }

        [Route("Agents/getAgentByCountryIDAndStateID")]
        [HttpGet]
        public List<AgentsEx> getAgentByCountryIDAndStateID(int cID, int sID)
        {
            try
            {
                return svcAgents.getAgentByCountryIDAndStateID(cID, sID);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "Agents-Log");
                return null;
            }
        }
    }
}
