﻿using System.Web.Http;
using Cooptavanza.Wallet.Service;
using Cooptavanza.Models.ViewModels;
using System;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Net;
using System.Collections.Generic;
using Cooptavanza.Wallet.Repository;


namespace Cooptavanza.Wallet.Controllers
{
    public class CBSWireTransferApiController : ApiController
    {

        public CBSWireTransferApiController(
            )
        {

        }

        [Route("CBSWiretransfer/SendNotif")]
        [HttpPost]
        public ResponseEx SendNotif([FromBody] CBSWireTransferEx w)
        {
            try
            {
                ResponseEx RE = new ResponseEx();

                if (w.sndAccntNo != null)
                {
                    RE.message = "SUCCESS";
                    RE.date = DateTime.Now;
                    RE.data = new
                    {
                        amounttransfered = w.amountToTransfer,
                        currency = w.currency,

                    };

                    CBSWireTransferNotification CWTN = new CBSWireTransferNotification();
                    CWTN.sendWireTransferEmail(w);

                }
                else
                {
                    RE.date = DateTime.Now;
                    RE.message = "NO ACCOUNT SELECTED";

                }

                return RE;
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "CBSWiretransfer-Log");
                return null;
            }

        }


     

    }
}
