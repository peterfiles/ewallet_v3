﻿using Cooptavanza.Models.ViewModels;
using Cooptavanza.Wallet.Service;
using System;
using System.Web.Http;

namespace Cooptavanza.Wallet.Controllers
{
    public class TransactionsFeeApiController : ApiController
    {
        readonly TransactionFeeService svcTransactionFee;
        public TransactionsFeeApiController(TransactionFeeService svcTransactionFee)
        {
            this.svcTransactionFee = svcTransactionFee;
        }

        [Route("TransactionFee")]
        [HttpGet]
        public TransactionFeeSearchViewModel Get(int id, string TID, string SearchText = "", string SortBy = "Id Asc", int PageNo = 1, int PageSize = 10)
        {
            try { 
            return new TransactionFeeSearchViewModel() { Results = svcTransactionFee.GetPaged(id, TID, SearchText, SortBy, PageNo, PageSize), Count = svcTransactionFee.Count(id, TID, SearchText) };
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }



        [Route("TransactionFee/{Id}")]
        [HttpGet]
        public TransactionFeeEx GetTransaction(int Id)
        {
            try { 
            return svcTransactionFee.Get(Id);
            }
            catch (Exception e)
            {
                new Generator().LogException(e, "TransactionAPI-Log");
                return null;
            }
        }

      
    }
}
