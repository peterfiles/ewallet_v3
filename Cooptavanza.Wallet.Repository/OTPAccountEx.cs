﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class OTPAccountEx
    {
        public int id { get; set; }
        public string OTP { get; set; }
        public int userId { get; set; }
        public string deviceId { get; set; }
        public int userRoles { get; set; }
        public Nullable<System.DateTime> dateTime { get; set; }
        public Nullable<System.DateTime> mobileUTC { get; set; }

    }
}
