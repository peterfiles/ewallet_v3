﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class BillerTypeEx
    {
        public int id { get; set; }
        public string name { get; set; }
        public string tid { get; set; }
    }
}
