﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CoopTransactionLogEx
    {
        public int id { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public int giverUserRoles { get; set; }
        public int giverUserId { get; set; }
        public string giverTID { get; set; }
        public int receiverUserRoles { get; set; }
        public int receiverUserId { get; set; }
        public string receiverTID { get; set; }
        public string currencyAccountFrom { get; set; }
        public string currencyAccountTo { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<bool> isCredit { get; set; }
        public string transactionTID { get; set; }
        public string transactionType { get; set; }
        public string transactionDescription { get; set; }
    }

    public class CoopTransactionListEx
    {
        public int id { get; set; }
        public string memberTypeOrigin { get; set; }
        public string memberTypeOriginName { get; set; }
        public string transactionType { get; set; }
        public string memberTypeReceiver { get; set; }
        public string memberTypeReceiverName { get; set; }
        public string currencyName { get; set; }
        public string currencyISO { get; set; }
        public decimal amount { get; set; }
        public string transactionTID { get; set; }
    }
}
