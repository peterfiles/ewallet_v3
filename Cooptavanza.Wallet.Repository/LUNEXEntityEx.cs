﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXEntityEx
    {
        public decimal Accounts { get; set; }
        public string AcctNum { get; set; }
        public string CompanyId { get; set; }
        public string Manager { get; set; }
        public string Status { get; set; }

    }

    public class LunexTopUpEx
    {
       public int accountEnrolledID { get; set; }
       public string phone { get; set; }
       public string username { get; set; }
       public string password { get; set; }
       public decimal amount { get; set; }
       public string skuid { get; set; }
       public string curr { get; set; }
       public string amountTodeductInEnrolledAccount { get; set; }
    }

    public class LunexTopUpCoopRedEx
    {
        public int userID { get; set; }
        public int roleID { get; set; }
        public string phoneToLoad { get; set; }
        public string amount { get; set; }
        public string skuID { get; set; }
        public string currFrom { get; set; }
        public string currTo { get; set; }
        public string amounToDeduct { get; set; }
        public string amountRemaining { get; set; }
        public serviceChargesAndCommissionsEx scac { get; set; }
    }

    public class LunexCustomURLEx
    {
        public string url { get; set; }
        public string m { get; set; }
    }
}
