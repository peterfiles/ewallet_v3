﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MEXResponseRemitValidateEx
    {
        public int id { get; set; }
        public string ActionCode { get; set; }
        public string ConversionRate { get; set; }
        public string FeeCurrency { get; set; }
        public string FloatAmount { get; set; }
        public string FloatCurrency { get; set; }
        public string KioskFeeAmount { get; set; }
        public string LastUpdateTime { get; set; }
        public string Note { get; set; }
        public string OriginatorUID { get; set; }
        public string PaidFeeAmount { get; set; }
        public string QuoteAmt { get; set; }
        public string QuoteCurr { get; set; }
        public string RcvAccntBankCode { get; set; }
        public string RcvAccntBankName { get; set; }
        public string RcvAccntNo { get; set; }
        public string RcvFirstName { get; set; }
        public string RcvLastName { get; set; }
        public string ReferenceId { get; set; }
        public string RespCode { get; set; }
        public string RespMessage { get; set; }
        public string SentAmount { get; set; }
        public string SentCurrency { get; set; }
        public string Signature { get; set; }
        public string SndKycUrl { get; set; }
        public string SubmitTime { get; set; }
        public string TotalFeeAmount { get; set; }
        public string TransactionId { get; set; }
        public string MEXType { get; set; }
    }
}
