﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ImageIdCardEx
    {
        public int id { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public int userId { get; set; }
        public string path { get; set; }
        public string imageString { get; set; }
        public string idCardNo { get; set; }
        public string idCardType { get; set; }
        public Nullable<System.DateTime> expiryDate { get; set; }
        public string status { get; set; }
        public Nullable<int> adminID { get; set; }
        public Nullable<int> adminRoleID { get; set; }
    }
}
