﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class DummyLoadEx
    {
        public int id { get; set; }
        public string accountnumber { get; set; }
        public string bankid { get; set; }
        public int userid { get; set; }
        public string userpin { get; set; }
        public decimal credit { get; set; }
        public bool status { get; set; }
        public string currencycode { get; set; }
    }
}
