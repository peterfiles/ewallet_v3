﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CountryEx
    {
        public int id { get; set; }
        public Nullable<int> id_code { get; set; }
        public string iso { get; set; }
        public string name { get; set; }
        public string commonname { get; set; }
        public string iso3 { get; set; }
        public Nullable<int> numcode { get; set; }
        public int phonecode { get; set; }
        public Nullable<decimal> salestaxrate { get; set; }
        public string currencycode { get; set; }
    }

    public class GeoLocationAddressCompoEx
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }


    public class GeoLocationCountryEx
    {
        public List<GeoLocationAddressCompoEx> address_components { get; set; }
        public string formatted_address { get; set; }
        public List<object> geometry { get; set; }
        public string place_id { get; set; }
        public List<object> types { get; set; }
    }

    public class GeoLocationEx
    {
        public GeoLocationCountryEx results { get; set; }
    }

}
