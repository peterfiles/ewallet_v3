﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TaxCommissionEx    {
        public int id { get; set; }
        public string taxaccountno { get; set; }
        public string transactiontype { get; set; }
        public int transactionID { get; set; }
        public string transactionIDResponse { get; set; }
        public string timestampoftransaction { get; set; }
        public decimal commissionamount { get; set; }
        public string country { get; set; }
        public string currencyTransacted { get; set; }
        public bool isCoopRedTransaction { get; set; }
        public System.DateTime dateCreated { get; set; }
    }
}
