﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class PaymentRatesForPayinsEx
    {
        public int id { get; set; }
        public string country { get; set; }
        public string localPaymentService { get; set; }
        public string method { get; set; }
        public string currency { get; set; }
        public string mdr { get; set; }
        public string txFee { get; set; }
        public string txFeeCurrency { get; set; }
        public string remarks { get; set; }
        public List<PayInsBreakDownEx> d2 { get; set; }
    }

    public class PayInsBreakDownEx
    {
        public int id { get; set; }
        public string currency { get; set; }
        public string txfee { get; set; }
        public string txfeeCurrency { get; set; }
    }
}
