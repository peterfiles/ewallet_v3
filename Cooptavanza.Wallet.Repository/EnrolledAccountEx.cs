﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class EnrolledAccountEx
    {
        public int id { get; set; }
        public string currency { get; set; }
        public int currenycid { get; set; }
        public Nullable<bool> accountDefault { get; set; }
        public int userId { get; set; }
        public string userTID { get; set; }
        public string saveStatus { get; set; }
        public string accountnumber { get; set; }
    }
}
