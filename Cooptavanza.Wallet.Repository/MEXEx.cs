﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MEXEx
    {
        public List<MEXAmlStatusEx> MAS { get; set; }
        public List<MEXIncludeFeesEx> IF { get; set; }
        public List<MEXKycCheckEx> KC { get; set; }
        public List<MEXSndIdTypeEx> SIT { get; set; }
        public List<MEXSndRemitPurposeEx> SRP { get; set; }
    }
}
