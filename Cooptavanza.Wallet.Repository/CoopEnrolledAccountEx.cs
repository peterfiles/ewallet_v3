﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CoopEnrolledAccountEx
    {
        public int id { get; set; }
        public string currency { get; set; }
        public int currenycid { get; set; }
        public Nullable<bool> accountDefault { get; set; }
        public int userId { get; set; }
        public string userTID { get; set; }
        public string accountnumber { get; set; }
        public string roleID { get; set; }
    }

    public class AccountListEx
    {
        public decimal amount { get; set; }
        public string currencyAccountTo { get; set; }
        public string name { get; set; }
    }
}
