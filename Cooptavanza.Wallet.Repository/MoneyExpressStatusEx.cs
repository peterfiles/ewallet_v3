﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyExpressStatusEx
    {
        public string MerchantId{ get; set; }
        public string ExtensionData { get; set; }
        public string PageIndex{ get; set; }
        public string PageSize{ get; set; }
        public string ReferenceId{ get; set; }
        public string SearchEndDate{ get; set; }
        public string SearchStartDate{ get; set; }
        public string Signature{ get; set; }
        public string TransactionId{ get; set; }
    }
}
