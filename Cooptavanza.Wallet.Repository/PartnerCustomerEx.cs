﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class PartnerCustomerEx
    {

        public int id { get; set; }
        public string accountnumber { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string suffix { get; set; }
        public string email { get; set; }
        public string mobilenumber { get; set; }
        public string phonenumber { get; set; }
        public string tid { get; set; }
        public System.DateTime dateCreated { get; set; }
        public int partnerId { get; set; }
        

    }
}
