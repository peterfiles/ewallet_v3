﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXTransactionEx
    {
        public decimal Amount { get; set; }
        public decimal Commission { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OrderType { get; set; }
        public string PaymentType { get; set; }
        public string Phone { get; set; }
        public string Seller { get; set; }
        public string SellerAcctType { get; set; }
        public string Sku { get; set; }
        public string Source { get; set; }
        public string Status { get; set; }
        public string Time { get; set; }
        public long TxId { get; set; }

    }
}
