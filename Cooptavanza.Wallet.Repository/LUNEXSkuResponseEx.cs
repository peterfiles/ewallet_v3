﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Cooptavanza.Wallet
{
    public class LUNEXSkuResponseEx
    {
        public List<LUNEXAITUResponseEx> List { get; set; }
    }

    
    public class LEPListEx
    {
        [JsonProperty("List")]
        public LEPProductsEx List { get; set; }
    }

    
    public class LEPProductsEx
    {
        [JsonProperty("Product")]
        public List<LEPProductEx> Product { get; set; }
    }

    
    public class LEPProductEx
    {
        [JsonProperty("Amounts")]
        public LEPAmountsEx Amounts { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }
        [JsonProperty("DataItems")]
        public string DataItems { get; set; }
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Repl")]
        public string Repl { get; set; }

        [JsonProperty("Sku")]
        public string Sku { get; set; }

        [JsonProperty("SubProduct")]
        public string SubProduct { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }

    }

    
    public class LEPAmountsEx
    {
        [JsonProperty("Amount")]
        public List<LEPAmountEx> Amount { get; set; }
    }

    
    public class LEPAmountEx
    {
        [JsonProperty("Amt")]
        public string Amt { get; set; }

        [JsonProperty("Denom")]
        public string Denom { get; set; }

        [JsonProperty("DestAmt")]
        public string DestAmt { get; set; }

        [JsonProperty("DestCurr")]
        public string DestCurr { get; set; }

        [JsonProperty("Fee")]
        public string Fee { get; set; }

    }
}
