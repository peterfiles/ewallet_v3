﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXAmountsListResponseEx
    {
        public List<LUNEXAmountResponseEx> Amount { get; set; }
    }
}
