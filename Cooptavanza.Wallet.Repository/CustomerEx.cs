﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CustomerEx
    {
        public int id { get; set; }
        public string accountnumber { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string currency { get; set; }
        public System.DateTime birthdate { get; set; }
        public string email { get; set; }
        public string mobilenumber { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string username { get; set; }
        public string zipcode { get; set; }
        public Nullable<int> cityid { get; set; }
        public Nullable<int> profileimageId { get; set; }
        public string recentbillimageId { get; set; }
        public int currencyid { get; set; }
        public int stateid { get; set; }
        public int countryid { get; set; }
        public string city { get; set; }
        public string tid { get; set; }
        public string password { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string theme { get; set; }
        public int themeid { get; set; }
        public Nullable<int> partnerID { get; set; }
        public Nullable<bool> gender { get; set; }

        public bool isVerified { get; set; }
        public bool isInOFACList { get; set; }

        public string membertype { get; set; }

        public List<EnrolledAccountEx> EnrolledList { get; set; }
        public ImageBillEx ImageBill { get; set; }
        public ImageProfileEx ImageProfile { get; set; }
        public ImageIdCardEx ImageIdCard { get; set; }
        /*  public virtual country country { get; set; }
          public virtual currency currency1 { get; set; }
          [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
          public virtual ICollection<imagebill> imagebills { get; set; }
          [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
          public virtual ICollection<imageprofile> imageprofiles { get; set; }
          public virtual state state { get; set; }*/

        #region Append AccountList Detail
        public coopAccountListData coopAccountListData { get; set; }
        #endregion

        #region Append products selected
        public compiledProductData[] compiledProductData { get; set; }
        #endregion

        #region Append giverNewAccount
        public coopAccountListgiver[] coopAccountListgiver { get; set; }
        #endregion

    }

    public class AccountFundingEx
    {
        public int userID { get; set; }
        public string fromISO { get; set; }
        public string toISO { get; set; }
        public decimal fromAmount { get; set; }
        public decimal toAmount { get; set; }
    }

    public class CustomerMobileEx
    {
        public int userID { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string date_of_birth { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public bool gender { get; set; }
        public string last_name { get; set; }
        public string middle_name { get; set; }
        public string mobile { get; set; }
        public string zipcode { get; set; }

    }

}
