﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ResponseEx
    {
        public string id { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public DateTime date { get; set; }
    }
}
