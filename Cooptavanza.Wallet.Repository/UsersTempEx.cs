﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class UsersTempEx
    {
        public int id { get; set; }
        public string mobile_number { get; set; }
        public string OTP { get; set; }
        public string token_id { get; set; }
        public string email { get; set; }
        public string password { get; set; }

    }
}
