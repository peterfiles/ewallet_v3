﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TransactionLogEx
    {
        public int id { get; set; }
        public string moduleName { get; set; }
        public string log { get; set; }
        public Nullable<System.DateTime> logDate { get; set; }
        public Nullable<int> userId { get; set; }
        public Nullable<int> userRole { get; set; }
        public bool status { get; set; }
    }
}
