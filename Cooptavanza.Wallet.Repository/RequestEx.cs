﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class RequestListEx
    {
        public DateTime dateStart { get; set; }
        public DateTime dateEnd { get; set; }
        public string searchText { get; set; }
        public string partnerCode { get; set; }
        public string signature { get; set; }
        public DateTime dateRequest { get; set; }
        public string orderBy { get; set; }
        public int pageNo { get; set; }
        public int pageSize { get; set; }
        public string referenceNo { get; set; }
        public string transactionType { get; set; }
    }

    public class RequestStatusEx
    {
        public string partnerCode { get; set; }
        public string signature { get; set; }
        public string referenceNo { get; set; }
    }

    public class RequestQouteEx
    {
        public string partnerCode { get; set; }
        public string signature { get; set; }
        public string transactionType { get; set; }
    }

    public class RequestTopupWithSKU
    {
        public int id { get; set; }
        public string partnerCode { get; set; }
        public string signature { get; set; }
        public string transactionType { get; set; }
    }

    public class ProcessTopUpEx
    {
        public string accountNumber { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public string email { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string middlename { get; set; }
        public string mobilenumber { get; set; }
        public string partnerCode { get; set; }
        public string phoneNumber { get; set; }
        public string phonetoTopUp { get; set; }
        public string referenceNo { get; set; }
        public string signature { get; set; }
        public string skuid { get; set; }
        public string suffix { get; set; }
        public string transactionType { get; set; }

    }
}
