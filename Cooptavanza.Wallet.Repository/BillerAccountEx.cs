﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class BillerAccountEx
    {
        public int id { get; set; }
        public int billerID { get; set; }
        public int billerType { get; set; }
        public string billerBranch { get; set; }
        public string billerAccount1 { get; set; }
        public string billerTID { get; set; }
        public int countryID { get; set; }
        public int stateID { get; set; }
        public int currencyID { get; set; }
        public string currencyISO { get; set; }
    }
}
