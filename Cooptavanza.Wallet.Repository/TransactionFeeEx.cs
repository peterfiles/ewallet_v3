﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TransactionFeeEx
    {
        public int id { get; set; }
        public Nullable<int> transactionid { get; set; }
        public string transactiontid { get; set; }
        public Nullable<int> servicefeeID { get; set; }
        public Nullable<decimal> servicefee { get; set; }
        public Nullable<decimal> taxPercentage { get; set; }
        public Nullable<decimal> tax { get; set; }
        public Nullable<decimal> subtotal { get; set; }
        public Nullable<decimal> totalfee { get; set; }
        public Nullable<decimal> grandtotal { get; set; }
        public string currencyiso { get; set; }
        public Nullable<int> currencyid { get; set; }
        public string country { get; set; }
        public string status { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
    }
}
