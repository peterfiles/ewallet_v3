﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyExpressTransactionLimitRequestEx
    {
        public string MerchantId{ get; set; }
        public string Signature{ get; set; }
        public string TransactionId{ get; set; }
    }
}
