﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TransferMoneySwapEx
    {
        public int id { get; set; }
        public int customerid { get; set; }
        public string customertid { get; set; }
        public string currencyfromiso { get; set; }
        public int currencyfromid { get; set; }
        public string currencytoiso { get; set; }
        public int currencytoid { get; set; }
        public decimal amountto { get; set; }
        public decimal amountfrom { get; set; }
        public int transactiontype { get; set; }
        public string benificiaryfullname { get; set; }
        public string benificiaryemail { get; set; }
        public string benificiarymobileno { get; set; }
        public string sendermobileno { get; set; }
        public int benificiaryid { get; set; }
        public string benificiarytid { get; set; }
        public string transactionTID { get; set; }
        public DateTime? datecreated { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public int transactionid { get; set; }
        public string status { get; set; }
        public string accountNumber { get; set; }


    }
}
