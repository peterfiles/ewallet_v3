﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CBSWireTransferEx
    {
        //sender
        public string sndAccntNo { get; set; }
        public string sndFirstname { get; set; }
        public string sndLastname { get; set; }
        public string sndMobile { get; set; }
        public string sndEmail { get; set; }

        //bank
        public string swiftCode { get; set; }
        public string bankName { get; set; }
        public string bankAddress { get; set; }
        public string bankCountry { get; set; }
        public string routingNum { get; set; }

        //receiver
        public string rcvName { get; set; }
        public string rcvAddress { get; set; }
        public string rcvAcctNo { get; set; }
        public string refMessage { get; set; }

        //transfer details
        public decimal amountToTransfer { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> dataCreated { get; set; }

    }
}
