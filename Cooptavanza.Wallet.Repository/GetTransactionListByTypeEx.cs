﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class GetTransactionListByTypeEx
    {
         public string transactionType { get; set; }
         public DateTime dataCreated { get; set; }
         public string CustomerFullName { get; set; }
         public decimal balance { get; set; }
         public string description { get; set; }
         public string status { get; set; }
         public string currencyfromiso { get; set; }
         public string currencytoiso { get; set; }
         public string ReceiverFullname { get; set; }
        
    }

}
