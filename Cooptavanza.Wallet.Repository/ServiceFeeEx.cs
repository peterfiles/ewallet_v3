﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ServiceFeeEx
    {
        public int id { get; set; }
        public string serviceFeeType { get; set; }
        public Nullable<decimal> serviceFeeAmount { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
        public Nullable<int> productID { get; set; }
        public Nullable<decimal> serviceFeePercentage { get; set; }

    }

    public class CalculateChargesEx{
        public string currency { get; set; }
        public decimal mdr { get; set; }
        public decimal receiverAmount { get; set; }
        public string receiverCurrency { get; set; }
        public decimal senderAmount { get; set; }
        public string senderCurrency { get; set; }
        public decimal senderConvertedAmount { get; set; }
        public decimal txFee { get; set; }
    }

    public class CrEx //calculatedCommissionFees
    {
        public decimal amntToSend { get; set; }
        public decimal dmdr { get; set; } //deductedMerchantDiscountRate
        public decimal mcf { get; set; } //merchantCommissionFee
        public decimal txcf { get; set; } //Transaction total Fee
    }

    public class FeesEx
    {
        public string country { get; set; }
        public string currency { get; set; }
        public decimal mdr { get; set; }
        public decimal receiverAmount { get; set; }
        public string receiverCurrency { get; set; }
        public decimal senderAmount { get; set; }
        public decimal senderConvertedAmount { get; set; }
        public string senderCurrency { get; set; }
        public decimal txFee { get; set; }
    }

    public class serviceChargesAndCommissionsEx
    {
        public CrEx cR { get; set; }
        public FeesEx fees { get; set; }
    }


}
