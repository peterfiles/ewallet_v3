﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXSpeedDialEx
    {
        public string AccessPhone { get; set; }
        public string AccessPhoneInt { get; set; }
        public string Description { get; set; }
        public int Num { get; set; }
        public string Phone { get; set; }
        public string Region { get; set; }
        public string RegionInt { get; set; }

    }
}
