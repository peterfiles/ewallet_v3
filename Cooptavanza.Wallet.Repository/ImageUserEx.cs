﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ImageUserEx
    {
        public int id { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public int userRoles { get; set; }
        public int userId { get; set; }
        public string path { get; set; }
        public string imageString { get; set; }
    }
}
