﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyExpressBinEx
    {
        public string ExtensionData{ get; set; }
        public string MerchantId { get; set; }
        public string PageIndex { get; set; }
        public string PageSize { get; set; }
        public string Signature { get; set; }


    }
}
