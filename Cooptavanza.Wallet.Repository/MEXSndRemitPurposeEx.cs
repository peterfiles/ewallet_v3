﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MEXSndRemitPurposeEx
    {
        public int id { get; set; }
        public string code { get; set; }
        public string definition { get; set; }

    }
}
