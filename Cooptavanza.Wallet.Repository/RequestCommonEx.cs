﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class RequestCommonEx
    {
       public string signature { get; set; }
        public string idCode { get; set; }
        public string param1 { get; set; }
        public string param2 { get; set; }
        public string param3 { get; set; }
        public string param4 { get; set; }
        public int intparam1 { get; set; }
        public int intparam2 { get; set; }
        public int intparam3 { get; set; }
        public int intparam4 { get; set; }
        public object objparam1 { get; set; }
        public object objparam2 { get; set; }
    }

}
