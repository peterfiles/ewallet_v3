﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IPayOutPaymentRatesRepository : IRepository<PaymentRatesForPayoutsEx>
    {
        PaymentRatesForPayoutsEx GetByID(int id);
        List<PaymentRatesForPayoutsEx> GetByCountryName(string countryName);
        List<PaymentRatesForPayoutsEx> GetByCountryID(int id);
    }
}
