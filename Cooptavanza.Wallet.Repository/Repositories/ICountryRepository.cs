﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ICountryRepository : IRepository<CountryEx>
    {
        CountryEx Get(int Id);
        List<CountryEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<CountryEx> SearchCountry(string where, string orderBy);
        object getCountryFilterByAgent();
        CountryEx GetByCountryName(string cname);
        List<CountryEx> GetDefault();
        CountryEx GetByCurrencyCode(string curr);
        List<CountryEx> GetCountries();
    }
}
