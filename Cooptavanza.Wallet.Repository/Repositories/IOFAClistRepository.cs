﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cooptavanza.Wallet.Repository.Repositories
{
    public interface IOFAClistRepository : IRepository<OFAClistEx>
    {
        OFAClistEx Get(int Id);
        List<OFAClistEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
    }
}