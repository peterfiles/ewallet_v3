﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IImageBillsRepository : IRepository<ImageBillEx>
    {
        ImageBillEx Get(int Id);
        List<ImageBillEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        ImageBillEx GetByUserId(int id);
    }
}
