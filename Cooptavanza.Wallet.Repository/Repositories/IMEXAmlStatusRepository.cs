﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IMEXAmlStatusRepository
    {
        MEXAmlStatusEx Get(int Id);
        List<MEXAmlStatusEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        
    }
}
