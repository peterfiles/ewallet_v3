﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IUserAccountRepository : IRepository<UserAccountEx>
    {
        UserAccountEx Get(int Id);
        List<UserAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        UserAccountEx GetLogin(string mobilenumber, string password);
        UserAccountEx GetAuthentication(int v, string password, string tid, int id);
        UserAccountEx GetAdminAuthenticate(string username, string password);
        UserAccountEx GetAccountByUserId(int r, int id, string tid);
        List<MemberEx> GetAllMember(string str,int psize,int pno);
        List<MemberEx> GetAllMemberByParentID(string str, int psize, int pno, int userID);
        List<MemberEx> GetAllMemberByParentIDAndRoleID(string str, int psize, int pno, int userId, int roleID);

        int countmember(string str);
        int countmemberFilterByParentID(string str, int parentID);
        int countmemberFilterByParentIDAndRoleID(string str, int parentID, int roleID);

        AuthGlobalEx GetAccountByUserPassword(AuthMobileEx n);
        UserAccountEx CheckUsername(string username);
        UserAccountEx GetAdminAuthenticate(string username, string password, string corporatename);
    }
}
