﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ITransactionLogsRepository : IRepository<TransactionLogEx>
    {
        TransactionLogEx GetModuleName(string modeName);
        void SaveLog(string log, string moduleName);
        List<TransactionLogEx> GetPaged(string where, string orderBy, int pageNo, int pageSize);
    }
}
