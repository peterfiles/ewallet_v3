﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IServiceFeeRepository : IRepository<ServiceFeeEx>
    {
        ServiceFeeEx Get(int Id);
        List<ServiceFeeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize);
        int Count(int id, string TID, string where);
        ServiceFeeEx GetByProductID(int id);
        ServiceFeeEx GetByServiceFeeType(string name);
    }
}
