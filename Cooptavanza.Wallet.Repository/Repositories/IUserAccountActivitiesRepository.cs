﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IUserAccountActivitiesRepository : IRepository<UserAccountActivitiesEx>
    {
        List<UserAccountActivitiesEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        void SaveActivity(UserAccountActivitiesEx uaa);

    }
}
