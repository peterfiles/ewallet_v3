﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ITaxCommissionRepository : IRepository<TaxCommissionEx>
    {
        TaxCommissionEx Get(int Id);
        List<TaxCommissionEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
    }
}
