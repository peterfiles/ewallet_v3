﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ICommissionsRepository : IRepository<CommissionsEx>
    {
        List<CommissionsEx> GetCommissionListWithFilter(int userID, int userRole, string orderby, int PageNo, int PageSize);
        List<CommissionsEx> GetCommissionListWithFilterByUserIDRoleIDMethod(int userID, int userRole, string method, string orderby, int PageNo, int PageSize);
        List<TotalCommissionEx> getTotalCommissionByUserIDRoleIDAndMethod(int userID, int roleID, string method);
        CommissionsEx GetCommissionAccountNumberByMethodAndCurrency(string currency, string method);
        List<TotalCommissionByCurrencyEx> GetTotalCommByUidAndRidGroupByCurr(int uID, int rID);
    }
}
