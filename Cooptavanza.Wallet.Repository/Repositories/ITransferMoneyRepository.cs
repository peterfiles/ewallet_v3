﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ITransferMoneyRepository : IRepository<TransferMoneyEx>
    {
        TransferMoneyEx Get(int Id);
       // List<TransferMoneyEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize);
        List<TransferMoneyEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        //int Count(int id, string TID, string where);
        int Count(string where);
        TransferMoneyEx GetTransferMoney(string tk, int id);
        TransferMoneyEx GetTransferMoneyWithOtp(string o, string tk, int id);
        TransferMoneyEx GetTransferMoney(string tk, int id, int p);
        TransferMoneyEx GetTransferMoneyByTransactionTID(string tID);
    }
}
