﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IBankDepositRepository : IRepository<BankDepositEx>
    {
        BankDepositEx Get(int Id);
        List<BankDepositEx> GetPaged(string where, string orderby, int PageNo, int PageSize, int userID, int roleID);
        int Count(string where);
        List<BankDepositEx> GetAllCountry();
        List<BankDepositEx> GetBankNameByCountryID(int countryID);
        List<BankDepositEx> GetBankNameByCountryIDAndBankName(int countryID, string bankName);
        List<BankDepositEx> GetBankByCountry();
        List<BankDepositEx> CustomListGetPaged(string str, int psize, int pno);
        int CustomListGetPagedCount(string str);
        BankDepositEx GetByRtCode(string rtCode);
    }
}
