﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IImageProfileRepository : IRepository<ImageProfileEx>
    {
        ImageProfileEx Get(int Id);
        List<ImageProfileEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        ImageProfileEx GetByUserId(int id);
    }
}
