﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IBillerAccountRepository : IRepository<BillerAccountEx>
    {
        BillerAccountEx Get(int Id);
        List<BillerAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        BillerAccountEx GetBillerByAccountNumber(string bA);
        BillerAccountEx GetBillerByCountryStateBranchISO(int cID, int sID, string branch, string iso);
        int CountBillerAccount();

    }
}
