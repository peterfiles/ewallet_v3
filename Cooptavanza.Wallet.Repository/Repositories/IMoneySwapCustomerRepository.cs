﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IMoneySwapCustomerRepository : IRepository<MoneySwapCustomerEx>
    {
        MoneySwapCustomerEx Get(int Id);
        List<MoneySwapCustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        MoneySwapCustomerEx GetByMobileNumber(string mobileNumber);
        
        MoneySwapCustomerEx GetCheckMobileEmail(string str);
        MoneySwapCustomerEx GetCheckEmail(string str);
        MoneySwapCustomerEx SaveMoneySwapCustomer(MoneySwapCustomerEx item);
    }
}
