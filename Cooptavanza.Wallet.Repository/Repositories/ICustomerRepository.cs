﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ICustomerRepository : IRepository<CustomerEx>
    {
        CustomerEx Get(int Id);
        List<CustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        CustomerEx GetByMobileNumber(string mobileNumber);
        CustomerEx GetLogin(string mobilenumber, string password);
        CustomerEx GetCheckMobileEmail(string str);
        CustomerEx GetCheckEmail(string str);
        CustomerEx SaveCustomer(CustomerEx item);
        int CountCustomer();
    }
}
