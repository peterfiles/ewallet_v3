﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IUserThemeRepository : IRepository<UserThemeEx>
    {
        UserThemeEx Get(int Id);
        UserThemeEx GetByUserId(int Id);
    }
}
