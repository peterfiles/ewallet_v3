﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IPayInPaymentRatesRepository : IRepository<PaymentRatesForPayinsEx>
    {
        PaymentRatesForPayinsEx GetByID(int id);
        List<PaymentRatesForPayinsEx> GetByCountryID(int id);
        List<PaymentRatesForPayinsEx> GetByCountryName(string name);
        List<PaymentRatesForPayinsEx> GetByMethod(string method);
        List<PaymentRatesForPayinsEx> GetByLocalPaymentService(string localPaymentService);
    }
}
