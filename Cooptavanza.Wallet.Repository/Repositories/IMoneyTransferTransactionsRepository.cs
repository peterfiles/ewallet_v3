﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IMoneyTransferTransactionsRepository : IRepository<MoneyTransferTransactionsEx>
    {
        MoneyTransferTransactionsEx Get(int Id);
        List<MoneyTransferTransactionsEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize);
        int Count(int id, string TID, string where);
        object getMoneyTransferTransactionsByTrackingNumberAndPassword(string accountNumber, string password);
    }
}
