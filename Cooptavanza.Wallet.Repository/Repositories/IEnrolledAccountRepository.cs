﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IEnrolledAccountRepository : IRepository<EnrolledAccountEx>
    {
        EnrolledAccountEx Get(int Id);
        List<EnrolledAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<EnrolledAccountEx> GetByUserId(int id);
        List<EnrolledAccountEx> GetByIdAndISO(int id, string iso);
        int retID();
        EnrolledAccountEx GetByUserIDAndCurrencyID(int userID, string iso);
        EnrolledAccountEx GetByUserIDUserTIDAndCurrencyISO(int userID, string tID, string iso);
    }
}
