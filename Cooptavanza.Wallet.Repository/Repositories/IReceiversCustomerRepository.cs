﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IReceiversCustomerRepository : IRepository<ReceiversCustomerEx>
    {
        ReceiversCustomerEx Get(int Id);
        List<ReceiversCustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        ReceiversCustomerEx GetByMobileNumber(string mobileNumber);
        ReceiversCustomerEx GetLogin(string mobilenumber, string password);
        ReceiversCustomerEx GetCheckMobileEmail(string str);
        ReceiversCustomerEx GetCheckEmail(string str);
        ReceiversCustomerEx SaveCustomer(ReceiversCustomerEx item);
    }
}
