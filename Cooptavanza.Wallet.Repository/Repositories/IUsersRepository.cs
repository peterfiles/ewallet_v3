﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IUsersRepository : IRepository<UsersEx>
    {
        UsersEx Get(int Id);
        List<UsersEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        UsersEx GetLogin(string mobile_number, string password);
    }
}
