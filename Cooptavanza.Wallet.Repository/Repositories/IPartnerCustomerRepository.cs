﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IPartnerCustomerRepository : IRepository<PartnerCustomerEx>
    {
        PartnerCustomerEx Get(int Id);
        List<PartnerCustomerEx> GetPaged(string where, int partnerId, string orderby, int PageNo, int PageSize);
        int Count(string where, int partnerId);
        
    }
}
