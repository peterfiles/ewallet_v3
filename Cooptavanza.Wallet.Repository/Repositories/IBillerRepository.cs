﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IBillerRepository : IRepository<BillerEx>
    {
        BillerEx Get(int Id);
        List<BillerEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        object GetBillerCountry();
        object GetBillerTypeByCountry(int id);
        object GetBillerByCountryAndType(int cID, int bTID);


    }
}
