﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IOTPAccountRepository : IRepository<OTPAccountEx>
    {
        OTPAccountEx Get(int Id);
        List<OTPAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        OTPAccountEx GetLogin(string mobile_number, string OTP, DateTime dateTime);
        OTPAccountEx getByDUR(string deviceId, int id, int role);
        OTPAccountEx getByUR(int id, int role);
    }
}
