﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IImageUserRepository : IRepository<ImageUserEx>
    {
        ImageUserEx Get(int Id);
        List<ImageUserEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        ImageUserEx GetByUserId(int id, int r);
    }
}
