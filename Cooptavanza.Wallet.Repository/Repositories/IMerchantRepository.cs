﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IMerchantRepository : IRepository<MerchantEx>
    {
        MerchantEx Get(int Id);
        List<MerchantEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        int CountMerchant();
    }
}
