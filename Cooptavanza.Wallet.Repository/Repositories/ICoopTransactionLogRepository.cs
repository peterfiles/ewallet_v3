﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ICoopTransactionLogRepository : IRepository<CoopTransactionLogEx>
    {
        CoopTransactionLogEx Get(int Id);
        List<CoopTransactionLogEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<CoopTransactionListEx> getCoopTransactionList(string str, int psize, int pno, int userID, int roleID);
        int getcountTransactionListUserIDAndRoleID(string str, int userID, int roleID);
        CoopTransactionLogEx GetByTransactionTID(string tid);
    }
}
