﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IMEXSndRemitPurposeRepository 
    {
        MEXSndRemitPurposeEx Get(int Id);
        List<MEXSndRemitPurposeEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        
    }
}
