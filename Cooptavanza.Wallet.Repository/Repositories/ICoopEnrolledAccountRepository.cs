﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ICoopEnrolledAccountRepository : IRepository<CoopEnrolledAccountEx>
    {
        CoopEnrolledAccountEx Get(int Id);
        List<CoopEnrolledAccountEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        string getAccountNumberForCoopRed(int userID, int currID, int roleID, int coopEnrolledID);
        List<AccountListEx> getCoopAccountList(int userID, int roleID);
        CoopEnrolledAccountEx getIdByUserIDRoleIDCurrency(int userID, int roleID, string iso);
    }
}
