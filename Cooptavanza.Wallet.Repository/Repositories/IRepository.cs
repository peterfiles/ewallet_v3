﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repository.Repositories
{
    public interface IRepository<T> where T : class
    {
        T Create();
        void Save(T item);
        void Remove(T item);
    }
}