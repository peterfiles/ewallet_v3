﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IConvertedCurrencyRepository : IRepository<ConvertedCurrencyEx>
    {
        ConvertedCurrencyEx Get(string quote);
        int Count();
        object RefreshConvertedCurrency();
        ConvertedCurrencyEx getDataByQoute(string qoute);
    }
}
