﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;


namespace Cooptavanza.Wallet.Repository.Repositories
{
    public class IRepositoryEF<T> : IRepository<T> where T : class
    {
        private readonly DbSet<T> dbSet;
        private readonly DbContext db;
        public IRepositoryEF(DbContext db)
        {
            this.db = db;
            dbSet = db.Set<T>();
        }
        public T Create()
        {
            return dbSet.Create();
        }
        public void Add(T item)
        {
            dbSet.Add(item);
        }
        public void Update(T item)
        {
            dbSet.Attach(item);
            db.Entry(item).State = EntityState.Modified;
        }
        public void Save(T item)
        {
            dbSet.Attach(item);
            db.Entry(item).State = EntityState.Modified;
        }
        public void Remove(T item)
        {
            dbSet.Remove(item);
        }
        public void SaveChanges()
        {
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException updateEx)
            {
                Exception ex = updateEx.InnerException;
                while (ex.InnerException != null)
                {
                    ex = ex.InnerException;
                }
                throw ex;
            }
            catch (DbEntityValidationException dbEx)
            {
                List<dynamic> aErrors = new List<dynamic>();
                foreach (var eve in dbEx.EntityValidationErrors)
                {
                    var error = new { entity = eve.Entry.Entity.GetType().Name, state = eve.Entry.State, properties = new List<dynamic>() };
                    foreach (var ve in eve.ValidationErrors)
                    {
                        var p = new { property = ve.PropertyName, message = ve.ErrorMessage };
                        error.properties.Add(p);
                    }
                    aErrors.Add(error);
                }
                throw dbEx;
            }
        }
        public IQueryable<T> Query()
        {
            return dbSet;
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}