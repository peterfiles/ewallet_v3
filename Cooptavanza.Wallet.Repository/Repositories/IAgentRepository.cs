﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IAgentRepository : IRepository<AgentsEx>
    {
        AgentsEx Get(int Id);
        List<AgentsEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<AgentsEx> getAgentByCountryIDAndStateID(int cID, int sID);
        int CountAgents();
    }
}
