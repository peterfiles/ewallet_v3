﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ITransactionFeeRepository : IRepository<TransactionFeeEx>
    {
        TransactionFeeEx Get(int Id);
        List<TransactionFeeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize);
        int Count(int id, string TID, string where);
    }
}
