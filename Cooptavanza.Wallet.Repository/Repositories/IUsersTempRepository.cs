﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IUsersTempRepository : IRepository<UsersTempEx>
    {
        UsersTempEx Get(int Id);
        List<UsersTempEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        UsersTempEx GetLogin(string mobile_number, string password);
    }
}
