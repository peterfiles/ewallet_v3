﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IStatesRepository : IRepository<StatesEx>
    {
        StatesEx Get(int Id);
        List<StatesEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<StatesEx> GetStatesByCountryId(int id);
        StatesEx GetByStateName(string sname);
        object getAgentStatesByCountryID(int id);
    }
}
