﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IMoneyTransferCustomerRepository : IRepository<MoneyTransferCustomerEx>
    {
        MoneyTransferCustomerEx Get(int Id);
        List<MoneyTransferCustomerEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        MoneyTransferCustomerEx GetByMobileNumber(string mobileNumber);
       
        MoneyTransferCustomerEx GetCheckMobileEmail(string str);
        MoneyTransferCustomerEx GetCheckEmail(string str);
        MoneyTransferCustomerEx SaveMoneyTransferCustomer(MoneyTransferCustomerEx item);
    }
}
