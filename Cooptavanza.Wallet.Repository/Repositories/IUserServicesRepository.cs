﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IUserServicesRepository : IRepository<UserServicesEx>
    {
        UserServicesEx Get(int Id);
        List<UserServicesEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<UserServicesEx> GetByUserID(int Id);
        List<UserServicesEx>  GetByUserIDAndRoleID(int Id, int roleID);
    }
}
