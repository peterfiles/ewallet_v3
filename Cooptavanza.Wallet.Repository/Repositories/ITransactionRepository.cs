﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ITransactionRepository : IRepository<TransactionEx>
    {
        TransactionEx Get(int Id);
        List<TransactionEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize);
        int Count(int id, string TID, string where);
        List<BalanceEx> GetBalance(int id);
        List<BalanceEx> GetBalancePending(int id);
        List<BalanceEx> GetBalanceOverall(int id);
        int GetTransactionLastIDNumber();
        List<GetTypeTransactionEx> GetTransactionType();
        List<GetTransactionListByTypeEx> GetTransactionFilterByTransactionType(string tType,string status);
        List<TransactionEx> GetFilterTransactionPaged(string where, string orderby, int PageNo, int PageSize);
        List<TransactionEx> GetTransactionByType(string SearchText, string orderby, int PageNo, int PageSize);
        List<TransactionEx> GetByPartner(string searchText, int partnerId, string dateStart, string dateEnd, string transactionType, string referenceCode, string orderBy, int pageNo, int pageSize);
        int CountByPartner(string v, int partnerId, string dateStart, string dateEnd, string transactionType, string referenceCode);
        TransactionEx getStatusByPartner(int id, string referenceNo);
        bool CheckTransactionId(string referenceNo, int partnerId);
        List<TransactionEx> GetByPartnerIDAndTransactionType(int userID, int roleID, int PageNo, int PageSize, string ttype);
        List<ChartsEx> GetChartsList(DateTime d1, DateTime d2);
        TransactionEx GetTransactionListByTID(string tid);
        List<TransactionEx> GetTransactionByTID(string tid, string orderby, int PageNo, int PageSize);
    }
}

