﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IPaymentBreakdownRepository : IRepository<PaymentBreakdownEx>
    {
        PaymentBreakdownEx Get(int Id);
        List<PaymentBreakdownEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        List<PaymentBreakdownEx> GetCommissionByPaymentBreakdown(int uID, int rID);
        List<PaymentBreakdownEx> GetPaymentBreakdownByTransactionID(string transactionID, string orderby, int PageNo, int PageSize);

    }
}
