﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface IProductsRepository : IRepository<ProductsEx>
    {
        ProductsEx Get(int Id);
        List<ProductsEx> GetPaged(string where, string orderby, int PageNo, int PageSize);
        int Count(string where);
        
    }
}
