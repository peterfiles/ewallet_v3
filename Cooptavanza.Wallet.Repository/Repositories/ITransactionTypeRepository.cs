﻿using Cooptavanza.Wallet.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet.Repositories
{
    public interface ITransactionTypeRepository : IRepository<TransactionTypeEx>
    {
        TransactionTypeEx Get(int Id);
        List<TransactionTypeEx> GetPaged(int id, string TID, string where, string orderby, int PageNo, int PageSize);
        int Count(int id, string TID, string where);
        TransactionTypeEx GetByCode(string code);
    }
}
