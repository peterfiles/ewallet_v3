﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class PartnersEx
    {
        public int id { get; set; }
        public string finame { get; set; }
        public string currency { get; set; }
        public System.DateTime expirationDate { get; set; }
        public Nullable<int> countryid { get; set; }
        public string mobilenumber { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string license { get; set; }
        public Nullable<int> stateid { get; set; }
        public string zipcode { get; set; }
        public Nullable<int> cityid { get; set; }
        public string servicetype { get; set; }
        public string tid { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public int currencyid { get; set; }
        public string partnerType { get; set; }
        public string membertype { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string partnerCode { get; set; }
        public string key { get; set; }
        public Nullable<int> masterPartnerID { get; set; }
        public string accountNumber { get; set; }

        #region Append AccountList Detail
        public coopAccountListData coopAccountListData {get;set;}
        #endregion

        #region Append products selected
        public compiledProductData[] compiledProductData { get; set; }
        #endregion

        #region Append giverNewAccount
        public coopAccountListgiver[] coopAccountListgiver { get; set; }
        #endregion

        // public virtual country country1 { get; set; }
        //  public virtual state state1 { get; set; }
    }

    public class coopAccountListAndCurrencyData
    {
        public decimal amount { get; set; }
        public string status { get; set; }
        public int index { get; set; }
        public CurrencyEx currency { get; set; }
    }
    
    public class coopRootAccountUserIDAndRole
    {
        public int loggedID { get; set; }
        public int roleID { get; set; }
    }

    public class coopAccountListData
    {
        public List<coopAccountListAndCurrencyData> coopAccountListAndcurencyData { get; set; }
        public coopRootAccountUserIDAndRole coopRootAccountUserIDAndRole { get; set;}
    }

    public class compiledProductsToFilter
    {
        public int id { get; set; }
        public string status { get; set; }
    }

    public class compiledProductData
    {
        public compiledProductsToFilter compiledProductsToFilter { get; set; }
    }

    public class coopAccountListgiver
    {
        public string name { get; set; }
        public string currencyAccountTo { get; set; }
        public decimal amount { get; set; }
    }
}
