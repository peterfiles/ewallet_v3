﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXNewOrderResponseEx
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public DateTime Time { get; set; }
        public string Merchant { get; set; }
        public List<Object> List { get; set; }

    }
}
