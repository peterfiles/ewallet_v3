﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class UserThemeEx
    {
        public int id { get; set; }
        public string theme { get; set; }
        public int userid { get; set; }
        public int roles { get; set; }
    }
}
