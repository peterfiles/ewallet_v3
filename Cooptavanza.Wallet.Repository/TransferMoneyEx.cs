﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TransferMoneyEx
    {
        public int id { get; set; }
        public int customerid { get; set; }
        public string customertid { get; set; }
        public string currencyfromiso { get; set; }
        public int currencyfromid { get; set; }
        public string currencytoiso { get; set; }
        public int currencytoid { get; set; }
        public decimal amountto { get; set; }
        public decimal amountfrom { get; set; }
        public string transactionType { get; set; }
        public string benificiaryfullname { get; set; }
        public string benificiaryemail { get; set; }
        public string benificiarymobileno { get; set; }
        public string sendermobileno { get; set; }
        public int benificiaryid { get; set; }
        public string benificiarytid { get; set; }
        public string transactionTID { get; set; }
        public DateTime? datecreated { get; set; }
        public string question { get; set; }
        public string answer { get; set; }
        public int transactionid { get; set; }
        public string status { get; set; }
        public string accountNumber { get; set; }


        public Nullable<System.DateTime> dateCreated { get; set; }
        public string senderFirstName { get; set; }
        public string senderLastName { get; set; }
        public Nullable<int> partnerId { get; set; }
        public string partnerName { get; set; }
        public string senderMiddleName { get; set; }
        public string sendersuffix { get; set; }
        public string receiverFirstname { get; set; }
        public string receiverLastname { get; set; }
        public string receiverMiddlename { get; set; }
        public string receiverSuffix { get; set; }
        public Nullable<int> transactionId { get; set; }


    }

    public class AdminETransferEx {
        public int userID { get; set; }
        public int roleID { get; set; }
        public string currencyFrom { get; set; }
        public string currencyTo { get; set; }
        public string amount { get; set; }
        public string convertedAmount { get; set; }
        public string remainingBalance { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string transactionType { get; set; }
        public string senderFullname { get; set; }
        public string senderMobileNo { get; set; }
        public string receiverFullName { get; set; }
        public string answer { get; set; }
        public string question { get; set; }
        public serviceChargesAndCommissionsEx scac { get; set; }
    }
}
