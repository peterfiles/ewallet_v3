﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXCustomerAccountEx
    {
        public decimal Balance { get; set; }
        public string City { get; set; }
        public DateTime Created { get; set; }
        public string Currency { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal MinBalance { get; set; }
        public object Notes { get; set; }
        public string Phone { get; set; }
        public decimal RefillAmt { get; set; }
        public string Sku { get; set; }
        public string State { get; set; }
        public string StatementOption { get; set; }
        public string Street { get; set; }
        public string ZipCode { get; set; }

    }
}
