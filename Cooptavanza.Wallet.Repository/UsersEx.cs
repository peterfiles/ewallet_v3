﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class UsersEx
    {
        public int id { get; set; }
        public string accountnumber { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int currency { get; set; }
        public System.DateTime birthdate { get; set; }
        public string email { get; set; }
        public Nullable<int> countryid { get; set; }
        public string mobilenumber { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string url { get; set; }
        public Nullable<int> stateid { get; set; }
        public string zipcode { get; set; }
        public Nullable<int> cityid { get; set; }
        public Nullable<System.DateTime> date_created { get; set; }
        public Nullable<System.DateTime> date_update { get; set; }
        public string password { get; set; }
        public string username { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public string tid { get; set; }
        public string membertype { get; set; }
        public int defaultPartner { get; set; }
  //      public virtual country country1 { get; set; }
   //     public virtual state state1 { get; set; }

    }
}
