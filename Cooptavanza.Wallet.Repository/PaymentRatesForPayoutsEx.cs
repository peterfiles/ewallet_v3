﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class PaymentRatesForPayoutsEx
    {
        public int id { get; set; }
        public string country { get; set; }
        public string localPaymentService { get; set; }
        public string method { get; set; }
        public string currency { get; set; }
        public Nullable<decimal> mdr { get; set; }
        public Nullable<decimal> txFee { get; set; }
        public string remarks { get; set; }
    }
}
