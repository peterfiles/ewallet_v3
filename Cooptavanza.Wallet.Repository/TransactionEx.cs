﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TransactionEx
    {
        public int id { get; set; }
        public string transactionType { get; set; }
        public Nullable<System.DateTime> dataCreated { get; set; }
        public bool debit { get; set; }
        public bool credit { get; set; }
        public int userId { get; set; }
        public string userTID { get; set; }
        public string transactionTID { get; set; }
        public decimal balance { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        public string currencyfromiso { get; set; }
        public string currencytoiso { get; set; }
        public Nullable<int> enrolledAccountsID { get; set; }
        public Nullable<int> enrolledAccountsCurrencyID { get; set; }
        public string accountNumber { get; set; }
        public Nullable<int> receiversID { get; set; }
        public Nullable<int> partnerId { get; set; }

    }
}
