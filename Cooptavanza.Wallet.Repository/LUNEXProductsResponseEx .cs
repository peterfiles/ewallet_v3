﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXProductResponseEx
    {
        public List<LUNEXAmountsListResponseEx> Amounts { get; set; }
        public int Countrycode { get; set; }
        public int MaxLen { get; set; }
        public int MinLen { get; set; }
        public string Name { get; set; }
        public string Repl { get; set; }
        public string Sku { get; set; }
        public string Type { get; set; }
    }
}
