﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXEntityAccountEx
    {
        public string AcctType { get; set; }
        public string Balance { get; set; }
        public string Currency { get; set; }

    }
}
