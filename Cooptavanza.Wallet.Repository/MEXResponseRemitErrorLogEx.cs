﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MEXResponseRemitErrorLogEx
    {
        public int id { get; set; }
        public string ActionCode { get; set; }
        public string QuoteAmt { get; set; }
        public string QuoteCurr { get; set; }
        public string RcvAccntBankCode { get; set; }
        public string RcvAccntBankName { get; set; }
        public string RcvAccntNo { get; set; }
        public string RcvFirstName { get; set; }
        public string RcvLastName { get; set; }
        public string ReferenceId { get; set; }
        public string RespCode { get; set; }
        public string RespMessage { get; set; }
        public string Signature { get; set; }
        public string SubmitTime { get; set; }
        public string TransactionId { get; set; }
        public string MEXType { get; set; }
    }
}
