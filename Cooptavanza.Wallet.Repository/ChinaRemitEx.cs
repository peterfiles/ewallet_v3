﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ChinaRemitEx
    {
        public string amlStatus {get;set;}
        public string floatCurrency { get; set; }
      //  public string IncludeFees { get; set; }
        public string kioskAddress { get; set; }
        public string kioskName { get; set; }
        public string kycCheck { get; set; }
        public string note { get; set; }
        public string originatorUID { get; set; }
        public string partnerCode { get; set; }
        //reciever part
        public string rcvAccntNo { get; set; }
        public string rcvAccntType { get; set; }
        public string rcvFirstName { get; set; }
        public string rcvMiddleName { get; set; }
        public string rcvSuffix { get; set; }
        public string rcvIdNumber { get; set; }
        public string rcvIdType { get; set; }
        public string rcvLastName { get; set; }
        public string rcvMobileNo { get; set; }
        public string rcvEmail { get; set; }
       // public string ReferenceId { get; set; }
        public string referenceNo { get; set; }

        //sender
        public string currencyFrom { get; set; }
        public string currencyTo { get; set; }
        public decimal amountFrom { get; set; }
        public decimal amountTo { get; set; }
        public string sndAccntNo { get; set; }
        public string sndCity { get; set; }
        public string sndCountry { get; set; }
        public string sndState { get; set; }
        public string sndStreet { get; set; }
        public string sndFirstName { get; set; }
        public string sndIdNo { get; set; }
        public string sndIdType { get; set; }
        public string sndKycUrl { get; set; }
        public string sndLastName { get; set; }
        public string sndMiddleName { get; set; }
        public string sndSuffix { get; set; }
        public string sndPhoneNo { get; set; }
        public string sndRemitPurpose { get; set; }
        public string sndEmail { get; set; } // added for documentation purpose on ewallet instance.
        public string answer { get; set; }// added for q.a purpose on ewallet instance.
        
        //teller
        public string tellerFirstName { get; set; }
        public string tellerLastName { get; set; }
        public string tranAmt { get; set; }
        public string tranCurr { get; set; }
        public string validateReferenceId { get; set; }
        public string signature { get; set; }

        //on this instance
        public string transactionType { get; set; }
        public decimal serviceFee { get; set; }
        public DateTime date { get; set; }

        public int id { get; set; }
        public string MEXType { get; set; }
    }
}
