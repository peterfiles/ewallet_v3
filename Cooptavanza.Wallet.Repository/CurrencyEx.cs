﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CurrencyEx
    {
        public int id { get; set; }
        public string iso { get; set; }
        public string name { get; set; }
        public string numeric_code { get; set; }
        public string minor_unit { get; set; }
        public bool isDefault { get; set; }
    }

    public class ApiLayerResEx
    {
        public string timestamp { get; set; }
        public string source { get; set; }
        public string quotes { get; set; }
    }
}
