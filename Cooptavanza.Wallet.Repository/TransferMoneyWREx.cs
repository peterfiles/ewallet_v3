﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class TransferMoneyWREx
    {
        public TransferMoneyEx tm { get; set; }
        public ReceiversCustomerEx rc { get; set; }
        public CustomerEx ce { get; set; }
        public int agentID { get; set; }
        public decimal sendersAmount { get; set; }
        public decimal receiversAmount { get; set; }
        public string memberType { get; set; }
        public string sCountryName { get; set; }
        public string sStateName { get; set; }
        public string rCountryName { get; set; }
        public string rStateName { get; set; }
        public serviceChargesAndCommissionsEx serviceChargesAndCommissions { get; set; }
    }

    public class TransferMoneyMobileEx
    {
        public int customerID { get; set; }
        public string accountCurrISO { get; set; }
        public decimal amount { get; set; }
        public string amountConverted { get; set; }
        public string rCurrISO { get; set; }
        public string rEmail { get; set; }
        public string rMobileNo { get; set; }
        public string rSecretCode { get; set; }//password
        public string rFirstName { get; set; }
        public string rMiddleName { get; set; }
        public string rLastName { get; set; }
        public int rCountryID { get; set; }
        public int rStateID { get; set; }
        public string rCity { get; set; }
        public string rAddress { get; set; }
        public string rPostalCode { get; set; }
    }
}
