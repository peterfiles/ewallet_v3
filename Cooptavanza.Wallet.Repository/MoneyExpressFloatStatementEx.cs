﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyExpressFloatStatementEx
    {
        public string ExtensionData{ get; set; }
            public string FloatCurrency{ get; set; }
            public string MerchantId{ get; set; }
            public string PageIndex{ get; set; }
            public string PageSize{ get; set; }
            public string SearchEndDate{ get; set; }
            public string SearchStartDate{ get; set; }
            public string Signature{ get; set; }
            
        
    }
}
