﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cooptavanza.Wallet.Repository
{
    public class OFAClistEx
    {
        public int id { get; set; }
        public Nullable<int> ofac_sdn_ent_num { get; set; }
        public string ofac_sdn_name { get; set; }
        public string ofac_sdn_type { get; set; }
        public string ofac_sdn_program { get; set; }
        public string ofac_sdn_title { get; set; }
        public string ofac_sdn_call_sign { get; set; }
        public string ofac_sdn_vess_type { get; set; }
        public string ofac_sdn_tonnage { get; set; }
        public string ofac_sdn_grt { get; set; }
        public string ofac_sdn_vess_flag { get; set; }
        public string ofac_sdn_vess_owner { get; set; }
        public string ofac_sdn_remarks { get; set; }
        public int userID { get; set; }
        public int userCode { get; set; }
        public bool userIsVerified { get; set; }
        public bool isCompany { get; set; }
    }
}