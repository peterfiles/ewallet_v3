﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXRegisterPhoneEx
    {
        public string AccessPhone { get; set; }
        public string Description { get; set; }
        public bool IsMain { get; set; }
        public string Language { get; set; }
        public string Phone { get; set; }
        public string PhoneType { get; set; }

    }
}
