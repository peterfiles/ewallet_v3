﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class AuthMobileEx
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string deviceId { get; set; }
        public System.DateTime dateTime { get; set; }
        public System.DateTime mobileUTC { get; set; }

    }
}
