﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ProductsEx
    {
        public int id { get; set; }
        public string name { get; set; }
        public string code { get; set; }
        public string tokenid { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> percentage { get; set; }
        public string check {get;set;}
        public string saveStatus { get; set; }
        public int serviceId { get; set; }

    }
    
    public class ChartsEx
    {
        public string productName { get; set; }
        public int transacted { get; set; }
    }
}
