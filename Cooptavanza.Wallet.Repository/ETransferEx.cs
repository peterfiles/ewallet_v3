﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ETransferEx
    {
      
        public decimal amountConverted { get; set; }
        public decimal amount { get; set; }
        public string answer { get; set; }
        public string currencyFrom { get; set; }
        public string currencyTo { get; set; }
        public DateTime date { get; set; }
        public string partnerCode { get; set; }
        public string question { get; set; }
        public string rcvAddress { get; set; }
        public string rcvCity { get; set; }
        public string rcvCountry { get; set; }
        public string rcvEmail { get; set; }
        public string rcvFirstName { get; set; }
        public string rcvLastName { get; set; }
        public string rcvMiddleName { get; set; }
        public string rcvMobileno { get; set; }
        public string rcvPostalCode { get; set; }
        public string rcvStates { get; set; }
        public string rcvSuffix { get; set; }
        public string referenceNo { get; set; }
        public decimal serviceFee { get; set; }
        public string sndAccntNo { get; set; }
        public string sndAddress { get; set; }
        public string sndCity { get; set; }
        public string sndCountry { get; set; }      
        public string sndEmail { get; set; }
        public string sndFirstName { get; set; }
        public string sndLastName { get; set; }
        public string sndMiddleName { get; set; }
        public string sndMobileNo { get; set; }
        public string sndPhoneNo { get; set; }
        public string sndPostalCode { get; set; }
        public string sndStates { get; set; }
        public string sndSuffix { get; set; }
        public string transactionType { get; set; }
        public string signature { get; set; }

    }
}
