﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyExpressRemitEx
    {
        public string AmlStatus {get;set;}
        public string FloatCurrency { get; set; }
        public string IncludeFees { get; set; }
        public string KioskAddress { get; set; }
        public string KioskName { get; set; }
        public string KycCheck { get; set; }
        public string MerchantId { get; set; }
        public string Note { get; set; }
        public string OriginatorUID { get; set; }
        //reciever part
        public string RcvAccntNo { get; set; }
        public string RcvAccntType { get; set; }
        public string RcvFirstName { get; set; }
        public string RcvIdNumber { get; set; }
        public string RcvIdType { get; set; }
        public string RcvLastName { get; set; }
        public string ReferenceId { get; set; }

        //sender
        public string SndAddress { get; set; }
        public string SndFirstName { get; set; }
        public string SndIdNo { get; set; }
        public string SndIdType { get; set; }
        public string SndKycUrl { get; set; }
        public string SndLastName { get; set; }
        public string SndPhoneNo { get; set; }
        public string SndRemitPurpose { get; set; }
        public string SndEmail { get; set; } // added for documentation purpose on ewallet instance.
        public string answer { get; set; }// added for q.a purpose on ewallet instance.

        //teller
        public string TellerFirstName { get; set; }
        public string TellerLastName { get; set; }
        public string TranAmt { get; set; }
        public string TranCurr { get; set; }
        public string ValidateReferenceId { get; set; }
        public string Signature { get; set; }

        public int id { get; set; }
        public string MEXType { get; set; }
    }
}
