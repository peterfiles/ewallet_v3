﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXAmountResponseEx
    {
       public decimal Amt { get; set; }
       public string Denom { get; set; }
       public decimal DestAmt { get; set; }
       public string DestCurr { get; set; }
       public object Instruction { get; set; }
    }
}
