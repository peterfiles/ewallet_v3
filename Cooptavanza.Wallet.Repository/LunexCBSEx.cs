﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LunexCBSEx
    {
        public decimal amount { get; set; }
        public string billAccountNo { get; set; }
        public string currency { get; set; }
        public DateTime date { get; set; }
        public string denomanation { get; set; }
        public string partnerCode { get; set; }
        public string product { get; set; }
        public string referenceNo { get; set; }
        public decimal serviceFee { get; set; }
        public string signature { get; set; }
        public string sndAccntNo { get; set; }
        public string sndAddress { get; set; }
        public string sndCity { get; set; }
        public string sndCountry { get; set; }
        public string sndEmail { get; set; }
        public string sndFirstName { get; set; }
        public string sndLastName { get; set; }
        public string sndMiddleName { get; set; }
        public string sndMobileNo { get; set; }
        public string sndPhoneNo { get; set; }
        public string sndPostalCode { get; set; }
        public string sndStates { get; set; }
        public string sndSuffix { get; set; }
        public string transactionType { get; set; }
    }
}
