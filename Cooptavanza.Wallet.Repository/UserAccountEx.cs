﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class UserAccountEx
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public int roles_id { get; set; }
        public string password { get; set; }
        public string tID { get; set; }
        public string username { get; set; }
        public string corporatename { get; set; }

    }
}
