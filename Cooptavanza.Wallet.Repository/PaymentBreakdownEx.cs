﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class PaymentBreakdownEx
    {
        public int id { get; set; }
        public int userID { get; set; }
        public int userRole { get; set; }
        public string transactionTID { get; set; }
        public string currencyISO { get; set; }
        public int currencyID { get; set; }
        public decimal amount { get; set; }
        public string mdr { get; set; }
        public string txFee { get; set; }
        public string txFeeCurrency { get; set; }
        public string remarks { get; set; }
        public Nullable<bool> isMerchant { get; set; }
        public Nullable<System.DateTime> dateCreated { get; set; }
        public string transactionType { get; set; }

    }

}
