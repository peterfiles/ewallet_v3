﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class OTPGeneratedEx
    {
        public string otp { get; set; }
        public string device_id { get; set; }
        public string message { get; set; }

    }
}
