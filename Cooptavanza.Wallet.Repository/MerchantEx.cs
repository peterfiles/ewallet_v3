﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MerchantEx
    {
        public int id { get; set; }
        public string accountnumber { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string currency { get; set; }
        public string email { get; set; }
        public Nullable<int> countryid { get; set; }
        public string mobilenumber { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string url { get; set; }
        public Nullable<int> stateid { get; set; }
        public string zipcode { get; set; }
        public string username { get; set; }
        public Nullable<int> cityid { get; set; }
        public string tid { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string state { get; set; }
        public bool isInOFACList { get; set; }
        public bool isVerified { get; set; }
        public int currencyid { get; set; }
        public Nullable<int> masterPartnerID { get; set; }
        public string name { get; set; }
        public string membertype { get; set; }
        public string password { get; set; }
        public int[] selectedProductsID { get; set; }
        public string code { get; set; }

        #region Append AccountList Detail
        public coopAccountListData coopAccountListData { get; set; }
        #endregion

        #region Append products selected
        public compiledProductData[] compiledProductData { get; set; }
        #endregion

        #region Append giverNewAccount
        public coopAccountListgiver[] coopAccountListgiver { get; set; }
        #endregion
        //   public virtual country country1 { get; set; }
        //    public virtual state state1 { get; set; }
    }
}
