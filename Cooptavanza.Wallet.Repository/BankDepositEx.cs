﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class BankDepositEx
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int userRole { get; set; }
        public DateTime dateCreated { get; set; }
        public int countryid { get; set; }
        public string bankName { get; set; }
        public string currenciesISO { get; set; }
        public int currenciesID { get; set; }
        public string rtcode { get; set; }

        public string countryName { get; set; }
        public string memberTypeName { get; set; }
    }

    public class BankDepositACHTestEx
    {
        public string sendersEmail { get; set; }
        public string fullname { get; set; }
        public string receivername { get; set; }
        public string mobilenumber { get; set; }
        public string bankName { get; set; }
    }

    public class ACHTransactEx
    {
        public string ach_account_no { get; set; }
        public string ach_receive_name { get; set; }
        public decimal amount_purchase { get; set; }
        public decimal amountto { get; set; }
        public string bankName { get; set; }
        public DateTime checkDate { get; set; }
        public string currencyfromiso { get; set; }
        public string fullname { get; set; }
        public bool isDefaultAddress { get; set; }
        public string mobilenumber { get; set; }
        public string rtcode { get; set; }
        public string sendersEmail { get; set; }
        public int userID { get; set; }
    }
}
