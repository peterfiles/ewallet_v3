﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class UserAccountActivitiesEx
    {
        public int id { get; set; }
        public int userId { get; set; }
        public int userRole { get; set; }
        public System.DateTime dateCreated { get; set; }
        public bool status { get; set; }
        public string token_id { get; set; }
        public string name { get; set; }
        public string type { get; set; }
    }
}
