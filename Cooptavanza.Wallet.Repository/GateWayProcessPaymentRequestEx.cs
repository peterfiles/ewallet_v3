﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Cooptavanza.Wallet.Repository
{
    public class GateWayProcessPaymentRequestEx
    {
        public int sid { get; set; }
        public string rcode { get; set; }
        //udetailsStart
        #region Udetails;
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string suburb_city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string uip { get; set; }
        #endregion Udetails:
        //udetailsEnd
        //payDetails
        #region PayDetails;
        public string payby { get; set; }
        public string card_name { get; set; }
        public string card_no { get; set; }
        public string card_ccv { get; set; }
        public string card_exp_month { get; set; }
        public int card_exp_year { get; set; }
        #endregion PayDetails;
        //payDetailsEnd
        //cartStart
        #region Cart;
        public int quantity { get; set; }
        public decimal amount_purchase { get; set; }
        public decimal amount_shipping { get; set; }
        public decimal amount_tax { get; set; }
        public string currency_code { get; set; }
        #endregion Cart;
        //cartEnd
        //item
        #region item;
        public string itemName { get; set; }
        public int itemQuantity { get; set; }
        public decimal amount_unit { get; set; }
        public string item_no { get; set; }
        public string item_desc { get; set; }
        #endregion item;
        //itemEnd
        //schedules
        #region Schedules;
        public string startdate { get; set; }
        public int frequency { get; set; }
        public string frequencyunit { get; set; }
        public int repeat { get; set; }
        public int item_no_sched { get; set; }
        public decimal amount_schedule { get; set; }
        #endregion;
        //schedulesEnd
        //purchase
        #region purchase;
        public decimal fraud_amount_purchase { get; set; }
        #endregion purchase;
        //purchaseEnd
        /// <summary>
        /// Top fields are parameter for method 1-4 (other fields are being re-used)
        /// txtParams are not required but must added on the xml
        /// </summary>
        //txid can be found from settlement method 5 (other fields are being re-used)
        public string txid { get; set; }
        //reference and postbackurl can be found from requery method 6 (other fields are being re-used)
        public string reference { get; set; }
        public string postbackurl { get; set; }
        //reason, amount, sendnotification can be found in refund method 7 (other fields are being re-used)
        public string reason { get; set; }
        public decimal amount_refund { get; set; }
        public string sendNotification { get; set; }
        //rid, from_timestamp, to_timestamp, tx_actions, responses,card_types, filter_type, filter_search, limit can be found in getTransactionsByRID method 8,9 (other fields are being re-used)
        public int rid { get; set; }
        public string from_timestamp { get; set; }
        public string to_timestamp { get; set; }
        public string tx_actions { get; set; }
        public string responses { get; set; }
        public string card_types { get; set; }
        public string filter_type { get; set; }
        public string filter_search { get; set; }
        public int limit { get; set; }
        //parent_txid, rebillkey can be found in processRebill method 10 (other fields are being re-used)
        public string parent_txid { get; set; }
        public string rebillkey { get; set; }
        public decimal amount_rebill { get; set; }
        //description and currency can be found in rebillRegister method 11 (other fields are being re-used
        public string description { get; set; }
        public string currency { get; set; }
        //
        public string merchantrebilling { get; set; }
        //
        public string verifyItemName { get; set; }
        public string verifyItemValue { get; set; }
        //
        public int customerID { get; set; }
        //responses
        #region responses;
        public string responseStatus { get; set; }
        public string responseTransactionID { get; set; }
        public string responseTransactionAction { get; set; }
        public string responseAmount { get; set; }
        public string responseCurrency { get; set; }
        public string responseType { get; set; }
        public string responseInfo { get; set; }
        public string responseErrCode { get; set; }
        #endregion responses;

        #region txParams;
        public string txParams_wid { get; set; }
        public string txParams_tid { get; set; }
        public string txParams_ref1 { get; set; }
        public string txParams_ref2 { get; set; }
        public string txParams_ref3 { get; set; }
        public string txParams_ref4 { get; set; }
        public string txParams_addinfo { get; set; }
        public string txParams_ipayinfo { get; set; }
        public string txParams_cmd { get; set; }
        public string txParams_postbackurl { get; set; }
        public string txParams_merchant_rebilling { get; set; }
        public string txParams_kyc_status { get; set; }
        #endregion;

        #region modulecode;
        public string moduleCode { get; set; }
        #endregion modulecode;

        #region convertedtoUSDAmounts
        public decimal usd_amount { get; set; }
        public decimal usd_amountShipping { get; set; }
        public decimal usd_amountTax { get; set; }
        #endregion convertedtoUSDAmounts;

        #region isDefaultAddress;
        public bool isDefaultAddress { get; set; }
        #endregion
    
        public string currencyType { get; set; }

        #region Required Fields for ACH;
        public string ach_routing_no { get; set; }
        public string ach_account_no { get; set; }
        public string ach_type { get; set; }
        public string ach_regulation_e { get; set; }
        public string ach_class { get; set; }
        public string ach_receive_name { get; set; }
        #endregion;

        public string response_comment { get; set; }
        public string response_descriptor { get; set; }
        public string response_rebillkey { get; set; }
        public string response_required { get; set; }
        public string response_TermUrl { get; set; }
        public string response_error { get; set; }
        public string response_sys { get; set; }
        public string response_msg { get; set; }
        
    }
}