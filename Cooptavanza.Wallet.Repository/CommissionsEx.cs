﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CommissionsEx
    {
        public int id { get; set; }
        public string commissionAccountNumber { get; set; }
        public int userID { get; set; }
        public int userRole { get; set; }
        public string userTID { get; set; }
        public string mdr { get; set; }
        public string currency { get; set; }
        public string txRate { get; set; }
        public string txRateFeeCurrency { get; set; }
        public string country { get; set; }
        public string localpaymentservice { get; set; }
        public string method { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string status { get; set; }
        public string transactionTID { get; set; }
    }

    public class TotalCommissionEx
    {
        public string commissionAccountNumber { get; set; }
        public decimal totalAmount { get; set; }
        public string currency { get; set; }
    }

    public class TotalCommissionByCurrencyEx
    {
        public decimal totalCommission { get; set; }
        public string currencyISO { get; set; }
    }

}
