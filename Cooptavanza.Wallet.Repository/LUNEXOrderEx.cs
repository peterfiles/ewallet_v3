﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class LUNEXOrderEx
    {
        public decimal AcctBalance { get; set; }
        public string AcctNum { get; set; }
        public string AcctType { get; set; }
        public decimal Amount { get; set;}
        public decimal Balance { get; set; }
        public decimal Bonus { get; set; }
        public string Carrier { get; set; }
        public string CarrierRetCode { get; set; }
        public string Country { get; set; }
        public string Currency { get; set; }
        public decimal DestAmount { get; set; }
        public string DestCurrency { get; set; }
        public string Instruction { get; set; }
        public string Phone { get; set; }
        public List<Pins> Pins { get; set; }
        public string PromoPhone { get; set; }
        public long PromoPin { get; set; }
        public int Quantity { get; set; }
        public string Sku { get; set; }
        public string Status { get; set; }
        public long TxId { get; set; }
        public string AccessPhone { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCsPhone { get; set; }
        public string CompanyHomeUrl { get; set; }

        public int CustomerId { get; set; }
        public string ProductName { get; set; }
        public string currencyfromiso { get; set; }

    }

    public class Pins {
        public string Pin { get; set; }
    }
}
