﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class CreditCardLogsEx
    {
        public int id { get; set; }
        public string transactionNo { get; set; }
        public string logs { get; set; }
        public Nullable<System.DateTime> dateLogged { get; set; }
        public string status { get; set; }
    }
}
