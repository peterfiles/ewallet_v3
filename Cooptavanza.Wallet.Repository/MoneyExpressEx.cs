﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyExpressEx
    {
        public string ActionCodeField{get;set;}

        public string ConversionRateField{get;set;}

        public string FeeCurrencyField{get;set;}

        public string FloatAmountField{get;set;}

        public string FloatCurrencyField{get;set;}

        public string KioskFeeAmountField{get;set;}

        public string LastUpdateTimeField{get;set;}

        public string NoteField{get;set;}

        public string OriginatorUIDField{get;set;}

        public string PaidFeeAmountField{get;set;}

        public string RcvAccntNoField{get;set;}

        public string RcvFirstNameField{get;set;}

        public string RcvLastNameField{get;set;}

        public string ReferenceIdField{get;set;}

        public string RespCodeField{get;set;}

        public string RespMessageField{get;set;}

        public string SentAmountField{get;set;}

        public string SentCurrencyField{get;set;}

        public string SndKycUrlField{get;set;}

        public string SubmitTimeField{get;set;}

        public string TotalFeeAmountField { get; set; }

        public string TransactionIdField { get; set; }

        public string SignatureField { get; set; }

        

        public string MerchantIdField { get; set; }

        public string TranCurrField { get; set; }
    }
}
