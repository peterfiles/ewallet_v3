﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ConvertedCurrencyEx
    {
        public int id { get; set; }
        public string timestamp { get; set; }
        public string qoutes { get; set; }
        public string @base { get; set; }
        public Nullable<decimal> amount { get; set; }
    }

}
