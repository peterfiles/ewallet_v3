﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class ReceiversCustomerEx
    {
        public int id { get; set; }
        public string accountnumber { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string currency { get; set; }
        public System.DateTime expiryDate { get; set; }
        public string email { get; set; }
        public string mobilenumber { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string username { get; set; }
        public string zipcode { get; set; }
        public Nullable<int> cityid { get; set; }
        public Nullable<int> profileimageId { get; set; }
        public string recentbillimageId { get; set; }
        public int currencyid { get; set; }
        public int stateid { get; set; }
        public int countryid { get; set; }
        public string city { get; set; }
        public string tid { get; set; }
        public bool isInOFACList { get; set; }
        public bool isVerified { get; set; }
        public string theme { get; set; }
        public int agentID { get; set; }
    }
}
