﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class BillerEx
    {
        public int id { get; set; }
        public string name { get; set; }
        public string tid { get; set; }
        public int countryid { get; set; }
        public string country { get; set; }
        public int stateid { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string logo { get; set; }
        public string sid { get; set; }
        public string address { get; set; }
        public int billerType { get; set; }
        public string currencyiso { get; set; }
        public int currencyid { get; set; }
        public List<CurrencyEx> currency { get; set; }
        public string branch { get; set; }

        public string billerTypeName { get; set; }
    }
}
