﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cooptavanza.Wallet
{
    public class MoneyTransferTransactionsEx
    {
        public int id { get; set; }
        public int userID { get; set; }
        public int receiversID { get; set; }
        public int agentID { get; set; }
        public int transactionID { get; set; }
        public string accountNumber { get; set; }
        public string status { get; set; }
        public string trackingnumber { get; set; }
        public string idType { get; set; }
        public string idNumber { get; set; }
        public Nullable<bool> walkin { get; set; }
    }
}
